/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.FacialBean;
import java.security.PublicKey;

/**
 *
 * @author Harpreet Singh
 */
public class verifyFacialSignature {

    CommonUtil util = new CommonUtil();
    VerifyDigitalSignature verify = new VerifyDigitalSignature();
    ParseAsmObject pso = new ParseAsmObject();
    FacialBean fb = new FacialBean();
    String SHAWithRSASIG[] = {"2a864886f70d01010d", "2a864886f70d010101"};
    String SHASignatureAlgo;
    // This is OID for PIV-CHUIDSecurity Object.
    boolean lb_signature = true;

    public verifyFacialSignature() {
        super();
    }

    public FacialBean verifySig(String str_hex_fac, String str_hex_chuid) {
        PublicKey pubkey;
        byte[] fac_data;
        byte[] signature;
        byte[] signed_data = null;
        boolean lb_verify = true;

        signature = getSignature(str_hex_fac);

        if (signature == null) {
            lb_signature = false;
            lb_verify = false;
            fb.setLb_verify(lb_verify);
        }

        fac_data = getFacData(str_hex_fac);
        fb.setFacial_data(fac_data);
        System.out.println("imgae 2 " + util.arrayToHex(fac_data));

        // get Public Key.
        pubkey = pso.getPublicKey(str_hex_chuid);

        if (pubkey == null) {
            lb_verify = false;
            fb.setLb_verify(lb_verify);
        }

        //get the actual data that was signed
        if (lb_verify) {
            signed_data = getSignedData(str_hex_fac);
        }

        if (lb_verify) {
            lb_verify = verify.VerifySignature(signed_data, signature, pubkey, SHASignatureAlgo);
        }

        fb.setLb_verify(lb_verify);
        
        return fb;

    }

    private byte[] getFacData(String str_hex_fac) {

        String fac_data = null;
        //,str_temp_fac, fac_identifier = "46414300";
        String image_start_tag = "ffd8";
        String image_end_tag = "ffd9";
        int start_loc, end_loc;

        // Get the length of Biometric Record and subtract 45. 45 is the fixed length of bytes added
        // to Biometric record to construct facial header and features block.
        //String bdb_len = str_hex_fac.substring(4,8);
        //biolen=Integer.valueOf(bdb_len, 16).intValue() - 45;

        //Now look for "46414300". This is Format Identifier for facial data.
        // Get FACIAL DATA..on which signature has been calculated.
        //start_loc = util.findEndLocation(str_hex_fac, fac_identifier) ;
        //str_temp_fac=str_hex_fac.substring(start_loc + 1);

        // Now look for image_start_tag. Subtract 3 to get the pointer to start of image tag
        start_loc = util.findEndLocation(str_hex_fac, image_start_tag) - 4;
        end_loc = util.findEndLocation(str_hex_fac, image_end_tag);
        fac_data = str_hex_fac.substring(start_loc, end_loc);



        return util.hex1ToByteArray(fac_data);

    }

    private byte[] getSignedData(String str_hex_fac) {

        byte[] error_byte = {(byte) 0xfe, 0x00};
        String fac_data = null;

        String image_end_tag = "ffd9";
        int start_loc, end_loc;

        start_loc = 0;
        // Now look for image_start_tag. Subtract 3 to get the pointer to start of image tag

        end_loc = util.findEndLocation(str_hex_fac, image_end_tag);
        fac_data = str_hex_fac.substring(start_loc, end_loc);

        System.out.println("sec - fac " + fac_data);

       return util.combine_data(util.hex1ToByteArray(fac_data), error_byte);

    }

    private byte[] getSignature(String str_hex_fac) {

        int i_startsig = 0, sigLen;
        // String octet_tag = "04";
        String str_sig;
        byte[] sig_encoded;
        String strNullSeprator="05"; 
        boolean loopVar=true;
        int i=0;
         String sig_length;
        
        while (loopVar == true ) {
            //Find End of "SHA512WithRSA" OID and add 1 to get to start of Certificate
            //i_startsig = util.findEndLocation(str_hex_chuid, SHAWithRSASIG[i]);
            i_startsig = util.findFromEnd(str_hex_fac, SHAWithRSASIG[i]);
            
            if (i_startsig > 0 || i== SHAWithRSASIG.length -1 ){
                SHASignatureAlgo = SHAWithRSASIG[i];
                loopVar = false;
            }
            i++;
        }
        
        
        
        if (i_startsig == 0) {
            
            sig_encoded = null;
        } else {

            
            if (str_hex_fac.substring(i_startsig, i_startsig+2).equalsIgnoreCase(strNullSeprator)) {
                sig_length = str_hex_fac.substring(i_startsig + 8, i_startsig + 12);
            }else{
                sig_length = str_hex_fac.substring(i_startsig + 4, i_startsig + 8);
            }
                
            
            
            sigLen = Integer.valueOf(sig_length, 16).intValue();

            str_sig = str_hex_fac.substring(i_startsig + 8, i_startsig + 8 + sigLen * 2);


            sig_encoded = util.hex1ToByteArray(str_sig);

        }

        return sig_encoded;

    }
}
