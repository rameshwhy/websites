/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;

/**
 *
 * @author Satish K
 */
public class FacialImageBean {
    String facialImage;
    byte[] facialData;
    String CreationDate;
    String ExpirationDate;
    String ValidFromDate;
    String Creator;
    private PublicKey publicKey;
    private PrivateKey privateKey;
    byte[] contentCertificate;
    Date signatureDate;
    String algoType;
    
     //YYMMDDhhmmssZ
    public String getCreationDate() {
        return CreationDate;
    }

    public void setCreationDate(String CreationDate) {
        this.CreationDate = CreationDate;
    }

    public String getCreator() {
        return Creator;
    }

    public void setCreator(String Creator) {
        this.Creator = Creator;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public void setExpirationDate(String ExpirationDate) {
        this.ExpirationDate = ExpirationDate;
    }

    public byte[] getContentCertificate() {
        return contentCertificate;
    }

    public void setContentCertificate(byte[] contentCertificate) {
        this.contentCertificate = contentCertificate;
    }

    public byte[] getFacialData() {
        return facialData;
    }

    public void setFacialData(byte[] facialData) {
        this.facialData = facialData;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public String getValidFromDate() {
        return ValidFromDate;
    }

    public void setValidFromDate(String ValidFromDate) {
        this.ValidFromDate = ValidFromDate;
    }

    public String getFacialImage() {
        return facialImage;
    }

    public void setFacialImage(String facialImage) {
        this.facialImage = facialImage;
    }

    public Date getSignatureDate() {
        return signatureDate;
    }

    public void setSignatureDate(Date signatureDate) {
        this.signatureDate = signatureDate;
    }

    public String getAlgoType() {
        return algoType;
    }

    public void setAlgoType(String algoType) {
        this.algoType = algoType;
    }

    
    
   
    
    
    
}
