package com.id3.fingerprint;

public class FingerCaptureDeviceModel
{
  public static final int Certis = 0;
  public static final int Secugen = 1;
  public static final int Upek = 2;
  public static final int Futronic = 3;
  public static final int Suprema = 4;
  public static final int Dakty = 5;
  public static final int PreciseBiometrics = 6;
  public static final int Biothentic = 7;
  public static final int CrossMatchVerifier = 8;
  public static final int DigitalPersona = 9;
  public static final int MorphoSmart = 10;
  public static final int CovadisAuriga = 11;
  public static final int Lumidigm = 12;
  public static final int SupremaRealScan = 13;
  public static final int GreenBit = 14;
  public static final int CrossMatchLScan = 15;
  public static final int Identix = 16;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCaptureDeviceModel
 * JD-Core Version:    0.6.2
 */