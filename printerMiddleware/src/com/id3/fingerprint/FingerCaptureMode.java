package com.id3.fingerprint;

public class FingerCaptureMode
{
  public static final int Flat = 0;
  public static final int Rolled = 1;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCaptureMode
 * JD-Core Version:    0.6.2
 */