
package com.secuera.middleware.cardreader.core;

import java.security.PublicKey;
import java.security.cert.CertificateException;

import sun.security.x509.X509CertImpl;

public class ParseAsmObject {

    // Author: Harpreet Singh
    CommonUtil util = new CommonUtil();
    VerifyDigitalSignature verify = new VerifyDigitalSignature();
    
    
    
    // This is OID for PIV-CHUIDSecurity Object.
    String PIVChuidSecurity = "6086480165030601";
    String SHAWithRSASIG[] =  {"2a864886f70d01010d","2a864886f70d010101"};
    String SHASignatureAlgo; 

    public ParseAsmObject() {
        super();
    }

    public boolean parseChuidObj(String str_hex_chuid) {

        PublicKey pubkey;
        byte[] chuid_data;
        byte[] signature_data;
        boolean lb_verify;

        // get Public Key.
        pubkey = getPublicKey(str_hex_chuid);
        System.out.println(pubkey.toString());
        if (pubkey == null) {
            lb_verify = false;
            return lb_verify;
        }

        chuid_data = getChuidData(str_hex_chuid);

        signature_data = getSignature(str_hex_chuid);

        if (signature_data == null) {
            lb_verify = false;
            return lb_verify;
        }


        lb_verify = verify.VerifySignature(chuid_data, signature_data, pubkey,SHASignatureAlgo);
        
        return lb_verify;

    }

    private byte[] getChuidData(String str_hex_chuid) {
        String start_tag = "30";
        String end_tag = "3E";
        byte[] error_byte = {(byte) 0xFE, 0x00};
        int start_loc, end_loc;

        // Get CHUID DATA..on which signature has been calculated.
        start_loc = util.findtagLocation(0, str_hex_chuid, start_tag);
        end_loc = util.findtagLocation(0, str_hex_chuid, end_tag);

        //String chuid_data = str_hex_chuid.substring(start_loc, end_loc);
        //String chuid_data = "5382061a3019d4e739da739ced39ce739da1685a08c92ade0a6184e739c3e232043132333433093132333435363738393410f15b57e29cde4e2bb0c116dcfe9ba9ab35083230313630313236fe00";
        String chuid_data = "3019d4e739da739ced39ce739da1685a08c92ade0a6184e739c3e232043132333433093132333435363738393410f15b57e29cde4e2bb0c116dcfe9ba9ab35083230313630313236fe00";
        
                //return util.combine_data(util.hex1ToByteArray(chuid_data), error_byte);

            return util.hex1ToByteArray(chuid_data);
    }

    public PublicKey getPublicKey(String str_hex_chuid) {
        int i_startcert, certLen;
        String any_tag = "A0";
        String str_cert;
        byte[] cert_encoded;

        //Find End of "PIVChuidSecurity" OID and add 1 to get to start of Certificate
        i_startcert = util.findEndLocation(str_hex_chuid, PIVChuidSecurity);//sat123 + 1;

        //a0 82 03 71
        if ((str_hex_chuid.substring(i_startcert, i_startcert + 2)).equalsIgnoreCase(any_tag)) {

            // Get Certificate data
            String cert_length = str_hex_chuid.substring(i_startcert + 4, i_startcert + 8);
            certLen = Integer.valueOf(cert_length, 16).intValue();
            str_cert = str_hex_chuid.substring(i_startcert + 8, i_startcert + 8 + certLen * 2);

            
            
            cert_encoded = util.hex1ToByteArray(str_cert);
            // parse PublicKey out of certificate data.
            X509CertImpl cert = null;
            try {
                cert = new X509CertImpl(cert_encoded);
            } catch (CertificateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
         
            return cert.getPublicKey();

        } else {
            return null;
        }


    }

    private byte[] getSignature(String str_hex_chuid) {

        int i_startsig = 0, sigLen;
        // String octet_tag = "04";
        String str_sig;
        byte[] sig_encoded;
        String strNullSeprator="05"; 
        boolean loopVar=true;
        int i=0;
         String sig_length;
        
        while (loopVar == true ) {
            //Find End of "SHA512WithRSA" OID and add 1 to get to start of Certificate
            //i_startsig = util.findEndLocation(str_hex_chuid, SHAWithRSASIG[i]);
            i_startsig = util.findFromEnd(str_hex_chuid, SHAWithRSASIG[i]);
            
            if (i_startsig > 0 || i== SHAWithRSASIG.length -1 ){
                SHASignatureAlgo = SHAWithRSASIG[i];
                loopVar = false;
            }
            i++;
        }
        
        
        
        if (i_startsig == 0) {
            
            sig_encoded = null;
        } else {

            
            if (str_hex_chuid.substring(i_startsig, i_startsig+2).equalsIgnoreCase(strNullSeprator)) {
                sig_length = str_hex_chuid.substring(i_startsig + 8, i_startsig + 12);
            }else{
                sig_length = str_hex_chuid.substring(i_startsig + 4, i_startsig + 8);
            }
                
            
            
            sigLen = Integer.valueOf(sig_length, 16).intValue();

            str_sig = str_hex_chuid.substring(i_startsig + 8, i_startsig + 8 + sigLen * 2);

            str_sig="1d672392fa93f834544962b39a5e2fc174c2bd01fbc817d8ebb45c6e34ad6151c630d191e942684b0351addca726e13a7589986d8c733d75042fc9aa24ee9342273bdfc7c5070490311e5c07d1514cb29bb6ec9d259ea7dbc915134d22e5132c6ae64592ecfe4ce9d530b5f333cffcb5836eb2d7b0f1203f6c289ded7fbad9959b267ebd1ced5b1679c890164d214b028d02f382874a6d1886edc5b38708c4fc5fa675f4303976718e3f5e4e5fed7b4f20321515589f3e0e05a1ab40f08933131041e3d6c75798c131d9072ee7f3f4acf7a138982fc38540862572da985c3ee90006069135d16048fd075c322412f2c50af2abe70af2f7e1d60ed1194a7a68ab";

            sig_encoded = util.hex1ToByteArray(str_sig);

        }

        return sig_encoded;

    }
}
