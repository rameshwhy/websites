/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.applet; 

import com.secuera.middleware.beans.piv.CSRBean;
import com.secuera.middleware.beans.piv.CardInfoBean;
import com.secuera.middleware.beans.piv.CertificateBean;
import com.secuera.middleware.beans.piv.ChuidBean;
import com.secuera.middleware.beans.piv.ContainerInfoBean;
import com.secuera.middleware.beans.piv.ContentCertificateBean;
import com.secuera.middleware.beans.piv.CredentialInformationBean;
import com.secuera.middleware.beans.piv.CredentialKeys;
import com.secuera.middleware.beans.piv.FacialBean;
import com.secuera.middleware.beans.piv.FacialImageBean;
import com.secuera.middleware.beans.piv.FascnBean;
import com.secuera.middleware.beans.piv.InitializationBean;
import com.secuera.middleware.beans.piv.LoadUserCertificates;
import com.secuera.middleware.beans.piv.MasterKeyCeremonyBean;
import com.secuera.middleware.beans.piv.MasterKeyCeremonyResultBean;
import com.secuera.middleware.beans.piv.PersonalInformationBean;
import com.secuera.middleware.beans.piv.PersonalizationBean;
import com.secuera.middleware.beans.piv.PinPolicyBean;
import com.secuera.middleware.beans.piv.PrintedInfoBean;
import com.secuera.middleware.beans.piv.UserCSR;
import com.secuera.middleware.beans.piv.UserCSRResponse;
import com.secuera.middleware.beans.piv.UserCertificates;
import com.secuera.middleware.beans.piv.UserContentCSR;
import com.secuera.middleware.beans.piv.UserFingerPintBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.validate.CardReaderValidator;
import com.secuera.middleware.core.common.CardComm;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.wrapper.piv.MiddleWareServerCardreader;
import com.secuera.middleware.wrapper.piv.MiddleWareServerCardreaderImpl;
import java.applet.Applet;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.CardChannel;

import org.json.JSONObject;



/**
 *
 * @author Nilay
 */
public class CardApplet extends Applet {

    static CardComm cardConnect = null;
    List<String> terminals = null;
    String cardConnectStatus;
    CardreaderError cardError = null;
    CardreaderResponseBean cardResponse = null;
    private static MiddleWareServerCardreader mServer ;
    
    //declare beans to access from applet
    public CardInfoBean getCardInfoBean() {
        return new CardInfoBean();
    }

    public ChuidBean getChuidBean() {
        return new ChuidBean();
    }

    public InitializationBean getInitializationBean() {
        return new InitializationBean();
    }

    public PrintedInfoBean getPrintedInfoBean() {
        return new PrintedInfoBean();
    }

    public PersonalizationBean getPersonalizationBean() {
        return new PersonalizationBean();
    }

    public CSRBean getCSRBean() {
        return new CSRBean();
    }
      
    public UserCSR getUserCSR() {
        return new UserCSR();
    }
 
    public FascnBean getFascnBean() {
        return new FascnBean();
    }
    
    public CredentialKeys getCredentialKeys() {
        return new CredentialKeys();
    }
    
    public MasterKeyCeremonyBean getMasterKeyCeremonyBean() {
        return new MasterKeyCeremonyBean();
    }

    public MasterKeyCeremonyResultBean getMasterKeyCeremonyResultBean() {
        return new MasterKeyCeremonyResultBean();
    }
    
    public UserFingerPintBean getUserFingerPintBean(){
        return new UserFingerPintBean();
    }

    public static MiddleWareServerCardreader getmServer() {
        if(mServer== null){
           mServer =  new MiddleWareServerCardreaderImpl();
        }
        return mServer;
    }
  
    
    
    //Get terminal list for terminal selection
    public CardreaderResponseBean getTerminalList() {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();

        List<String> terminalName = null;
        try {

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            terminalName = getmServer().getTerminalsList();


            //set return Value
            cardResponse.setCardreaderResponse(terminalName);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        }
        return cardResponse;
    }

    //Get terminal list for terminal selection
    public boolean isCardPresent(String deviceName) {
        String returnIIN;
        boolean retStatus = false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                return false;
            }


            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnIIN = getmServer().readIIN(channel);
            if (returnIIN.length() != 0) {
                retStatus = true;
            }

        } catch (Exception e) {
            retStatus = false;

        } finally {
            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return retStatus;
    }

    
    
    
    
    /******************* Read credential Information ***************************************/
    public CardreaderResponseBean readCredentialInformation(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        final String deviceNm = deviceName;

        //Check if the PIN is Valid            
        try {
            Object returnObject = AccessController.doPrivileged(new PrivilegedAction() {
                @Override
                public Object run() {
                    CardChannel channel = null;
                    CredentialInformationBean credentialInformation = null;
                    try {
                        // Start communication with card & return error Message is any 
                        channel = startCardCommunication(deviceNm);
                        if (channel == null) {
                            cardError.setErrorCode("-1");
                            cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                            return cardError;
                        }

                        MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
                        credentialInformation = mws.readCredentialInformation(channel);

                    } catch (MiddleWareException ex) {
                        cardError.setErrorCode("-1");
                        cardError.setErrorMsg(ex.getMessage());
                        return cardError;
                    } finally {                        
                        //stop communication & return success
                        if (channel != null) {
                            cardConnect.stopComm();
                        }
                    }
                    return credentialInformation; // nothing to return
                }
            });

            if (null != returnObject && (returnObject instanceof CardreaderError)) {
                cardResponse.setCardreaderError((CardreaderError) returnObject);
            } else if (null != returnObject && (returnObject instanceof CredentialInformationBean)) {
                cardResponse.setCardreaderResponse((CredentialInformationBean) returnObject);
                
            } else {
                throw new Exception("The return object of the code is not developed properly");
            }
        } catch (Exception ex) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(ex.getMessage());
            cardResponse.setCardreaderError(cardError);            
        }
        return cardResponse;
    }
    
    //CHUID-FASCN Functions
    public CardreaderResponseBean readChuid(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        ChuidBean chuid = null;
        final String deviceNm = deviceName;
        try {
            Object returnObject = AccessController.doPrivileged(new PrivilegedAction() {
                @Override
                public Object run() {
                    CardChannel channel = null;
                    ChuidBean chuid = null;
                    try {
                        // Start communication with card & return error Message is any 
                        channel = startCardCommunication(deviceNm);
                        if (channel == null) {
                            cardError.setErrorCode("-1");
                            cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                            return cardError;
                        }

                        MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
                        chuid = mws.readChuid(channel);

                    } catch (MiddleWareException ex) {
                        cardError.setErrorCode("-1");
                        cardError.setErrorMsg(ex.getMessage());
                        return cardError;
                    } finally {
                        // stop communication & return success
                        if (channel != null) {
                            cardConnect.stopComm();
                        }
                    }
                    return chuid; // nothing to return
                }
            });

            //Check if the response Object is of type Error else set the exception
            if (null != returnObject && (returnObject instanceof CardreaderError)) {
                cardResponse.setCardreaderError((CardreaderError) returnObject);
            } else if (null != returnObject && (returnObject instanceof ChuidBean)) {
                cardResponse.setCardreaderResponse((ChuidBean) returnObject);
            } else {
                throw new Exception("The return object of the code is not developed properly");
            }

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        }
        return cardResponse;
    }
    
    //CIN/IIN/CUID/BAP
    public CardreaderResponseBean readCIN(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        String returnCIN;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnCIN = getmServer().readCIN(channel);

            //set return Value
            cardResponse.setCardreaderResponse(returnCIN);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read CIN : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read CIN (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;

    }

    public CardreaderResponseBean readIIN(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        String returnIIN;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnIIN = getmServer().readIIN(channel);

            //set return Value
            cardResponse.setCardreaderResponse(returnIIN);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read IIN : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read IIN (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean readCUID(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        String returnCUID;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnCUID = getmServer().readCUID(channel);

            //set return Value
            cardResponse.setCardreaderResponse(returnCUID);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read CUID : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read CUID (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean readBAP(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        String returnBAP;
        CardChannel channel = null;

        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnBAP = getmServer().readBAP(channel);

            //set return Value
            cardResponse.setCardreaderResponse(returnBAP);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read BAP : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read BAP (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }
    
    public CardreaderResponseBean readATR(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        String returnBAP;
        CardChannel channel = null;

        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnBAP = getmServer().readATR(channel);

            //set return Value
            cardResponse.setCardreaderResponse(returnBAP);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read ATR : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read ATR (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }
    
    public CardreaderResponseBean readCardModel(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        String returnBAP;
        CardChannel channel = null;

        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnBAP = getmServer().readCardModel(channel);

            //set return Value
            cardResponse.setCardreaderResponse(returnBAP);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read Card Model : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read Card Model (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }
    
    //read card info = CIN/IIN/CUID/BAP
    public CardreaderResponseBean readCardInfo(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        CardInfoBean cardInfo = new CardInfoBean();
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            cardInfo = getmServer().readCardInfo(channel);

            //set return Value
            cardResponse.setCardreaderResponse(cardInfo);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read Card Info : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read Card Info (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;

    }
    /*****************************************************************************************/
    
    
    /************************ User certificates *********************************************/
    public CardreaderResponseBean readUserCertificates(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        
        final String deviceNm = deviceName;
        try {

            Object returnObject = AccessController.doPrivileged(new PrivilegedAction() {
                public Object run() {
                    UserCertificates userCertificate;
                    CardChannel channel = null;
                    try {  
                        channel = startCardCommunication(deviceNm);
                        if (channel == null) {
                            cardError.setErrorCode("-1");
                            cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                            return cardError;
                        }
                        
                        MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
                        userCertificate = mws.readUserCertificates(channel);
                        
                    } catch (MiddleWareException ex) {
                        cardError.setErrorCode("-1");
                        cardError.setErrorMsg(ex.getMessage());
                        return cardError;
                    } finally {
                        // stop communication & return success
                        if (channel != null) {
                            cardConnect.stopComm();
                        }
                    }
                    return userCertificate; // nothing to return
                }
            });

            
            //Check if the response Object is of type Error else set the exception
            if (null != returnObject && (returnObject instanceof CardreaderError)) {
                cardResponse.setCardreaderError((CardreaderError) returnObject);
            } else if (null != returnObject && (returnObject instanceof UserCertificates)) {
                cardResponse.setCardreaderResponse((UserCertificates) returnObject);
            } else {
                throw new Exception("The return object of the code is not developed properly");
            }
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        }
        return cardResponse;

        

    }
    
    public CardreaderResponseBean readCertificate(String deviceName, String certType) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        
        final String certTypeO = certType;
        final String deviceNm = deviceName;
        try {

            //certBean = (CertificateBean) AccessController.doPrivileged(new PrivilegedAction() {
            Object returnObject = AccessController.doPrivileged(new PrivilegedAction() {
                public Object run() {
                    CertificateBean certBean;
                    CardChannel channel = null;
                    try {
                        
                        channel = startCardCommunication(deviceNm);
                        if (channel == null) {
                            cardError.setErrorCode("-1");
                            cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                            return cardError;
                        }
                        
                        MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
                        certBean = mws.readCertificate(channel, certTypeO);
                        
                        
                    } catch (MiddleWareException ex) {
                        cardError.setErrorCode("-1");
                        cardError.setErrorMsg(ex.getMessage());
                        return cardError;
                    } finally {
                        // stop communication & return success
                        if (channel != null) {
                            cardConnect.stopComm();
                        }
                    }                    
                    return certBean; // nothing to return
                }
            });

            //Check if the response Object is of type Error else set the exception
            if (null != returnObject && (returnObject instanceof CardreaderError)) {
                cardResponse.setCardreaderError((CardreaderError) returnObject);
            } else if (null != returnObject && (returnObject instanceof CertificateBean)) {
                cardResponse.setCardreaderResponse((CertificateBean) returnObject);
            } else {
                throw new Exception("The return object of the code is not developed properly");
            }
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        }

        return cardResponse;

    }
    
    /*****************************************************************************************/
    
    /************************ generate User CSR/Certificate  *******************************************/
    public CardreaderResponseBean generateUserCSR(String deviceName, String adminKey, String localPin, UserCSR userCSR) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        UserCSR retUserCSR = new UserCSR();
        final String deviceNameO = deviceName;
        final String adminkeyO = adminKey;
        final String localPinO = localPin;
        final UserCSR userCSRO = userCSR;
        try {

            //retUserCSR = (UserCSR) AccessController.doPrivileged(new PrivilegedAction() {
            Object returnObject = AccessController.doPrivileged(new PrivilegedAction() {

                public Object run() {
                    UserCSR retUserCSR;
                    CardChannel channel = null;
                    // System.out.println("Certificate Bean >>>>>>>>>> 222  :  " + deviceNameO + "  :  " + certTypeO);
                    try {
                        channel = startCardCommunication(deviceNameO);
                        if (channel == null) {
                            cardError.setErrorCode("-1");
                            cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                            return cardError;
                        }

                        MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
                        retUserCSR = mws.generateUserCSR(channel, adminkeyO, localPinO, userCSRO);

                    } catch (MiddleWareException ex) {
                        cardError.setErrorCode("-1");
                        cardError.setErrorMsg(ex.getMessage());
                        return cardError;
                    } finally {
                        // stop communication & return success
                        if (channel != null) {
                            cardConnect.stopComm();
                        }
                    }  
                    return retUserCSR; // nothing to return
                }
            });

            //Check if the response Object is of type Error else set the exception
            if (null != returnObject && (returnObject instanceof CardreaderError)) {
                cardResponse.setCardreaderError((CardreaderError) returnObject);
            } else if (null != returnObject && (returnObject instanceof UserCSR)) {
                cardResponse.setCardreaderResponse((UserCSR) returnObject);
            } else {
                throw new Exception("The return object of the code is not developed properly");
            }
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        }

        return cardResponse;

    }
    
    public CardreaderResponseBean generateUserCertificate(String deviceName, UserCSR userCSR, String inURL,String caType) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        CardChannel channel = null;
        
        
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            userCSR = getmServer().generateUserCertificate(channel, userCSR, inURL,caType);

            //set return status
            cardResponse.setCardreaderResponse(userCSR);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            // stop communication & return success
            cardConnect.stopComm();
        }

        return cardResponse;

    }
    /*****************************************************************************************/
    
    /************************ generate Content CSR/Certificate *******************************************/
    public CardreaderResponseBean generateContentCSR(String deviceName,CSRBean certBean) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        final String deviceNameO = deviceName;
        final CSRBean csrBeanO = certBean;
        try {

            //retUserCSR = (UserCSR) AccessController.doPrivileged(new PrivilegedAction() {
            Object returnObject = AccessController.doPrivileged(new PrivilegedAction() {

                public Object run() {
                    UserContentCSR retUserContentCSR;
                    CardChannel channel = null;                    
                    try {
                        channel = startCardCommunication(deviceNameO);
                        if (channel == null) {
                            cardError.setErrorCode("-1");
                            cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                            return cardError;
                        }

                        MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
                        retUserContentCSR = mws.generateContentCSR(channel, csrBeanO);

                    } catch (MiddleWareException ex) {
                        cardError.setErrorCode("-1");
                        cardError.setErrorMsg(ex.getMessage());
                        return cardError;
                    } finally {
                        // stop communication & return success
                        if (channel != null) {
                            cardConnect.stopComm();
                        }
                    }  
                    return retUserContentCSR; // nothing to return
                }
            });

            //Check if the response Object is of type Error else set the exception
            if (null != returnObject && (returnObject instanceof CardreaderError)) {
                cardResponse.setCardreaderError((CardreaderError) returnObject);
            } else if (null != returnObject && (returnObject instanceof UserContentCSR)) {
                cardResponse.setCardreaderResponse((UserContentCSR) returnObject);
            } else {
                throw new Exception("The return object of the code is not developed properly");
            }
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        }

        return cardResponse;

    }
    
    public CardreaderResponseBean generateContentCertificate(String deviceName,String contentCSR, String inURL,String caType) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        CardChannel channel = null;
        String ContentCertificate=null;
        
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            ContentCertificate = getmServer().generateContentCertificate(channel, contentCSR,inURL,caType);

            //set return status
            cardResponse.setCardreaderResponse(ContentCertificate);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            // stop communication & return success
            cardConnect.stopComm();
        }

        return cardResponse;

    }
    /******************************************************************************************/
    
    
    /**************************** Personal Information ***************************************/
    public CardreaderResponseBean readPersonalInformation(String deviceName, String pin,String pinType) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        PersonalInformationBean personalInfo = new PersonalInformationBean();
        CardChannel channel = null;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            personalInfo = getmServer().readPersonalInformation(channel, pin,pinType);

            //set return status
            cardResponse.setCardreaderResponse(personalInfo);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read Personal Info : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read Personal Info (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }
    
    public CardreaderResponseBean readPrintedInfo(String deviceName, String pin,String pinType) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        PrintedInfoBean printInfo = new PrintedInfoBean();
        CardChannel channel = null;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            printInfo = getmServer().readPrintedInfo(channel, pin,pinType);

            //set return status
            cardResponse.setCardreaderResponse(printInfo);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read Printed Info : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read Printed Info (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean readFacialImage(String deviceName, String pin,String pinType) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        CardChannel channel = null;
        String imc = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            FacialBean facialBean = new FacialBean();
            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            facialBean = getmServer().readFacialImage(channel, pin,pinType);

            //set return Value
            cardResponse.setCardreaderResponse(facialBean);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read Facial Image : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read Facial Image (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;
    }

    /******************************************************************************************/
    
    /********************** PIN Management Functions ******************************************/ 
    //Local PIN Management Functions
    public CardreaderResponseBean verifyLocalPin(String deviceName, String localPin) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean returnStatus = false;
        CardChannel channel = null;
        
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }
            
            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().verifyLocalPin(channel, localPin);

            //set return Value
            cardResponse.setCardreaderResponse(returnStatus);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Verify Local PIN : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Verify Local PIN (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;
    }

    public CardreaderResponseBean changeLocalPin(String deviceName, String oldPin, String newPin) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean returnStatus = false;
        CardChannel channel = null;
        
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().changeLocalPin(channel, oldPin, newPin);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Change Local PIN : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Change Local PIN (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean resetLocalPin(String deviceName, String unblockPin, String newPin) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean returnStatus = false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().resetLocalPin(channel, unblockPin, newPin);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);


        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Reset Local PIN : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Reset Local PIN (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    //Global PIN Management Functions
    public CardreaderResponseBean verifyGlobalPin(String deviceName, String globalPin) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean returnStatus = false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().verifyGlobalPin(channel, globalPin);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Verify Global PIN : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Verify Global PIN (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean changeGlobalPin(String deviceName, String oldPin, String newPin) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean returnStatus = false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().changeGlobalPin(channel, oldPin, newPin);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);


        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Change Global PIN : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Change Global PIN (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean resetGlobalPin(String deviceName,CredentialKeys credentialKeys, String newPin,int globalPinRetry) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean returnStatus = false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().resetGlobalPin(channel, credentialKeys, newPin,globalPinRetry);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);


        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Reset Global PIN : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Reset Global PIN (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;
    }

    //change PUK
    public CardreaderResponseBean changePUK(String deviceName, String oldPUK, String newPUK) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean returnStatus = false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().changePUK(channel, oldPUK, newPUK);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);


        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Change PUK : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Change PUK (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean resetPUK(String deviceName, CredentialKeys credentialKeys, String newPUK, int pukRetry) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean returnStatus = false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().resetPUK(channel, credentialKeys, newPUK, pukRetry);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);


        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Rest PUK : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Reset PUK (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }
    
    /******************************************************************************************/
    
    /********************** card personalization ***********************************************/ 
    public CardreaderResponseBean cardPersonalization(String deviceName,CredentialKeys credentialKeys, PersonalizationBean persoBean) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        
        CardChannel channel = null;
        List errorList = new ArrayList(); 
        
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }
            
            // Initialize - personalize card
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            errorList = getmServer().cardPersonalization(channel, credentialKeys, persoBean);

            //set return Value
            cardResponse.setCardreaderResponse(errorList);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Card Personalization : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Card Personalization (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;
    }
        
    public CardreaderResponseBean cardInitialization(String deviceName,CredentialKeys credentialKeys,InitializationBean initvalue) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        
        CardChannel channel = null;
        List errorList = new ArrayList(); 
        
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }
            
            // Initialize - personalize card
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            errorList = getmServer().cardInitialization(channel, credentialKeys, initvalue);

            //set return Value
            cardResponse.setCardreaderResponse(errorList);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Card Personalization : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Card Personalization (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;
    }
       
    public CardreaderResponseBean writeChuid(String deviceName, ChuidBean chuid, String adminKey,String algorithmType) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean chuidStatus=false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            chuidStatus = getmServer().writeChuid(channel, chuid, adminKey,algorithmType);



            //Security.addProvider(bouncyCastleProvider);
            //set return Value
            cardResponse.setCardreaderResponse(chuidStatus);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Write CHUID : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Write CHUID (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean writePrintedInfo(String deviceName, String adminKey, PrintedInfoBean infoBean) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean returnStatus = false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            //validate values
            List printedInfoList = new ArrayList();
            CardReaderValidator cardValidator = new CardReaderValidator();

            printedInfoList = cardValidator.validatePrintedInfo(infoBean);
            if (!printedInfoList.isEmpty()) {
                cardError.setErrorCode("-1");
                //cardError.setErrorMsg(retValue);
                cardError.setErrorList(printedInfoList);
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }
            
            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().writePrintedInfo(channel, infoBean, adminKey);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);
       } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Write Printed Info : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Write Printed Info (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean writeFacialImage(String deviceName, String adminKey,FacialImageBean facialImage,String algoType) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean returnStatus = false;
        CardChannel channel = null;

        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }
            CommonUtil cmnutil = new CommonUtil();
            //byteimage="/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADnAKwDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDp9YX/AInV/wD9fEn/AKEaz2XjFaurjOr33/XxJ/6EazWGTWq2MHuRiIdaQripqguZkgheR2AVRkk0Ac14v1m30rTysr4aTgKvJNeS32qyahLuKHA4RB2q34q1VtV1eWTeWjBwv0rKswPPGTg4+X61LLS0LWnQxXEjxz/Llcg+9NWQLuQY29PpTlki2nPDjpVJt6uSBkHrUlFkNs4Y5XtVu3jnjAeH7uc89KzlDEAjOK0rKSVCEAyvpikMfeREjdKrRuevHBrNWZoSUB+VjyK15o7uYlTuKemKqtpUsuRjDjpTuHKV4Lx4Jd2fatiy8QzloYJHCxj5dxHQVlTaNeRAHy2IPfFVJoZoWAdCpFCaE4s6Oe4dnZbQgxB8ZPc4rGuWkjuCSMYPWq6XUqYw1WDcRXSH7VIQV+4FFMD6T+FXj61vfCVva6hOiXFt+6/3lHSvQ7TXdOvZfKgukaT+7nmvjvTdRutJEc9pLmIHDL0z7V69ot1D4isYJ4FNrcuv7uaE8bx2NSxWPeM0ZrmfA+vPr3h9JZ+LmFjDN/vLxXSE/jQA7NJk+tN3H+7RuPpTuI811Zf+Jve/9d3/APQjWc0ZJyM12OqaK8l5NKgzvdm/M1g69bjR9EkupW2MzLGmOpJ9P1q76EW1MG4uobZHeZ9qJ1Jrzzxf4uhuLV7OxbJcYZvQVr3usWj28mkatGVcuXeRjtO3298V5bq0dvbX862splgLHy3J5K9qV7lqJQLMSO4FP3DZ5e3JzkYqNdzMqqMseAAOtdn4d8ITTPHNOOTzg9qmUlFamkYuRz1jol3fuNkbc+1dfYfD24kRWmOPYV6Bpuj21lGu2MbvU1rLgY9K5pVX0OmNJI8/HgBEXgE47VYtvBHlvxC+T7Zr0WBVbAwK1bdY0AO3ocUKTY3FI89h8GODkA4PG0jFTP4OA+XaVIHU16grxbBtQMarzmF22kYrQzsecR+GSCqvCCp/Wuf8S+DxNExgj2sPSvYGSExnb+dZF3brznkGs27GiinufOGqeG7ywUvtZlHXA6VibiOCK+jL3TrebcrRgg9RivKvGXhM2kj3dlH+76uo7VcKvRmc6NtUcrakyL5eW2E5wOa7XwZrUumpdWoLIARMhPYj/wCtXAwuyvgHB6c1t+HGSbWYYr26WC3Y4kkY8be9bdDnPp34XRn/AIRmW9dNjXlw84X2J4ruwcivLvCvj7RPtq6VFLiFUAjkIwCB6V6HBq9lNgLOqk9AxxSJL9FNBBGQaWkA1lBY5Heub8aaC+v+HZrWFgkykSRt6MOldL1rE8VyX0Xh27bTSVugh2MBnBqyep88eL7/AEnUrMW+rxS2WvWg8t2VMiQDpmvL5Buc85FbOuXNze38kt5I8k5OGZutSeH9JF/qsSMMouGYUnpqaRuzd8HeFTKyXd3HknlAewr0q2tVgUAAD2FMsYEt4gqgdKsOxWuWTcndnXFcqsiXfThJxVMuxNOVnbtU2KuakE5B9KvLdFWHTHuaxAWqVJnDDIyPWrSE2dCl02MbiD7UvnYHDEknmsdX7j73erCtkAuxzjjFUKxoNPnoBn2qjdTcY4phYqn3sVSmdiTg8VLGiKaQE1m3UaTxsrgEN2NTzMc1SdiDyazZdzyzxV4fbTLw3MCnyJOoA+6a5feQeOBXuOpWUd9ZPE6ghlxXi+qWLafeyQMMFTxXRTldWZz1YW1Rd0vUpkvYJBOUZMAMe1esaF4pvp7u2traB5ULfPM6Z3Dv9K8StnZJVK+te4+CNYu2tIYlsIRGMK8qcOVxnjtWhi7Htmlakb1NnlFCnDVq5rN0VbZdNjNqcxn165960qCAprqHRlIyCMU6iqIPlz4p+H4dG8TTNHgCYmTb6VmeDY1jZpDySa9G+O+lSE2WohRsGY2IrznwwD8o96mextT3PRYpAEFSh9x9apRMSgqYbu2a5rHUmWVQM3FWYocmqsThRzwatRzYwaQ00XVsiygjmmtbmMnjinRXmFxUvnRuOvPvVoGMUD8B60/Kqw3Y596idxnAPH1qMA9ecUxGgIkfqcegpsloAucVHFKqpknkU06nE643/MOopMaRn3MIy2BWTPEVOTWtc3atkg1lTTbj61mURl8IBivO/HdgvyXSjnOCa7525xmuY8WRiTSZQRnAyKcNJEy1R5ghw4Poa9T+H13OLSd0dikO1jntzXlbcNxXqnwwgmvbK7tYM+Y7rz2x711HIz3DwRqE1y97E4zCrr5Z/CuxzWJ4c0mPS9OjUcyHlm9TWzmkR1H0UUlWQcF8XIo28B3kkoBKkFTjoc14x4RtR9i84jr0r3L4n2rXngDU0wPkQOD9DXjvhWLZoUBxyeaiexrS3NyBSzgYqxJPDDhGPzVmahqqaXbliNz9hXnuo+Ir6a4dvnGTxgVnGNzZyseotcxZ+8DTRegHANePHXr4NzI4/Gr1p4nukAyxbHXNaqmiPaM9biuicjPFTifaM5ry+38ZTB8FAR7Gujh8RIbdJJWCg9KfIilUZ1v2vtmkN2yj71Y0V8syBlPB5qO5v1iHJqeVFc7Nlrwj+Ks+SQRu0gmyzH7vpXOXOv5LIhwQOtYNx4gnt15YFicn6Uci6C9od498vQmpEaNvm3j868xm8SyGPPOapnxLeqcrIePelKkCqnrcoTbkEE1zviBC+mzjHRc1y2n+LLlbhDMdyniuxu3W70aaVPmVoif0rJwsy+e6PNbPTm1J4rOygknvZXwEVTwK+mfhx4AHhfRl+2YN3L80gHb2qP4T+FtO0vwza6gtqhvblA7zMMtj0B7CvRK3ZytjhgAAdKUGm0UhE9Iaj80UnmVZmYHjx1TwPrDP0+ztXjWix+VotqMY/diur+J/i92srzRYEUIzCKRj1JyKw7eMR2sceOFUAVEmdFOLWrMe/tvtE2XGQPWqUlrZQrh1WtbUAUUkZ/CuTktprm+AuHeOAnBweSP6Uky7XJ55tGTKyLH+VZlzDpcqkxQ8eqmrHi3Slt/s0ltB/ou0Alf15qDwvpkd1qcrGNktNpB3tnb6c+tbJdDK9tWVYdLtXb90zA9cNWklnvVYycbelPuoLexvD5cokUHHHXFTwnzLgbah3RorM3dF0+e4KxqeB1NT+ItAurKIS5LRkc8V0/hm2jjhQsBuPJrotctY7uyKMVIZcVi5s2VNHz7MuGOevbHWsy6tJJW+ZgM1t6pF9i1eaCTgIT+VTafp8WpTBvMVI+4z8xrWN5bGMrR3MC30m14NxK4H04q+um6WRhcH6msi+XF7dRSzSx+WSI1Aznn9K0dJ0o3OkyzyM8bqfkbsatrzM0/Ikk0a3HzR10Wh5axlszzlSBXMWNxN5hhl7V12hfLcIxGAOTWUjVLsfQmgWq2WhWVsnAjiVR+VaeazdFuo7zR7W4icMrxg5FX81Rzj80UzNLmiwzxaT413LnZDp8W71yajPxj1EctbW49ua8ia6EY2x4HvUJmLHrn3pFcqO+1jWJPF863ohjSVJk80IOo9a6SM5Tg1xfhEFbO7fsSBmuwtXDgc9RWcjZbIiuMHqOKzbm2WQZAzW3LECKpSW5U5B4oKMCWKT/VlmMZzlT0qJIQFKLlV744rbkj56VXMBYkBcVpGVhON9THMSocRRDceM4rX0fSt8wZhyetSQ2YDZxk+tb1giQgZ6monO5UI2NKyj8plAzXQzjfYknrjisi2UbgcV0LRxHTC3esr3N7WPFvGWmOt19sVc5ODWDZlWIcDDD04r1HW7WKeF42xzXnUli1tcOFHQ9K1pzMakLlS6t45pt7JlvU96dc3Fw8CQKVSMD7qirgVWOGGDUgtQx6CrcjNQRnWtoMqx+8K6zTbfyoPMwORVOy0wyyrkcCt26RLW0CrwKycirHpHw6OPCMC4I2yOOT15rrM1yPg7XdIn0uCytpRFLGuDG/BJ7ketdVu461qjlktSTNJuFVbq9gtIGmuJVjjUZLMcAVw958WtAtbp4VMsoU43qOD9KASufNIYd24+lPGNwwwqAMVWpg4dMkDimWdr4XkP9l3IX/noM/SuotJcMBXLeDbNotLu7knCSMFC/St6Bv3g4rKbNo7WNzIPfmo2UsaZCenNWo13daBoqNb55qN7cgZVc81riMYBprRgDpRuXsZTr5aZ6VowW6JGrs3OM1R1BsIMDgGmy3nl2xkkfCKuTQ466BF9Tobe4QEDcKvm/2w+WT8v1rz3SvEVrqEzpA0gKn+JSM1rzXrLCSX4AoUA5y9qDxSnBcAk+tc1Pbhrx0PXrVWLUVvrl/Km3GM4Iq9C5mvQxHQYNK1mVdMBpAkGcULphibpW/CQgxiiQDBPrSegWuZ8BEABxVDVrwNEwB7Vdu3CKcDgVzGoXBLMo7il1JkZseoXVo3nROeDng8ivUPAnxJF262GpzZyPklY8j2NeRoSQcEHnkVlOJbfU08osiMcg9Metao5mjvviT44utU1CS1hk22MTEJsP3vc15z5zP8xfOe9X7iGOaTdKTk9s8mqEzJBIY0XCiqQWIZRyAOwpsL5bB6VIwJBPc1WjJGMdzTEd94b1m2j0w6c7hZi2UB/irdTKyCvMLafy9Ut36BXXNenR/OBWc0aRdzWgbOMYrQi65JrKtjtIB5rRR8gDsaRdyyXx3pNxcY71WY80faVgGepqloO/cW+tx9jZmrmrqSVojEuGX3rY1DUfNtyozz6Guemlb+HqO1CTE5okt0UKBtVSO6ijULiQRlN2RioGcx2+edxOailZ3RsLuwPzq0jPnZVsrloZSURAT3rsbC2DWiy9WYZNcIuVlKjhq6rTdUaO3VG/hHSokn0NITXU2WlaIYzihLrzFOOaqSXInTfio1lVUAXj1qHqaJjr6TCEn0rlLuTfK56AVu310oQ5PQVy11IFhdifvZ4pIlvQqIfmJXmllHnRFH/A9warROeOOanE6dCefetDAy0kZJmSTqpxTmCsckZpt2Va5DKO3NMyfWqERseM9qpg4K1ZlOAT7VUbqPWmIk3kuT716dpk32iyhl/vKDXlucCu18IXxlsnt2bmM8fSpmroqLO1hY7d3pV2CTPFZkR56/Srcb7TUostStgE9hWbNOQ24tgGtCQgxHPGRWLPbGYnJOP6VSsKV2NkvLQEhplx3GaqyaraBsIoJ9TSSaHFuDIOfemCw8s/PEMeuKq6HGPcBqiOdrIjD0FMlv1jz5cSj0yabPaRBgY1we9RSWUTKCev1p2RfKMN7bmUGVFz6irAngeXdFKMY4FZraWzsdqkD1NTQ6SsI3Aktmk7IzaNaK5ZXxn5aus3yZrPt4eRnselWbiUqm2s5a7FRdtzOvpd77R0rC1CYAbK1LqTYWY1zt3Lum5ORSSE3oOEm1Caz7y83DC8MDwauow4BGRUwit35MSk/SrMzAEkkj5Lc19P8A7POf+Ff32ev9qSf+ioq+adQtBC/mRfcPb0r6T/Z1JPw9vs/9BST/ANFRVQHzRMcqAOtQN606UkSEVETQIDWjod+bDUkfOEb5W+lZtFAz163mDYIOc1pRMcVxXhXUftVn5EhPmRHAPqK6tJSoAqLWZaZffBXrzUXC9ajWU565NOxvwQc5pMpA7KykZrPuRIoJEpA9Kvyx+WOcVQmXevXNCkNoxZmiV/3k0hP1NJCAzYimcD0Jq7JbKSOB+NQJbnzcAY+lXfQjlLiRhQCzE1INp6VestPWRMsTj3p8+mKqlkPSsnI0UTN4XBBqC4k96dOwjbbmqtxINmR3poTMnUpQQTnArnvMYyEsDgnirmsXWJPKVvc1RikU8fzrRbGTZficY9RTnlIX0qBW4HFTJGrEMzfhRYRGFaaNgxHlnsa+h/2dl2eANQX01WT/ANFRV8/yyKFxjj0FfQP7PJB8B6iR/wBBaT/0VFQhnkEngXTkc77mckjPak/4QXTSq4uJMsM9a6W8Ny24JGXU/IDj8P8AGmJp808q7/3YAwAD1rr9nFHD7WbOSk8F2QO2OadjuxkAGtKy8C6dbfNcM80jcorcAfhXT+Vb2ULrGcKB8y9T9aj8xjENoBLABWPXmkqa3G6strmP9ktoSYrWJIyo5EagDNSxSZXJPSqNxqyW+vxaaqjklXJ6lsZq7dQtEfNTlD94DtXPPSR1U/huSbzwc1JFc7DjNVw4kjBToKikVl5Gce1RYtM0ZZjIhxUMTKRtzk1Ta8cIFI57mmLc/KTVcoc5ekVAcE9ajWNEJPOaqG7XucGmPfKYjhuRSauCkan9pmKLYtQzawzKeccVh+a07nBPSnA5Uljz70uRdR+0fQGmMrnB681FNKTFyeajZwhyDnNQXRMdszOSuR27CnbsK5nHRTeSb47pN7no4obwzqsG5vsxkUdWjO4fpV62i3rHJu78SJ/UV1el/wBoQojp+/iLZJArbkRz+0adjgDDLbHEiMjD+FlIpyTBiRivaVnstShki1GwSQYCjenSsbUfhpp94S+jXRglxkxycrUOm7FRqxeh5gxzx3r6J/Z7/wCRD1H/ALCsn/oqKvG7/wAD63p0TyNaGWNerx817P8AACKSHwLfrIjIx1SQ4YYP+qiqTW6ZiX0TJZvIkZVccIfWs1sqVRlbABI/Sug1e3uJbOQxAszNwP8AP0rnxJcfaDG+NwU8fjXYprY89xe5UlijdQdhyzj8t1UJJSNftIkx5fcA8HitZiyQ5crnbxXNwmRtWhcDkSbhmqepOi2Ry/iHNn4480njeHBrt7edJ4gQQQRyKxfFXh2/1do7u0tcyocbQRkisy1uL3T/AJLiKSOReCrCuGtFp3PQoTUo2udDcWkluTJBkqeq1XF1yA2Qfep7DWIroBWIDehq1PYxXC714PqKhS7mko9ikHRuoBBqF7dGVuCOeg7059PdOqtj1WomtpM4WUj61oRYT7BbsPm3E/WmfZ4ImJ2jPuaY0Nz3k+nFVZYZT9+Ymi4rFmW6hj6FQOlUjcmZ9kSlmP5U6PTGkbJBC+prTit47eP5QM9zUuVikrlSK28pd8nzP/Kqd2wJ56elXZ5dxIU4A6msqeQM5x0HSovc0tYTTpzYahGhIMEjYO4ce4r03QNNW38yFpWMDjdG45xXl8sbywLsUs4cYFew6FFMNIto7jbFPKuxVI74reEm0c1WKTNy3tIRGrZL8/MrCnyWwjO9UxvIGV7CqMZntVBbO4cP7VrWsoZCp5UjILGtbWML3JPMBcICGjQdDXa+GoUj0WN0Tb5rM7D3zj+QFcKI2ydih8H5l6fjXfeHmL6HbMylT8wwe3zGs57GlLc4W+k8qLYgBlb7pFcnfWL/AGrgbnf73tXW+R5eBJ80rcqfSnGwUyKSiu3VmrYxucFrdu1nBHGRw/APpXPTowlEivtaNeDXdeJovtTGVIxsi4Nci9uTaMByznoe1awvYymtTL1DxTqdvo6TW+PMV8OcZAq/4e1mLxNaPDcW8QuE+9x196q2lmssNzbyICjH5s1Ho2gR6JqMl4Z38lhgDp+dN3vpsKLXK77hrnhU2p+1aZIcjlkzyKy7DxFNaHy7pCQOM130c8dwMqAEA+8D94VW/svTrmV5mto9p65HWsZ4ZN3idFPFtK09TMtNYtbtf3bjPpU8zQuvzRhvcVh6t4a8h3u9LVtq8mIHkD29ao2mq5+R5nRhwQa5pRlB2Z1wnGavE25vJ5wsg/GqTyQxndgA+rGq806Ov/H22PpWZNNAGwGdz71BRqNqCZwoLfyqN7sMPnbj0FZ0UV5cH9xbOR64p39k6pLki1lOOvFFmUmhLq73cKcCo7O1uL+cRQRs7E9hXT6F4LEsIvNWk8qIf8ss/Ma6H+0NO0pNmnQJEAMFiMmtIUXIxqYiMdh3h/w5b6QiT3jxmcjID4wtWU1OPUvEsUFoxaO2HLjjLVyer667rneXdjxk8V0PgWxMUH2uRfnlOa6FBROV1HM9BubZZYw56SL8w9TWbFvhGCpJjOMe1bqj/Rcjjae3PFUpYcXIHRXGMmkAnnBHSTJYNxx+ld9oRzo1uQMfe/8AQjXnqxkRSQdAvc+hrvfDLbvD1qf98f8Aj5rOa0NqW5y/k+Uo3sG3dTiq0zFIWmiJ3HoD39KKK1vpc57a2KN5aCaBIkb55PvZrldV0qSCRm2rtXuD3ooo5mNxTOb8sq5br5h47Vo71liaKUfJtziiiqUmQ4oz7lHslEtmf3J5MbGrNhqqXh+QbU6OuKKK0UmRKKaNGdVjT/Rs46kHjH0rl/EGgJd273lvtS6HzYHAcf0NFFE0pQdyaUnCouUwNH0m+1IA7kSLOCxOTXW2nh20050dx57nuw4oorOlSjZM6MRWndpMu3brbEPH0/ugYFMn8QXNhAFi2M7dytFFbuK5TljJ825zl/4huJmxubzD+VRrJcLbl5X3Z/hNFFZdDfqRWGny6jqCJwxzxk9K9j0vT/s1lawpjC4zRRWUnoi12OktxktFuDblz0xUMiloQ6rlk5yT6UUUhoiuNu+OVvmz8pA967Xw+oXQ7cKMD5uP+BGiis57GtL4j//Z";
            //System.out.println("Uploaded Image " + cmnutil.stringtoByte(byteimage));
            
            // create instance of middleware
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            
            //convert facial image into byte[]
            //byte[] decodedBytes = Base64.decodeBase64(facialImage.getFacialImage());
            byte[] decodedBytes  = cmnutil.hex1StringToByteArray(facialImage.getFacialImage()); 
            facialImage.setFacialData(decodedBytes);
            
            
            //returnStatus = mws.writeFacialImage(channel, decodedBytes, piv_admin_key);
            returnStatus = getmServer().writeFacialImage(channel,facialImage, adminKey,algoType);
            
            
            //set return Value
            cardResponse.setCardreaderResponse(returnStatus);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Write Facial Image : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Write Facial Image (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;
    }

    public CardreaderResponseBean loadCertificate(String deviceName, String adminKey, byte[] certificate, String certificateType) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        CardChannel channel = null;
        boolean returnStatus = false;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().loadCertificate(channel, adminKey, certificate, certificateType);

            //set return status
            cardResponse.setCardreaderResponse(certificate);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            // stop communication & return success
            cardConnect.stopComm();
        }

        return cardResponse;

    }
    
    public CardreaderResponseBean loadUserCertificate(String deviceName, String adminKey,LoadUserCertificates userCertificate) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        CardChannel channel = null;
        boolean returnStatus = false;
        List errorList = new ArrayList();
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            errorList = getmServer().loadUserCertificate(channel, adminKey, userCertificate);

            //set return status
            cardResponse.setCardreaderResponse(errorList);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            // stop communication & return success
            cardConnect.stopComm();
        }

        return cardResponse;

    }
    
    /********************************************************************************************************/
    
    /**************************** Finger Print Function *****************************************************/    
    public CardreaderResponseBean enrollUserFingerPrints(String deviceName, UserFingerPintBean userFingerPrint, String adminKey) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        CardChannel channel = null;
        boolean returnStatus = false;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().enrollUserFingerPrints(channel, userFingerPrint, adminKey);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            // stop communication & return success
            cardConnect.stopComm();
        }

        return cardResponse;

    }
    
    public CardreaderResponseBean enrollFingerPrint(String deviceName, byte[] fingerPrint, String fingerID, String adminKey) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        CardChannel channel = null;
        boolean returnStatus = false;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().enrollFingerPrint(channel, fingerPrint, fingerID, adminKey);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            // stop communication & return success
            cardConnect.stopComm();
        }

        return cardResponse;

    }

    public CardreaderResponseBean verifyFingerPrint(String deviceName,String fingerPrint) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        CardChannel channel = null;
        boolean returnStatus = false;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = getmServer().verifyFingerPrint(channel, fingerPrint);

            //set return status
            cardResponse.setCardreaderResponse(returnStatus);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            // stop communication & return success
            cardConnect.stopComm();
        }

        return cardResponse;

    }
    
    /********************************************************************************************************/
    
    
    /*************************** Lock Card functions *********************************************************/
    public CardreaderResponseBean cardLock(String deviceName,CredentialKeys credentialKeys) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean cardLockStatus=false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Call Card Lock Function
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            cardLockStatus = getmServer().cardLock(channel, credentialKeys);

            //set return Value
            cardResponse.setCardreaderResponse(cardLockStatus);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Card Lock : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Card Lock (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean cardUnlock(String deviceName, CredentialKeys credentialKeys) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean cardLockStatus=false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Unlock Card
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            cardLockStatus = getmServer().cardUnlock(channel, credentialKeys);



            //set return Value
            cardResponse.setCardreaderResponse(cardLockStatus);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Unlock Card : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Unlock Card (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }

    public CardreaderResponseBean isCardLocked(String deviceName) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean cardLockStatus=false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if Card is Locked
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            cardLockStatus = getmServer().isCardLocked(channel);

            //set return Value
            cardResponse.setCardreaderResponse(cardLockStatus);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Unlock Card : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Unlock Card (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }
    
    /********************************************************************************************************/
       
    
    /**************************** Misc Function *****************************************************/    
    public CardreaderResponseBean readContainerInfo(String deviceName, String adminKey) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        ContainerInfoBean containerInfo = new ContainerInfoBean();
        CardChannel channel = null;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }
            
            // read container information
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            containerInfo = getmServer().readContainerInfo(channel, adminKey);

            //set return status
            cardResponse.setCardreaderResponse(containerInfo);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Read Container Info : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Read Container Info (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;

    }
    
    public CardreaderResponseBean loadSecurityObject(String deviceName, ContentCertificateBean contentCertificate,Date signatureDate, String adminKey, String algoType) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        CardChannel channel = null;
        boolean retValue;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }
            
            // read container information
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            retValue = getmServer().loadSecurityObject(channel, contentCertificate,signatureDate,adminKey,algoType);

            //set return status
            cardResponse.setCardreaderResponse(retValue);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Load Security Container : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Load Security Container (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;

    }
        
    public CardreaderResponseBean masterKeyCeremony(String deviceName,  MasterKeyCeremonyBean masterKeyBean) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
         MasterKeyCeremonyResultBean masterKeyResult = new MasterKeyCeremonyResultBean();
        CardChannel channel = null;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }
            
            // read container information
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            masterKeyResult = getmServer().masterKeyCeremony(channel, masterKeyBean);

            //set return status
            cardResponse.setCardreaderResponse(masterKeyResult);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Master Key Ceremoney : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Master Key Ceremoney (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;

    }
     
    public CardreaderResponseBean verifySecureChannel(String deviceName, String secureChannel) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        boolean cardLockStatus=false;
        CardChannel channel = null;
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if Card is Locked
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            cardLockStatus = getmServer().verifySecureChannel(channel, secureChannel);

            
            //set return Value
            cardResponse.setCardreaderResponse(cardLockStatus);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Verify Secure Channel : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Verify Secure Channel (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }
    
    public CardreaderResponseBean verifyCredentialKeys(String deviceName, CredentialKeys credentialKeys, InitializationBean initvalue,String secureChannel) {
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();        
        CardChannel channel = null;
        boolean returnValue;
        
        try {

            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            // Check if Card is Locked
            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnValue = getmServer().verifyCredentialKeys(channel, credentialKeys,initvalue,secureChannel);

            //set return Value
            cardResponse.setCardreaderResponse(returnValue);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println("Verify Credential Keys : " + cardResponse.getCardreaderResponse().toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println("Verify Credential Keys (Error) : " + cardResponse.getCardreaderError().toString());
            }

            // stop communication & return success
            if (channel != null) {
                cardConnect.stopComm();
            }
        }

        return cardResponse;

    }
    
    public CardreaderResponseBean readPinLength(String deviceName, String pinType) {
        CardChannel channel;
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        channel = null;
        int pinLength = 0;
        channel = startCardCommunication(deviceName);
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            pinLength = getmServer().readPinLength(channel, pinType).intValue();
            cardResponse.setCardreaderResponse(Integer.valueOf(pinLength));

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println((new StringBuilder("Read PIN Length : ")).append(cardResponse.getCardreaderResponse().toString()).toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println((new StringBuilder("Read PIN Length (Error) : ")).append(cardResponse.getCardreaderError().toString()).toString());
            }
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;
    }
    
    public CardreaderResponseBean readPinPolicy(String deviceName) {
        CardChannel channel;
        cardError = new CardreaderError();
        cardResponse = new CardreaderResponseBean();
        channel = null;
        PinPolicyBean pinPolicy = new PinPolicyBean();
        channel = startCardCommunication(deviceName);
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardreaderError(cardError);
                return cardResponse;
            }

            //MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            pinPolicy = getmServer().readPinPolicy(channel);
            cardResponse.setCardreaderResponse(pinPolicy);

        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardreaderError(cardError);
        } finally {
            if (cardResponse.getCardreaderResponse() != null) {
                System.out.println((new StringBuilder("Read PIN Policy : ")).append(cardResponse.getCardreaderResponse().toString()).toString());
            }
            if (cardResponse.getCardreaderError() != null) {
                System.out.println((new StringBuilder("Read PIN Policy (Error) : ")).append(cardResponse.getCardreaderError().toString()).toString());
            }
            if (channel != null) {
                cardConnect.stopComm();
            }
        }
        return cardResponse;
    }

    /********************************************************************************************************/
    
       
    public CardChannel startCardCommunication(String deviceName) {
        CardChannel retValue = null;
        final String dvcName = deviceName.trim();
        CardChannel cardChannel = null;
        // ---------------------------------------------------------- //
        // Start communication with card
        // ---------------------------------------------------------- //

        try {
            retValue = (CardChannel) AccessController.doPrivileged(new PrivilegedAction() {

                public Object run() {
                    CardChannel cardChannel;
                    try {
                        cardConnect = new CardComm();
                        //CardApplet.bouncyCastleProvider = new BouncyCastleProvider();

                        cardChannel = cardConnect.startComm(dvcName);

                    } catch (Exception e) {
                        cardChannel = null;
                    }
                    return cardChannel; // nothing to return
                }
            });

        } catch (Exception e) {
            //retValue = "Unable to load jnisgfplib";
        }

        return retValue; // nothing to return
    }
    
    @Override
    public void destroy() {


        System.out.println("applet:destroyStart");

        AccessController.doPrivileged(new PrivilegedAction() {

            @Override
            public Void run() {
                try {
                    removeAll();
                    System.gc();

                    RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();

                    //
                    // Get name representing the running Java virtual machine.
                    // It returns something like 6460@AURORA. Where the value
                    // before the @ symbol is the PID.
                    //
                    String jvmName = bean.getName();
                    System.out.println("Name = " + jvmName);

                    //
                    // Extract the PID by splitting the string returned by the
                    // bean.getName() method.
                    //
                    long pid = Long.valueOf(jvmName.split("@")[0]);
                    System.out.println("PID  = " + pid);

                    String command = "taskkill /F /PID " + pid;

                    System.out.println("applet:destroyRemoved");
                    Runtime runtime = Runtime.getRuntime();
                    //runtime.exec ("taskkill /f /im java.exe").waitFor ();
                    runtime.exec(command).waitFor();


                } catch (InterruptedException intex) {
                    Logger.getLogger(CardApplet.class.getName()).log(Level.SEVERE, null, intex);
                } catch (IOException ex) {
                    Logger.getLogger(CardApplet.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        });
    }

    public JSONObject convertToJSON(Object obj) {
        JSONObject jsonObj = null;
        jsonObj = new JSONObject(obj);
        return jsonObj;
    }
   
}
