
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

/**
 *
 * @author Harpreet Singh
 */
public class FacialBean {
    
   private byte[] facial_data;
   private boolean signatureVerified;
   private String facialImage;
    public byte[] getFacial_data() {
        return facial_data;
    }

    public void setFacial_data(byte[] facial_data) {
        this.facial_data = facial_data;
    }

    //public boolean isLb_verify() {
    //    return lb_verify;
   // }

    //public void setLb_verify(boolean lb_verify) {
    //    this.lb_verify = lb_verify;
    //}

    public String getFacialImage() {
        return facialImage;
    }

    public void setFacialImage(String facialImage) {
        this.facialImage = facialImage;
    }

    public boolean isSignatureVerified() {
        return signatureVerified;
    }

    public void setSignatureVerified(boolean signatureVerified) {
        this.signatureVerified = signatureVerified;
    }

    
    
   
    
}
