// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena;

import java.io.File;

public interface TransferDoneListener
{

    public abstract void transferDone(File file);

    public abstract void transferFailed(int i, String s);
}
