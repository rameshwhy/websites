/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader;

import java.util.List;

/**
 *
 * @author admin
 */
public class CardReaderError {

    private String errorCode;
    private String errorMsg;
    private List errorList;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List getErrorList() {
        return errorList;
    }

    public void setErrorList(List errorList) {
        this.errorList = errorList;
    }

    @Override
    public String toString() {
        return "CardreaderError [errorCode=" + errorCode + ", errorMsg="
                + errorMsg + ", errorList=" + errorList + "]";
    }
}
