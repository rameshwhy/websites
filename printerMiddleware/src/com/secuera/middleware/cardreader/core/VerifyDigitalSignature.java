package com.secuera.middleware.cardreader.core;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;




public class VerifyDigitalSignature {

    public VerifyDigitalSignature() {
        super();
    }
    
    public boolean VerifySignature(byte[] input_data, byte[] signature, PublicKey pubkey, String SHASignatureAlgo) {
        boolean lb_verify = false;
        String algo="";

        
        switch (SHASignatureAlgo) {
            case "2a864886f70d01010d":
                algo = "SHA512withRSA";
                break;
            case "2a864886f70d010101":
                algo = "SHA256withRSA";
                break;
        }

        Signature sig = null;


        try {
           
                sig = Signature.getInstance(algo);
            
           
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (Exception t){
            t.printStackTrace();
        }


//String sat="382010f003082010a028201010097b6b124df0b637f1de58fa6e3ed1f9a40cc0108e87b63226da09db47c5fd1fea1c06ad8851a7609c35a6ed0a78e6d1843f0d0d9f2e005bd5c659c24ce79be32489cf5c83090d486350fe179842ee1af28eb0b968f68665085a75514a1c82302dc85e50d8b857ef1ecc760b1e2faac23c814f17ef408683b24d45791b442fce5b90dfb2277f4230499b459f806b8abe012736bc7dde4f328880a1252df6ee3581b72a8756cb1dbead8917f9cbdb1abd14cf524bd5e7932d89d9863137c21c933b7492130fe6052a7b5ea79b314986aa549472e1fbe4766eba67ca34d942c4716805301e7858a468c6eb7b0b050e8cb82691e6e3ed37016935b55239a23ed69770203010001";
        
        
        try {
            sig.initVerify(pubkey);
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (Exception t){
            t.printStackTrace();
        }

        
        
        
        
        try {
            sig.update(input_data);
        } catch (SignatureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (Exception t){
            t.printStackTrace();
        }
         
        
        
      
        try {
            
            lb_verify = sig.verify(signature);
        } catch (SignatureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception t){
            t.printStackTrace();
        }
   
        
        
        
        return lb_verify;

    }
    
    
    /*
    public boolean VerifySignature(byte[] input_data, byte[] signature, PublicKey pubkey, String SHASignatureAlgo) {
        boolean lb_verify = false;
        String algo="";

        
        switch (SHASignatureAlgo) {
            case "2a864886f70d01010d":
                algo = "SHA512withRSA";
                break;
            case "2a864886f70d010101":
                algo = "SHA256withRSA";
                break;
        }

        Signature sig = null;


        try {
            sig = Signature.getInstance(algo);
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        try {
            sig.initVerify(pubkey);
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        
        
        
        
        try {
            sig.update(input_data);
        } catch (SignatureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         
        
        
      
        try {
            
            lb_verify = sig.verify(signature);
        } catch (SignatureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
   
        
        
        
        return lb_verify;

    }*/
}
