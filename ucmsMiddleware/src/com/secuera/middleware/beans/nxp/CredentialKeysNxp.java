/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.nxp;

/**
 *
 * @author Harpreet Singh
 */
public class CredentialKeysNxp {

    private byte[] encKey;
    private byte[] macKey;
    
    private byte[] cardManagerAid;
    
    private String str_appletType;

    public String getStr_appletType() {
        return str_appletType;
    }

    public void setStr_appletType(String str_appletType) {
        this.str_appletType = str_appletType;
    }

    
    
    public byte[] getEncKey() {
        return encKey;
    }

    public void setEncKey(byte[] encKey) {
        this.encKey = encKey;
    }

    public byte[] getMacKey() {
        return macKey;
    }

    public void setMacKey(byte[] macKey) {
        this.macKey = macKey;
    }

    public byte[] getCardManagerAid() {
        return cardManagerAid;
    }

    public void setCardManagerAid(byte[] cardManagerAid) {
        this.cardManagerAid = cardManagerAid;
    }
    
   
    
    
    
    
}
