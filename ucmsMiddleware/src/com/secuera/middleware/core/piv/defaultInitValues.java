/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import java.util.Date;
import com.secuera.middleware.beans.piv.ChuidBean;
import com.secuera.middleware.beans.piv.FascnBean;
import com.secuera.middleware.beans.piv.PrintedInfoBean;
/**
 *
 * @author admin
 */
public class defaultInitValues {

	private ChuidBean chd;
	private String localPin;
	private String globalPin;
	private PrintedInfoBean printInfo;
	private byte[] byteimage;
	
	
	public defaultInitValues(){
		//set Local/Global PINs
		localPin = "123456";
		globalPin = "12345678";
		
		//set CHUID & FASCN
		ChuidBean chuid = new ChuidBean();
		FascnBean fascn = new FascnBean();
		Date expDate = new Date();
		
		fascn.setAgencyCode("9999");
		fascn.setSystemCode("9999");
		fascn.setCredentialNumber("999999");
		fascn.setCs("1");
		fascn.setIci("1");
		fascn.setPerson("1112223333112345");
				
		//chuid.setFascn(fascn);
		chuid.setOrganizationID("123");
		chuid.setDuns("122");
		chuid.setExpirationDate("");
	}


	public ChuidBean getChd() {
		return chd;
	}


	public void setChd(ChuidBean chd) {
		this.chd = chd;
	}


	public String getLocalPin() {
		return localPin;
	}


	public void setLocalPin(String localPin) {
		this.localPin = localPin;
	}


	public String getGlobalPin() {
		return globalPin;
	}


	public void setGlobalPin(String globalPin) {
		this.globalPin = globalPin;
	}


	public PrintedInfoBean getPrintInfo() {
		return printInfo;
	}


	public void setPrintInfo(PrintedInfoBean printInfo) {
		this.printInfo = printInfo;
	}


	public byte[] getByteimage() {
		return byteimage;
	}


	public void setByteimage(byte[] byteimage) {
		this.byteimage = byteimage;
	}
	
	
	
}

