/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.beans.piv.PrintedInfoBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author SEAdmin
 */
public class ManagePrintedInfo {
    
    public byte[] printedInfoDataSecu;
    public ManagePrintedInfo() {
        super();
    }
    
    SendCommand sendCard = new SendCommand();
    CommonUtil util = new CommonUtil();
    GeneralAuth auth = new GeneralAuth();
    ExceptionMessages expMsg = new ExceptionMessages();
    
   
    
    
    public PrintedInfoBean readPrintedinfo(CardChannel channel) throws MiddleWareException {
        
        String strPrintedInfo;
        int tag_loc;
        PrintedInfoBean printedInfo;

        try {

            strPrintedInfo = getPrintedinfo(channel);
            
            
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (strPrintedInfo == null || strPrintedInfo.length()== 0) {
            printedInfo = new PrintedInfoBean();
        } else {
            

            printedInfo = new PrintedInfoBean();
            int i =0;
            String str_name ="";
            String str_affiliation ="";
            String str_expDate="";
            String str_CardSerialNumber ="";
            String str_IssuerID ="";
            String str_org1 ="";
            String str_org2 ="";
            // Get Employee Name
            tag_loc = util.findtagLocation(0, strPrintedInfo, UcmsMiddlewareConstants.NAMETAG);
            if (tag_loc>=0){
                String name_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
                i = Integer.valueOf(name_length, 16).intValue();
                str_name = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));
                strPrintedInfo=strPrintedInfo.substring(tag_loc + 4 + i * 2);
            }
            
            // Get Employee Affiliation
            tag_loc = util.findtagLocation(0, strPrintedInfo, UcmsMiddlewareConstants.EMPLOYEETAG);
            if (tag_loc>=0){
                String employee_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
                i = Integer.valueOf(employee_length, 16).intValue();
                str_affiliation = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));
                strPrintedInfo=strPrintedInfo.substring(tag_loc + 4 + i * 2);
            }
            
            // Get Expiration Date
            tag_loc = util.findtagLocation(0, strPrintedInfo, UcmsMiddlewareConstants.EXPDTTAG);
            if (tag_loc>=0){                
                String expdt_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
                i = Integer.valueOf(expdt_length, 16).intValue();
                str_expDate = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));
                strPrintedInfo=strPrintedInfo.substring(tag_loc + 4 + i * 2);
            }
            
            // Get Agency Card Serial Number
            tag_loc = util.findtagLocation(0, strPrintedInfo, UcmsMiddlewareConstants.AGENCYCARDTAG);
            if (tag_loc>=0){
                String agencyCard_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
                i = Integer.valueOf(agencyCard_length, 16).intValue();
                str_CardSerialNumber = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));
                strPrintedInfo=strPrintedInfo.substring(tag_loc + 4 + i * 2);
            }
            
            // Get Issuer info
            tag_loc = util.findtagLocation(0, strPrintedInfo, UcmsMiddlewareConstants.ISSUERTAG);
            if (tag_loc>=0){
                String issuer_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
                i = Integer.valueOf(issuer_length, 16).intValue();
                str_IssuerID = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));
                strPrintedInfo=strPrintedInfo.substring(tag_loc + 4 + i * 2);
            }
            
            // Get Organization1
            tag_loc = util.findtagLocation(0, strPrintedInfo, UcmsMiddlewareConstants.ORG1TAG);
            if (tag_loc>=0){
                String org1_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
                i = Integer.valueOf(org1_length, 16).intValue();
                str_org1 = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));
                strPrintedInfo=strPrintedInfo.substring(tag_loc + 4 + i * 2);
            }
            
            // Get Organization2
            tag_loc = util.findtagLocation(0, strPrintedInfo, UcmsMiddlewareConstants.ORG2TAG);
            if (tag_loc>=0){
                String org2_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
                i = Integer.valueOf(org2_length, 16).intValue();
                str_org2 = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));
                strPrintedInfo=strPrintedInfo.substring(tag_loc + 4 + i * 2);
            }    
            printedInfo.setName(str_name);
            printedInfo.setAffiliation(str_affiliation);
            printedInfo.setExpirationDate(str_expDate);
            printedInfo.setCardSerialNumber(str_CardSerialNumber);
            printedInfo.setIssuerID(str_IssuerID);
            printedInfo.setOrganisationLine1(str_org1);
            printedInfo.setOrganisationLine2(str_org2);
        }
        return printedInfo;

    }

    
     public String getPrintedinfo(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        StringBuffer sb = new StringBuffer();
        String strPrintedInfo;

        byte[] apdu_printed = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05,
            0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x09}; // BERT-TLV

        CommandAPDU READ_APDU = new CommandAPDU(apdu_printed);

        System.out.println("Final Command " + util.arrayToHex(apdu_printed));

        System.out.println("---------");

        try {

            res = sendCard.sendCommand(channel, READ_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        
         if (res == null || res.length == 0) {
             strPrintedInfo = "";

         } else {
             //convert byte array to string & remove parity bits
             strPrintedInfo = util.arrayToHex(res);

             //remove leading characters
             if (strPrintedInfo.substring(2, 4).equals("82")) {
                 strPrintedInfo = strPrintedInfo.substring(8);
             } else if (strPrintedInfo.substring(2, 4).equals("81")) {
                 strPrintedInfo = strPrintedInfo.substring(6);
             } else {
                 strPrintedInfo = strPrintedInfo.substring(4);
             }
         }
        return strPrintedInfo;

    }
    
    
    public boolean writePrintedinfo(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY, String str_name,
            String str_emp, String str_date, String str_ACSN, String str_Iden,
            String str_OA1, String str_OA2) throws MiddleWareException {

        // TO write Printed Information Container.
        // Name 0x01 32 bytes
        // Employee Affiliation 0x02 20 bytes
        // Expiration date 0x04 9 bytes
        // Agency Card Serial Number 0X05 10 bytes
        // Issuer Identification 0X06 15 bytes
        // Organization Affiliation Line 1 0x07 20 bytes
        // Organization Affiliation Line 2 0x08 20 bytes
        // Error Detection Code 0xFE 0 bytes

        byte[] newData;
        byte[] command_data;
        String hex_name ;
        String hex_emp;
        String hex_date;
        String hex_ACSN;
        String hex_Iden;
        String hex_OA1;
        String hex_OA2;
        StringBuffer sb = new StringBuffer();
        StringBuilder hex_data = new StringBuilder();
        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        
          
    
        if (!isEmpty(str_name))
        {
            String hex_name_len;
            hex_name = util.stringToHex(str_name);
            hex_name_len=util.intToHex(str_name.length());
            hex_data.append(UcmsMiddlewareConstants.NAMETAG).append(hex_name_len).append( hex_name) ;
           
        }
        
        if (!isEmpty(str_emp))
        {
            String hex_emp_len;
            hex_emp = util.stringToHex(str_emp);
            hex_emp_len=util.intToHex(str_emp.length());
            hex_data.append(UcmsMiddlewareConstants.EMPLOYEETAG).append(hex_emp_len).append(hex_emp) ;
         }
        
        if (!isEmpty(str_date))
        {
            String hex_date_len;
            hex_date = util.stringToHex(str_date);
            hex_date_len=util.intToHex(str_date.length());
            hex_data.append(UcmsMiddlewareConstants.EXPDTTAG).append(hex_date_len).append(hex_date) ;
        }
    
         
    
        if (!isEmpty(str_ACSN))
        {
            String hex_ACSN_len;
            hex_ACSN = util.stringToHex(str_ACSN);
            hex_ACSN_len=util.intToHex(str_ACSN.length());
            hex_data.append(UcmsMiddlewareConstants.AGENCYCARDTAG).append(hex_ACSN_len).append(hex_ACSN) ;
            
        }
        if (!isEmpty(str_Iden))
        {
            String hex_Iden_len;
            hex_Iden = util.stringToHex(str_Iden);
            hex_Iden_len=util.intToHex(str_Iden.length());
            hex_data.append(UcmsMiddlewareConstants.ISSUERTAG).append(hex_Iden_len).append(hex_Iden) ;
             
        }
        if (!isEmpty(str_OA1))
        {
            String hex_OA1_len;
            hex_OA1 = util.stringToHex(str_OA1);
            hex_OA1_len=util.intToHex(str_OA1.length());
            hex_data.append(UcmsMiddlewareConstants.ORG1TAG).append(hex_OA1_len).append(hex_OA1) ;
         
        }
        if (!isEmpty(str_OA2))
        {
            String hex_OA2_len;
            hex_OA2 = util.stringToHex(str_OA2);
            hex_OA2_len=util.intToHex(str_OA2.length());
            hex_data.append(UcmsMiddlewareConstants.ORG2TAG).append(hex_OA2_len).append(hex_OA2) ;
          
        }
        
        
        // Now convert to Hex.
        //hex_name = "0120" + util.formatHexString(64, hex_name);
        //hex_emp = "0214" + util.formatHexString(40, hex_emp);
        //hex_date = "0409" + util.formatHexString(18, hex_date);
       // hex_ACSN = "050A" + util.formatHexString(20, hex_ACSN);
       // hex_Iden = "060F" + util.formatHexString(30, hex_Iden);
       // hex_OA1 = "0714" + util.formatHexString(40, hex_OA1);
       // hex_OA2 = "0814" + util.formatHexString(40, hex_OA2);
        String hex_error = "FE00";

        hex_data.append(hex_error);
        
       // hex_data = hex_name + hex_emp + hex_date + hex_ACSN + hex_Iden
        //        + hex_OA1 + hex_OA2 + hex_error;

        newData = util.hexToByteArray(hex_data.toString());
        //populate this variable for security container
        printedInfoDataSecu = newData;
        //////////////////////////////////////////////////////	
        byte[] apdu_printed = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};
        byte[] cont_id = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x09}; //For Write Printed Container
        ///////////////////////////////////////////////////////
         
        //Now Calculate size of the Printed record to be stored
        Integer len_total_bert = newData.length;

        byte[] bert_tag_total=util.calcBertValue(len_total_bert, true,false);

        byte[][] printed_info_arrays = new byte[3][];
        printed_info_arrays[0] = cont_id;
        printed_info_arrays[1] = bert_tag_total;
        printed_info_arrays[2] = newData;

        command_data = util.combine_data(printed_info_arrays);
        
        
        
        //Now Calculate size of the Printed record to be stored
        Integer len_total_apdu = command_data.length;
        byte[] apdu_total=util.calcBertValue(len_total_apdu, false,true);
        
        
        byte[][] apdu_command = new byte[3][];
        apdu_command[0] = apdu_printed;
        apdu_command[1] = apdu_total;
        apdu_command[2] = command_data;
        
        command_data = util.combine_data(apdu_command);

        System.out.println("Final Command " + util.arrayToHex(command_data));

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        System.out.println("Final Result " + sb.toString());
        return true;
    }
    
    
     private boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
}
