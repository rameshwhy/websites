package com.secuera.middleware.cardreader.core;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;

public class GenDigitalSignature {

    CommonUtil util = new CommonUtil();

    public GenDigitalSignature() {
        super();
    }

    public byte[] GenSignature(byte[] input_data, PrivateKey privatekey) {

        byte[] signature = null;
        String algo = "SHA512withRSA";

        Signature sig = null;


        try {
            sig = Signature.getInstance(algo);
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        try {
            sig.initSign(privatekey, null);
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            sig.update(input_data);
        } catch (SignatureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            signature = sig.sign();
        } catch (SignatureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



        System.out.println("Signature Result: " + util.arrayToHex(signature));

        return signature;

    }
}
