/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.beans;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 *
 * @author Satish K
 * This bean defines the input parameter for generating CSR & Certificate 
 * in case of regular Certificates the public & private keys will be null
 * in case of content certificate if these keys are supplied from outside we will use these keys
 * otherwise we will internally generate these keys
 */
public class CSRBean {
    /*
    private String algorithmType = "RSA2048";
    private String organisationalunit = "OU";
    private String organisation = "org";
    private String city = "city";
    private String state = "NC";
    private String country = "US";    
    private String certificateTemplate = "PIV-I Encryption";
    private String caType = "MS";
     */

    private char certificateType;
    private String algorithmType;
    private String organisationalunit;
    private String organisation;
    private String city;
    private String state;
    private String country;
    private int[] cert_template_oid;
    private int majorVer;
    private int minorVer;
    private String caType;
    private String commonName;
    private String certURL;
    private String emailId;
    PublicKey publicKey;
    PrivateKey privateKey;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAlgorithmType() {
        return algorithmType;
    }

    public void setAlgorithmType(String algorithmType) {
        this.algorithmType = algorithmType;
    }

    public String getCaType() {
        return caType;
    }

    public void setCaType(String caType) {
        this.caType = caType;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getOrganisationalunit() {
        return organisationalunit;
    }

    public void setOrganisationalunit(String organisationalunit) {
        this.organisationalunit = organisationalunit;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public char getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(char certificateType) {
        this.certificateType = certificateType;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getCertURL() {
        return certURL;
    }

    public void setCertURL(String certURL) {
        this.certURL = certURL;
    }

    public int[] getCert_template_oid() {
        return cert_template_oid;
    }

    public void setCert_template_oid(int[] cert_template_oid) {
        this.cert_template_oid = cert_template_oid;
    }

    public int getMajorVer() {
        return majorVer;
    }

    public void setMajorVer(int majorVer) {
        this.majorVer = majorVer;
    }

    public int getMinorVer() {
        return minorVer;
    }

    public void setMinorVer(int minorVer) {
        this.minorVer = minorVer;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public String toString() {
        return "CSRBean{" + "certificateType=" + certificateType + ", algorithmType=" + algorithmType + ", organisationalunit=" + organisationalunit + ", organisation=" + organisation + ", city=" + city + ", state=" + state + ", country=" + country + ", cert_template_oid=" + cert_template_oid + ", majorVer=" + majorVer + ", minorVer=" + minorVer + ", caType=" + caType + ", commonName=" + commonName + ", certURL=" + certURL + ", emailId=" + emailId + ", publicKey=" + publicKey + ", privateKey=" + privateKey + '}';
    }
}
