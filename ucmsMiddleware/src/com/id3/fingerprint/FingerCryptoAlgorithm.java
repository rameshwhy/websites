package com.id3.fingerprint;

public class FingerCryptoAlgorithm
{
  public static final int AES_GCM = 0;
  public static final int RSAES_OAEP_SHA = 1;
  public static final int RSAES_PKCS1v15 = 2;
  public static final int AES_CBC = 3;
  public static final int TripeDES_CBC = 4;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCryptoAlgorithm
 * JD-Core Version:    0.6.2
 */