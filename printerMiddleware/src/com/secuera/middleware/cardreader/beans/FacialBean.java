
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.beans;

/**
 *
 * @author Harpreet Singh
 */
public class FacialBean {
    
   private byte[] facial_data;
   private boolean lb_verify;
   private String facialImage;
    public byte[] getFacial_data() {
        return facial_data;
    }

    public void setFacial_data(byte[] facial_data) {
        this.facial_data = facial_data;
    }

    public boolean isLb_verify() {
        return lb_verify;
    }

    public void setLb_verify(boolean lb_verify) {
        this.lb_verify = lb_verify;
    }

    public String getFacialImage() {
        return facialImage;
    }

    public void setFacialImage(String facialImage) {
        this.facialImage = facialImage;
    }

    public String toString() {
        return "FacialBean{" + "facial_data=" + facial_data + ", lb_verify=" + lb_verify + ", facialImage=" + facialImage + '}';
    }
    
   
    
}
