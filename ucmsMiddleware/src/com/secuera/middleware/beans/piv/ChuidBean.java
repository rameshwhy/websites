package com.secuera.middleware.beans.piv;

import java.util.Date;

public class ChuidBean {

    private String agencyCode;          //Bytes = 4
    private String systemCode;          //Bytes = 4
    private String credentialNumber;    //Bytes = 6
    private String cs;                  //Bytes = 1 (always set to 1)
    private String ici;                 //Bytes = 1 (always set to 1)
    private String person;              //Bytes = 16
    private String organizationID;      //Bytes = 4	
    private String duns;                // byte = 9
    private String GUID;                //Byte = 16
    private String expirationDate;      //Byte = 8  (09JAN2013)
    private String fascn;
   
    String algoType; 
    boolean SignatureVerified;
    Date signatureDate;
    ContentCertificateBean contentCertificateBean;
    
    /* 
     * AlgoType
     * "SHA1"
     * "SHA256"
     * "SHA512"
     */
    
    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getCredentialNumber() {
        return credentialNumber;
    }

    public void setCredentialNumber(String credentialNumber) {
        this.credentialNumber = credentialNumber;
    }

    public String getCs() {
        return cs;
    }

    public void setCs(String cs) {
        this.cs = cs;
    }

    public String getIci() {
        return ici;
    }

    public void setIci(String ici) {
        this.ici = ici;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(String organizationID) {
        this.organizationID = organizationID;
    }

    public String getDuns() {
        return duns;
    }

    public void setDuns(String duns) {
        this.duns = duns;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getGUID() {
        return GUID;
    }

    public void setGUID(String gUID) {
        GUID = gUID;
    }

    public String getFascn() {
        return fascn;
    }

    public void setFascn(String fascn) {
        this.fascn = fascn;
    }

    public ContentCertificateBean getContentCertificateBean() {
        return contentCertificateBean;
    }

    public void setContentCertificateBean(ContentCertificateBean contentCertificateBean) {
        this.contentCertificateBean = contentCertificateBean;
    }

    
   

    public boolean isSignatureVerified() {
        return SignatureVerified;
    }

    public void setSignatureVerified(boolean SignatureVerified) {
        this.SignatureVerified = SignatureVerified;
    }

    public Date getSignatureDate() {
        return signatureDate;
    }

    public void setSignatureDate(Date signatureDate) {
        this.signatureDate = signatureDate;
    }

    public String getAlgoType() {
        return algoType;
    }

    public void setAlgoType(String algoType) {
        this.algoType = algoType;
    }

   
    

}
