package com.id3.fingerprint;

public class FingerCaptureDeviceCertification
{
  public static final int NoCertification = 0;
  public static final int AFIS = 1;
  public static final int PIV = 2;
  public static final int OpticalScanner = 3;
  public static final int IQS = 8;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCaptureDeviceCertification
 * JD-Core Version:    0.6.2
 */