/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.io.Serializable;

/**
 *
 * @author Satish K
 */
public class UserRetiredKeysBean implements Serializable{

    private RetiredKeysBean[] retiredKey;
    private String retiredKeyURL;
    private int oncardCertificates;
    private int offcardCertificates;
    
    
    public RetiredKeysBean[] getRetiredKey() {
        return retiredKey;
    }

    public void setRetiredKey(RetiredKeysBean[] retiredKey) {
        this.retiredKey = retiredKey;
    }

    public String getRetiredKeyURL() {
        return retiredKeyURL;
    }

    public void setRetiredKeyURL(String retiredKeyURL) {
        this.retiredKeyURL = retiredKeyURL;
    }

    public int getOffcardCertificates() {
        return offcardCertificates;
    }

    public void setOffcardCertificates(int offcardCertificates) {
        this.offcardCertificates = offcardCertificates;
    }

    public int getOncardCertificates() {
        return oncardCertificates;
    }

    public void setOncardCertificates(int oncardCertificates) {
        this.oncardCertificates = oncardCertificates;
    }
    
    
}
