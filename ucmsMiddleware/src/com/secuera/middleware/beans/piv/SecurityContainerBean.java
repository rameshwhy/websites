/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

/**
 *
 * @author Satish K
 */
public class SecurityContainerBean {
    String chuid;
    String printedInfo;
    String facialImage;

    public String getChuid() {
        return chuid;
    }

    public void setChuid(String chuid) {
        this.chuid = chuid;
    }

    public String getFacialImage() {
        return facialImage;
    }

    public void setFacialImage(String facialImage) {
        this.facialImage = facialImage;
    }

    public String getPrintedInfo() {
        return printedInfo;
    }

    public void setPrintedInfo(String printedInfo) {
        this.printedInfo = printedInfo;
    }

}
