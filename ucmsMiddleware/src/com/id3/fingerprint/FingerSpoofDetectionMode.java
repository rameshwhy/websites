package com.id3.fingerprint;

public class FingerSpoofDetectionMode
{
  public static final int Disabled = 0;
  public static final int Enabled = 1;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerSpoofDetectionMode
 * JD-Core Version:    0.6.2
 */