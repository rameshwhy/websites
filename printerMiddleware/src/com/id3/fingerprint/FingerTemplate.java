package com.id3.fingerprint;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.io.File; 

public class FingerTemplate
{
  private long _handle;
  private boolean _disposed = false;
  private boolean _disposable = true;
  private FingerMinutia[] _minutiae = null;

  static
  {
    String tmpStr = "id3Finger.dll";
      File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
      File localFile = new File(TMP_DIR, tmpStr);


       System.load(localFile.getAbsolutePath());
  }

  public FingerTemplate()
    throws FingerException
  {
    long[] handle_ptr = new long[1];
    int result = nInitialize(handle_ptr);

    if (result != 0) {
      throw new FingerException(result);
    }
    this._handle = handle_ptr[0];
  }

  public FingerTemplate(long handle, boolean disposable)
    throws FingerException
  {
    this._handle = handle;
    this._disposable = disposable;
  }

  public void dispose()
  {
    nDispose(this._handle);
    this._handle = 0L;
    this._disposed = true;
  }

  protected void finalize()
    throws Throwable
  {
    try
    {
      if (this._disposable)
      {
        dispose();
      }
    }
    catch (Exception localException)
    {
    }
    finally
    {
      super.finalize();
    }
  }

  public boolean equals(Object obj)
  {
    if ((obj == null) || (obj.getClass() != getClass())) {
      return false;
    }
    FingerTemplate device = (FingerTemplate)obj;
    return this._handle == device._handle;
  }

  public int hashCode()
  {
    return (int)this._handle;
  }

  public FingerTemplate clone()
  {
    try
    {
      long[] hClonedTemplate = new long[1];
      int result = nClone(this._handle, hClonedTemplate);

      if (result == 0) {
        return new FingerTemplate(hClonedTemplate[0], true);
      }
    }
    catch (FingerException localFingerException)
    {
    }
    return null;
  }

  public long getHandle()
  {
    return this._handle;
  }

  public static FingerTemplate fromBuffer(byte[] data, int format)
    throws FingerException
  {
    long[] handle = new long[1];
    int result = nFromBuffer(data, format, handle);

    if (result != 0) {
      throw new FingerException(result);
    }
    return new FingerTemplate(handle[0], true);
  }

  public static FingerTemplate fromFile(String filename)
    throws FingerException
  {
    long[] handle = new long[1];
    int result = nFromFile(filename, 0, handle);

    if (result != 0) {
      throw new FingerException(result);
    }
    return new FingerTemplate(handle[0], true);
  }

  public static FingerTemplate fromFile(String filename, int format)
    throws FingerException
  {
    long[] handle = new long[1];
    int result = nFromFile(filename, format, handle);

    if (result != 0) {
      throw new FingerException(result);
    }
    return new FingerTemplate(handle[0], true);
  }

  public void save(String filename, int format)
    throws FingerException
  {
    int result = nSave(this._handle, format, filename);

    if (result != 0)
      throw new FingerException(result);
  }

  public byte[] toBuffer(int format)
    throws FingerException
  {
    byte[] data = (byte[])null;
    int[] dataSize = new int[1];

    int result = nToBuffer(this._handle, format, data, dataSize);

    if (result == -13)
    {
      data = new byte[dataSize[0]];

      result = nToBuffer(this._handle, format, data, dataSize);
    }

    if (result != 0) {
      throw new FingerException(result);
    }
    return data;
  }

  public byte[] toIso19794CompactCard(int maxMinutiae)
    throws FingerException
  {
    byte[] data = (byte[])null;
    int[] dataSize = new int[1];

    int result = nToIso19794CompactCard(this._handle, maxMinutiae, data, dataSize);

    if (result == -13)
    {
      data = new byte[dataSize[0]];

      result = nToIso19794CompactCard(this._handle, maxMinutiae, data, dataSize);
    }

    if (result != 0) {
      throw new FingerException(result);
    }
    return data;
  }

  public int compareTo(FingerTemplate other)
    throws FingerException
  {
    int[] score = new int[1];
    int result = nCompareTo(this._handle, other._handle, score);

    if (result != 0) {
      throw new FingerException(result);
    }
    return score[0];
  }

  public void translateAndRotate(int dx, int dy, int cx, int cy, byte theta)
    throws FingerException
  {
    int result = nTranslateAndRotate(this._handle, dx, dy, cx, cy, theta);

    if (result != 0)
      throw new FingerException(result);
  }

  public void addMinutia(FingerMinutia minutia)
    throws FingerException
  {
    int result = nAddMinutia(this._handle, minutia);

    if (result != 0) {
      throw new FingerException(result);
    }
    this._minutiae = null;
  }

  public void removeMinutia(FingerMinutia minutia)
    throws FingerException
  {
    for (int i = 0; i < getMinutiaCount(); i++)
    {
      FingerMinutia m = getMinutiae()[i];

      if ((m.x == minutia.x) && (m.y == minutia.y) && (m.angle == minutia.angle) && (m.type == minutia.type))
      {
        removeMinutia(i);
        break;
      }
    }
  }

  public void removeMinutia(int index)
    throws FingerException
  {
    int result = nRemoveMinutiaAt(this._handle, index);

    if (result != 0) {
      throw new FingerException(result);
    }
    this._minutiae = null;
  }

 /* public void drawMinutiae(Graphics g)
  {
    drawMinutiae(g, java.awt.Color.red);
  }
*/
  public void drawMinutiae(Graphics g, java.awt.Color color)
  {
    FingerMinutia[] minutiae = (FingerMinutia[])null;
    try
    {
      minutiae = getMinutiae();
    } catch (FingerException e) {
      return;
    }

    g.setColor(color);

    for (FingerMinutia m : minutiae)
    {
      int x = m.x;
      int y = m.y;
      int lineSize = 10;
      int circleWidth = 6;

      g.drawOval(x - circleWidth / 2, y - circleWidth / 2, circleWidth, circleWidth);
      g.drawLine(x, y, 
        (int)Math.round(x + lineSize * Math.cos(m.angle * 3.141592653589793D / 128.0D)), 
        (int)Math.round(y - lineSize * Math.sin(m.angle * 3.141592653589793D / 128.0D)));
    }
  }

  public void drawMinutiae(Graphics g, java.awt.Color color, Rectangle size)
  {
    try
    {
      float zoomFactorX = size.width / getImageWidth();
      float zoomFactorY = size.height / getImageHeight();
      float zoomFactor = Math.min(zoomFactorX, zoomFactorY);
      int dx = (int)((size.width - getImageWidth() * zoomFactor) / 2.0F);
      int dy = (int)((size.height - getImageHeight() * zoomFactor) / 2.0F);

      drawMinutiae(g, color, dx, dy, zoomFactor);
    }
    catch (FingerException localFingerException)
    {
    }
  }

  public void drawMinutiae(Graphics g, java.awt.Color color, int dx, int dy, float zoomFactor)
  {
    g.setColor(color);

    Graphics2D g2D = (Graphics2D)g;
    g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
      RenderingHints.VALUE_ANTIALIAS_ON);
    try
    {
      for (FingerMinutia m : getMinutiae())
      {
        float x = m.x * zoomFactor + dx;
        float y = m.y * zoomFactor + dy;
        float lineSize = 15.0F * zoomFactor;

        if (m.type == 2)
        {
          float circleWidth = 6.0F * zoomFactor;
          g.drawRect(
            Math.round(x - circleWidth / 2.0F), 
            Math.round(y - circleWidth / 2.0F), 
            Math.round(circleWidth), 
            Math.round(circleWidth));
        }
        else
        {
          float circleWidth = 7.0F * zoomFactor;
          g.drawOval(
            Math.round(x - circleWidth / 2.0F), 
            Math.round(y - circleWidth / 2.0F), 
            Math.round(circleWidth), 
            Math.round(circleWidth));
        }

        g.drawLine(
          Math.round(x), 
          Math.round(y), 
          (int)Math.round(x + lineSize * Math.cos(m.angle * 3.141592653589793D / 128.0D)), 
          (int)Math.round(y - lineSize * Math.sin(m.angle * 3.141592653589793D / 128.0D)));
      }
    }
    catch (FingerException localFingerException)
    {
    }
  }

 /*  public void drawMinutiae(GraphicsContext g, javafx.scene.paint.Color color)
  {
    try
    {
      FingerMinutia[] minutiae = getMinutiae();

      g.setStroke(color);

      for (FingerMinutia m : minutiae)
      {
        int x = m.x;
        int y = m.y;
        int lineSize = 10;
        int circleWidth = 6;

        g.strokeOval(x - circleWidth / 2, y - circleWidth / 2, circleWidth, circleWidth);
        g.strokeLine(x, y, 
          (int)Math.round(x + lineSize * Math.cos(m.angle * 3.141592653589793D / 128.0D)), 
          (int)Math.round(y - lineSize * Math.sin(m.angle * 3.141592653589793D / 128.0D)));
      }
    }
    catch (FingerException localFingerException)
    {
    }
  }

 public void drawMinutiae(GraphicsContext g, javafx.scene.paint.Color color, double width, double height)
  {
    try
    {
      double zoomFactorX = width / getImageWidth();
      double zoomFactorY = height / getImageHeight();
      double zoomFactor = Math.min(zoomFactorX, zoomFactorY);
      int dx = 0;
      int dy = 0;

      drawMinutiae(g, color, dx, dy, zoomFactor);
    }
    catch (FingerException localFingerException)
    {
    }
  }

  public void drawMinutiae(GraphicsContext g, javafx.scene.paint.Color color, int dx, int dy, double zoomFactor)
  {
    g.setStroke(color);
    try
    {
      for (FingerMinutia m : getMinutiae())
      {
        double x = m.x * zoomFactor + dx;
        double y = m.y * zoomFactor + dy;
        double lineSize = 15.0D * zoomFactor;

        if (m.type == 2)
        {
          double circleWidth = 6.0D * zoomFactor;
          g.strokeRect(
            (int)Math.round(x - circleWidth / 2.0D), 
            (int)Math.round(y - circleWidth / 2.0D), 
            (int)Math.round(circleWidth), 
            (int)Math.round(circleWidth));
        }
        else
        {
          double circleWidth = 7.0D * zoomFactor;
          g.strokeOval(
            (int)Math.round(x - circleWidth / 2.0D), 
            (int)Math.round(y - circleWidth / 2.0D), 
            (int)Math.round(circleWidth), 
            (int)Math.round(circleWidth));
        }

        g.strokeLine(
          (int)Math.round(x), 
          (int)Math.round(y), 
          (int)Math.round(x + lineSize * Math.cos(m.angle * 3.141592653589793D / 128.0D)), 
          (int)Math.round(y - lineSize * Math.sin(m.angle * 3.141592653589793D / 128.0D)));
      }
    }
    catch (FingerException localFingerException)
    {
    }
  }
*/
  public int getMinutiaCount()
    throws FingerException
  {
    int[] count = new int[1];
    int result = nGetMinutiaCount(this._handle, count);

    if (result != 0) {
      throw new FingerException(result);
    }
    return count[0];
  }

  public FingerMinutia[] getMinutiae()
    throws FingerException
  {
    int mcount = getMinutiaCount();
    FingerMinutia[] minutiae = new FingerMinutia[mcount];

    for (int i = 0; i < mcount; i++) {
      minutiae[i] = new FingerMinutia();
    }
    int result = nGetMinutiae(this._handle, minutiae);

    if (result != 0) {
      throw new FingerException(result);
    }
    return minutiae;
  }

  public void setMinutiae(FingerMinutia[] minutiae)
    throws FingerException
  {
    int result = nSetMinutiae(this._handle, minutiae, minutiae.length);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getImpressionType()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetImpressionType(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setImpressionType(int value)
    throws FingerException
  {
    int result = nSetImpressionType(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getPosition()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetFingerPosition(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setPosition(int value)
    throws FingerException
  {
    int result = nSetFingerPosition(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getImageWidth()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetImageWidth(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public int getImageHeight()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetImageHeight(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public int getHorizontalResolution()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetHorizontalResolution(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setHorizontalResolution(int value)
    throws FingerException
  {
    int result = nSetHorizontalResolution(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getVerticalResolution()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetVerticalResolution(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setVerticalResolution(int value)
    throws FingerException
  {
    int result = nSetVerticalResolution(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getImageQuality()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetImageQuality(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setImageQuality(int value)
    throws FingerException
  {
    int result = nSetImageQuality(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getCaptureDeviceCertification()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetCaptureDeviceCertification(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setCaptureDeviceCertification(int value)
    throws FingerException
  {
    int result = nSetCaptureDeviceCertification(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getCaptureDeviceTechnology()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetCaptureDeviceTechnology(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setCaptureDeviceTechnology(int value)
    throws FingerException
  {
    int result = nSetCaptureDeviceTechnology(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getCaptureDeviceVendor()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetCaptureDeviceVendor(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setCaptureDeviceVendor(int value)
    throws FingerException
  {
    int result = nSetCaptureDeviceVendor(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getCaptureDeviceType()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetCaptureDeviceType(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setCaptureDeviceType(int value)
    throws FingerException
  {
    int result = nSetCaptureDeviceType(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getCbeffProductID()
    throws FingerException
  {
    int[] ptr = new int[1];

    int result = nGetCbeffProductID(this._handle, ptr);

    if (result != 0) {
      throw new FingerException(result);
    }
    return ptr[0];
  }

  public void setCbeffProductID(int value)
    throws FingerException
  {
    int result = nSetCbeffProductID(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  private static final native int nInitialize(long[] paramArrayOfLong);

  private static final native int nDispose(long paramLong);

  private static final native int nClone(long paramLong, long[] paramArrayOfLong);

  private static final native int nFromBuffer(byte[] paramArrayOfByte, int paramInt, long[] paramArrayOfLong);

  private static final native int nFromFile(String paramString, int paramInt, long[] paramArrayOfLong);

  private static final native int nSave(long paramLong, int paramInt, String paramString);

  private static final native int nToBuffer(long paramLong, int paramInt, byte[] paramArrayOfByte, int[] paramArrayOfInt);

  private static final native int nToIso19794CompactCard(long paramLong, int paramInt, byte[] paramArrayOfByte, int[] paramArrayOfInt);

  private static final native int nToOnCardComparisonFormat(long paramLong, int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfByte, int[] paramArrayOfInt);

  private static final native int nCompareTo(long paramLong1, long paramLong2, int[] paramArrayOfInt);

  private static final native int nGetMinutiaCount(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetCbeffProductID(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetFingerPosition(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetHorizontalResolution(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetImageHeight(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetImpressionType(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetImageWidth(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetImageQuality(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetVerticalResolution(long paramLong, int[] paramArrayOfInt);

  private static final native int nSetCbeffProductID(long paramLong, int paramInt);

  private static final native int nSetImpressionType(long paramLong, int paramInt);

  private static final native int nSetFingerPosition(long paramLong, int paramInt);

  private static final native int nSetHorizontalResolution(long paramLong, int paramInt);

  private static final native int nSetImageQuality(long paramLong, int paramInt);

  private static final native int nSetImageHeight(long paramLong, int paramInt);

  private static final native int nSetImageWidth(long paramLong, int paramInt);

  private static final native int nSetVerticalResolution(long paramLong, int paramInt);

  private static final native int nGetMinutiae(long paramLong, FingerMinutia[] paramArrayOfFingerMinutia);

  private static final native int nSetMinutiae(long paramLong, FingerMinutia[] paramArrayOfFingerMinutia, int paramInt);

  private static final native int nGetCaptureDeviceCertification(long paramLong, int[] paramArrayOfInt);

  private static final native int nSetCaptureDeviceCertification(long paramLong, int paramInt);

  private static final native int nGetCaptureDeviceVendor(long paramLong, int[] paramArrayOfInt);

  private static final native int nSetCaptureDeviceVendor(long paramLong, int paramInt);

  private static final native int nGetCaptureDeviceTechnology(long paramLong, int[] paramArrayOfInt);

  private static final native int nSetCaptureDeviceTechnology(long paramLong, int paramInt);

  private static final native int nGetCaptureDeviceType(long paramLong, int[] paramArrayOfInt);

  private static final native int nSetCaptureDeviceType(long paramLong, int paramInt);

  private static final native int nTranslateAndRotate(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, byte paramByte);

  private static final native int nAddMinutia(long paramLong, FingerMinutia paramFingerMinutia);

  private static final native int nRemoveMinutiaAt(long paramLong, int paramInt);
}
