package com.id3.fingerprint;

public class FingerImageCompression
{
  public static final int Uncompressed = 0;
  public static final int UncompressedPacked = 1;
  public static final int CompressedWSQ = 2;
  public static final int CompressedJPEG = 3;
  public static final int CompressedJPEG2000 = 4;
  public static final int PNG = 5;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerImageCompression
 * JD-Core Version:    0.6.2
 */