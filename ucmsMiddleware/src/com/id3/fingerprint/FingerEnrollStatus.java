package com.id3.fingerprint;

public class FingerEnrollStatus
{
  public static final int InProgress = 0;
  public static final int WrongFinger = 1;
  public static final int UnknownFinger = 2;
  public static final int NonMatch = 3;
  public static final int Completed = 4;
  public static final int Failed = 5;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerEnrollStatus
 * JD-Core Version:    0.6.2
 */