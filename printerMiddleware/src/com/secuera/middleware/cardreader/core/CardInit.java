/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import com.secuera.middleware.cardreader.beans.InitializationBean;
import com.secuera.middleware.cardreader.beans.SecureChannelBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;

/**
 *
 * @author admin
 */
public class CardInit {
    /*
    mode = A >> initialize complete card
    mode = L >> reset local PIN
    mode = G >> reset Global PIN
    mode = P >> reset PUK PIN
    */        
    private byte[] Local_Pin;
    private byte[] Global_Pin;
    private byte[] Unblock_Pin;
    
    private byte[] Local_PIN_PTC;
    private byte[] Global_PIN_PTC;
    private byte[] Unblock_PIN_PTC;

    private byte[] PRE_PERSO_PIV_ADMIN_KEY;
    private byte[] PIV_ADMIN_KEY;
    private byte[] PIV_9E08;
    private byte[] zero_card_pad = {(byte) 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00};
    private byte[] IV_AES = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  
    byte[] AUTH_DEK_KEY;
    byte[] S_ENC_KEY;
    byte[] S_MAC_KEY;
    byte[] PREV_MAC;
    String card_unlock = "6283";
    // 7 bytes of pin pad.
    byte[] PIN_PAD = {(byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    String DiversifyKey = "N";
    // *************************************************************
    // Create instances of other classes.
    // *************************************************************
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    GeneralAuth auth = new GeneralAuth();
    PivUtilInterface piv = new PivUtil();
    ExceptionMessages expMsg = new ExceptionMessages();
    InitializationBean initValues = new InitializationBean();
    SecureChannelBean scbean = new SecureChannelBean();
    //ManageKeys mkey;

    public CardInit() {
        super();
    }

    // Constructor get initial class values
    public CardInit(InitializationBean iValues) {
        //InitializationBean InitValues= new InitializationBean();
        initValues = iValues;
        //get default key values
        Local_Pin = iValues.getLocal_Pin();
        Global_Pin = iValues.getGlobal_Pin();
        Unblock_Pin = iValues.getUnblock_Pin();

        Local_PIN_PTC = iValues.getLocal_PIN_PTC();
        Global_PIN_PTC = iValues.getGlobal_PIN_PTC();
        Unblock_PIN_PTC = iValues.getUnblock_PIN_PTC();
        
        DiversifyKey = iValues.getDiversifyKey();
        
        //Global Platform Keys (Security Domain Keys)
        //ENC_KEY = iValues.getENC_KEY();
        //MAC_KEY = iValues.getMAC_KEY();
        //DEK_KEY = iValues.getDEK_KEY();

        //Master Keys
        //MASTER_KEY = iValues.getMASTER_KEY();
        //MASTER_KEY_AES = iValues.getMASTER_KEY_AES();

        //PIV Admin Keys
        PRE_PERSO_PIV_ADMIN_KEY = iValues.getPRE_PERSO_PIV_ADMIN_KEY();
        PIV_ADMIN_KEY = iValues.getPIV_ADMIN_KEY();

        PIV_9E08 = iValues.getPIV_9E08();


    }

    
    
    
    public boolean initCard(CardChannel channel, String mode) throws MiddleWareException {
        boolean retValue = true;        
        try {
            
            //check if the card is locked
            retValue = selectManager(channel, false);

            //unlock card if card is locked
            if (retValue == true) {
                CardUtilInterface cUtil = new CardUtil(initValues);
                cUtil.CardUnlock(channel);                
            }

            // Select logical channel 01
            retValue = selectManager(channel, true);

            // Select Security Domain of the Card.
            getFreeEEPROM(channel);            
            getCPLC(channel);                
            selectSecurityDomain(channel);
            
            // Select PIV Instance. Very Important to execute this,before Admin
            // authentication can be performed.
            checkPIVInstance(channel);

            //Get Admin Authentication with pre_perso_key if fail use diversified key.
            try {

                auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
            } catch (MiddleWareException e) {
                try {
                    auth.generalAuth(channel, PIV_ADMIN_KEY);
                } catch (MiddleWareException m) {
                    throw new MiddleWareException(m.getMessage());
                }
            }

            //Clear all the containers.
            clearContainer(channel);

            // Clear all the Keys.
            clearKeys(channel);

            // Now open secure channel 03 to manage/upload the PIN'S (Local,Global and PUK).   
            SecureChannel03 scp03= new SecureChannel03(initValues);
            scbean = scp03.openSecureChannel03(channel,false);
            
            //get values from secure channel 03
            S_MAC_KEY = scbean.getS_MAC_KEY();
            PREV_MAC = scbean.getPREV_MAC();
            AUTH_DEK_KEY = scbean.getAUTH_DEK_KEY();
            
            //Upload Local Pin
            System.out.println("Uploading Local PIN");
            if (mode.equalsIgnoreCase("A") || mode.equalsIgnoreCase("L")) {
                uploadLocalPin(channel);
            }


            //Upload Global Pin
            System.out.println("Uploading Global PIN");
            if (mode.equalsIgnoreCase("A") || mode.equalsIgnoreCase("G")) {
                uploadGlobalPin(channel);
            }



            // Now Upload Un-Block Pin
            System.out.println("Uploading UnLock PIN");
            if (mode.equalsIgnoreCase("A") || mode.equalsIgnoreCase("P")) {
                uploadUnblockPin(channel);
            }

            // Load Admin Key
            System.out.println("Loading Key piv_admin_key");
            if (mode.equalsIgnoreCase("A")) {
                //loadKey_9B03(channel);
                // loadKey_AES(channel,tag_9B03);
                byte[] tag_9B03 = {(byte) 0x9B, 0x03};
               
                if (PIV_ADMIN_KEY.length == 16) {
                    loadKey_AES(channel, PIV_ADMIN_KEY, tag_9B03);
                } else {
                    loadKey_TDES(channel, PIV_ADMIN_KEY, tag_9B03);
                }             

            }
            
            /* commented to be implemeneted later on
            // Load Symmetric Card Authentication Key 9E08
            System.out.println("Loading Key 9E08");
            if (mode.equalsIgnoreCase("A")) {
                loadKey_9E08(channel);
            }
            */ 
            return true;
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    
    /* Delete previous data in all the containers */
    private void clearContainer(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] res = null;

        // Command to read discovery container 0x00,0x00,
        byte[] apdu_command = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x04,
            0x5C, 0x02, 0x3F, (byte) 0xF6};

        CommandAPDU CONTAINER_APDU = new CommandAPDU(apdu_command);

        System.out.println("1st command" + util.arrayToHex(apdu_command));
        try {

            res = sendCard.sendCommand(channel, CONTAINER_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // System.out.println("All Containers: " + util.arrayToHex(res));
        // System.out.println("Container_ID: " + util.arrayToHex(cont_id));
        // 53 81 87
        // 5fc107 01 37 53 35 b2 b2
        // 5fc102 0b 57 53 35 53 b2
        // 5fc10507f75335b2b25fc1030fb73d35b2b25fc10604375335b2b25fc10831b73d35b2b25fc10900973d35b2b25fc10a07f75335b2b25fc10b07f75335b2b25fc10107f7533553b200007e0037533553b25fc10d3a975335b2b25fc10e3a973535b2b25fc10f3a973535b2b25fc1103a973535b2b2

        // Start the loop from i=3 to skip the tag '53' in zero position and 2
        // bytes denoting length of retrieved data
        byte[] cont_id = null;

        for (int i = 3; i < res.length; i += 9) {
            cont_id = util.extract_data(res, i, 3);
            delete(channel, cont_id, 'C');
        }
    }

    /* Delete previous data in all the keys */
    private void clearKeys(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] res = null;

        // Command to read/discover key slots
        // 00 CB 3FFF 000004 5C023FF7 0000

        byte[] apdu_command = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00,
            0x00, 0x04, 0x5C, 0x02, 0x3F, (byte) 0xF7, 0x00, 0x00};

        CommandAPDU CONTAINER_APDU = new CommandAPDU(apdu_command);

        try {

            res = sendCard.sendCommand(channel, CONTAINER_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // System.out.println("All Keys: " + util.arrayToHex(res));
        // System.out.println("Key_ID: " + util.arrayToHex(key_id));
        // System.out.println("key_id_9b: " + key_id_9b);
        // System.out.println("Key_ID Inside delete: " +
        // util.arrayToHex(key_id));
        // 53 82 01 38
        // 82 02 00 00 00 00 00 00 3d 35 b2 b2
        // 83 02 00 00 00 00 00 00 3d 35 b2 b2
        // 84020000000000003d35b2b285020000000000003d35b2b286020000000000003d35b2b287020000000000003d35b2b288020000000000003d35b2b289020000000000003d35b2b28a020000000000003d35b2b28b020000000000003d35b2b28c020000000000003d35b2b28d020000000000003d35b2b28e020000000000003d35b2b28f020000000000003d35b2b290020000000000003d35b2b291020000000000003d35b2b292020000000000003d35b2b293020000000000003d35b2b294020000000000003d35b2b295020000000000003d35b2b29a020000000000003d35b2b29b033500000000015335b2b29c020000000000001935b2b29d020000000000003d35b2b29e01290000000000533553b29e02000000000000533553b2

        // Start the loop from i=4 to skip the tag '53' in zero position and 3
        // bytes denoting length of retrieved data
        // Delete all the keys,if present except Admin key "9B"
        byte[] key_id = null;
        String key_9B = "9b";

        for (int i = 4; i < res.length; i += 12) {

            key_id = util.extract_data(res, i, 2);
            String key_id_9b = util.arrayToHex(util.extract_data(res, i, 1));

            if (!key_9B.equals(key_id_9b)) {
                try {
                    delete(channel, key_id, 'K');
                } catch (MiddleWareException m) {
                    throw new MiddleWareException(m.getMessage());
                }
            }

        }
    }

    void delete(CardChannel channel, byte[] id, char record_type) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();

        // Command to delete container
        // 00 DB 3FFF <?> 5C02 H(1;2) 5300 (9000)
        // 'C' ---> means container
        // 'K' --> Mean key slot
        byte[] apdu_command;
        byte[] cont_id = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF, 0x07, 0x5C,
            0x03};
        byte[] key_id = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF, 0x06, 0x5C,
            0x02};

        if (record_type == 'C') {
            apdu_command = cont_id;
        } else {
            apdu_command = key_id;
        }

        byte[] del_command = {0x53, 0x00};

        apdu_command = util.combine_data(apdu_command, id);
        apdu_command = util.combine_data(apdu_command, del_command);

        // System.out.print("Delete Keys: " + util.arrayToHex(apdu_command));

        CommandAPDU DEL_APDU = new CommandAPDU(apdu_command);

        try {

            sendCard.sendCommand(channel, DEL_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    // Load Symmetric Card Authentication Key 9E08
    private void loadKey_9E08(CardChannel channel,byte[] load_key) throws MiddleWareException {

        byte[] tag_9E08 = {(byte) 0x9E, 0x08};

        loadKey_AES(channel, load_key,tag_9E08);

    }

    private void loadKey_AES(CardChannel channel, byte[] load_key, byte[] key_tag) throws MiddleWareException {

        byte[] key_verification_data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

        //byte[] load_key = PIV_9E08;//mkey.getKey(key_tag);

        // COMPUTE Key Check Value.
        byte[] key_check = util.extract_data(
                aesencrypt(load_key, key_verification_data, false), 0, 3);

        // NOw call Secure key loading function.
        loadKey_secure(channel, load_key, key_check, key_tag);

    }

    private void loadKey_TDES(CardChannel channel, byte[] load_key,byte[] key_tag) throws MiddleWareException {

        byte[] key_verification_data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x00, 0x00};       
        
        // COMPUTE Key Check Value.
        byte[] key_check = util.extract_data( desencrypt(load_key, key_verification_data), 0, 3);

        // NOw call Secure key loading function.
        loadKey_secure(channel, load_key, key_check, key_tag);

    }

    /* Load keys method */
    private void loadKey_secure(CardChannel channel, byte[] load_key, byte[] key_check,byte[] key_tag) throws MiddleWareException {

        byte[] tag_9B03 = {(byte) 0x9B, 0x03};
        byte[] tag_9E08 = {(byte) 0x9E, 0x08};
        // **----------------------------------------------
        // ** Secure injection of Keys
        // **----------------------------------------------
        // First of all format key according to secure encryption requirement.
        load_key = add_securepad(load_key);

        // Now Encrypt Key value with KEK in CBC Mode.
        byte[] key_encrypt = aesencrypt(AUTH_DEK_KEY, load_key, true);

        // Now compute new MAC and prepare APDU command for Key Injection
        StringBuffer sb = new StringBuffer();

        // *****************************************************//
        // 04 ---> 00 is changed to 04 to indicate Secure messaging by setting
        // bit # 3.

        byte[] apdu_data = {(byte) 0x04, (byte) 0xDB, 0x3F, (byte) 0xFF};
        byte[] key_reference = {0x5C, 0x02};

        // Calculate length of encrypted key for ber_tlv tag value.
        byte[] key_len = util.hexToByteArray(Integer.toHexString(key_encrypt.length));
        byte[] ber_tlv = {0x53};

        ber_tlv = util.combine_data(ber_tlv, key_len);

        byte[] ber_tlv_check = {0x54, 0x02, 0x00, 0x00, 0x55, 0x03};

        int len = key_reference.length + key_tag.length + ber_tlv.length
                + key_encrypt.length + ber_tlv_check.length + key_check.length
                + 8;

        byte[] apdu_len = util.hexToByteArray(Integer.toHexString(len));

        byte[] set_key_data = util.combine_data(PREV_MAC, zero_card_pad);
        set_key_data = util.combine_data(set_key_data, apdu_data);
        set_key_data = util.combine_data(set_key_data, apdu_len);
        set_key_data = util.combine_data(set_key_data, key_reference);
        set_key_data = util.combine_data(set_key_data, key_tag);
        set_key_data = util.combine_data(set_key_data, ber_tlv);
        set_key_data = util.combine_data(set_key_data, key_encrypt);
        set_key_data = util.combine_data(set_key_data, ber_tlv_check);
        set_key_data = util.combine_data(set_key_data, key_check);

        // Now add secure pad according to encryption requirement.
        set_key_data = add_securepad(set_key_data);

        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, set_key_data, true);

        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        int i_offset = getOffset(encrypt_data);

        PREV_MAC = util.extract_data(encrypt_data, i_offset, 8);

        byte[] key_command_data = util.combine_data(apdu_data, apdu_len);
        key_command_data = util.combine_data(key_command_data, key_reference);
        key_command_data = util.combine_data(key_command_data, key_tag);
        key_command_data = util.combine_data(key_command_data, ber_tlv);
        key_command_data = util.combine_data(key_command_data, key_encrypt);
        key_command_data = util.combine_data(key_command_data, ber_tlv_check);
        key_command_data = util.combine_data(key_command_data, key_check);

        key_command_data = util.combine_data(key_command_data, PREV_MAC);

        // System.out.println("key_command_data :" +
        // util.arrayToHex(key_command_data) );

        CommandAPDU KEY_APDU = new CommandAPDU(key_command_data);

        try {

            sendCard.sendCommand(channel, KEY_APDU, sb);

            // check if no success then throw exception 
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();

                if (key_tag.toString().equals(tag_9B03.toString())) {
                    errMsg.append(ExceptionMessages.INIT_KEY_9B03);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());

                } else if (key_tag.toString().equals(tag_9E08.toString())) {
                    errMsg.append(ExceptionMessages.INIT_KEY_9E08);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());
                }
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    public byte[] aesencrypt(byte[] key, byte[] from_card, boolean vector) throws MiddleWareException {

        // Create encrypter/decrypter class
        AesEncrypter encrypter;
        byte[] res = null;
        try {

            if (vector) {
                encrypter = new AesEncrypter(key, IV_AES);
            }
            {
                encrypter = new AesEncrypter(key);
            }

            // Encrypt
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return res;

    }

    byte[] desencrypt(byte[] key, byte[] from_card) throws MiddleWareException {
        try {
            // Create encrypter/decrypter class
            DesEncrypter encrypter = new DesEncrypter(key);
            // Encrypt
            byte[] res = encrypter.encrypt(from_card);

            return res;
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    private byte[] macencrypt(byte[] key, byte[] vector, byte[] from_card) throws MiddleWareException {

        MacEncrypter encrypter = new MacEncrypter(key, vector);

        // Encrypt
        byte[] res = null;
        try {
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        return res;

    }

    
    /* select Card Manager method */
    private boolean selectManager(CardChannel channel, Boolean logicalChannel) throws MiddleWareException {

        // Card manager selection on logical channel 0
        // 00 A4 04 00 00 (6CXX, 6283, 9000) ; Status 6283 is returned when the
        // CM is locked

        StringBuffer sb = new StringBuffer();
        boolean retValue = false;

        byte[] log_ch0 = {0x00, (byte) 0xA4, 0x04, 0x00, 0x00};
        byte[] log_ch1 = {0x01, (byte) 0xA4, 0x04, 0x00, 0x00};

        byte[] apdu_manager;

        if (logicalChannel) {
            apdu_manager = log_ch1;
        } else {
            apdu_manager = log_ch0;
        }


        CommandAPDU INIT_APDU = new CommandAPDU(apdu_manager);

        try {
            sendCard.sendCommand(channel, INIT_APDU, sb);

            // check if success return true
            if (sb.toString().equals(card_unlock)) {
                retValue = true;
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return retValue;
    }

    /* select Security Domain method */
    void selectSecurityDomain(CardChannel channel) throws MiddleWareException {

        // **----------------------------------------------
        // ** Check presence and status of PIV-SD
        // **----------------------------------------------
        // ; Select PIV Application Security Domain using Oberthur registered
        // AID
        // 00 A4 04 00 10 A0 00 00 00 77 01 00 00 06 10 00 FD 00 00 00 27 (9000,
        // 6A82)

        StringBuffer sb = new StringBuffer();
        byte[] apdu_security_piv = {0x00, (byte) 0xA4, 0x04, 0x00, 0x10,
            (byte) 0xA0, 0x00, 0x00, 0x00, 0x77, 0x01, 0x00, 0x00, 0x06,
            0x10, 0x00, (byte) 0xFD, 0x00, 0x00, 0x00, 0x27};

        CommandAPDU SEC_APDU_PIV = new CommandAPDU(apdu_security_piv);

        try {
            sendCard.sendCommand(channel, SEC_APDU_PIV, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // ; Select PIV Application Security Domain using GP AID
        // 00 A4 04 00 07 A0 00 00 01 51 00 01 (9000, 6A82)

        byte[] apdu_security = {0x00, (byte) 0xA4, 0x04, 0x00, 0x07,
            (byte) 0xA0, 0x00, 0x00, 0x01, 0x51, 0x00, 0x01};

        CommandAPDU SEC_APDU = new CommandAPDU(apdu_security);

        try {
            sendCard.sendCommand(channel, SEC_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public void checkPIVInstance(CardChannel channel) throws MiddleWareException {

        // ; Check PIV instance FCI
        // 00 A4 04 00 09 A0 00 00 03 08 00 00 10 00 00 [ \

        StringBuffer sb = new StringBuffer();
        byte[] apdu_security_piv = {0x00, (byte) 0xA4, 0x04, 0x00, 0x09,
            (byte) 0xA0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00,
            0x00};

        CommandAPDU SEC_APDU_PIV = new CommandAPDU(apdu_security_piv);

        try {
            sendCard.sendCommand(channel, SEC_APDU_PIV, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    private void uploadLocalPin(CardChannel channel) throws MiddleWareException {

        // **----------------------------------------------
        // ** Define Local PIN and associated PTC
        // **----------------------------------------------
        byte[] pin_data = util.combine_data(Local_Pin, Local_PIN_PTC);
        pin_data = util.combine_data(pin_data, PIN_PAD);

        byte[] Local_pin_data = aesencrypt(AUTH_DEK_KEY, pin_data, true);

        // System.out.println("Upload local PIN_1 :" +
        // util.arrayToHex(Local_pin_data) );
        try {
            loadPin(channel, Local_pin_data, 'L');
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getMessage());
        }

    }

    void uploadGlobalPin(CardChannel channel) throws MiddleWareException {

        // **----------------------------------------------
        // ** Define Global PIN and associated PTC
        // **----------------------------------------------

        byte[] pin_data = util.combine_data(Global_Pin, Global_PIN_PTC);
        pin_data = util.combine_data(pin_data, PIN_PAD);

        byte[] Global_pin_data = aesencrypt(AUTH_DEK_KEY, pin_data, true);

        // System.out.println("Upload Global PIN_1 :" +
        // util.arrayToHex(Global_pin_data) );
        try {
            loadPin(channel, Global_pin_data, 'G');
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getMessage());
        }
    }

    void uploadUnblockPin(CardChannel channel) throws MiddleWareException {

        // **----------------------------------------------
        // ** Define Global PIN and associated PTC
        // **----------------------------------------------

        byte[] pin_data = util.combine_data(Unblock_Pin, Unblock_PIN_PTC);
        pin_data = util.combine_data(pin_data, PIN_PAD);

        byte[] Unblock_pin_data = aesencrypt(AUTH_DEK_KEY, pin_data, true);

        // System.out.println("Upload Unblock PIN_1 :" +
        // util.arrayToHex(Unblock_pin_data) );
        try {
            loadPin(channel, Unblock_pin_data, 'U');
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getMessage());
        }


    }

    private void loadPin(CardChannel channel, byte[] pin_data, char pin_type) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();

        // *****************************************************//
        // 04 ---> 00 is changed to 04 to indicate Secure messaging by setting
        // bit # 3.

        byte[] apdu_data;
        byte[] apdu_local = {(byte) 0x04, (byte) 0x2c, (byte) 0x02,
            (byte) 0x80, 0x18};
        byte[] apdu_global = {(byte) 0x04, (byte) 0x2c, (byte) 0x02,
            (byte) 0x00, 0x18};
        byte[] apdu_puk = {(byte) 0x04, (byte) 0x2c, (byte) 0x02, (byte) 0x81,
            0x18};

        if (pin_type == 'L') {
            apdu_data = apdu_local;
        } else if (pin_type == 'G') {
            apdu_data = apdu_global;
        } else {
            apdu_data = apdu_puk;
        }

        byte[] pin_command_data = util.combine_data(apdu_data, pin_data);

        byte[] set_pin_data = util.combine_data(PREV_MAC, zero_card_pad);
        set_pin_data = util.combine_data(set_pin_data, pin_command_data);

        // Now pad with following bytes to make the above buffer multiple of 16
        // bytes
        byte[] pad = {(byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00};

        set_pin_data = util.combine_data(set_pin_data, pad);

        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, set_pin_data, true);

        // System.out.println("ENCRYPT DATA :" + util.arrayToHex(encrypt_data)
        // );
        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        PREV_MAC = util.extract_data(encrypt_data, 32, 8);

        // Now set the APDU command for PIN Upload.
        pin_command_data = util.combine_data(pin_command_data, PREV_MAC);

        CommandAPDU PIN_APDU = new CommandAPDU(pin_command_data);

        try {

            sendCard.sendCommand(channel, PIN_APDU, sb);

            // check if no success then throw exception 
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();

                if (pin_type == 'L') {
                    errMsg.append(ExceptionMessages.INIT_LOCAL_PIN);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());

                } else if (pin_type == 'G') {
                    errMsg.append(ExceptionMessages.INIT_GLOBAL_PIN);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());

                } else {
                    errMsg.append(ExceptionMessages.INIT_UNLOCK_PIN);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());
                }
            }
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }


    }

    /* Get FREE EEPROM */
    private void getFreeEEPROM(CardChannel channel) throws MiddleWareException {
        String errorMsg;
        StringBuffer sb = new StringBuffer();
        byte[] apdu_eeprom = {0x01, (byte) 0xCA, (byte) 0xDF, 0x64, 0x00};

        CommandAPDU INIT_APDU = new CommandAPDU(apdu_eeprom);

        try {
            sendCard.sendCommand(channel, INIT_APDU, sb);

            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    /* Get CPLC data */
    private void getCPLC(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] apdu_cplc = {(byte) 0x81, (byte) 0xCA, (byte) 0x9F, 0x7F, 0x2D};

        CommandAPDU INIT_APDU = new CommandAPDU(apdu_cplc);

        try {

            sendCard.sendCommand(channel, INIT_APDU, sb);

            /*
            if (!sb.toString().equals("9000")){
            errorMsg = expMsg.GetErrorMsg(sb.toString(),"");				
            throw new MiddleWareException(errorMsg);
            }
             */
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    /* Adding secure pad to data to be encrypted for AES CBC Mode */
    byte[] add_securepad(byte[] data) {

        byte[] append_80 = {(byte) 0x80};
        byte[] append_00 = {0x00};

        int i_len = data.length;
        int k = i_len % 16; // Divide by 16, because one standard bye block of
        // data is 16.
        int i_cnt = 16 - k; // Calculate how many more bytes required to make a
        // block of 16 bytes.

        if (k == 0) {
            return data;
        } else {
            for (int j = 0; j < i_cnt; j++) {
                // For the first append, "use append_80"
                if (j == 0) {
                    data = util.combine_data(data, append_80);

                } else {
                    data = util.combine_data(data, append_00);
                }

            }

            return data;

        }

    }

    /*
     * Determine the offset position of last block of 16 byte data from byte[]
     * array.
     */
    int getOffset(byte[] data) {

        // Subtract by 16,because one standard bye block of data is 16.
        int i_len = data.length - 16;

        return i_len;

    }
}
