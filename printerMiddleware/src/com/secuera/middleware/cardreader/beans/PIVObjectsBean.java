/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.beans;

/**
 *
 * @author Satish K
 */
public class PIVObjectsBean {
    private String PIVObjectID;
    private String PIVObjectName;
    private Integer ContainerSize;
    private String DataFormat;
    private String ContactRead;
    private String ContactUpdate;
    private String ContactLessRead;
    private String ContactLessUpdate;
    

    public String getContactLessRead() {
        return ContactLessRead;
    }

    public void setContactLessRead(String ContactLessRead) {
        this.ContactLessRead = ContactLessRead;
    }

    public String getContactLessUpdate() {
        return ContactLessUpdate;
    }

    public void setContactLessUpdate(String ContactLessUpdate) {
        this.ContactLessUpdate = ContactLessUpdate;
    }

    public String getContactRead() {
        return ContactRead;
    }

    public void setContactRead(String ContactRead) {
        this.ContactRead = ContactRead;
    }

    public String getContactUpdate() {
        return ContactUpdate;
    }

    public void setContactUpdate(String ContactUpdate) {
        this.ContactUpdate = ContactUpdate;
    }

    public String getDataFormat() {
        return DataFormat;
    }

    public void setDataFormat(String DataFormat) {
        this.DataFormat = DataFormat;
    }

    public String getPIVObjectID() {
        return PIVObjectID;
    }

    public void setPIVObjectID(String PIVObjectID) {
        this.PIVObjectID = PIVObjectID;
    }

    public String getPIVObjectName() {
        return PIVObjectName;
    }

    public void setPIVObjectName(String PIVObjectName) {
        this.PIVObjectName = PIVObjectName;
    }

    public Integer getContainerSize() {
        return ContainerSize;
    }

    public void setContainerSize(Integer ContainerSize) {
        this.ContainerSize = ContainerSize;
    }

    public String toString() {
        return "PIVObjectsBean{" + "PIVObjectID=" + PIVObjectID + ", PIVObjectName=" + PIVObjectName + ", DataFormat=" + DataFormat + ", ContactRead=" + ContactRead + ", ContactUpdate=" + ContactUpdate + ", ContactLessRead=" + ContactLessRead + ", ContactLessUpdate=" + ContactLessUpdate + ", ContainerSize=" + ContainerSize + '}';
    }

   
    
}
