/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.exceptions;

/**
 *
 * @author admin
 */

public class LoggerWrapper {

	/**
	 * Log a trace message when exiting a method.
	 *
	 * @param className - The name of the class
	 * @param methodName - The name of the method (Do not include parentheses)
	 */
	public static synchronized void entering(String className, String methodName) {
		entering(className, methodName, "");
	}
	
	/**
	 * Log a trace method for exiting a method
	 *
	 * @param className - The name of the class
	 * @param methodName - The name of the method (Do not include parentheses)
	 */
	public static synchronized void exiting(String className, String methodName) {
		exiting(className, methodName, "");
	}
	
	/**
	 * Log a trace message when entering a method
	 *
	 * @param className - The name of the class
	 * @param methodName - The name of the method (Do not include parentheses)
	 * @param paramTypes - The names of the parameter types separated by spaces.
	 */
	public static synchronized void entering(String className, String methodName,
			String paramTypes) {
		// TODO: Create a logging method
		System.out.println(className + " : " + methodName);
	}
	/**
	 * Log a trace message when exiting a method
	 *
	 * @param className - The name of the class
	 * @param methodName - The name of the method (Do not include parentheses)
	 * @param paramTypes - The names of the parameter types separated by spaces.
	 */
	private static synchronized void exiting(String className, String methodName,
			String paramTypes) {
		// TODO : Create a logging method
		System.out.println(className + " : " + methodName);
	}
	
	/**
	 * Log a message according to the specified priority.
	 *
	 * @param priority - The priority (a LogDefs constant)
	 * @param msg - The message
	 */
	public static synchronized void log(int priority, String msg) {
		//TODO:
	}

	/**
	 * Log a message and error code according to the specified priority.
	 *
	 * @param priority - The priority (a LogDefs constant)
	 * @param msg - The message
	 * @param code - The error code
	 */
	public static synchronized void log(int priority, String msg, long code) {
		//TODO:
	}

	/**
	 * Log an exception as a specified priority.
	 *
	 * @param priority - The priority (a LogDefs constant)
	 * @param throwable - The exception.
	 */
	public static synchronized void log(int priority, Throwable throwable) {
		// TODO:
		
	}

	public static void error(String message) {
		// TODO Auto-generated method stub
		System.out.println(message);
		
	}
	
	public static void error(Throwable e) {
		// TODO Auto-generated method stub
		
	}

	public static void info(String message) {
		// TODO Auto-generated method stub
		System.out.println(message);
	}
}
