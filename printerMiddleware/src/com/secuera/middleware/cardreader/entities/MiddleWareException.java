package com.secuera.middleware.cardreader.entities;

public class MiddleWareException extends Exception {
    

	private static final long serialVersionUID = 1L;
	
	public static final short BPE_WARNING = 1;
	public static final short BPE_HARD_ERROR = 3;
	public static final short RATING_FAILURE = 4;
	public static final short BPE_LOC_RATE = 4;
	
	/**
     * The container for the exception's data
     */
    private ExceptionEntry exceptionEntry = new ExceptionEntry();
    

    /**
     * Construct with NO descriptive info...avoid!
     */
    public MiddleWareException() {
        super();
    }
    
    /**
     * Create an exception to be used from JNI code
     * @param s exception message
     */
    public MiddleWareException(String s) {
        // need this here so that messages the JNI code produces don't get lost
        // Should only use this method for JNI code.
    	super(s);
    }

    /**
     * Creates an exception with the argument's information
     * @param e the exception object to copy the data
     */
    public MiddleWareException(Exception e) {
        this(e.getMessage());
    }

    /**
     * Copy constructor
     * @param re the exception object to copy the data from
     */
    public MiddleWareException(MiddleWareException re) {
        this(re.getErrorCode(), re.getErrorSeverity(), re.getErrorLocation(),
                re.getErrorMsg(), re.getDigest());
    }

    /**
     * Creates an instance of this calss
     * @param errNum erro number
     * @param sev severity
     * @param errLoc error location
     * @param errMsg error message
     * @param theDigest digest
     */
    public MiddleWareException(int errNum, int sev, String errLoc, String errMsg,
            String theDigest) {
        // if exceptionEntry is already filled in, overwrite it
        super(errMsg);	// so the errMsg is the root exception geterrormessage
        exceptionEntry = new ExceptionEntry(errNum, sev, errLoc, errMsg,
                theDigest);
    }

    /**
     * @return result of a call to {@link ExceptionEntry#getErrorCode() } on the {@link #exceptionEntry}
     */
    public int getErrorCode() {
        return exceptionEntry.getErrorCode();
    }

    /**
     * @return result of a call to {@link ExceptionEntry#getErrorMsg()  } on the {@link #exceptionEntry}
     */
    public String getErrorMsg() {
        return exceptionEntry.getErrorMsg();
    }

    /**
     * @return result of a call to {@link ExceptionEntry#getSeverity()  } on the {@link #exceptionEntry}
     */
    public int getErrorSeverity() {
        return exceptionEntry.getSeverity();
    }

    /**
     * @return result of a call to {@link ExceptionEntry#getErrorLocation()  } on the {@link #exceptionEntry}
     */
    public String getErrorLocation() {
        return exceptionEntry.getErrorLocation();
    }

    /**
     * @return result of a call to {@link ExceptionEntry#getDigest()  } on the {@link #exceptionEntry}
     */
    public String getDigest() {
        return exceptionEntry.getErrorDigest();
    }

    public String toString() {
        final String NL = System.getProperty("line.separator");
        StringBuffer b = new StringBuffer();
        b.append(this.getClass().getName()).append(": ").append(getErrorMsg());
        b.append(NL).append("errorCode: ").append(getErrorCode());
        b.append(NL).append("severity: ").append(getErrorSeverity());
        b.append(NL).append("location: ").append(getErrorLocation());
        b.append(NL).append("digest: ").append(getDigest());
        return b.toString();
    }

}
