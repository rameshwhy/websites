// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.wia;

import eu.gnome.morena.*;
import java.util.*;
import java.util.logging.Level;

// Referenced classes of package eu.gnome.morena.wia:
//            WIADevice, WIAScanner, WIACamera

public class WIAManager extends Manager
    implements Runnable
{

    private long handle;
    private boolean isInitialized;
    private List devices;
    private static int mode = 0;
    public static final WIAManager INSTANCE = new WIAManager();
    private Thread poll;
    
    private native long _initialize(int i, boolean flag)
        throws Exception;

    private native void _release()
        throws Exception;

    private synchronized native void _poll();

    public synchronized void run()
    {
       System.out.println("Poll thread started");
        try
        {
            do
            {
                wait(3000L);
                listDevices();
            } while(true);
        }
        catch(Exception exception)
        {
            System.out.println((new StringBuilder()).append("Poll thread finished: ").append(exception.toString()).toString());
        }
    }

    private void add(String s, int i, String s1)
    {
        for(Iterator iterator = devices.iterator(); iterator.hasNext();)
        {
            Device device = (Device)iterator.next();
            if(((WIADevice)device).UID.equals(s))
            {
                ((WIADevice)device).available = true;
                return;
            }
        }

        Object obj = Configuration.isScanner(s1, (i & 1) != 0) ? ((Object) (new WIAScanner(s1, s))) : ((Object) (new WIACamera(s1, s)));
        devices.add(obj);
        fireDeviceConnected(((Device) (obj)));
        System.out.println((new StringBuilder()).append("Adding device ").append(s).append(" \"").append(s1).append("\" Type=").append(Integer.toHexString(i)).toString());
    }

    private void remove(String s, String s1)
    {
        for(Iterator iterator = devices.iterator(); iterator.hasNext();)
        {
            Device device = (Device)iterator.next();
            if(((WIADevice)device).UID.equals(s))
            {
                devices.remove(device);
                System.out.println((new StringBuilder()).append("Removing device ").append(s).append(" \"").append(s1).toString());
                ((WIADevice)device).release();
                fireDeviceDisconnected(device);
                return;
            }
        }

    }

    private WIAManager()
    {
        handle = 0L;
        isInitialized = false;
        devices = new Vector();
        poll = null;
    }

    public void close()
    {
        if(isInitialized)
            try
            {
                Device device;
                for(Iterator iterator = devices.iterator(); iterator.hasNext(); ((WIADevice)device).release())
                    device = (Device)iterator.next();

                devices.clear();
                _release();
                isInitialized = false;
            }
            catch(Exception exception) { }
        System.out.println((new StringBuilder()).append("WIA").append(wiaver).append(" closed.").toString());
    }

    public synchronized List listDevices()
    {
        if(available())
        {
            if(isPollEnabled() || wiaver == 1 && isNativeUIEnabled())
            {
                for(Iterator iterator = devices.iterator(); iterator.hasNext();)
                {
                    Device device = (Device)iterator.next();
                    ((WIADevice)device).available = false;
                }

                _poll();
                Vector vector = new Vector();
                Iterator iterator1 = devices.iterator();
                do
                {
                    if(!iterator1.hasNext())
                        break;
                    Device device1 = (Device)iterator1.next();
                    if(!((WIADevice)device1).available)
                        vector.add((WIADevice)device1);
                } while(true);
                WIADevice wiadevice;
                for(Iterator iterator2 = vector.iterator(); iterator2.hasNext(); remove(((WIADevice)wiadevice).UID, wiadevice.toString()))
                    wiadevice = (WIADevice)iterator2.next();

            }
            return devices;
        } else
        {
            return null;
        }
    }

    public boolean available()
    {
       if ((!this.isInitialized) || (this.poll == null) || (!this.poll.isAlive())){
       try
            {
                mode = Configuration.getMode();
               System.out.println((new StringBuilder()).append("Initializing WIA").append(wiaver).append(" ...").toString());
                handle = _initialize(mode, Configuration.getLogLevel().equals(Level.ALL));
                if(isPollEnabled())
                {
                    poll = new Thread(this, "WIA device tree scanner");
                    poll.setDaemon(true);
                    poll.start();
                }
              
            }
            catch(Exception exp)
            {
                exp.printStackTrace();
               return   false;
            } 
            
         }
        return this.handle != 0L;
        }

    public String toString()
    {
        return "WIA";
    }

    public static int getVersion()
    {
        return wiaver;
    }

    public static boolean isNativeUIEnabled()
    {
        return (mode & 1) > 0;
    }

    public static boolean isPollEnabled()
    {
        return wiaver == 1 && (mode & 2) > 0;
    }

  

}
