package com.id3.fingerprint;

public class FingerMatchDecision
{
  public static final int NonMatch = 0;
  public static final int Match = 1;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerMatchDecision
 * JD-Core Version:    0.6.2
 */