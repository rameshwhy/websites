/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;


import java.security.InvalidAlgorithmParameterException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import com.secuera.middleware.cardreader.entities.MiddleWareException;


/**
 *
 * @author admin
 */

public class AesEncrypter {

	Cipher ecipher;
	Cipher dcipher;

	CommonUtil util = new CommonUtil();

	public AesEncrypter(byte[] key_p) throws MiddleWareException {
		try {

			String transformation = "AES/ECB/NoPadding";

			SecretKeySpec keySpec = new SecretKeySpec(key_p, "AES");

			ecipher = Cipher.getInstance(transformation);
			ecipher.init(Cipher.ENCRYPT_MODE, keySpec);

		} catch (javax.crypto.NoSuchPaddingException e) {
			throw new MiddleWareException (e.getMessage());	
		} catch (java.security.NoSuchAlgorithmException e) {
			throw new MiddleWareException (e.getMessage());	
		} catch (java.security.InvalidKeyException e) {			
			throw new MiddleWareException (e.getMessage());		
		}
	}

	public AesEncrypter(byte[] key_p, byte[] IV) throws MiddleWareException {
		try {

			String transformation = "AES/CBC/NoPadding";

			SecretKeySpec keySpec = new SecretKeySpec(key_p, "AES");

			// AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
			IvParameterSpec iv = new IvParameterSpec(IV);

			ecipher = Cipher.getInstance(transformation);
			ecipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);

		} catch (javax.crypto.NoSuchPaddingException e) {
			throw new MiddleWareException (e.getMessage());	
		} catch (java.security.NoSuchAlgorithmException e) {
			throw new MiddleWareException (e.getMessage());	
		} catch (java.security.InvalidKeyException e) {
			throw new MiddleWareException (e.getMessage());	
		} catch (InvalidAlgorithmParameterException e) {
			throw new MiddleWareException (e.getMessage());			
		}
	}

	public byte[] encrypt(byte[] encr) throws MiddleWareException {
		byte[] enc = null;
		byte[] temp_enc = null;
		
		try {
			for (int i = 0; i < encr.length; i = i + 16) {
				// Encrypt 
				if (i == 0) {
					//(get the first 16 blocks of 16 bytes)
					temp_enc = ecipher.doFinal(util.extract_data(encr, i, 16)); 
				}

				if (i > 0) {
					byte[] next_block;
					byte[] res = new byte[16];

					next_block = util.extract_data(encr, i, 16);

					for (int j = 0; j < next_block.length; j++) {
						res[j] = (byte) (next_block[j] ^ temp_enc[j]);
					}

					temp_enc = ecipher.doFinal(res);

				}

				if (i > 0) {
					enc = util.combine_data(enc, temp_enc);
				} else {
					enc = temp_enc;
				}
			}

			// Encode bytes to base64 to get a string
			// return new sun.misc.BASE64Encoder().encode(enc);
		
		} catch (javax.crypto.BadPaddingException e) {
			throw new MiddleWareException (e.getMessage());	
		} catch (IllegalBlockSizeException e) {
			throw new MiddleWareException (e.getMessage());	
		}
		return enc;
	}

	public byte[] decrypt(byte[] dcr) throws MiddleWareException {
		byte[] dcry = null;
		try {
			// Decode base64 to get bytes
			// byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

			// Decrypt
			dcry = dcipher.doFinal(dcr);

			// Decode using utf-8
			// return new String(utf8, "UTF8");	

		} catch (javax.crypto.BadPaddingException e) {
			throw new MiddleWareException(e.getMessage());
		} catch (IllegalBlockSizeException e) {
			throw new MiddleWareException(e.getMessage());
		}
		return dcry;
	}

}

