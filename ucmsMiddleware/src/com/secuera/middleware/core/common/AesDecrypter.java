/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.common;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;
import com.secuera.middleware.cardreader.entities.MiddleWareException;

/**
 *
 * @author Harpreet Singh
 */
public class AesDecrypter {

    private String ECB_TRANSFORM_STRING = "AES/ECB/NoPadding";
    
    Cipher dcipher;
    CommonUtil util = new CommonUtil();

    public AesDecrypter(byte[] key_p) throws MiddleWareException {
        try {

            SecretKeySpec keySpec = new SecretKeySpec(key_p, "AES");

            dcipher = Cipher.getInstance(ECB_TRANSFORM_STRING);
            dcipher.init(Cipher.DECRYPT_MODE, keySpec);

        } catch (javax.crypto.NoSuchPaddingException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (java.security.InvalidKeyException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    
    
    
    public byte[] decrypt(byte[] dcr) throws MiddleWareException {
        byte[] dcry = null;
        try {
            // Decode base64 to get bytes
            // byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

            // Decrypt
            dcry = dcipher.doFinal(dcr);

            // Decode using utf-8
            // return new String(utf8, "UTF8");	

        } catch (javax.crypto.BadPaddingException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (IllegalBlockSizeException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return dcry;
    }
}

