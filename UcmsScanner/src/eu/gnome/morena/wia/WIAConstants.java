// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.wia;


public interface WIAConstants
{

    public static final int WIA_CATEGORY_FINISHED_FILE = 1;
    public static final int WIA_CATEGORY_FLATBED = 2;
    public static final int WIA_CATEGORY_FEEDER = 3;
    public static final int WIA_CATEGORY_FILM = 4;
    public static final int WIA_CATEGORY_ROOT = 5;
    public static final int WIA_CATEGORY_FOLDER = 6;
    public static final int WIA_CATEGORY_FEEDER_FRONT = 7;
    public static final int WIA_CATEGORY_FEEDER_BACK = 8;
    public static final int WIA_CATEGORY_AUTO = 9;
    public static final int WIA_HANDLING_CAPABILITY_FEED = 1;
    public static final int WIA_HANDLING_CAPABILITY_FLAT = 2;
    public static final int WIA_HANDLING_CAPABILITY_DUP = 4;
    public static final int WIA_HANDLING_SELECT_FEEDER = 1;
    public static final int WIA_HANDLING_SELECT_FLATBED = 2;
    public static final int WIA_HANDLING_SELECT_DUPLEX = 4;
    public static final int WIA_HANDLING_SELECT_FRONT_FIRST = 8;
    public static final int WIA_HANDLING_SELECT_BACK_FIRST = 16;
    public static final int WIA_HANDLING_SELECT_FRONT_ONLY = 32;
    public static final int WIA_HANDLING_SELECT_BACK_ONLY = 64;
    public static final int WIA_HANDLING_SELECT_NEXT_PAGE = 128;
    public static final int WIA_HANDLING_SELECT_PREFEED = 256;
    public static final int WIA_HANDLING_SELECT_AUTO_ADVANCE = 512;
    public static final int WIA_ERROR_GENERAL_ERROR = 1;
    public static final int WIA_ERROR_PAPER_JAM = 2;
    public static final int WIA_ERROR_PAPER_EMPTY = 3;
    public static final int WIA_ERROR_PAPER_PROBLEM = 4;
    public static final int WIA_ERROR_OFFLINE = 5;
    public static final int WIA_ERROR_BUSY = 6;
    public static final int WIA_ERROR_WARMING_UP = 7;
    public static final int WIA_ERROR_USER_INTERVENTION = 8;
    public static final int WIA_ERROR_ITEM_DELETED = 9;
    public static final int WIA_ERROR_DEVICE_COMMUNICATION = 10;
    public static final int WIA_ERROR_INVALID_COMMAND = 11;
    public static final int WIA_ERROR_INCORRECT_HARDWARE_SETTING = 12;
    public static final int WIA_ERROR_DEVICE_LOCKED = 13;
    public static final int WIA_ERROR_EXCEPTION_IN_DRIVER = 14;
    public static final int WIA_ERROR_INVALID_DRIVER_RESPONSE = 15;
    public static final int WIA_ERROR_COVER_OPEN = 16;
    public static final int WIA_ERROR_LAMP_OFF = 17;
    public static final int WIA_ERROR_DESTINATION = 18;
    public static final int WIA_ERROR_NETWORK_RESERVATION_FAILED = 19;
    public static final int WIA1_IT_MSG_DATA_HEADER = 1;
    public static final int WIA1_IT_MSG_DATA = 2;
    public static final int WIA1_IT_MSG_STATUS = 3;
    public static final int WIA1_IT_MSG_TERMINATION = 4;
    public static final int WIA1_IT_MSG_NEW_PAGE = 5;
    public static final int WIA1_IT_MSG_FILE_PREVIEW_DATA = 6;
    public static final int WIA1_IT_MSG_FILE_PREVIEW_DATA_HEADER = 7;
    public static final int WIA_TRANSFER_MSG_STATUS = 1;
    public static final int WIA_TRANSFER_MSG_END_OF_STREAM = 2;
    public static final int WIA_TRANSFER_MSG_END_OF_TRANSFER = 3;
    public static final int WIA_TRANSFER_MSG_DEVICE_STATUS = 5;
    public static final int WIA_TRANSFER_MSG_NEW_PAGE = 6;
    public static final int WIA_PROP_READ = 1;
    public static final int WIA_PROP_WRITE = 2;
    public static final int WIA_PROP_SYNC_REQUIRED = 4;
    public static final int WIA_PROP_NONE = 8;
    public static final int WIA_PROP_RANGE = 16;
    public static final int WIA_PROP_LIST = 32;
    public static final int WIA_PROP_FLAG = 64;
    public static final int WIA_PROP_CACHEABLE = 0x10000;
    public static final int WIA_RANGE_MIN = 0;
    public static final int WIA_RANGE_NOM = 1;
    public static final int WIA_RANGE_MAX = 2;
    public static final int WIA_RANGE_STEP = 3;
    public static final int WIA_RANGE_NUM_ELEMS = 4;
    public static final int WIA_LIST_COUNT = 0;
    public static final int WIA_LIST_NOM = 1;
    public static final int WIA_LIST_VALUES = 2;
    public static final int WIA_LIST_NUM_ELEMS = 2;
    public static final int WIA_FLAG_NOM = 0;
    public static final int WIA_FLAG_VALUES = 1;
    public static final int WIA_FLAG_NUM_ELEMS = 2;
    public static final int WIA_IPS_XRES = 6147;
    public static final int WIA_IPS_YRES = 6148;
    public static final int WIA_IPA_DATATYPE = 4103;
    public static final int WIA_IPA_DEPTH = 4104;
    public static final int WIA_IPS_XPOS = 6149;
    public static final int WIA_IPS_YPOS = 6150;
    public static final int WIA_IPS_XEXTENT = 6151;
    public static final int WIA_IPS_YEXTENT = 6152;
    public static final int WIA_IPS_PAGE_SIZE = 3097;
    public static final int WIA_IPS_BRIGHTNESS = 6154;
    public static final int WIA_IPS_CONTRAST = 6155;
}
