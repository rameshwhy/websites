/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.beans.piv.SecurityBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class verifySecudataSignature {

    SendCommand sendCard = new SendCommand();
    PivUtilInterface Putil = new PivUtil();
    CommonUtil util = new CommonUtil();
    VerifyDigitalSignature verify = new VerifyDigitalSignature();
    ParseAsmObject pso = new ParseAsmObject();
    SecurityBean secBean = new SecurityBean();
    ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();
    ExceptionMessages expMsg = new ExceptionMessages();
    ManageCHUID chuid = new ManageCHUID();
    String SHASignatureAlgo;
    String SHA_AlgoOID;
    String SHA_Algo;
    // This is OID for PIV-CHUIDSecurity Object.
    String RSAContentType = "2a864886f70d010903";
    String LDSOID = "06052b1b010101";
    boolean lb_signature = true;
    private String str_hexChuid;
    private String str_hexPrinted;
    private String str_hexFacial;
    private String str_hexFinger;
    private String str_hexSec;
    private String str_hashGrps;
    private int grp_count;

    public verifySecudataSignature() {
        super();
    }

    public SecurityBean readSecContainer(CardChannel channel) throws MiddleWareException{
        byte[] res = null;
        String returnValue;
        String errorMsg;
        SecurityBean sec = null;

        StringBuffer sb = new StringBuffer();
        byte[] apdu_sec = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05, 0x5C,
            0x03, (byte) 0x5F, (byte) 0xC1, 0x06}; //

        try {
            CommandAPDU SEC_APDU = new CommandAPDU(apdu_sec);

            res = sendCard.sendCommand(channel, SEC_APDU, sb);

            //raise error if unable to Read CHUID
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "SecurityContainer");
                throw new MiddleWareException(errorMsg);
            }
            if (res == null || res.length == 0) {
            } else {
                //convert byte array to string & remove parity bits
                returnValue = util.arrayToHex(res);
                System.out.println(returnValue);

                if (returnValue.substring(2, 4).equals("82")) {
                    returnValue = returnValue.substring(8);
                } else if (returnValue.substring(2, 4).equals("81")) {
                    returnValue = returnValue.substring(6);
                } else {
                    returnValue = returnValue.substring(4);
                }


                //get CHUID
                String str_CHUID = chuid.getChuid(channel);
                String str_PrintedInfo = mngPrintedInfo.getPrintedinfo(channel);
                String str_FacialInfo = Putil.getFacialinfo(channel);
                try {
                    //Verify Signature and hash values of various containers
                    secBean = verifySig(returnValue, str_CHUID, str_PrintedInfo, str_FacialInfo, "");
                } catch (IOException ex) {
                    throw new MiddleWareException(ex.getMessage());
                }
                
            }
            return secBean;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    private SecurityBean verifySig(String str_hex_sec, String str_hex_chuid, String str_hex_Printed,
            String str_hex_Facial, String str_hex_Finger) throws IOException, MiddleWareException {

        PublicKey pubkey;
        byte[] signature;
        byte[] signed_data = null;
        boolean lb_verify = true;

        str_hexSec = str_hex_sec;
        str_hexChuid = str_hex_chuid;
        str_hexPrinted = str_hex_Printed;
        str_hexFacial = str_hex_Facial;
        str_hexFinger = str_hex_Finger;

        signature = getSignature();

        if (signature == null) {
            lb_signature = false;
            lb_verify = false;
        }


        // get Public Key.
        pubkey = pso.getPublicKey(str_hex_chuid);

        if (pubkey == null) {
            lb_verify = false;

        }

        //get the actual data that was signed
        if (lb_verify) {
            signed_data = getSignedData();
        }


        if (lb_verify) {
            
            lb_verify = verify.VerifySignature(signed_data, signature, pubkey, SHASignatureAlgo);
            
        }

        secBean.setLb_sigVerify(lb_verify);

        verifyContainerHashes();

        return secBean;

    }

    private byte[] getSignedData() {
        int i_startRSA, i_startcontent;
        String any_tag = "a0";
        String str_content_len, str_content, str_hextemp;
        int contentLen, i_start;

        i_startRSA = str_hexSec.toLowerCase().indexOf(RSAContentType.toLowerCase());
        i_startcontent = str_hexSec.toLowerCase().lastIndexOf(any_tag, i_startRSA);

        str_hextemp = str_hexSec.substring(i_startcontent);

        if (str_hextemp.substring(2, 4).equals("82")) {
            str_content_len = str_hextemp.substring(4, 8);
            i_start = 8;
        } else if (str_hextemp.substring(2, 4).equals("81")) {
            str_content_len = str_hextemp.substring(4, 6);
            i_start = 6;
        } else {
            str_content_len = str_hextemp.substring(2, 4);
            i_start = 4;
        }
        contentLen = Integer.valueOf(str_content_len, 16).intValue();
        str_content = str_hextemp.substring(2, i_start + contentLen * 2);
        str_content = "31" + str_content;

        return util.hex1ToByteArray(str_content);

    }

    private byte[] getSignature() {

        int i_startsig = 0, sigLen;
        // String octet_tag = "04";
        String str_sig;
        byte[] sig_encoded;
        String strNullSeprator = "05";
        boolean loopVar = true;
        int i = 0;
        String sig_length;

        while (loopVar == true) {
            //Find End of "SHA512WithRSA" OID and add 1 to get to start of Certificate
            //i_startsig = util.findEndLocation(str_hex_chuid, SHAWithRSASIG[i]);
            i_startsig = util.findFromEnd(str_hexSec, UcmsMiddlewareConstants.SHAWithSIG[i]);

            if (i_startsig > 0 || i == UcmsMiddlewareConstants.SHAWithSIG.length - 1) {
                SHASignatureAlgo = UcmsMiddlewareConstants.SHAWithSIG[i];
                loopVar = false;
            }
            i++;
        }

        if (i_startsig == 0) {

            sig_encoded = null;
        } else {


            if (str_hexSec.substring(i_startsig, i_startsig + 2).equalsIgnoreCase(strNullSeprator)) {
                sig_length = str_hexSec.substring(i_startsig + 8, i_startsig + 12);
                i_startsig = i_startsig + 4;
            } else {
                sig_length = str_hexSec.substring(i_startsig + 4, i_startsig + 8);
            }

            sigLen = Integer.valueOf(sig_length, 16).intValue();

            str_sig = str_hexSec.substring(i_startsig + 8, i_startsig + 8 + sigLen * 2);
            sig_encoded = util.hex1ToByteArray(str_sig);

        }


        return sig_encoded;
    }

    private void verifyContainerHashes() throws IOException, MiddleWareException {

        //Determine Hash algo.
        getHashAlgo();
        // Get Hash Groups
        getHashGroups();

        // First byte will be tag 'BA' ..2nd byte is length byte.
        String str_grp_len = str_hexSec.substring(2, 4);
        int grp_len = Integer.valueOf(str_grp_len, 16).intValue();

        String str_grp = str_hexSec.substring(4, 4 + grp_len * 2);


        for (int i = 0; i < str_grp.length(); i = i + 6) {

            //Store group count to be used in later functions.
            grp_count = grp_len / 3; // Each group consists of 3 bytes.

            String grp_nbr;
            String Str_cont_id;


            grp_nbr = str_grp.substring(i, i + 2);
            Str_cont_id = str_grp.substring(i + 2, i + 2 + 4);



            if (Str_cont_id.equals(UcmsMiddlewareConstants.printedContId)) {
                verifyPrintedHash(grp_nbr);

            } else if (Str_cont_id.equals(UcmsMiddlewareConstants.chuidContId)) {
                verifyChuidHash(grp_nbr);

            } else if (Str_cont_id.equals(UcmsMiddlewareConstants.fingerContId)) {

                verifyFingerHash(grp_nbr);


            } else if (Str_cont_id.equals(UcmsMiddlewareConstants.facialContId)) {

                verifyFacialHash(grp_nbr);
            }

        }


    }

    private void verifyChuidHash(String grp_nbr) throws IOException, MiddleWareException {
        boolean lb_chuidVerify;
        byte[] hash_new;
        byte[] hash_original;
        hash_new = getMessageHash(util.hex1ToByteArray(str_hexChuid));

        hash_original = getOriginalHash(grp_nbr);

        if (Arrays.equals(hash_new, hash_original)) {
            lb_chuidVerify = true;

        } else {
            lb_chuidVerify = false;
        }

        secBean.setLb_chuidVerify(lb_chuidVerify);

    }

    private void verifyPrintedHash(String grp_nbr) throws IOException, MiddleWareException {

        boolean lb_printedVerify;
        byte[] hash_new;
        byte[] hash_original;
        hash_new = getMessageHash(util.hex1ToByteArray(str_hexPrinted));

        hash_original = getOriginalHash(grp_nbr);

        if (Arrays.equals(hash_new, hash_original)) {
            lb_printedVerify = true;

        } else {
            lb_printedVerify = false;
        }
        secBean.setLb_printedVerify(lb_printedVerify);

    }

    private void verifyFacialHash(String grp_nbr) throws IOException, MiddleWareException {
        boolean lb_facialVerify;
        byte[] hash_new;
        byte[] hash_original;
        hash_new = getMessageHash(util.hex1ToByteArray(str_hexFacial));

        hash_original = getOriginalHash(grp_nbr);
        if (Arrays.equals(hash_new, hash_original)) {
            lb_facialVerify = true;

        } else {
            lb_facialVerify = false;
        }

        secBean.setLb_facialVerify(lb_facialVerify);
    }

    private void verifyFingerHash(String grp_nbr) throws IOException, MiddleWareException {
        boolean lb_fingerVerify;
        byte[] hash_new;
        byte[] hash_original;
        hash_new = getMessageHash(util.hex1ToByteArray(str_hexFinger));

        hash_original = getOriginalHash(grp_nbr);


        if (Arrays.equals(hash_new, hash_original)) {
            lb_fingerVerify = true;

        } else {
            lb_fingerVerify = false;
        }

        secBean.setLb_fingerVerify(lb_fingerVerify);
    }

    private byte[] getMessageHash(byte[] input_data) throws IOException,MiddleWareException {

        byte[] hash;

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance(SHA_Algo);
        } catch (NoSuchAlgorithmException ex) {
            throw new MiddleWareException(ex.getMessage());
        }
        
        digest.update(input_data);
        hash = digest.digest();

        return hash;

    }

    private void getHashAlgo() {
        int i = 0;
        int i_startPos;
        boolean loopVar = true;
        // First of all find the position of OID of LDS Security Object
        i_startPos = util.findEndLocation(str_hexSec, LDSOID);
        String str_hexSec_1 = str_hexSec.substring(i_startPos);

        while (loopVar == true) {
            i_startPos = util.findEndLocation(str_hexSec_1, UcmsMiddlewareConstants.SHAAlgo[i]);

            if (i_startPos > 0 || i == UcmsMiddlewareConstants.SHAAlgo.length - 1) {
                SHA_AlgoOID = UcmsMiddlewareConstants.SHAAlgo[i];
                loopVar = false;
            }
            i++;
        }

        if(SHA_AlgoOID.equalsIgnoreCase(UcmsMiddlewareConstants.SHA512)){
            SHA_Algo = "SHA-512";
        }else if(SHA_AlgoOID.equalsIgnoreCase(UcmsMiddlewareConstants.SHA256)){
            SHA_Algo = "SHA-256";
        }else if(SHA_AlgoOID.equalsIgnoreCase(UcmsMiddlewareConstants.SHA224)){
            SHA_Algo = "SHA-224";
        }else if(SHA_AlgoOID.equalsIgnoreCase(UcmsMiddlewareConstants.SHA384)){
            SHA_Algo = "SHA-384";
        }else if(SHA_AlgoOID.equalsIgnoreCase(UcmsMiddlewareConstants.SHA1)){
            SHA_Algo = "SHA1";
        }
        /*
        switch (SHA_AlgoOID) {
            case UcmsMiddlewareConstants.SHA512:
                SHA_Algo = "SHA-512";
                break;
            case UcmsMiddlewareConstants.SHA256:
                SHA_Algo = "SHA-256";
                break;
            case UcmsMiddlewareConstants.SHA224:
                SHA_Algo = "SHA-224";
                break;
            case UcmsMiddlewareConstants.SHA384:
                SHA_Algo = "SHA-384";
                break;
            case UcmsMiddlewareConstants.SHA1:
                SHA_Algo = "SHA1";
                break;

        }
         */ 
    }

    private void getHashGroups() {

        String seq_tag = "30";
        String str_grps_len;
        int grp_len, tag_loc, i_startPos;

        i_startPos = util.findEndLocation(str_hexSec, LDSOID);
        String str_hexSec_1 = str_hexSec.substring(i_startPos);

        i_startPos = util.findEndLocation(str_hexSec_1, SHA_AlgoOID);

        str_hexSec_1 = str_hexSec_1.substring(i_startPos);

        tag_loc = util.findtagLocation(0, str_hexSec_1, seq_tag);


        if (tag_loc >= 0) {
            if(str_hexSec_1.substring(tag_loc + 2, tag_loc + 4).equalsIgnoreCase("82")){
                str_grps_len = str_hexSec_1.substring(tag_loc + 4, tag_loc + 8);
                grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                str_hashGrps = str_hexSec_1.substring(tag_loc + 8, tag_loc + 8 + grp_len * 2);
            }else if(str_hexSec_1.substring(tag_loc + 2, tag_loc + 4).equalsIgnoreCase("81")){
               str_grps_len = str_hexSec_1.substring(tag_loc + 4, tag_loc + 6);
               grp_len = Integer.valueOf(str_grps_len, 16).intValue();
               str_hashGrps = str_hexSec_1.substring(tag_loc + 6, tag_loc + 6 + grp_len * 2); 
            } else{
                str_grps_len = str_hexSec_1.substring(tag_loc + 2, tag_loc + 4);
                grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                str_hashGrps = str_hexSec_1.substring(tag_loc + 4, tag_loc + 4 + grp_len * 2);
            }
            /*
            switch (str_hexSec_1.substring(tag_loc + 2, tag_loc + 4)) {
                case "82":
                    str_grps_len = str_hexSec_1.substring(tag_loc + 4, tag_loc + 8);
                    grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                    str_hashGrps = str_hexSec_1.substring(tag_loc + 8, tag_loc + 8 + grp_len * 2);
                    break;
                case "81":
                    str_grps_len = str_hexSec_1.substring(tag_loc + 4, tag_loc + 6);
                    grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                    str_hashGrps = str_hexSec_1.substring(tag_loc + 6, tag_loc + 6 + grp_len * 2);
                    break;
                default:
                    str_grps_len = str_hexSec_1.substring(tag_loc + 2, tag_loc + 4);
                    grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                    str_hashGrps = str_hexSec_1.substring(tag_loc + 4, tag_loc + 4 + grp_len * 2);
                    break;
            }
            */ 
        }

    }

    private byte[] getOriginalHash(String grp_nbr) {

        byte[] hash = null;
        String grp_nbr_extract;
        String str_grp_hash_len;
        String str_grp_hash = null;
        int grp_hash_len;


        for (int i = 0; i < str_hashGrps.length();) {

            // Read the 5th byte for group number.
            grp_nbr_extract = str_hashGrps.substring(i + 8, i + 10);
            // Read the 7th Byte for length of hash bytes for the above grp_number
            str_grp_hash_len = str_hashGrps.substring(i + 12, i + 14);
            grp_hash_len = Integer.valueOf(str_grp_hash_len, 16).intValue();


            if (grp_nbr_extract.equals(grp_nbr)) {
                if (grp_count == 0) {
                    str_grp_hash = str_hashGrps.substring(i + 14);
                } else {
                    str_grp_hash = str_hashGrps.substring(i + 14, i + 14 + grp_hash_len * 2);
                }

                break;
            }

            //If it does not match group nbr, then skip the group and check the next one.
            grp_count--;
            i = i + 14 + grp_hash_len * 2;
        }

        hash = util.hex1ToByteArray(str_grp_hash);
        return hash;

    }
}
