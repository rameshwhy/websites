package com.secuera.middleware.biometrics.applet;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author NSingh
 */
public class BiometricsJAppletHelper {

    /**
     * *****************************************************************************************************************************
     * Static Variables
     * *****************************************************************************************************************************
     */
    
    private static BiometricsJAppletHelper instance = null;
    protected static String arch;
    protected static String os;
    protected static String vendor;
    protected static String userHome;
    protected static String str = null;
    protected static final String tmpStr = "id3Finger.dll"; 
    protected static final String[] scannerDllNames = {"Biothentic.dll", "bsapi.dll", "DCfg.dll", 
        "DFpd.dll", "DFpdUsb.dll", "DImage.dll", "ftrScanAPI.dll", "id3CertisImage.dll", 
        "id3Image.dll", "id3ImageWsq.dll", "LS_USB_Stub.dll", "LS_USB_Stub_100.dll", "LS_USB_Stub_Us.dll", 
        "LScanEssentials.dll", "MORPHO_SDK.dll", "MSO100.dll", "pbict.dll", "UFLicense.dat", "UFScanner.dll", 
        "sgfpamx.dll", "sgfplib.dll", "UFScanner.dll", "USB4XX.dll"};
    
    static {

        arch = System.getProperty("os.arch").toLowerCase();
        os = System.getProperty("os.name").toLowerCase();
        vendor = System.getProperty("java.vm.vendor").toLowerCase();
        
        
        trace("Staring the static variables >>>>>>>>>>>>>>>> ");

        File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));

        trace("Setting Java Library Path >>>>>>>>>>>>>>>> " + TMP_DIR.getAbsolutePath());


        System.setProperty("java.library.path", TMP_DIR.getAbsolutePath());

        try {
            if (os.indexOf("windows") >= 0) {
                if (arch.indexOf("64") >= 0) {

                    trace("Staring the static 64 bit variables >>>>>>>>>>>>>>>> ");
                    str = "id3Finger_64.dll";
                } else {

                    trace("Staring the static 32 bit variables >>>>>>>>>>>>>>>> ");
                    str = "id3Finger_32.dll";
                }
            }


            File localFile = new File(TMP_DIR, tmpStr);

            trace("Local File Name >>>>>>>>>>>>>>>> " + localFile.getName());
            trace("Local File >>>>>>>>>>>>>>>> " + localFile.exists() + " Not found !!!");

            ClassLoader bio = BiometricsJAppletHelper.class.getClassLoader();
            if (!localFile.exists()) {
                trace("Local File >>>>>>>>>>>>>>>> " + localFile.getName() + " Not found !!!");
                InputStream is = bio.getResourceAsStream(str);
               // byte[] bytes = IOUtils.toByteArray(is, str.length());
                //is.close();
                //System.out.println(">>>>>>>>> " + bytes.length);
                
                //FileUtils.writeByteArrayToFile(localFile, bytes, false);
                
                OutputStream out = FileUtils.openOutputStream(localFile);
                
                IOUtils.copy(is, out);
                
                is.close();
                out.close();
                
                trace(localFile.getAbsolutePath() + " loaded...");
            } else {
                trace("Local File >>>>>>>>>>>>>>>> " + localFile.getName() + "  found.. Loading !!!");
            }
            
            for(String dllName : scannerDllNames){
                loadDlls(TMP_DIR, dllName, bio, localFile);
            }
        } catch (Exception localException1) {
            try {
                Object localObject = new File(TMP_DIR + "\\", str);
                trace("Exception >>>>>>>>>>  " + localException1.getLocalizedMessage());
                trace(((File) localObject).getAbsolutePath() + " loaded  1234...");
            } catch (Exception ex) {
                trace(str + " not found...\n");
                ex.printStackTrace();
            }
        }
        
    }

        
    private BiometricsJAppletHelper() {
    }
    
    public static synchronized BiometricsJAppletHelper getInstance() {
        if (instance == null) {
            instance = new BiometricsJAppletHelper();
        }
        return instance;
    }

    private static void loadDlls(File TMP_DIR, String dllName, ClassLoader bio, File localFile) throws IOException {
        File scannerLocalFile = new File(TMP_DIR, dllName);
        if (!scannerLocalFile.exists()) {
             File writeFile = new File(TMP_DIR, dllName);
             writeFile.createNewFile();
             
            trace("Local File >>>>>>>>>>>>>>>> " + scannerLocalFile.getName() + " Not found !!!");
            InputStream is = bio.getResourceAsStream(dllName);
           // byte[] bytes = IOUtils.toByteArray(is);
            //System.out.println(">>>>>>>>> " + bytes.length);
            //FileUtils.writeByteArrayToFile(localFile, bytes);
            //is.close();
            OutputStream out = FileUtils.openOutputStream(writeFile);
                
                IOUtils.copyLarge(is, out);
                IOUtils.closeQuietly(out);
                IOUtils.closeQuietly(is);
                
            trace(writeFile.getAbsolutePath() +" Size of file " + writeFile.length() + " loaded...");
            
            
        } else {
            trace("Local File >>>>>>>>>>>>>>>> " + scannerLocalFile.getName() + "  found.. Loading !!!");
        }
    }


    public void initialiseTheme() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                System.out.println("Theme Names >>>>>>>>  " + info.getName());
                if ("Nimbus".equals(info.getName())) {
                     
                     javax.swing.UIManager.put("ProgressBar.background", Color.BLACK); //colour of the background
                     javax.swing.UIManager.put("ProgressBar.foreground", Color.RED); //colour of progress bar
                     javax.swing.UIManager.put("ProgressBar.selectionBackground",Color.YELLOW); //colour of percentage counter on black background
                     javax.swing.UIManager.put("ProgressBar.selectionForeground",Color.BLUE); //colour of precentage counter on red background
                     javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    
                    break;
                }
            }
            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BiometricsJApplet.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
   
          
    
    public static void trace(String paramString) {
        System.out.println("INFO: " + paramString);
    }
    
}
