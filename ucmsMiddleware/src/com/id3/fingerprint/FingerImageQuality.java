package com.id3.fingerprint;

public class FingerImageQuality
{
  public static final int Low = 20;
  public static final int Fair = 40;
  public static final int Good = 60;
  public static final int VeryGood = 80;
  public static final int Excellent = 100;
  public static final int NotReported = 254;
  public static final int FailedToCompute = 255;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerImageQuality
 * JD-Core Version:    0.6.2
 */