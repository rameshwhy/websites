package com.secuera.middleware.cardreader.beans;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 *
 * @author Harpreet Singh
 */
public class PKIBean {

    PublicKey publicKey;
    PrivateKey privateKey;
    byte[] contentCertificate;

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public byte[] getContentCertificate() {
        return contentCertificate;
    }

    public void setContentCertificate(byte[] contentCertificate) {
        this.contentCertificate = contentCertificate;
    }
}
