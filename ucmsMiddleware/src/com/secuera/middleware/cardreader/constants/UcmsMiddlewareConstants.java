/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.constants;

/**
 *
 * @author SEAdmin
 */
public interface UcmsMiddlewareConstants {

    public static String LOCAL_PIN = "L";
    public static String GLOBAL_PIN = "G";
    public static String PUK_PIN = "P";
    public static String SO_PIN = "S";
    public static String BIO = "B";
    public static int FACIAL_IMAGE_CREATOR_LENGTH = 17;
    //PRINTED INFO
    public static String NAMETAG = "01";
    public static String EMPLOYEETAG = "02";
    public static String EXPDTTAG = "04";
    public static String AGENCYCARDTAG = "05";
    public static String ISSUERTAG = "06";
    public static String ORG1TAG = "07";
    public static String ORG2TAG = "08";
    public static byte[] ZERO_CARD_PAD = {(byte) 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00};
    public static byte[] IV_AES = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    public String CARDUNLOCK = "6283";
    public byte[] PINPAD = {(byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    //Derivation keys
    public byte[] DER_ENC_KEY = {0x01, (byte) 0x82, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    public byte[] DER_MAC_KEY = {0x01, 0x01, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    public String AES128ECB = "08";
    public String TripleDESECB = "03";
    public byte[] TAG9B03 = {(byte) 0x9B, 0x03};
    public byte[] TAG9B08 = {(byte) 0x9B, 0x08};
    public String printedContId = "3001"; //Printed Info container mapped to DG1.
    public String chuidContId = "3000"; //CHUID container mapped to DG2.
    public String fingerContId = "6010"; //Finger Print container mapped to DG2.
    public String facialContId = "6030"; //Facial container mapped to DG4.
    
    
    
    public byte[] RSA_2048 = {0x07};
    public byte[] RSA_1024 = {0x06};
    
    public String bigLength="82";
    public String smallLength="81";
    
    // Standard Algorithims
    public String RSA1024 = "06";
    public String RSA2048 = "07";
    public String ECC224 = "0E";
    public String ECC256 = "11";
    public String ECC384 = "14";
    
    //Key Sizes
    public int RSA1024KeySize=128;
    public int RSA2048KeySize=256;
    public int ECC224KeySize=28; // 28 bytes each for X and Y co-ordinates.
    public int ECC256KeySize=32; // 32 bytes each for X and Y co-ordinates.
    public int ECC384KeySize=48; // 48 bytes each for X and Y co-ordinates.
    public int ECC512KeySize=64; // 64 bytes each for X and Y co-ordinates.
    
    // OID of Hashes
    String SHAAlgo[] = {"608648016503040204","608648016503040201","608648016503040202", "608648016503040203", 
                "2b0e03021a"};
    
    public String SHA224 = "608648016503040204";
    public String SHA256 = "608648016503040201";
    public String SHA384 = "608648016503040202";
    public String SHA512 = "608648016503040203";
    
    
    public String SHA1 = "2b0e03021a";
    
    public int Digest_SHA224_data[] = {2, 16, 840, 1, 101, 3, 4, 2, 4};
    public int Digest_SHA256_data[] = {2, 16, 840, 1, 101, 3, 4, 2, 1};
    public int Digest_SHA384_data[] = {2, 16, 840, 1, 101, 3, 4, 2, 2};
    public int Digest_SHA512_data[] = {2, 16, 840, 1, 101, 3, 4, 2, 3};
    
   
        
    // OID of Signatures
    String SHAWithSIG[] = {"2a864886f70d01010d", "2a864886f70d01010b", "2a864886f70d010101", "06072a8648ce3d0401", "06082a8648ce3d040301",
        "06082a8648ce3d040302", "06082a8648ce3d040303", "06082a8648ce3d040304"};
    
    public String SHA1withRSA = "2a864886f70d010101";
    public String SHA256withRSA = "2a864886f70d01010b";
    public String SHA512withRSA = "2a864886f70d01010d";
    public String sha1WithECDSA = "06072a8648ce3d0401";
    public String sha224WithECDSA = "06082a8648ce3d040301";
    public String sha256WithECDSA = "06082a8648ce3d040302";
    public String sha384WithECDSA = "06082a8648ce3d040303";
    public String sha512WithECDSA = "06082a8648ce3d040304";
   
    public int sha512WithRSAEncryption_data[] = {1, 2, 840, 113549, 1, 1, 13};
    public int sha256WithRSAEncryption_data[] = {1, 2, 840, 113549, 1, 1, 11};
    public int sha1WithECDSA_data[] = {1, 2, 840, 10045, 4, 1};            //06072a8648ce3d0401
    public int sha224WithECDSA_data[] = {1, 2, 840, 10045, 4, 3, 1};		// 06082a8648ce3d040301
    public int sha256WithECDSA_data[] = {1, 2, 840, 10045, 4, 3, 2};		// 06082a8648ce3d040302
    public int sha384WithECDSA_data[] = {1, 2, 840, 10045, 4, 3, 3};		// 06082a8648ce3d040303
    public int sha512WithECDSA_data[] = {1, 2, 840, 10045, 4, 3, 4};		// 06082a8648ce3d040304

    final int SHA1WithRSAEncryption_data[] = {1, 2, 840, 113549, 1, 1, 1};
    
    
    
    
    // Extension Request
    public int Extension_request_data[] = {1, 2, 840, 113549, 1, 9, 14};
    // Key Usage
    public int KeyUsage_data[] = {2, 5, 29, 15};
    // Os Version microsoft CA
    public int os_version_data[] = {1, 3, 6, 1, 4, 1, 311, 13, 2, 3};
    // Email Address
    public int emailAddress_data[] = {1, 2, 840, 113549, 1, 9, 1};
    public int certificateTemplate_data[] = {1, 3, 6, 1, 4, 1, 311, 21, 7};
    // Request Client Info
    public int requestClient_Info_data[] = {1, 3, 6, 1, 4, 1, 311, 21, 20};
    //enrolmentCSP
    public int enrolmentCSP_data[] = {1, 3, 6, 1, 4, 1, 311, 13, 2, 2};
    //Alternate Subject Information
    public int subject_altinfo_data[] = {2, 5, 29, 17};
    public int fascn_data[] = {2, 16, 840, 1, 101, 3, 6, 6};
    public int upn_data[] = {1, 3, 6, 1, 4, 1, 311, 20, 2, 3};
    public String principalName = "2b060104018237140203";
    //private String card_unlock = "6283";
    public byte[] SCP03 = {(byte) 0x2B, 0x06, 0x01, 0x04, 0x01, (byte) 0x81, (byte) 0xEF, 0x6F, 0x02, 0x04, 0x03, 0x15};
    public byte[] SCP01 = {(byte) 0x2A, (byte) 0x86, 0x48, (byte) 0x86, (byte) 0xFC, 0x6B, 0x04, 0x01, 0x05};
    public byte[] piv_auth = {(byte) 0x9A};
    public byte[] card_digi = {(byte) 0x9C};
    public byte[] key_manage = {(byte) 0x9D};
    public byte[] card_auth = {(byte) 0x9E};
    public String pivAuthentication9A = "9A";
    public String digSignature9C = "9C";
    public String keyManagement9D = "9D";
    public String cardAuthentication9E = "9E";
    public String contentCertificate = "CS";
    
    
    
    //finger Pint constants
    public String noInformationGiven = "00";
    public String rightThumb = "05";
    public String rightIndex = "09";
    public String rightMiddle = "0D";
    public String rightRing = "11";
    public String rightLittle = "15";
    public String leftThumb = "06";
    public String leftIndex = "0A";
    public String leftMiddle = "0E";
    public String leftRing = "12";
    public String leftLittle = "16";
    public int facialImageLength = 12704;
    public int certficateLength = 2000;
    byte[] subImpType = {0x00};
    public static String digitalSignature = "digitalSignature";
    public static String nonRepudiation = "nonRepudiation";
    public static String keyEncipherment = "keyEncipherment";
    public static String dataEncipherment = "dataEncipherment";
    public static String keyAgreement = "keyAgreement";
    public static String keyCertSign = "keyCertSign";
    public static String cRLSign = "cRLSign";
    public static String encipherOnly = "encipherOnly";
    public static String decipherOnly = "decipherOnly";
    //finger print status messages
    public static String CaptureStopped = "Capture stopped.";
    public static String PlaceFinger = "Place finger";
    public static String RemoveFinger = "Remove finger";
    public static String RollFinger = "Roll your finger";
    public static String FingerPlaced = "Finger placed";
    public static String FingerRemoved = "Finger removed";
    public static String FingerDownBorder = "Finger significantly displaced to the down border";
    public static String FingerTopBorder = "Finger goes out on the top border";
    public static String FingerRightBorder = "Finger goes out on the right border";
    public static String FingerLeftBorder = "Finger goes out on the left border";
    public static String PressFingerHarder = "Press finger harder";
    public static String SwipeTooFast = "The finger swipe was too fast";
    public static String SwipeTooSlow = "The finger swipe was too slow";
    public static String SwipeTooSkewed = "The finger swipe was too skewed";
    public static String InapSpeed = "The finger was swiped with inapropriate speed";
    public static String ImageSmall = "The captured image is too small";
    public static String PoorQuality = "The captured image is of poor quality";
    public static String FingerLifted = "A part of the finger is lifted";
    public static String RollingFast = "Rolling speed is too fast";
    public static String DirtySensor = "The sensor surface is dirty, or more than one finger is detected";
    public static String FingerShifted = "The rolling finger is shifted";
    public static String RollingTooShort = "Rolling time is too short";
    public static String FewerFinger = "Fewer fingers detected";
    public static String TooManyFinger = "Too many fingers detected";
    public static String WrongHand = "Wrong hand";
    //certtificate extended key usage
    public static String AnyPurpose = "Any Purpose (2.5.29.37.0)";
    public static String ClientAuthentication = "Client Authentication (1.3.6.1.5.5.7.3.2)";
    public static String SecureEmail = "Secure Email (1.3.6.1.5.5.7.3.4)";
    public static String SmartCardLogon = "Smart Card Logon (1.3.6.1.4.1.311.20.2.2)";
    public static String PIVContentSigning = "PIV Content Signing(2.16.840.1.101.3.6.7)";
    //constants for discovery container
    public static String ID5FC107 = "Card Capabilities Container";
    public static String ID5FC102 = "Card Holder Unique Identifier (CHUID)";
    public static String ID5FC105 = "X.509 Certificate for PIV Authentication";
    public static String ID5FC103 = "Card Holder Fingerprints";
    public static String ID5FC106 = "Security Object";
    public static String ID5FC108 = "Card Holder Facial Image";
    public static String ID5FC109 = "Printed Information";
    public static String ID5FC10A = "X.509 Certificate for Digital Signature";
    public static String ID5FC10B = "X.509 Certificate for Key management";
    public static String ID5FC101 = "X.509 Certificate for Card Authentication";
    public static String ID7E = "Discovery Object";
    public static String ID5FC10C = "Key History Object";
    public static String ID5FC10D = "Retired X.509 Certificates for Key Management 1";
    public static String ID5FC10E = "Retired X.509 Certificates for Key Management 2";
    public static String ID5FC10F = "Retired X.509 Certificates for Key Management 3";
    public static String ID5FC110 = "Retired X.509 Certificates for Key Management 4";
    public static String ID5FC111 = "Retired X.509 Certificates for Key Management 5";
    public static String ID5FC112 = "Retired X.509 Certificates for Key Management 6";
    public static String ID5FC113 = "Retired X.509 Certificates for Key Management 7";
    public static String ID5FC114 = "Retired X.509 Certificates for Key Management 8";
    public static String ID5FC115 = "Retired X.509 Certificates for Key Management 9";
    public static String ID5FC116 = "Retired X.509 Certificates for Key Management 10";
    public static String ID5FC117 = "Retired X.509 Certificates for Key Management 11";
    public static String ID5FC118 = "Retired X.509 Certificates for Key Management 12";
    public static String ID5FC119 = "Retired X.509 Certificates for Key Management 13";
    public static String ID5FC11A = "Retired X.509 Certificates for Key Management 14";
    public static String ID5FC11B = "Retired X.509 Certificates for Key Management 15";
    public static String ID5FC11C = "Retired X.509 Certificates for Key Management 16";
    public static String ID5FC11D = "Retired X.509 Certificates for Key Management 17";
    public static String ID5FC11E = "Retired X.509 Certificates for Key Management 18";
    public static String ID5FC11F = "Retired X.509 Certificates for Key Management 19";
    public static String ID5FC120 = "Retired X.509 Certificates for Key Management 20";
    public static String ID5FC121 = "Iris Image container";
}
