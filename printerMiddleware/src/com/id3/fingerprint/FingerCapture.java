 package com.id3.fingerprint;
 
import java.io.File;
import java.security.AccessController;
import java.security.PrivilegedAction;
 import java.util.ArrayList;
 import java.util.List;
 //import javafx.application.Platform;
 import javax.swing.SwingUtilities;
 
 public class FingerCapture
 {
   private long _handle;
   private boolean _disposed = false;
   protected FingerCaptureDevice[] _deviceArray;
   protected ArrayList<FingerCaptureDevice> _devices = new ArrayList();
   //private final boolean _isJavaFx;
   private static ArrayList<FingerCaptureListener> _captureListeners = new ArrayList();
   private static FingerCapture _instance;
 
   static
   {
       String tmpStr = "id3Finger.dll";
       File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
       File localFile = new File(TMP_DIR, tmpStr);
       
    
        System.load(localFile.getAbsolutePath());
   }
 
   public FingerCapture(){
       
   }
   
   public static FingerCapture getInstance(FingerCaptureListener listener)
     throws FingerException
   {
     if (_instance == null)
       _instance = new FingerCapture(listener, true);
     else {
       _captureListeners.add(listener);
     }
     return _instance;
   }
 
   public static synchronized FingerCapture getInstance() throws FingerException
   {
     if (_instance == null) {
       _instance = new FingerCapture();
     }
     return _instance;
   }
 
   public FingerCapture(FingerCaptureListener listener)
     throws FingerException
   {
     //this._isJavaFx = Platform.isFxApplicationThread();
     _captureListeners.add(listener);
 
     long[] handle_ptr = new long[1];
 
     int result = nInitialize(handle_ptr);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     this._handle = handle_ptr[0];
     if (_instance == null)
       _instance = this;
   }
 
   public FingerCapture(FingerCaptureListener listener, int deviceModel)
     throws FingerException
   {
     //this._isJavaFx = Platform.isFxApplicationThread();
     _captureListeners.add(listener);
 
     long[] handle_ptr = new long[1];
 
     int result = nInitializeEx(deviceModel, handle_ptr);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     this._handle = handle_ptr[0];
     if (_instance == null)
       _instance = this;
   }
 
   public FingerCapture(FingerCaptureListener listener, boolean asynchronous)
     throws FingerException
   {
    // this._isJavaFx = (Boolean) AccessController.doPrivilegedWithCombiner(new PrivilegedAction() {
      //              public Object run() {
       //                 boolean isAppThread = Platform.isFxApplicationThread();
      //                  return isAppThread;
      //              }
        
     //   });
     _captureListeners.add(listener);
 
     long[] handle_ptr = new long[1];
     int result;
      
     if (asynchronous)
       result = nBeginInitialize(handle_ptr);
     else {
       result = nInitialize(handle_ptr);
     }
     if (result != 0) {
       throw new FingerException(result);
     }
     this._handle = handle_ptr[0];
     if (_instance == null)
       _instance = this;
   }
 
   public void dispose()
   {
     nDispose(this._handle);
     this._handle = 0L;
     this._disposed = true;
     _captureListeners.clear();
     _instance = null;
   }
 
   protected void finalize()
     throws Throwable
   {
     try
     {
       dispose();
     }
     catch (Exception localException)
     {
     }
     finally
     {
       super.finalize();
     }
   }
 
   public boolean isModeAvailable(int mode)
     throws FingerException
   {
     int[] value = new int[1];
 
     int result = nIsModeAvailable(this._handle, mode, value);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     if (value[0] == 1) {
       return true;
     }
     return false;
   }
 
   public boolean canCapturePosition(int position)
     throws FingerException
   {
     int[] value = new int[1];
 
     int result = nCanCapturePosition(this._handle, position, value);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     if (value[0] == 1) {
       return true;
     }
     return false;
   }
 
   public void start()
     throws FingerException
   {
     int result = nStart(this._handle, 0);
 
     if (result != 0)
       throw new FingerException(result);
   }
 
   public void start(int position)
     throws FingerException
   {
     int result = nStart(this._handle, position);
 
     if (result != 0)
       throw new FingerException(result);
   }
 
   public void stop()
     throws FingerException
   {
     int result = nStop(this._handle);
 
     if (result != 0)
       throw new FingerException(result);
   }
 
   public boolean getAutoProcessing()
     throws FingerException
   {
     int[] value = new int[1];
 
     int result = nGetAutoProcessing(this._handle, value);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     return value[0] != 0;
   }
 
   public void setAutoProcessing(boolean value)
     throws FingerException
   {
     int result = nSetAutoProcessing(this._handle, value ? 1 : 0);
 
     if (result != 0)
       throw new FingerException(result);
   }
 
   public int getDeviceCount()
     throws FingerException
   {
     int[] value_ptr = new int[1];
 
     int result = nGetDeviceCount(this._handle, value_ptr);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     return value_ptr[0];
   }
 
   public FingerCaptureDevice[] getDevices()
     throws FingerException
   {
     if (this._deviceArray == null)
     {
       this._deviceArray = new FingerCaptureDevice[this._devices.size()];
       this._devices.toArray(this._deviceArray);
     }
 
     return this._deviceArray;
   }
 
   public boolean isCapturing()
     throws FingerException
   {
     int[] value_ptr = new int[1];
 
     int result = nIsCapturing(this._handle, value_ptr);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     return value_ptr[0] != 0;
   }
 
   public int getMode()
     throws FingerException
   {
     int[] value = new int[1];
 
     int result = nGetMode(this._handle, value);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     return value[0];
   }
 
   public void setMode(int mode)
     throws FingerException
   {
     int result = nSetMode(this._handle, mode);
 
     if (result != 0)
       throw new FingerException(result);
   }
 
   public int getStatus()
     throws FingerException
   {
     int[] value = new int[1];
 
     int result = nGetStatus(this._handle, value);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     return value[0];
   }
 
   public int getSpoofDetectionMode()
     throws FingerException
   {
     int[] value = new int[1];
 
     int result = nGetSpoofDetectionMode(this._handle, value);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     return value[0];
   }
 
   public void setSpoofDetectionMode(int value)
     throws FingerException
   {
     int result = nSetSpoofDetectionMode(this._handle, value);
 
     if (result != 0)
       throw new FingerException(result);
   }
 
   public boolean isSpoofDetectionAvailable()
     throws FingerException
   {
     int[] value = new int[1];
 
     int result = nIsSpoofDetectionAvailable(this._handle, value);
 
     if (result != 0) {
       throw new FingerException(result);
     }
     if (value[0] == 1) {
       return true;
     }
     return false;
   }
 
   protected FingerCaptureDevice findDeviceFromHandle(List<FingerCaptureDevice> devices, long hDevice)
   {
     for (int i = 0; i < devices.size(); i++)
     {
       FingerCaptureDevice device = (FingerCaptureDevice)devices.get(i);
 
       if (device.getHandle() == hDevice) {
         return device;
       }
     }
     return null;
   }
 
   protected void onDeviceAdded(long sender, long hDevice)
   {
     if (_captureListeners.size() != 0)
     {
       final FingerCaptureDevice device = new FingerCaptureDevice(hDevice);
 
       this._devices.add(device);
 
       this._deviceArray = null;
 
       /*if (this._isJavaFx)
         Platform.runLater(new Runnable()
         {
           public void run()
           {
             for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
               ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).deviceAdded(FingerCapture._instance, device);
           }
         });*/
       //else
         SwingUtilities.invokeLater(new Runnable()
         {
           public void run()
           {
             for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
               ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).deviceAdded(FingerCapture._instance, device);
           }
         });
     }
   }
 
   protected void onDeviceRemoved(long sender, long hDevice)
   {
     if (_captureListeners.size() != 0)
     {
       FingerCaptureDevice device = findDeviceFromHandle(this._devices, hDevice);
 
       this._devices.remove(device);
 
       this._deviceArray = null;
       try
       {
         final String deviceName = device.getName();
 
         /*if (this._isJavaFx)
           Platform.runLater(new Runnable()
           {
             public void run()
             {
               for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
                 ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).deviceRemoved(FingerCapture._instance, deviceName);
             }
           });
         else*/
           SwingUtilities.invokeLater(new Runnable()
           {
             public void run()
             {
               for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
                 ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).deviceRemoved(FingerCapture._instance, deviceName);
             }
           });
       }
       catch (FingerException localFingerException)
       {
       }
     }
   }
 
   protected void onStatusChanged(long sender, long hDevice, int status)
   {
     if (_captureListeners.size() != 0)
     {
       final FingerCaptureDevice device = findDeviceFromHandle(this._devices, hDevice);
       final int captureStatus = status;
 
      /* if (this._isJavaFx)
         Platform.runLater(new Runnable()
         {
           public void run()
           {
             for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
               ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).statusChanged(FingerCapture._instance, device, captureStatus);
           }
         });
       else*/
         SwingUtilities.invokeLater(new Runnable()
         {
           public void run()
           {
             for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
               ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).statusChanged(FingerCapture._instance, device, captureStatus);
           }
         });
     }
   }
 
   protected void onImagePreview(long sender, long hDevice, long hImage)
   {
     if (_captureListeners.size() != 0)
     {
       try
       {
         final FingerCaptureDevice device = findDeviceFromHandle(this._devices, hDevice);
         final FingerImage image = hImage != 0L ? new FingerImage(hImage, false) : null;
 
        /*if (this._isJavaFx)
           Platform.runLater(new Runnable()
           {
             public void run()
             {
               for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
                 ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).imagePreview(FingerCapture._instance, device, image);
             }
           });
         else*/
           SwingUtilities.invokeLater(new Runnable()
           {
             public void run()
             {
               for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
                 ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).imagePreview(FingerCapture._instance, device, image);
             }
           });
       }
       catch (FingerException localFingerException)
       {
       }
     }
   }
 
   protected void onImageCaptured(long sender, long hDevice, long hImage)
   {
     if (_captureListeners.size() != 0)
     {
       try
       {
         final FingerCaptureDevice device = findDeviceFromHandle(this._devices, hDevice);
         FingerImage image = hImage != 0L ? new FingerImage(hImage, false) : null;
         final FingerImage finalImage = image != null ? image.clone() : null;
 
         /*if (this._isJavaFx)
           Platform.runLater(new Runnable()
           {
             public void run()
             {
               for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
                 ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).imageCaptured(FingerCapture._instance, device, finalImage);
             }
           });
         else*/
           SwingUtilities.invokeLater(new Runnable()
           {
             public void run()
             {
               for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
                 ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).imageCaptured(FingerCapture._instance, device, finalImage);
             }
           });
       }
       catch (FingerException localFingerException)
       {
       }
     }
   }
 
   private void onImageProcessed(long sender, long hDevice, long hImage)
   {
     if (_captureListeners.size() != 0)
     {
       try
       {
         final FingerCaptureDevice device = findDeviceFromHandle(this._devices, hDevice);
         FingerImage image = hImage != 0L ? new FingerImage(hImage, false) : null;
         final FingerImage finalImage = image != null ? image.clone() : null;
 
        /* if (this._isJavaFx)
           Platform.runLater(new Runnable()
           {
             public void run()
             {
               for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
                 ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).imageProcessed(FingerCapture._instance, device, finalImage);
             }
           });
         else*/
           SwingUtilities.invokeLater(new Runnable()
           {
             public void run()
             {
               for (int i = 0; i < FingerCapture._captureListeners.size(); i++)
                 ((FingerCaptureListener)FingerCapture._captureListeners.get(i)).imageProcessed(FingerCapture._instance, device, finalImage);
             }
           });
       }
       catch (FingerException localFingerException)
       {
       }
     }
   }
 
   private final native int nInitialize(long[] paramArrayOfLong);
 
   private final native int nInitializeEx(int paramInt, long[] paramArrayOfLong);
 
   private final native int nBeginInitialize(long[] paramArrayOfLong);
 
   private final native int nDispose(long paramLong);
 
   private final native int nIsModeAvailable(long paramLong, int paramInt, int[] paramArrayOfInt);
 
   private final native int nCanCapturePosition(long paramLong, int paramInt, int[] paramArrayOfInt);
 
   private final native int nStart(long paramLong, int paramInt);
 
   private final native int nStop(long paramLong);
 
   private final native int nGetAutoProcessing(long paramLong, int[] paramArrayOfInt);
 
   private final native int nSetAutoProcessing(long paramLong, int paramInt);
 
   private final native int nGetDeviceCount(long paramLong, int[] paramArrayOfInt);
 
   private final native int nIsCapturing(long paramLong, int[] paramArrayOfInt);
 
   private final native int nGetStatus(long paramLong, int[] paramArrayOfInt);
 
   private final native int nGetMode(long paramLong, int[] paramArrayOfInt);
 
   private final native int nSetMode(long paramLong, int paramInt);
 
   private final native int nGetSpoofDetectionMode(long paramLong, int[] paramArrayOfInt);
 
   private final native int nSetSpoofDetectionMode(long paramLong, int paramInt);
 
   private final native int nIsSpoofDetectionAvailable(long paramLong, int[] paramArrayOfInt);
 }
