package com.secuera.middleware.cardreader.entities;

import java.util.Date;

public class chuid {
	fascn fascN;
	String organizationID;
	String duns;
	String guid;
	Date expirationDate;
	
	public fascn getFascN() {
		return fascN;
	}
	public void setFascN(fascn fascN) {
		this.fascN = fascN;
	}
	public String getOrganizationID() {
		return organizationID;
	}
	public void setOrganizationID(String organizationID) {
		this.organizationID = organizationID;
	}
	public String getDuns() {
		return duns;
	}
	public void setDuns(String duns) {
		this.duns = duns;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
}
