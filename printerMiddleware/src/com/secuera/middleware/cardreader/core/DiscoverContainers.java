/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.ContainerInfoBean;
import com.secuera.middleware.cardreader.beans.PIVObjectsBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.util.ArrayList;
import java.util.List;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class DiscoverContainers {

    private CommonUtil util = new CommonUtil();
    private SendCommand sendCard = new SendCommand();
    private GeneralAuth auth = new GeneralAuth();

    public DiscoverContainers() {
        super();
    }

    public ContainerInfoBean getContainerInfo(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY)
            throws MiddleWareException {

        // Id---> Byte 0 Byte 1 Byte 2
        // Size ---> Byte 3 Byte 4
        // Access Contact Read ---> Byte 5
        // Access Contact Update ---> Byte 6
        // Access Contactless Read ---> Byte 7
        // Access Contactless Update ---> Byte 8


        /*  
         * Access Condition Access condition Byte
         * ALW                      ‘53’
         * NEV                      ‘B2’
         * PIN                      ‘18’
         * PIN ^ BIO                ‘3D’
         * PIN ALWAYS               ‘05’
         * PIV_ADM                  ‘35’
         * MUTUAL_AUTH              ‘29’
         * PIN & BIO                ‘8F’
         * PIN OR BIO ALWAYS        ‘19’
         */


        StringBuffer sb = new StringBuffer();
        byte[] res = null;

        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }

        // Command to read discovery container
        byte[] command_data = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x04,
            0x5C, 0x02, 0x3F, (byte) 0xF6};


        System.out.println("1st command" + util.arrayToHex(command_data));
        //prepare APDU command for authorisation
        CommandAPDU DISCOVER_CONT_APDU = new CommandAPDU(command_data);



        try {

            res = sendCard.sendCommand(channel, DISCOVER_CONT_APDU, sb);
            if (!sb.toString().equals("9000")) {
                throw new MiddleWareException("Error reading Discovery Container. Error Code = " + sb);
            }

        } catch (CardException e1) {
            throw new MiddleWareException("Card Exception.");
        }

        System.out.println("Result: " + util.arrayToHex(res));

        //ID >> 5fc107
        //Size >> 0137
        //Access Contact Read >> 53
        //Access Contact Update >> 35
        //Access Contactless Read >> b2
        //Access Contactless Update >> b2
        //5fc1020b57533553b25fc10507f75335b2b25fc1030fb73d35b2b25fc10604375335b2b25fc10831b73d35b2b25fc10900973d35b2b25fc10a07f75335b2b25fc10b07f75335b2b25fc10107f7533553b200007e0037533553b25fc10c00975335b2b25fc10d07f75335b2b25fc10e07f75335b2b25fc10f07f75335b2b25fc11007f75335b2b25fc11107f75335b2b25fc1211bd73d35b2b2



        String returnValue = util.arrayToHex(res);

        if (returnValue.substring(2, 4).equals("82")) {
            returnValue = returnValue.substring(8);
        } else if (returnValue.substring(2, 4).equals("81")) {
            returnValue = returnValue.substring(6);
        } else {
            returnValue = returnValue.substring(4);
        }


        ContainerInfoBean containerInfo = new ContainerInfoBean();
        List myList = new ArrayList();

        while (returnValue.length() > 0) {
            PIVObjectsBean PIVObjects = new PIVObjectsBean();
            if (returnValue.substring(0, 2).equalsIgnoreCase("7E")) {
                PIVObjects.setPIVObjectID(returnValue.substring(0, 2));
                PIVObjects.setPIVObjectName(getContainerName(returnValue.substring(0, 2)));
                PIVObjects.setDataFormat("ISO");
                PIVObjects.setContainerSize(Integer.valueOf(returnValue.substring(2, 6), 16).intValue());
                PIVObjects.setContactRead(getAccessType(returnValue.substring(6, 8)));
                PIVObjects.setContactUpdate(getAccessType(returnValue.substring(8, 10)));
                PIVObjects.setContactLessRead(getAccessType(returnValue.substring(10, 12)));
                PIVObjects.setContactLessUpdate(getAccessType(returnValue.substring(12, 14)));
                System.out.println(returnValue);
                returnValue = returnValue.substring(14);

            } else {
                PIVObjects.setPIVObjectID(returnValue.substring(0, 6));
                PIVObjects.setPIVObjectName(getContainerName(returnValue.substring(0, 6)));
                PIVObjects.setDataFormat("NIST");
                PIVObjects.setContainerSize(Integer.valueOf(returnValue.substring(6, 10), 16).intValue());
                PIVObjects.setContactRead(getAccessType(returnValue.substring(10, 12)));
                PIVObjects.setContactUpdate(getAccessType(returnValue.substring(12, 14)));
                PIVObjects.setContactLessRead(getAccessType(returnValue.substring(14, 16)));
                PIVObjects.setContactLessUpdate(getAccessType(returnValue.substring(16, 18)));
                System.out.println(returnValue);
                returnValue = returnValue.substring(18);
                System.out.println(returnValue);
            }
            System.out.println(PIVObjects.toString());
            myList.add(PIVObjects);
        }

        containerInfo.setPIVObjectsBean(myList);




        return containerInfo;

    }

    private static String getContainerName(String containerID) {
        // show the list of available terminals
        String containerName = null;
        if (containerID.equalsIgnoreCase("5FC107")) {
            containerName = "Card Capabilities Container";
        } else if (containerID.equalsIgnoreCase("5FC102")) {
            containerName = "Card Holder Unique Identifier (CHUID)";
        } else if (containerID.equalsIgnoreCase("5FC105")) {
            containerName = "X.509 Certificate for PIV Authentication";
        } else if (containerID.equalsIgnoreCase("5FC103")) {
            containerName = "Card Holder Fingerprints";
        } else if (containerID.equalsIgnoreCase("5FC106")) {
            containerName = "Security Object";
        } else if (containerID.equalsIgnoreCase("5FC108")) {
            containerName = "Card Holder Facial Image";
        } else if (containerID.equalsIgnoreCase("5FC109")) {
            containerName = "Printed Information";
        } else if (containerID.equalsIgnoreCase("5FC10A")) {
            containerName = "X.509 Certificate for Digital Signature";
        } else if (containerID.equalsIgnoreCase("5FC10B")) {
            containerName = "X.509 Certificate for Key management";
        } else if (containerID.equalsIgnoreCase("5FC101")) {
            containerName = "X.509 Certificate for Card Authentication";
        } else if (containerID.equalsIgnoreCase("7E")) {
            containerName = "Discovery Object";
        } else if (containerID.equalsIgnoreCase("5FC10C")) {
            containerName = "Key History Object";
        } else if (containerID.equalsIgnoreCase("5FC10D")) {
            containerName = "Retired X.509 Certificates for Key Management 1";
        } else if (containerID.equalsIgnoreCase("5FC10E")) {
            containerName = "Retired X.509 Certificates for Key Management 2";
        } else if (containerID.equalsIgnoreCase("5FC10F")) {
            containerName = "Retired X.509 Certificates for Key Management 3";
        } else if (containerID.equalsIgnoreCase("5FC110")) {
            containerName = "Retired X.509 Certificates for Key Management 4";
        } else if (containerID.equalsIgnoreCase("5FC111")) {
            containerName = "Retired X.509 Certificates for Key Management 5";
        } else if (containerID.equalsIgnoreCase("5FC112")) {
            containerName = "Retired X.509 Certificates for Key Management 6";
        } else if (containerID.equalsIgnoreCase("5FC113")) {
            containerName = "Retired X.509 Certificates for Key Management 7";
        } else if (containerID.equalsIgnoreCase("5FC114")) {
            containerName = "Retired X.509 Certificates for Key Management 8";
        } else if (containerID.equalsIgnoreCase("5FC115")) {
            containerName = "Retired X.509 Certificates for Key Management 9";
        } else if (containerID.equalsIgnoreCase("5FC116")) {
            containerName = "Retired X.509 Certificates for Key Management 10";
        } else if (containerID.equalsIgnoreCase("5FC117")) {
            containerName = "Retired X.509 Certificates for Key Management 11";
        } else if (containerID.equalsIgnoreCase("5FC118")) {
            containerName = "Retired X.509 Certificates for Key Management 12";
        } else if (containerID.equalsIgnoreCase("5FC119")) {
            containerName = "Retired X.509 Certificates for Key Management 13";
        } else if (containerID.equalsIgnoreCase("5FC11A")) {
            containerName = "Retired X.509 Certificates for Key Management 14";
        } else if (containerID.equalsIgnoreCase("5FC11B")) {
            containerName = "Retired X.509 Certificates for Key Management 15";
        } else if (containerID.equalsIgnoreCase("5FC11C")) {
            containerName = "Retired X.509 Certificates for Key Management 16";
        } else if (containerID.equalsIgnoreCase("5FC11D")) {
            containerName = "Retired X.509 Certificates for Key Management 17";
        } else if (containerID.equalsIgnoreCase("5FC11F")) {
            containerName = "Retired X.509 Certificates for Key Management 18";
        } else if (containerID.equalsIgnoreCase("5FC11F")) {
            containerName = "Retired X.509 Certificates for Key Management 19";
        } else if (containerID.equalsIgnoreCase("5FC120")) {
            containerName = "Retired X.509 Certificates for Key Management 20";
        } else if (containerID.equalsIgnoreCase("5FC121")) {
            containerName = "Iris Image container	";
        }



        return containerName;
    }

    private static String getAccessType(String accessTypeID) {
        // show the list of available terminals
        String accessType = null;

        if (accessTypeID.equalsIgnoreCase("53")) {
            accessType = "ALW";
        } else if (accessTypeID.equalsIgnoreCase("B2")) {
            accessType = "NEV";
        } else if (accessTypeID.equalsIgnoreCase("18")) {
            accessType = "PIN";
        } else if (accessTypeID.equalsIgnoreCase("3D")) {
            accessType = "PIN ^ BIO";
        } else if (accessTypeID.equalsIgnoreCase("05")) {
            accessType = "PIN ALWAYS";
        } else if (accessTypeID.equalsIgnoreCase("35")) {
            accessType = "PIV_ADM";
        } else if (accessTypeID.equalsIgnoreCase("29")) {
            accessType = "MUTUAL_AUTH";
        } else if (accessTypeID.equalsIgnoreCase("8F")) {
            accessType = "PIN & BIO";
        } else if (accessTypeID.equalsIgnoreCase("19")) {
            accessType = "PIN OR BIO ALWAYS";
        }
        return accessType;

    }
}
