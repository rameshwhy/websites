/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.io.Serializable;

/**
 *
 * @author Satish K
 */
public class FingerPrintBean implements Serializable {
    private boolean rightThumb;
    private boolean rightIndex;
    private boolean rightMiddle;
    private boolean rightRing;
    private boolean rightLittle;
    private boolean leftThumb;
    private boolean leftIndex;
    private boolean leftMiddle;
    private boolean leftRing;
    private boolean leftLittle;

    public boolean isLeftIndex() {
        return leftIndex;
    }

    public void setLeftIndex(boolean leftIndex) {
        this.leftIndex = leftIndex;
    }

    public boolean isLeftLittle() {
        return leftLittle;
    }

    public void setLeftLittle(boolean leftLittle) {
        this.leftLittle = leftLittle;
    }

    public boolean isLeftMiddle() {
        return leftMiddle;
    }

    public void setLeftMiddle(boolean leftMiddle) {
        this.leftMiddle = leftMiddle;
    }

    public boolean isLeftRing() {
        return leftRing;
    }

    public void setLeftRing(boolean leftRing) {
        this.leftRing = leftRing;
    }

    public boolean isLeftThumb() {
        return leftThumb;
    }

    public void setLeftThumb(boolean leftThumb) {
        this.leftThumb = leftThumb;
    }

    public boolean isRightIndex() {
        return rightIndex;
    }

    public void setRightIndex(boolean rightIndex) {
        this.rightIndex = rightIndex;
    }

    public boolean isRightLittle() {
        return rightLittle;
    }

    public void setRightLittle(boolean rightLittle) {
        this.rightLittle = rightLittle;
    }

    public boolean isRightMiddle() {
        return rightMiddle;
    }

    public void setRightMiddle(boolean rightMiddle) {
        this.rightMiddle = rightMiddle;
    }

    public boolean isRightRing() {
        return rightRing;
    }

    public void setRightRing(boolean rightRing) {
        this.rightRing = rightRing;
    }

    public boolean isRightThumb() {
        return rightThumb;
    }

    public void setRightThumb(boolean rightThumb) {
        this.rightThumb = rightThumb;
    }

    
}
