/*     */ package com.id3.fingerprint;

import java.io.File;

/*     */ 
/*     */ public class FingerEnroll
/*     */ {
/*  15 */   private boolean _disposed = false;
/*  16 */   private long _handle = 0L;
/*     */ 
/*     */   static
/*     */   {
/* 286 */      String tmpStr = "id3Finger.dll";
                File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
                File localFile = new File(TMP_DIR, tmpStr);


                 System.load(localFile.getAbsolutePath());
/*     */   }
/*     */ 
/*     */   public FingerEnroll()
/*     */     throws FingerException
/*     */   {
/*  25 */     long[] handle_ptr = new long[1];
/*  26 */     int result = nInitialize(handle_ptr);
/*     */ 
/*  28 */     if (result != 0) {
/*  29 */       throw new FingerException(result);
/*     */     }
/*  31 */     this._handle = handle_ptr[0];
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/*  39 */     nDispose(this._handle);
/*  40 */     this._handle = 0L;
/*  41 */     this._disposed = true;
/*     */   }
/*     */ 
/*     */   protected void finalize()
/*     */     throws Throwable
/*     */   {
/*     */     try
/*     */     {
/*  51 */       dispose();
/*     */     }
/*     */     catch (Exception localException)
/*     */     {
/*     */     }
/*     */     finally
/*     */     {
/*  58 */       super.finalize();
/*     */     }
/*     */   }
/*     */ 
/*     */   public int getQuality()
/*     */     throws FingerException
/*     */   {
/*  69 */     int[] quality = new int[1];
/*  70 */     int result = nGetQuality(this._handle, quality);
/*     */ 
/*  72 */     if (result != 0) {
/*  73 */       throw new FingerException(result);
/*     */     }
/*  75 */     return quality[0];
/*     */   }
/*     */ 
/*     */   public int getQuality(int position)
/*     */     throws FingerException
/*     */   {
/*  86 */     int[] quality = new int[1];
/*  87 */     int result = nGetFingerQuality(this._handle, position, quality);
/*     */ 
/*  89 */     if (result != 0) {
/*  90 */       throw new FingerException(result);
/*     */     }
/*  92 */     return quality[0];
/*     */   }
/*     */ 
/*     */   public int addImage(FingerImage image)
/*     */     throws FingerException
/*     */   {
/* 103 */     int[] status = new int[1];
/* 104 */     int result = nAddImage(this._handle, image.getHandle(), status);
/*     */ 
/* 106 */     if (result != 0) {
/* 107 */       throw new FingerException(result);
/*     */     }
/* 109 */     return status[0];
/*     */   }
/*     */ 
/*     */   public FingerTemplateRecord createTemplateRecord(int maxTemplatesPerFinger)
/*     */     throws FingerException
/*     */   {
/* 120 */     long[] hTemplateRecord = new long[1];
/*     */ 
/* 122 */     int result = nCreateTemplateRecord(this._handle, maxTemplatesPerFinger, hTemplateRecord);
/*     */ 
/* 124 */     if (result != 0) {
/* 125 */       throw new FingerException(result);
/*     */     }
/* 127 */     return new FingerTemplateRecord(hTemplateRecord[0], true);
/*     */   }
/*     */ 
/*     */   public FingerImageRecord createImageRecord(int maxImagesPerFinger)
/*     */     throws FingerException
/*     */   {
/* 138 */     long[] hImageRecord = new long[1];
/*     */ 
/* 140 */     int result = nCreateImageRecord(this._handle, maxImagesPerFinger, hImageRecord);
/*     */ 
/* 142 */     if (result != 0) {
/* 143 */       throw new FingerException(result);
/*     */     }
/* 145 */     return new FingerImageRecord(hImageRecord[0], true);
/*     */   }
/*     */ 
/*     */   public void removePosition(int position)
/*     */     throws FingerException
/*     */   {
/* 155 */     int result = nRemovePosition(this._handle, position);
/*     */ 
/* 157 */     if (result != 0)
/* 158 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public void changePosition(int oldPosition, int newPosition)
/*     */     throws FingerException
/*     */   {
/* 169 */     int result = nChangePosition(this._handle, oldPosition, newPosition);
/*     */ 
/* 171 */     if (result != 0)
/* 172 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public FingerImage getBestImage(int position)
/*     */     throws FingerException
/*     */   {
/* 183 */     long[] hImage = new long[1];
/* 184 */     int result = nGetBestImage(this._handle, position, hImage);
/*     */ 
/* 186 */     if (result != 0) {
/* 187 */       throw new FingerException(result);
/*     */     }
/* 189 */     if (hImage[0] != 0L) {
/* 190 */       return new FingerImage(hImage[0], false);
/*     */     }
/* 192 */     return null;
/*     */   }
/*     */ 
/*     */   public int getBestFinger()
/*     */     throws FingerException
/*     */   {
/* 202 */     int[] position = new int[1];
/* 203 */     int result = nGetBestFinger(this._handle, position);
/*     */ 
/* 205 */     if (result != 0) {
/* 206 */       throw new FingerException(result);
/*     */     }
/* 208 */     return position[0];
/*     */   }
/*     */ 
/*     */   public int getFingerCount()
/*     */     throws FingerException
/*     */   {
/* 218 */     int[] value = new int[1];
/* 219 */     int result = nGetFingerCount(this._handle, value);
/*     */ 
/* 221 */     if (result != 0) {
/* 222 */       throw new FingerException(result);
/*     */     }
/* 224 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getImageQualityThreshold()
/*     */     throws FingerException
/*     */   {
/* 234 */     int[] value = new int[1];
/* 235 */     int result = nGetImageQualityThreshold(this._handle, value);
/*     */ 
/* 237 */     if (result != 0) {
/* 238 */       throw new FingerException(result);
/*     */     }
/* 240 */     return value[0];
/*     */   }
/*     */ 
/*     */   public void setImageQualityThreshold(int value)
/*     */     throws FingerException
/*     */   {
/* 250 */     int result = nSetImageQualityThreshold(this._handle, value);
/*     */ 
/* 252 */     if (result != 0)
/* 253 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public int getMinimumImageCount()
/*     */     throws FingerException
/*     */   {
/* 263 */     int[] value = new int[1];
/* 264 */     int result = nGetMinimumImageCount(this._handle, value);
/*     */ 
/* 266 */     if (result != 0) {
/* 267 */       throw new FingerException(result);
/*     */     }
/* 269 */     return value[0];
/*     */   }
/*     */ 
/*     */   public void setMinimumImageCount(int value)
/*     */     throws FingerException
/*     */   {
/* 279 */     int result = nSetMinimumImageCount(this._handle, value);
/*     */ 
/* 281 */     if (result != 0)
/* 282 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   private static final native int nInitialize(long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nDispose(long paramLong);
/*     */ 
/*     */   private static final native int nAddImage(long paramLong1, long paramLong2, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetQuality(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetFingerQuality(long paramLong, int paramInt, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nCreateTemplateRecord(long paramLong, int paramInt, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nCreateImageRecord(long paramLong, int paramInt, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nRemovePosition(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nChangePosition(long paramLong, int paramInt1, int paramInt2);
/*     */ 
/*     */   private static final native int nGetBestImage(long paramLong, int paramInt, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nGetBestFinger(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetFingerCount(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetImageQualityThreshold(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nSetImageQualityThreshold(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nGetMinimumImageCount(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nSetMinimumImageCount(long paramLong, int paramInt);
/*     */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerEnroll
 * JD-Core Version:    0.6.2
 */