/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

/**
 *
 * @author admin
 */

public class FacialHeader {
	String formatIdentifier = "FAC";
	String versionNumber = "010";
	int recordLength = 0;
	int imageCount = 0;

    public String getFormatIdentifier() {
        return formatIdentifier;
    }

    public void setFormatIdentifier(String formatIdentifier) {
        this.formatIdentifier = formatIdentifier;
    }

    public int getImageCount() {
        return imageCount;
    }

    public void setImageCount(int imageCount) {
        this.imageCount = imageCount;
    }

    public int getRecordLength() {
        return recordLength;
    }

    public void setRecordLength(int recordLength) {
        this.recordLength = recordLength;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }
    
    public boolean initWithByteString(String byteString)
    {
      String chunk;
	boolean retval = false;

	// XXX need to vet length here, not sure about the length for record length,
	// 	num facial images, inferring from the data on the IDProtect Duo cards.
	if (byteString.length() >= (14 * 2))
	{
	//	byteString = removeWhitespace(byteString);

		// 4 bytes for "Format Identifier", a NULL terminated string:
		//this.setFormatIdentifier(hexStringToASCII(byteString.substring(0, 8)));
                  this.setFormatIdentifier(byteString.substring(0, 8));
		// 4 bytes for "Version Number", a NULL terminated string:
		//this.setVersionNumber(hexStringToASCII(byteString.substring(8, 16)));
              this.setVersionNumber((byteString.substring(8, 16)));
		// 4 bytes for "Record Length":
		// XXX vet this len
		this.setRecordLength(Integer.parseInt(byteString.substring(16, 24), 16));

		// 2 bytes for "number of Facial Images":
		// XXX vet this len
		this.setImageCount(Integer.parseInt(byteString.substring(24, 28), 16));

		retval = true;
	}

	return retval;
    }
    
}
