/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.id3.fingerprint.FingerCapture;
import com.id3.fingerprint.FingerCaptureDevice;
import com.id3.fingerprint.FingerCaptureListener;
import com.id3.fingerprint.FingerCaptureStatus;
import com.id3.fingerprint.FingerEnroll;
import com.id3.fingerprint.FingerEnrollStatus;
import com.id3.fingerprint.FingerException;
import com.id3.fingerprint.FingerImage;
import com.id3.fingerprint.FingerLicense;
import com.id3.fingerprint.FingerTemplate;
import com.id3.fingerprint.FingerTemplateRecord;
import com.secuera.middleware.biometrics.BiometricError;
import com.secuera.middleware.cardreader.beans.BiometricBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.MiddlewareConstants;

/**
 *
 * @author secuera
 */
public class Id3BioCapture implements FingerCaptureListener {

    private FingerCaptureDevice[] devices;
    private FingerCaptureDevice capturedevice;
    private FingerCapture capture;
    private FingerEnroll fgEnroll;
    private BiometricBean fgBean = new BiometricBean();
    private BiometricError fgerror = new BiometricError();
    private boolean stopCapture = false;
    private boolean deviceCapture = false;
    public int imageCount;
    private int fgType;
    private int Imagequality;
    /*Define Device Handle and Finger Type   */
    private long DeviceHandle;
    private static Id3BioCapture instance = null;
    private static String[] statusMessage = {
        MiddlewareConstants.CaptureStopped,
        MiddlewareConstants.PlaceFinger,
        MiddlewareConstants.RemoveFinger,
        MiddlewareConstants.RollFinger,
        MiddlewareConstants.FingerPlaced,
        MiddlewareConstants.FingerRemoved,
        MiddlewareConstants.FingerDownBorder,
        MiddlewareConstants.FingerTopBorder,
        MiddlewareConstants.FingerRightBorder,
        MiddlewareConstants.FingerLeftBorder,
        MiddlewareConstants.PressFingerHarder,
        MiddlewareConstants.SwipeTooFast,
        MiddlewareConstants.SwipeTooSlow,
        MiddlewareConstants.SwipeTooSkewed,
        MiddlewareConstants.InapSpeed,
        MiddlewareConstants.ImageSmall,
        MiddlewareConstants.PoorQuality,
        MiddlewareConstants.FingerLifted,
        MiddlewareConstants.RollingFast,
        MiddlewareConstants.DirtySensor,
        MiddlewareConstants.FingerShifted,
        MiddlewareConstants.RollingTooShort,
        MiddlewareConstants.FewerFinger,
        MiddlewareConstants.TooManyFinger,
        MiddlewareConstants.WrongHand};

    private Id3BioCapture() {
        super();
    }

    public static Id3BioCapture getInstance(int Imagequality, int ImgCount) throws MiddleWareException {
        if (instance == null) {
            try {
                instance = new Id3BioCapture(Imagequality, ImgCount);
            } catch (MiddleWareException ex) {
                throw new MiddleWareException(ex.getMessage());
            }
        }

        return instance;
    }

    private Id3BioCapture(int ImgQuality, int ImgCount) throws MiddleWareException {


        Imagequality = ImgQuality;

        try {
            FingerLicense.checkLicense();

            System.out.print("License is valid\n");
            System.out.print(" - path : " + FingerLicense.getLicensePath() + "\n");
            System.out.print(" - owner : " + FingerLicense.getLicenseOwner() + "\n");
            System.out.print(" - count : " + FingerLicense.getMaxFingerCount() + "\n");

        } catch (FingerException ex) {
            //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            //System.out.print("\nAn error has occured !");
            // TODO Auto-generated catch block
            //ex.printStackTrace();
            throw new MiddleWareException(ex.getMessage());
        }

        try {
            fgEnroll = new FingerEnroll();
        } catch (FingerException ex) {
            throw new MiddleWareException(ex.getMessage());
            //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            fgEnroll.setMinimumImageCount(ImgCount);
        } catch (FingerException ex) {
            Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<String> getFGTerminals() throws MiddleWareException {

        List deviceList = new ArrayList<String>();

        try {
            System.out.print("\nGetting List of capture devices...\n");
            try {
                devices = getFPDevice();
            } catch (MiddleWareException e) {
            }
            int device_cnt = capture.getDeviceCount();
            for (int i = 0; i < device_cnt; i++) {
                deviceList.add(devices[i].getName());
            }

        } catch (FingerException e) {
            System.out.print("\nAn error has occured !");

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return deviceList;

    }
    
     public List<FingerCaptureDevice> getTerminals() throws MiddleWareException {

        List<FingerCaptureDevice> deviceList = new ArrayList<FingerCaptureDevice>();

        try {
            System.out.print("\nGetting List of capture devices...\n");
            try {
                devices = getFPDevice();
            } catch (MiddleWareException e) {
            }
            int device_cnt = capture.getDeviceCount();
            for (int i = 0; i < device_cnt; i++) {
                deviceList.add(devices[i]);
            }

        } catch (FingerException e) {
            System.out.print("\nAn error has occured !");

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return deviceList;

    }

    private FingerCaptureDevice[] getFPDevice() throws MiddleWareException {
        FingerCaptureDevice[] devices = null;

        try {
            capture = new FingerCapture(this, false);


            while (!deviceCapture) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
                    throw new MiddleWareException(ex.getMessage());
                }
            }

            devices = capture.getDevices();

        } catch (FingerException e) {
            //System.out.print("\nAn error has occured !");
            // TODO Auto-generated catch block
            //e.printStackTrace();
            throw new MiddleWareException(e.getMessage());
        }


        return devices;

    }

    private long getDeviceHandle(String deviceName) throws FingerException {
        long deviceHandle = 0;

        System.out.println("Before getting FingerCapture ============================  ");
        
        capture = new FingerCapture(this, false);
 
        System.out.println("After getting FingerCapture ============================  " + capture);
        
        
        try {
            
        System.out.println("Before getting the FP Device ============================  ");
        
           if(devices == null){
               devices = getFPDevice();
           }
            
            System.out.println("List of devices >>>>>>>>>>>>>>>>  " + devices);
            System.out.println("List of devices >>>>>>>>>>>>>>>>  " + devices.length);
        } catch (MiddleWareException e) {
        }
        int device_cnt = capture.getDeviceCount();
        
        System.out.println("Device Count >>>>>>>>>>>>>>>>  " + device_cnt);
        
        for (int i = 0; i < device_cnt; i++) {
             System.out.println("Device Name >>>>>>>>>>>>>>>>  " + devices[i].getName());
            if (devices[i].getName().equalsIgnoreCase(deviceName)) {
                deviceHandle = devices[i].getHandle();
                break;
            }
        }

        return deviceHandle;
    }

      public BiometricBean startCapture(FingerCaptureDevice deviceHandle, int fgType) throws MiddleWareException {

         
        
        //capture finger print
        try {
            //wait till we capture the finger print devices
            //Thread.sleep(600L);
            System.out.println("startCapture :: Before Starting the Fingure Capture >>>>>>>>>>>>>>>>  " + deviceHandle);
         
            System.out.println("startCapture ::   Fingure Type >>>>>>>>>>>>>>>>  " + fgType);
            Thread.sleep(500);
            deviceHandle.startCapture(fgType);
        } catch (InterruptedException ex) {
            Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FingerException ex) {
            Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            //ex.printStackTrace();
            //throw new MiddleWareException(ex.getMessage());
        }


        // wait for capture completed
        //long i = 1;
        while (!stopCapture) {
            try {
                Thread.sleep(50);
                // i = 1 + 1;
                //if (i == 10) {
                //   stopCapture = true;
                //}

            } catch (InterruptedException e) {
                throw new MiddleWareException(e.getMessage());
            }
        }

        /*Set Stopcapture for next capture.*/
        stopCapture = false;
        return fgBean;

    }
      
      
    public BiometricBean startCapture(String deviceName, int fgtype) throws MiddleWareException {

        fgType = fgtype;

        //get Device Handle
        try {
            System.out.println("Before getting the device handle ============================  " + deviceName);
            DeviceHandle = getDeviceHandle(deviceName);
        } catch (FingerException ex) {
            //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            throw new MiddleWareException(ex.getMessage());
        }

        //get Device 
        capturedevice = new FingerCaptureDevice(DeviceHandle);

        //wait till we capture the finger print devices
        while (!deviceCapture) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
                throw new MiddleWareException(ex.getMessage());
            }

        }


        //capture finger print
        try {
            capturedevice.startCapture(fgtype);
        } catch (FingerException ex) {
            //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);

            throw new MiddleWareException(ex.getMessage());
        }


        // wait for capture completed
        //long i = 1;
        while (!stopCapture) {
            try {
                Thread.sleep(50);
                // i = 1 + 1;
                //if (i == 10) {
                //   stopCapture = true;
                //}

            } catch (InterruptedException e) {
                throw new MiddleWareException(e.getMessage());
            }
        }

        /*Set Stopcapture for next capture.*/
        stopCapture = false;
        return fgBean;

    }

    @Override
    public void deviceAdded(FingerCapture sender, FingerCaptureDevice device) {
        if (device != null) {
            deviceCapture = true;
        }
    }

    @Override
    public void deviceRemoved(FingerCapture sender, String deviceName) {
        System.out.print(deviceName + " : Device removed.\n");
    }

    @Override
    public void statusChanged(FingerCapture sender, FingerCaptureDevice device, int status) {
        System.out.print("\n- " + statusMessage[status] + " - \n");
        String errorCode = null;

        switch (status) {
            case 0:
                errorCode = "";
                break;
            case 1:
                errorCode = "";
                break;
            case 2:
                errorCode = "-1";
                break;
            case 3:
                errorCode = "-1";
                break;
            case 4:
                errorCode = "";
                break;
            case 5:
                errorCode = "-1";
                break;
            case 6:
                errorCode = "-1";
                break;
            case 7:
                errorCode = "-1";
                break;
            case 8:
                errorCode = "-1";
                break;
            case 9:
                errorCode = "-1";
                break;
            case 10:
                errorCode = "-1";
                break;
            case 11:
                errorCode = "-1";
                break;
            case 12:
                errorCode = "-1";
                break;
            case 13:
                errorCode = "-1";
                break;
            case 14:
                errorCode = "-1";
                break;
            case 15:
                errorCode = "-1";
                break;
            case 16:
                errorCode = "-1";
                break;
            case 17:
                errorCode = "-1";
                break;
            case 18:
                errorCode = "-1";
                break;
            case 19:
                errorCode = "-1";
                break;
            case 20:
                errorCode = "-1";
                break;
            case 21:
                errorCode = "-1";
                break;
            case 22:
                errorCode = "-1";
                break;
            case 23:
                errorCode = "-1";
                break;
            case 24:
                errorCode = "-1";
                break;
        }

        if (status == FingerCaptureStatus.Stopped) {
            stopCapture = true;
            fgBean.setFPStatus("Success");


        }
        if (errorCode.equalsIgnoreCase("-1")) {

            fgerror.setErrorMsg(statusMessage[status]);
            fgerror.setErrorCode("-1");
            fgBean.setBioError(fgerror);
            stopCapture = true;
        }


    }

    @Override
    public void imagePreview(FingerCapture sender, FingerCaptureDevice device, FingerImage image) {
    }

    @Override
    public void imageCaptured(FingerCapture sender, FingerCaptureDevice device,
            FingerImage image) {
        try {
            if (device != null) {
                System.out.print(device.getName() + " : ");
            }

            if (image.getSegmentCount() > 0) {
                System.out.printf("   - %d finger(s) detected.\n", image.getSegmentCount());
            }

        } catch (FingerException e) {
        }



    }

    @Override
    public void imageProcessed(FingerCapture sender, FingerCaptureDevice device, FingerImage image) {

        try {
            // display image information
            displayImageInformation(image);
        } catch (FingerException ex) {
            Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
        }


        try {
            addImage(image);

        } catch (MiddleWareException ex) {
            fgerror.setErrorMsg(ex.getMessage());
            fgerror.setErrorCode("-1");
            fgBean.setBioError(fgerror);
            // Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    private void displayImageInformation(FingerImage image) throws FingerException {
        System.out.print("   - capture device vendor : " + image.getCaptureDeviceVendor() + "\n");
        System.out.print("   - image width : " + image.getWidth() + " pixels\n");
        System.out.print("   - image height : " + image.getHeight() + " pixels\n");
        System.out.print("   - impression type : " + image.getImpressionType() + "\n");
        System.out.print("   - Horizontal res. : " + image.getHorizontalResolution() + " dpi\n");
        System.out.print("   - Vertical res. : " + image.getVerticalResolution() + " dpi\n");
        System.out.print("   - NFIQ : " + image.getQuality() + "\n");
        fgBean.setFingerImage((BufferedImage) image.createImageAwt());
    }

    public void addImage(FingerImage image) throws MiddleWareException {

        int fgImgStatus = 0;
        String errorCode = "";
        String errorMsg = "";
        if (fgEnroll == null) {
            try {

                fgEnroll = new FingerEnroll();
            } catch (FingerException ex) {
                throw new MiddleWareException(ex.getMessage());
                //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //add image
        try {
            fgImgStatus = fgEnroll.addImage(image);
        } catch (FingerException ex) {
            stopCapture = true;
            throw new MiddleWareException(ex.getMessage());
            //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
        }


        //check image status
        switch (fgImgStatus) {
            case FingerEnrollStatus.InProgress:
                errorCode = "";
                errorMsg = "In Progress";
                break;
            case FingerEnrollStatus.WrongFinger:
                errorCode = "-1";
                errorMsg = "Wrong Finger Placed";
                break;
            case FingerEnrollStatus.UnknownFinger:
                errorCode = "-1";
                errorMsg = "Unknown Finger Placed";
                break;
            case FingerEnrollStatus.NonMatch:
                errorCode = "-1";
                errorMsg = "The presented finger does not match with the previous presentations.";
                break;
            case FingerEnrollStatus.Completed:
                errorCode = "";
                errorMsg = "Compelete.";
                break;
            case FingerEnrollStatus.Failed:
                errorCode = "-1";
                errorMsg = "Finger Catpure Failed.";
                break;

        }

        //return if there is an error
        if (errorCode.equalsIgnoreCase("-1")) {
            fgBean.setFPStatus("InProgress");
            fgerror.setErrorMsg(errorMsg);
            fgerror.setErrorCode("-1");


        } else {

            fgBean.setFPStatus("InProgress");
            imageCount++;
        }

        fgBean.setBioError(fgerror);
        stopCapture = true;




    }

    public BiometricBean createCompactTemplate() {
        {

            FingerTemplate[] fgTemplates = null;
            FingerTemplate fgTemplate = null;
            FingerTemplateRecord fgTemplateRecord = null;
            FingerImage image = null;


            try {
                image = fgEnroll.getBestImage(fgType);
            } catch (FingerException ex) {
                Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                fgTemplateRecord = fgEnroll.createTemplateRecord(1);
            } catch (FingerException ex) {
                Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            }


            fgTemplates = fgTemplateRecord.getTemplates();
            fgTemplate = fgTemplates[0];
            int minuCount = 0;

            try {
                minuCount = fgTemplate.getMinutiaCount();
            } catch (FingerException ex) {
                Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                //id3FingerTemplate_ToIso19794CompactCard
                fgBean.setFingerPrint(fgTemplate.toIso19794CompactCard(minuCount));
            } catch (FingerException ex) {
                Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            }

            BufferedImage fgImage = null;
            try {
                fgImage = (BufferedImage) image.createImageAwt();
            } catch (FingerException ex) {
                Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            }


            /*   File outputfile = new File("C:\\01SecueraMiddleware\\finger.png");
             try {
             ImageIO.write(fgImage, "png", outputfile);
             } catch (IOException ex) {
             Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
             }
    
             System.out.println("Image saved on C Drive");
             */

            fgBean.setFingerImage(fgImage);

        }


        fgBean.setFPStatus("Success");
        imageCount = 0;
        setFgType(0);
        fgEnroll.dispose();
        return fgBean;

    }

    private void setFgType(int fgType) {
        this.fgType = fgType;
    }

    public int getImageCount() {
        return imageCount;
    }

    public void dispose() {
        capture.dispose();

    }
}
