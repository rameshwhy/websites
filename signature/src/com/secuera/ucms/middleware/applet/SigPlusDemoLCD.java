package com.secuera.ucms.middleware.applet;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;

import gnu.io.*;
import java.io.*;

import com.topaz.sigplus.*;

public class SigPlusDemoLCD extends Frame implements Runnable
   {
   SigPlus              sigObj = null;
   static final int     numImages = 2;
   Image[]              rawImages;

   static final String  fileBase = "C:\\SecuEra Demo\\demo\\sigplusjava2_60\\sigplusjava2_60\\lcd4x5demo\\lcd4x5demo\\lcd4x5demo\\";
   int			AppScreen;
   Thread               eventThread;
   int                  currentImage;	
   int                  page;
   String		signature;
	
   public static void main(String Args[])
      {
      SigPlusDemoLCD demo = new SigPlusDemoLCD();
      }

   public SigPlusDemoLCD()
      {
      int      i;
      String   fileName;

      try
         {

              ClassLoader cl = (com.topaz.sigplus.SigPlus.class).getClassLoader();
	      sigObj = (SigPlus)Beans.instantiate(cl, "com.topaz.sigplus.SigPlus");

	      setLayout(new GridLayout(1,1));
  	      add( sigObj );
	      pack();
  	      setTitle("Topaz LCD 4X5 Demo");


         addWindowListener(new WindowAdapter()
            {
            public void windowClosing(WindowEvent we)
               {
               System.exit(0);
               }

            public void windowClosed(WindowEvent we)
               {
               System.exit(0);
               }
	    });


         sigObj.addSigPlusListener(new SigPlusListener()
            {
            public void handleTabletTimerEvent(SigPlusEvent0 evt)
               {
               }

            public void handleNewTabletData(SigPlusEvent0 evt)
               {
               }


            public void handleKeyPadData(SigPlusEvent0 evt)
               {
               }
	    });

         setSize(640, 256);
         show();

         sigObj.setTabletModel("SignatureGemLCD4X5");
  	 sigObj.setTabletComPort("HID1");

         sigObj.setLCDCaptureMode(2);
	 sigObj.setTabletState(1);

         sigObj.setDisplayRotation(0);
         sigObj.setDisplayJustifyMode(1);
         sigObj.setDisplayJustifyX(50);
         sigObj.setDisplayJustifyY(50);

         sigObj.keyPadSetSigWindow(1, 12, 186, 318, 52);
         sigObj.lcdSetWindow(3, 178, 309, 51);

         sigObj.keyPadAddHotSpot (0, 1, 8, 73, 25, 15); //option 1
         sigObj.keyPadAddHotSpot (1, 1, 8, 102, 25, 15); //option 2
         sigObj.keyPadAddHotSpot (2, 1, 40, 160, 37, 14);  //clear
	 sigObj.keyPadAddHotSpot (3, 1, 260, 160, 48, 14); //ok



         MediaTracker mt = new MediaTracker(this);
         rawImages = new Image[ numImages ];
         String[] imageTitles = 
		{
			"img4x5", 
			"thankyou"
                };

	 for(i = 0; i < numImages; i++)
            {
               fileName = fileBase + imageTitles[i] + ".jpg";
               System.out.print("File NAme >>>>  " + fileName);
               rawImages[ i ] = Toolkit.getDefaultToolkit().getImage(fileName);
               mt.addImage(rawImages[ i ], i + 1);
            }
         try
            {
               mt.waitForAll();
            }
         catch(Exception e)
            {
               System.out.println("Error opening bitmap files");
            }

         sigObj.lcdRefresh(0, 0, 0, 320, 240);

         sigObj.lcdWriteImage (1, 2, 0, 0, 320, 240, rawImages[0]);
	 sigObj.lcdRefresh(2, 0, 0, 320, 240);
	 sigObj.setLCDCaptureMode(2);

         
         eventThread = new Thread(this);
         eventThread.start();
         }
      catch (Exception e)
         {
         return;
         }
      }

   public void run()
      {
      try
	 {
	 while (true)
	    {
	    Thread.sleep(100);



            if(sigObj.keyPadQueryHotSpot(0) !=0)
            {
               sigObj.keyPadClearSigWindow(1);
               sigObj.lcdRefresh(1, 11, 74, 14, 13);
               Thread.sleep(300);
               sigObj.lcdRefresh(1, 11, 74, 14, 13);
            }

            if(sigObj.keyPadQueryHotSpot(1) !=0)
            {
               sigObj.keyPadClearSigWindow(1);
               sigObj.lcdRefresh(1, 11, 103, 14, 13);
               Thread.sleep(300);
               sigObj.lcdRefresh(1, 11, 103, 14, 13);
            }

            if(sigObj.keyPadQueryHotSpot(2) !=0)
               {
                     //clear chosen
                     sigObj.keyPadClearSigWindow(1);
                     sigObj.lcdRefresh(1, 32, 153, 37, 15);
		     sigObj.clearTablet();
                     Thread.sleep(300);
		     sigObj.lcdRefresh(1, 32, 153, 37, 15);
                     sigObj.lcdRefresh(2, 0, 170, 320, 70);
               }
            else if(sigObj.keyPadQueryHotSpot(3) !=0)
               {
                     sigObj.keyPadClearSigWindow(1);
                     sigObj.lcdRefresh(1, 250, 153, 34, 15);
                     Thread.sleep(300);
                     sigObj.lcdRefresh(1, 250, 153, 34, 15);

    		     if( sigObj.numberOfTabletPoints() > 0){
       			//sigObj.setTabletState ( 0 );
       			String mySigString;
		   	sigObj.autoKeyStart();
		   	sigObj.setAutoKeyData("Sample Encryption Data");
		   	sigObj.autoKeyFinish();
                   	sigObj.setEncryptionMode(2);
			//return signature string
      			mySigString = sigObj.getSigString();
                        sigObj.setTabletState(1);
    		        sigObj.setLCDCaptureMode(1);
    		        sigObj.lcdRefresh(0, 0, 0, 240, 64);
    		        sigObj.setTabletState(0);
		        System.exit(0);
                     }
                     else
                     {
                        System.out.println("Must sign first!");
                     }

               }
   
                     sigObj.keyPadClearSigWindow(1);
             }
           }
      catch (InterruptedException e)
         {
	 }
      }

   }


