// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.wia;

import eu.gnome.morena.DeviceBase;
import eu.gnome.morena.TransferListener;
import java.io.File;
import java.io.PrintStream;
import java.util.*;

// Referenced classes of package eu.gnome.morena.wia:
//            WIAItem, WIAConstants, SupportedPropertyValues

public class WIADevice extends DeviceBase
    implements WIAConstants
{

    private native void _release(long l)
        throws Exception;

    protected void fireTransferDone(String s, String s1)
    {
        File file = new File(s);
        if(s1 != null)
        {
            File file1 = new File(s);
            file = new File((new StringBuilder()).append(s).append(".").append(s1).toString());
            file1.renameTo(file);
        }
        super.fireTransferDone(file.getAbsolutePath());
    }

    protected void fireTransferFailed(int i, String s)
    {
        super.fireTransferFailed(i, s);
    }

    protected void fireTransferProgress(int i, int j, long l)
    {
        if(transferDoneListener instanceof TransferListener)
            ((TransferListener)transferDoneListener).transferProgress(j);
    }

    WIADevice(String s, String s1)
    {
        super(s, 0L);
        UID = s1;
        items = new Vector();
    }

    public void displayProperties()
    {
        System.out.println((new StringBuilder()).append("Device ").append(description).toString());
        for(Iterator iterator = items.iterator(); iterator.hasNext();)
        {
            WIAItem wiaitem = (WIAItem)iterator.next();
            System.out.println((new StringBuilder()).append(" --- ").append(wiaitem.toString()).toString());
            Iterator iterator1 = wiaitem.getPropertyIds().iterator();
            while(iterator1.hasNext()) 
            {
                Integer integer = (Integer)iterator1.next();
                SupportedPropertyValues supportedpropertyvalues = wiaitem.getProperty(integer.intValue());
                System.out.println((new StringBuilder()).append(" ").append(supportedpropertyvalues.print()).toString());
            }
        }

    }

    void release()
    {
        try
        {
            if(handle != 0L)
                _release(handle);
        }
        catch(Exception exception) { }
    }

    String UID;
    boolean available;
    protected List items;
}
