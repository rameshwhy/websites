/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.nxp;

import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessagesNXP;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import com.secuera.middleware.core.common.SendCommand;

/**
 *
 * @author Harpreet Singh
 */
public class FinalizePerso {

    SendCommand sendCard = new SendCommand();
    ExceptionMessagesNXP expMsg = new ExceptionMessagesNXP();

    public FinalizePerso() {
    }

    public boolean finalizePerso(CardChannel channel) throws MiddleWareException {

        if (!finalPerso(channel)) {
            return false;
        }

        if (!cleanUp(channel)) {
            return false;
        }

        return true;

    }

    private boolean finalPerso(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();

        //   FINALIZE
        //  send "80 E2 80 00 00" "00009000"

        byte[] apdu_finalize = {(byte) 0x80, (byte) 0xE2, (byte) 0x80, 0x00, 0x00};

        CommandAPDU FINAL_APDU = new CommandAPDU(apdu_finalize);

        try {
            sendCard.sendCommand(channel, FINAL_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equalsIgnoreCase("00009000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }

    private boolean cleanUp(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();

        /*  # CLEANUP
         /send "80 EA 00 00" "9000"
         */

        byte[] apdu_cleanup = {(byte) 0x80, (byte) 0xEA, (byte) 0x00, 0x00};

        CommandAPDU CLEAN_UP = new CommandAPDU(apdu_cleanup);

        try {
            sendCard.sendCommand(channel, CLEAN_UP, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equalsIgnoreCase("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }
}
