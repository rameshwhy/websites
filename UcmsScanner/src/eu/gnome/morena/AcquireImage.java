package eu.gnome.morena;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import eu.gnome.morena.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class AcquireImage
{
    static class FileTransferHandler
        implements TransferDoneListener
    {

        public void transferDone(File file)
        {
            imageFile = file;
            notifyRequestor();
        }

        public void transferFailed(int code, String error)
        {
            this.code = code;
            this.error = error;
            notifyRequestor();
        }

        private synchronized void notifyRequestor()
        {
            transferDone = true;
            notify();
        }

        File imageFile;
        int code;
        String error;
        boolean transferDone;

        FileTransferHandler()
        {
            transferDone = false;
        }
    }

    static class ImageTransferHandler
        implements TransferDoneListener
    {

        public void transferDone(File file)
        {
            if(file != null)
                try
                {
                    image = ImageIO.read(file);
                }
                catch(IOException e)
                {
                    error = e.getLocalizedMessage();
                }
            notifyRequestor();
        }

        public void transferFailed(int code, String error)
        {
            this.code = code;
            this.error = error;
            notifyRequestor();
        }

        private synchronized void notifyRequestor()
        {
            transferDone = true;
            notify();
        }

        BufferedImage image;
        int code;
        String error;
        boolean transferDone;

        ImageTransferHandler()
        {
            transferDone = false;
        }
    }


    public AcquireImage()
    {
    }

    public static BufferedImage scanImage(Device device)
        throws Exception
    {
        return scanImage(device, 0);
    }

    public static BufferedImage scanImage(Device device, int item)
        throws Exception
    {
        ImageTransferHandler th = new ImageTransferHandler();
        synchronized(th)
        {
            ((DeviceBase)device).startTransfer(th, item);
            while(!th.transferDone) 
                th.wait();
        }
        if(th.image != null)
            return th.image;
        else
            throw new Exception(th.error);
    }

    public static File scanFile(Device device)
        throws Exception
    {
        return scanFile(device, 0);
    }

    public static File scanFile(Device device, int item)
        throws Exception
    {
        FileTransferHandler th = new FileTransferHandler();
        synchronized(th)
        {
            ((DeviceBase)device).startTransfer(th, item);
            while(!th.transferDone) 
                th.wait();
        }
        if(th.imageFile != null)
            return th.imageFile;
        else
            throw new Exception(th.error);
    }

    public static final int WIA_ERROR_PAPER_EMPTY = 417;
}