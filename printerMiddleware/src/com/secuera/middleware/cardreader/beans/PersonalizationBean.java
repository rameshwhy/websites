package com.secuera.middleware.cardreader.beans;

import java.util.Arrays;

public class PersonalizationBean {

    private ChuidBean chuid;
    private String localPin;
    private String globalPin;
    private PrintedInfoBean printInfo;
    byte[] byteimage;

    public ChuidBean getChuid() {
        return chuid;
    }

    public void setChuid(ChuidBean chuid) {
        this.chuid = chuid;
    }

    public String getLocalPin() {
        return localPin;
    }

    public void setLocalPin(String localPin) {
        this.localPin = localPin;
    }

    public String getGlobalPin() {
        return globalPin;
    }

    public void setGlobalPin(String globalPin) {
        this.globalPin = globalPin;
    }

    public PrintedInfoBean getPrintInfo() {
        return printInfo;
    }

    public void setPrintInfo(PrintedInfoBean printInfo) {
        this.printInfo = printInfo;
    }

    public byte[] getByteimage() {
        return byteimage;
    }

    public void setByteimage(byte[] byteimage) {
        this.byteimage = byteimage;
    }

    @Override
    public String toString() {
        return "PersonalizationBean [chuid=" + chuid + ", localPin=" + localPin
                + ", globalPin=" + globalPin + ", printInfo=" + printInfo
                + ", byteimage=" + Arrays.toString(byteimage) + "]";
    }
}
