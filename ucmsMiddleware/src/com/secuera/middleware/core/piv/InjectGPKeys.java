/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.MacEncrypter;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.AesEncrypter;
import com.secuera.middleware.beans.piv.CredentialKeys;
import com.secuera.middleware.beans.piv.DiversifiedCredentialKeys;
import com.secuera.middleware.beans.piv.InitializationBean;
import com.secuera.middleware.beans.piv.SecureChannelBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class InjectGPKeys {
    
    // declare class variable for different keys
    private static byte[] CARD_MANAGER_AID;
    
    private byte[] CUSTOM_ENC_KEY;
    private byte[] CUSTOM_MAC_KEY;
    private byte[] CUSTOM_DEK_KEY;
    
    int macLength=8;
    private byte[] S_MAC_KEY;
    private byte[] PREV_MAC;
    private byte[] AUTH_DEK_KEY;
    
    // *****************************************************************
    // Create instances of other classes.
    // *****************************************************************
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    CredentialKeys credentialKeys = new CredentialKeys();
    SecureChannelBean scbean = new SecureChannelBean();
    InitializationBean initValues = new InitializationBean();
    DiversifiedCredentialKeys diversifiedCredentialKeys = new DiversifiedCredentialKeys();
    
    // Constructor get initial class values
    public InjectGPKeys(CredentialKeys crKeys,InitializationBean iValues,DiversifiedCredentialKeys dCrKeys) {
         credentialKeys = crKeys;
         diversifiedCredentialKeys = dCrKeys;
        //CARD_MANAGER_AID = crKeys.getCardManagerAid();
    
        CUSTOM_ENC_KEY = iValues.getEncKey();
        CUSTOM_MAC_KEY = iValues.getMacKey();
        CUSTOM_DEK_KEY = iValues.getDekKey();

    }

    
    public boolean loadGPKeys(CardChannel channel) throws MiddleWareException {
      try {
           selectSecurityDomain(channel);
                //open secure channel
             SecureChannel03 scp03 = new SecureChannel03(credentialKeys,diversifiedCredentialKeys);
             scbean = scp03.openSecureChannel03(channel, false);

             
                //get values from secure channel 03
                S_MAC_KEY = scbean.getS_MAC_KEY();
                PREV_MAC = scbean.getPREV_MAC();
                AUTH_DEK_KEY=scbean.getAUTH_DEK_KEY();
                
                loadKeys(channel);

        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return true;
    }
    
    private void loadKeys(CardChannel channel) throws MiddleWareException
     {
        
       
                         
	byte[] enc_key_check=computeKeyCheck(CUSTOM_ENC_KEY);
	byte[] mac_key_check=computeKeyCheck(CUSTOM_MAC_KEY);
	byte[] dek_key_check=computeKeyCheck(CUSTOM_DEK_KEY);

	byte[] enc_key_check_len = util.calcBertValue(enc_key_check.length,false,true);
	byte[] mac_key_check_len = util.calcBertValue(mac_key_check.length,false,true);
        byte[] dek_key_check_len = util.calcBertValue(dek_key_check.length,false,true);
	
        
        // **----------------------------------------------
        // ** Secure injection of Keys
        // **----------------------------------------------
        // First of all format key according to secure encryption requirement.
          CUSTOM_ENC_KEY = add_securepad(CUSTOM_ENC_KEY);
          CUSTOM_MAC_KEY = add_securepad(CUSTOM_MAC_KEY);
          CUSTOM_DEK_KEY = add_securepad(CUSTOM_DEK_KEY);


        // Now Encrypt Key values with KEK in CBC Mode.
        byte[] key_enc_encrypt = aesencrypt(AUTH_DEK_KEY, CUSTOM_ENC_KEY, true);
        byte[] key_mac_encrypt = aesencrypt(AUTH_DEK_KEY, CUSTOM_MAC_KEY, true);
        byte[] key_dek_encrypt = aesencrypt(AUTH_DEK_KEY, CUSTOM_DEK_KEY, true);

        
        
	byte[] key_enc_len = util.hexToByteArray(Integer.toHexString(key_enc_encrypt.length));
	byte[] key_mac_len = util.hexToByteArray(Integer.toHexString(key_mac_encrypt.length));
	byte[] key_dek_len = util.hexToByteArray(Integer.toHexString(key_dek_encrypt.length));

        // Now compute new MAC and prepare APDU command for Key Injection
        StringBuffer sb = new StringBuffer();

        // *****************************************************//
        // Using Replacement version command.

        byte[] apdu_data = {(byte) 0x84, (byte) 0xD8, 0x01, (byte) 0x81};
        byte[] gp_key_tag={(byte)0x88};
        byte[] gp_key_version={0x01};

        
        // Now prepare for MAC computation.
        byte[][] key_arrays = new byte[16][];
        key_arrays[0] = gp_key_version;
        key_arrays[1] = gp_key_tag;
        key_arrays[2] = key_enc_len;
        key_arrays[3] =key_enc_encrypt;
        key_arrays[4] =enc_key_check_len;
        key_arrays[5] =enc_key_check;
	key_arrays[6] = gp_key_tag;
        key_arrays[7] = key_mac_len;
        key_arrays[8] =key_mac_encrypt;
        key_arrays[9] =mac_key_check_len;
        key_arrays[10] =mac_key_check;
        key_arrays[11] = gp_key_tag;
        key_arrays[12] = key_dek_len;
        key_arrays[13] =key_dek_encrypt;
        key_arrays[14] =dek_key_check_len;
        key_arrays[15] =dek_key_check;


        byte[] set_key_data = util.combine_data(key_arrays);

        byte[] apdu_len = util.calcBertValue(set_key_data.length + macLength ,false,true);

	byte[][] mac_arrays = new byte[5][];
        mac_arrays[0] = PREV_MAC;
        mac_arrays[1] = UcmsMiddlewareConstants.ZERO_CARD_PAD;
        mac_arrays[2] = apdu_data;
        mac_arrays[3] = apdu_len;
        mac_arrays[4] = set_key_data;

	 byte[] mac_calc_data = util.combine_data(mac_arrays);

        // Now add secure pad according to encryption requirement.
        mac_calc_data = add_securepad(mac_calc_data);

        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, mac_calc_data, true);

        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        int i_offset = getOffset(encrypt_data);

        PREV_MAC = util.extract_data(encrypt_data, i_offset, 8);

           byte[] LE={0x00};
        
	// Now Prepare the command data for the card.
	    byte[][] key_command = new byte[5][];
	    key_command[0] = apdu_data;
       	    key_command[1] = apdu_len;
       	    key_command[2] = set_key_data;
       	    key_command[3] = PREV_MAC;
            key_command[4] = LE ;

	  byte[] key_command_data = util.combine_data(key_command);


        System.out.println("GPKeys_command_data :" + util.arrayToHex(key_command_data) );

        CommandAPDU KEY_APDU = new CommandAPDU(key_command_data);

        try {

            sendCard.sendCommand(channel, KEY_APDU, sb);

            // check if no success then throw exception
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();
	        errMsg.append(ExceptionMessages.INIT_GPKEYS);
                errMsg.append(" (").append(sb.toString()).append(")");
                throw new MiddleWareException(errMsg.toString());

            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
     }

    
    private byte[] computeKeyCheck(byte[] gpKey) throws MiddleWareException 
    {

        byte[] key_verification_data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

        // COMPUTE Key Check Value.
        byte[] key_check = util.extract_data(aesencrypt(gpKey, key_verification_data, false), 0, 3);
        
        return key_check;
        
    }
        
    
    /* select Security Domain method */
    void selectSecurityDomain(CardChannel channel) throws MiddleWareException {

        // **----------------------------------------------
        // ** Check presence and status of PIV-SD
        // **----------------------------------------------
        // ; Select PIV Application Security Domain using Oberthur registered
        // AID
        // 00 A4 04 00 10 A0 00 00 00 77 01 00 00 06 10 00 FD 00 00 00 27 (9000,
        // 6A82)
        // 00 A4 04 00 
        //Recv : 6F 3C 84 07
        //A0 00 00 01 51 00 00
        //A5 31 9F 6E 2A 48 20 50 2B 82 31 80 30 00 63 31 16 00 00 00 0C 00 00 14 32 31 16 14 33 31 16 14 34 31 16 00 00 00 00 14 35 31 16 00 00 00 00 9F 65 01 FF 


        StringBuffer sb = new StringBuffer();
        byte[] apdu_security_piv = {0x00, (byte) 0xA4, 0x04, 0x00, 0x10,
            (byte) 0xA0, 0x00, 0x00, 0x00, 0x77, 0x01, 0x00, 0x00, 0x06,
            0x10, 0x00, (byte) 0xFD, 0x00, 0x00, 0x00, 0x27};

        CommandAPDU SEC_APDU_PIV = new CommandAPDU(apdu_security_piv);

        try {
            sendCard.sendCommand(channel, SEC_APDU_PIV, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // ; Select PIV Application Issuer Security Domain
        // 00 A4 04 00 07 A0 00 00 01 51 00 00 (9000, 6A82)

        byte[] apdu_security = {0x00, (byte) 0xA4, 0x04, 0x00, 0x07,
            (byte) 0xA0, 0x00, 0x00, 0x01, 0x51, 0x00, 0x00};

        CommandAPDU SEC_APDU = new CommandAPDU(apdu_security);

        try {
            sendCard.sendCommand(channel, SEC_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    
    
    
    byte[] macencrypt(byte[] key, byte[] vector, byte[] from_card) throws MiddleWareException {

        MacEncrypter encrypter = new MacEncrypter(key, vector);

        // Encrypt
        byte[] res = null;
        try {
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        return res;

    }

    
    
    public byte[] aesencrypt(byte[] key, byte[] from_card, boolean vector) throws MiddleWareException {

        // Create encrypter/decrypter class
        AesEncrypter encrypter;
        byte[] res = null;
        try {

            if (vector) {
                encrypter = new AesEncrypter(key, UcmsMiddlewareConstants.IV_AES);
            }
            {
                encrypter = new AesEncrypter(key);
            }

            // Encrypt
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return res;

    }

      

    /* Adding secure pad to data to be encrypted for AES CBC Mode */
    byte[] add_securepad(byte[] data) {

        byte[] append_80 = {(byte) 0x80};
        byte[] append_00 = {0x00};

        int i_len = data.length;
        int k = i_len % 16; // Divide by 16, because one standard bye block of
        // data is 16.
        int i_cnt = 16 - k; // Calculate how many more bytes required to make a
        // block of 16 bytes.

        if (k == 0) {
            return data;
        } else {
            for (int j = 0; j < i_cnt; j++) {
                // For the first append, "use append_80"
                if (j == 0) {
                    data = util.combine_data(data, append_80);

                } else {
                    data = util.combine_data(data, append_00);
                }

            }

            return data;

        }

    }

    /*
     * Determine the offset position of last block of 16 byte data from byte[]
     * array.
     */
    int getOffset(byte[] data) {

        // Subtract by 16,because one standard bye block of data is 16.
        int i_len = data.length - 16;

        return i_len;

    }
    
    
}

