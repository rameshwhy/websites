// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena;

import java.util.List;

// Referenced classes of package eu.gnome.morena:
//            Device, TransferDoneListener

public interface Scanner
    extends Device
{

    public abstract int getMode();

    public abstract void setMode(int i);

    public abstract List getSupportedModes();

    public abstract int getResolution();

    public abstract void setResolution(int i);

    public abstract List getSupportedResolutions();

    public abstract void setFrame(int i, int j, int k, int l);

    public abstract void startTransfer(TransferDoneListener transferdonelistener, int i)
        throws Exception;

    public abstract void cancelTransfer();

    public abstract int getFunctionalUnit();

    public abstract void setFunctionalUnit(int i);

    public abstract int getFlatbedFunctionalUnit();

    public abstract int getFeederFunctionalUnit();

    public abstract boolean isDuplexSupported();

    public abstract boolean isDuplexEnabled();

    public abstract void setDuplexEnabled(boolean flag);

    public static final int RGB_8 = 8;
    public static final int GRAY_8 = -8;
    public static final int RGB_16 = 16;
    public static final int GRAY_16 = -16;
    public static final int BLACK_AND_WHITE = -1;
    public static final int DUPLEX_NOT_AVAIL = 0;
    public static final int DUPLEX_AVAIL = 1;
    public static final int DUPLEX_AVAIL_FRONT_ONLY = 2;
    public static final int DUPLEX_AVAIL_BACK_ONLY = 3;
}
