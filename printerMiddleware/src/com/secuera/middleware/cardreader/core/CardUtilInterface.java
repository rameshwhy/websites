/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import javax.smartcardio.CardChannel;
import com.secuera.middleware.cardreader.entities.MiddleWareException;

/**
 *
 * @author admin
 */
public interface CardUtilInterface {

    public boolean CardLock(CardChannel channel) throws MiddleWareException;

    public boolean CardUnlock(CardChannel channel) throws MiddleWareException;
}
