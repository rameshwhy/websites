/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.SecurityBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Arrays;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class verifySecudataSignature {

    SendCommand sendCard = new SendCommand();
    PivUtilInterface Putil = new PivUtil();
    CommonUtil util = new CommonUtil();
    VerifyDigitalSignature verify = new VerifyDigitalSignature();
    ParseAsmObject pso = new ParseAsmObject();
    SecurityBean secBean = new SecurityBean();
    ExceptionMessages expMsg = new ExceptionMessages();
    ManageCHUID chuid= new ManageCHUID();
    String SHAWithRSASIG[] =  {"2a864886f70d01010d","2a864886f70d010101"};
    String SHASignatureAlgo; 
    
    // This is OID for PIV-CHUIDSecurity Object.
    String SHA512WithRSA = "2a864886f70d01010d";
    String LDSOID = "06052b1b010101";
    boolean lb_signature = true;
    private String str_hexChuid;
    private String str_hexPrinted;
    private String str_hexFacial;
    private String str_hexFinger;
    private String str_hexSec;
    private int grp_count;

    public verifySecudataSignature() {
        super();
    }

      public SecurityBean readSecContainer(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String returnValue;
        String errorMsg;
        SecurityBean sec = null;
        
        StringBuffer sb = new StringBuffer();
        byte[] apdu_sec = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05, 0x5C,
            0x03, (byte) 0x5F, (byte) 0xC1, 0x06}; // 
        
        try {
            CommandAPDU SEC_APDU = new CommandAPDU(apdu_sec);

            res = sendCard.sendCommand(channel, SEC_APDU, sb);

            //raise error if unable to Read CHUID
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "SecurityContainer");
                throw new MiddleWareException(errorMsg);
            }
            if (res == null || res.length == 0) {
                

            } else {
                //convert byte array to string & remove parity bits
                returnValue = util.arrayToHex(res);
                System.out.println(returnValue);

                if (returnValue.substring(2, 4).equals("82")) {
                    returnValue = returnValue.substring(8);
                } else if (returnValue.substring(2, 4).equals("81")) {
                    returnValue = returnValue.substring(6);
                } else {
                    returnValue = returnValue.substring(4);
                }
                
                ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();
                
                //get CHUID
                String str_CHUID =chuid.getChuid(channel);
                String str_PrintedInfo =mngPrintedInfo.getPrintedinfo(channel);
                String str_FacialInfo =Putil.getFacialinfo( channel);
                
                //Verify Signature and hash values of various containers
                try {
                    secBean = verifySig(returnValue, str_CHUID, str_PrintedInfo, str_FacialInfo,"");
                  //  if (result == false) {
                    // }
                   // }
                } catch (IOException ex) {
                     throw new MiddleWareException(ex.getMessage());
                }
            }
            return sec;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }
    
    
    private SecurityBean verifySig(String str_hex_sec, String str_hex_chuid, String str_hex_Printed,
        String str_hex_Facial, String str_hex_Finger) throws IOException {
        
        PublicKey pubkey;
        byte[] signature;
        byte[] signed_data = null;
        boolean lb_verify = true;
        
        str_hexSec = str_hex_sec;
        str_hexChuid = str_hex_chuid;
        str_hexPrinted = str_hex_Printed;
        str_hexFacial = str_hex_Facial;
        str_hexFinger = str_hex_Finger;

        signature = getSignature();

        if (signature == null) {
           lb_signature = false;
            lb_verify = false;
        }


        // get Public Key.
        pubkey = pso.getPublicKey(str_hex_chuid);

        if (pubkey == null) {
            lb_verify = false;

        }

        //get the actual data that was signed
        if (lb_verify) {
            signed_data = getSignedData();
        }


        if (lb_verify) {
            lb_verify = verify.VerifySignature(signed_data, signature, pubkey,SHASignatureAlgo);
        }

        secBean.setLb_sigVerify(lb_verify);
        
        verifyContainerHashes();

        return secBean;

    }

    private byte[] getSignedData() {
        String seq_tag = "30";
        String str_grps_len;
        String str_hex_grps = null;
        int grp_len;
        int i_startPos;
        int tag_loc;

        // First of all find the position of OID of LDS Security Object
        i_startPos = util.findEndLocation(str_hexSec, LDSOID);
        String str_hexSec_1 = str_hexSec.substring(i_startPos);

        tag_loc = util.findtagLocation(0, str_hexSec_1, seq_tag);

        if (tag_loc == -1) {
            return null;
        }
        if (tag_loc >= 0) {
            switch (str_hexSec_1.substring(tag_loc + 2, tag_loc + 4)) {
                case "82":
                    str_grps_len = str_hexSec_1.substring(tag_loc + 4, tag_loc + 8);
                    grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                    str_hex_grps = str_hexSec_1.substring(tag_loc, tag_loc + 8 + grp_len * 2);
                    break;
                case "81":
                    str_grps_len = str_hexSec_1.substring(tag_loc + 4, tag_loc + 6);
                    grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                    str_hex_grps = str_hexSec_1.substring(tag_loc , tag_loc + 6 + grp_len * 2);
                    break;
                default:
                    str_grps_len = str_hexSec_1.substring(tag_loc + 2, tag_loc + 4);
                    grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                    str_hex_grps = str_hexSec_1.substring(tag_loc , tag_loc + 4 + grp_len * 2);
                    break;
            }

        }


        return util.hex1ToByteArray(str_hex_grps);

    }

    private byte[] getSignature() {

        int i_startsig, sigLen;
        String str_sig;
        byte[] sig_encoded;

        //Find End of "SHA512WithRSA" OID  get to start of Certificate
        i_startsig = util.findEndLocation(str_hexSec, SHA512WithRSA);

        if (i_startsig == 0) {
            sig_encoded = null;
        } else {
            String sig_length = str_hexSec.substring(i_startsig + 4, i_startsig + 8);
            sigLen = Integer.valueOf(sig_length, 16).intValue();

            str_sig = str_hexSec.substring(i_startsig + 8, i_startsig + 8 + sigLen * 2);

            sig_encoded = util.hex1ToByteArray(str_sig);
        }

        return sig_encoded;

    }

    private void verifyContainerHashes() throws IOException {
        byte[] printed_cont = {0x30, 0x01}; //Printed Info container mapped to DG1.
        byte[] chuid_cont = {0x30, 0x00}; //CHUID container mapped to DG2.
        byte[] finger_cont = {0x60, 0x10}; //Finger Print container mapped to DG2.
        byte[] facial_cont = {0x60, 0x30}; //Facial container mapped to DG4.

        // First byte will be tag 'BA' ..2nd byte is length byte.
        String str_grp_len = str_hexSec.substring(2, 4);
        int grp_len = Integer.valueOf(str_grp_len, 16).intValue();

        String str_grp = str_hexSec.substring(4, 4 + grp_len * 2);
        

        for (int i = 0; i < str_grp.length(); i = i + 6) {

            //Store group count to be used in later functions.
            grp_count = grp_len / 3; // Each group consists of 3 bytes.

            String grp_nbr;
            byte[] cont_id;


            grp_nbr = str_grp.substring(i, i + 2);
            cont_id = util.hex1ToByteArray(str_grp.substring(i + 2, i + 2 + 4));


            if (Arrays.equals(cont_id, printed_cont)) {
                verifyPrintedHash(grp_nbr);

            } else if (Arrays.equals(cont_id, chuid_cont)) {
                verifyChuidHash(grp_nbr);

            } else if (Arrays.equals(cont_id, finger_cont)) {

                verifyFingerHash(grp_nbr);


            } else if (Arrays.equals(cont_id, facial_cont)) {

                verifyFacialHash(grp_nbr);
            }

        }


    }

    private void verifyChuidHash(String grp_nbr ) throws IOException {
        boolean lb_chuidVerify;
        byte[] hash_new;
        byte[] hash_original;
        hash_new = getMessageHash(util.hex1ToByteArray(str_hexChuid));

        hash_original = getOriginalHash(grp_nbr);

        if (Arrays.equals(hash_new, hash_original)) {
            lb_chuidVerify = true;

        } else {
            lb_chuidVerify = false;
        }

        secBean.setLb_chuidVerify(lb_chuidVerify);

    }

    private void verifyPrintedHash(String grp_nbr) throws IOException {

        boolean lb_printedVerify;
        byte[] hash_new;
        byte[] hash_original;
        hash_new = getMessageHash(util.hex1ToByteArray(str_hexPrinted));

        hash_original = getOriginalHash(grp_nbr);

        if (Arrays.equals(hash_new, hash_original)) {
            lb_printedVerify = true;

        } else {
            lb_printedVerify = false;
        }
        secBean.setLb_printedVerify(lb_printedVerify);
        
    }

    private void verifyFacialHash(String grp_nbr) throws IOException {
        boolean lb_facialVerify;
        byte[] hash_new;
        byte[] hash_original;
        hash_new = getMessageHash(util.hex1ToByteArray(str_hexFacial));

        hash_original = getOriginalHash(grp_nbr);
        if (Arrays.equals(hash_new, hash_original)) {
            lb_facialVerify = true;

        } else {
            lb_facialVerify = false;
        }
        
        secBean.setLb_facialVerify(lb_facialVerify);
    }

    private void verifyFingerHash(String grp_nbr) throws IOException {
        boolean lb_fingerVerify;
        byte[] hash_new;
        byte[] hash_original;
        hash_new = getMessageHash(util.hex1ToByteArray(str_hexFinger));

        hash_original = getOriginalHash(grp_nbr);


        if (Arrays.equals(hash_new, hash_original)) {
            lb_fingerVerify = true;

        } else {
            lb_fingerVerify = false;
        }
        
        secBean.setLb_fingerVerify(lb_fingerVerify);
    }

    private byte[] getMessageHash(byte[] input_data) throws IOException {

        byte[] hash;

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        digest.update(input_data);
        hash = digest.digest();


        return hash;

    }

    private byte[] getOriginalHash(String grp_nbr) {

        byte[] hash = null;
        String seq_tag = "30";
        String str_grps_len;
        String str_hex_grps=null;
        String grp_nbr_extract;
        String str_grp_hash_len;
        String str_grp_hash = null;
        int grp_len, grp_hash_len;
        int i_startPos;
        int tag_loc;

        // First of all find the position of OID of LDS Security Object
        i_startPos = util.findEndLocation(str_hexSec, LDSOID);
        String str_hexSec_1 = str_hexSec.substring(i_startPos);

        i_startPos = util.findEndLocation(str_hexSec_1, SHA512WithRSA);
        str_hexSec_1 = str_hexSec_1.substring(i_startPos);

        tag_loc = util.findtagLocation(0, str_hexSec_1, seq_tag);

        if (tag_loc == -1) {
            return hash;
        }
        if (tag_loc >= 0) {
            switch (str_hexSec_1.substring(tag_loc + 2, tag_loc + 4)) {
                case "82":
                    str_grps_len = str_hexSec_1.substring(tag_loc + 4, tag_loc + 8);
                    grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                    str_hex_grps = str_hexSec_1.substring(tag_loc + 8, tag_loc + 8 + grp_len * 2);
                    break;
                case "81":
                    str_grps_len = str_hexSec_1.substring(tag_loc + 4, tag_loc + 6);
                    grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                    str_hex_grps = str_hexSec_1.substring(tag_loc + 6, tag_loc + 6 + grp_len * 2);
                    break;
                default:
                    str_grps_len = str_hexSec_1.substring(tag_loc + 2, tag_loc + 4);
                    grp_len = Integer.valueOf(str_grps_len, 16).intValue();
                    str_hex_grps = str_hexSec_1.substring(tag_loc + 4, tag_loc + 4 + grp_len * 2);
                    break;
            }



        }


        for (int i = 0; i < str_hex_grps.length(); ) {

            // Read the 5th byte for group number.
            grp_nbr_extract = str_hex_grps.substring(i + 8, i + 10);
            // Read the 7th Byte for length of hash bytes for the above grp_number
            str_grp_hash_len = str_hex_grps.substring(i + 12, i + 14);
            grp_hash_len = Integer.valueOf(str_grp_hash_len, 16).intValue();


            if (grp_nbr_extract.equals(grp_nbr)) {
                if (grp_count == 0) {
                    str_grp_hash = str_hex_grps.substring(i + 14);
                } else {
                    str_grp_hash = str_hex_grps.substring(i + 14, i + 14 + grp_hash_len * 2);
                }

                break;
            }


            //If it does not match group nbr, then skip the group and check the next one.
            grp_count--;
            i = i + 14 + grp_hash_len * 2;
        }


        hash = util.hex1ToByteArray(str_grp_hash);


        return hash;

    }
}
