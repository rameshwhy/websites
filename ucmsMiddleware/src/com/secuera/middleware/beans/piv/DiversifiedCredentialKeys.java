/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

/**
 *
 * @author SEAdmin
 */
public class DiversifiedCredentialKeys {
    
    private byte[] diversifiedEncKey;
    private byte[] diversifiedMacKey;
    private byte[] diversifiedDekKey;
    private byte[] diversifiedAdminKey;

    public byte[] getDiversifiedAdminKey() {
        return diversifiedAdminKey;
    }

    public void setDiversifiedAdminKey(byte[] diversifiedAdminKey) {
        this.diversifiedAdminKey = diversifiedAdminKey;
    }

    public byte[] getDiversifiedDekKey() {
        return diversifiedDekKey;
    }

    public void setDiversifiedDekKey(byte[] diversifiedDekKey) {
        this.diversifiedDekKey = diversifiedDekKey;
    }

    public byte[] getDiversifiedEncKey() {
        return diversifiedEncKey;
    }

    public void setDiversifiedEncKey(byte[] diversifiedEncKey) {
        this.diversifiedEncKey = diversifiedEncKey;
    }

    public byte[] getDiversifiedMacKey() {
        return diversifiedMacKey;
    }

    public void setDiversifiedMacKey(byte[] diversifiedMacKey) {
        this.diversifiedMacKey = diversifiedMacKey;
    }

    
    
    
    
}
