package com.id3.fingerprint;

public class FingerSlapStatus
{
  public static final int NotSegmented = 0;
  public static final int Success = 1;
  public static final int FewerFingers = 2;
  public static final int WrongHand = 3;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerSlapStatus
 * JD-Core Version:    0.6.2
 */