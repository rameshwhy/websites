// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena;


public class MorenaException extends Exception
{

    public MorenaException(int i, String s, String s1, int j)
    {
        super((new StringBuilder()).append(s).append(" (").append(s1).append(" [").append(j).append("])").toString());
        errorCode = 0;
        location = null;
        errorCode = i;
        location = (new StringBuilder()).append(s1).append(" [").append(j).append("]").toString();
    }

    public int getErrorCode()
    {
        return errorCode;
    }

    public String getLocation()
    {
        return location;
    }

    private static final long serialVersionUID = 0xf563ba18579e9e84L;
    private int errorCode;
    private String location;
}
