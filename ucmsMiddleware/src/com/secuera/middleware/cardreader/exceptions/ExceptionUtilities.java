/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
/**
 *
 * @author admin
 */

public class ExceptionUtilities {
	/**
	 * Check an Exception. If it is a MiddleWareException return it as-is. Otherwise,
	 * return a MiddleWareException using its contents.
	 * 
	 * @param t
	 *            - The Throwable (We should not be catching Throwable, but
	 *            again, pre-existing behavior)
	 * @return - The MiddleWareException to use.protected
	 */
	public static MiddleWareException createIfNotMiddleWareException(Throwable t) {
		MiddleWareException re = null;
		if (t instanceof MiddleWareException) {
			re = (MiddleWareException) t;
		} else {
			re = createMiddleWareException("",t);
		}
		return re;
	}

	/**
	 * createMiddleWareException creates a new exception using the existing exception
	 * along with a stack trace.
	 * 
	 * @param t
	 *            - The throwable caught
	 * @return - The MiddleWareException to use.
	 */
	protected static MiddleWareException createMiddleWareException(String msg, Throwable t) {
		LoggerWrapper.entering("MiddleWareServerImpl", "createMiddleWareException");

		MiddleWareException e = null;
		String s = "[Stack Trace] " + getStackTrace(t);

		if (t instanceof MiddleWareException) {

			MiddleWareException r = (MiddleWareException) t;

			String digest = r.getDigest();
			if (digest != null)
				digest = digest + " " + s;
			else
				digest = s;

			e = new MiddleWareException(r.getErrorCode(), r.getErrorSeverity(),
					r.getErrorLocation(), r.getErrorMsg(), digest);
		} else
			e = new MiddleWareException(0, 0, "", t.getMessage(), s);

		return e;
	}
	
	/**
	 * getStackTrace extracts the stack trace from Throwable and stores it into
	 * a string buffer to be returned to the caller
	 * 
	 * @param t
	 *            - throwable to extract message
	 * @return stack trace in string format
	 */
	protected static String getStackTrace(Throwable t) {
		LoggerWrapper.entering("MiddleWareServerImpl", "getStackTrace");
		// didn't use SC's CommonFuncs.getStackTraceString because it adds
		// too many words along with the stack trace making it harder to
		// read.
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		String stackTrace = "Throwable is null";
		if (t != null) {
			t.printStackTrace(pw);
			stackTrace = sw.getBuffer().toString();
		}
		return stackTrace;
	}
	
	
}

