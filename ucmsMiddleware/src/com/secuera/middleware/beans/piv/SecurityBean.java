/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

/**
 *
 * @author Harpreet Singh
 */
public class SecurityBean {

    private boolean lb_sigVerify;
    private boolean lb_chuidVerify;
    private boolean lb_printedVerify;
    private boolean lb_facialVerify;
    private boolean lb_fingerVerify;
    
    public boolean isLb_sigVerify() {
        return lb_sigVerify;
    }

    public void setLb_sigVerify(boolean lb_sigVerify) {
        this.lb_sigVerify = lb_sigVerify;
    }

    public boolean isLb_chuidVerify() {
        return lb_chuidVerify;
    }

    public void setLb_chuidVerify(boolean lb_chuidVerify) {
        this.lb_chuidVerify = lb_chuidVerify;
    }

    public boolean isLb_printedVerify() {
        return lb_printedVerify;
    }

    public void setLb_printedVerify(boolean lb_printedVerify) {
        this.lb_printedVerify = lb_printedVerify;
    }

    public boolean isLb_facialVerify() {
        return lb_facialVerify;
    }

    public void setLb_facialVerify(boolean lb_facialVerify) {
        this.lb_facialVerify = lb_facialVerify;
    }

    public boolean isLb_fingerVerify() {
        return lb_fingerVerify;
    }

    public void setLb_fingerVerify(boolean lb_fingerVerify) {
        this.lb_fingerVerify = lb_fingerVerify;
    }
    
   
}
