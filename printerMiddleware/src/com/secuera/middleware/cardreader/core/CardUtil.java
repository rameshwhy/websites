/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import com.secuera.middleware.cardreader.beans.InitializationBean;
import com.secuera.middleware.cardreader.beans.SecureChannelBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import java.util.Arrays;

/**
 *
 * @author admin
 */
public class CardUtil implements CardUtilInterface {

    // declare class variable for different keys
    private static byte[] CARD_MANAGER_AID;
    private byte[] S_MAC_KEY;
    private byte[] PREV_MAC;
    private byte[] IV_AES = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    private String card_unlock = "6283";
    private byte[] SCP03 = {(byte) 0x2B, 0x06, 0x01, 0x04, 0x01, (byte) 0x81, (byte) 0xEF, 0x6F, 0x02, 0x04, 0x03, 0x15};
    private byte[] SCP01 = {(byte) 0x2A, (byte) 0x86, 0x48, (byte) 0x86, (byte) 0xFC, 0x6B, 0x04, 0x01, 0x05};
    // *****************************************************************
    // Create instances of other classes.
    // *****************************************************************
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    GeneralAuth auth = new GeneralAuth();
    InitializationBean initValues = new InitializationBean();
    SecureChannelBean scbean = new SecureChannelBean();

    // Constructor get initial class values
    public CardUtil(InitializationBean iValues) {
        initValues = iValues;
        CARD_MANAGER_AID = iValues.getCARD_MANAGER_AID();
    }

    @Override
    public boolean CardLock(CardChannel channel) throws MiddleWareException {

        byte[] scp_data;

        try {
            selectSecurityDomain(channel);

            scp_data = getCardData(channel);

            if (Arrays.equals(scp_data, SCP03)) {
                //open secure channel
                SecureChannel03 scp03 = new SecureChannel03(initValues);
                scbean = scp03.openSecureChannel03(channel, false);

                //get values from secure channel 03
                S_MAC_KEY = scbean.getS_MAC_KEY();
                PREV_MAC = scbean.getPREV_MAC();

                // Disable contact less communication.
                disableContactless03(channel);
                lockCardManager03(channel);

            } else {
                //open secure channel
                SecureChannel01 sc01 = new SecureChannel01(initValues);
                scbean = sc01.openSecureChannel01(channel);

                //get values from secure channel
                S_MAC_KEY = scbean.getS_MAC_KEY();
                PREV_MAC = scbean.getPREV_MAC();

                // Lock Card Manager
                disableContactless(channel);
                lockCardManager(channel);
            }

        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean CardUnlock(CardChannel channel) throws MiddleWareException {

        byte[] scp_data;
        boolean retValue = true;

        try {
            //check if the card is locked
            retValue = selectManager(channel, false);

            //unlock card if card is locked
            if (retValue == true) {
                selectSecurityDomain(channel);
                scp_data = getCardData(channel);

                if (Arrays.equals(scp_data, SCP03)) {

                    SecureChannel03 scp03 = new SecureChannel03(initValues);
                    scbean = scp03.openSecureChannel03(channel, false);

                    //get values from secure channel 03
                    S_MAC_KEY = scbean.getS_MAC_KEY();
                    PREV_MAC = scbean.getPREV_MAC();

                    unlockCard03(channel);

                } else {

                    //open secure channel
                    SecureChannel01 sc01 = new SecureChannel01(initValues);
                    scbean = sc01.openSecureChannel01(channel);

                    //get values from secure channel
                    S_MAC_KEY = scbean.getS_MAC_KEY();
                    PREV_MAC = scbean.getPREV_MAC();

                    unlockCard(channel);

                }

            }
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return retValue;
    }

    /* select Security Domain method */
    void selectSecurityDomain(CardChannel channel) throws MiddleWareException {

        // **----------------------------------------------
        // ** Check presence and status of PIV-SD
        // **----------------------------------------------
        // ; Select PIV Application Security Domain using Oberthur registered
        // AID
        // 00 A4 04 00 10 A0 00 00 00 77 01 00 00 06 10 00 FD 00 00 00 27 (9000,
        // 6A82)
        // 00 A4 04 00 
        //Recv : 6F 3C 84 07
        //A0 00 00 01 51 00 00
        //A5 31 9F 6E 2A 48 20 50 2B 82 31 80 30 00 63 31 16 00 00 00 0C 00 00 14 32 31 16 14 33 31 16 14 34 31 16 00 00 00 00 14 35 31 16 00 00 00 00 9F 65 01 FF 



        StringBuffer sb = new StringBuffer();
        byte[] apdu_security_piv = {0x00, (byte) 0xA4, 0x04, 0x00, 0x10,
            (byte) 0xA0, 0x00, 0x00, 0x00, 0x77, 0x01, 0x00, 0x00, 0x06,
            0x10, 0x00, (byte) 0xFD, 0x00, 0x00, 0x00, 0x27};

        CommandAPDU SEC_APDU_PIV = new CommandAPDU(apdu_security_piv);

        try {
            sendCard.sendCommand(channel, SEC_APDU_PIV, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // ; Select PIV Application Issuer Security Domain
        // 00 A4 04 00 07 A0 00 00 01 51 00 00 (9000, 6A82)

        byte[] apdu_security = {0x00, (byte) 0xA4, 0x04, 0x00, 0x07,
            (byte) 0xA0, 0x00, 0x00, 0x01, 0x51, 0x00, 0x00};

        CommandAPDU SEC_APDU = new CommandAPDU(apdu_security);

        try {
            sendCard.sendCommand(channel, SEC_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    /* select Card Manager method */
    private byte[] getCardData(CardChannel channel) throws MiddleWareException {

        // Get Card Recognition Data to find out SCP protocol to use
        // I(1;1) CA 0066 00 (9000)
        //   73 32 
        // 06 07 2a864886fc6b01
        //600c060a2a864886fc6b02020101
        //63 09 06 07 2a 86 48 86 fc 6b 03
        //64 0e 06 0c 2b 06 01 04 01 81 ef 6f 02 04 03 15


        // 73 2f 
        //  06 07 2a 86 48 86 fc 6b 01 
        //  60 0c 06 0a 2a 86 48 86 fc 6b 02 02 01 01 
        //  63 09 06 07 2a 86 48 86 fc 6b 03 
        //  64 0b 06 09 2a 86 48 86 fc 6b 04 01 05
        // 2A864886FC6B040105


        StringBuffer sb = new StringBuffer();
        String scp_tag = "64";
        String str_scp_data;
        String str_res;
        int tag_loc;
        byte[] res = null;
        byte[] scp_data = null;
        byte[] apdu_manager = {0x00, (byte) 0xCA, 0x00, 0x66, 0x00};

        CommandAPDU INIT_APDU = new CommandAPDU(apdu_manager);

        try {
            res = sendCard.sendCommand(channel, INIT_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        System.out.println("Card Data: " + util.arrayToHex(res));

        str_res = util.arrayToHex(res);
        // Get Secure Channel Protocol Data.
        tag_loc = util.findtagLocation(0, str_res, scp_tag);
        String scp_length = str_res.substring(tag_loc + 6, tag_loc + 8);
        int i = Integer.valueOf(scp_length, 16).intValue();

        str_scp_data = str_res.substring(tag_loc + 8, tag_loc + 8 + i * 2);

        scp_data = util.hex1ToByteArray(str_scp_data);

        return scp_data;
    }

    void disableContactless(CardChannel channel) throws MiddleWareException {

        // Now compute new MAC and prepare APDU command for Key Injection
        StringBuffer sb = new StringBuffer();

        // *****************************************************//
        // 80 E2 80 00 04 DF 6A 01 10 ---Original Command.
        // 84 ---> 80 is changed to 84 to indicate Secure messaging by setting
        // bit # 3.
        // 0C----> 04 to 0C---added 8 byte length to accommodate for MAC
        // 84 E2 80 00 0C DF 6A 01 10

        byte[] apdu_data = {(byte) 0x84, (byte) 0xE2, (byte) 0x80, 0x00, 0x0C,
            (byte) 0xDF, 0X6A, 0x01, 0x10};
        byte[] mac_pad = {(byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

        // Set Session mac key.
        byte[] AUTH_MAC_KEY_24 = util.combine_data(S_MAC_KEY,
                util.extract_data(S_MAC_KEY, 0, 8));

        byte[] enc_mac = util.combine_data(apdu_data, mac_pad);

        enc_mac = macencrypt(AUTH_MAC_KEY_24, PREV_MAC, enc_mac);

        byte[] enc_mac_8 = util.extract_data(enc_mac, 8, 8);

        // ///////////
        PREV_MAC = enc_mac_8; // Store the Mac for next comand to card.

        apdu_data = util.combine_data(apdu_data, enc_mac_8);

        CommandAPDU EXT_APDU = new CommandAPDU(apdu_data);

        try {

            sendCard.sendCommand(channel, EXT_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessages.DISABLECONTACTLESS_EXCEPTION);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }

    }

    void disableContactless03(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();

        // *****************************************************//
        // 80 E2 80 00 04 DF 6A 01 10 ---Original Command.
        // 84 ---> 80 is changed to 84 to indicate Secure messaging by setting
        // bit # 3.
        // 0C----> 04 to 0C---added 8 byte length to accommodate for MAC
        // 84 E2 80 00 0C DF 6A 01 10
        byte[] zero_card_pad = {(byte) 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00};

        byte[] apdu_data = {(byte) 0x84, (byte) 0xE2, (byte) 0x80, 0x00, 0x0C,
            (byte) 0xDF, 0X6A, 0x01, 0x10};

        //byte[] pin_command_data = util.combine_data(apdu_data, pin_data);
        // 16 + 9 + 7 = 32

        byte[] set_data = util.combine_data(PREV_MAC, zero_card_pad);
        set_data = util.combine_data(set_data, apdu_data);
        // Now pad with following bytes to make the above buffer multiple of 16
        // bytes
        byte[] pad = {(byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        //, 0x00, 0x00, 0x00, 0x00};

        set_data = util.combine_data(set_data, pad);

        //15 ff 74 1e b2 7e 63 bf 00 00 00 00 00 00 00 00 
        //84 e2 80 00 0c df 6a 01 10 80 00 00 00 00 00 00
        System.out.println("ENCRYPT DATA :" + util.arrayToHex(set_data));

        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, set_data, true);

        System.out.println("ENCRYPT DATA :" + util.arrayToHex(encrypt_data));

        //cb585b70cd16e74d9dfb4f22533bf62c
        //db576b32260fc8b487a6fcc93bc98fdb
        //84 e2 80 00 0c df 6a 01 10 db 57 6b 32 26 0f c8 b4
        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        PREV_MAC = util.extract_data(encrypt_data, 16, 8);

        // Now set the APDU command for PIN Upload.
        apdu_data = util.combine_data(apdu_data, PREV_MAC);

        System.out.println("APDU DATA :" + util.arrayToHex(apdu_data));

        //CommandAPDU apdu_APDU = new CommandAPDU(apdu_data);

        //


        // Set Session mac key.
        //byte[] AUTH_MAC_KEY_24 = util.combine_data(S_MAC_KEY,
        //       util.extract_data(S_MAC_KEY, 0, 8));

        //byte[] enc_mac = util.combine_data(apdu_data, mac_pad);

        //enc_mac = macencrypt(AUTH_MAC_KEY_24, PREV_MAC, enc_mac);

        //byte[] enc_mac_8 = util.extract_data(enc_mac, 8, 8);

        // ///////////
        //PREV_MAC = enc_mac_8; // Store the Mac for next comand to card.
        //apdu_data = util.combine_data(apdu_data, enc_mac_8);

        CommandAPDU EXT_APDU = new CommandAPDU(apdu_data);

        try {

            sendCard.sendCommand(channel, EXT_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessages.DISABLECONTACTLESS_EXCEPTION);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }
    }

    /* Lock Card */
    void lockCardManager(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();


        // Command : 84 F0 80 7F 0F
        // Input Data : A0 00 00 01 51 00 00 4C 0A B4 29 D8 68 37 2B

        // 84 ---> 80 is changed to 84 to indicate Secure messaging by setting
        // bit # 3.
        // 0F (LC) ---> LC is changed from 07 to 0F (to count for length of MAC
        // added in command data field )
        byte[] apdu_data = {(byte) 0x84, (byte) 0xF0, (byte) 0x80, 0x7F, 0x0F};

        byte[] apdu_pad = {(byte) 0x80, 0x00, 0x00, 0x00};

        apdu_data = util.combine_data(apdu_data, CARD_MANAGER_AID);

        byte[] apdu_data_encrypt = util.combine_data(apdu_data, apdu_pad);

        // Now Encrypt the command data field.
        byte[] AUTH_MAC_KEY_24 = util.combine_data(S_MAC_KEY,
                util.extract_data(S_MAC_KEY, 0, 8));

        PREV_MAC = util.extract_data(
                macencrypt(AUTH_MAC_KEY_24, PREV_MAC, apdu_data_encrypt), 8, 8);

        byte[] apdu_command = util.combine_data(apdu_data, PREV_MAC);

        CommandAPDU LOCK_APDU = new CommandAPDU(apdu_command);

        try {

            sendCard.sendCommand(channel, LOCK_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessages.CARD_LOCK_EXCEPTION);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }
    }

    byte[] macencrypt(byte[] key, byte[] vector, byte[] from_card) throws MiddleWareException {

        MacEncrypter encrypter = new MacEncrypter(key, vector);

        // Encrypt
        byte[] res = null;
        try {
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        return res;

    }

    /* select Card Manager method */
    private boolean selectManager(CardChannel channel, Boolean logicalChannel) throws MiddleWareException {

        // Card manager selection on logical channel 0
        // 00 A4 04 00 00 (6CXX, 6283, 9000) ; Status 6283 is returned when the
        // CM is locked

        StringBuffer sb = new StringBuffer();
        boolean retValue = false;

        byte[] log_ch0 = {0x00, (byte) 0xA4, 0x04, 0x00, 0x00};
        byte[] log_ch1 = {0x01, (byte) 0xA4, 0x04, 0x00, 0x00};

        byte[] apdu_manager;

        if (logicalChannel) {
            apdu_manager = log_ch1;
        } else {
            apdu_manager = log_ch0;
        }


        CommandAPDU INIT_APDU = new CommandAPDU(apdu_manager);

        try {
            sendCard.sendCommand(channel, INIT_APDU, sb);

            // check if success return true
            if (sb.toString().equals(card_unlock)) {
                retValue = true;
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return retValue;
    }

    /* Unlock Card */
    private void unlockCard(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        // 84 ---> 80 is changed to 84 to indicate Secure messaging by setting
        // bit # 3.
        // 0F (LC) ---> LC is changed from 07 to 0F (to count for length of MAC
        // added in command data field )
        byte[] apdu_data = {(byte) 0x84, (byte) 0xF0, (byte) 0x80, 0x0F, 0x0F,
            (byte) 0xA0, 0x00, 0x00, 0x01, 0x51, 0x00, 0x00};

        byte[] apdu_pad = {(byte) 0x80, 0x00, 0x00, 0x00};

        byte[] apdu_data_encrypt = util.combine_data(apdu_data, apdu_pad);

        // Now Encrypt the command data field.
        byte[] AUTH_MAC_KEY_24 = util.combine_data(S_MAC_KEY,
                util.extract_data(S_MAC_KEY, 0, 8));

        PREV_MAC = util.extract_data(
                macencrypt(AUTH_MAC_KEY_24, PREV_MAC, apdu_data_encrypt), 8, 8);

        byte[] apdu_command = util.combine_data(apdu_data, PREV_MAC);

        CommandAPDU UNLOCK_APDU = new CommandAPDU(apdu_command);

        try {
            sendCard.sendCommand(channel, UNLOCK_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessages.CARD_UNLOCK_EXCEPTION);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }

    }

    /* Unlock Card */
    private void unlockCard03(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        // 84 ---> 80 is changed to 84 to indicate Secure messaging by setting
        // bit # 3.
        // 0F (LC) ---> LC is changed from 07 to 0F (to count for length of MAC
        // added in command data field )

        byte[] zero_card_pad = {(byte) 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00};

        byte[] apdu_data = {(byte) 0x84, (byte) 0xF0, (byte) 0x80, (byte) 0x0F, 0x0F};

        apdu_data = util.combine_data(apdu_data, CARD_MANAGER_AID);

        byte[] set_data = util.combine_data(PREV_MAC, zero_card_pad);
        set_data = util.combine_data(set_data, apdu_data);
        // Now pad with following bytes to make the above buffer multiple of 16
        // bytes
        byte[] pad = {(byte) 0x80, 0x00, 0x00, 0x00};

        set_data = util.combine_data(set_data, pad);
        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, set_data, true);

        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        PREV_MAC = util.extract_data(encrypt_data, 16, 8);

        // System.out.println("APDU DATA :" + util.arrayToHex(apdu_data));

        /**
         * ******************************************************
         */
        byte[] apdu_command = util.combine_data(apdu_data, PREV_MAC);

        CommandAPDU UNLOCK_APDU = new CommandAPDU(apdu_command);

        try {
            sendCard.sendCommand(channel, UNLOCK_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessages.CARD_UNLOCK_EXCEPTION);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }

    }

    public void checkPIVInstance(CardChannel channel) throws MiddleWareException {

        // ; Check PIV instance FCI
        // 00 A4 04 00 09 A0 00 00 03 08 00 00 10 00 00 [ \

        StringBuffer sb = new StringBuffer();
        byte[] apdu_security_piv = {0x00, (byte) 0xA4, 0x04, 0x00, 0x09,
            (byte) 0xA0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00,
            0x00};

        CommandAPDU SEC_APDU_PIV = new CommandAPDU(apdu_security_piv);

        try {
            sendCard.sendCommand(channel, SEC_APDU_PIV, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    void lockCardManager03(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();

        // Command : 84 F0 80 7F 0F
        // Input Data : A0 00 00 01 51 00 00 4C 0A B4 29 D8 68 37 2B

        // 84 ---> 80 is changed to 84 to indicate Secure messaging by setting
        // bit # 3.
        // 0F (LC) ---> LC is changed from 07 to 0F (to count for length of MAC
        // added in command data field )
        /**
         * *****************************************************
         */
        byte[] zero_card_pad = {(byte) 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00};

        byte[] apdu_data = {(byte) 0x84, (byte) 0xF0, (byte) 0x80, (byte) 0x7F, 0x0F};

        apdu_data = util.combine_data(apdu_data, CARD_MANAGER_AID);

        byte[] set_data = util.combine_data(PREV_MAC, zero_card_pad);
        set_data = util.combine_data(set_data, apdu_data);
        // Now pad with following bytes to make the above buffer multiple of 16
        // bytes
        byte[] pad = {(byte) 0x80, 0x00, 0x00, 0x00};

        set_data = util.combine_data(set_data, pad);
        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, set_data, true);


        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        PREV_MAC = util.extract_data(encrypt_data, 16, 8);

        // Now set the APDU command for PIN Upload.
        apdu_data = util.combine_data(apdu_data, PREV_MAC);


        /**
         * ******************************************************************************
         */
        CommandAPDU LOCK_APDU = new CommandAPDU(apdu_data);

        try {

            sendCard.sendCommand(channel, LOCK_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessages.CARD_LOCK_EXCEPTION);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }
    }

    public byte[] aesencrypt(byte[] key, byte[] from_card, boolean vector) throws MiddleWareException {

        // Create encrypter/decrypter class
        AesEncrypter encrypter;
        byte[] res = null;
        try {

            if (vector) {
                encrypter = new AesEncrypter(key, IV_AES);
            }
            {
                encrypter = new AesEncrypter(key);
            }

            // Encrypt
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return res;

    }

    void lockCardManagertest(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();


        // Command : 84 F0 80 7F 0F
        // Input Data : A0 00 00 01 51 00 00 4C 0A B4 29 D8 68 37 2B

        // 84 ---> 80 is changed to 84 to indicate Secure messaging by setting
        // bit # 3.
        // 0F (LC) ---> LC is changed from 07 to 0F (to count for length of MAC
        // added in command data field )
        /**
         * ***********************************************
         */
        byte[] apdu_data = {(byte) 0x80, (byte) 0xF0, (byte) 0x40, (byte) 0x7F, 0x07};

        apdu_data = util.combine_data(apdu_data, CARD_MANAGER_AID);


        //84 f0 80 7f 0f 
        //84 f0 80 7f 0f 
        //a0 00 00 01 51 00 00 
        //92 33 e3 26 50 9d 0d 2f
        System.out.println("APDU DATA :" + util.arrayToHex(apdu_data));

        /**
         * ******************************************************************************
         */
        CommandAPDU LOCK_APDU = new CommandAPDU(apdu_data);

        try {

            sendCard.sendCommand(channel, LOCK_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessages.CARD_LOCK_EXCEPTION);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }
    }

    void getStatus(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();

        // Command : 84 F0 80 7F 0F
        // Input Data : A0 00 00 01 51 00 00 4C 0A B4 29 D8 68 37 2B

        // 84 ---> 80 is changed to 84 to indicate Secure messaging by setting
        // bit # 3.
        // 0F (LC) ---> LC is changed from 07 to 0F (to count for length of MAC
        // added in command data field )

        byte[] zero_card_pad = {(byte) 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00};

        byte[] apdu_data = {(byte) 0x84, (byte) 0xF2, (byte) 0x80, (byte) 0x20, 0x0A, 0x4F, 0x00};

        //apdu_data = util.combine_data(apdu_data, CARD_MANAGER_AID);

        byte[] set_data = util.combine_data(PREV_MAC, zero_card_pad);
        set_data = util.combine_data(set_data, apdu_data);
        // Now pad with following bytes to make the above buffer multiple of 16
        // bytes
        byte[] pad = {(byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        //0x00,0x00};

        set_data = util.combine_data(set_data, pad);

        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, set_data, true);

        System.out.println("ENCRYPT DATA :" + util.arrayToHex(encrypt_data));

        //19 b2 2a a7 d2 13 5e d2 f7 1e d6 9c f1 1c c8 20 
        //92 33 e3 26 50 9d 0d 2f 52 7d ac f9 18 cf 33 8f

        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        PREV_MAC = util.extract_data(encrypt_data, 16, 8);

        // Now set the APDU command for PIN Upload.
        apdu_data = util.combine_data(apdu_data, PREV_MAC);

        /**
         * ******************************************************************************
         */
        CommandAPDU LOCK_APDU = new CommandAPDU(apdu_data);

        try {

            sendCard.sendCommand(channel, LOCK_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessages.CARD_LOCK_EXCEPTION);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }
    }
}
