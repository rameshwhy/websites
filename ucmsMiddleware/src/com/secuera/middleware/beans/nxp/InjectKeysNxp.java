/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.nxp;

/**
 *
 * @author Harpreet Singh
 */
public class InjectKeysNxp {
    
    private byte[] eac_encKey;
    private byte[] eac_macKey;

    private byte[] epki_encKey;
    private byte[] epki_macKey;
    
    private byte[] moc_encKey;
    private byte[] moc_macKey;

    public byte[] getEac_encKey() {
        return eac_encKey;
    }

    public void setEac_encKey(byte[] eac_encKey) {
        this.eac_encKey = eac_encKey;
    }

    public byte[] getEac_macKey() {
        return eac_macKey;
    }

    public void setEac_macKey(byte[] eac_macKey) {
        this.eac_macKey = eac_macKey;
    }

    public byte[] getEpki_encKey() {
        return epki_encKey;
    }

    public void setEpki_encKey(byte[] epki_encKey) {
        this.epki_encKey = epki_encKey;
    }

    public byte[] getEpki_macKey() {
        return epki_macKey;
    }

    public void setEpki_macKey(byte[] epki_macKey) {
        this.epki_macKey = epki_macKey;
    }

    public byte[] getMoc_encKey() {
        return moc_encKey;
    }

    public void setMoc_encKey(byte[] moc_encKey) {
        this.moc_encKey = moc_encKey;
    }

    public byte[] getMoc_macKey() {
        return moc_macKey;
    }

    public void setMoc_macKey(byte[] moc_macKey) {
        this.moc_macKey = moc_macKey;
    }
    
    
}
