package com.secuera.middleware.core.piv;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date; 




/*
 * This class is very specific to connect to Microsoft CA.
 * This class Exposes the functions to get the URL
 */
class MicrosoftCertServer {
    /*
     * This function is used to get the Cert Approval ID from the server
     * This Approval ID will be used to download the certificate from CA Server
     * 
     * 
     * */

    public String HttpGetApprovalID(String certCSRBase64Encoded, String inURL) throws Exception {
        String message = "Requesting Certificate from Server Failed ";
        BufferedReader in = null;

        try {
            //URL url = new URL("http://win-h0e2ce40dp0/certsrv/"+"certfnsh.asp");//from front end
            URL url = new URL(inURL);//from front end
            String pkcs10 = certCSRBase64Encoded;

            String data = "Mode=newreq&CertRequest=";
            String agent = "&CertAttrib=UserAgent ";
            String userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8";
            String property = userAgent;

            String certRequestID = null;

            String friendlyname = "FriendlyType=Saved-Request Certificate " + (new Date()).toLocaleString();
            String thumbprint = "&ThumbPrint=&TargetStoreFlags=0&SaveCert=yes";

            data += URLEncoder.encode(pkcs10);
            data += agent;
            data += URLEncoder.encode(property);
            data += "&" + URLEncoder.encode(friendlyname);
            data += thumbprint;


            URLConnection conn = url.openConnection();

          //--------------------
                  

         
          //----------------------        

            conn.setConnectTimeout(400000);
            //conn.setAllowUserInteraction(false);
            conn.setRequestProperty("User-Agent", userAgent);

            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            // Get the response
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()), 2048);

            String line = "";

            while ((line = in.readLine()) != null) {
                //CA Auto-issued the certificate
                if (line.contains("certnew.cer") && !line.contains("CACert")) {
                    int start = 0;
                    int end = 0;

                    start = line.indexOf("ReqID");
                    end = line.indexOf("&", start);
                    certRequestID = line.substring(start + 6, end);

                    message = "The certificate you requested was issued to you"
                            + "\n  Your Request Id is: " + certRequestID;
                    // Break the loop as soon as request id is captured
                    break;
                }

                //Certificate is pending and waiting for approval by CA			
                if (line.contains("Your") && line.contains("Request") && line.contains("Id")) {
                    int start = 0;
                    int end = 0;

                    start = line.indexOf("is");
                    end = line.indexOf(".", start);
                    certRequestID = line.substring(start + 3, end);
                    //return this message to display to user
                    message = "  Your certificate request has been receieved and is pending for"
                            + "an administrator to issue the certificate you requested.\n";
                    message += "  Your Request Id is: " + certRequestID;
                }

            }

            return certRequestID;
        } catch (Exception e) {
           throw new Exception("Error processing your certificate request to SCEP server\n" + e.getMessage());
        }
    }

    // this function is used to check whether the server is available or not
    public boolean canConnectToCAServer(String serverUrl) throws IOException {
        boolean retVal = true;
        try {
            URL url = new URL(serverUrl);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            String contentType = conn.getContentType();
            if (contentType == null) {
                retVal = false;
                throw new IOException("Cannot Connect to server Check URL address and connection to the server");
            }

        } catch (IOException e) {
            throw e;
        }
        return retVal;
    }
    //String urlCA = "http://win-h0e2ce40dp0/certsrv/";
    //String urlCA = "http://secuera-gst-01.ca.secuera.com/certsrv/";
    // This function is used to download the certificate from the MS CA Server and into .der format and convert the .der format to .pem format
    // Dependency is on the Open SSL to convert it to .pem format
    //public byte[] HttpGetUserCert(String reqID,String userCertPath, String userPemPath) throws Exception

    public byte[] HttpGetUserCert(String reqID, String inURL) throws Exception {
        //BufferedReader inPend = null;

        try {
            String userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8";
            //satish String data = urlCA + "certnew.cer?ReqID=" + reqID;
            String data = inURL + "certnew.cer?ReqID=" + reqID;
            //Encoding b64= Base64; bin=DER
            data += "&Enc=bin";

            //Setting up socket connection and type of browser format
            URL url = new URL(data);
            URLConnection conn = url.openConnection();
            conn.setRequestProperty("User-Agent", userAgent);

            conn.setDoOutput(true);
            String contentType = conn.getContentType();
            int contentLength = conn.getContentLength();

            //Throw an exception if not a binary file
            if (contentType.startsWith("text/") || contentLength == -1) {
                throw new IOException("An invalid response from server while retrieving certificate\n"
                        + "Check with a CA administrator"
                        + ".");
            }



            //write the binary stream to a DER File
            InputStream raw = conn.getInputStream();
            InputStream in = new BufferedInputStream(raw);
            byte[] buffer = new byte[contentLength];
            int bytesRead = 0;
            int offset = 0;
            while (offset < contentLength) {
                bytesRead = in.read(buffer, offset, buffer.length - offset);
                if (bytesRead == -1) {
                    break;
                }
                offset += bytesRead;
            }

            //System.out.println(offset);
            in.close();

            if (offset != contentLength) {
                throw new IOException("Only read " + offset + " bytes; Expected " + contentLength + " bytes");
            }

            return buffer;

            /*
            FileOutputStream out = new FileOutputStream(userCertPath);
            out.write(buffer);
            out.flush();
            out.close();
            
            BufferedReader reader = new BufferedReader(	new FileReader(userCertPath));
            String line;
            while ((line = reader.readLine()) != null)
            {
            //CA Auto-issued the certificate
            if (line.contains("locInfoChkPnd"))
            {
            throw new Exception("Your certificate request is still pending. You must wait for an " +
            "administrator to issue the certificate");
            }
            }
            reader.close();
            
            //Convert file from DER to PEM encoding
            convertDERToPEM(userCertPath, userPemPath);
             */
        } catch (Exception e) {
            throw e;
        }
    }

    private void convertDERToPEM(String derFilePath, String pemFilePath) throws Exception {
        try {
            //Convert file from DER to PEM encoding
            File file = new File(pemFilePath);
            if (file.exists()) {
                file.delete();

            }
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec(
                    "C:\\Program Files (x86)\\GnuWin32\\bin\\openssl x509 -inform der -in " + derFilePath + " -out " + pemFilePath);

            int exitVal = process.waitFor();
            if (exitVal != 0) {
                throw new Exception("SSL PEM conversion failed\n");
            }
        } catch (IOException e) {
            //e.printStackTrace();
            throw e;
        }
    }
}