/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.biometrics.applet;

import com.secuera.middleware.biometrics.*;

/**
 *
 * @author Satish K
 */
public class BiometricResponseBean {
    private BiometricError biometricError;
    private Object biometricResponse;

    public BiometricError getBiometricError() {
        return biometricError;
    }

    public void setBiometricError(BiometricError biometricError) {
        this.biometricError = biometricError;
    }

    public Object getBiometricResponse() {
        return biometricResponse;
    }

    public void setBiometricResponse(Object biometricResponse) {
        this.biometricResponse = biometricResponse;
    }
    
    @Override
    public String toString() {
        return "BiometricResponseBean [biometricError=" + biometricError
                + ", biometricResponse=" + biometricResponse + "]";
    }
    
}
