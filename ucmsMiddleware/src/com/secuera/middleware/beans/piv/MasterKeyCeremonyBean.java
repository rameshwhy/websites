/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import com.secuera.middleware.core.common.CommonUtil;

/**
 *
 * @author Harpreet Singh
 */
public class MasterKeyCeremonyBean {
    
    public byte[] compKey1;
    public byte[] compKey2;
    public byte[] compKey3;
    
    public byte[] kcv1;
    public byte[] kcv2;
    public byte[] kcv3;
    
    public byte[] zcmkcv;
    
    public byte[] cryptogram;
    public byte[] kcv;
    public String algo;
    
    CommonUtil cmnUtil = new CommonUtil();
    
    public byte[] getCompKey1() {
        return compKey1;
    }

    public void setCompKey1(String compKey1) {
        this.compKey1 = cmnUtil.hex1ToByteArray(compKey1);
    }

    public byte[] getCompKey2() {
        return compKey2;
    }

    public void setCompKey2(String compKey2) {
        this.compKey2 = cmnUtil.hex1ToByteArray(compKey2);
    }

    public byte[] getCompKey3() {
        return compKey3;
    }

    public void setCompKey3(String compKey3) {
        this.compKey3 = cmnUtil.hex1ToByteArray(compKey3);
    }

    public byte[] getKcv1() {
        return kcv1;
    }

    public void setKcv1(String kcv1) {
        this.kcv1 = cmnUtil.hex1ToByteArray(kcv1);
    }

    public byte[] getKcv2() {
        return kcv2;
    }

    

    public void setKcv2(String kcv2) {
        this.kcv2 = cmnUtil.hex1ToByteArray(kcv2);
    }

    public byte[] getKcv3() {
        return kcv3;
    }

    public void setKcv3(String kcv3) {
        this.kcv3 = cmnUtil.hex1ToByteArray(kcv3);
    }
    
    
    public byte[] getZcmkcv() {
        return zcmkcv;
    }

    public void setZcmkcv(String zcmkcv) {
        this.zcmkcv = cmnUtil.hex1ToByteArray(zcmkcv);
    }

    public String getAlgo() {
        return algo;
    }

    public void setAlgo(String algo) {
        this.algo = algo;
    }

    public byte[] getCryptogram() {
        return cryptogram;
    }

    public void setCryptogram(String cryptogram) {
        this.cryptogram = cmnUtil.hex1ToByteArray(cryptogram);
    }

    

    
    public byte[] getkcv() {
        return kcv;
    }

    public void setkcv(String kcv) {
        this.kcv = cmnUtil.hex1ToByteArray(kcv);
    }

       
    
    
    
}
