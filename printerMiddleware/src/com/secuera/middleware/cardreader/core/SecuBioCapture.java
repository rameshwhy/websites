/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import SecuGen.FDxSDKPro.jni.JSGFPLib;
import SecuGen.FDxSDKPro.jni.SGDeviceInfoParam;
import SecuGen.FDxSDKPro.jni.SGFDxDeviceName;
import SecuGen.FDxSDKPro.jni.SGFDxErrorCode;
import SecuGen.FDxSDKPro.jni.SGFDxSecurityLevel;
import SecuGen.FDxSDKPro.jni.SGFDxTemplateFormat;
import SecuGen.FDxSDKPro.jni.SGFingerInfo;
import SecuGen.FDxSDKPro.jni.SGFingerPosition;
import SecuGen.FDxSDKPro.jni.SGImpressionType;
import SecuGen.FDxSDKPro.jni.SGPPPortAddr;
import com.secuera.middleware.cardreader.beans.BiometricBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 *
 * @author secuera
 */
public class SecuBioCapture {

    private long deviceName;
    private long devicePort;
    private JSGFPLib fp = null;
    private SGDeviceInfoParam deviceInfo = new SGDeviceInfoParam();
    private long ret_err;
    CommonUtil util = new CommonUtil();
    //private boolean[] matched;

    public SecuBioCapture() {
    }

    ;

	

    public boolean startDevice() throws MiddleWareException {

        if (initDevice()) {
            System.out.println("Device Initliazed ");
            return true;
        } else {
            closeDevice();
            throw new MiddleWareException(
                    "Unable to initialize Finger Print Device.");

        }
    }

    public boolean initDevice() {
        this.deviceName = SGFDxDeviceName.SG_DEV_FDU04;
        this.devicePort = SGPPPortAddr.AUTO_DETECT;

        // if (fp == null)
        fp = new JSGFPLib();

        ret_err = fp.Init(this.deviceName);
        if ((fp != null) && (ret_err == SGFDxErrorCode.SGFDX_ERROR_NONE)) {

            ret_err = fp.OpenDevice(devicePort);

            if (ret_err == SGFDxErrorCode.SGFDX_ERROR_NONE) {
                ret_err = fp.GetDeviceInfo(deviceInfo);

                if (ret_err == SGFDxErrorCode.SGFDX_ERROR_NONE) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public byte[] getTemplateSingle() throws MiddleWareException {

        int[] Size = new int[1];
        byte[] ISOminutiae;
        byte[] compact_ISO;
        long lError;
        int[] quality = new int[1];

        lError = SGFDxErrorCode.SGFDX_ERROR_NONE;

        SGFingerInfo fingerInfo = new SGFingerInfo();
        fingerInfo.FingerNumber = SGFingerPosition.SG_FINGPOS_UK;
        fingerInfo.ImageQuality = quality[0];
        fingerInfo.ImpressionType = SGImpressionType.SG_IMPTYPE_LP;
        fingerInfo.ViewNumber = 1;

        // imageBuff = new byte[deviceInfo.imageHeight * deviceInfo.imageWidth];

        // ret_err =fp.SetLedOn(true);
        // Set Template format ISO19794
        lError = fp.SetTemplateFormat(SGFDxTemplateFormat.TEMPLATE_FORMAT_ISO19794);
        // /////////////////////////////////////////////
        // Get Max Template Size for ISO19794
        lError = fp.GetMaxTemplateSize(Size);

        // /////////////////////////////////////////////
        // Create ISO19794 Template for Finger1
        ISOminutiae = new byte[Size[0]];

        BufferedImage fgImage = new BufferedImage(deviceInfo.imageWidth, deviceInfo.imageHeight, BufferedImage.TYPE_BYTE_GRAY);
        byte[] imageBuff = ((java.awt.image.DataBufferByte) fgImage.getRaster().getDataBuffer()).getData();

        lError = fp.GetImageEx(imageBuff, 5 * 1000, 0, 50);

        Credential.setFgImage(fgImage);

        fp.GetImageQuality(deviceInfo.imageWidth, deviceInfo.imageHeight, imageBuff, quality);

        if (lError == SGFDxErrorCode.SGFDX_ERROR_NONE) {
            if (quality[0] == 0) {
                // Message Print;
                throw new MiddleWareException("Image Quality not good.");
            } else {
                lError = fp.CreateTemplate(fingerInfo, imageBuff, ISOminutiae);

                fp.GetTemplateSize(ISOminutiae, Size);

                // System.out.println("Size " + Size[0]);
                // fp.getMinutiae(imageBuff, ISOminutiae);
                // String sFileName="E:\\Rvalide\\input.scc";
                // util.writeByteArrayToFile(sFileName,compact_ISO);

                if (lError == SGFDxErrorCode.SGFDX_ERROR_NONE) {

                    compact_ISO = util.strip_data_end(ISOminutiae, Size[0]);

                    return compact_ISO;

                } else {
                    throw new MiddleWareException("Unable to create Template.");
                }
            }

        } else {
            throw new MiddleWareException("No Image Found.");
        }

    }

    public BiometricBean getTemplateSecu() throws MiddleWareException {

        int[] Size = new int[1];
        byte[] ISOminutiae;
        byte[] compact_ISO;
        long lError;
        int[] quality = new int[1];

        lError = SGFDxErrorCode.SGFDX_ERROR_NONE;

        SGFingerInfo fingerInfo = new SGFingerInfo();
        fingerInfo.FingerNumber = SGFingerPosition.SG_FINGPOS_UK;
        fingerInfo.ImageQuality = quality[0];
        fingerInfo.ImpressionType = SGImpressionType.SG_IMPTYPE_LP;
        fingerInfo.ViewNumber = 1;

        // imageBuff = new byte[deviceInfo.imageHeight * deviceInfo.imageWidth];
        // ret_err =fp.SetLedOn(true);
        // Set Template format ISO19794
        lError = fp.SetTemplateFormat(SGFDxTemplateFormat.TEMPLATE_FORMAT_ISO19794);
        // /////////////////////////////////////////////
        // Get Max Template Size for ISO19794
        lError = fp.GetMaxTemplateSize(Size);

        // /////////////////////////////////////////////
        // Create ISO19794 Template for Finger1
        ISOminutiae = new byte[Size[0]];

        BufferedImage fgImage = new BufferedImage(deviceInfo.imageWidth,
                deviceInfo.imageHeight, BufferedImage.TYPE_BYTE_GRAY);
        byte[] imageBuff = ((java.awt.image.DataBufferByte) fgImage.getRaster()
                .getDataBuffer()).getData();

        lError = fp.GetImageEx(imageBuff, 5 * 1000, 0, 50);

        //Credential.setFgImage(fgImage);



        fp.GetImageQuality(deviceInfo.imageWidth, deviceInfo.imageHeight,
                imageBuff, quality);

        if (lError == SGFDxErrorCode.SGFDX_ERROR_NONE) {
            if (quality[0] == 0) {
                // Message Print;
                return null; // Image Quality not good.
            } else {
                lError = fp.CreateTemplate(fingerInfo, imageBuff, ISOminutiae);

                fp.GetTemplateSize(ISOminutiae, Size);

                // System.out.println("Size " + Size[0]);
                // fp.getMinutiae(imageBuff, ISOminutiae);
                // String sFileName="E:\\Rvalide\\input.scc";
                // util.writeByteArrayToFile(sFileName,compact_ISO);

                if (lError == SGFDxErrorCode.SGFDX_ERROR_NONE) {

                    compact_ISO = util.strip_data_end(ISOminutiae, Size[0]);
                    try {
                        compact_ISO = convertCC(compact_ISO);
                    } catch (IOException e) {
                        throw new MiddleWareException("to be coded");
                    }

                    BiometricBean fintemp = new BiometricBean();
                    fintemp.setFingerImage(fgImage);
                    fintemp.setFingerPrint(compact_ISO);


                    // compact_ISO = convertCC(compact_ISO);
                    //System.out.println("Template created "
                    //		+ util.arrayToHex(compact_ISO));

                    //if (i_capture == 1) {
                    //	System.out.println("Ist template ");
                    //	Credential.set_ISO1(compact_ISO);

                    //} else if (i_capture == 2) {
                    //	System.out.println("2nd template ");
                    //	Credential.set_ISO2(compact_ISO);
                    //} else {
                    //	Credential.set_ISO1(Credential.get_ISO2());
                    //	Credential.set_ISO2(compact_ISO);
                    //}

                    return fintemp;

                } else {
                    throw new MiddleWareException("Template creation failed.");
                    //return -3; // Template creation failed.
                }
            }

        } else {
            throw new MiddleWareException("No Image Found.");
            //System.out.println("No Image Found");
            //return -4;
        }

    }

    public int getTemplate(int i_capture) throws IOException {

        int[] Size = new int[1];
        byte[] ISOminutiae;
        byte[] compact_ISO;
        long lError;
        int[] quality = new int[1];

        lError = SGFDxErrorCode.SGFDX_ERROR_NONE;

        SGFingerInfo fingerInfo = new SGFingerInfo();
        fingerInfo.FingerNumber = SGFingerPosition.SG_FINGPOS_UK;
        fingerInfo.ImageQuality = quality[0];
        fingerInfo.ImpressionType = SGImpressionType.SG_IMPTYPE_LP;
        fingerInfo.ViewNumber = 1;

        // imageBuff = new byte[deviceInfo.imageHeight * deviceInfo.imageWidth];
        // ret_err =fp.SetLedOn(true);
        // Set Template format ISO19794
        lError = fp.SetTemplateFormat(SGFDxTemplateFormat.TEMPLATE_FORMAT_ISO19794);
        // /////////////////////////////////////////////
        // Get Max Template Size for ISO19794
        lError = fp.GetMaxTemplateSize(Size);

        // /////////////////////////////////////////////
        // Create ISO19794 Template for Finger1
        ISOminutiae = new byte[Size[0]];

        BufferedImage fgImage = new BufferedImage(deviceInfo.imageWidth,
                deviceInfo.imageHeight, BufferedImage.TYPE_BYTE_GRAY);
        byte[] imageBuff = ((java.awt.image.DataBufferByte) fgImage.getRaster()
                .getDataBuffer()).getData();

        lError = fp.GetImageEx(imageBuff, 5 * 1000, 0, 50);

        Credential.setFgImage(fgImage);

        fp.GetImageQuality(deviceInfo.imageWidth, deviceInfo.imageHeight,
                imageBuff, quality);

        if (lError == SGFDxErrorCode.SGFDX_ERROR_NONE) {
            if (quality[0] == 0) {
                // Message Print;
                return -2; // Image Quality not good.
            } else {
                lError = fp.CreateTemplate(fingerInfo, imageBuff, ISOminutiae);

                fp.GetTemplateSize(ISOminutiae, Size);

                // System.out.println("Size " + Size[0]);
                // fp.getMinutiae(imageBuff, ISOminutiae);
                // String sFileName="E:\\Rvalide\\input.scc";
                // util.writeByteArrayToFile(sFileName,compact_ISO);

                if (lError == SGFDxErrorCode.SGFDX_ERROR_NONE) {

                    compact_ISO = util.strip_data_end(ISOminutiae, Size[0]);
                    // compact_ISO = convertCC(compact_ISO);
                    System.out.println("Template created "
                            + util.arrayToHex(compact_ISO));

                    if (i_capture == 1) {
                        System.out.println("Ist template ");
                        Credential.set_ISO1(compact_ISO);

                    } else if (i_capture == 2) {
                        System.out.println("2nd template ");
                        Credential.set_ISO2(compact_ISO);
                    } else {
                        Credential.set_ISO1(Credential.get_ISO2());
                        Credential.set_ISO2(compact_ISO);
                    }

                    return 1;

                } else {
                    return -3; // Template creation failed.
                }
            }

        } else {
            System.out.println("No Image Found");
            return -4;
        }

    }

    public boolean getMatch() {
        System.out.println("Match-- ISO1 __ Start");
        boolean[] matched = new boolean[1];
        matched[0] = false;

        System.out.println("Match-- ISO1 "
                + util.arrayToHex(Credential.get_ISO1()));
        System.out.println("Match-- ISO2 "
                + util.arrayToHex(Credential.get_ISO2()));

        long iError = fp
                .MatchIsoTemplate(Credential.get_ISO1(), 0,
                Credential.get_ISO2(), 0, SGFDxSecurityLevel.SL_NORMAL,
                matched);

        if (iError == SGFDxErrorCode.SGFDX_ERROR_NONE) {
            return matched[0];

        } else {
            return false;
        }

    }

   
    public byte[] convertCC(byte[] ISO) throws IOException {

        byte[] ISOCC = null;

        System.out.println("ISO_Full " + util.arrayToHex(ISO));

        // String iso
        // ="464d5200203230000000013800000102015000c500c501000010002f40a800523d00807f005e440040890062c70040ab0068ca008095006a3f004069006e5000403800713a00407600724700407c0072db00408400764a00402000773a0040c70078500080a400804d00806a00855b00808c0086d70080190095c300403400a4ec00403600aff100406700b1db00805e00b25a00409000b35500401200b7c000403b00bbf100805800be6200802b00c4c60040ba00c4d000403e00c5e700801f00d24300404600d7d000402900d74000402800dac000803500e2c700801300e8bc00401c00ecc700806e00f3c700402300f44600807500fec000400b0103c700809a010dc700804601115700406801163a0080a20121c6004065012aae0040b10130c70040c90138c60040ce013c47008070013fb2000000";

        // ISO=util.hex1ToByteArray(iso);

        // 46 4d 52 00 //REcord header..Format identifier.
        // 20 32 30 00 //Version nbr
        // 00 00 00 ae //Length of record
        // 00 00 // Equipment Certification and Equipment ID
        // 01 02 // Image size in X
        // 01 50 // Image size in Y
        // 00 c5 // X horizontal resolution
        // 00 c5 // Y vertical resolution
        // 01 // nbr of finger views
        // 00 // Reserved byte .... 24 bytes up to this point.
        // 00 10 00 18 // 4 bytes for finger header record...

        long resX = (ISO[18] & 0x00FF) << 8 | (ISO[19] & 0x00FF);
        long resY = (ISO[20] & 0x00FF) << 8 | (ISO[21] & 0x00FF);

        int nbr_minutiae = ISO[27] & 0x00FF;

        List<ScardComISO> list = new ArrayList<ScardComISO>();

        int start_pos = 28;

        for (int i = 1; i <= nbr_minutiae; i++) {
            list.add(ScardComISO.getConvertedTemp(ISO, start_pos, resX, resY));
            start_pos += 6;
        }

        // Sort the list of minutia list
        Collections.sort(list);

        ScardComISO s0 = list.get(0);
        for (int i = 1; i < list.size();) {
            ScardComISO sl = list.get(i);
            if ((sl.XCC == s0.XCC && sl.YCC == s0.YCC) || i == 80) {
                list.remove(i);
            } else {
                s0 = sl;
                i++;
            }
        }

        // Now Put into compact card format.
        ISOCC = new byte[list.size() * 3];
        int offset_pos = 0;

        for (int i = 0; i < list.size(); i++) {
            offset_pos += list.get(i).ISOCC(ISOCC, offset_pos);
        }

        System.out.println("Compact ISO " + util.arrayToHex(ISOCC));
        System.out.println("Compact ISO length" + ISOCC.length);

        return ISOCC;

    }

    public void closeDevice() {

        fp.Close();
    }
}
