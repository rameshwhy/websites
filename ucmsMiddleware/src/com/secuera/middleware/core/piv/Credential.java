/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;
import java.awt.image.BufferedImage;
/**
 *
 * @author admin
 */
public class Credential {
	 
	 private static String loginType; /*L/G/B*/
	    private static String local_pin;
	    private static String global_pin;
	    private static byte[] finger_print;     
	    private static String[] str_fingerType;
	    private static boolean picchanged;
	    private static boolean login_status;
	    private static byte[] ISO1;
	    private static byte[] ISO2;
	    private static BufferedImage fgImage;

	   
	    
	    
	    //private static char[] lc_pin;
	    //private static char[] gc_pin;

	    public static BufferedImage getFgImage() {
	        return fgImage;
	    }

	    public static void setFgImage(BufferedImage fgImage) {
	        Credential.fgImage = fgImage;
	    }

	    
	    
	    
	  

	    public static boolean isPicchanged() {
	        return Credential.picchanged;
	    }

	    public static void setPicchanged(boolean picchanged) {
	        Credential.picchanged = picchanged;
	    }

	    //public static char[] getGc_pin() {
	    //    return gc_pin;
	    //}

	    //public static void setGc_pin(char[] gc_pin) {
	    //    Credential.gc_pin = gc_pin;
	    //}

	    //public static char[] getLc_pin() {
	    //    return lc_pin;
	    //}

	    //public static void setLc_pin(char[] lc_pin) {
	    //    Credential.lc_pin = lc_pin;
	    //}

	    public static String getLocal_pin() {
	        return local_pin;
	    }

	    public static void setLocal_pin(String local_pin) {
	        Credential.local_pin = local_pin;
	    }

	    public static String getGlobal_pin() {
	        return global_pin;
	    }

	    public static void setGlobal_pin(String global_pin) {
	        Credential.global_pin = global_pin;
	    }

	    public void resetCredential() {
	        Credential.local_pin = "";
	        Credential.global_pin = "";

	    }

	    public static boolean getLogin_status() {
	        return login_status;
	    }

	    public static void setLogin_status(boolean login_status) {
	        Credential.login_status = login_status;
	    }

	    public static byte[] get_ISO1() {
	        return ISO1;
	    }

	    public static void set_ISO1(byte[] ISO1) {
	        Credential.ISO1 = ISO1;
	    }

	    public static byte[] get_ISO2() {
	        return ISO2;
	    }

	    public static void set_ISO2(byte[] ISO2) {
	        Credential.ISO2 = ISO2;
	    }

	    public static byte[] getfinger_print() {
	        return finger_print;
	    }

	    public static void setfinger_print(byte[] finger_pin) {
	        Credential.finger_print = finger_pin;
	    }

	    public static String getLoginType() {
	        return loginType;
	    }

	    public static void setLoginType(String loginType) {
	        Credential.loginType = loginType;
	    }

	    public static String[] getStr_fingerType() {
	        return str_fingerType;
	    }

	    public static void setStr_fingerType(String[] str_fingerType) {
	        Credential.str_fingerType = str_fingerType;
	    }
    
    
    
}
