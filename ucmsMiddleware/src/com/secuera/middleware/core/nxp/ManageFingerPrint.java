/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.nxp;

import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstantsNXP;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessagesNXP;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.SendCommand;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class ManageFingerPrint {

    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    ExceptionMessagesNXP expMsg = new ExceptionMessagesNXP();
    private byte[] sub_bio = new byte[1];
    private byte[] fp_oid = new byte[1];
    private byte[] fp_enrollid = new byte[1];
    int i_offset;

    public ManageFingerPrint() {
        super();
    }

    public void createFPObjects(CardChannel channel) throws MiddleWareException {
        byte[] fileID;
        int len;
        len = UcmsMiddlewareConstantsNXP.fp_id.length;

        for (int i = 0; i < len; i++) {
            fileID = UcmsMiddlewareConstantsNXP.fp_id[i];

            if (i == 10) {
                createFPLog(channel, fileID);
                updateFPLog(channel, true);

            } else {
                createObject(channel, fileID);
                applyACL(channel, fileID);
            }

        }

    }

    public boolean enrollFP(CardChannel channel, byte[] fingerInfo, String fgType) throws MiddleWareException {
        boolean retValue;
        getFPObject(fgType);
        updateObject(channel);
        retValue = updateFingerPrint(channel, fingerInfo);
        updateFPLog(channel, false);

        return retValue;
    }

    public boolean resetFP(CardChannel channel, byte[] fingerInfo, String fgType) throws MiddleWareException {
        boolean retValue;

        getFPObject(fgType);
        updateObject(channel);
        retValue = resetFingerPrint(channel, fingerInfo);
        updateFPLog(channel, false);

        return retValue;

    }

    public boolean verifyFP(CardChannel channel, byte[] fingerInfo, String fgType) throws MiddleWareException {
        boolean retValue;
        getFPObject(fgType);

        retValue = verifyFingerPrint(channel, fingerInfo);

        return retValue;

    }

    private boolean createObject(CardChannel channel, byte[] fileID) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;

        /*# create DO <0210> FP01
         /send 80 E0 00 06 1D 62 1B 82 03 1042 21 83 02 02 10
         //86 0A 00 00 00 00 00 00 00 00 00 00 9A 01 03 9B 01 03 9000
         */

        byte[] create_object = {(byte) 0x80, (byte) 0xE0, 0x00, 0x06, 0x1D};
        byte[] FCP = {0x62, 0x1B};
        byte[] file_type = {(byte) 0x82, 0x03, 0x10, 0x42, 0x21}; //03 means next 3 bytes define file type.
        byte[] file_id = {(byte) 0x83, 0x02}; // 0x10 02 means next 2 bytes define file type.
        byte[] access_rules = {(byte) 0x86, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        byte[] nbr_of_tries = {(byte) 0x9A, 0x01, 0x03}; // 03 means 3 tries.
        byte[] remaining_tries = {(byte) 0x9B, 0x01, 0x03}; //


        byte[][] create_arrays = new byte[8][];
        create_arrays[0] = create_object;
        create_arrays[1] = FCP;
        create_arrays[2] = file_type;
        create_arrays[3] = file_id;
        create_arrays[4] = fileID;
        create_arrays[5] = access_rules;
        create_arrays[6] = nbr_of_tries;
        create_arrays[7] = remaining_tries;

        byte[] create = util.combine_data(create_arrays);

        CommandAPDU CREATE_APDU = new CommandAPDU(create);

        try {
            sendCard.sendCommand(channel, CREATE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equalsIgnoreCase("9000")) {

            // This means file already Exists.
            if (sb.toString().equalsIgnoreCase("6A89")) {
                return true;
            }

            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to Create Finger Print Object";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }

    private boolean createFPLog(CardChannel channel, byte[] fileID) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;

        // # create EF <0101> DG01
        ///send 00 E0 00 00 19 62 17
        // 80 02 00 0A
        //82 01 01
        //83 02 02 1A 860 A00000000000000000000 9000

        byte[] create_object = {(byte) 0x00, (byte) 0xE0, 0x00, 0x00, 0x19};
        byte[] FCP = {0x62, 0x17, (byte) 0x80, 0x02, 0x00, 0x0A};
        byte[] file_type = {(byte) 0x82, 0x01, 0x01};
        byte[] file_id = {(byte) 0x83, 0x02}; // 02 means next 2 bytes define file type.
        byte[] access_rules = {(byte) 0x86, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};


        byte[][] create_arrays = new byte[6][];
        create_arrays[0] = create_object;
        create_arrays[1] = FCP;
        create_arrays[2] = file_type;
        create_arrays[3] = file_id;
        create_arrays[4] = fileID;
        create_arrays[5] = access_rules;

        byte[] create = util.combine_data(create_arrays);

        CommandAPDU CREATE_APDU = new CommandAPDU(create);

        try {
            sendCard.sendCommand(channel, CREATE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equalsIgnoreCase("9000")) {

            // This means file already Exists.
            if (sb.toString().equalsIgnoreCase("6A89")) {
                return true;
            }

            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to Create Finger Print Log File";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }

    public boolean applyACL(CardChannel channel, byte[] fileID) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;

        /*# update ACL <0210> FP01
         /send 00 DA 01 6F 11
         * 82 03 10 42 21
         * 83 02 02 10
         * 86 06 242424242424 9000
         */

        byte[] acl_cmnd = {(byte) 0x00, (byte) 0xDA, 0x01, (byte) 0x6F, 0x11}; //11 is the length of total nbr of bytes being sent.
        byte[] file_type = {(byte) 0x82, 0x03, 0x10, 0x42, 0x21};
        byte[] fp_id = {(byte) 0x83, 0x02};
        byte[] access_rule = {(byte) 0x86, 0x06, 0x24, 0x24, 0x24, 0x24, 0x24, 0x24};


        byte[][] data_arrays = new byte[5][];
        data_arrays[0] = acl_cmnd;
        data_arrays[1] = file_type;
        data_arrays[2] = fp_id;
        data_arrays[3] = fileID;
        data_arrays[4] = access_rule;


        byte[] access = util.combine_data(data_arrays);

        CommandAPDU ACCESS_APDU = new CommandAPDU(access);

        try {
            sendCard.sendCommand(channel, ACCESS_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equalsIgnoreCase("9000")) {

            if (sb.toString().equalsIgnoreCase("6982")) {
                return true;
            }
            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to Update Finger Print Object Access Rules";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }

    public boolean updateObject(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;

        /*#  PUT DATA components
         /send 00 DB 3F FF
         28 70 26 BF 82 10 22 7F60 1F
         83 01 01
         * A1 1A 81 01 08
         * 82 01 00
         * 8702010188020006B10A81020C30820100830100 9000
         */

        byte[] put_data = {(byte) 0x00, (byte) 0xDB, 0x3F, (byte) 0xFF, 0x28}; //28 is the length of total nbr of bytes being sent.
        byte[] FCP = {0x70, 0x26, (byte) 0xBF};
        byte[] fp_type = {(byte) 0x82};
        byte[] bit_template_hdr = {0x22, 0x7F, 0x60, 0x1F, (byte) 0x83, 0x01, 0x01};
        byte[] bit_template = {(byte) 0xA1, 0x1A, (byte) 0x81, 0x01, 0x08, (byte) 0x82, 0x01, 0x00,
            (byte) 0x87, 0x02, 0x01, 0x01, (byte) 0x88, 0x02, 0x00, 0x06, (byte) 0xB1, 0x0A, (byte) 0x81, 0x02, 0x0C, 0x30,
            (byte) 0x82, 0x01, 0x00, (byte) 0x83, 0x01, 0x00};


        byte[][] data_arrays = new byte[6][];
        data_arrays[0] = put_data;
        data_arrays[1] = FCP;
        data_arrays[2] = fp_type;
        data_arrays[3] = fp_oid;
        data_arrays[4] = bit_template_hdr;
        data_arrays[5] = bit_template;

        byte[] update = util.combine_data(data_arrays);

        CommandAPDU UPDATE_APDU = new CommandAPDU(update);

        try {
            sendCard.sendCommand(channel, UPDATE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equalsIgnoreCase("9000")) {
            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to Update Finger Print Object";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }

    private boolean resetFingerPrint(CardChannel channel, byte[] fingerInfo) throws MiddleWareException {

        /*
         * //send 00 24 01 90 97
         * // 7F2E 81 93 81
         * 81 90 len3
         * 4B416547474566497D514E656E4E5D77537C1D586A305A6A4D5B856B627A1C6A4A426A6B7D6
         //A59506C44676C78817675617A5F29806D1B846C5B8A6F8794735F965133996F509C755F9D547D9D71629D4F5FA05777A26E1FA9AE2AAFAF53B37A33B
         //4B247B5767AB66A16B76D83B98E43BC753FC27479CDA61FCF7146CF5588D54A14D8AF53E37C25E6966FE78351F05B 9000
         */

        boolean rtnvalue = false;
        StringBuffer sb = new StringBuffer();
        byte[] command_data;
        byte[] zero_size = {0x00};

        byte[] apdu_enroll = {0x00, (byte) 0x2C, 0x02};
        byte[] var_len_1;
        byte[] apdu_tag = {0x7F, 0x2E};
        byte[] var_len_2;
        byte[] minutiae_tag = {(byte) 0x81};

        byte[] var_len_3;

        // Now calculate actual data size being stored
        var_len_3 = util.calcBertValue(fingerInfo.length, false, false);

        // Now calculate length of var2.
        var_len_2 = util.calcBertValue(fingerInfo.length + var_len_3.length + minutiae_tag.length, false, false);
        // Now calculate the LC Value of commands
        var_len_1 = util.calcBertValue(fingerInfo.length + var_len_3.length + minutiae_tag.length + var_len_2.length + apdu_tag.length, false, true);

        if (var_len_1.length > 1) {
            var_len_1 = util.combine_data(zero_size, var_len_1);
        }

        byte[][] data_arrays = new byte[8][];
        data_arrays[0] = apdu_enroll;
        data_arrays[1] = fp_enrollid;
        data_arrays[2] = var_len_1;
        data_arrays[3] = apdu_tag;
        data_arrays[4] = var_len_2;
        data_arrays[5] = minutiae_tag;
        data_arrays[6] = var_len_3;
        data_arrays[7] = fingerInfo;

        command_data = util.combine_data(data_arrays);
        //util.arrayToHex(command_data);

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (sb.toString().equals("9000")) {
            rtnvalue = true;
        }

        return rtnvalue;

    }

    private boolean updateFingerPrint(CardChannel channel, byte[] fingerInfo) throws MiddleWareException {

        /*
         * //send 00 24 01 90 97
         * // 7F2E 81 93 81
         * 81 90 len3
         * 4B416547474566497D514E656E4E5D77537C1D586A305A6A4D5B856B627A1C6A4A426A6B7D6
         //A59506C44676C78817675617A5F29806D1B846C5B8A6F8794735F965133996F509C755F9D547D9D71629D4F5FA05777A26E1FA9AE2AAFAF53B37A33B
         //4B247B5767AB66A16B76D83B98E43BC753FC27479CDA61FCF7146CF5588D54A14D8AF53E37C25E6966FE78351F05B 9000
         */

        boolean rtnvalue = false;
        StringBuffer sb = new StringBuffer();
        byte[] command_data;
        byte[] zero_size = {0x00};


        byte[] apdu_enroll = {0x00, (byte) 0x24, 0x01};
        byte[] var_len_1;
        byte[] apdu_tag = {0x7F, 0x2E};
        byte[] var_len_2;
        byte[] minutiae_tag = {(byte) 0x81};

        byte[] var_len_3;

        // Now calculate actual data size being stored
        var_len_3 = util.calcBertValue(fingerInfo.length, false, false);

        // Now calculate length of var2.
        var_len_2 = util.calcBertValue(fingerInfo.length + var_len_3.length + minutiae_tag.length, false, false);
        // Now calculate the LC Value of commands
        var_len_1 = util.calcBertValue(fingerInfo.length + var_len_3.length + minutiae_tag.length + var_len_2.length + apdu_tag.length, false, true);

        if (var_len_1.length > 1) {
            var_len_1 = util.combine_data(zero_size, var_len_1);
        }

        byte[][] data_arrays = new byte[8][];
        data_arrays[0] = apdu_enroll;
        data_arrays[1] = fp_enrollid;
        data_arrays[2] = var_len_1;
        data_arrays[3] = apdu_tag;
        data_arrays[4] = var_len_2;
        data_arrays[5] = minutiae_tag;
        data_arrays[6] = var_len_3;
        data_arrays[7] = fingerInfo;

        command_data = util.combine_data(data_arrays);
        // System.out.println("Final Command " + util.arrayToHex(command_data));

        //util.arrayToHex(command_data);


        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (sb.toString().equals("9000")) {
            rtnvalue = true;
        }

        return rtnvalue;

    }

    private boolean updateFPLog(CardChannel channel, boolean fpInit) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;
        byte[] upd_command = null;

        // b8=1 in P1 then b6-5 are set to 0 (RFU bits).
        //bit5-1 of P1 are a short EF identifier and
        //P2 is the offset of the first byte to be updated in
        //data units from the beginning of the file.

        byte[] init_object = {(byte) 0x00, (byte) 0xD6, (byte) 0x80, 0x00, 0x01, 0x01};
        byte[] update_object = {(byte) 0x00, (byte) 0xD6, (byte) 0x9F}; //
        byte[] P2 = util.calcBertValue(i_offset, false, true);
        byte[] Lc = {0x01};


        if (!fpInit) {
            byte[][] update_arrays = new byte[4][];
            update_arrays[0] = update_object;
            update_arrays[1] = P2;
            update_arrays[2] = Lc;
            update_arrays[3] = sub_bio;

            upd_command = util.combine_data(update_arrays);



        } else {

            upd_command = init_object;
        }

        util.arrayToHex(upd_command);

        CommandAPDU UPD_APDU = new CommandAPDU(upd_command);

        try {
            sendCard.sendCommand(channel, UPD_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equalsIgnoreCase("9000")) {
            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to Update Finger Print Log File";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }

    public byte[] readFPLog(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;
        byte[] res;
        byte[] read_file = {(byte) 0x00, (byte) 0xB0, (byte) 0x9F, 0x00, 0x0A};

        CommandAPDU CREATE_APDU = new CommandAPDU(read_file);

        try {
            res = sendCard.sendCommand(channel, CREATE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        //util.arrayToHex(res)
        //
        if (!sb.toString().equalsIgnoreCase("9000")) {
            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to Read Finger Print Log File";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return res;
        }

    }

    private boolean verifyFingerPrint(CardChannel channel, byte[] fingerInfo) throws MiddleWareException {

        // send 00 21 00 90
        // 97 7F2E 81 93 81 81 90
        //4B416547474566497D514E656E4E5D77537C1D586A305A6A4D5B856B627A1C6A4A426A6B7D6A59506C44676C78817675617A5F29806D1B846C5B8A6F8794735F965133996F509C755F9D547D9D71629D4F5FA05777A26E1FA9AE2AAFAF53B37A33B4B247B5767AB66A16B76D83B98E43BC753FC27479CDA61FCF7146CF5588D54A14D8AF53E37C25E6966FE78351F05B 9000

        String ErrorMsg;
        boolean rtnvalue = false;
        StringBuffer sb = new StringBuffer();
        byte[] command_data;
        byte[] zero_size = {0x00};


        byte[] apdu_enroll = {0x00, (byte) 0x21, 0x00};
        byte[] var_len_1;
        byte[] apdu_tag = {0x7F, 0x2E};
        byte[] var_len_2;
        byte[] minutiae_tag = {(byte) 0x81};

        byte[] var_len_3;

        // Now calculate actual data size being stored
        var_len_3 = util.calcBertValue(fingerInfo.length, false, false);

        // Now calculate length of var2.
        var_len_2 = util.calcBertValue(fingerInfo.length + var_len_3.length + minutiae_tag.length, false, false);
        // Now calculate the LC Value of commands
        var_len_1 = util.calcBertValue(fingerInfo.length + var_len_3.length + minutiae_tag.length + var_len_2.length + apdu_tag.length, false, true);

        if (var_len_1.length > 1) {
            var_len_1 = util.combine_data(zero_size, var_len_1);
        }

        byte[][] data_arrays = new byte[8][];
        data_arrays[0] = apdu_enroll;
        data_arrays[1] = fp_enrollid;
        data_arrays[2] = var_len_1;
        data_arrays[3] = apdu_tag;
        data_arrays[4] = var_len_2;
        data_arrays[5] = minutiae_tag;
        data_arrays[6] = var_len_3;
        data_arrays[7] = fingerInfo;

        command_data = util.combine_data(data_arrays);
        System.out.println("Final COmmand " + util.arrayToHex(command_data));

        //util.arrayToHex(command_data);

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (sb.toString().equalsIgnoreCase("6983")) {
            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Finger Print Object is Locked. Please reset it.";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        }

        if (sb.toString().equals("9000")) {
            rtnvalue = true;

        }

        return rtnvalue;

    }

    /*private boolean selectfile(CardChannel channel) throws MiddleWareException {

     StringBuffer sb = new StringBuffer();
     String ErrorMsg;

     //CMD_SELECT_EF0001 = {(byte)0x00, (byte)0xA4, (byte)0x00, (byte)0x0C, (byte)0x02,
     // (byte)0x00, (byte)0x01 };



     byte[] select_file = {(byte) 0x00, (byte) 0xA4, 0x00, 0x0C, 0x02};
     byte[] short_file_id = {0x01, 0x1F};

     byte[][] select_arrays = new byte[2][];
     select_arrays[0] = select_file;
     select_arrays[1] = short_file_id;

     byte[] select = util.combine_data(select_arrays);

     CommandAPDU select_APDU = new CommandAPDU(select);

     try {
     sendCard.sendCommand(channel, select_APDU, sb);
     } catch (CardException e) {
     throw new MiddleWareException(e.getMessage());
     }

     if (!sb.toString().equals("9000")) {
     StringBuilder errMsg = new StringBuilder();
     ErrorMsg = "Unable to Select FP Log file";
     errMsg.append(ErrorMsg).append("  ");
     errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
     throw new MiddleWareException(errMsg.toString());

     } else {
     return true;
     }

     }
     */
    public void getFPObject(String fgType) {

        byte[] RT = {0x05}; // Right thumb
        byte[] RI = {0x09}; // Right index
        byte[] RM = {0x0D}; // Right middle
        byte[] RR = {0x11}; // Right ring
        byte[] RL = {0x15}; // Right little
        byte[] LT = {0x06}; // Left thumb
        byte[] LI = {(byte) 0x0A}; // Left index
        byte[] LM = {0x0E}; // Left middle
        byte[] LR = {0x12}; // Left ring
        byte[] LL = {0x16}; // Left little

        if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.rightThumb)) {
            sub_bio = RT;
            i_offset = 0;
            fp_oid = util.extract_data(UcmsMiddlewareConstantsNXP.fp_id[0], 1, 1);
            fp_enrollid = UcmsMiddlewareConstantsNXP.fp_upid[0];

        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.rightIndex)) {
            sub_bio = RI;
            i_offset = 1;
            fp_oid = util.extract_data(UcmsMiddlewareConstantsNXP.fp_id[1], 1, 1);
            fp_enrollid = UcmsMiddlewareConstantsNXP.fp_upid[1];

        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.rightMiddle)) {
            sub_bio = RM;
            i_offset = 2;
            fp_oid = util.extract_data(UcmsMiddlewareConstantsNXP.fp_id[2], 1, 1);
            fp_enrollid = UcmsMiddlewareConstantsNXP.fp_upid[2];
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.rightRing)) {
            sub_bio = RR;
            i_offset = 3;
            fp_oid = util.extract_data(UcmsMiddlewareConstantsNXP.fp_id[3], 1, 1);
            fp_enrollid = UcmsMiddlewareConstantsNXP.fp_upid[3];
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.rightLittle)) {
            sub_bio = RL;
            i_offset = 4;
            fp_oid = util.extract_data(UcmsMiddlewareConstantsNXP.fp_id[4], 1, 1);
            fp_enrollid = UcmsMiddlewareConstantsNXP.fp_upid[4];
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.leftThumb)) {
            sub_bio = LT;
            i_offset = 5;
            fp_oid = util.extract_data(UcmsMiddlewareConstantsNXP.fp_id[5], 1, 1);
            fp_enrollid = UcmsMiddlewareConstantsNXP.fp_upid[5];
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.leftIndex)) {
            sub_bio = LI;
            i_offset = 6;
            fp_oid = util.extract_data(UcmsMiddlewareConstantsNXP.fp_id[6], 1, 1);
            fp_enrollid = UcmsMiddlewareConstantsNXP.fp_upid[6];
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.leftMiddle)) {
            sub_bio = LM;
            i_offset = 7;
            fp_oid = util.extract_data(UcmsMiddlewareConstantsNXP.fp_id[7], 1, 1);
            fp_enrollid = UcmsMiddlewareConstantsNXP.fp_upid[7];
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.leftRing)) {
            sub_bio = LR;
            i_offset = 8;
            fp_oid = util.extract_data(UcmsMiddlewareConstantsNXP.fp_id[8], 1, 1);
            fp_enrollid = UcmsMiddlewareConstantsNXP.fp_upid[8];
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.leftLittle)) {
            sub_bio = LL;
            i_offset = 9;
            fp_oid = util.extract_data(UcmsMiddlewareConstantsNXP.fp_id[9], 1, 1);
            fp_enrollid = UcmsMiddlewareConstantsNXP.fp_upid[9];
        }

    }
}
