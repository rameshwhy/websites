package com.id3.fingerprint;

public abstract interface FingerCaptureListener
{
  public abstract void deviceAdded(FingerCapture paramFingerCapture, FingerCaptureDevice paramFingerCaptureDevice);

  public abstract void deviceRemoved(FingerCapture paramFingerCapture, String paramString);

  public abstract void statusChanged(FingerCapture paramFingerCapture, FingerCaptureDevice paramFingerCaptureDevice, int paramInt);

  public abstract void imagePreview(FingerCapture paramFingerCapture, FingerCaptureDevice paramFingerCaptureDevice, FingerImage paramFingerImage);

  public abstract void imageCaptured(FingerCapture paramFingerCapture, FingerCaptureDevice paramFingerCaptureDevice, FingerImage paramFingerImage);

  public abstract void imageProcessed(FingerCapture paramFingerCapture, FingerCaptureDevice paramFingerCaptureDevice, FingerImage paramFingerImage);
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCaptureListener
 * JD-Core Version:    0.6.2
 */