/*     */ package com.id3.fingerprint;
/*     */ 
/*     */ public final class FingerCaptureDevice
/*     */ {
/*     */   private long _handle;
/*     */ 
/*     */   public FingerCaptureDevice(long handle)
/*     */   {
/*  22 */     this._handle = handle;
/*     */   }
/*     */ 
/*     */   public boolean equals(Object obj)
/*     */   {
/*  32 */     if ((obj == null) || (obj.getClass() != getClass())) {
/*  33 */       return false;
/*     */     }
/*  35 */     FingerCaptureDevice device = (FingerCaptureDevice)obj;
/*  36 */     return this._handle == device._handle;
/*     */   }
/*     */ 
/*     */   public int hashCode()
/*     */   {
/*  45 */     return (int)this._handle;
/*     */   }
/*     */ 
/*     */   public boolean isModeAvailable(int mode)
/*     */     throws FingerException
/*     */   {
/*  57 */     int[] value = new int[1];
/*     */ 
/*  59 */     int result = nIsModeAvailable(this._handle, mode, value);
/*     */ 
/*  61 */     if (result != 0) {
/*  62 */       throw new FingerException(result);
/*     */     }
/*  64 */     if (value[0] == 1) {
/*  65 */       return true;
/*     */     }
/*  67 */     return false;
/*     */   }
/*     */ 
/*     */   public boolean canCapturePosition(int position)
/*     */     throws FingerException
/*     */   {
/*  79 */     int[] value = new int[1];
/*     */ 
/*  81 */     int result = nCanCapturePosition(this._handle, position, value);
/*     */ 
/*  83 */     if (result != 0) {
/*  84 */       throw new FingerException(result);
/*     */     }
/*  86 */     if (value[0] == 1) {
/*  87 */       return true;
/*     */     }
/*  89 */     return false;
/*     */   }
/*     */ 
/*     */   public void startCapture(int position)
/*     */     throws FingerException
/*     */   {
/* 101 */     int result = nStartCapture(this._handle, position);
/*     */ 
/* 103 */     if (result != 0)
/* 104 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public void stopCapture()
/*     */     throws FingerException
/*     */   {
/* 115 */     int result = nStopCapture(this._handle);
/*     */ 
/* 117 */     if (result != 0)
/* 118 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public long getHandle()
/*     */   {
/* 127 */     return this._handle;
/*     */   }
/*     */ 
/*     */   public String getName()
/*     */     throws FingerException
/*     */   {
/* 137 */     return nGetName(this._handle);
/*     */   }
/*     */ 
/*     */   public String getSerialNumber()
/*     */     throws FingerException
/*     */   {
/* 147 */     return nGetSerialNumber(this._handle);
/*     */   }
/*     */ 
/*     */   public int getCaptureMode()
/*     */     throws FingerException
/*     */   {
/* 158 */     int[] value = new int[1];
/*     */ 
/* 160 */     int result = nGetCaptureMode(this._handle, value);
/*     */ 
/* 162 */     if (result != 0) {
/* 163 */       throw new FingerException(result);
/*     */     }
/* 165 */     return value[0];
/*     */   }
/*     */ 
/*     */   public void setCaptureMode(int mode)
/*     */     throws FingerException
/*     */   {
/* 177 */     int result = nSetCaptureMode(this._handle, mode);
/*     */ 
/* 179 */     if (result != 0)
/* 180 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public int getModel()
/*     */     throws FingerException
/*     */   {
/* 192 */     int[] value = new int[1];
/*     */ 
/* 194 */     int result = nGetModel(this._handle, value);
/*     */ 
/* 196 */     if (result != 0) {
/* 197 */       throw new FingerException(result);
/*     */     }
/* 199 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getVendor()
/*     */     throws FingerException
/*     */   {
/* 210 */     int[] value = new int[1];
/*     */ 
/* 212 */     int result = nGetVendor(this._handle, value);
/*     */ 
/* 214 */     if (result != 0) {
/* 215 */       throw new FingerException(result);
/*     */     }
/* 217 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getTechnology()
/*     */     throws FingerException
/*     */   {
/* 228 */     int[] value = new int[1];
/*     */ 
/* 230 */     int result = nGetTechnology(this._handle, value);
/*     */ 
/* 232 */     if (result != 0) {
/* 233 */       throw new FingerException(result);
/*     */     }
/* 235 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getCertification()
/*     */     throws FingerException
/*     */   {
/* 246 */     int[] value = new int[1];
/*     */ 
/* 248 */     int result = nGetCertification(this._handle, value);
/*     */ 
/* 250 */     if (result != 0) {
/* 251 */       throw new FingerException(result);
/*     */     }
/* 253 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getTypeId()
/*     */     throws FingerException
/*     */   {
/* 264 */     int[] value = new int[1];
/*     */ 
/* 266 */     int result = nGetType(this._handle, value);
/*     */ 
/* 268 */     if (result != 0) {
/* 269 */       throw new FingerException(result);
/*     */     }
/* 271 */     return value[0];
/*     */   }
/*     */ 
/*     */   public boolean getEnabled()
/*     */     throws FingerException
/*     */   {
/* 282 */     int[] enabled = new int[1];
/*     */ 
/* 284 */     int result = nIsEnabled(this._handle, enabled);
/*     */ 
/* 286 */     if (result != 0) {
/* 287 */       throw new FingerException(result);
/*     */     }
/* 289 */     return enabled[0] != 0;
/*     */   }
/*     */ 
/*     */   public void setEnabled(boolean value)
/*     */     throws FingerException
/*     */   {
/* 300 */     int result = nSetEnabled(this._handle, value ? 1 : 0);
/*     */ 
/* 302 */     if (result != 0)
/* 303 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public boolean isCapturing()
/*     */     throws FingerException
/*     */   {
/* 314 */     int[] isCapturing = new int[1];
/*     */ 
/* 316 */     int result = nIsCapturing(this._handle, isCapturing);
/*     */ 
/* 318 */     if (result != 0) {
/* 319 */       throw new FingerException(result);
/*     */     }
/* 321 */     return isCapturing[0] != 0;
/*     */   }
/*     */ 
/*     */   public int getCaptureStatus()
/*     */     throws FingerException
/*     */   {
/* 331 */     int[] captureStatus = new int[1];
/*     */ 
/* 333 */     int result = nGetCaptureStatus(this._handle, captureStatus);
/*     */ 
/* 335 */     if (result != 0) {
/* 336 */       throw new FingerException(result);
/*     */     }
/* 338 */     return captureStatus[0];
/*     */   }
/*     */ 
/*     */   public int getSpoofDetectionMode()
/*     */     throws FingerException
/*     */   {
/* 349 */     int[] value = new int[1];
/*     */ 
/* 351 */     int result = nGetSpoofDetectionMode(this._handle, value);
/*     */ 
/* 353 */     if (result != 0) {
/* 354 */       throw new FingerException(result);
/*     */     }
/* 356 */     return value[0];
/*     */   }
/*     */ 
/*     */   public void setSpoofDetectionMode(int value)
/*     */     throws FingerException
/*     */   {
/* 367 */     int result = nSetSpoofDetectionMode(this._handle, value);
/*     */ 
/* 369 */     if (result != 0)
/* 370 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public boolean isSpoofDetectionAvailable()
/*     */     throws FingerException
/*     */   {
/* 381 */     int[] value = new int[1];
/*     */ 
/* 383 */     int result = nIsSpoofDetectionAvailable(this._handle, value);
/*     */ 
/* 385 */     if (result != 0) {
/* 386 */       throw new FingerException(result);
/*     */     }
/* 388 */     if (value[0] == 1) {
/* 389 */       return true;
/*     */     }
/* 391 */     return false;
/*     */   }
/*     */ 
/*     */   private static final native int nInitialize(long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nIsModeAvailable(long paramLong, int paramInt, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nCanCapturePosition(long paramLong, int paramInt, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nIsEnabled(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nSetEnabled(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nIsCapturing(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetCaptureStatus(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nStartCapture(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nStopCapture(long paramLong);
/*     */ 
/*     */   private static final native String nGetName(long paramLong);
/*     */ 
/*     */   private static final native String nGetSerialNumber(long paramLong);
/*     */ 
/*     */   private static final native int nGetCaptureMode(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nSetCaptureMode(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nGetCertification(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetModel(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetVendor(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetTechnology(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetType(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetSpoofDetectionMode(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nSetSpoofDetectionMode(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nIsSpoofDetectionAvailable(long paramLong, int[] paramArrayOfInt);
/*     */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCaptureDevice
 * JD-Core Version:    0.6.2
 */