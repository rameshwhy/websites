/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.nxp;

import com.secuera.middleware.beans.nxp.InjectKeysNxp;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessagesNXP;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.SendCommand;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class PersoBacKeys {

    // Create instances of other classes.
    // *************************************************************
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    ExceptionMessagesNXP expMsg = new ExceptionMessagesNXP();
    InjectKeysNxp injectKeys = new InjectKeysNxp();
    byte[] apdu_key = {(byte) 0x80, (byte) 0xDA, 0x01, 0x6E, 0x16};
    byte[] key_oid_enc = {(byte) 0x83, 0x02, 0x20, 0x01};
    byte[] key_oid_mac = {(byte) 0x83, 0x02, 0x20, 0x03};
    byte[] key_tag = {(byte) 0x8F, 0x10}; //Tag and length
    byte[] enc_key;
    byte[] mac_key;
    byte[] file_id;
    byte[] short_file_id;
    byte[] nbr_of_tries;
    byte[] remaining_tries;
    byte[] access_rules;
    boolean lb_moc=false;
    
    byte[] eac_short_file_id = {0x0A, 0x21};
    byte[] eac_file_id = {(byte) 0x83, 0x02, 0x0A, 0x21};
    byte[] eac_nbr_of_tries = {(byte) 0x9A, 0x01, 0x00}; // 00 means infinte.;
    byte[] eac_remaining_tries = {(byte) 0x9B, 0x01, 0x00}; // 00 means locked;
    byte[] eac_access_rules = {(byte) 0x86, 0x0A, 0x0D, 0x0C, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF,
        (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};

    byte[] moc_short_file_id = {0x0A, 0x04};
    byte[] moc_file_id = {(byte) 0x83, 0x02, 0x0A, 0x04};
    byte[] moc_nbr_of_tries = {(byte) 0x9A, 0x01, 0x05}; // 00 means infinte.
    byte[] moc_remaining_tries = {(byte) 0x9B, 0x01, 0x05}; // 00 means locked
    byte[] moc_access_rules = {(byte) 0x86, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    
    
    byte[] ao_object = {(byte) 0x80, (byte) 0xE0, 0x00, 0x06};
    

    public PersoBacKeys() {
        super();
    }

    public PersoBacKeys(InjectKeysNxp injKeys) {
        injectKeys = injKeys;
    }

    @SuppressWarnings("empty-statement")
    public boolean persokeys(CardChannel channel) throws MiddleWareException {

        // Inject keys in the EAC Applet.
        if (injectKeys.getEac_encKey() != null && injectKeys.getEac_macKey() != null) {
            enc_key = injectKeys.getEac_encKey();
            mac_key = injectKeys.getEac_macKey();

            file_id = eac_file_id;
            nbr_of_tries = eac_nbr_of_tries;
            remaining_tries = eac_remaining_tries;
            access_rules = eac_access_rules;
            short_file_id=eac_short_file_id;
            
            injectkeys(channel);


        }

        // Inject keys in the EPKI Applet.
        if (injectKeys.getEpki_encKey() != null && injectKeys.getEpki_macKey() != null) {

            enc_key = injectKeys.getEpki_encKey();
            mac_key = injectKeys.getEpki_macKey();

            injectkeys(channel);
        }

        // Inject keys For FP MOC.
        if (injectKeys.getMoc_encKey() != null && injectKeys.getMoc_macKey() != null) {
            enc_key = injectKeys.getMoc_encKey();
            mac_key = injectKeys.getMoc_macKey();
    
            file_id  = moc_file_id;
            nbr_of_tries  = moc_nbr_of_tries; // 00 means infinte.
            remaining_tries  = moc_remaining_tries; // 00 means locked
            access_rules  = moc_access_rules;
            short_file_id=moc_short_file_id;
            
            lb_moc=true;
            injectkeys(channel);
        }

        return true;

    }

    private boolean injectkeys(CardChannel channel) throws MiddleWareException {


        if (!createObject(channel)) {
            return false;
        }

        //selectObject(channel);
        
        if (!injectEncKey(channel)) {
            return false;
        }

        if (!injectMacKey(channel)) {
            return false;
        }

        if (!authObject(channel)) {
            return false;
        }

        return true;

    }

    private boolean createObject(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;
        //send "80 E0 0007 1C
        //62 1A 82 02 10 64 83 02 0A 21
        //86 0A 0D 0C FF FF FF FF FF FF FF FF 9A 01 00 9B 01 00" "9000"

        //(byte)0x80, 0x01,0x01,(byte)0x84, 0x01,0x08};
        //"80 E0 0006
        //1C 62 1A
        //82 0C 10 43
        //06 F3 B4 30 00 21 B8 30 00 21
        //83 02 01 21 
        //86 06 0D FF FF FF FF FF" "9000"
        
        // # create DO <0A04> KEY04
        //send 80 E0 00 07 1C
        //62 1A
        //82 02 10 64
        //83 02 0A 04
        //86 0A00000000000000000000
        //9A 01 05 9B 01 05 9000



        byte[] create_object = {(byte) 0x80, (byte) 0xE0, 0x00, 0x07, 0x1C};
        byte[] FCP = {0x62, 0x1A};
        byte[] file_type = {(byte) 0x82, 0x02, 0x10, 0x64}; //02 means next 2 bytes define file type.



        byte[][] create_arrays = new byte[7][];
        create_arrays[0] = create_object;
        create_arrays[1] = FCP;
        create_arrays[2] = file_type;
        create_arrays[3] = file_id;
        create_arrays[4] = access_rules;
        create_arrays[5] = nbr_of_tries;
        create_arrays[6] = remaining_tries;

        byte[] create = util.combine_data(create_arrays);
        //util.arrayToHex(create)

        // 80 e0 00 07 1c 62"
        //1a 82 02 10 64 83 02 0a 04 
        //86 0a 00000000000000000000 9a 01 05 9b 01 05"
        
        //80 e0 0007
        //1c 62 1a
        //82 02 10 64
        //83 02 0a 21
        //86 0a 0d 0c ff ff ff ff ff ff ff ff
        //9a 01 00 9b 01 00

        CommandAPDU CREATE_APDU = new CommandAPDU(create);

        try {
            sendCard.sendCommand(channel, CREATE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equalsIgnoreCase("9000")) {

            // This means file already Exists.
            if (sb.toString().equalsIgnoreCase("6A89")) {
                if (!selectObject(channel)) {
                    return false;
                }
                return true;
            }

            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to Create Object for BAC keys";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }

    private boolean selectObject(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;

        byte[] select_object = {(byte) 0x80, (byte) 0xA4, 0x00, 0x07, 0x02};
        

        byte[][] select_arrays = new byte[2][];
        select_arrays[0] = select_object;
        select_arrays[1] = short_file_id;

        byte[] select = util.combine_data(select_arrays);

        CommandAPDU select_APDU = new CommandAPDU(select);

        //util.arrayToHex(select)
        try {
            sendCard.sendCommand(channel, select_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to Select key Object";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }

    private boolean injectEncKey(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;

        //send "80 DA 01 6E 16
        //83 02 20 01
        //8F 10 4CB9D3A8E35A5E2CCD58FA1182332126" "9000"

        //#  PUT DATA OCI component SKS_MAC_DES KEY04

        byte[][] enc_arrays = new byte[4][];
        enc_arrays[0] = apdu_key;
        enc_arrays[1] = key_oid_enc;
        enc_arrays[2] = key_tag;
        enc_arrays[3] = enc_key;

        byte[] enc = util.combine_data(enc_arrays);

        CommandAPDU ENC_APDU = new CommandAPDU(enc);

        //80 e0 0007
        //1c 62 1a
        //82 02 10 64
        //83 02 0a 21
        //86 0a 0d 0c ff ff ff ff ff ff ff ff
        //9a 01 00 9b 01 00

         //1000 0000
        //"80 da 01 6e 
        //16 83 02 20 01 8f 10 040142434445464748494a4b4c4d4e4f"
        util.arrayToHex(enc);
        try {
            sendCard.sendCommand(channel, ENC_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to Inject ENC key";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }

    private boolean injectMacKey(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;
        //send "80 DA 01 6E 16
        // 83 02 20 03
        //8F 10 1C4C046B4223ABF89D3C4CFF30F69EDE" "9000"


        //#  PUT DATA OCI component SKS_ENC_DES KEY04
        //# update ACL <0A04> KEY04
        //send 00 DA 01 6F 0B 82 02 10 64 83 02 0A 04 860100 9000

        byte[][] mac_arrays = new byte[4][];
        mac_arrays[0] = apdu_key;
        mac_arrays[1] = key_oid_mac;
        mac_arrays[2] = key_tag;
        mac_arrays[3] = mac_key;

        byte[] mac = util.combine_data(mac_arrays);

        CommandAPDU MAC_APDU = new CommandAPDU(mac);
    //util.arrayToHex(mac)
        
        try {
            sendCard.sendCommand(channel, MAC_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to Inject MAC key";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }

    private boolean authObject(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;
        byte[] lc;
        byte[] FCP ;
        byte[] file_type;
        
        if (lb_moc)
        {
          //  updACL(channel);
        }
        
        
        //"80 E0 0006
        //1C 62 1A
        //82 0C 10 43
        //06 F3 B4 30 00 21 B8 30 00 21
        //83 02 01 21 
        //86 06 0D FF FF FF FF FF" "9000"

        //80 E0 00 06 
        //17 62 15 
        //82 0C 10 43 24 A6 B4 30 00 04 B8 30 00 04 
        //83 02 AA 04 
        //86 01 FF
        
        byte[] lc_moc={0x17};
        byte[] lc_eac={0x1C};
        byte[] fcp_moc={0x62,0x15};
        byte[] fcp_eac={0x62, 0x1A};
        byte[] file_type_eac = {(byte) 0x82, 0x0C, 0x10, 0x43, 0x06, (byte) 0xF3, (byte) 0xB4, 0x30, 0x00, 0x21, (byte) 0xB8, 0x30, 0x00, 0x21};
        byte[] file_type_moc={(byte) 0x82, 0x0C, 0x10, 0x43, 0x24, (byte)0xA6,(byte)0xB4, 0x30, 0x00, 0x04, (byte)0xB8, 0x30, 0x00, 0x04};
        byte[] file_id_eac = {(byte) 0x83, 0x02, 0x01, 0x21}; //02 means next 2 bytes define file type i-e Object ID AO
        byte[] file_id_moc = {(byte) 0x83, 0x02, (byte)0xAA, 0x04};
        byte[] access_rules_eac = {(byte) 0x86, 0x06, 0x0D, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
        byte[] access_rules_moc={(byte) 0x86, 0x01, (byte) 0xFF};
                
        
        if (lb_moc)
        {
            lc=lc_moc;
            FCP=fcp_moc;
            file_type=file_type_moc;
            file_id=file_id_moc;
            access_rules=access_rules_moc;
            
        }else
        {
            lc=lc_eac;
            FCP=fcp_eac;
            file_type=file_type_eac;
            file_id=file_id_eac;
            access_rules=access_rules_eac;
         }
                
        
        byte[][] ao_arrays = new byte[6][];
        ao_arrays[0] = ao_object;
        ao_arrays[1] = lc;
        ao_arrays[2] = FCP;
        ao_arrays[3] = file_type;
        ao_arrays[4] = file_id;
        ao_arrays[5] = access_rules;


        byte[] ao = util.combine_data(ao_arrays);
        //util.arrayToHex(ao)


        CommandAPDU AO_APDU = new CommandAPDU(ao);

        try {
            sendCard.sendCommand(channel, AO_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {

            // Object already Exists
            if (sb.toString().equalsIgnoreCase("6A89")) {
                return true;   
            }
            
            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to create Security Authenticate Object for BAC keys";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }
    
    
    
     private boolean updACL(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String ErrorMsg;
        
        //00 DA 01 6F 0B 
        //82 02 10 64 
        //83 02 0A 04 
        //86 01 00
        
        byte[] acl_cmd = {0x00, (byte) 0xDA, 0x01, 0x6F, 0x0B};
        byte[] upd_type = {(byte) 0x82, 0x02, 0x10,0x64};
        byte[] upd_id = {(byte) 0x83, 0x02, 0x0A, 0x04}; 
        byte[] acl_rules = {(byte) 0x86, 0x0A, 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};


        byte[][] ao_arrays = new byte[4][];
        ao_arrays[0] = acl_cmd;
        ao_arrays[1] = upd_type;
        ao_arrays[2] = upd_id;
        ao_arrays[3] = acl_rules;
        
     //00 da 01 6f 0b 
        //82 02 1064 
        //83 02 0a 04 
        //86 0a 00 00 00 00 00 00 00 00 00 00"
        
        byte[] ao = util.combine_data(ao_arrays);
        //util.arrayToHex(ao)


        CommandAPDU AO_APDU = new CommandAPDU(ao);

        try {
            sendCard.sendCommand(channel, AO_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            ErrorMsg = "Unable to create Security Authenticate Object for BAC keys";
            errMsg.append(ErrorMsg).append("  ");
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());

        } else {
            return true;
        }

    }
    
    
}
