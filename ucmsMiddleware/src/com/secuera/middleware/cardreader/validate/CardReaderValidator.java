/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.validate;

import com.secuera.middleware.beans.piv.CSRBean;
import com.secuera.middleware.beans.piv.ChuidBean;
import com.secuera.middleware.beans.piv.CredentialKeys;
import com.secuera.middleware.beans.piv.FacialImageBean;
import com.secuera.middleware.beans.piv.FascnBean;
import com.secuera.middleware.beans.piv.InitializationBean;
import com.secuera.middleware.beans.piv.PrintedInfoBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.core.common.CommonUtil;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class CardReaderValidator {

    CommonUtil cmnUtil = new CommonUtil();

    public List validatePrintedInfo(PrintedInfoBean printInfo) {

        List myList = new ArrayList();
        
        if (printInfo.getName() == null) {
            printInfo.setName("");
        } else {
            if (printInfo.getName().length() > 32) {
                myList.add("Invalid Length - Name");
            }
        }
        
        
        if (printInfo.getAffiliation() == null) {
            printInfo.setAffiliation("");
        } else {
            if (printInfo.getAffiliation().length() > 20) {
                myList.add("Invalid Length - Affiliation");
            }
        }
        
        
        if (printInfo.getCardSerialNumber() == null) {
            printInfo.setCardSerialNumber("");
        } else {
            if (printInfo.getCardSerialNumber().length() > 10) {
                myList.add("Invalid Length - Card Serial Number");
            }
        }
        
        
        if (printInfo.getIssuerID() == null) {
            printInfo.setIssuerID("");
        } else {
            if (printInfo.getIssuerID().length() > 15) {
                myList.add("Invalid Length - Issuer ID");
            }
        }
        
        
        if (printInfo.getOrganisationLine1() == null) {
            printInfo.setOrganisationLine1("");
        } else {
            if (printInfo.getOrganisationLine1().length() > 20) {
                myList.add("Invalid Length - Organization 1");
            }
        }
        
        
        if (printInfo.getOrganisationLine2()==null) {
            printInfo.setOrganisationLine2("");
        } else {
            if (printInfo.getOrganisationLine2().length() > 20) {
                myList.add("Invalid Length - Organization 2");
            }
        }

        return myList;
    }

    public List validateUserCertificate(CSRBean csrBean) {
        List myList = new ArrayList();
        String algoType;


        if (csrBean.getCertificateType() == null || csrBean.getCertificateType().length() == 0) {
            myList.add("Invalid Certificate Type");
        }

        if (csrBean.getOrganisationalunit() == null || csrBean.getOrganisationalunit().length() == 0) {
            myList.add("Invalid Organizational Unit");
        }

        if (csrBean.getOrganisation() == null || csrBean.getOrganisation().length() == 0) {
            myList.add("Invalid Organization");
        }

        if (csrBean.getCity() == null || csrBean.getCity().length() == 0) {
            myList.add("Invalid City");
        }

        if (csrBean.getState() == null || csrBean.getState().length() == 0) {
            myList.add("Invalid State");
        }

        if (csrBean.getCountry() == null || csrBean.getCountry().length() == 0) {
            myList.add("Invalid Country");
        }

        if (csrBean.getCert_template_oid() == null || csrBean.getCert_template_oid().equalsIgnoreCase("")) {
            myList.add("Invalid Template OID");
        }
        if ((Object) csrBean.getMajorVer() == null) {
            myList.add("Invalid Major ID");
        }
        if ((Object) csrBean.getMinorVer() == null) {
            myList.add("Invalid Major ID");
        }

        if (csrBean.getCaType() == null || csrBean.getCaType().length() == 0) {
            myList.add("Invalid CA Type");
        }

        if (csrBean.getCommonName() == null || csrBean.getCommonName().length() == 0) {
            myList.add("Invalid Common Name");
        }

        if (csrBean.getCertURL() == null || csrBean.getCertURL().length() == 0) {
            myList.add("Invalid URL");
        }

       

        if (csrBean.getKeyAlgorithmType() == null || csrBean.getKeyAlgorithmType().equalsIgnoreCase("")) {
            myList.add("Invalid Key Algorithm Type");
        }else{
            algoType = csrBean.getKeyAlgorithmType();
            if( algoType.equalsIgnoreCase(UcmsMiddlewareConstants.RSA1024 ) || 
                algoType.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048 ) || 
                algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC224 )  || 
                algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC256 )  ||   
                algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC384 ) ){

            }else{
                myList.add("Invalid Key Algorithm Type");
            }
        }  
        
        


        return myList;
    }

    
    public List validateCSR(CSRBean csrBean) {
        List myList = new ArrayList();



        if (csrBean.getCertificateType() == null || csrBean.getCertificateType().length() == 0) {
            myList.add("Invalid Certificate Type");
        }

        if (csrBean.getOrganisationalunit() == null || csrBean.getOrganisationalunit().length() == 0) {
            myList.add("Invalid Organizational Unit");
        }

        if (csrBean.getOrganisation() == null || csrBean.getOrganisation().length() == 0) {
            myList.add("Invalid Organization");
        }

        if (csrBean.getCity() == null || csrBean.getCity().length() == 0) {
            myList.add("Invalid City");
        }

        if (csrBean.getState() == null || csrBean.getState().length() == 0) {
            myList.add("Invalid State");
        }

        if (csrBean.getCountry() == null || csrBean.getCountry().length() == 0) {
            myList.add("Invalid Country");
        }

        if (csrBean.getCert_template_oid() == null || csrBean.getCert_template_oid().equalsIgnoreCase("")) {
            myList.add("Invalid Template OID");
        }
        if ((Object) csrBean.getMajorVer() == null) {
            myList.add("Invalid Major ID");
        }
        if ((Object) csrBean.getMinorVer() == null) {
            myList.add("Invalid Major ID");
        }

        
        if (csrBean.getCommonName() == null || csrBean.getCommonName().length() == 0) {
            myList.add("Invalid Common Name");
        }

        if (csrBean.getKeyAlgorithmType() == null || csrBean.getKeyAlgorithmType().equalsIgnoreCase("")) {
            myList.add("Invalid Key Algorithm Type");
        }


        return myList;
    }
    
    public List validateFASCN(FascnBean fascn) {
        List myList = new ArrayList();



        if (fascn.getAgencyCode() == null || fascn.getAgencyCode().length() == 0 || fascn.getAgencyCode().length() > 4) {
            myList.add("Invalid Agency Code");
        }

        if (fascn.getSystemCode() == null || fascn.getSystemCode().length() == 0 || fascn.getSystemCode().length() > 4) {
            myList.add("Invalid System Code");
        }

        if (fascn.getCredentialNumber() == null || fascn.getCredentialNumber().length() == 0 || fascn.getCredentialNumber().length() > 6) {
            myList.add("Invalid Credential Number");
        }

        if (fascn.getCs() == null || fascn.getCs().length() == 0 || fascn.getCs().length() > 1) {
            myList.add("Invalid CS");
        }

        if (fascn.getIci() == null || fascn.getIci().length() == 0 || fascn.getIci().length() > 1) {
            myList.add("Invalid ICI");
        }

        if (fascn.getPerson() == null || fascn.getPerson().length() == 0 || fascn.getPerson().length() > 16) {
            myList.add("Invalid Person");
        }




        return myList;
    }

    public List validateFacialImage(FacialImageBean facialImage) {
        List myList = new ArrayList();

        if (facialImage.getCreationDate() == null || facialImage.getCreationDate().length() == 0) {
            myList.add("Invalid Creation Date");
        }

        if (facialImage.getExpirationDate() == null || facialImage.getExpirationDate().length() == 0) {
            myList.add("Invalid Expiration Date");
        }

        if (facialImage.getValidFromDate() == null || facialImage.getValidFromDate().length() == 0) {
            myList.add("Invalid Valid From Date");
        }

        if (facialImage.getCreator() == null || facialImage.getCreator().length() == 0) {
            myList.add("Invalid Creator");
        }
        return myList;
    }

    public List validateCHUID(ChuidBean chuid) {
        List myList = new ArrayList();


        if (chuid.getAgencyCode() == null || chuid.getAgencyCode().length() != 4 || !chuid.getAgencyCode().matches("[0-9]+")) {
            myList.add("Invalid Agency Code, Agency Code should be Numeric(4)");
        }

        if (chuid.getSystemCode() == null || chuid.getSystemCode().length() != 4 || !chuid.getSystemCode().matches("[0-9]+")) {
            myList.add("Invalid Agency Code, System Code should be Numeric(4)");
        }

        if (chuid.getCredentialNumber() == null || chuid.getCredentialNumber().length() != 6 || !chuid.getCredentialNumber().matches("[0-9]+")) {
            myList.add("Invalid Agency Code, Credential Number should be Numeric(6)");
        }

        if (chuid.getCs() == null || chuid.getCs().length() != 1 || !chuid.getCs().matches("[0-9]+")) {
            myList.add("Invalid Agency Code, CS should be Numeric(1)");
        }

        if (chuid.getIci() == null || chuid.getIci().length() != 1 || !chuid.getIci().matches("[0-9]+")) {
            myList.add("Invalid Agency Code, ICI should be Numeric(1)");
        }

        if (chuid.getPerson() == null || chuid.getPerson().length() != 16 || !chuid.getPerson().matches("[0-9]+")) {
            myList.add("Invalid Agency Code, Person should be Numeric(16)");
        }
        


        return myList;
    }

    public List validateCredentialKeys(CredentialKeys credentialKeys) {
        List myList = new ArrayList();
        String algorithmType = "";
        int keyLength;

        //check admin Key
        if (credentialKeys.getAdminKey() == null || cmnUtil.arrayToHex(credentialKeys.getAdminKey()).length() == 0) {
            myList.add("Admin Key Missing.");
        } else {
            if (credentialKeys.getAdminKeyAlgorithmType() == null || credentialKeys.getAdminKeyAlgorithmType().length() == 0) {
                myList.add("Invalid AdminKey Algorithm Type.");
            } else {
                algorithmType = credentialKeys.getAdminKeyAlgorithmType();
                keyLength = cmnUtil.arrayToHex(credentialKeys.getAdminKey()).length();

                if (UcmsMiddlewareConstants.AES128ECB.equalsIgnoreCase(algorithmType) && keyLength != 32) {
                    myList.add("Invalid AdminKey Key Length.");
                }
                if (UcmsMiddlewareConstants.TripleDESECB.equalsIgnoreCase(algorithmType) && keyLength != 48) {
                    myList.add("Invalid AdminKey Key Length.");
                }
            }
        }

        //Check GP Keys
        if (credentialKeys.getMasterKey() == null || cmnUtil.arrayToHex(credentialKeys.getMasterKey()).length() == 0) {
            credentialKeys.setMasterKey("");
            //Check for ENC Key
            if (credentialKeys.getEncKey() == null || cmnUtil.arrayToHex(credentialKeys.getEncKey()).length() == 0) {
                myList.add("Invalid Master Key/GP keys (Enc Key)");
            } else {
                if (credentialKeys.getGpKeysAlgorithmType() == null || credentialKeys.getGpKeysAlgorithmType().length() == 0) {
                    myList.add("Invalid GP Keys Algorithm Type.");
                } else {
                    algorithmType = credentialKeys.getGpKeysAlgorithmType();
                    keyLength = cmnUtil.arrayToHex(credentialKeys.getEncKey()).length();

                    if (UcmsMiddlewareConstants.AES128ECB.equalsIgnoreCase(algorithmType) && keyLength != 32) {
                        myList.add("Invalid ENC Key Length.");
                    }
                    if (UcmsMiddlewareConstants.TripleDESECB.equalsIgnoreCase(algorithmType) && keyLength != 48) {
                        myList.add("Invalid ENC Key Length.");
                    }
                }
            }

            //Check for MAC Key
            if (credentialKeys.getMacKey() == null || cmnUtil.arrayToHex(credentialKeys.getMacKey()).length() == 0) {
                myList.add("Invalid Master Key/GP keys (MAC Key)");
            } else {
                if (credentialKeys.getGpKeysAlgorithmType() == null || credentialKeys.getGpKeysAlgorithmType().length() == 0) {
                    myList.add("Invalid GP Keys Algorithm Type.");
                } else {
                    algorithmType = credentialKeys.getGpKeysAlgorithmType();
                    keyLength = cmnUtil.arrayToHex(credentialKeys.getMacKey()).length();

                    if (UcmsMiddlewareConstants.AES128ECB.equalsIgnoreCase(algorithmType) && keyLength != 32) {
                        myList.add("Invalid MAC Key Length.");
                    }
                    if (UcmsMiddlewareConstants.TripleDESECB.equalsIgnoreCase(algorithmType) && keyLength != 48) {
                        myList.add("Invalid MAC Key Length.");
                    }
                }
            }


            //Check for DEK Key
            if (credentialKeys.getDekKey() == null || cmnUtil.arrayToHex(credentialKeys.getDekKey()).length() == 0) {
                myList.add("Invalid Master Key/GP keys (DEK Key)");
            } else {
                if (credentialKeys.getGpKeysAlgorithmType() == null || credentialKeys.getGpKeysAlgorithmType().length() == 0) {
                    myList.add("Invalid GP Keys Algorithm Type.");
                } else {
                    algorithmType = credentialKeys.getGpKeysAlgorithmType();
                    keyLength = cmnUtil.arrayToHex(credentialKeys.getDekKey()).length();

                    if (UcmsMiddlewareConstants.AES128ECB.equalsIgnoreCase(algorithmType) && keyLength != 32) {
                        myList.add("Invalid DEK Key Length.");
                    }
                    if (UcmsMiddlewareConstants.TripleDESECB.equalsIgnoreCase(algorithmType) && keyLength != 48) {
                        myList.add("Invalid DEK Key Length.");
                    }
                }
            }
        } else {
            if (credentialKeys.getMasterKeyAlgorithmType() == null || credentialKeys.getMasterKeyAlgorithmType().length() == 0) {
                myList.add("Invalid Master Key Algorithm Type.");
            } else {
                algorithmType = credentialKeys.getMasterKeyAlgorithmType();
                keyLength = cmnUtil.arrayToHex(credentialKeys.getMasterKey()).length();

                if (UcmsMiddlewareConstants.AES128ECB.equalsIgnoreCase(algorithmType) && keyLength != 32) {
                    myList.add("Invalid Master Key Length.");
                }
                if (UcmsMiddlewareConstants.TripleDESECB.equalsIgnoreCase(algorithmType) && keyLength != 48) {
                    myList.add("Invalid Master Key Length.");
                }
            }
        }

        if (credentialKeys.getCardManagerAid() == null || credentialKeys.getCardManagerAid().length == 0) {
            myList.add("Invalid Card Manager AID.");
        }

        return myList;
    }

    public List validateInitValues(InitializationBean initvalue) {
        List myList = new ArrayList();
        String algorithmType = "";
        int keyLength;

        //check admin Key
        if (initvalue.getAdminKey() == null || cmnUtil.arrayToHex(initvalue.getAdminKey()).length() == 0) {
            myList.add("Custom - Admin Key Missing.");
        } else {
            if (initvalue.getAdminKeyAlgorithmType() == null || initvalue.getAdminKeyAlgorithmType().length() == 0) {
                myList.add("Custom - AdminKey Algorithm Type Missing.");
            } else {
                algorithmType = initvalue.getAdminKeyAlgorithmType();
                keyLength = cmnUtil.arrayToHex(initvalue.getAdminKey()).length();

                if (UcmsMiddlewareConstants.AES128ECB.equalsIgnoreCase(algorithmType) && keyLength != 32) {
                    myList.add("Custom - AdminKey Key Length error.");
                }
                if (UcmsMiddlewareConstants.TripleDESECB.equalsIgnoreCase(algorithmType) && keyLength != 48) {
                    myList.add("Custom - AdminKey Key Length error.");
                }
            }
        }
        
        //insertGPKeys
        if ("Y".equalsIgnoreCase(initvalue.getInsertGPKeys())) {
            //Check for ENC Key
            if (initvalue.getEncKey() == null || cmnUtil.arrayToHex(initvalue.getEncKey()).length() == 0) {
                myList.add("Custom - GP keys(ENC Key) Invalid");
            } else {
                if (initvalue.getGpKeysAlgorithmType() == null || initvalue.getGpKeysAlgorithmType().length() == 0) {
                    myList.add("Custom - GP Keys Algorithm Type Invalid.");
                } else {
                    algorithmType = initvalue.getGpKeysAlgorithmType();
                    keyLength = cmnUtil.arrayToHex(initvalue.getEncKey()).length();

                    if (UcmsMiddlewareConstants.AES128ECB.equalsIgnoreCase(algorithmType) && keyLength != 32) {
                        myList.add("Custom - ENC Key Length error.");
                    }
                    if (UcmsMiddlewareConstants.TripleDESECB.equalsIgnoreCase(algorithmType) && keyLength != 48) {
                        myList.add("Custom - ENC Key Length error.");
                    }
                }
            }

            //Check for MAC Key
            if (initvalue.getMacKey() == null || cmnUtil.arrayToHex(initvalue.getMacKey()).length() == 0) {
                myList.add("Custom - GP keys(MAC Key) Invalid");
            } else {
                if (initvalue.getGpKeysAlgorithmType() == null || initvalue.getGpKeysAlgorithmType().length() == 0) {
                    myList.add("Custom - GP Keys Algorithm Type Invalid.");
                } else {
                    algorithmType = initvalue.getGpKeysAlgorithmType();
                    keyLength = cmnUtil.arrayToHex(initvalue.getMacKey()).length();

                    if (UcmsMiddlewareConstants.AES128ECB.equalsIgnoreCase(algorithmType) && keyLength != 32) {
                        myList.add("Custom - MAC Key Length error.");
                    }
                    if (UcmsMiddlewareConstants.TripleDESECB.equalsIgnoreCase(algorithmType) && keyLength != 48) {
                        myList.add("Custom - MAC Key Length error.");
                    }
                }
            }


            //Check for DEK Key
            if (initvalue.getDekKey() == null || cmnUtil.arrayToHex(initvalue.getDekKey()).length() == 0) {
                myList.add("Custom - GP keys(DEK Key) Invalid");
            } else {
                if (initvalue.getGpKeysAlgorithmType() == null || initvalue.getGpKeysAlgorithmType().length() == 0) {
                    myList.add("Invalid GP Keys Algorithm Type.");
                } else {
                    algorithmType = initvalue.getGpKeysAlgorithmType();
                    keyLength = cmnUtil.arrayToHex(initvalue.getDekKey()).length();

                    if (UcmsMiddlewareConstants.AES128ECB.equalsIgnoreCase(algorithmType) && keyLength != 32) {
                        myList.add("Custom - MAC Key Length error.");
                    }
                    if (UcmsMiddlewareConstants.TripleDESECB.equalsIgnoreCase(algorithmType) && keyLength != 48) {
                        myList.add("Invalid DEK Key Length.");
                    }
                }
            }
        }
        
        if(initvalue.getLocalPin()==null || initvalue.getLocalPin().length()==0 ){
            myList.add("Invalid Loacl PIN.");
        }
        
        if(initvalue.getGlobalPin()==null || initvalue.getGlobalPin().length()==0 ){
            myList.add("Invalid Global PIN.");
        }
        
        if(initvalue.getUnblockPin()==null || initvalue.getUnblockPin().length()==0 ){
            myList.add("Invalid Unblock PIN.");
        }
        return myList;
    }
}
