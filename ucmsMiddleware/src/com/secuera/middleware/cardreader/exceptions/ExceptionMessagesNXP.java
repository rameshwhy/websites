package com.secuera.middleware.cardreader.exceptions;

//import com.secuera.entities.MiddleWareException;
public class ExceptionMessagesNXP {

    public static String INVALID_CHALLENGE = "Error while trying to get challenge from card";
    public static String FILE_ALREADY_EXISTS = "Error: The file already exists";
    public static String APPLET_SELECTION_ERROR = "Error while trying to select applet on the card";
    public static String INVALID_SECUREENV1 = "Error while trying to set security environment(1)";
    public static String INVALID_SECUREENV2 = "Error while trying to set security environment(2)";
     

    
        
    
    public String GetErrorMsg(String errorCode, String modType) {
        StringBuilder retValue = new StringBuilder();
        retValue.append("");


        if (modType.equalsIgnoreCase("PIN")) {

            System.out.println(errorCode.substring(0, 3));
            if (errorCode.substring(0, 3).equalsIgnoreCase("63C")) {
                retValue.append("Invalid PIN.").append(errorCode);
            }

            if (errorCode.equalsIgnoreCase("6983")) {
                retValue.append("PIN is Blocked. Please run Unblock Local PIN.").append(errorCode);
            }
        }



        if (errorCode.equalsIgnoreCase("6e00")) {

            retValue.append("Card is Locked, please run Unlock the card function.").append(errorCode);

        }

        if (errorCode.equalsIgnoreCase("6a88")) {

            retValue.append("Reference data not not found.").append(errorCode);

        }

        if (errorCode.equalsIgnoreCase("6a80")) {

            retValue.append("Reference data not not found.").append(errorCode);

        }
        if (errorCode.equalsIgnoreCase("6d00")) {

            retValue.append("instruction code not supported or invalid.").append(errorCode);

        }

        if (errorCode.substring(0, 2).equalsIgnoreCase("6C")) {
            retValue.append("Wrong length Le,SW2 indicates the correct Length.").append(errorCode);
        }





        if (retValue.toString().equalsIgnoreCase("")) {
            retValue.append("Unknown Error.").append(errorCode);
        }

        return retValue.toString();
    }
}
