/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.Date;

import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import sun.security.util.DerEncoder;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.util.ObjectIdentifier;

/**
 *
 * 
 * @author Harpreet Singh
 */
public class LoadSecurityCont {

	SendCommand sendCard = new SendCommand();
	CommonUtil util = new CommonUtil();
	PivUtilInterface piv = new PivUtil();
        GenAsmSig asmSig= new GenAsmSig();
        GeneralAuth auth = new GeneralAuth();
        
         /* Declare OIDS here*/
         final int Signed_data[] = {1,2,840,113549,1,7,2};  //- signedData OID
         final int LDS_Security_Object[]= {1,3,27,1,1,1}; //LDS Security object OID
         final int Digest_SHA512_data[] = {2, 16, 840, 1, 101, 3, 4, 2, 3};

         byte[] num_bytes;

	public void loadSecCont(CardChannel channel,byte[] Printed_info,byte[] Chuid_info,byte[] Finger_info,
                                byte[] Facial_info,PrivateKey privateKey, byte[] cert,Date dt_signingtime,
                byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY) throws IOException, MiddleWareException
        {
            byte[] asmSignature;
            byte[] LDS_Object;
            byte[] SecurityData;
            
            
            LDS_Object=LDSObject(Printed_info,Chuid_info,Finger_info,Facial_info);
            
            String str_hex_input=util.arrayToHex(LDS_Object);
             
            // Now get the Signed Object.
            asmSignature=util.hex1StringToByteArray(asmSig.genAsmSig(str_hex_input,privateKey,cert,"",dt_signingtime,'S'));
             
            //Now add header OID's to the LDS Object.
              LDS_Object =addLDSHeader(LDS_Object);
                        
            SecurityData=util.combine_data(LDS_Object, asmSignature);
            
            // Now add Signed data OID and put the whole data in Sequence.
            byte[] signedData_oid=getSignedDataOID();
            SecurityData=util.combine_data(signedData_oid, SecurityData);
            
            SecurityData=wrapInSequence(SecurityData);
            
            // Now load the security data on the card.
            loadCont(channel,PRE_PERSO_PIV_ADMIN_KEY,PIV_ADMIN_KEY,SecurityData);
            
        }
         
        private byte[] LDSObject(byte[] Printed_info,byte[] Chuid_info,byte[] Finger_info,
                                byte[] Facial_info) throws IOException, MiddleWareException
        {
            DerOutputStream out_write;
            out_write = new DerOutputStream();
                
            byte[] lds_ver={0x02,0x01,0x00};   // This represents version nbr of LDS Security object.
            byte[] digest_algo;
            byte[] group_hash;
            
            byte[] LDS_Object;
            //Get Digest Algo.
            digest_algo=getDigestAlgo();
            //Get dataGroupHashValues
            group_hash=getGroupHash(Printed_info,Chuid_info,Finger_info,Facial_info);
            
                
                byte[][] grp_arrays = new byte[3][];
                         grp_arrays[0] = lds_ver;
                         grp_arrays[1] = digest_algo;
                         grp_arrays[2] = group_hash;
                         
                byte[] grp_bytes = util.combine_data(grp_arrays);
                
                DerValue attr_grp = new DerValue(grp_bytes);
                DerValue[] x = {attr_grp};
                out_write.putSequence(x);
            
                LDS_Object=out_write.toByteArray();
            
                out_write.close();
                
            return LDS_Object;
        }
        
        
        
        private byte[] getGroupHash(byte[] Printed_info,byte[] Chuid_info,byte[] Finger_info,byte[] Facial_info) throws IOException
        {
        
                byte[] printed_hash=null;
                byte[] chuid_hash=null;
                byte[] finger_hash=null;
                byte[] facial_hash=null;
                byte[] grp_hash;
                
                DerOutputStream out_write;
                out_write = new DerOutputStream();
                
                printed_hash=getMessageHash(Printed_info);
                chuid_hash=getMessageHash(Chuid_info);
                finger_hash=getMessageHash(Finger_info);
                facial_hash=getMessageHash(Facial_info);
            
            
                byte[] der_printed=getDerMessageDigest(printed_hash,1);              
                byte[] der_chuid=getDerMessageDigest(chuid_hash,2);
                byte[] der_finger=getDerMessageDigest(finger_hash,3);
                byte[] der_facial=getDerMessageDigest(facial_hash,4);
             
                byte[][] grp_arrays = new byte[4][];
                         grp_arrays[0] = der_printed;
                         grp_arrays[1] = der_chuid;
                         grp_arrays[2] = der_finger;
                         grp_arrays[3] = der_facial;
                
                byte[] grp_bytes = util.combine_data(grp_arrays);
                
                DerValue attr_grp = new DerValue(grp_bytes);
                DerValue[] x = {attr_grp};
                out_write.putSequence(x);

                grp_hash=out_write.toByteArray();
                
                out_write.close();
                
              return grp_hash;
                        
	}

	
        public boolean loadCont(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY,
            byte[] secData) throws MiddleWareException
        {

		// TO write Security Container
		// Security Object (PIV) 0x9000
		// Data Element (TLV) Tag Type Max. Bytes
		// Mapping of DG to container ID 0xBA Variable 100
		// LDS Security Object (MRTDDocument SO) 0xBB Variable 900
		// Error Detection Code 0xFE LRC 0

		// 00 DB 3F FF 00
		// 03 5D 5C 03 5F C1 06 
                // 53 82 03 54 BA 15 01 DB 00
		// 02 30 00 03 60 10 05 60 30 06 30 01 07 60 50 09
		// 10 15 BB 82 03 37 30 82 03 33 06 09 2A 86 48 86
		// F7 0D 01 07 02 A0 82 03 24 30 82 03 20 02 01 03
		// 31 0F 30 0D 06 09 60 86 48 01 65 03 04 02 01 05
		// 00 30 82 01 3A 06 05 2B 1B 01 01 01 A0 82 01 2F
		// 04 82 01 2B 30 82 01 27 02 01 00 30 0D 06 09 60

		// Here are listed all the container ID's which may be mapped/loaded to
		// security container.
		// Card Capability Container 0xDB00
		// CHUID 0x3000
		// FingerPrint 0x6010
		// Facial Image 0x6030
		// Printed Info 0x3001
		// Discovery Object 0x6050
		// Iris Image 0x1015

        boolean rtnvalue = true;
        StringBuffer sb = new StringBuffer();
        byte[] apdu_security = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};
        byte[] error_tag = {(byte) 0xFE, 0x00};
        
        Integer len = secData.length;
        calcBertValue(len,false);
        
        byte[] byte_LDS_len = num_bytes;
        
        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        
        //////////////////////////////////////////////////////	
        byte[] cont_id = { 0x5c, 0x03, 0x5F, (byte) 0xC1, 0x06 }; //For Security Container

	// Now Define Datagroups to container mapping.
	byte[] map_tag = { (byte) 0xBA }; // Tag indicating start of mapping DG1 to containers.
	byte[] map_len= {0x0C};       // Total length of DG mapping is 4*3=12.
        byte[] printed_cont = {0x01,0x30,0x01 }; //Printed Info container mapped to DG1.
        byte[] chuid_cont = {0x02,0x30,0x00 }; //CHUID container mapped to DG2.
        byte[] finger_cont = {0x03,0x60,0x10 }; //Finger Print container mapped to DG2.
        byte[] facial_cont = {0x04,0x60,0x30 }; //Facial container mapped to DG4.
                
        // Now define Security object Tag.
	byte[] security_tag = {(byte) 0xBB };
       
        ///////////////////////////////////////////////////////
         byte[][] grp_arrays = new byte[10][];
                         grp_arrays[0] = map_tag;
                         grp_arrays[1] = map_len;
                         grp_arrays[2] = printed_cont;
                         grp_arrays[3] = chuid_cont;
                         grp_arrays[4] = finger_cont;
                         grp_arrays[5] = facial_cont;
                         grp_arrays[6] = security_tag;
                         grp_arrays[7] = byte_LDS_len;
                         grp_arrays[8] = secData;
                         grp_arrays[9] = error_tag;
                         
        byte[] grp_bytes = util.combine_data(grp_arrays);

       //Calculate size of the total Security data record to be stored.
        Integer len_sec = grp_bytes.length;
        calcBertValue(len_sec,true);
        
        byte[] byte_sec_len = num_bytes;
                
        byte[] apdu_sec_data = util.combine_data(byte_sec_len, grp_bytes);
                apdu_sec_data = util.combine_data(cont_id, apdu_sec_data);
        
        Integer len_total_apdu = apdu_sec_data.length;
        calcBertValue(len_total_apdu, false);

        byte[] total_size = num_bytes;
        byte[] zero_size = {0x00};

        if (total_size.length > 2) {
            total_size = util.combine_data(zero_size, total_size);
        }

        
        apdu_sec_data = util.combine_data(total_size,apdu_sec_data);
        apdu_sec_data = util.combine_data(apdu_security,apdu_sec_data);


        System.out.println(util.arrayToHex(apdu_sec_data));

        CommandAPDU READ_APDU = new CommandAPDU(apdu_sec_data);

        try {

            sendCard.sendCommand(channel, READ_APDU, sb);

            //Raise Error if APDU command is not successful
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();
                errMsg.append("Unable to write Facial Image");
                errMsg.append(" (").append(sb.toString()).append(")");
                throw new MiddleWareException(errMsg.toString());
            }
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        return rtnvalue;
    }
        
        
        

        
        private byte[] getMessageHash(byte[] input_data) throws IOException {

        byte[] hash ;

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        digest.update(input_data);
        hash = digest.digest();
        
        
        return hash;

        }
        
     private byte[] getSignedDataOID() throws MiddleWareException, IOException {
        
         byte[] signedData = null;
        
        DerOutputStream out_1;
        out_1 = new DerOutputStream();
        
        // Out put the OID
        ObjectIdentifier Signed_data_OID = ObjectIdentifier.newInternal(Signed_data);
        out_1.putOID(Signed_data_OID);

        signedData=out_1.toByteArray();
        out_1.close();
        
        return signedData;

    }
 
        
        
      private byte[] getDigestAlgo() throws MiddleWareException, IOException {
        
            byte[] digestAlgo = null;
        
        DerOutputStream out_1, out_2;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        
        // Out put the OID
        ObjectIdentifier Digest_SHA512_data_OID = ObjectIdentifier.newInternal(Digest_SHA512_data);
        out_1.putOID(Digest_SHA512_data_OID);

        DerValue attr_oid = new DerValue(out_1.toByteArray());

        DerValue[] x = {attr_oid};
        out_2.putSequence(x);

        digestAlgo = out_2.toByteArray();

        out_1.close();
        out_2.close();
        
        return digestAlgo;

    }
     
      
       private byte[] addLDSHeader(byte[] input_data) throws MiddleWareException, IOException {
        
         byte[] headerFormat = null;
         byte[] lds_ver={0x02,0x01,0x03};   // This represents version nbr of Signed object.
         
        DerOutputStream out_1, out_2,out_3;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        
        //Out put the OID
        ObjectIdentifier LDS_Security_Object_OID = ObjectIdentifier.newInternal(LDS_Security_Object);
        out_1.putOID(LDS_Security_Object_OID);

        byte[] sec_data=util.combine_data(out_1.toByteArray(), input_data);
        
        DerValue attr_oid = new DerValue(sec_data);
        DerValue[] x = {attr_oid};
        out_2.putSequence(x);

        /*************/
        // Now get the set of digestAlgorithms
        byte[] set_of_algo=getSetDigestAlgo();
        
        /***********/
         byte[][] hdr_arrays = new byte[3][];
                         hdr_arrays[0] = lds_ver;
                         hdr_arrays[1] = set_of_algo;
                         hdr_arrays[2] = out_2.toByteArray();
                         
                         
        byte[] hdr_bytes = util.combine_data(hdr_arrays);

        /***Now put the Header bytes in Sequence***/
        
        DerValue attr_hdr = new DerValue(hdr_bytes);
        DerValue[] y = {attr_hdr};
        out_3.putSequence(y);
        
        
        headerFormat = out_3.toByteArray();

        out_1.close();
        out_2.close();
        out_3.close();
        
        return headerFormat;

    }
      
       
       
       private byte[] getSetDigestAlgo() throws MiddleWareException, IOException {
        
         byte[] digestAlgo = null;
        
        DerOutputStream out_1, out_2,out_3;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        
        // Out put the OID
        ObjectIdentifier Digest_SHA512_data_OID = ObjectIdentifier.newInternal(Digest_SHA512_data);
        out_1.putOID(Digest_SHA512_data_OID);

        DerValue attr_oid = new DerValue(out_1.toByteArray());

        DerValue[] x = {attr_oid};
        out_2.putSequence(x);

        // Encode the values in Set
        DerEncoder[] y = {out_2};
        out_3.putOrderedSet(DerValue.tag_Set, y);
        
        digestAlgo = out_3.toByteArray();

        out_1.close();
        out_2.close();
        out_3.close();
        
        return digestAlgo;

    }
      
       private byte[] wrapInSequence(byte[] input_data) throws MiddleWareException, IOException {
        
        byte[] output_data = null;
        
        DerOutputStream out_1;
        out_1 = new DerOutputStream();
        
        // Out put the OID
        DerValue attr_oid = new DerValue(input_data);

        DerValue[] x = {attr_oid};
        out_1.putSequence(x);

        output_data = out_1.toByteArray();

        out_1.close();
        
        
        return output_data;

    }

                
      private byte[] getDerMessageDigest(byte[] hash,int group_nbr) throws IOException {

        byte[] derHash = null;
        
        DerOutputStream out_1, out_2, out_hash;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_hash = new DerOutputStream();

        //Get Value of the Hash of original message.
        out_1.putInteger(group_nbr);
        out_2.putOctetString(hash);

        DerValue attr_grp = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_2.toByteArray());

        DerValue[] x = {attr_grp,attr_value};
        out_hash.putSequence(x);
  
        derHash = out_hash.toByteArray();

        out_1.close();
        out_2.close();
        out_hash.close();

        return derHash;

    }
  
             
      private void calcBertValue(Integer len, boolean binclude_bert) {
        // Determine value of Bert Tag
        // if (binclude_bert)
        byte[] bert_tag;
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for
        // size of data bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size
        // of data bytes
        byte[] len_tag_small = {(byte) 0x81};
        byte[] len_tag_big = {(byte) 0x82};

        if (len > 255) {
            if (binclude_bert) {
                bert_tag = bert_tag_size_big;
            } else {
                bert_tag = len_tag_big;
            }

        } else {
            if (binclude_bert) {
                bert_tag = bert_tag_size_small;
            } else {
                bert_tag = len_tag_small;
            }

        }

        // Now calculate actual data size being stored.
        // Following condition makes sure that converted hex string is always
        // even.

        String tot_hex_size;

        if (len > 255 && len < 4096) {

            tot_hex_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_size = Integer.toHexString(len);

        }

        num_bytes = util.combine_data(bert_tag, util.hex1ToByteArray(tot_hex_size));

    }
      
}
