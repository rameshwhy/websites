/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.applet;
 

/**
 *
 * @author admin
 */
public class CardreaderResponseBean {

    private CardreaderError cardreaderError;
    private Object cardreaderResponse;

    public CardreaderError getCardreaderError() {
        return cardreaderError;
    }

    public void setCardreaderError(CardreaderError cardreaderError) {
        this.cardreaderError = cardreaderError;
    }

    public Object getCardreaderResponse() {
        return cardreaderResponse;
    }

    public void setCardreaderResponse(Object cardreaderResponse) {
        this.cardreaderResponse = cardreaderResponse;
    }

}
