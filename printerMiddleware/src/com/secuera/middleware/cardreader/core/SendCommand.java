/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import javax.smartcardio.ResponseAPDU;
/**
 *
 * @author admin
 */
public class SendCommand {
	byte[] retValue;

	public byte[] sendCommand(CardChannel channel, CommandAPDU command,
			StringBuffer sb) throws CardException {

		try {

			ResponseAPDU responseAPDU = channel.transmit(command);
			int responseStatus = responseAPDU.getSW();

			sb.delete(0, sb.length());
			sb.append(Integer.toHexString(responseStatus));
			return responseAPDU.getData();

		} catch (CardException e) {
			throw e;
		}
		
	}

	/*
	 * public byte[] sendCommand(CardChannel channel, CommandAPDU command,
	 * StringBuffer sb) throws CardException {
	 * 
	 * 
	 * ResponseAPDU responseAPDU = channel.transmit(command); int responseStatus
	 * = responseAPDU.getSW();
	 * 
	 * 
	 * sb.delete(0, sb.length());
	 * sb.append(Integer.toHexString(responseStatus));
	 * 
	 * //if (!isResponseOk(responseStatus)) {
	 * //System.out.println("Error code: " + responseStatus); // throw new
	 * RuntimeException("Error code: " + responseStatus); //}
	 * //System.out.println("GetData " + responseAPDU.toString());
	 * 
	 * return responseAPDU.getData(); }
	 * 
	 * //private boolean isResponseOk(int responseStatus) { // return
	 * responseStatus == 0x9000; //}
	 */
}

