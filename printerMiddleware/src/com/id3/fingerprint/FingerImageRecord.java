/*     */ package com.id3.fingerprint;
/*     */ 
import java.io.File;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class FingerImageRecord
/*     */ {
/*  16 */   private long _handle = 0L;
/*  17 */   private boolean _disposed = false;
/*  18 */   private boolean _disposable = true;
/*  19 */   ArrayList<FingerImage> _images = new ArrayList();
/*     */   private FingerImage[] _imageArray;
/*     */ 
/*     */   static
/*     */   {
/* 469 */     String tmpStr = "id3Finger.dll";
                File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
                File localFile = new File(TMP_DIR, tmpStr);


                 System.load(localFile.getAbsolutePath());
/*     */   }
/*     */ 
/*     */   public FingerImageRecord()
/*     */     throws FingerException
/*     */   {
/*  29 */     long[] handle_ptr = new long[1];
/*  30 */     int result = nInitialize(handle_ptr);
/*     */ 
/*  32 */     if (result != 0) {
/*  33 */       throw new FingerException(result);
/*     */     }
/*  35 */     this._handle = handle_ptr[0];
/*     */   }
/*     */ 
/*     */   public FingerImageRecord(FingerImage image)
/*     */     throws FingerException
/*     */   {
/*  48 */     long[] handle_ptr = new long[1];
/*  49 */     int result = nInitialize(handle_ptr);
/*     */ 
/*  51 */     if (result == 0) {
/*  52 */       this._handle = handle_ptr[0];
/*  53 */       add(image);
/*     */     }
/*     */ 
/*  56 */     if (result != 0)
/*  57 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public FingerImageRecord(long handle, boolean disposable)
/*     */     throws FingerException
/*     */   {
/*  71 */     this._handle = handle;
/*  72 */     this._disposable = disposable;
/*     */ 
/*  74 */     int imageCount = getImageCount();
/*     */ 
/*  76 */     for (int i = 0; i < imageCount; i++) {
/*  77 */       long[] hImage = new long[1];
/*     */ 
/*  79 */       int result = nGetImage(this._handle, i, hImage);
/*     */ 
/*  81 */       if (result != 0) {
/*  82 */         throw new FingerException(result);
/*     */       }
/*  84 */       this._images.add(new FingerImage(hImage[0], disposable));
/*     */     }
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/*  93 */     nDispose(this._handle, 0);
/*  94 */     this._handle = 0L;
/*  95 */     this._disposed = true;
/*     */   }
/*     */ 
/*     */   protected void finalize()
/*     */     throws Throwable
/*     */   {
/*     */     try
/*     */     {
/* 103 */       this._imageArray = null;
/* 104 */       this._images.clear();
/*     */ 
/* 106 */       if (this._disposable)
/* 107 */         dispose();
/*     */     } catch (Exception localException) {
/*     */     } finally {
/* 110 */       super.finalize();
/*     */     }
/*     */   }
/*     */ 
/*     */   public long getHandle()
/*     */   {
/* 120 */     return this._handle;
/*     */   }
/*     */ 
/*     */   public FingerImageRecord clone()
/*     */   {
/* 129 */     long[] hClonedImage = new long[1];
/* 130 */     int result = nClone(this._handle, hClonedImage);
/*     */ 
/* 132 */     if (result == 0)
/*     */       try {
/* 134 */         return new FingerImageRecord(hClonedImage[0], true);
/*     */       }
/*     */       catch (FingerException localFingerException) {
/*     */       }
/* 138 */     return null;
/*     */   }
/*     */ 
/*     */   public static FingerImageRecord fromFile(String filename, int format)
/*     */     throws FingerException
/*     */   {
/* 158 */     long[] handle = new long[1];
/* 159 */     int result = nFromFile(filename, format, handle);
/*     */ 
/* 161 */     if (result != 0) {
/* 162 */       throw new FingerException(result);
/*     */     }
/* 164 */     return new FingerImageRecord(handle[0], true);
/*     */   }
/*     */ 
/*     */   public static FingerImageRecord fromBuffer(byte[] data, int format)
/*     */     throws FingerException
/*     */   {
/* 183 */     long[] handle = new long[1];
/* 184 */     int result = nFromBuffer(data, format, handle);
/*     */ 
/* 186 */     if (result != 0) {
/* 187 */       throw new FingerException(result);
/*     */     }
/* 189 */     return new FingerImageRecord(handle[0], true);
/*     */   }
/*     */ 
/*     */   public void save(String filename, int format)
/*     */     throws FingerException
/*     */   {
/* 206 */     int result = nSave(this._handle, format, filename);
/*     */ 
/* 208 */     if (result != 0)
/* 209 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public byte[] toBuffer(int format)
/*     */     throws FingerException
/*     */   {
/* 225 */     byte[] data = (byte[])null;
/* 226 */     long[] dataSize = new long[1];
/*     */ 
/* 230 */     int result = nToBuffer(this._handle, format, data, dataSize);
/*     */ 
/* 232 */     if (result == -13) {
/* 233 */       data = new byte[(int)dataSize[0]];
/*     */ 
/* 235 */       result = nToBuffer(this._handle, format, data, dataSize);
/*     */     }
/*     */ 
/* 238 */     if (result != 0) {
/* 239 */       throw new FingerException(result);
/*     */     }
/* 241 */     return data;
/*     */   }
/*     */ 
/*     */   public void add(FingerImage image)
/*     */     throws FingerException
/*     */   {
/* 253 */     int result = nAddImage(this._handle, image.getHandle());
/*     */ 
/* 255 */     if (result != 0) {
/* 256 */       throw new FingerException(result);
/*     */     }
/*     */ 
/* 259 */     this._images.add(image);
/*     */ 
/* 262 */     this._imageArray = null;
/*     */   }
/*     */ 
/*     */   public void add(FingerImageRecord imageRecord)
/*     */     throws FingerException
/*     */   {
/* 274 */     int result = nAddImageRecord(this._handle, imageRecord.getHandle());
/*     */ 
/* 276 */     if (result != 0) {
/* 277 */       throw new FingerException(result);
/*     */     }
/*     */ 
/* 280 */     for (int i = 0; i < imageRecord.getImageCount(); i++) {
/* 281 */       FingerImage image = imageRecord.getImages()[i];
/* 282 */       this._images.add(image);
/*     */     }
/*     */ 
/* 286 */     this._imageArray = null;
/*     */   }
/*     */ 
/*     */   public void remove(FingerImage image)
/*     */     throws FingerException
/*     */   {
/* 298 */     int result = nRemoveImage(this._handle, image.getHandle());
/*     */ 
/* 300 */     if (result != 0) {
/* 301 */       throw new FingerException(result);
/*     */     }
/*     */ 
/* 304 */     this._images.remove(image);
/*     */ 
/* 307 */     this._imageArray = null;
/*     */   }
/*     */ 
/*     */   public void remove(int index)
/*     */     throws FingerException
/*     */   {
/* 319 */     int result = nRemoveImageAt(this._handle, index);
/*     */ 
/* 321 */     if (result != 0) {
/* 322 */       throw new FingerException(result);
/*     */     }
/*     */ 
/* 325 */     this._images.remove(index);
/*     */ 
/* 328 */     this._imageArray = null;
/*     */   }
/*     */ 
/*     */   public boolean equals(Object obj)
/*     */   {
/* 341 */     if ((obj == null) || (obj.getClass() != getClass())) {
/* 342 */       return false;
/*     */     }
/* 344 */     FingerImageRecord record = (FingerImageRecord)obj;
/* 345 */     return this._handle == record._handle;
/*     */   }
/*     */ 
/*     */   public int hashCode()
/*     */   {
/* 356 */     return (int)this._handle;
/*     */   }
/*     */ 
/*     */   public int getFingerCount()
/*     */   {
/* 365 */     return nGetFingerCount(this._handle);
/*     */   }
/*     */ 
/*     */   public int getImageCount()
/*     */     throws FingerException
/*     */   {
/* 376 */     int[] count = new int[1];
/* 377 */     int result = nGetImageCount(this._handle, count);
/*     */ 
/* 379 */     if (result != 0) {
/* 380 */       throw new FingerException(result);
/*     */     }
/* 382 */     return count[0];
/*     */   }
/*     */ 
/*     */   public FingerImage[] getImages()
/*     */   {
/* 392 */     if (this._imageArray == null) {
/* 393 */       this._imageArray = new FingerImage[this._images.size()];
/* 394 */       this._images.toArray(this._imageArray);
/*     */     }
/*     */ 
/* 397 */     return this._imageArray;
/*     */   }
/*     */ 
/*     */   public int getCompressionAlgorithm()
/*     */     throws FingerException
/*     */   {
/* 409 */     int[] compressionAlgorithm = new int[1];
/* 410 */     float[] compressionLevel = { 0.0F };
/* 411 */     int result = nGetCompression(this._handle, compressionAlgorithm, 
/* 412 */       compressionLevel);
/*     */ 
/* 414 */     if (result != 0) {
/* 415 */       throw new FingerException(result);
/*     */     }
/* 417 */     return compressionAlgorithm[0];
/*     */   }
/*     */ 
/*     */   public float getCompressionLevel()
/*     */     throws FingerException
/*     */   {
/* 428 */     int[] compressionAlgorithm = new int[1];
/* 429 */     float[] compressionLevel = { 0.0F };
/* 430 */     int result = nGetCompression(this._handle, compressionAlgorithm, 
/* 431 */       compressionLevel);
/*     */ 
/* 433 */     if (result != 0) {
/* 434 */       throw new FingerException(result);
/*     */     }
/* 436 */     return compressionLevel[0];
/*     */   }
/*     */ 
/*     */   public void setCompression(int algorithm, float level)
/*     */     throws FingerException
/*     */   {
/* 462 */     int result = nSetCompression(this._handle, algorithm, level);
/*     */ 
/* 464 */     if (result != 0)
/* 465 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   private static final native int nInitialize(long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nDispose(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nClone(long paramLong, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nFromFile(String paramString, int paramInt, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nFromBuffer(byte[] paramArrayOfByte, int paramInt, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nToBuffer(long paramLong, int paramInt, byte[] paramArrayOfByte, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nSave(long paramLong, int paramInt, String paramString);
/*     */ 
/*     */   private static final native int nGetFingerCount(long paramLong);
/*     */ 
/*     */   private static final native int nGetImageCount(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetImage(long paramLong, int paramInt, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nAddImage(long paramLong1, long paramLong2);
/*     */ 
/*     */   private static final native int nAddImageRecord(long paramLong1, long paramLong2);
/*     */ 
/*     */   private static final native int nRemoveImage(long paramLong1, long paramLong2);
/*     */ 
/*     */   private static final native int nRemoveImageAt(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nGetCompression(long paramLong, int[] paramArrayOfInt, float[] paramArrayOfFloat);
/*     */ 
/*     */   private static final native int nSetCompression(long paramLong, int paramInt, float paramFloat);
/*     */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerImageRecord
 * JD-Core Version:    0.6.2
 */