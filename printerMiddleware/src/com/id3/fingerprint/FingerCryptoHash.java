package com.id3.fingerprint;

public class FingerCryptoHash
{
  public static final int MD5 = 0;
  public static final int SHA1 = 1;
  public static final int SHA256 = 2;
  public static final int SHA512 = 3;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCryptoHash
 * JD-Core Version:    0.6.2
 */