package com.secuera.middleware.beans.piv;

import com.secuera.middleware.biometrics.BiometricError;
import java.awt.image.BufferedImage;
import java.io.Serializable;

/*FPStatus  
 *      Complete >> get value of fingerPrint & fingerImage
 *      Success  >> image read complete try next capture   
 * 
 */
public class BiometricBean implements Serializable{
    
    private byte[] fingerPrint;
    private BufferedImage fingerImage;
    private String FPStatus;    
   
    private BiometricError bioError;

    public byte[] getFingerPrint() {
        return fingerPrint;
    }

    public void setFingerPrint(byte[] fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public BufferedImage getFingerImage() {
        return fingerImage;
    }

    public void setFingerImage(BufferedImage fingerImage) {
        this.fingerImage = fingerImage;
    }

   
    public BiometricError getBioError() {
        return bioError;
    }

    public void setBioError(BiometricError bioError) {
        this.bioError = bioError;
    }

    public String getFPStatus() {
        return FPStatus;
    }

    public void setFPStatus(String FPStatus) {
        this.FPStatus = FPStatus;
    }
   
}
