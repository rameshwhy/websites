package com.secuera.middleware.core.nxp;

import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

import com.secuera.middleware.core.common.*;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessagesNXP;
import com.secuera.middleware.core.common.SendCommand;

public class FileManager {
	
	// File Oject related core functionalities will be available here 
  	 SendCommand sendCard = new SendCommand();
	 CommonUtil cmnUtil = new CommonUtil();
     StringBuffer sb = new StringBuffer();
	
	//6a89 file already exists 
     //Create DG.EF1 on LDS/BAC
    public boolean createDGEF1File(CardChannel channel) throws MiddleWareException
    { 
        // parameters to be passed:  File type(Binary, Dedicated), File size,  File Id(EF{EF.DG1,EF.DG2} (DF){})
    	//00E000001762158002005F820101830201018602060C8801018A0105
    	//00E00000 17 62 15 create file (CLA 00 INS E0 P1-P2 00 00) 17  FCP header 62 and length 15 
    	//80 02 005F  File size 80 and length(005F) //  length is based on data we are we update
    	//82 01 01    File type 82 binary type file 01 
    	//83 02 0101  file id 83 DG.EF1 code 0101
    	//86 02 060C  Acess rules 06 0C FF FF FF FF (read : after successful BAC ; update: during personalization)
    	//88 01 08    short file id (optional)
    	//8A 01 05    life cycle status 05 activated
    	
         byte[] create_file = {0x00 , (byte)0xE0, 0x00, 0x00, 0x17};
         byte[] FCP = {0x62 , 0x15};
         byte[] file_size =  {(byte)0x80 , (byte)0x02, (byte)0x00, (byte)0x64};
         byte[] file_type =    {(byte)0x82, 0x01, 0x01};
         byte[] file_id =      {(byte)0x83, 0x02, 0x01, 0x01};
         byte[] access_rules = {(byte)0x86, 0x02, 0x06, 0x0C};
         byte[] short_file_id = {(byte)0x88 , 0x01, 0x08};
         byte[] life_cycle_status = {(byte)0x8A, 0x01, 0x05};
         
         byte[][] add_arrays = new byte[8][];
         add_arrays[0] = create_file;
         add_arrays[1] = FCP;
         add_arrays[2] = file_size;
         add_arrays[3] = file_type;
         add_arrays[4] = file_id;
         add_arrays[5] = access_rules;
         add_arrays[6] = short_file_id;
         add_arrays[7] = life_cycle_status; 

         byte[] byte_apdu = cmnUtil.combine_data(add_arrays);                                       
                                        
         CommandAPDU SELECT_APDU = new CommandAPDU(byte_apdu); 
         
         try {
 			sendCard.sendCommand(channel, SELECT_APDU, sb);
 		} catch (CardException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        if(sb.toString().equals("6a89"))
        {
        	 StringBuilder errMsg = new StringBuilder();
             errMsg.append(ExceptionMessagesNXP.FILE_ALREADY_EXISTS);
             errMsg.append(" (").append(sb.toString()).append(")");
             throw new MiddleWareException(errMsg.toString());
        }
        else
        { 
         return sb.toString().equals("9000");
        }
    }

    //File created for EF.DG2 on LDS/BAC Applet
    public boolean CreateDGEF2File(CardChannel channel) throws MiddleWareException
    {
    	//00E00000 17 62 15 create file (CLA 00 INS E0 P1-P2 00 00) 17  FCP header 62 and length 15 
    	//80 02 3F5F  File size 80 and length(005F) //  length is based on data we are we update
    	//82 01 01    File type 82 binary type file 01 
    	//83 02 0102  file id 83 DG.EF2 code 0102
    	//86 02 060C  Acess rules 06 0C FF FF FF FF (read : after successful BAC ; update: during personalization)
    	//88 01 08    short file id (optional)
    	//8A 01 05    life cycle status 05 activated
    	
    	//00E00000 17 62|15<80|02<3F5F> 82|01<01> 83|02<0102> 86|02<060C> 88|01<10> 8A|01<05>  For understanding 
    	
    	// use file id as constants
    	// what shall be access rules for files
    	// file type
    	// file size as input or shall we keep it as constants 
    	//String strHexFileCreation = "00E0000017621580023F5F820101830201028602060C8801108A0105"; 
    	
         byte[] create_file = {0x00 , (byte)0xE0, 0x00, 0x00, 0x17};
         byte[] FCP = {0x62 , 0x15};
         byte[] file_size =  {(byte)0x80 , (byte)0x02, (byte)0x3F, (byte)0x5F};
         byte[] file_type =    {(byte)0x82, 0x01, 0x01};
         byte[] file_id =      {(byte)0x83, 0x02, 0x01, 0x02};
         byte[] access_rules = {(byte)0x86, 0x02, 0x06, 0x0C};
         // Below two are optional parameters
         byte[] short_file_id = {(byte)0x88 , 0x01, 0x10};
         byte[] life_cycle_status = {(byte)0x8A, 0x01, 0x05};
         
         byte[][] add_arrays = new byte[8][];
         add_arrays[0] = create_file;
         add_arrays[1] = FCP;
         add_arrays[2] = file_size;
         add_arrays[3] = file_type;
         add_arrays[4] = file_id;
         add_arrays[5] = access_rules;
         add_arrays[6] = short_file_id;
         add_arrays[7] = life_cycle_status; 

         byte[] byte_apdu = cmnUtil.combine_data(add_arrays);
    	
          CommandAPDU SELECT_APDU = new CommandAPDU(byte_apdu);
         
         try {
 		sendCard.sendCommand(channel, SELECT_APDU, sb);
 			
 		} catch (CardException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
         if(sb.toString().equals("6A89"))
         {
         	 StringBuilder errMsg = new StringBuilder();
              errMsg.append(ExceptionMessagesNXP.FILE_ALREADY_EXISTS);
              errMsg.append(" (").append(sb.toString()).append(")");
              throw new MiddleWareException(errMsg.toString());
         }
         else
         {         
          return sb.toString().equals("9000");
         }
    }
	
    //Creates custom files provided fize size in byte[] and file id in byte[] 
    // file size or file id example {0xXX. 0xXX}
    public boolean createCustomFile(CardChannel channel, byte[] file_size, byte[] file_id) throws MiddleWareException
    {
    
	//Create EF <0101> DG01
   	//send 00E000001962178002000182010183020101860A00000000000000000000
   	/* 00E00000 19 
   	       62 17 
   	       80 02 0001 
   	       82 01 01
   	       83 02 0101
   	       86 0A 0000 0000 0000 0000 0000*/
   	   
   	    // Create EF <0101> DG01
   	   /* byte[] create_file_apdu = {0x00, (byte) 0xE0, 0x00, 0x00, 0x19};
   	    byte[] file_header_len =  {0x62, 0x17};
   	    byte[] file_size = {(byte) 0x80, 0x02, 0x00, 0x64};
   	    byte[] file_type = {(byte) 0x82, 0x01, 0x01};
   	    byte[] file_id = {(byte)0x83, 0x02, 0x01, 0x01};
   	    byte[] access_rules = {(byte) 0x86, (byte)0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};*/
   	    
   	    //Create EF <0102> DG02
   	/* byte[] create_file_apdu = {0x00, (byte) 0xE0, 0x00, 0x00, 0x15};
	    byte[] file_header_len =  {0x62, 0x13};
	    byte[] file_size = {(byte) 0x80, 0x02, 0x00, 0x64};
	    byte[] file_type = {(byte) 0x82, 0x01, 0x01};
	    byte[] file_id = {(byte)0x83, 0x02, 0x01, 0x02};
	    byte[] access_rules = {(byte) 0x86, (byte)0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};*/
   	    
    	 //Create EF <0103> DG03    	
    	byte[] create_file_apdu = {0x00, (byte) 0xE0, 0x00, 0x00, 0x19};
	    byte[] file_header_len =  {0x62, 0x17};
	    byte[] file_size_header = {(byte) 0x80, 0x02};
	    byte[] file_type = {(byte) 0x82, 0x01, 0x01};
	    byte[] file_id_header = {(byte)0x83, 0x02};
	    byte[] access_rules = {(byte) 0x86, (byte)0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	   
   	    
   	 byte[][] create_file = new byte[8][];
   	create_file[0] = create_file_apdu;
   	create_file[1] = file_header_len;
   	create_file[2] = file_size_header;
   	create_file[3] = file_size;
   	create_file[4] = file_type;
   	create_file[5] = file_id_header;
   	create_file[6] = file_id;
   	create_file[7] = access_rules;				
   	
   	byte[] file_create_APDU = cmnUtil.combine_data(create_file);
   	    
   	CommandAPDU cmd_file_create = new CommandAPDU(file_create_APDU);

   	
   	try {
   	sendCard.sendCommand(channel, cmd_file_create, sb);
   	} catch (CardException e) {
        throw new MiddleWareException(e.getMessage());
   	}
    if(sb.toString().equals("6A89"))
    {
    	 StringBuilder errMsg = new StringBuilder();
         errMsg.append(ExceptionMessagesNXP.FILE_ALREADY_EXISTS);
         errMsg.append(" (").append(sb.toString()).append(")");
         throw new MiddleWareException(errMsg.toString());
    }
    else
    {         
     return sb.toString().equals("9000");
    }
   	    
    }
    
}
