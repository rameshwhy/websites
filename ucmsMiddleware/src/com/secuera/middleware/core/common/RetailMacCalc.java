/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.common;

import com.secuera.middleware.cardreader.entities.MiddleWareException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Harpreet Singh
 */
public class RetailMacCalc {

    CommonUtil util = new CommonUtil();
    private byte[] pad = {(byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x00};
    private byte[] IV = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    Cipher ecipher,dcipher;
    String transformation = "DES/CBC/NoPadding";
    
    public RetailMacCalc() {
        super();
    }

    public byte[] retailMac(byte[] key, byte[] data) throws MiddleWareException{
       
        byte[] encdata=null;
        byte[] temp_enc = null;
        // Create Keys
        byte[] key1 = util.extract_data(key, 0,8);
        byte[] key2 = util.extract_data(key, 8,8);

        // ISO/IEC 9797-1 or ISO 7816d4
           byte[] pdata = util.combine_data(data,pad);
 
        try {
           
             SecretKey keySpecA = new SecretKeySpec(key1, "DES");
             ecipher = Cipher.getInstance(transformation);
             ecipher.init(Cipher.ENCRYPT_MODE, keySpecA , new IvParameterSpec(IV));
 
             SecretKey keySpecB = new SecretKeySpec(key2, "DES");
             dcipher = Cipher.getInstance(transformation);
             dcipher.init(Cipher.DECRYPT_MODE, keySpecB , new IvParameterSpec(IV));
              
             
             
             for (int i = 0; i < pdata.length; i = i + 8) {
                 // Encrypt 
                 if (i == 0) {
                     //(get the first 8 bytes of blocks )
                     temp_enc = ecipher.doFinal(util.extract_data(pdata, i, 8));
                 }

                 if (i > 0) {
                     byte[] next_block;
                     byte[] res = new byte[8];

                     next_block = util.extract_data(pdata, i,8);

                     for (int j = 0; j < next_block.length; j++) {
                         res[j] = (byte) (next_block[j] ^ temp_enc[j]);
                     }

                     temp_enc = ecipher.doFinal(res);

                 }

                
             }
             
             encdata = temp_enc;
             
                // Decrypt the resulting block with Key2
                encdata  = dcipher.doFinal(encdata);
                // Encrypt the resulting block with Key2
                encdata  = ecipher.doFinal(encdata);
            }   catch (Exception e) {
                    throw new MiddleWareException(e.getMessage());
            
        }
        return encdata;
    }
    
    
   
    
}

