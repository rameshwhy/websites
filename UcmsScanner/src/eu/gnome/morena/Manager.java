package eu.gnome.morena;

import eu.gnome.morena.ica.ICAManager;
import eu.gnome.morena.wia.WIAManager;
import java.awt.Component;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.commons.codec.binary.Base64;
import sun.misc.BASE64Decoder;
public abstract class Manager
  implements Version
{
  protected static String arch;
  protected static String os;
  protected static String verOS;
  protected static int wiaver = 0;
  protected static String vendor;
  protected static String userHome;
  protected static byte[] licenseText;
  protected static byte[] licenseSignature;
  private static Logger logger = null;
  private static Vector<DeviceListChangeListener> deviceListChangeListeners;
  private boolean fireCalled = false;

  private static void installLibrary(File paramFile, String paramString)
    throws Exception
  {
    InputStream localInputStream = Manager.class.getClassLoader().getResourceAsStream(paramString);
    FileOutputStream localFileOutputStream = new FileOutputStream(paramFile);
    byte[] arrayOfByte = new byte[10240];
    int i;
    while ((i = localInputStream.read(arrayOfByte)) > 0)
      localFileOutputStream.write(arrayOfByte, 0, i);
    localFileOutputStream.close();
    localInputStream.close();
  }

  public static Manager getInstance()
  {
    if (WIAManager.INSTANCE.available())
      return WIAManager.INSTANCE;
    if (ICAManager.INSTANCE.available())
      return ICAManager.INSTANCE;
    throw new Error("No Morena7 Manager implementation available");
  }

  public static void info(String paramString)
  {
    logger.log(Level.INFO, paramString);
  }

  public static void warning(String paramString)
  {
    logger.log(Level.WARNING, paramString);
  }

  public static void error(String paramString)
  {
    logger.log(Level.SEVERE, paramString);
  }

  public static void error(String paramString, Throwable paramThrowable)
  {
    logger.log(Level.SEVERE, paramString, paramThrowable);
  }

  public static void debug(String paramString)
  {
    logger.log(Level.FINE, paramString);
  }

  public static void trace(String paramString)
  {
    logger.log(Level.FINEST, paramString);
  }

  public Device selectDevice(Component paramComponent)
  {
    Vector localVector = new Vector();
    if (WIAManager.INSTANCE.available())
      localVector.addAll(WIAManager.INSTANCE.listDevices());
    if (ICAManager.INSTANCE.available())
      localVector.addAll(ICAManager.INSTANCE.listDevices());
    Device[] arrayOfDevice = (Device[])localVector.toArray(new Device[localVector.size()]);
    if (arrayOfDevice.length == 0)
      return null;
    return (Device)JOptionPane.showInputDialog(paramComponent, "Select device", "Select device", 3, null, arrayOfDevice, arrayOfDevice[0]);
  }

  public abstract List<Device> listDevices();

  public void close()
  {
  }

  public void addDeviceListChangeListener(DeviceListChangeListener paramDeviceListChangeListener)
  {
    deviceListChangeListeners.add(paramDeviceListChangeListener);
  }

  public void removeDeviceListChangeListener(DeviceListChangeListener paramDeviceListChangeListener)
  {
    deviceListChangeListeners.remove(paramDeviceListChangeListener);
  }

  protected void fireDeviceConnected(Device paramDevice)
  {
    if (!this.fireCalled)
    {
      this.fireCalled = true;
      Iterator localIterator = deviceListChangeListeners.iterator();
      while (localIterator.hasNext())
      {
        DeviceListChangeListener localDeviceListChangeListener = (DeviceListChangeListener)localIterator.next();
        localDeviceListChangeListener.deviceConnected(paramDevice);
      }
      this.fireCalled = false;
    }
  }

  protected void fireDeviceDisconnected(Device paramDevice)
  {
    if (!this.fireCalled)
    {
      this.fireCalled = true;
      Iterator localIterator = deviceListChangeListeners.iterator();
      while (localIterator.hasNext())
      {
        DeviceListChangeListener localDeviceListChangeListener = (DeviceListChangeListener)localIterator.next();
        localDeviceListChangeListener.deviceDisconnected(paramDevice);
      }
      this.fireCalled = false;
    }
  }

  public boolean available()
  {
    return (WIAManager.INSTANCE.available()) || (ICAManager.INSTANCE.available());
  }

  static
  {
    logger = Logger.getLogger("morena");
    logger.setUseParentHandlers(false);
    logger.addHandler(new LogHandler());
    logger.setLevel(Configuration.getLogLevel());
    arch = System.getProperty("os.arch").toLowerCase();
    os = System.getProperty("os.name").toLowerCase();
    verOS = System.getProperty("os.version").toLowerCase();
    String str1 = System.getProperty("wia.version", "2");
    vendor = System.getProperty("java.vm.vendor").toLowerCase();
    userHome = System.getProperty("user.home");
    String str2 = null;
    File localFile = null;
    try
    {
      if (os.indexOf("windows") >= 0)
      {
        if (arch.indexOf("64") >= 0)
        {
          if ((verOS.startsWith("5")) || (str1.equals("1")))
          {
            str2 = System.mapLibraryName("morena64W1_25");
            wiaver = 1;
          }
          else if (verOS.startsWith("6"))
          {
            str2 = System.mapLibraryName("morena64W2_25");
            wiaver = 2;
          }
          else
          {
            System.err.println("OS version " + verOS + " is not supported!");
          }
        }
        else if ((verOS.startsWith("5")) || (str1.equals("1")))
        {
          str2 = System.mapLibraryName("morena32W1_25");
          wiaver = 1;
        }
        else if (verOS.startsWith("6"))
        {
          str2 = System.mapLibraryName("morena32W2_25");
          wiaver = 2;
        }
        else
        {
          System.err.println("OS version " + verOS + " is not supported!");
        }
      }
      else
      {
        str2 = System.mapLibraryName("morena_25");
        if ((os.indexOf("mac") >= 0) && (vendor.indexOf("oracle") >= 0))
          str2 = str2.substring(0, str2.lastIndexOf(".")) + ".jnilib";
      }
      localFile = new File(TMP_DIR, str2);
      installLibrary(localFile, str2);
      System.load(localFile.getAbsolutePath());
      trace(localFile.getAbsolutePath() + " loaded...");
    }
    catch (Exception localException1)
    {
      try
      {
        trace(localFile.getAbsolutePath() + " loaded failed: " + localException1.getMessage());
        if ((!localFile.exists()) || (localFile.length() == 0L))
        {
          localFile = new File("tmp", str2);
          if ((!localFile.exists()) || (localFile.length() == 0L))
            installLibrary(localFile, str2);
        }
        System.load(localFile.getAbsolutePath());
        trace(localFile.getAbsolutePath() + " 2. loaded...");
      }
      catch (Exception localException3)
      {
        System.err.println(str2 + " not found...");
      }
    }
    try
    {
      InputStream localInputStream = Version.class.getResourceAsStream("/javatwain.license.properties");
      ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(1024);
      byte[] arrayOfByte = new byte[1024];
      int i;
      while ((i = localInputStream.read(arrayOfByte)) > 0)
        localByteArrayOutputStream.write(arrayOfByte, 0, i);
      localInputStream.close();
      localByteArrayOutputStream.flush();
      licenseText = localByteArrayOutputStream.toByteArray();
      localInputStream = Version.class.getResourceAsStream("/javatwain.license.signature");
      localByteArrayOutputStream = new ByteArrayOutputStream(1024);
      while ((i = localInputStream.read(arrayOfByte)) > 0)
        localByteArrayOutputStream.write(arrayOfByte, 0, i);
      localInputStream.close();
      localByteArrayOutputStream.flush();
      licenseSignature = new BASE64Decoder().decodeBuffer(new String(localByteArrayOutputStream.toByteArray()));
    }
    catch (Exception localException2)
    {
      throw new Error("License file not found");
    }
    deviceListChangeListeners = new Vector();
  }

  private static class LogHandler extends Handler
  {
    public void close()
      throws SecurityException
    {
    }

    public void flush()
    {
      System.err.flush();
    }

    public void publish(LogRecord paramLogRecord)
    {
      System.err.println(paramLogRecord.getMessage());
    }
  }
}
