/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.PrintedInfoBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author SEAdmin
 */
public class ManagePrintedInfo {
    
    public ManagePrintedInfo() {
        super();
    }
    
    SendCommand sendCard = new SendCommand();
    CommonUtil util = new CommonUtil();
    GeneralAuth auth = new GeneralAuth();
    ExceptionMessages expMsg = new ExceptionMessages();
    
    public PrintedInfoBean readPrintedinfo(CardChannel channel) throws MiddleWareException {
        
        String strPrintedInfo;
        int tag_loc;
        PrintedInfoBean printedInfo;
        // TO Read Printed Information Container.
        // Name 0x01 32 bytes
        // Employee Affiliation 0x02 20 bytes
        // Expiration date 0x04 9 bytes
        // Agency Card Serial Number 0X05 10 bytes
        // Issuer Identification 0X06 15 bytes
        // Organization Affiliation Line 1 0x07 20 bytes
        // Organization Affiliation Line 2 0x08 20 bytes
        // Error Detection Code 0xFE 0 bytes

        String name_tag = "01";
        String employee_tag = "02";
        String expdt_tag = "04";
        String agencyCard_tag = "05";
        String issuer_tag = "06";
        String org1_tag = "07";
        String org2_tag = "08";

        

        try {

            strPrintedInfo = getPrintedinfo(channel);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (strPrintedInfo == null || strPrintedInfo.length()== 0) {
            printedInfo = new PrintedInfoBean();
        } else {
            //System.out.println("Printed Read Result " + util.arrayToHex(res));
            //System.out.println("Final Command " + sb.toString());
            //53 81 8e 
            //01 20 
            //49442d4f4e45205049562054455354204341524420202020202020202020202002145445535420434152442020202020202020202020
            //0409323031384a414e3236050a30313233343536373839060f4f626572746875722020313233343507144f6265727468757220546563
            //682e2020202020200814476f762026204944204469766973696f6e202020fe00
            //parse printed info byte array

            printedInfo = new PrintedInfoBean();
            
            // Get Employee Name
            tag_loc = util.findtagLocation(0, strPrintedInfo, name_tag);
            String name_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
            int i = Integer.valueOf(name_length, 16).intValue();

            String str_name = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));

            // Get Employee Affiliation
            tag_loc = util.findtagLocation(0, strPrintedInfo, employee_tag);
            String employee_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
            i = Integer.valueOf(employee_length, 16).intValue();

            String str_affiliation = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));

            // Get Expiration Date
            tag_loc = util.findtagLocation(0, strPrintedInfo, expdt_tag);
            String expdt_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
            i = Integer.valueOf(expdt_length, 16).intValue();

            String str_expDate = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));

            // Get Agency Card Serial Number
            tag_loc = util.findtagLocation(0, strPrintedInfo, agencyCard_tag);
            String agencyCard_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
            i = Integer.valueOf(agencyCard_length, 16).intValue();

            String str_CardSerialNumber = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));

            // Get Issuer info
            tag_loc = util.findtagLocation(0, strPrintedInfo, issuer_tag);
            String issuer_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
            i = Integer.valueOf(issuer_length, 16).intValue();

            String str_IssuerID = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));

            // Get Organization1
            tag_loc = util.findtagLocation(0, strPrintedInfo, org1_tag);
            String org1_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
            i = Integer.valueOf(org1_length, 16).intValue();

            String str_org1 = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));

            // Get Organization2
            tag_loc = util.findtagLocation(0, strPrintedInfo, org2_tag);
            String org2_length = strPrintedInfo.substring(tag_loc + 2, tag_loc + 4);
            i = Integer.valueOf(org2_length, 16).intValue();

            String str_org2 = util.hexToString(strPrintedInfo.substring(tag_loc + 4, tag_loc + 4 + i * 2));

            printedInfo.setName(str_name);
            printedInfo.setAffiliation(str_affiliation);
            printedInfo.setExpirationDate(str_expDate);
            printedInfo.setCardSerialNumber(str_CardSerialNumber);
            printedInfo.setIssuerID(str_IssuerID);
            printedInfo.setOrganisationLine1(str_org1);
            printedInfo.setOrganisationLine2(str_org2);
        }
        return printedInfo;

    }

    
     public String getPrintedinfo(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        StringBuffer sb = new StringBuffer();
        String strPrintedInfo;

        byte[] apdu_printed = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05,
            0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x09}; // BERT-TLV

        CommandAPDU READ_APDU = new CommandAPDU(apdu_printed);

        System.out.println("Final Command " + util.arrayToHex(apdu_printed));

        System.out.println("---------");

        try {

            res = sendCard.sendCommand(channel, READ_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        
         if (res == null || res.length == 0) {
             strPrintedInfo = "";

         } else {
             //convert byte array to string & remove parity bits
             strPrintedInfo = util.arrayToHex(res);

             //remove leading characters
             if (strPrintedInfo.substring(2, 4).equals("82")) {
                 strPrintedInfo = strPrintedInfo.substring(8);
             } else if (strPrintedInfo.substring(2, 4).equals("81")) {
                 strPrintedInfo = strPrintedInfo.substring(6);
             } else {
                 strPrintedInfo = strPrintedInfo.substring(4);
             }
         }
        return strPrintedInfo;

    }
    
    
    public boolean writePrintedinfo(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY, String str_name,
            String str_emp, String str_date, String str_ACSN, String str_Iden,
            String str_OA1, String str_OA2) throws MiddleWareException {

        // TO write Printed Information Container.
        // Name 0x01 32 bytes
        // Employee Affiliation 0x02 20 bytes
        // Expiration date 0x04 9 bytes
        // Agency Card Serial Number 0X05 10 bytes
        // Issuer Identification 0X06 15 bytes
        // Organization Affiliation Line 1 0x07 20 bytes
        // Organization Affiliation Line 2 0x08 20 bytes
        // Error Detection Code 0xFE 0 bytes

        byte[] newData;
        byte[] command_data;
        StringBuffer sb = new StringBuffer();
        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        // Now convert to Hex.
        String hex_name = util.stringToHex(str_name);
        String hex_emp = util.stringToHex(str_emp);
        String hex_date = util.stringToHex(str_date);
        String hex_ACSN = util.stringToHex(str_ACSN);
        String hex_Iden = util.stringToHex(str_Iden);
        String hex_OA1 = util.stringToHex(str_OA1);
        String hex_OA2 = util.stringToHex(str_OA2);

        hex_name = "0120" + util.formatHexString(64, hex_name);
        hex_emp = "0214" + util.formatHexString(40, hex_emp);
        hex_date = "0409" + util.formatHexString(18, hex_date);
        hex_ACSN = "050A" + util.formatHexString(20, hex_ACSN);
        hex_Iden = "060F" + util.formatHexString(30, hex_Iden);
        hex_OA1 = "0714" + util.formatHexString(40, hex_OA1);
        hex_OA2 = "0814" + util.formatHexString(40, hex_OA2);
        String hex_error = "FE00";

        String hex_data = hex_name + hex_emp + hex_date + hex_ACSN + hex_Iden
                + hex_OA1 + hex_OA2 + hex_error;

        newData = util.hexToByteArray(hex_data);

        byte[] apdu_printed = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF,
            (byte) 0x96, 0x5C, 0x03, 0x5F, (byte) 0xC1, 0x09, 0x53,
            (byte) 0x81, (byte) 0x8E}; // BERT-TLV

        command_data = util.combine_data(apdu_printed, newData);

        System.out.println("Final Command " + util.arrayToHex(command_data));

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        System.out.println("Final Result " + sb.toString());
        return true;
    }
}
