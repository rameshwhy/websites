package com.id3.fingerprint;

public class FingerImpression
{
  public static final int LiveScanPlain = 0;
  public static final int LiveScanRolled = 1;
  public static final int NonliveScanPlain = 2;
  public static final int NonliveScanRolled = 3;
  public static final int LiveScanSwipe = 8;
  public static final int VerticalRoll = 9;
  public static final int LiveScanOpticalContactlessPlain = 24;
  public static final int Other = 28;
  public static final int Unknown = 29;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerImpression
 * JD-Core Version:    0.6.2
 */