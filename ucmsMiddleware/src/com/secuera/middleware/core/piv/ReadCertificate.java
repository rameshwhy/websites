package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.beans.piv.CertificateBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;


import java.util.Iterator;
import java.util.List;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;


import sun.security.x509.X509CertImpl;

//import com.secuera.entities.MiddleWareException;s
public class ReadCertificate {

    SendCommand sendCard = new SendCommand();
    CommonUtil util = new CommonUtil();
    X509CertImpl cert = null;

    public ReadCertificate() {
        super();
    }
    
    public CertificateBean readCert(CardChannel channel, String cert_type) throws MiddleWareException{

        byte[] res = null;
        byte[] cert_id = null;
        byte[] cert_tag = {0x70};
        byte[] encoded = null;
        StringBuffer sb = new StringBuffer();
        CertificateBean certBean = new CertificateBean();
        byte[] apdu_cert = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0X00, 0X00, 0x05};


        byte[] piv_auth = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x05};
        byte[] card_digi = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x0A};
        byte[] key_manage = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x0B};
        byte[] card_auth = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x01};

        if (cert_type.equalsIgnoreCase(UcmsMiddlewareConstants.pivAuthentication9A)) {
            cert_id = piv_auth;
        } else if (cert_type.equalsIgnoreCase(UcmsMiddlewareConstants.digSignature9C)) {
            cert_id = card_digi;
        } else if (cert_type.equalsIgnoreCase(UcmsMiddlewareConstants.keyManagement9D)) {
            cert_id = key_manage;
        } else if (cert_type.equalsIgnoreCase(UcmsMiddlewareConstants.cardAuthentication9E)) {
            cert_id = card_auth;    //'A'
        }


        

        apdu_cert = util.combine_data(apdu_cert, cert_id);
        CommandAPDU CERT_APDU = new CommandAPDU(apdu_cert);

        try {
            res = sendCard.sendCommand(channel, CERT_APDU, sb);


        } catch (CardException ex) {
            throw new MiddleWareException(ex.getMessage());
            //Logger.getLogger(ReadCertificate.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(util.arrayToHex(res));
        if (res.length > 0) {

            /*Now Extract Certificate data out of response from the card */
            byte[] res_cert_tag = util.extract_data(res, 4, 1);

            if (Arrays.equals(res_cert_tag, cert_tag)) {
                int cert_length = util.byteToInt(util.extract_data(res, 6, 2));

                encoded = util.extract_data(res, 8, cert_length);

                certBean = parseCert(encoded);


            } else {
                //error. throww exception >> .
            }
        }

        return certBean;

    }

    public CertificateBean parseCert(byte[] encoded) throws MiddleWareException {

        try {
            cert = new X509CertImpl(encoded);
        } catch (CertificateException ex) {
            // TODO Auto-generated catch block
            throw new MiddleWareException(ex.getMessage());
        }

      

        String[] keyUsageType = {
            UcmsMiddlewareConstants.digitalSignature,
            UcmsMiddlewareConstants.nonRepudiation,
            UcmsMiddlewareConstants.keyEncipherment,
            UcmsMiddlewareConstants.dataEncipherment,
            UcmsMiddlewareConstants.keyAgreement,
            UcmsMiddlewareConstants.keyCertSign,
            UcmsMiddlewareConstants.cRLSign,
            UcmsMiddlewareConstants.encipherOnly,
            UcmsMiddlewareConstants.decipherOnly,};

        StringBuilder keyUsage = new StringBuilder();

        boolean[] keyUsageVal;
        CertificateBean certBean = new CertificateBean();
        keyUsageVal = cert.getKeyUsage();
        if (keyUsageVal != null) {
            for (int i = 0; i < keyUsageVal.length; i++) {
                if (keyUsageVal[i]) {
                    if (i == 0) {
                        keyUsage.append(keyUsageType[i]);
                    } else {
                        keyUsage.append(",");
                        keyUsage.append(keyUsageType[i]);
                    }


                }

            }
            certBean.setVersion('V' + Integer.toString(cert.getVersion()));
            certBean.setSerialNumber(cert.getSerialNumber().toString());
            certBean.setSignatureAlgorithm(cert.getSigAlgName());
            certBean.setIssuer(cert.getIssuerDN().toString());
            certBean.setValidFrom(cert.getNotBefore().toString());
            certBean.setValidTo(cert.getNotAfter().toString());




            Collection subjectNames = null;
            try {
                subjectNames = cert.getSubjectAlternativeNames();
            } catch (CertificateParsingException ex) {
                throw new MiddleWareException(ex.getMessage());
            }


           

            certBean.setSubject(cert.getSubjectDN().toString());
            certBean.setPublicKey(cert.getPublicKey().toString());
            certBean.setKeyUsage(keyUsage.toString());

            System.out.println(cert.getElements().toString());


            // List<String> ExtendedKeyUsage = new String ;
            StringBuilder ExtnkeyUsage = new StringBuilder();
            String retValue = "";
            /*
            public static String AnyPurpose  = "Any Purpose (2.5.29.37.0)";
            public static String ClientAuthentication  = "Client Authentication (1.3.6.1.5.5.7.3.2)";
            public static String SecureEmail  = "Secure Email (1.3.6.1.5.5.7.3.4)";
            public static String SmartCardLogon  = "Smart Card Logon (1.3.6.1.4.1.311.20.2.2)";
            MiddlewareConstants
             */

            try {
                List<String> ExtndedkeyUsage = cert.getExtendedKeyUsage();

                if(ExtndedkeyUsage!=null)
                {
                for (int i = 0; i < ExtndedkeyUsage.size(); i++) {

                    if (ExtndedkeyUsage.get(i).equalsIgnoreCase("2.16.840.1.101.3.6.7")){
                        retValue = UcmsMiddlewareConstants.PIVContentSigning;
                    }else if(ExtndedkeyUsage.get(i).equalsIgnoreCase("2.5.29.37.0")){
                        retValue = UcmsMiddlewareConstants.AnyPurpose;
                    }else if(ExtndedkeyUsage.get(i).equalsIgnoreCase("1.3.6.1.5.5.7.3.2")){
                        retValue = UcmsMiddlewareConstants.ClientAuthentication;
                    }else if(ExtndedkeyUsage.get(i).equalsIgnoreCase("1.3.6.1.5.5.7.3.4")){
                        retValue = UcmsMiddlewareConstants.SecureEmail;
                    }else if(ExtndedkeyUsage.get(i).equalsIgnoreCase("1.3.6.1.4.1.311.20.2.2")){
                        retValue = UcmsMiddlewareConstants.SmartCardLogon;
                    }
                    /*
                    switch (ExtndedkeyUsage.get(i)) {
                        case "2.16.840.1.101.3.6.7":
                            retValue = UcmsMiddlewareConstants.PIVContentSigning;
                            break;
                        case "2.5.29.37.0":
                            retValue = UcmsMiddlewareConstants.AnyPurpose;
                            break;
                        case "1.3.6.1.5.5.7.3.2":
                            retValue = UcmsMiddlewareConstants.ClientAuthentication;
                            break;

                        case "1.3.6.1.5.5.7.3.4":
                            retValue = UcmsMiddlewareConstants.SecureEmail;
                            break;
                        case "1.3.6.1.4.1.311.20.2.2":
                            retValue = UcmsMiddlewareConstants.SmartCardLogon;
                            break;
                    }*/

                    if (i == 0) {


                        ExtnkeyUsage.append(retValue);
                    } else {
                        keyUsage.append(",");
                        ExtnkeyUsage.append(retValue);
                    }

                }

                }

            } catch (CertificateParsingException ex) {
                throw new MiddleWareException(ex.getMessage());
            }
            certBean.setExtnkeyUsage(ExtnkeyUsage.toString());




        }


        //"CN=myid-WIN-H0E2CE40DP0-CA, DC=myid, DC=com"

        String issuerDN = cert.getIssuerDN().toString();
        certBean.setIssuerDn(issuerDN);
        String issuerarr[] = issuerDN.split(",");
        String issuerInfo[];


        certBean.setIssuerCommonName("");
        certBean.setIssuerOrganizationUnit("");
        certBean.setIssuerOrganizationName("");
        certBean.setIssuerCountry("");



        for (int i = 0; i < issuerarr.length; i++) {
            issuerInfo = issuerarr[i].split("=");

            if (issuerInfo[0].trim().equalsIgnoreCase("CN")) {
                certBean.setIssuerCommonName(issuerInfo[1]);
            } else if (issuerInfo[0].trim().equalsIgnoreCase("OU")) {
                certBean.setIssuerOrganizationUnit(issuerInfo[1]);
            } else if (issuerInfo[0].trim().equalsIgnoreCase("O")) {
                certBean.setIssuerOrganizationName(issuerInfo[1]);
            } else if (issuerInfo[0].trim().equalsIgnoreCase("C")) {
                certBean.setIssuerCountry(issuerInfo[1]);
            }

        }

        certBean.setBGserialNumber(cert.getSerialNumber());


        certBean.setSubjectCommonName("");
        certBean.setSubjectOrganizationUnit("");
        certBean.setSubjectOrganizationName("");
        certBean.setSubjectCity("");
        certBean.setSubjectState("");
        certBean.setSubjectCountry("");




        String subjectDN = cert.getSubjectDN().toString();
        certBean.setSubjectDn(subjectDN);
        String subjectarr[] = subjectDN.split(",");
        String subjectInfo[];

        for (int i = 0; i < subjectarr.length; i++) {
            subjectInfo = subjectarr[i].split("=");

            if (subjectInfo[0].trim().equalsIgnoreCase("CN")) {
                certBean.setSubjectCommonName(subjectInfo[1]);
            } else if (subjectInfo[0].trim().equalsIgnoreCase("OU")) {
                certBean.setSubjectOrganizationUnit(subjectInfo[1]);
            } else if (subjectInfo[0].trim().equalsIgnoreCase("O")) {
                certBean.setSubjectOrganizationName(subjectInfo[1]);
            } else if (subjectInfo[0].trim().equalsIgnoreCase("L")) {
                certBean.setSubjectCity(subjectInfo[1]);
            } else if (subjectInfo[0].trim().equalsIgnoreCase("ST")) {
                certBean.setSubjectState(subjectInfo[1]);
            } else if (subjectInfo[0].trim().equalsIgnoreCase("C")) {
                certBean.setSubjectCountry(subjectInfo[1]);
            }
        }


        certBean.setSubjectEmail(getSubjectAltName());



        return certBean;
    }

    public String getSubjectAltName() {
        int i_startprincipal, i_startcontent;
        String any_tag = "a0";
        String str_content_len, str_content;
        int contentLen;
        String email = "";
         String str_subjectAlternateName="";
        try {


            Iterator iter = cert.getSubjectAlternativeNames().iterator();
            while (iter.hasNext()) {
                List next = (List) iter.next();
                Object obj = next.get(1);
                if(obj instanceof byte[]){
                    byte[] subjectAlternateName = (byte[]) obj;
                    str_subjectAlternateName = util.arrayToHex(subjectAlternateName);
                    
                    i_startprincipal = str_subjectAlternateName.toLowerCase().lastIndexOf(UcmsMiddlewareConstants.principalName.toLowerCase());
                    if (i_startprincipal > 0) {
                        i_startcontent = str_subjectAlternateName.toLowerCase().lastIndexOf(any_tag);
                        str_subjectAlternateName = str_subjectAlternateName.substring(i_startcontent + 2);

                        str_content_len = str_subjectAlternateName.substring(0, 2);


                        contentLen = Integer.valueOf(str_content_len, 16).intValue();
                        str_content = str_subjectAlternateName.substring(2, 2 + (contentLen * 2));
                        email = util.hexToString(str_content).trim();
                    }
                
                }else if(obj instanceof String){
                    email = (String) obj;
                }
                


                
            }

        } catch (CertificateParsingException e) {
            email = "";
        }

        return email;
    }
}
