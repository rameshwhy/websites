/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.nxp;

import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessagesNXP;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.SendCommand;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class ManageApplet {
    
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    ExceptionMessagesNXP expMsg = new ExceptionMessagesNXP();
    
    public ManageApplet() {
        super();
    }
    
    public void selectApplet(CardChannel channel, byte[] CardManagerAid ) throws MiddleWareException
    {
        StringBuffer sb = new StringBuffer();
         
        byte[] apdu_applet = {0x00, (byte) 0xA4, 0x04, 0x00};
        byte[] num_bytes = util.calcBertValue(CardManagerAid.length, false, true);
        byte[] Le ={0x00};
                
        // Now Calculate the total bytes for the structure.
        byte[][] applet_arrays = new byte[4][];
        applet_arrays[0] = apdu_applet;
        applet_arrays[1] = num_bytes;
        applet_arrays[2] = CardManagerAid;
        applet_arrays[3] = Le;
     
        byte[] apdu_select_applet = util.combine_data(applet_arrays);
        
      //  util.arrayToHex(apdu_select_applet)
        CommandAPDU SELECT_APDU = new CommandAPDU(apdu_select_applet);

        try {
           sendCard.sendCommand(channel, SELECT_APDU, sb);
            
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        
        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessagesNXP.APPLET_SELECTION_ERROR);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }

       
    }
}
