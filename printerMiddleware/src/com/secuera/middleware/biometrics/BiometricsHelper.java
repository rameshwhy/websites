/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.biometrics;

import com.secuera.middleware.cardreader.CardReaderError;
import com.secuera.middleware.cardreader.CardReaderResponseBean;
import com.secuera.middleware.cardreader.core.CardComm;
import com.secuera.middleware.cardreader.core.MiddleWareServerCardreader;
import com.secuera.middleware.cardreader.core.MiddleWareServerCardreaderImpl;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.List;
import javax.smartcardio.CardChannel;

/**
 *
 * @author Nilay Singh
 */
public class BiometricsHelper {

    static CardComm CardConnect = null;
    List<String> terminals = null;
    String cardConnectStatus;
    CardReaderError cardError = null;
    CardReaderResponseBean cardResponse = null; 
    
    //finger print functions
    public CardReaderResponseBean enrollFingerPrint(String deviceName, byte[] fingerPrint, String fingerID, String piv_admin_key) {
        cardError = new CardReaderError();
        cardResponse = new CardReaderResponseBean();
        CardChannel channel = null;
        boolean returnStatus = false;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardReaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = mws.enrollFingerPrint(channel, fingerPrint, fingerID, piv_admin_key);

            //set return status
            cardResponse.setCardReaderResponse(returnStatus);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardReaderError(cardError);
        } finally {
            // stop communication & return success
            CardConnect.stopComm();
        }

        return cardResponse;

    }

    public CardReaderResponseBean verifyFingerPrint(String deviceName, byte[] fingerPrint) {
        cardError = new CardReaderError();
        cardResponse = new CardReaderResponseBean();
        CardChannel channel = null;
        boolean returnStatus = false;
        try {
            // Start communication with card & return error Message is any 
            channel = startCardCommunication(deviceName);
            if (channel == null) {
                cardError.setErrorCode("-1");
                cardError.setErrorMsg("Error Communicating with Card, Check Terminal Connection.");
                cardResponse.setCardReaderError(cardError);
                return cardResponse;
            }

            // Check if the PIN is Valid
            MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            returnStatus = mws.verifyFingerPrint(channel, fingerPrint);

            //set return status
            cardResponse.setCardReaderResponse(returnStatus);
        } catch (Exception e) {
            cardError.setErrorCode("-1");
            cardError.setErrorMsg(e.getMessage());
            cardResponse.setCardReaderError(cardError);
        } finally {
            // stop communication & return success
            CardConnect.stopComm();
        }

        return cardResponse;

    }
    
     
    public CardChannel startCardCommunication(String deviceName) {

        CardChannel retValue = null;
        final String dvcName = deviceName.trim();
        CardChannel cardChannel = null;
        // ---------------------------------------------------------- //
        // Start communication with card
        // ---------------------------------------------------------- //

        try {
            retValue = (CardChannel) AccessController.doPrivileged(new PrivilegedAction() {

                public Object run() {
                    CardChannel cardChannel;
                    try {
                        CardConnect = new CardComm();
                        //CardApplet.bouncyCastleProvider = new BouncyCastleProvider();

                        cardChannel = CardConnect.startComm(dvcName);

                    } catch (Exception e) {
                        cardChannel = null;
                    }
                    return cardChannel; // nothing to return
                }
            });

        } catch (Exception e) {
            //retValue = "Unable to load jnisgfplib";
        }

        return retValue; // nothing to return
    }

}
