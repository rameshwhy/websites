/*     */ package com.id3.fingerprint;
/*     */ 
/*     */ public class FingerMatchDetails
/*     */ {
/*  15 */   private boolean _disposed = false;
/*     */   private long _handle;
/*     */ 
/*     */   public FingerMatchDetails(long handle)
/*     */   {
/*  23 */     this._handle = handle;
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/*  31 */     nDispose(this._handle);
/*  32 */     this._handle = 0L;
/*  33 */     this._disposed = true;
/*     */   }
/*     */ 
/*     */   protected void finalize()
/*     */     throws Throwable
/*     */   {
/*     */     try
/*     */     {
/*  43 */       dispose();
/*     */     }
/*     */     catch (Exception localException)
/*     */     {
/*     */     }
/*     */     finally
/*     */     {
/*  50 */       super.finalize();
/*     */     }
/*     */   }
/*     */ 
/*     */   public long getHandle()
/*     */   {
/*  59 */     return this._handle;
/*     */   }
/*     */ 
/*     */   public int getIndexInReference()
/*     */     throws FingerException
/*     */   {
/*  68 */     int[] value = new int[1];
/*  69 */     int result = nGetIndexInReference(this._handle, value);
/*     */ 
/*  71 */     if (result != 0) {
/*  72 */       throw new FingerException(result);
/*     */     }
/*  74 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getIndexInProbe()
/*     */     throws FingerException
/*     */   {
/*  82 */     int[] value = new int[1];
/*  83 */     int result = nGetIndexInProbe(this._handle, value);
/*     */ 
/*  85 */     if (result != 0) {
/*  86 */       throw new FingerException(result);
/*     */     }
/*  88 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getScore()
/*     */     throws FingerException
/*     */   {
/*  98 */     int[] value = new int[1];
/*  99 */     int result = nGetScore(this._handle, value);
/*     */ 
/* 101 */     if (result != 0) {
/* 102 */       throw new FingerException(result);
/*     */     }
/* 104 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getSimilarityRate()
/*     */     throws FingerException
/*     */   {
/* 115 */     int[] value = new int[1];
/* 116 */     int result = nGetSimilarityRate(this._handle, value);
/*     */ 
/* 118 */     if (result != 0) {
/* 119 */       throw new FingerException(result);
/*     */     }
/* 121 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getDecision()
/*     */     throws FingerException
/*     */   {
/* 131 */     int[] value = new int[1];
/* 132 */     int result = nGetDecision(this._handle, value);
/*     */ 
/* 134 */     if (result != 0) {
/* 135 */       throw new FingerException(result);
/*     */     }
/* 137 */     return value[0];
/*     */   }
/*     */ 
/*     */   private static final native int nDispose(long paramLong);
/*     */ 
/*     */   private static final native int nGetIndexInProbe(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetIndexInReference(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetScore(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetSimilarityRate(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetDecision(long paramLong, int[] paramArrayOfInt);
/*     */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerMatchDetails
 * JD-Core Version:    0.6.2
 */