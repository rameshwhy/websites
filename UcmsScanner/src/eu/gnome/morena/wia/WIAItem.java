// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.wia;

import java.util.*;

// Referenced classes of package eu.gnome.morena.wia:
//            SupportedPropertyValues, WIAConstants

class WIAItem
    implements WIAConstants
{

    public WIAItem(String s, int i)
    {
        name = s;
        category = i;
        propValues = new HashMap();
        propList = new ArrayList();
    }

    public void addProperty(int i, String s, boolean flag, int j, int k, int ai[])
    {
        SupportedPropertyValues supportedpropertyvalues = new SupportedPropertyValues(i, s, flag, j, k, ai);
        propList.add(Integer.valueOf(i));
        propValues.put(Integer.valueOf(i), supportedpropertyvalues);
    }

    public boolean isFeeder()
    {
        return category == 3 || category == 8 || category == 7;
    }

    public boolean isFlatbed()
    {
        return category == 2;
    }

    public List getSupportedModes()
    {
        ArrayList arraylist = new ArrayList();
        List list = ((SupportedPropertyValues)propValues.get(Integer.valueOf(4103))).getList();
        if(list.size() > 0)
        {
            if(list.contains(Integer.valueOf(0)))
                arraylist.add(Integer.valueOf(-1));
            if(list.contains(Integer.valueOf(2)))
                arraylist.add(Integer.valueOf(-8));
            if(list.contains(Integer.valueOf(3)))
                arraylist.add(Integer.valueOf(8));
        } else
        {
            List list1 = ((SupportedPropertyValues)propValues.get(Integer.valueOf(4104))).getList();
            if(list1.contains(Integer.valueOf(1)))
                arraylist.add(Integer.valueOf(-1));
            if(list1.contains(Integer.valueOf(8)))
                arraylist.add(Integer.valueOf(-8));
            if(list1.contains(Integer.valueOf(24)))
                arraylist.add(Integer.valueOf(8));
        }
        return arraylist;
    }

    public List getSupportedValues(int i)
    {
        SupportedPropertyValues supportedpropertyvalues = (SupportedPropertyValues)propValues.get(Integer.valueOf(i));
        if(supportedpropertyvalues != null)
            return supportedpropertyvalues.getList();
        else
            return new ArrayList();
    }

    public SupportedPropertyValues getProperty(int i)
    {
        return (SupportedPropertyValues)propValues.get(Integer.valueOf(i));
    }

    public List getPropertyIds()
    {
        return propList;
    }

    public String toString()
    {
        StringBuilder stringbuilder = new StringBuilder(64);
        stringbuilder.append("Item ").append(name).append("    of category ").append(getCategoryDescription(category));
        return stringbuilder.toString();
    }

    private String getCategoryDescription(int i)
    {
        switch(i)
        {
        case 1: // '\001'
            return "Finished file";

        case 2: // '\002'
            return "Flatbed";

        case 3: // '\003'
            return "Feeder";

        case 4: // '\004'
            return "Film";

        case 5: // '\005'
            return "Root";

        case 6: // '\006'
            return "Folder";

        case 7: // '\007'
            return "Feeder front";

        case 8: // '\b'
            return "Feeder back";

        case 9: // '\t'
            return "Auto";
        }
        return "Other";
    }

    private String name;
    private int category;
    private Map propValues;
    private List propList;
}
