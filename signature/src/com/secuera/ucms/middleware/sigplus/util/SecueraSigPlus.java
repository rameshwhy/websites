package com.secuera.ucms.middleware.sigplus.util;

import com.topaz.sigplus.SigPlusEvent0;
import com.topaz.sigplus.SigPlusListener;
import com.topaz.sigplus.TopazSigCapData;
import com.topaz.sigplus.util.LCDGraphicsSupport;
import com.topaz.sigplus.util.SigCrypt;
import com.topaz.sigplus.util.SigDataEventHandler;
import com.topaz.sigplus.util.SigDataPoint;
import com.topaz.sigplus.util.SigDataType;
import com.topaz.sigplus.util.SigDrawParameters;
import com.topaz.sigplus.util.SigDrawType;
import com.topaz.sigplus.util.SigInfo;
import com.topaz.sigplus.util.SigReader;
import com.topaz.sigplus.util.SigWriter;
import com.topaz.sigplus.util.TabletInterface;
import com.topaz.sigplus.util.TabletParameters;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Vector;

public class SecueraSigPlus extends Component
  implements SigDataEventHandler
{
  private transient SigDrawType sigDraw;
  private transient SigDataType sig;
  private transient TabletInterface tablet;
  private volatile int tabletState;
  private TabletParameters tabParams;
  private SigDrawParameters displayParams;
  private SigDrawParameters imageParams;
  private boolean tabletInvisible;
  private Vector m_SigPlusListeners = new Vector();
  private transient int compressionMode;
  private transient SigCrypt cryptor;
  static final String SigPlusVersion = "SigPlusJava v2.60";
  private LCDGraphicsSupport lcdGraphics;
  private int lcdPixelDepth;

  public SecueraSigPlus()
  {
    this.tabParams = new TabletParameters();
    this.displayParams = new SigDrawParameters();
    this.imageParams = new SigDrawParameters();
    this.cryptor = new SigCrypt();

    initSigPlus();
  }

  void initSigPlus()
  {
    this.tabletInvisible = false;

    this.sig = new SigDataType();
    this.sig.setNewDataHandler(this);

    this.sigDraw = new SigDrawType(this.sig, this.tabParams);
    this.tablet = new TabletInterface(this.tabParams, this.sig);
    this.lcdGraphics = new LCDGraphicsSupport(this.tablet);

    this.tabletState = 0;
    this.compressionMode = 0;
    this.lcdPixelDepth = 0;
  }

  public void fireNewData()
  {
    if (this.displayParams.getXSize() == 1)
    {
      this.displayParams.setXSize(getSize().width);
      this.displayParams.setYSize(getSize().height);
    }
    Graphics g = getGraphics();
    if (g != null)
    {
      this.sigDraw.paint(g, this.displayParams, this.tabletState);
    }
  }

  public void fireNewKeyPadData()
  {
    fireKeyPadData();
  }

  public boolean isOpaque()
  {
    return true;
  }

  public void paint(Graphics g)
  {
    if (!this.tabletInvisible)
    {
      this.displayParams.setXSize(getSize().width);
      this.displayParams.setYSize(getSize().height);
      this.sigDraw.update(g, this.displayParams, this.tabletState);
    }
  }

  public boolean exportSigFile(String fileName)
  {
    SigWriter writer = new SigWriter(this.compressionMode, this.cryptor);

    boolean status = writer.writeSigToFile(fileName, this.sig, this.displayParams, this.imageParams);
    return status;
  }

  public boolean importSigFile(String fileName)
  {
    SigReader reader = new SigReader(this.compressionMode, this.cryptor);

    boolean status = reader.readSigFromFile(fileName, this.sig, this.displayParams, this.imageParams);
    repaint();
    return status;
  }

  public void clearTablet()
  {
    this.sig.clearSig();
    this.tablet.badPointCounter = 0;
    repaint();
  }

  public boolean getTabletInvisible()
  {
    return this.tabletInvisible;
  }

  public void setTabletInvisible(boolean newValue)
  {
    this.tabletInvisible = newValue;
  }

  public void keyPadSetSigWindow(int coords, int xPos, int yPos, int xSize, int ySize)
  {
    this.sig.setSigWindow(this.tabParams, coords, xPos, yPos, xSize, ySize);
  }

  public void keyPadClearSigWindow(int inside)
  {
    this.sig.clearSigWindow(inside);
  }

  public void keyPadAddHotSpot(int keyCode, int coords, int xPos, int yPos, int xSize, int ySize)
  {
    this.sig.addHotSpot(this.tabParams, keyCode, coords, xPos, yPos, xSize, ySize);
  }

  public void keyPadClearHotSpotList()
  {
    this.sig.clearHotSpotList();
  }

  public int keyPadQueryHotSpot(int keyCode)
  {
    return this.sig.queryHotSpot(keyCode);
  }

  public int getLCDCaptureMode()
  {
    return this.tabParams.getTabletLCDCaptureMode();
  }

  public void setLCDCaptureMode(int newMode)
  {
    this.tabParams.setTabletLCDCaptureMode(newMode);
    if (this.tabletState != 0)
    {
      this.tablet.lcdTabletSetup();
    }
  }

  public boolean lcdSetWindow(int xPos, int yPos, int xSize, int ySize)
  {
    return this.tablet.lcdSetWindow(xPos, yPos, xSize, ySize);
  }

  public boolean lcdWriteImage(int dest, int mode, int xPos, int yPos, int xSize, int ySize, Image imageData)
  {
    BufferedImage sigImage = new BufferedImage(imageData.getWidth(this), imageData.getHeight(this), 1);
    sigImage.getGraphics().drawImage(imageData, 0, 0, this);
    return this.lcdGraphics.lcdWriteImage(dest, mode, xPos, yPos, xSize, ySize, sigImage);
  }

  public boolean lcdRefresh(int mode, int xPos, int yPos, int xSize, int ySize)
  {
    return this.tablet.lcdRefresh(mode, xPos, yPos, xSize, ySize);
  }

  public String getTimeStamp()
  {
    return this.sig.getTimeStamp();
  }

  public void setTimeStamp(String newValue)
  {
    if (!this.cryptor.changeDataAllowed(this.sig))
    {
      return;
    }
    this.sig.setTimeStamp(newValue);
  }

  public String getAnnotation()
  {
    return this.sig.getAnnotation();
  }

  public void setAnnotation(String newValue)
  {
    if (!this.cryptor.changeDataAllowed(this.sig))
    {
      return;
    }
    this.sig.setAnnotation(newValue);
  }

  public String getSigString()
  {
    SigWriter writer = new SigWriter(this.compressionMode, this.cryptor);
    byte[] buffer = writer.writeSigToByteArray(this.sig, this.displayParams, 
      this.imageParams);

    String str = convertByteArrayToHexString(buffer);
    return str;
  }

  public void setSigString(String sigString)
  {
    SigReader reader = new SigReader(this.compressionMode, this.cryptor);
    byte[] buffer = convertHexStringToByteArray(sigString);

    reader.readSigFromByteArray(buffer, this.sig, this.displayParams, this.imageParams);
    repaint();
  }

  public int getTabletState()
  {
    return this.tabletState;
  }

  public void setTabletState(int newValue)
  {
    if ((newValue == 1) && (this.tabletState == 0))
    {
      if (!this.cryptor.changeDataAllowed(this.sig))
      {
        return;
      }
      if (this.tablet.openTablet())
      {
        repaint();
        this.tabletState = 1;
        this.tablet.lcdTabletSetup();
      }

    }
    else if ((newValue == 0) && (this.tabletState == 1))
    {
      this.tablet.closeTablet();

      repaint();
      this.tabletState = 0;
    }

    try
    {
      Thread.sleep(10L);
    }
    catch (InterruptedException localInterruptedException)
    {
    }
  }

  public int getTabletLogicalXSize()
  {
    return this.tabParams.getTabletLogicalXSize();
  }

  public int getTabletLogicalYSize()
  {
    return this.tabParams.getTabletLogicalYSize();
  }

  public int getTabletXStart()
  {
    return this.tabParams.getTabletXStart();
  }

  public void setTabletXStart(int newValue)
  {
    this.tabParams.setTabletXStart(newValue);
  }

  public int getTabletXStop()
  {
    return this.tabParams.getTabletXStop();
  }

  public void setTabletXStop(int newValue)
  {
    this.tabParams.setTabletXStop(newValue);
  }

  public int getTabletYStart()
  {
    return this.tabParams.getTabletYStart();
  }

  public void setTabletYStart(int newValue)
  {
    this.tabParams.setTabletYStart(newValue);
  }

  public int getTabletYStop()
  {
    return this.tabParams.getTabletYStop();
  }

  public void setTabletYStop(int newValue)
  {
    this.tabParams.setTabletYStop(newValue);
  }

  public int getTabletFilterPoints()
  {
    return this.tabParams.getTabletFilterPoints();
  }

  public void setTabletFilterPoints(int newValue)
  {
    this.tabParams.setTabletFilterPoints(newValue);
  }

  public int getTabletTimingAdvance()
  {
    return this.tabParams.getTabletTimingAdvance();
  }

  public void setTabletTimingAdvance(int newValue)
  {
    this.tabParams.setTabletTimingAdvance(newValue);
  }

  public int getTabletBaudRate()
  {
    return this.tabParams.getTabletBaudRate();
  }

  public void setTabletBaudRate(int newValue)
  {
    this.tabParams.setTabletBaudRate(newValue);
  }

  public int getTabletResolution()
  {
    return this.tabParams.getTabletResolution();
  }

  public void setTabletResolution(int newValue)
  {
    this.tabParams.setTabletResolution(newValue);
  }

  public int getTabletRotation()
  {
    return this.tabParams.getTabletRotationMode();
  }

  public void setTabletRotation(int newValue)
  {
    this.tabParams.setTabletRotationMode(newValue);
  }

  public String getTabletComPort()
  {
    return this.tabParams.getTabletComPort();
  }

  public void setTabletComPort(String newValue)
  {
    this.tabParams.setTabletComPort(newValue);
  }

  public boolean getTabletComTest()
  {
    return this.tabParams.getTabletComTest();
  }

  public void setTabletComTest(boolean newValue)
  {
    this.tabParams.setTabletComTest(newValue);
  }

  public int getTabletFormat()
  {
    return this.tabParams.getTabletFormat();
  }

  public void setTabletFormat(int newValue)
  {
    this.tabParams.setTabletFormat(newValue);
  }

  public String getTabletModel()
  {
    return this.tabParams.getTabletModel();
  }

  public void setTabletModel(String newValue)
  {
    boolean oldLcd = false;

    if (newValue == "SignatureGemLCD4X3")
    {
      if (this.tabletState == 0)
      {
        if (this.tablet.openTablet())
        {
          oldLcd = this.tablet.isRI();
          this.tablet.closeTablet();
        }
      }
      else
      {
        oldLcd = this.tablet.isRI();
      }
      if (oldLcd)
      {
        this.tabParams.setTabletModel("SignatureGemLCD4X3Old");
      }
      else
      {
        this.tabParams.setTabletModel("SignatureGemLCD4X3New");
      }
    }
    else
    {
      this.tabParams.setTabletModel(newValue);
    }
  }

  public boolean getTabletClippingMode()
  {
    return this.tabParams.getTabletClippingMode();
  }

  public void setTabletClippingMode(boolean newValue)
  {
    this.tabParams.setTabletClippingMode(newValue);
  }

  public int getTabletLCDType()
  {
    return this.tabParams.getTabletLCDType();
  }

  public void getTabletLCDType(int newValue)
  {
    this.tabParams.setTabletLCDType(newValue);
  }

  public int getTabletLCDXSize()
  {
    return this.tabParams.getTabletLCDXSize();
  }

  public void setTabletLCDXSize(int newValue)
  {
    this.tabParams.setTabletLCDXSize(newValue);
  }

  public int getTabletLCDYSize()
  {
    return this.tabParams.getTabletLCDYSize();
  }

  public void setTabletLCDYSize(int newValue)
  {
    this.tabParams.setTabletLCDYSize(newValue);
  }

  public int getTabletLCDXStart()
  {
    return this.tabParams.getTabletLCDXStart();
  }

  public void setTabletLCDXStart(int newValue)
  {
    this.tabParams.setTabletLCDXStart(newValue);
  }

  public int getTabletLCDXStop()
  {
    return this.tabParams.getTabletLCDXStop();
  }

  public void setTabletLCDXStop(int newValue)
  {
    this.tabParams.setTabletLCDXStop(newValue);
  }

  public int getTabletLCDYStart()
  {
    return this.tabParams.getTabletLCDYStart();
  }

  public void setTabletLCDYStart(int newValue)
  {
    this.tabParams.setTabletLCDYStart(newValue);
  }

  public int getTabletLCDYStop()
  {
    return this.tabParams.getTabletLCDYStop();
  }

  public void setTabletLCDYStop(int newValue)
  {
    this.tabParams.setTabletLCDYStop(newValue);
  }

  public float getDisplayPenWidth()
  {
    return this.displayParams.getPenWidth();
  }

  public void setDisplayPenWidth(float newValue)
  {
    this.displayParams.setPenWidth(newValue);
  }

  public boolean getDisplayTransparentMode()
  {
    return this.displayParams.getTransparentMode();
  }

  public void setDisplayTransparentMode(boolean newValue)
  {
    this.displayParams.setTransparentMode(newValue);
  }

  public int getDisplayRotation()
  {
    return this.displayParams.getRotation();
  }

  public void setDisplayRotation(int newValue)
  {
    this.displayParams.setRotation(newValue);
  }

  public int getDisplayJustifyX()
  {
    return this.displayParams.getJustifyX();
  }

  public void setDisplayJustifyX(int newValue)
  {
    this.displayParams.setJustifyX(newValue);
  }

  public int getDisplayJustifyY()
  {
    return this.displayParams.getJustifyY();
  }

  public void setDisplayJustifyY(int newValue)
  {
    this.displayParams.setJustifyY(newValue);
  }

  public int getDisplayJustifyMode()
  {
    return this.displayParams.getJustifyMode();
  }

  public void setDisplayJustifyMode(int newValue)
  {
    this.displayParams.setJustifyMode(newValue);
  }

  public int getDisplayDisplayMode()
  {
    return this.displayParams.getDisplayMode();
  }

  public void setDisplayDisplayMode(int newValue)
  {
    this.displayParams.setDisplayMode(newValue);
  }

  public int getDisplayTimeStampX()
  {
    return this.displayParams.getTimeStampX();
  }

  public void setDisplayTimeStampX(int newValue)
  {
    this.displayParams.setTimeStampX(newValue);
  }

  public int getDisplayTimeStampY()
  {
    return this.displayParams.getTimeStampY();
  }

  public void setDisplayTimeStampY(int newValue)
  {
    this.displayParams.setTimeStampY(newValue);
  }

  public int getDisplayTimeStampSize()
  {
    return this.displayParams.getTimeStampSize();
  }

  public void setDisplayTimeStampSize(int newValue)
  {
    this.displayParams.setTimeStampSize(newValue);
  }

  public int getDisplayAnnotationX()
  {
    return this.displayParams.getAnnotationX();
  }

  public void setDisplayAnnotationX(int newValue)
  {
    this.displayParams.setAnnotationX(newValue);
  }

  public int getDisplayAnnotationY()
  {
    return this.displayParams.getAnnotationY();
  }

  public void setDisplayAnnotationY(int newValue)
  {
    this.displayParams.setAnnotationY(newValue);
  }

  public int getDisplayAnnotationSize()
  {
    return this.displayParams.getAnnotationSize();
  }

  public void setDisplayAnnotationSize(int newValue)
  {
    this.displayParams.setAnnotationSize(newValue);
  }

  public boolean getDisplayTimeStamp()
  {
    return this.displayParams.getTimeStamp();
  }

  public void setDisplayTimeStamp(boolean newValue)
  {
    this.displayParams.setTimeStamp(newValue);
  }

  public boolean getDisplayAnnotation()
  {
    return this.displayParams.getAnnotation();
  }

  public void setDisplayAnnotation(boolean newValue)
  {
    this.displayParams.setAnnotation(newValue);
  }

  public int getImageXSize()
  {
    return this.imageParams.getXSize();
  }

  public void setImageXSize(int newValue)
  {
    this.imageParams.setXSize(newValue);
  }

  public int getImageYSize()
  {
    return this.imageParams.getYSize();
  }

  public void setImageYSize(int newValue)
  {
    this.imageParams.setYSize(newValue);
  }

  public float getImagePenWidth()
  {
    return this.imageParams.getPenWidth();
  }

  public void setImagePenWidth(float newValue)
  {
    this.imageParams.setPenWidth(newValue);
  }

  public boolean getImageTransparentMode()
  {
    return this.imageParams.getTransparentMode();
  }

  public void setImageTransparentMode(boolean newValue)
  {
    this.imageParams.setTransparentMode(newValue);
  }

  public int getImageRotation()
  {
    return this.imageParams.getRotation();
  }

  public void setImageRotation(int newValue)
  {
    this.imageParams.setRotation(newValue);
  }

  public int getImageJustifyX()
  {
    return this.imageParams.getJustifyX();
  }

  public void setImageJustifyX(int newValue)
  {
    this.imageParams.setJustifyX(newValue);
  }

  public int getImageJustifyY()
  {
    return this.imageParams.getJustifyY();
  }

  public void setImageJustifyY(int newValue)
  {
    this.imageParams.setJustifyY(newValue);
  }

  public int getImageJustifyMode()
  {
    return this.imageParams.getJustifyMode();
  }

  public void setImageJustifyMode(int newValue)
  {
    this.imageParams.setJustifyMode(newValue);
  }

  public int getImageDisplayMode()
  {
    return this.imageParams.getDisplayMode();
  }

  public void setImageDisplayMode(int newValue)
  {
    this.imageParams.setDisplayMode(newValue);
  }

  public int getImageTimeStampX()
  {
    return this.imageParams.getTimeStampX();
  }

  public void setImageTimeStampX(int newValue)
  {
    this.imageParams.setTimeStampX(newValue);
  }

  public int getImageTimeStampY()
  {
    return this.imageParams.getTimeStampY();
  }

  public void setImageTimeStampY(int newValue)
  {
    this.imageParams.setTimeStampY(newValue);
  }

  public int getImageTimeStampSize()
  {
    return this.imageParams.getTimeStampSize();
  }

  public void setImageTimeStampSize(int newValue)
  {
    this.imageParams.setTimeStampSize(newValue);
  }

  public int getImageAnnotationX()
  {
    return this.imageParams.getAnnotationX();
  }

  public void setImageAnnotationX(int newValue)
  {
    this.imageParams.setAnnotationX(newValue);
  }

  public int getImageAnnotationY()
  {
    return this.imageParams.getAnnotationY();
  }

  public void setImageAnnotationY(int newValue)
  {
    this.imageParams.setAnnotationY(newValue);
  }

  public int getImageAnnotationSize()
  {
    return this.imageParams.getAnnotationSize();
  }

  public void setImageAnnotationSize(int newValue)
  {
    this.imageParams.setAnnotationSize(newValue);
  }

  public boolean getImageTimeStamp()
  {
    return this.imageParams.getTimeStamp();
  }

  public void setImageTimeStamp(boolean newValue)
  {
    this.imageParams.setTimeStamp(newValue);
  }

  public boolean getImageAnnotation()
  {
    return this.imageParams.getAnnotation();
  }

  public void setImageAnnotation(boolean newValue)
  {
    this.imageParams.setAnnotation(newValue);
  }

  private byte[] convertHexStringToByteArray(String hexStr)
  {
    byte[] buffer = new byte[hexStr.length() / 2];

    int i = 0;
    for (int j = 0; j < buffer.length; j++)
    {
      char msNibble = hexStr.charAt(i++);
      char lsNibble = hexStr.charAt(i++);
      int outByte = fromHex(msNibble) << 4 | fromHex(lsNibble);
      buffer[j] = ((byte)outByte);
    }
    return buffer;
  }

  private String convertByteArrayToHexString(byte[] byteArray)
  {
    String buffer = new String();

    for (int j = 0; j < byteArray.length; j++)
    {
      int msNibble = (byteArray[j] & 0xF0) >> 4;
      int lsNibble = byteArray[j] & 0xF;
      buffer = buffer + toHex(msNibble);
      buffer = buffer + toHex(lsNibble);
    }
    return buffer;
  }

  private int fromHex(char hexChar)
  {
    if ((hexChar >= '0') && (hexChar <= '9'))
    {
      return hexChar - '0';
    }
    if ((hexChar >= 'A') && (hexChar <= 'F'))
    {
      return hexChar - 'A' + 10;
    }
    if ((hexChar >= 'a') && (hexChar <= 'f'))
    {
      return hexChar - 'a' + 10;
    }

    return 0;
  }

  private char toHex(int hexInt)
  {
    if (hexInt < 10)
    {
      int returnChar = (hexInt & 0xF) + 48;
      return (char)returnChar;
    }

    int returnChar = (hexInt & 0xF) - 10 + 65;
    return (char)returnChar;
  }

  public synchronized void addSigPlusListener(SigPlusListener l)
  {
    this.m_SigPlusListeners.addElement(l);
  }

  public synchronized void removeSigPlusListener(SigPlusListener l)
  {
    this.m_SigPlusListeners.removeElement(l);
  }

  protected void fireTabletTimerEvent()
  {
    SecueraSigPlusEvent0 evt = new SecueraSigPlusEvent0(this);
    Vector l;
    synchronized (this) { l = (Vector)this.m_SigPlusListeners.clone(); }

    //for (int i = 0; i < l.size(); i++)
      //((SigPlusListener)l.elementAt(i)).handleTabletTimerEvent(evt);
  }

  protected void fireNewTabletData()
  {
    SecueraSigPlusEvent0 evt = new SecueraSigPlusEvent0(this);
    Vector l;
    synchronized (this) { l = (Vector)this.m_SigPlusListeners.clone(); }

   // for (int i = 0; i < l.size(); i++)
    //  ((SigPlusListener)l.elementAt(i)).handleNewTabletData(evt);
  }

  protected void fireKeyPadData()
  {
    SecueraSigPlusEvent0 evt = new SecueraSigPlusEvent0(this);
    Vector l;
    synchronized (this) { l = (Vector)this.m_SigPlusListeners.clone(); }

   // for (int i = 0; i < l.size(); i++)
     // ((SigPlusListener)l.elementAt(i)).handleKeyPadData(evt);
  }

  public void autoKeyAddData(byte[] buffer)
  {
    this.cryptor.autoKeyAddData(buffer, this.sig);
  }
  public void autoKeyFinish() {
    this.cryptor.autoKeyFinish(this.sig);
  }

  public void autoKeyStart()
  {
    this.cryptor.autoKeyStart(this.sig);
  }

  public int getEncryptionMode()
  {
    return this.cryptor.getEncryptionMode();
  }
  public int getKeyReceipt() {
    return this.cryptor.getKeyReceipt();
  }
  public String getKeyReceiptAscii() {
    return this.cryptor.getKeyReceiptAscii();
  }
  public String getKeyString() {
    return this.cryptor.getKeyString();
  }
  public boolean getSaveSigInfo() {
    return this.sig.getSaveSigInfo();
  }
  public int getSigCompressionMode() {
    return this.compressionMode;
  }

  public String getSigPlusVersion()
  {
    return "SigPlusJava v2.60";
  }
  public int getSigReceipt() {
    return this.cryptor.getSigReceipt(this.sig, this.displayParams, this.imageParams, this.compressionMode);
  }
  public String getSigReceiptAscii() {
    return this.cryptor.getSigReceiptAscii(this.sig, this.displayParams, this.imageParams, this.compressionMode);
  }
  public int numberOfTabletPoints() {
    SigInfo info = new SigInfo();
    this.sig.getSigData(info);
    return info.getNumPoints();
  }
  public void setAutoKeyData(String autoKeyData) {
    this.cryptor.setAutoKeyData(autoKeyData, this.sig);
  }

  public void setEncryptionMode(int encryptionMode) {
    this.cryptor.setEncryptionMode(encryptionMode, this.sig);
  }
  public void setKeyString(String keyString) {
    this.cryptor.setKeyString(keyString, this.sig);
  }
  public void setSaveSigInfo(boolean saveSigInfo) {
    this.sig.setSaveSigInfo(saveSigInfo);
  }
  public void setSigCompressionMode(int compMode) {
    this.compressionMode = compMode;
  }

  public BufferedImage sigImage()
  {
    SigDrawType imageDraw = new SigDrawType(this.sig, this.tabParams);

    BufferedImage sigImage = new BufferedImage(this.imageParams.getXSize(), this.imageParams.getYSize(), 5);
    Graphics sigGraph = sigImage.getGraphics();

    if (sigGraph != null)
    {
      try
      {
        imageDraw.paint(sigGraph, this.imageParams, 0);
      }
      finally
      {
        sigGraph.dispose();
      }
    }

    return sigImage;
  }

  public int getBadPointCounter()
  {
    return this.tablet.badPointCounter;
  }

  public long getModelNumber()
  {
    return this.tablet.getTabletModelNumber();
  }

  public String getPdfString()
  {
    SigDrawType pdfDraw = new SigDrawType(this.sig, this.tabParams);

    return pdfDraw.getPdfString(this.imageParams);
  }

  public long getSerialNumber()
  {
    return this.tablet.getTabletSerialNumber();
  }

  public TopazSigCapData getSignatureData()
  {
    TopazSigCapData sigCapData = new TopazSigCapData();

    int numPoints = 0;
    int numStrokes = 0;
    int numStrokePoints = 0;
    int numPointsInVect = 0;
    int numPointsExtracted = 0;

    SigInfo sigData = new SigInfo();

    this.sig.getSigData(sigData);

    numStrokes = sigData.getNumStrokes();

    numPoints = sigData.getNumPoints();

    if (numPoints > 0)
    {
      sigCapData.m_PointArray = new SigDataPoint[numPoints + numStrokes];

      Vector tmpVect = sigData.getPointData();

      for (int j = 0; j < numStrokes; j++)
      {
        numStrokePoints = sigData.getNumPointsInStroke(j);

        for (int i = 0; i < numStrokePoints; i++)
        {
          SigDataPoint tmpDataPoint = (SigDataPoint)tmpVect.elementAt(numPointsInVect);
          sigCapData.m_PointArray[numPointsExtracted] = new SigDataPoint(tmpDataPoint.x, tmpDataPoint.y, 0L);
          numPointsInVect++;
          numPointsExtracted++;
        }

        sigCapData.m_PointArray[numPointsExtracted] = new SigDataPoint(65535, 65535, 0L);
        numPointsExtracted++;
      }

      sigCapData.m_byRawData = getSigString();
    }

    return sigCapData;
  }

  public int getTabletMaxPointDelta()
  {
    return this.tabParams.getTabletMaxPointDelta();
  }

  public int getXExtent()
  {
    SigInfo info = new SigInfo();
    this.sig.getSigData(info);
    return info.getXExtent();
  }

  public int getYExtent()
  {
    SigInfo info = new SigInfo();
    this.sig.getSigData(info);
    return info.getYExtent();
  }

  public byte[] lcdSendCmdString(byte[] cmdString, int returnCount, long timeOut)
  {
    return this.tablet.lcdSendCmdString(cmdString, returnCount, timeOut);
  }

  public void lcdWriteString(int dest, int mode, int xpos, int ypos, String text, Font typeFace)
  {
    this.lcdGraphics.lcdWriteString(dest, mode, xpos, ypos, text, typeFace);
  }

  public void setTabletMaxPointDelta(int newDelta)
  {
    this.tabParams.setTabletMaxPointDelta(newDelta);
  }

  public int getNumberOfStrokes()
  {
    SigInfo info = new SigInfo();
    this.sig.getSigData(info);
    return info.getNumStrokes();
  }

  public int getNumPointsForStroke(int strokeNumber)
  {
    SigInfo info = new SigInfo();
    this.sig.getSigData(info);
    return info.getNumPointsInStroke(strokeNumber);
  }

  public long getPointTValue(int strokeNumber, int pointNumber)
  {
    SigInfo info = new SigInfo();

    if (this.cryptor.getEncryptionMode() > 1)
    {
      return 0L;
    }
    this.sig.getSigData(info);
    SigDataPoint pnt = info.getPointInStroke(strokeNumber, pointNumber);
    if (pnt != null)
    {
      return pnt.t;
    }

    return 0L;
  }

  public int getPointXValue(int strokeNumber, int pointNumber)
  {
    SigInfo info = new SigInfo();

    if (this.cryptor.getEncryptionMode() > 1)
    {
      return 0;
    }
    this.sig.getSigData(info);
    SigDataPoint pnt = info.getPointInStroke(strokeNumber, pointNumber);
    if (pnt != null)
    {
      return pnt.x;
    }

    return 0;
  }

  public int getPointYValue(int strokeNumber, int pointNumber)
  {
    SigInfo info = new SigInfo();

    if (this.cryptor.getEncryptionMode() > 1)
    {
      return 0;
    }
    this.sig.getSigData(info);
    SigDataPoint pnt = info.getPointInStroke(strokeNumber, pointNumber);
    if (pnt != null)
    {
      return pnt.y;
    }

    return 0;
  }

  public int getPointPValue(int strokeNumber, int pointNumber)
  {
    SigInfo info = new SigInfo();

    if (this.cryptor.getEncryptionMode() > 1)
    {
      return 0;
    }
    this.sig.getSigData(info);
    SigDataPoint pnt = info.getPointInStroke(strokeNumber, pointNumber);
    if (pnt != null)
    {
      return (int)pnt.p;
    }

    return 0;
  }

  public boolean getSavePressureData()
  {
    return this.sig.getSavePressureData();
  }

  public boolean getSaveTimeData()
  {
    return this.sig.getSaveTimeData();
  }

  public long getTabletModelNumber()
  {
    return this.tablet.getTabletModelNumber();
  }

  public long getTabletSerialNumber()
  {
    return this.tablet.getTabletSerialNumber();
  }

  public void setLCDTabletMap(int newLCDType, int newLCDXSize, int newLCDYSize, int newLCDXStart, int newLCDYStart, int newLCDXStop, int newLCDYStop, int newLCDCompMode, int newLCDFastComp, int newLCDSlowComp)
  {
    this.tabParams.setTabletLCDParameters(newLCDType, newLCDXSize, newLCDYSize, 
      newLCDXStart, newLCDYStart, 
      newLCDXStop, newLCDYStop, newLCDCompMode, newLCDFastComp, newLCDSlowComp);
  }

  public void setSavePressureData(boolean state)
  {
    this.sig.setSavePressureData(state);
  }

  public void setSaveTimeData(boolean state)
  {
    this.sig.setSaveTimeData(state);
  }

  public void setTabletLogicalXSize(int newValue)
  {
    this.tabParams.setTabletLogicalXSize(newValue);
  }

  public void setTabletLogicalYSize(int newValue)
  {
    this.tabParams.setTabletLogicalYSize(newValue);
  }

  public void autoKeyFromFile(String filename)
  {
    this.cryptor.autoKeyFromFile(filename, this.sig);
  }

  public int getLCDPixelDepth()
  {
    return this.lcdPixelDepth;
  }
  public boolean lcdWriteImage(int dest, int mode, int xPos, int yPos, int xSize, int ySize, Image imageData, int pixelDepth) {
    BufferedImage sigImage = new BufferedImage(imageData.getWidth(this), imageData.getHeight(this), 1);
    sigImage.getGraphics().drawImage(imageData, 0, 0, this);
    return this.lcdGraphics.lcdWriteImage(dest, mode, xPos, yPos, xSize, ySize, sigImage, pixelDepth);
  }

  public void setLCDPixelDepth(int pixelDepth)
  {
    this.lcdPixelDepth = pixelDepth;
  }
}