/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.beans.piv.KeyHistoryBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.SendCommand;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class KeyHistoryObject {

    private CommonUtil util = new CommonUtil();
    private SendCommand sendCard = new SendCommand();
    ExceptionMessages expMsg = new ExceptionMessages();
    String errorMsg;
    KeyHistoryBean keyhistbean = new KeyHistoryBean();

    public KeyHistoryObject() {
        super();
    }

    public KeyHistoryBean readKeyObj(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        String onCardCertsTag = "C1";
        String totalCertsTag = "C2";
        byte[] res = null;
        int i;
        byte[] readData = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x05,
            0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0C};

        // Command to read discovery container
        CommandAPDU DISCOVER_KEY_APDU = new CommandAPDU(readData);

        try {

            res = sendCard.sendCommand(channel, DISCOVER_KEY_APDU, sb);
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "Error While Reading Key History Object");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException ex) {
            throw new MiddleWareException("Card Exception.  " + ex.getMessage());
        }

        if (res.length < 3) {
            return keyhistbean;
        }


        String returnValue = util.arrayToHex(res);

        if (returnValue.substring(2, 4).equalsIgnoreCase(UcmsMiddlewareConstants.bigLength)) {
            returnValue = returnValue.substring(8);
        } else if (returnValue.substring(2, 4).equalsIgnoreCase(UcmsMiddlewareConstants.smallLength)) {
            returnValue = returnValue.substring(6);
        } else {
            returnValue = returnValue.substring(4);
        }



        if (returnValue.substring(0, 2).equalsIgnoreCase(onCardCertsTag)) {
            keyhistbean.setOnCardCerts(Integer.valueOf(returnValue.substring(4, 6), 16).intValue());
        } else if (returnValue.substring(0, 2).equalsIgnoreCase(totalCertsTag)) {
            keyhistbean.setOffCardCerts(Integer.valueOf(returnValue.substring(4, 6), 16).intValue());
        }

        if (returnValue.substring(6, 8).equalsIgnoreCase(onCardCertsTag)) {
            keyhistbean.setOnCardCerts(Integer.valueOf(returnValue.substring(10, 12), 16).intValue());
        } else if (returnValue.substring(6, 8).equalsIgnoreCase(totalCertsTag)) {
            keyhistbean.setOffCardCerts(Integer.valueOf(returnValue.substring(10, 12), 16).intValue());
        }

        returnValue = returnValue.substring(12);

        //Now get the URL and HASH Value
        i = Integer.valueOf(returnValue.substring(2, 4), 16).intValue();
        returnValue = returnValue.substring(4);

        keyhistbean.setURL(util.hexToString(returnValue.substring(0, i * 2)));

        return keyhistbean;



    }

    public boolean updateKeyHistoryobj(CardChannel channel, KeyHistoryBean keyhistbean) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] command_data;

        byte[] ErrorByte = {(byte) 0xFE, 0x00};

        byte[] tagOnCardCerts = {(byte) 0xC1, 0x01};
        byte[] tagOffCardCerts = {(byte) 0xC2, 0x01};
        byte[] tagURL = {(byte) 0xF3};

        byte[] byteOnCardCerts;
        byte[] byteOffCardCerts;
        byte[] URL = null;


        int OnCardCerts;
        int OffCardCerts;
        String hexURL;

        OnCardCerts = keyhistbean.getOnCardCerts();
        OffCardCerts = keyhistbean.getOffCardCerts();
        hexURL = keyhistbean.getURL();


        byteOnCardCerts = util.calcBertValue(OnCardCerts, false, true);
        byteOffCardCerts = util.calcBertValue(OffCardCerts, false, true);



        if (!isEmpty(hexURL)) {
            URL = util.hex1ToByteArray(util.stringToHex(hexURL));

        }


        //////////////////////////////////////////////////////
        byte[] apdu = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};
        byte[] cont_id = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x0C}; //For Key History Object
        ///////////////////////////////////////////////////////

        //Now Calculate size of the record to be stored
        Integer len_total_bert = tagOnCardCerts.length+ byteOnCardCerts.length + 
                tagOffCardCerts.length + byteOffCardCerts.length + tagURL.length + URL.length;


        byte[] bert_tag_total = util.calcBertValue(len_total_bert, true, false);

        byte[][] command_arrays = new byte[9][];
        command_arrays[0] = cont_id;
        command_arrays[1] = bert_tag_total;
        command_arrays[2] = tagOnCardCerts;
        command_arrays[3] = byteOnCardCerts;
        command_arrays[4] = tagOffCardCerts;
        command_arrays[5] = byteOffCardCerts;
        command_arrays[6] = tagURL;
        command_arrays[7] = URL;
        command_arrays[8] = ErrorByte;


        command_data = util.combine_data(command_arrays);

        //Now Calculate size of the record to be stored
        Integer len_total_apdu = command_data.length;
        byte[] apdu_total = util.calcBertValue(len_total_apdu, false, true);


        command_arrays = new byte[3][];
        command_arrays[0] = apdu;
        command_arrays[1] = apdu_total;
        command_arrays[2] = command_data;

        command_data = util.combine_data(command_arrays);

       // System.out.println("Final Command " + util.arrayToHex(command_data));

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        //System.out.println("Final Result " + sb.toString());
        return true;
    }

    private boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
}
