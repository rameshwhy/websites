package com.id3.fingerprint;

public class FingerCaptureDeviceTechnology
{
  public static final int Unknown = 0;
  public static final int WhiteLightOpticalTIR = 1;
  public static final int WhiteLightOpticalDirectViewOnPlaten = 2;
  public static final int WhiteLightOpticalToucheless = 3;
  public static final int MonochromaticVisibleOpticalTIR = 4;
  public static final int MonochromaticVisibleOpticalDirectViewOnPlaten = 5;
  public static final int MonochromaticVisibleOpticalToucheless = 6;
  public static final int MonochromaticIROpticalTIR = 7;
  public static final int MonochromaticIROpticalDirectViewOnPlaten = 8;
  public static final int MonochromaticIROpticalToucheless = 9;
  public static final int MultispectralOpticalTIR = 10;
  public static final int MultispectralOpticalDirectViewOnPlaten = 11;
  public static final int MultispectralOpticalToucheless = 12;
  public static final int ElectroLuminescent = 13;
  public static final int SemiconductorCapacitive = 14;
  public static final int SemiconductorRF = 15;
  public static final int SemiconductorThermal = 16;
  public static final int PressureSensitive = 17;
  public static final int Ultrasound = 18;
  public static final int Mechanical = 19;
  public static final int GlassFiber = 20;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCaptureDeviceTechnology
 * JD-Core Version:    0.6.2
 */