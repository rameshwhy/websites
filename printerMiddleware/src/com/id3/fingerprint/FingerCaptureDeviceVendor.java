package com.id3.fingerprint;

public class FingerCaptureDeviceVendor
{
  public static final int Unreported = 0;
  public static final int Identix = 3;
  public static final int Secugen = 10;
  public static final int PreciseBiometrics = 11;
  public static final int Upek = 18;
  public static final int CrossMatch = 24;
  public static final int SagemMorpho = 29;
  public static final int Lumidigm = 37;
  public static final int DigitalPersona = 51;
  public static final int id3 = 63;
  public static final int GreenBit = 64;
  public static final int AuthenTec = 66;
  public static final int Suprema = 68;
  public static final int Futronic = 77;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCaptureDeviceVendor
 * JD-Core Version:    0.6.2
 */