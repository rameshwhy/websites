// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.wia;

import java.util.*;

// Referenced classes of package eu.gnome.morena.wia:
//            WIAConstants

public class SupportedPropertyValues
    implements WIAConstants
{

    public SupportedPropertyValues(int i, String s, boolean flag, int j, int k, int ai[])
    {
        propId = i;
        desc = s;
        isCurrentValueSet = flag;
        currentValue = j;
        propertyFlag = k;
        values = ai;
    }

    public List getList()
    {
        Vector vector = new Vector();
        if(isList())
        {
            for(int i = 2; i < values.length; i++)
                vector.add(Integer.valueOf(values[i]));

        } else
        if(isRange())
        {
            for(int j = values[0]; j <= values[2]; j += values[3])
                vector.add(Integer.valueOf(j));

        }
        return vector;
    }

    public int getDefaultValue()
    {
        if(values.length > 0)
        {
            if(isList())
                return values[1];
            if(isRange())
                return values[1];
            if(isFlag())
                return values[0];
        }
        return 0;
    }

    public boolean isCurrentValueSet()
    {
        return isCurrentValueSet;
    }

    public int getCurrentValue()
    {
        return currentValue;
    }

    public boolean isFlagSet(int i)
    {
        return (values[0] & i) > 0;
    }

    public boolean isWritable()
    {
        return (propertyFlag & 2) > 0;
    }

    public boolean isList()
    {
        return (propertyFlag & 0x20) > 0 && values.length > 2;
    }

    public boolean isRange()
    {
        return (propertyFlag & 0x10) > 0 && values.length > 3;
    }

    public boolean isFlag()
    {
        return (propertyFlag & 0x40) > 0 && values.length > 1;
    }

    public boolean isNone()
    {
        return (propertyFlag & 8) > 0;
    }

    public int getMaxValue()
    {
        if(isRange())
            return values[2];
        if(isList())
        {
            int i = 0x80000000;
            for(int j = 0; j < values.length; j++)
                if(i < values[j])
                    i = values[j];

            return i;
        } else
        {
            return 0x7fffffff;
        }
    }

    public int getMinValue()
    {
        if(isRange())
            return values[0];
        if(isList())
        {
            int i = 0x7fffffff;
            for(int j = 0; j < values.length; j++)
                if(i > values[j])
                    i = values[j];

            return i;
        } else
        {
            return 0x80000000;
        }
    }

    public String toString()
    {
        return (new StringBuilder()).append(propId).append(" ").append(Arrays.toString(values)).toString();
    }

    public String print()
    {
        StringBuilder stringbuilder = new StringBuilder(128);
        String s = isCurrentValueSet ? String.format("- 0x%1$-8X (%1$d) ", new Object[] {
            Integer.valueOf(currentValue)
        }) : "                         ";
        stringbuilder.append(String.format("%8d - ", new Object[] {
            Integer.valueOf(propId)
        })).append(String.format("%-30s ", new Object[] {
            desc
        })).append(String.format("%-25s", new Object[] {
            s
        })).append(String.format("   %05X ", new Object[] {
            Integer.valueOf(propertyFlag)
        })).append(String.format("%3s ", new Object[] {
            accessFlags(propertyFlag)
        }));
        if(isList())
            stringbuilder.append("list ").append(getList()).append(" (").append(values[1]).append(")");
        else
        if(isRange())
            stringbuilder.append("range from ").append(values[0]).append(" to ").append(values[2]).append(" step ").append(values[3]).append(" (").append(values[1]).append(")");
        else
        if(isFlag())
            stringbuilder.append("flag ").append(String.format("%08X ", new Object[] {
                Integer.valueOf(values[1])
            })).append(" (").append(values[0]).append(")");
        else
        if(isNone())
            stringbuilder.append("none");
        else
        if((propertyFlag & 0x20) > 0)
            stringbuilder.append("list empty");
        else
        if((propertyFlag & 0x10) > 0)
            stringbuilder.append("range empty");
        else
        if((propertyFlag & 0x40) > 0)
            stringbuilder.append("flag empty");
        return stringbuilder.toString();
    }

    private String accessFlags(int i)
    {
        StringBuilder stringbuilder = new StringBuilder(8);
        if((i & 0x10000) > 0)
            stringbuilder.append('C');
        if((i & 1) > 0)
            stringbuilder.append('R');
        if((i & 2) > 0)
            stringbuilder.append('W');
        return stringbuilder.toString();
    }

    private int propId;
    private String desc;
    private int propertyFlag;
    private boolean isCurrentValueSet;
    private int currentValue;
    private int values[];
}
