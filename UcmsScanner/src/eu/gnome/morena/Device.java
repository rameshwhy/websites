// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena;


// Referenced classes of package eu.gnome.morena:
//            Version, TransferDoneListener

public interface Device
    extends Version
{

    public abstract boolean setupDevice(Object obj);

    public abstract void startTransfer(TransferDoneListener transferdonelistener)
        throws Exception;

    public abstract void startTransfer()
        throws Exception;

    public abstract String toString();
}
