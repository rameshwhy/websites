/*     */ package com.id3.fingerprint;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class FingerMatchResult
/*     */ {
/*  18 */   private boolean _disposed = false;
/*     */   private long _handle;
/*     */ 
/*     */   public FingerMatchResult(long handle)
/*     */   {
/*  26 */     this._handle = handle;
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/*  34 */     nDispose(this._handle);
/*  35 */     this._handle = 0L;
/*  36 */     this._disposed = true;
/*     */   }
/*     */ 
/*     */   protected void finalize()
/*     */     throws Throwable
/*     */   {
/*     */     try
/*     */     {
/*  46 */       dispose();
/*     */     }
/*     */     catch (Exception localException)
/*     */     {
/*     */     }
/*     */     finally
/*     */     {
/*  53 */       super.finalize();
/*     */     }
/*     */   }
/*     */ 
/*     */   public boolean equals(Object obj)
/*     */   {
/*  64 */     if ((obj == null) || (obj.getClass() != getClass())) {
/*  65 */       return false;
/*     */     }
/*  67 */     FingerMatchResult object = (FingerMatchResult)obj;
/*  68 */     return this._handle == object._handle;
/*     */   }
/*     */ 
/*     */   public int hashCode()
/*     */   {
/*  77 */     return (int)this._handle;
/*     */   }
/*     */ 
/*     */   public long getHandle()
/*     */   {
/*  85 */     return this._handle;
/*     */   }
/*     */ 
/*     */   public int getScore()
/*     */     throws FingerException
/*     */   {
/*  96 */     int[] value = new int[1];
/*  97 */     int result = nGetScore(this._handle, value);
/*     */ 
/*  99 */     if (result != 0) {
/* 100 */       throw new FingerException(result);
/*     */     }
/* 102 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getSimilarityRate()
/*     */     throws FingerException
/*     */   {
/* 113 */     int[] value = new int[1];
/* 114 */     int result = nGetSimilarityRate(this._handle, value);
/*     */ 
/* 116 */     if (result != 0) {
/* 117 */       throw new FingerException(result);
/*     */     }
/* 119 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getDecision()
/*     */     throws FingerException
/*     */   {
/* 129 */     int[] value = new int[1];
/* 130 */     int result = nGetDecision(this._handle, value);
/*     */ 
/* 132 */     if (result != 0) {
/* 133 */       throw new FingerException(result);
/*     */     }
/* 135 */     return value[0];
/*     */   }
/*     */ 
/*     */   public FingerMatchDetails getFingerDetails(int position)
/*     */     throws FingerException
/*     */   {
/* 146 */     long[] handle = new long[1];
/* 147 */     int result = nGetFingerDetails(this._handle, position, handle);
/*     */ 
/* 149 */     if (result != 0) {
/* 150 */       throw new FingerException(result);
/*     */     }
/* 152 */     return new FingerMatchDetails(handle[0]);
/*     */   }
/*     */ 
/*     */   public FingerMatchDetails[] getDetails()
/*     */     throws FingerException
/*     */   {
/* 162 */     ArrayList detailsList = new ArrayList();
/*     */ 
/* 164 */     for (int i = 0; i < 11; i++)
/*     */     {
/* 166 */       FingerMatchDetails details = getFingerDetails(i);
/*     */ 
/* 168 */       if (details.getIndexInProbe() != -1)
/* 169 */         detailsList.add(details);
/*     */     }
/* 171 */     return (FingerMatchDetails[])detailsList.toArray();
/*     */   }
/*     */ 
/*     */   private static final native int nDispose(long paramLong);
/*     */ 
/*     */   private static final native int nGetScore(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetSimilarityRate(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetDecision(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetFingerDetails(long paramLong, int paramInt, long[] paramArrayOfLong);
/*     */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerMatchResult
 * JD-Core Version:    0.6.2
 */