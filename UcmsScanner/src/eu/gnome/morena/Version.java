// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena;

import java.io.File;

public interface Version
{

    public static final String VERSION_MAJOR = "7";
    public static final String VERSION_MINOR = "1";
    public static final String VERSION_BUILD = "25";
    public static final String VERSION = "7.1 build 25";
    public static final String BUILD_DATE = "21/10/2013";
    public static final String BUILD_TIME = "21:13:49";
    public static final File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));

}
