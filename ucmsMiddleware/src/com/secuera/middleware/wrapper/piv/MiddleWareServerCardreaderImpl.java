/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.wrapper.piv;

import com.secuera.middleware.beans.piv.LoadUserCertificates;
import com.secuera.middleware.beans.piv.MasterKeyCeremonyBean;
import com.secuera.middleware.beans.piv.MasterKeyCeremonyResultBean;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.DesEncrypter;
import com.secuera.middleware.core.common.AesEncrypter;
import com.secuera.middleware.beans.piv.BiometricBean;
import com.secuera.middleware.beans.piv.CardInfoBean;
import com.secuera.middleware.beans.piv.CSRBean;
import com.secuera.middleware.beans.piv.CertificateBean;
import com.secuera.middleware.beans.piv.PrintedInfoBean;
import com.secuera.middleware.beans.piv.UserCSR;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import com.secuera.middleware.cardreader.exceptions.LoggerWrapper;
import javax.smartcardio.CardChannel;
import com.secuera.middleware.beans.piv.ChuidBean;
import com.secuera.middleware.beans.piv.ContainerInfoBean;
import com.secuera.middleware.beans.piv.ContentCertificateBean;
import com.secuera.middleware.beans.piv.CredentialInformationBean;
import com.secuera.middleware.beans.piv.CredentialKeys;
import com.secuera.middleware.beans.piv.DiversifiedCredentialKeys;
import com.secuera.middleware.beans.piv.ErrorMessage;
import com.secuera.middleware.beans.piv.FacialBean;
import com.secuera.middleware.beans.piv.FacialImageBean;
import com.secuera.middleware.beans.piv.FascnBean;
import com.secuera.middleware.beans.piv.FingerPrintBean;
import com.secuera.middleware.beans.piv.InitializationBean;
import com.secuera.middleware.beans.piv.PersonalInformationBean;
import com.secuera.middleware.beans.piv.PersonalizationBean;
import com.secuera.middleware.beans.piv.PinPolicyBean;
import com.secuera.middleware.beans.piv.SecurityBean;
import com.secuera.middleware.beans.piv.UserCertificates;
import com.secuera.middleware.beans.piv.UserContentCSR;
import com.secuera.middleware.beans.piv.UserFingerPintBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.exceptions.ExceptionUtilities;
import com.secuera.middleware.cardreader.validate.CardReaderValidator;
import com.secuera.middleware.core.piv.CardInit;
import com.secuera.middleware.core.piv.CardUtil;
import com.secuera.middleware.core.piv.CardUtilInterface;
import com.secuera.middleware.core.piv.DiscoverContainers;
import com.secuera.middleware.core.piv.EndOfPerso;
import com.secuera.middleware.core.piv.GenCertificate;
import com.secuera.middleware.core.piv.GenContentCertificate;
import com.secuera.middleware.core.piv.GeneralAuth;
import com.secuera.middleware.core.piv.Id3BioCapture;
import com.secuera.middleware.core.piv.LoadCertificate;
import com.secuera.middleware.core.piv.LoadSecurityCont;
import com.secuera.middleware.core.piv.ManageCHUID;
import com.secuera.middleware.core.piv.ManagePrintedInfo;
import com.secuera.middleware.core.piv.MasterKeyCeremony;
import com.secuera.middleware.core.piv.PivUtil;
import com.secuera.middleware.core.piv.PivUtilInterface;
import com.secuera.middleware.core.piv.ReadCertificate;
import com.secuera.middleware.core.piv.SecureChannel01;
import com.secuera.middleware.core.piv.SecureChannel03;
import com.secuera.middleware.core.piv.VerifyFP;
import com.secuera.middleware.core.piv.defaultInitValues;
import com.secuera.middleware.core.piv.verifySecudataSignature;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import sun.misc.BASE64Encoder;

/*
 * 
 * 
import com.secuera.beans.PrintedInfoBean;
import com.secuera.core.CardComm;
import com.secuera.core.CardUtil;
import com.secuera.core.DesEncrypter;
import com.secuera.core.PivUtil;
import com.secuera.core.CommonUtil;
import com.secuera.core.defaultInitValues;
import com.secuera.entities.MiddleWareException;
import com.secuera.beans.BiometricBean;
import com.secuera.core.VerifyFP;
import com.secuera.core.PivUtilInterface;
import com.secuera.core.CardInit;
 */
/**
 *
 * @author admin
 */
public class MiddleWareServerCardreaderImpl implements MiddleWareServerCardreader {

    private String client = "";
    private CommonUtil cmnUtil = null;
    private VerifyFP fpUtil = null;
    private PrintedInfoBean prntInfo = null;
    private defaultInitValues defvalue = null;
    private DiversifiedCredentialKeys diversifiedKeys = null;
    private  int minPinLength=0;
    private  int maxPinLength= 0;
    private static PivUtilInterface pivUtil ;
    

    /*
     * Function : verifyLocalPin Purpose : verify local pin with card 
     * Date : 09/11/2012 
     * version : 1.0
     */
    //basic Card Access Functions
    public List<String> getTerminalsList() throws MiddleWareException {
        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = null;
        List terminalName = new ArrayList<String>();
        try {
            terminals = factory.terminals().list();
            
            for (int i = 0; i < terminals.size(); i++) {
                CardTerminal terminal = terminals.get(i);
                terminalName.add(terminal.getName().toString());
            }

            return terminalName;
        } catch (CardException e) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.NO_CARD_TERMINAL_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }catch (NullPointerException npe){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.NO_CARD_TERMINAL_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }
        
    }
        

    private PivUtilInterface getPivUtil() {
        if(pivUtil== null){
           pivUtil =  new PivUtil();
        }
        return pivUtil;
    }

    
    
    /************************ credential Information ***************************************/
    @Override
    public CredentialInformationBean readCredentialInformation(CardChannel channel) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "readCredentialInformation");
        CredentialInformationBean credentialInformation = new CredentialInformationBean();
        ChuidBean chuid = new ChuidBean();
        String retValue = null;
        
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCredentialInformation start time: "
                + startTime);

        cmnUtil = new CommonUtil();
        ManageCHUID mngCHUID = new ManageCHUID();
        //Read CHUID & set value of credential Bean
        chuid = mngCHUID.readChuid(channel);
        credentialInformation.setChuid(chuid);

        //Read CIN & set value of credential Bean
        retValue = getPivUtil().readCIN(channel);
        credentialInformation.setCIN(retValue);

        //Read IIN & set value of credential Bean
        retValue = getPivUtil().readIIN(channel);
        credentialInformation.setIIN(retValue);

        //Read CUID & set value of credential Bean
        retValue = getPivUtil().readCUID(channel);
        credentialInformation.setCUID(retValue);

        //Read BAP & set value of credential Bean
        retValue = getPivUtil().readBAP(channel);
        credentialInformation.setBAP(retValue);

        //read ATR
        credentialInformation.setATR(cmnUtil.arrayToHex(channel.getCard().getATR().getBytes()));

        //read Card Model
        credentialInformation.setCardModel("");

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCredentialInformation total time: "
                + totalTime);

        return credentialInformation;

    }

    //CHUID-FASCN Functions    
    @Override
    public byte[] readFASCN(CardChannel channel) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "readFASCN");
        byte[] FASCN = null;
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readFASCN start time: "
                + startTime);


        // ---------------------------------------------------------- //
        // Read FASCN
        // ---------------------------------------------------------- //
        //PivUtilInterface pivUtil = new PivUtil();
        FASCN = getPivUtil().getFASCN(channel);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readFASCN total time: "
                + totalTime);


        return FASCN;

    }

    @Override
    public ChuidBean readChuid(CardChannel channel) throws MiddleWareException {

        LoggerWrapper.entering("MiddleWareServerImpl", "readChuid");

        ChuidBean chuid = new ChuidBean();
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readChuid start time: "
                + startTime);


        // ---------------------------------------------------------- //
        // Read CHUID
        // ---------------------------------------------------------- //
        ManageCHUID mngCHUID = new ManageCHUID();
        chuid = mngCHUID.readChuid(channel);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readChuid total time: "
                + totalTime);

        

        return chuid;



    }

    //CIN/IIN/CUID/BAP
    @Override
    public String readCIN(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readCIN");
        String retValue = "";
       

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCIN start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Read CIN
        // ---------------------------------------------------------- //
        //PivUtilInterface pivUtil = new PivUtil();
        retValue = getPivUtil().readCIN(channel);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCIN total time: "
                + totalTime);

        
        return retValue.toUpperCase();
    }

    @Override
    public String readIIN(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readIIN");
        String retValue = "";
       
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readIIN start time: "
                + startTime);


        // ---------------------------------------------------------- //
        // Read IIN
        // ---------------------------------------------------------- //   
        
        retValue = getPivUtil().readIIN(channel);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readIIN total time: "
                + totalTime);

        
        return retValue.toUpperCase();
    }

    @Override
    public String readCUID(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readCUID");
        String retValue = "";

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCUID start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Read CUID
        // ---------------------------------------------------------- //
        //PivUtilInterface pivUtil = new PivUtil();
        retValue = getPivUtil().readCUID(channel);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCUID total time: "
                + totalTime);

       
        return retValue.toUpperCase();
    }

    @Override
    public String readBAP(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readBAP");
        String retValue = "";
       

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readBAP start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Read BAP
        // ---------------------------------------------------------- //
        //PivUtilInterface pivUtil = new PivUtil();
        retValue = getPivUtil().readBAP(channel);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readBAP total time: "
                + totalTime);

        
        return retValue.toUpperCase();
    }

    @Override
    public String readATR(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readATR");
        String retValue = "";


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readATR start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Read ATR
        // ---------------------------------------------------------- //
        retValue = cmnUtil.arrayToHex(channel.getCard().getATR().getBytes());

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readATR total time: "
                + totalTime);

        
        return retValue.toUpperCase();
    }

    @Override
    public String readCardModel(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readCardModel");
        String retValue = "";
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCardModel start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Read BAP
        // ---------------------------------------------------------- //
        //PivUtilInterface pivUtil = new PivUtil();
        retValue = getPivUtil().readBAP(channel);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCardModel total time: "
                + totalTime);

        
        return retValue.toUpperCase();
    }

    @Override
    public CardInfoBean readCardInfo(CardChannel channel) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "readCardInfo");
        CardInfoBean cardInfo = new CardInfoBean();
        String retValue = null;
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCardInfo start time: "
                + startTime);


        cmnUtil = new CommonUtil();
        //PivUtilInterface pivUtil = new PivUtil();

        //Read CIN & set value of credential Bean
        retValue = getPivUtil().readCIN(channel);
        cardInfo.setCIN(retValue);

        //Read IIN & set value of credential Bean
        retValue = getPivUtil().readIIN(channel);
        cardInfo.setIIN(retValue);

        //Read CUID & set value of credential Bean
        retValue = getPivUtil().readCUID(channel);
        cardInfo.setCUID(retValue);

        //Read BAP & set value of credential Bean
        retValue = getPivUtil().readBAP(channel);
        cardInfo.setBAP(retValue);

        //read ATR
        cardInfo.setATR(cmnUtil.arrayToHex(channel.getCard().getATR().getBytes()));

        //read Card Model
        cardInfo.setCardModel("");

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCardInfo total time: "
                + totalTime);

        

        return cardInfo;
    }
    
    /***************************************************************************************************/
    
    
    
    /************************************ User certificates ********************************************/
       @Override
    public UserCertificates readUserCertificates(CardChannel channel) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "readUserCertificates");

        final CardChannel cardChannel = channel;
        UserCertificates userCert = new UserCertificates();

        userCert = (UserCertificates) AccessController.doPrivileged(new PrivilegedAction() {

            @Override
            public Object run() {
                CertificateBean certBean = new CertificateBean();
                UserCertificates userCert = new UserCertificates();
                

                // ---------------------------------------------------------- //
                // LOGGING INFORMATON
                // ---------------------------------------------------------- //
                long startTime = System.currentTimeMillis();
                LoggerWrapper.info(client
                        + " MiddleWareServerImpl::readUserCertificates start time: "
                        + startTime);

                ReadCertificate rc = new ReadCertificate();
                // ---------------------------------------------------------- //
                // Read pivAuthentication9A Certificate
                // ---------------------------------------------------------- //  
                try {
                    certBean = rc.readCert(cardChannel, UcmsMiddlewareConstants.pivAuthentication9A);
                } catch (MiddleWareException ex) {
                    ex.printStackTrace();
                }
                userCert.setPivAuthentication9A(certBean);
                try {
                    certBean = rc.readCert(cardChannel, UcmsMiddlewareConstants.digSignature9C);
                } catch (MiddleWareException ex) {
                    ex.printStackTrace();
                }
                userCert.setDigSignature9C(certBean);
                try {
                    certBean = rc.readCert(cardChannel, UcmsMiddlewareConstants.keyManagement9D);
                } catch (MiddleWareException ex) {
                    ex.printStackTrace();
                }
                userCert.setKeyManagement9D(certBean);
                try {
                    certBean = rc.readCert(cardChannel, UcmsMiddlewareConstants.cardAuthentication9E);
                } catch (MiddleWareException ex) {
                    ex.printStackTrace();
                }
                userCert.setCardAuthentication9E(certBean);
                
                // ---------------------------------------------------------- //
                // LOGGING INFORMATON
                // ---------------------------------------------------------- //
                long stopTime = System.currentTimeMillis();
                long totalTime = stopTime - startTime;
                LoggerWrapper.info(client
                        + " MiddleWareServerImpl::readUserCertificates total time: "
                        + totalTime);

                
                return userCert;
            }
        });
        return userCert;
    }
  
     @Override
    public CertificateBean readCertificate(CardChannel channel, String CertificateType) throws MiddleWareException {
        final CardChannel cardChannel = channel;
        final String certType = CertificateType;
        CertificateBean certBean = new CertificateBean();

        
        LoggerWrapper.entering("MiddleWareServerImpl", "readCertificate");
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readCertificate start time: "
                + startTime);
        
        //certBean = (CertificateBean) AccessController.doPrivileged(new PrivilegedAction() {
        Object returnObject = AccessController.doPrivileged(new PrivilegedAction() {
            @Override
            public Object run() {
                CertificateBean certBean = new CertificateBean();
                ErrorMessage errMsg = new ErrorMessage();    

                ReadCertificate rc = new ReadCertificate();
                // ---------------------------------------------------------- //
                // Read pivAuthentication9A Certificate
                // ---------------------------------------------------------- //          
                try {
                    certBean = rc.readCert(cardChannel, certType);
                } catch (MiddleWareException ex) {
                    errMsg.setErrorCode("-1");
                    errMsg.setErrorMsg(ex.getMessage());
                    return errMsg; 
                    
                }             

                
                return certBean;
            }
        });
        
        //Check if the response Object is of type Error else set the exception
        if (null != returnObject && (returnObject instanceof ErrorMessage)) {
            ErrorMessage em = (ErrorMessage) returnObject;
            throw new MiddleWareException(em.getErrorMsg());
        } else if (null != returnObject && (returnObject instanceof CertificateBean)) {
            certBean = (CertificateBean) returnObject;
        } else {
            throw new MiddleWareException("The return object of the code is not developed properly");
        }

            
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readUserCertificates total time: "
                + totalTime);
                
                
                
        return certBean;
    }
    
    /****************************************************************************************************/ 
    
     
     
    /************************ generate User CSR/Certificate  *******************************************/    
    @Override
    public UserCSR generateUserCSR(CardChannel channel, String adminKey, String localPin, UserCSR userCSR) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "generateUserCSR");
        UserCSR retUserCSR = new UserCSR();
        List requiredValueMissing = new ArrayList(); 
        
        
        String certCSR;
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::generateUserCSR start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Diversify Admin Key
        // ---------------------------------------------------------- //
        cmnUtil = new CommonUtil();
        byte[] adminKeyByte = cmnUtil.hex1ToByteArray(adminKey);
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);

        boolean pinPolicyVerified=false;
        // ---------------------------------------------------------- //
        // Verify Local PIN Length
        // ---------------------------------------------------------- //

        pinPolicyVerified =verifyPinPolicy(channel,localPin,UcmsMiddlewareConstants.LOCAL_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }

        FascnBean fascn = new FascnBean();
        CardReaderValidator validator = new CardReaderValidator();            
        // ---------------------------------------------------------- //
        // Get FASCN & Validate Required Values
        // ---------------------------------------------------------- //
        fascn = userCSR.getFascn();
        if (fascn != null) {
            //check required values
            requiredValueMissing = validator.validateFASCN(fascn);
            if (!requiredValueMissing.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Required Value Missing (FASCN)");
                sb.append(requiredValueMissing.toString());
                throw new MiddleWareException(sb.toString());
            }
        }

        CSRBean certBean = new CSRBean();
        GenCertificate gencert = new GenCertificate();
        // ---------------------------------------------------------- //
        // Generate Certificate pivAuthentication9A
        // ---------------------------------------------------------- //
        
        certBean = userCSR.getReqCSRPivAuthentication9A();
        if (certBean != null) {
            //check required values
            requiredValueMissing = validator.validateUserCertificate(certBean);
            if (!requiredValueMissing.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Required Value Missing (pivAuthentication9A)");
                sb.append(requiredValueMissing.toString());
                throw new MiddleWareException(sb.toString());
            }

            certCSR = gencert.generateCSR(channel, adminKeyByte, diversifiedAdminKey, localPin, maxPinLength, certBean, fascn);
            retUserCSR.setCsrPivAuthentication9A(certCSR);
        }
         
        // ---------------------------------------------------------- //
        // Generate Certificate digSignature9C
        // ---------------------------------------------------------- //
        certBean = userCSR.getReqCSRDigSignature9C();
        if (certBean != null) {
            //check required values
            requiredValueMissing = validator.validateUserCertificate(certBean);
            if(!requiredValueMissing.isEmpty()){
                StringBuilder sb = new StringBuilder();
                sb.append("Required Value Missing (digSignature9C)");
                sb.append(requiredValueMissing.toString());
                throw new MiddleWareException(sb.toString());
            }

            certCSR = gencert.generateCSR(channel, adminKeyByte, diversifiedAdminKey, localPin, maxPinLength, certBean, fascn);
            retUserCSR.setCsrDigSignature9C(certCSR);  
        }

        // ---------------------------------------------------------- //
        // Generate Certificate keyManagement9D
        // ---------------------------------------------------------- //
        certBean = userCSR.getReqCSRKeyManagement9D();
        if (certBean != null) {
            //check required values
            requiredValueMissing = validator.validateUserCertificate(certBean);
            if(!requiredValueMissing.isEmpty()){
                StringBuilder sb = new StringBuilder();
                sb.append("Required Value Missing (keyManagement9D)");
                sb.append(requiredValueMissing.toString());
                throw new MiddleWareException(sb.toString());
            }

            certCSR = gencert.generateCSR(channel, adminKeyByte, diversifiedAdminKey, localPin, maxPinLength, certBean, fascn);
            retUserCSR.setCsrKeyManagement9D(certCSR);  
        }

        // ---------------------------------------------------------- //
        // Generate Certificate cardAuthentication9E
        // ---------------------------------------------------------- //
        certBean = userCSR.getReqCSRCardAuthentication9E();
        if (certBean != null) {
            //check required values
            requiredValueMissing = validator.validateUserCertificate(certBean);
            if(!requiredValueMissing.isEmpty()){
                StringBuilder sb = new StringBuilder();
                sb.append("Required Value Missing (cardAuthentication9E)");
                sb.append(requiredValueMissing.toString());
                throw new MiddleWareException(sb.toString());
            }

            certCSR = gencert.generateCSR(channel, adminKeyByte, diversifiedAdminKey, localPin, maxPinLength, certBean, fascn);
            retUserCSR.setCsrCardAuthentication9E(certCSR);  
        }

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::generateUserCSR total time: "
                + totalTime);

        

        return retUserCSR;
    }
    
    @Override
    public UserCSR generateUserCertificate(CardChannel channel, UserCSR userCSR, String inURL,String caType) throws MiddleWareException{
        LoggerWrapper.entering("MiddleWareServerImpl", "generateUserCertificate");
        byte[] certificate = null;
        String certCSR ="";
        cmnUtil = new CommonUtil();
        UserCSR retUserCSR = new UserCSR();
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::generateUserCertificate start time: "
                + startTime);


        GenCertificate gencert = new GenCertificate();
        // ---------------------------------------------------------- //
        // Generate Certificate pivAuthentication9A
        // ---------------------------------------------------------- //
        certCSR = userCSR.getCsrPivAuthentication9A();
        if(!isEmpty(certCSR)){
            certificate = gencert.generateUserCertificate(certCSR, inURL, caType);
            retUserCSR.setRespCertPivAuthentication9A(cmnUtil.arrayToHex(certificate));
        }

        // ---------------------------------------------------------- //
        // Generate Certificate digSignature9C
        // ---------------------------------------------------------- //
        certCSR = userCSR.getCsrDigSignature9C();
        if(!isEmpty(certCSR)){
            certificate = gencert.generateUserCertificate(certCSR, inURL, caType);
            retUserCSR.setRespCertDigSignature9C(cmnUtil.arrayToHex(certificate));
        }

        // ---------------------------------------------------------- //
        // Generate Certificate keyManagement9D
        // ---------------------------------------------------------- //
        certCSR = userCSR.getCsrKeyManagement9D();
        if(!isEmpty(certCSR)){
            certificate = gencert.generateUserCertificate(certCSR, inURL, caType);
            retUserCSR.setRespCertKeyManagement9D(cmnUtil.arrayToHex(certificate));
        }

        // ---------------------------------------------------------- //
        // Generate Certificate cardAuthentication9E
        // ---------------------------------------------------------- //
        certCSR = userCSR.getCsrCardAuthentication9E();
        if(!isEmpty(certCSR)){
            certificate = gencert.generateUserCertificate(certCSR, inURL, caType);
            retUserCSR.setRespCertCardAuthentication9E(cmnUtil.arrayToHex(certificate));
        }

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::generateUserCertificate total time: "
                + totalTime);

        

        return retUserCSR;
    }
    /****************************************************************************************************/ 
    
    /************************ generate Content CSR/Certificate  *******************************************/
    @Override
    public UserContentCSR generateContentCSR(CardChannel channel, CSRBean certBean) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "generateContentCSR");
        
        List requiredValueMissing = new ArrayList(); 
        UserContentCSR userContentCSR = new UserContentCSR();
        
        String certCSR="";
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::generateContentCSR start time: "
                + startTime);


        CardReaderValidator validator = new CardReaderValidator();            

        GenContentCertificate genContentCert = new GenContentCertificate();
        // ---------------------------------------------------------- //
        // Generate Certificate Content certificate
        // ---------------------------------------------------------- //

        if (certBean != null) {
            //check required values
            requiredValueMissing = validator.validateUserCertificate(certBean);
            if (!requiredValueMissing.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Required Value Missing (Content Certificate)");
                sb.append(requiredValueMissing.toString());
                throw new MiddleWareException(sb.toString());
            }

            userContentCSR = genContentCert.generateContentCSR(channel, certBean);

        }



        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::generateContentCSR total time: "
                + totalTime);



        return userContentCSR;
    }
    
    @Override
    public String generateContentCertificate(CardChannel channel, String contentCSR, String inURL,String caType) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "generateContentCertificate");
        byte[] certificate = null;
        String contentCertificate="";
        //ContentCertificateBean contentCertificate = new ContentCertificateBean();

        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::generateContentCertificate start time: "
                + startTime);

        cmnUtil = new CommonUtil();
        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //
        GenContentCertificate genContentcert = new GenContentCertificate();

        certificate = genContentcert.generateContentCertificate(contentCSR, inURL,caType);

        contentCertificate = cmnUtil.arrayToHex(certificate);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::generateContentCertificate total time: "
                + totalTime);



        return contentCertificate;
    }
    /*******************************************************************************************************/
    
    
    
    /******************************************* Personal Information ***************************************/
    
    @Override
    public PersonalInformationBean readPersonalInformation(CardChannel channel, String pin, String pinType) throws MiddleWareException {

        PersonalInformationBean personalInfoBean = new PersonalInformationBean();
        final CardChannel cardChannel = channel;
        final String pinValue = pin;
        final String pinTypeValue = pinType;
        personalInfoBean = (PersonalInformationBean) AccessController.doPrivileged(new PrivilegedAction() {

            @Override
            public Object run() {
                PrintedInfoBean printedInfo = new PrintedInfoBean();
                FacialBean facialBean = new FacialBean();
                PersonalInformationBean personalInfoBean = new PersonalInformationBean();
                FingerPrintBean fingerPrintBean = new FingerPrintBean();
                boolean pinVerified = false;
                SecurityBean securityBean = null;
                // Logging information
                LoggerWrapper.entering("MiddleWareServerImpl", "readPersonalInformation");
                try {

                    // ---------------------------------------------------------- //
                    // LOGGING INFORMATON
                    // ---------------------------------------------------------- //
                    long startTime = System.currentTimeMillis();
                    LoggerWrapper.info(client
                            + " MiddleWareServerImpl::readPersonalInformation start time: "
                            + startTime);


                    // ---------------------------------------------------------- //
                    // Verify Local/Global/Bio
                    // ---------------------------------------------------------- //
                    try{
                        pinVerified = verifyPin(cardChannel, pinValue, pinTypeValue);
                        if (!pinVerified) {
                            throw new MiddleWareException("Invalid PIN.");
                        }
                    } catch(MiddleWareException e){
                        throw new MiddleWareException("Invalid PIN.");
                    }        
                    //PivUtilInterface pivUtil = new PivUtil();
                    // ---------------------------------------------------------- //
                    // Read printed information
                    // ---------------------------------------------------------- //	
                    ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();
                    printedInfo = mngPrintedInfo.readPrintedinfo(cardChannel);
                    personalInfoBean.setPrintedInfo(printedInfo);

                    // ---------------------------------------------------------- //
                    // Read Facial information
                    // ---------------------------------------------------------- //	
                    facialBean = getPivUtil().readFacialinfo(cardChannel);

                    if(facialBean.getFacial_data()!=null){
                        String encodedBytes = new BASE64Encoder().encode(facialBean.getFacial_data());
                        facialBean.setFacialImage(encodedBytes);
                    }
                    personalInfoBean.setFacialBean(facialBean);

                    // ---------------------------------------------------------- //
                    // Read Biometric information
                    // ---------------------------------------------------------- //	
                    fingerPrintBean = getPivUtil().getBioInfo(cardChannel);
                    personalInfoBean.setFingerPrintBean(fingerPrintBean);

                    
                    verifySecudataSignature  verifySignature = new verifySecudataSignature();
                    securityBean = verifySignature.readSecContainer(cardChannel);
                    personalInfoBean.setSecurityBean(securityBean);
                    
                    // ---------------------------------------------------------- //
                    // LOGGING INFORMATON
                    // ---------------------------------------------------------- //
                    long stopTime = System.currentTimeMillis();
                    long totalTime = stopTime - startTime;
                    LoggerWrapper.info(client
                            + " MiddleWareServerImpl::readPersonalInformation total time: "
                            + totalTime);
                } catch (NumberFormatException t){
                     LoggerWrapper.info(" MiddleWareServerImpl::readPersonalInformation error: " + t.getMessage());
                } catch (MiddleWareException me ) {
                    LoggerWrapper.info(" MiddleWareServerImpl::readPersonalInformation error: " + me.getMessage());
                    //throw new MiddleWareException("Invalid PIN.");

                }
                return personalInfoBean;
            }
        });



        return personalInfoBean;
    }

    @Override
    public PrintedInfoBean readPrintedInfo(CardChannel channel, String pin, String pinType) throws MiddleWareException {
        PrintedInfoBean printedInfo = new PrintedInfoBean();
        boolean pinVerified = false;


        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readPrintedInfo");
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readPrintedInfo start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Verify Local/Global/Bio
        // ---------------------------------------------------------- //
        pinVerified = verifyPin(channel, pin, pinType);
        if (!pinVerified) {
            throw new MiddleWareException("Invalid PIN.");
        }

        ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();
        // ---------------------------------------------------------- //
        // Read printed information
        // ---------------------------------------------------------- //			
        printedInfo = mngPrintedInfo.readPrintedinfo(channel);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readPrintedinfo total time: "
                + totalTime);

        
        return printedInfo;
    }

    @Override
    public FacialBean readFacialImage(CardChannel channel, String pin, String pinType) throws MiddleWareException {
        boolean pinVerified = false;
        LoggerWrapper.entering("MiddleWareServerImpl", "readFacialImage");



        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readFacialImage start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Verify Local/Global/Bio
        // ---------------------------------------------------------- //
        pinVerified = verifyPin(channel, pin, pinType);
        if (!pinVerified) {
            throw new MiddleWareException("Invalid PIN.");
        }

        FacialBean facialBean = new FacialBean();
        //PivUtilInterface pivUtil = new PivUtil();
        // ---------------------------------------------------------- //
        // Read facial Information
        // ---------------------------------------------------------- //
        facialBean = getPivUtil().readFacialinfo(channel);
        if (facialBean.getFacial_data() != null) {

            String encodedBytes = new BASE64Encoder().encode(facialBean.getFacial_data());
            facialBean.setFacialImage(encodedBytes);
        }


        return facialBean;
        

    }

    /*******************************************************************************************************/
    
    
    /*********************************** PIN Management Functions ******************************************/ 
    //Local PIN Management Functions
    @Override
    public boolean verifyLocalPin(CardChannel channel, String localPin) throws MiddleWareException {
        
        boolean pinPolicyVerified = false;
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "verifyLocalPin");

        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::verifyLocalPin start time: "
                + startTime);

        //PivUtilInterface pivUtil = new PivUtil();

        //-----------------------------------------------------------//
        //get and verify local pin length
        //-----------------------------------------------------------//
        pinPolicyVerified =verifyPinPolicy(channel,localPin,UcmsMiddlewareConstants.LOCAL_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }




        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //			
        if (getPivUtil().verifyLocalPin(channel, localPin, maxPinLength) == false) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.INVALID_LOCAL_PIN_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::verifyLocalPin total time: "
                + totalTime);

        
        return true;
    }

    @Override
    public boolean changeLocalPin(CardChannel channel, String oldPin, String newPin) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "changeLocalPin");
        boolean pinPolicyVerified = false;

        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::changeLocalPin start time: "
                + startTime);

        //PivUtilInterface pivUtil = new PivUtil();
        //-----------------------------------------------------------//
        //get and verify local pin length
        //-----------------------------------------------------------//
        pinPolicyVerified =verifyPinPolicy(channel,oldPin,UcmsMiddlewareConstants.LOCAL_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }
        pinPolicyVerified =verifyPinPolicy(channel,newPin,UcmsMiddlewareConstants.LOCAL_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }

        // ---------------------------------------------------------- //
        // Change Local PIN
        // ---------------------------------------------------------- //			
        if (getPivUtil().changeLocalPin(channel, oldPin, newPin, maxPinLength) == false) {
            throw new MiddleWareException("Invalid Pin.");
        }

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::changeLocalPin total time: "
                + totalTime);

        
        return true;
    }

    @Override
    public boolean resetLocalPin(CardChannel channel, String unblockPin, String newPin) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "resetLocalPin");
        String retValue;
        boolean pinPolicyVerified = false;



        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::resetLocalPin start time: "
                + startTime);

        //PivUtilInterface pivUtil = new PivUtil();
        //-----------------------------------------------------------//
        //get and verify PUK length
        //-----------------------------------------------------------//
        pinPolicyVerified =verifyPinPolicy(channel,unblockPin,UcmsMiddlewareConstants.PUK_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        } 

        pinPolicyVerified =verifyPinPolicy(channel,newPin,UcmsMiddlewareConstants.LOCAL_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }


        // ---------------------------------------------------------- //
        // rest Local PIN
        // ---------------------------------------------------------- //			
        if (getPivUtil().resetLockPin(channel, unblockPin, newPin, maxPinLength) == false) {
            // Stop communication & return Error
            //CardConnect.stopComm();
            throw new MiddleWareException("Error Resting Local PIN.");
        }

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::resetLocalPin total time: "
                + totalTime);

       
        return true;
    }

    //Global PIN Management Functions
    @Override
    public boolean verifyGlobalPin(CardChannel channel, String globalPin) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "verifyGlobalPin");
        boolean pinPolicyVerified=false;
        String retValue;
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::verifyGlobalPin start time: "
                + startTime);

        //PivUtilInterface pivUtil = new PivUtil();
        //-----------------------------------------------------------//
        //get and verify local pin length
        //-----------------------------------------------------------//
        pinPolicyVerified =verifyPinPolicy(channel,globalPin,UcmsMiddlewareConstants.GLOBAL_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        } 

        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //			
        if (getPivUtil().verifyGlobalPin(channel, globalPin, maxPinLength) == false) {
            // Stop communication & return Error
            //CardConnect.stopComm();
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.INVALID_GLOBAL_PIN_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::verifyGlobalPin total time: "
                + totalTime);

        
        return true;
    }

    @Override
    public boolean changeGlobalPin(CardChannel channel, String oldPin, String newPin) throws MiddleWareException {
        String retValue;
        Integer pinLength;
        boolean pinPolicyVerified = false;
        LoggerWrapper.entering("MiddleWareServerImpl", "changeGlobalPin");



        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::changeGlobalPin start time: "
                + startTime);

        //PivUtilInterface pivUtil = new PivUtil();
        //-----------------------------------------------------------//
        //get and verify local pin length
        //-----------------------------------------------------------//
        pinPolicyVerified =verifyPinPolicy(channel,oldPin,UcmsMiddlewareConstants.GLOBAL_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }    
        pinPolicyVerified =verifyPinPolicy(channel,newPin,UcmsMiddlewareConstants.GLOBAL_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }


        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //

        if (getPivUtil().changeGlobalPin(channel, oldPin, newPin, maxPinLength) == false) {
            // Stop communication & return Error
            //CardConnect.stopComm();
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.INVALID_GLOBAL_PIN_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::changeGlobalPin total time: "
                + totalTime);

        
        return true;
    }

    @Override
    public boolean resetGlobalPin(CardChannel channel, CredentialKeys credentialKeys, String newPin, int globalPinRetry) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "resetGlobalPin");

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + "MiddleWareServerImpl::resetGlobalPin start time: "
                + startTime);
        
        List requiredValueMissing = new ArrayList();
        CardReaderValidator validator = new CardReaderValidator(); 
        // ---------------------------------------------------------- //
        // check required values - Credential Keys
        // ---------------------------------------------------------- //        
        requiredValueMissing = validator.validateCredentialKeys(credentialKeys);
        if (!requiredValueMissing.isEmpty()) {
            StringBuilder sb = new StringBuilder();            
            sb.append(requiredValueMissing.toString());
            throw new MiddleWareException(sb.toString());
        }
        
        // ---------------------------------------------------------- //
        // diversify Credential Keys
        // ---------------------------------------------------------- //
        diversifiedKeys = new DiversifiedCredentialKeys();
        diversifiedKeys = diversifyCredentialKeys(channel, credentialKeys);


        //set values on  InitializationBean
        InitializationBean initbean = new InitializationBean();
        initbean.setGlobalPin(newPin);
        initbean.setGlobalPinPTC(globalPinRetry);


        CardInit sinit = new CardInit(credentialKeys, initbean, diversifiedKeys);

        if (sinit.initCard(channel, "G") == false) {
            throw new MiddleWareException("Error initializing PIV Card.");

        }


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::resetGlobalPin total time: "
                + totalTime);

       
        return true;
    }

    //PUK functions Management Functions
    @Override
    public boolean changePUK(CardChannel channel, String oldPUK, String newPUK) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "changePUK");
        boolean pinPolicyVerified = false;
        String retValue;
        Integer pukLength;
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::changePUK start time: "
                + startTime);



        //PivUtilInterface pivUtil = new PivUtil();
        //-----------------------------------------------------------//
        //get and verify local pin length
        //-----------------------------------------------------------//
        pinPolicyVerified =verifyPinPolicy(channel,oldPUK,UcmsMiddlewareConstants.PUK_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }    

        pinPolicyVerified =verifyPinPolicy(channel,newPUK,UcmsMiddlewareConstants.PUK_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }


        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //			
        if (getPivUtil().changePUK(channel, oldPUK, newPUK, maxPinLength) == false) {

            throw new MiddleWareException("Error Unblocking PIN.");
        }


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::changePUK total time: "
                + totalTime);

       
        return true;

    }

    @Override
    public boolean resetPUK(CardChannel channel, CredentialKeys credentialKeys, String newPUK, int pukRetry) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "resetPUK");

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + "MiddleWareServerImpl::resetPUK start time: "
                + startTime);

        
        List requiredValueMissing = new ArrayList();
        CardReaderValidator validator = new CardReaderValidator(); 
        // ---------------------------------------------------------- //
        // check required values - Credential Keys
        // ---------------------------------------------------------- //        
        requiredValueMissing = validator.validateCredentialKeys(credentialKeys);
        if (!requiredValueMissing.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(requiredValueMissing.toString());
            throw new MiddleWareException(sb.toString());
        }

        // ---------------------------------------------------------- //
        // diversify Credential Keys
        // ---------------------------------------------------------- //
        diversifiedKeys = new DiversifiedCredentialKeys();
        diversifiedKeys = diversifyCredentialKeys(channel, credentialKeys);



        //set values on  InitializationBean
        InitializationBean initbean = new InitializationBean();
        initbean.setUnblockPin(newPUK);
        initbean.setUnblockPinPTC(pukRetry);


        CardInit sinit = new CardInit(credentialKeys, initbean, diversifiedKeys);

        if (sinit.initCard(channel, "P") == false) {
            throw new MiddleWareException("Error initializing PIV Card.");

        }


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::resetPUK total time: "
                + totalTime);

        
        return true;
    }

    /*******************************************************************************************************/
    
    
    /******************************card personalization*****************************************************/
    
    @Override
    public List cardPersonalization(CardChannel channel, CredentialKeys credentialKeys, PersonalizationBean persoBean) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "cardPersonalization");
        ChuidBean chuid = new ChuidBean();
        PrintedInfoBean printedInfo = new PrintedInfoBean();
        FacialImageBean facialImage = new FacialImageBean();
        UserFingerPintBean userFingerPrint = new UserFingerPintBean();        
        List errorList = new ArrayList();
        List requiredValueMissing = new ArrayList(); 
        byte[] chuidData = null;
        byte[] printedInfoData = null;
        byte[] facialImageData = null;
        byte[] fingerPrint = null;
        boolean returnValue = false;
        ContentCertificateBean contentBean = new ContentCertificateBean();
        
        CardReaderValidator validator = new CardReaderValidator();
                    
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + "MiddleWareServerImpl::cardPersonalization start time: "
                    + startTime);

        // ---------------------------------------------------------- //
        // Diversify Admin Key
        // ---------------------------------------------------------- //
        cmnUtil = new CommonUtil();
        byte[] adminKeyByte = credentialKeys.getAdminKey();
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);


        ManageCHUID mngCHUID = new ManageCHUID();
        // ---------------------------------------------------------- //
        // Write CHUID
        // ---------------------------------------------------------- //            
        chuid = persoBean.getChuid();
        if (chuid != null) {
            //check required values
            requiredValueMissing = validator.validateCHUID(chuid);
            if (!requiredValueMissing.isEmpty()) {
                throw new MiddleWareException("Personalization - Required Value Missing - " + requiredValueMissing.toString());
            } 
            
            //write CHUID to Card
            returnValue = mngCHUID.writeChuid(channel, chuid, chuid.getSignatureDate(), credentialKeys.getAdminKey(), diversifiedAdminKey, chuid.getAlgoType());
            if (!returnValue) {
                throw new MiddleWareException("Personalization - Error writing CHUID.");
            }
            chuidData = mngCHUID.chuidDataSecu;
            
        }


        ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();
        // ---------------------------------------------------------- //
        // Write Printed Info
        // ---------------------------------------------------------- //   
        printedInfo = persoBean.getPrintInfo();
        if (printedInfo != null) {
            //check required values
            requiredValueMissing = validator.validatePrintedInfo(printedInfo);
            if (!requiredValueMissing.isEmpty()) {
                throw new MiddleWareException("Personalization - Required Value Missing - " + requiredValueMissing.toString());
            } 
            
            // write printed information on card
            returnValue = mngPrintedInfo.writePrintedinfo(channel, adminKeyByte, diversifiedAdminKey, printedInfo.getName(),
                printedInfo.getAffiliation(), printedInfo.getExpirationDate(),
                printedInfo.getCardSerialNumber(), printedInfo.getIssuerID(),
                printedInfo.getOrganisationLine1(),
                printedInfo.getOrganisationLine2());
            
            printedInfoData = mngPrintedInfo.printedInfoDataSecu;
            
            if (!returnValue) {
                throw new MiddleWareException("Personalization - Error writing Printed Info.");
            }
        }
        


        //PivUtilInterface pivUtil = new PivUtil();
        // ---------------------------------------------------------- //
        // Write Facial Image
        // ---------------------------------------------------------- //  
        facialImage = persoBean.getFacialImage();

        if (facialImage != null) {
            //check required values
            requiredValueMissing = validator.validateFacialImage(facialImage);
            if (!requiredValueMissing.isEmpty()) {
                throw new MiddleWareException("Personalization - Required Value Missing - " + requiredValueMissing.toString());
            }

            byte[] byteimage = cmnUtil.hex1StringToByteArray(facialImage.getFacialImage());
            if (byteimage.length > 12000) {
                throw new MiddleWareException("Personalization - Size of Picture is to big.");
            }

            if (facialImage.getCreator().length() > UcmsMiddlewareConstants.FACIAL_IMAGE_CREATOR_LENGTH) {
                throw new MiddleWareException("Personalization - Creator Length can not be more than " + UcmsMiddlewareConstants.FACIAL_IMAGE_CREATOR_LENGTH);
            }

            //read FASCN & convert fascn to String Value
            byte[] FASCN = getPivUtil().getFASCN(channel);
            String fascn = null;
            fascn = cmnUtil.arrayToHex(FASCN);

            returnValue = getPivUtil().writeFacialinfo(channel, adminKeyByte, diversifiedAdminKey, byteimage, facialImage.getCreationDate(), facialImage.getExpirationDate(),
                    facialImage.getValidFromDate(), facialImage.getCreator(), fascn, facialImage.getSignatureDate(), facialImage.getContentCertificate(), facialImage.getPrivateKey(), facialImage.getAlgoType());

            facialImageData = getPivUtil().getfacialImageDataSecu();
            
            
            if (!returnValue) {
                throw new MiddleWareException("Personalization - Error writing Facial Image.");
            }
        }
        


        
        // ---------------------------------------------------------- //
        // Write Finger Print
        // ---------------------------------------------------------- // 
        userFingerPrint = persoBean.getFingerPrints();
        if (userFingerPrint != null) {
            // ---------------------------------------------------------- //
            // Enroll rightThumb
            // ---------------------------------------------------------- //
            if (userFingerPrint.getRightThumb() != null) {
                fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getRightThumb());
                returnValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.rightThumb, UcmsMiddlewareConstants.subImpType);
                if (!returnValue) {
                    throw new MiddleWareException("Personalization - Error enrolling Right Thumb.");
                }
            }

            // ---------------------------------------------------------- //
            // Enroll rightIndex
            // ---------------------------------------------------------- //
            if (userFingerPrint.getRightIndex() != null) {
                fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getRightIndex());
                returnValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.rightIndex, UcmsMiddlewareConstants.subImpType);
                if (!returnValue) {
                    throw new MiddleWareException("Personalization - Error enrolling Right Index.");
                }

            }
            
            // ---------------------------------------------------------- //
            // Enroll rightMiddle
            // ---------------------------------------------------------- //
            if (userFingerPrint.getRightMiddle() != null) {
                fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getRightMiddle());
                returnValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.rightMiddle, UcmsMiddlewareConstants.subImpType);
                if (!returnValue) {
                    throw new MiddleWareException("Personalization - Error enrolling Right Middle.");
                }
            }

            // ---------------------------------------------------------- //
            // Enroll rightRing
            // ---------------------------------------------------------- //
            if (userFingerPrint.getRightRing() != null) {
                fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getRightRing());
                returnValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.rightRing, UcmsMiddlewareConstants.subImpType);
                if (!returnValue) {
                    throw new MiddleWareException("Personalization - Error enrolling Right Ring.");
                }
            }
                
            // ---------------------------------------------------------- //
            // Enroll rightLittle
            // ---------------------------------------------------------- //
            if (userFingerPrint.getRightLittle() != null) {
                fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getRightLittle());
                returnValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.rightLittle, UcmsMiddlewareConstants.subImpType);
                if (!returnValue) {
                    throw new MiddleWareException("Personalization - Error enrolling Right Little.");
                }
            }
            
            // ---------------------------------------------------------- //
            // Enroll leftThumb
            // ---------------------------------------------------------- //
            if (userFingerPrint.getLeftThumb() != null) {
                fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getLeftThumb());
                returnValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.leftThumb, UcmsMiddlewareConstants.subImpType);
                if (!returnValue) {
                    throw new MiddleWareException("Personalization - Error enrolling Left Thumb.");
                }
            }
            
            // ---------------------------------------------------------- //
            // Enroll leftIndex
            // ---------------------------------------------------------- //
            if (userFingerPrint.getLeftIndex() != null) {
                fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getLeftIndex());
                returnValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.leftIndex, UcmsMiddlewareConstants.subImpType);
                if (!returnValue) {
                    throw new MiddleWareException("Personalization - Error enrolling Left Index.");
                }
            }
            
            // ---------------------------------------------------------- //
            // Enroll leftMiddle
            // ---------------------------------------------------------- //
            if (userFingerPrint.getLeftMiddle() != null) {
                fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getLeftMiddle());
                returnValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.leftMiddle, UcmsMiddlewareConstants.subImpType);
                if (!returnValue) {
                    throw new MiddleWareException("Personalization - Error enrolling Left Middle.");
                }
            }
            
            // ---------------------------------------------------------- //
            // Enroll leftRing
            // ---------------------------------------------------------- //
            if (userFingerPrint.getLeftRing() != null) {
                fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getLeftRing());
                returnValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.leftRing, UcmsMiddlewareConstants.subImpType);
                if (!returnValue) {
                    throw new MiddleWareException("Personalization - Error enrolling Left Ring.");
                }
            }
            
            // ---------------------------------------------------------- //
            // Enroll leftLittle
            // ---------------------------------------------------------- //
            if (userFingerPrint.getLeftLittle() != null) {
                fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getLeftLittle());
                returnValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.leftLittle, UcmsMiddlewareConstants.subImpType);
                if (!returnValue) {
                    throw new MiddleWareException("Personalization - Error enrolling Left Little.");
                }
            }
         }

        LoadCertificate loadCertificate = new LoadCertificate();
        // ---------------------------------------------------------- //
        // Write certificate pivAuthentication9A
        // ---------------------------------------------------------- //  
        if (persoBean.getPivAuthentication9A() != null && persoBean.getPivAuthentication9A().length>0 ) {
            byte[] pivAuthentication9A = persoBean.getPivAuthentication9A();
            
            // Check certificate Length            
            if (pivAuthentication9A.length > UcmsMiddlewareConstants.certficateLength) {
                throw new MiddleWareException("Personalization - PivAuthentication 9A - " + ExceptionMessages.INVALID_CERTIFICATE_LENGTH);
            }
            
            //write certificate
            returnValue = loadCertificate.loadCert(channel, adminKeyByte, diversifiedAdminKey, pivAuthentication9A, UcmsMiddlewareConstants.pivAuthentication9A);
            if (!returnValue) {
                throw new MiddleWareException("Personalization - Error writing  PIVAuthentication9A Certificate.");
            }
        }
        
        // ---------------------------------------------------------- //
        // Write certificate digSignature9C
        // ---------------------------------------------------------- //  
        if (persoBean.getDigSignature9C() != null && persoBean.getDigSignature9C().length > 0 ) {
            byte[] digSignature9C = persoBean.getDigSignature9C();
            
            // Check certificate Length            
            if (digSignature9C.length > UcmsMiddlewareConstants.certficateLength) {
                throw new MiddleWareException("Personalization - DigSignature 9C - " + ExceptionMessages.INVALID_CERTIFICATE_LENGTH);
            }
            //write certificate
            returnValue = loadCertificate.loadCert(channel, adminKeyByte, diversifiedAdminKey, digSignature9C, UcmsMiddlewareConstants.digSignature9C);
            if (!returnValue) {
                throw new MiddleWareException("Personalization - Error writing  DigSignature9C Certificate.");
            }
        }
        // ---------------------------------------------------------- //
        // Write certificate keyManagement9D
        // ---------------------------------------------------------- //  
        if (persoBean.getKeyManagement9D() != null && persoBean.getKeyManagement9D().length>0) {
            byte[] keyManagement9D = persoBean.getKeyManagement9D();
            
            // Check certificate Length            
            if (keyManagement9D.length > UcmsMiddlewareConstants.certficateLength) {
                throw new MiddleWareException("Personalization - KeyManagement 9C - " + ExceptionMessages.INVALID_CERTIFICATE_LENGTH);
            }
            //write certificate
            returnValue = loadCertificate.loadCert(channel, adminKeyByte, diversifiedAdminKey, keyManagement9D, UcmsMiddlewareConstants.keyManagement9D);
            if (!returnValue) {
                throw new MiddleWareException("Personalization - Error writing  KeyManagement 9D Certificate.");
            }
        }

        // ---------------------------------------------------------- //
        // Write certificate cardAuthentication9E
        // ---------------------------------------------------------- //  
        if (persoBean.getCardAuthentication9E() != null && persoBean.getCardAuthentication9E().length>0) {
            byte[] cardAuthentication9E = persoBean.getCardAuthentication9E();
            
            // Check certificate Length            
            if (cardAuthentication9E.length > UcmsMiddlewareConstants.certficateLength) {
                throw new MiddleWareException("Personalization - CardAuthentication 9E - " + ExceptionMessages.INVALID_CERTIFICATE_LENGTH);
            }
            //write certificate
            returnValue = loadCertificate.loadCert(channel, adminKeyByte, diversifiedAdminKey, cardAuthentication9E, UcmsMiddlewareConstants.cardAuthentication9E);

            if (!returnValue) {
                throw new MiddleWareException("Personalization - Error writing  CardAuthentication 9E Certificate.");
            }
        }
        
        // ---------------------------------------------------------- //
        // Load security Container
        // ---------------------------------------------------------- //  
        contentBean = chuid.getContentCertificateBean();
        LoadSecurityCont secuCont = new LoadSecurityCont();
        secuCont.loadSecCont(channel,printedInfoData,chuidData,fingerPrint,facialImageData,contentBean.getPrivateKey(),contentBean.getContentCertificate(),chuid.getSignatureDate(),
               adminKeyByte,diversifiedAdminKey,chuid.getAlgoType());
        
        
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::writeChuid total time: "
                + totalTime);

        
        return errorList;
    }
        
    @Override
    public List cardInitialization(CardChannel channel, CredentialKeys credentialKeys, InitializationBean initvalue) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "cardInitialization");
        List errorList = new ArrayList();
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + "MiddleWareServerImpl::cardInitialization start time: "
                + startTime);

        List requiredValueMissing = new ArrayList();
        CardReaderValidator validator = new CardReaderValidator(); 
        // ---------------------------------------------------------- //
        // check required values - Credential Keys
        // ---------------------------------------------------------- //        
        requiredValueMissing = validator.validateCredentialKeys(credentialKeys);
        if (!requiredValueMissing.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(requiredValueMissing.toString());
            throw new MiddleWareException(sb.toString());
        }
        
        
        diversifiedKeys = new DiversifiedCredentialKeys();
        diversifiedKeys = diversifyCredentialKeys(channel, credentialKeys);


        //diversify Initializaion keys
        if (initvalue.getDiversifyAdminKey().equalsIgnoreCase("Y")) {
            initvalue.setAdminKey(cmnUtil.arrayToHex(keyDiversify(channel, initvalue.getAdminKey())));
        }
        if (initvalue.getDiversifyGPKeys().equalsIgnoreCase("Y") && initvalue.getEncKey().length > 0) {
            initvalue.setEncKey(cmnUtil.arrayToHex(keyDiversify(channel, initvalue.getEncKey())));
            initvalue.setMacKey(cmnUtil.arrayToHex(keyDiversify(channel, initvalue.getMacKey())));
            initvalue.setDekKey(cmnUtil.arrayToHex(keyDiversify(channel, initvalue.getDekKey())));
        }


        //initvalue=initGPKeys(channel,initvalue);
        //credentialKeys=setCredentialKeys(channel,credentialKeys);

        // ---------------------------------------------------------- //
        // create instance & initialize the card
        // ---------------------------------------------------------- //

        CardInit sinit = new CardInit(credentialKeys, initvalue, diversifiedKeys);

        if (sinit.initCard(channel, "A") == false) {
            throw new MiddleWareException("Error initializing PIV Card.");
        }


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::cardInitialization total time: "
                + totalTime);

        
        return errorList;

    }
    
    @Override
    public boolean writeChuid(CardChannel channel, ChuidBean chuid, String adminKey, String algoType) throws MiddleWareException {

        LoggerWrapper.entering("MiddleWareServerImpl", "writeChuid");



        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + "MiddleWareServerImpl::writeChuid start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Diversify Admin Key
        // ---------------------------------------------------------- //
        cmnUtil = new CommonUtil();
        byte[] adminKeyByte = cmnUtil.hex1ToByteArray(adminKey);
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);

        // ---------------------------------------------------------- //
        // Write CHUID
        // ---------------------------------------------------------- //
        ManageCHUID mngCHUID = new ManageCHUID();
      
            if (mngCHUID.writeChuid(channel, chuid, chuid.getSignatureDate(), adminKeyByte, diversifiedAdminKey, algoType) == false) {
                throw new MiddleWareException("Error writing CHUID.");
            }
      
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::writeChuid total time: "
                + totalTime);

        
        return true;
    }

    @Override
    public boolean writePrintedInfo(CardChannel channel, PrintedInfoBean printInfo, String adminKey) throws MiddleWareException {
        boolean returnValue = false;

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "writePrintedInfo");



        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::writePrintedInfo start time: "
                + startTime);



        //PivUtilInterface pivUtil = new PivUtil();

        cmnUtil = new CommonUtil();
        byte[] adminKeyByte = cmnUtil.hex1ToByteArray(adminKey);

        //diversify PIV_admin_key
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);

        ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();

        // write printed information on card
        returnValue = mngPrintedInfo.writePrintedinfo(channel, adminKeyByte, diversifiedAdminKey, printInfo.getName(),
                printInfo.getAffiliation(), printInfo.getExpirationDate(),
                printInfo.getCardSerialNumber(), printInfo.getIssuerID(),
                printInfo.getOrganisationLine1(),
                printInfo.getOrganisationLine2());

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::WritePrinted Info total time: "
                + totalTime);

        
        return returnValue;

    }

    @Override
    public boolean writeFacialImage(CardChannel channel, FacialImageBean facialImage, String adminKey, String algoType) throws MiddleWareException {
        String stogo;
        boolean retValue = false;
        LoggerWrapper.entering("MiddleWareServerImpl", "writeFacialImage");



        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::writeFacialImage start time: "
                + startTime);


        cmnUtil = new CommonUtil();
        byte[] byteimage = facialImage.getFacialData();
        if (byteimage.length > 12000) {
            throw new MiddleWareException("Size of Picture is to big.");
        }

        if (facialImage.getCreator().length() > UcmsMiddlewareConstants.FACIAL_IMAGE_CREATOR_LENGTH) {
            throw new MiddleWareException("Creator Length can not be more than " + UcmsMiddlewareConstants.FACIAL_IMAGE_CREATOR_LENGTH);
        }

        //stogo = cmnUtil.arrayToHex(byteimage);

        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //
        //PivUtilInterface pivUtil = new PivUtil();

        //convert adminKey to hex
        byte[] adminKeyByte = cmnUtil.hex1ToByteArray(adminKey);

        //diversify adminKey
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);
        byte[] FASCN = getPivUtil().getFASCN(channel);



        String fascn = null;
        fascn = cmnUtil.arrayToHex(FASCN);
        retValue = getPivUtil().writeFacialinfo(channel, adminKeyByte, diversifiedAdminKey, byteimage, facialImage.getCreationDate(), facialImage.getExpirationDate(),
                facialImage.getValidFromDate(), facialImage.getCreator(), fascn, facialImage.getSignatureDate(), facialImage.getContentCertificate(), facialImage.getPrivateKey(), algoType);


        return retValue;
        
    }

    @Override
    public boolean loadCertificate(CardChannel channel, String adminKey, byte[] certificate, String certType) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "loadCertificate");
        boolean returnStatus = false;



        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::loadCertificate start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Check certificate Length
        // ---------------------------------------------------------- //
        if (certificate.length > UcmsMiddlewareConstants.certficateLength) {
            // stop communication & return success            
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.INVALID_CERTIFICATE_LENGTH);
            throw new MiddleWareException(sb.toString());
        }

        // ---------------------------------------------------------- //
        // Diversify Admin Key
        // ---------------------------------------------------------- //
        cmnUtil = new CommonUtil();
        byte[] adminKeyByte = cmnUtil.hex1ToByteArray(adminKey);
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);



        // ---------------------------------------------------------- //
        // Load Certificate
        // ---------------------------------------------------------- //
        LoadCertificate loadCertificate = new LoadCertificate();
        returnStatus = loadCertificate.loadCert(channel, adminKeyByte, diversifiedAdminKey, certificate, certType);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::loadCertificate total time: "
                + totalTime);

        

        return returnStatus;
    }

     @Override
    public List loadUserCertificate(CardChannel channel, String adminKey, LoadUserCertificates userCertificate) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "loadUserCertificate");
        boolean returnValue = false;
        List errorList = new ArrayList();
        
        try {

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::loadUserCertificate start time: "
                    + startTime);

            // ---------------------------------------------------------- //
            // Diversify Admin Key
            // ---------------------------------------------------------- //
            cmnUtil = new CommonUtil();
            byte[] adminKeyByte = cmnUtil.hex1ToByteArray(adminKey);
            byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);
            
            
            LoadCertificate loadCertificate = new LoadCertificate();
            // ---------------------------------------------------------- //
            // Write certificate pivAuthentication9A
            // ---------------------------------------------------------- //  
            if (userCertificate.getPivAuthentication9A() != null) {
                byte[] pivAuthentication9A = userCertificate.getPivAuthentication9A();
                try {
                    // Check certificate Length            
                    if (pivAuthentication9A.length > UcmsMiddlewareConstants.certficateLength) {
                        errorList.add("pivAuthentication9A " + ExceptionMessages.INVALID_CERTIFICATE_LENGTH);
                    }
                    
                    //write certificate
                    returnValue = loadCertificate.loadCert(channel, adminKeyByte, diversifiedAdminKey, pivAuthentication9A, UcmsMiddlewareConstants.pivAuthentication9A);

                    if (!returnValue) {
                        errorList.add("Error writing pivAuthentication9A Certificate.");
                    }
                } catch (MiddleWareException e) {
                    errorList.add(e.getMessage());
                }
            }
            // ---------------------------------------------------------- //
            // Write certificate digSignature9C
            // ---------------------------------------------------------- //  
            if (userCertificate.getDigSignature9C() != null) {
                byte[] digSignature9C = userCertificate.getDigSignature9C();
                try {
                    // Check certificate Length            
                    if (digSignature9C.length > UcmsMiddlewareConstants.certficateLength) {
                        errorList.add("digSignature9C " + ExceptionMessages.INVALID_CERTIFICATE_LENGTH);
                    }
                    //write certificate
                    returnValue = loadCertificate.loadCert(channel, adminKeyByte, diversifiedAdminKey, digSignature9C, UcmsMiddlewareConstants.digSignature9C);
                    if (!returnValue) {
                        errorList.add("Error writing digSignature9C Certificate.");
                    }

                } catch (MiddleWareException e) {
                    errorList.add(e.getMessage());
                }
            }
            // ---------------------------------------------------------- //
            // Write certificate keyManagement9D
            // ---------------------------------------------------------- //  
            if (userCertificate.getKeyManagement9D() != null) {
                byte[] keyManagement9D = userCertificate.getKeyManagement9D();
                try {
                    // Check certificate Length            
                    if (keyManagement9D.length > UcmsMiddlewareConstants.certficateLength) {
                        errorList.add("keyManagement9D " + ExceptionMessages.INVALID_CERTIFICATE_LENGTH);
                    }
                    //write certificate
                    returnValue = loadCertificate.loadCert(channel, adminKeyByte, diversifiedAdminKey, keyManagement9D, UcmsMiddlewareConstants.keyManagement9D);
                    if (!returnValue) {
                        errorList.add("Error writing keyManagement9D Certificate.");
                    }

                } catch (MiddleWareException e) {
                    errorList.add(e.getMessage());
                }
            }

            // ---------------------------------------------------------- //
            // Write certificate cardAuthentication9E
            // ---------------------------------------------------------- //  
            if (userCertificate.getCardAuthentication9E() != null) {
                byte[] cardAuthentication9E = userCertificate.getCardAuthentication9E();

                try {
                    // Check certificate Length            
                    if (cardAuthentication9E.length > UcmsMiddlewareConstants.certficateLength) {
                        errorList.add("cardAuthentication9E " + ExceptionMessages.INVALID_CERTIFICATE_LENGTH);
                    }
                    //write certificate
                    returnValue = loadCertificate.loadCert(channel, adminKeyByte, diversifiedAdminKey, cardAuthentication9E, UcmsMiddlewareConstants.cardAuthentication9E);

                    if (!returnValue) {
                        errorList.add("Error writing cardAuthentication9E Certificate.");
                    }

                } catch (MiddleWareException e) {
                    errorList.add(e.getMessage());
                }
            }


            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::loadUserCertificate total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }

        return errorList;
    }

     
    /*******************************************************************************************************/
    
    /**************************** Finger Print Function *****************************************************/
    @Override
    public boolean enrollUserFingerPrints(CardChannel channel, UserFingerPintBean userFingerPrint, String adminKey) throws MiddleWareException {
        boolean retValue = false;
        byte[] fingerPrint = null;


        LoggerWrapper.entering("MiddleWareServerImpl", "enrollUserFingerPrints");
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::enrollUserFingerPrints start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Diversify Admin Key
        // ---------------------------------------------------------- //
        cmnUtil = new CommonUtil();
        byte[] adminKeyByte = cmnUtil.hex1ToByteArray(adminKey);
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);
        //byte[] sub_imp_type = {0x00};


        //PivUtil pivUtil = new PivUtil();

        // ---------------------------------------------------------- //
        // Enroll rightThumb
        // ---------------------------------------------------------- //
        if (userFingerPrint.getRightThumb() != null) {
            fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getRightThumb());
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.rightThumb, UcmsMiddlewareConstants.subImpType);
        }

        // ---------------------------------------------------------- //
        // Enroll rightIndex
        // ---------------------------------------------------------- //
        if (userFingerPrint.getRightIndex() != null) {
            fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getRightIndex());
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.rightIndex, UcmsMiddlewareConstants.subImpType);
        }
        // ---------------------------------------------------------- //
        // Enroll rightMiddle
        // ---------------------------------------------------------- //
        if (userFingerPrint.getRightMiddle() != null) {
            fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getRightMiddle());
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.rightMiddle, UcmsMiddlewareConstants.subImpType);
        }

        // ---------------------------------------------------------- //
        // Enroll rightRing
        // ---------------------------------------------------------- //
        if (userFingerPrint.getRightRing() != null) {
            fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getRightRing());
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.rightRing, UcmsMiddlewareConstants.subImpType);
        }
        // ---------------------------------------------------------- //
        // Enroll rightLittle
        // ---------------------------------------------------------- //
        if (userFingerPrint.getRightLittle() != null) {
            fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getRightLittle());
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.rightLittle, UcmsMiddlewareConstants.subImpType);
        }
        // ---------------------------------------------------------- //
        // Enroll leftThumb
        // ---------------------------------------------------------- //
        if (userFingerPrint.getLeftThumb() != null) {
            fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getLeftThumb());
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.leftThumb, UcmsMiddlewareConstants.subImpType);
        }
        // ---------------------------------------------------------- //
        // Enroll leftIndex
        // ---------------------------------------------------------- //
        if (userFingerPrint.getLeftIndex() != null) {
            fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getLeftIndex());
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.leftIndex, UcmsMiddlewareConstants.subImpType);
        }
        // ---------------------------------------------------------- //
        // Enroll leftMiddle
        // ---------------------------------------------------------- //
        if (userFingerPrint.getLeftMiddle() != null) {
            fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getLeftMiddle());
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.leftMiddle, UcmsMiddlewareConstants.subImpType);
        }
        // ---------------------------------------------------------- //
        // Enroll leftRing
        // ---------------------------------------------------------- //
        if (userFingerPrint.getLeftRing() != null) {
            fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getLeftRing());
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.leftRing, UcmsMiddlewareConstants.subImpType);
        }
        // ---------------------------------------------------------- //
        // Enroll leftLittle
        // ---------------------------------------------------------- //
        if (userFingerPrint.getLeftLittle() != null) {
            fingerPrint = cmnUtil.hex1StringToByteArray(userFingerPrint.getLeftLittle());
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, UcmsMiddlewareConstants.leftLittle, UcmsMiddlewareConstants.subImpType);
        }

        
        return retValue;
    }
    
    @Override
    public boolean enrollFingerPrint(CardChannel channel, byte[] fingerPrint, String fingerID, String adminKey) throws MiddleWareException {
        boolean retValue = false;
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "enrollFingerPrint");
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::enrollFingerPrint start time: "
                + startTime);


        cmnUtil = new CommonUtil();
        //convert PIV_admin_key to hex
        byte[] adminKeyByte = cmnUtil.hex1ToByteArray(adminKey);

        //diversify PIV_admin_key
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);


        byte[] sub_imp_type = {0x00};

        //PivUtil pivUtil = new PivUtil();
        try {
            retValue = getPivUtil().updateFingerPrint(channel, adminKeyByte, diversifiedAdminKey, fingerPrint, fingerID, sub_imp_type);

        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        
        return retValue;
    }

    @Override
    public boolean verifyFingerPrint(CardChannel channel, String fingerPrint) throws MiddleWareException {
        byte[] sub_imp_type = {0x00};
        byte[] verrifyFingerPrint=null;
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "verifyFingerPrint");

        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::verifyFingerPrint start time: "
                + startTime);

        cmnUtil = new CommonUtil();
        verrifyFingerPrint = cmnUtil.hex1StringToByteArray(fingerPrint);

        //create instance of utility class
        //PivUtilInterface pivUtil = new PivUtil();

        //call verify finger Print
        if (getPivUtil().verifyFingerPrint(channel, verrifyFingerPrint, sub_imp_type) == false) {

            throw new MiddleWareException("Invalid Finger Print.");
        }

        return true;

        

    }

    /*******************************************************************************************************/
    
    /**************************** Lock Card functions *****************************************************/
    @Override
    public boolean cardLock(CardChannel channel, CredentialKeys credentialKeys) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "CardLock");
        boolean retStatus = false;


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::CardLock start time: "
                + startTime);

        List requiredValueMissing = new ArrayList();
        CardReaderValidator validator = new CardReaderValidator(); 
        // ---------------------------------------------------------- //
        // check required values - Credential Keys
        // ---------------------------------------------------------- //        
        requiredValueMissing = validator.validateCredentialKeys(credentialKeys);
        if (!requiredValueMissing.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(requiredValueMissing.toString());
            throw new MiddleWareException(sb.toString());
        }
        
        
        // ---------------------------------------------------------- //
        // set default value for card manage AID & lock card
        // ---------------------------------------------------------- //
        diversifiedKeys = new DiversifiedCredentialKeys();
        diversifiedKeys = diversifyCredentialKeys(channel, credentialKeys);

        CardUtilInterface lcard = new CardUtil(credentialKeys, diversifiedKeys);
        retStatus = lcard.CardLock(channel);

        if (retStatus == false) {
            // Stop communication & return Error
            throw new MiddleWareException("Unable to Lock Card.");
        }

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::CardLock total time: "
                + totalTime);

        
        return retStatus;
    }

    @Override
    public boolean cardUnlock(CardChannel channel, CredentialKeys credentialKeys) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "cardUnlock");
        boolean retStatus;
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::cardUnlock start time: "
                + startTime);

        List requiredValueMissing = new ArrayList();
        CardReaderValidator validator = new CardReaderValidator(); 
        // ---------------------------------------------------------- //
        // check required values - Credential Keys
        // ---------------------------------------------------------- //        
        requiredValueMissing = validator.validateCredentialKeys(credentialKeys);
        if (!requiredValueMissing.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(requiredValueMissing.toString());
            throw new MiddleWareException(sb.toString());
        }
        
        // ---------------------------------------------------------- //
        // set default value for card manage AID & UNlock card
        // ---------------------------------------------------------- //

        diversifiedKeys = new DiversifiedCredentialKeys();
        diversifiedKeys = diversifyCredentialKeys(channel, credentialKeys);

        CardUtil unlcard = new CardUtil(credentialKeys, diversifiedKeys);
        retStatus = unlcard.CardUnlock(channel);
        if (retStatus == false) {
            throw new MiddleWareException("Unable to UnLock Card.");
        }


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::cardUnlock total time: "
                + totalTime);

        
        return retStatus;
    }

    @Override
    public boolean isCardLocked(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "isCardLocked");
        boolean retStatus = false;
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::isCardLocked start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // set default value for card manage AID & lock card
        // ---------------------------------------------------------- //

        CardUtilInterface lcard = new CardUtil();
        retStatus = lcard.isCardLocked(channel);



        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::isCardLocked total time: "
                + totalTime);

        
        return retStatus;
    }

    /*******************************************************************************************************/
    
    /*********************************** Misc Function *****************************************************/ 
    @Override
    public ContainerInfoBean readContainerInfo(CardChannel channel, String adminKey) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readContainerInfo");
        String retValue;
        ContainerInfoBean containerInfo = new ContainerInfoBean();
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readContainerInfo start time: "
                + startTime);

        DiscoverContainers discContainer = new DiscoverContainers();

        //-----------------------------------------------------------//
        //get and verify local pin length
        //-----------------------------------------------------------//
        cmnUtil = new CommonUtil();

        byte[] adminKeyByte = cmnUtil.hex1ToByteArray(adminKey);

        //diversify PIV_admin_key
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);


        containerInfo = discContainer.getContainerInfo(channel, adminKeyByte, diversifiedAdminKey);


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //

        System.out.println("true");
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readContainerInfo total time: "
                + totalTime);

        
        return containerInfo;
    }
    
    @Override
    public boolean loadSecurityObject(CardChannel channel, ContentCertificateBean contentCertificate,Date signatureDate, String adminKey, String algoType) throws MiddleWareException {
        
        LoggerWrapper.entering("MiddleWareServerImpl", "loadSecurityObject");

        ChuidBean chuid = new ChuidBean();
        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::loadSecurityObject start time: "
                + startTime);


        // ---------------------------------------------------------- //
        // Read CHUID
        // ---------------------------------------------------------- //
        ManageCHUID mngCHUID = new ManageCHUID();
        chuid = mngCHUID.readChuid(channel);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readChuid total time: "
                + totalTime);

        return true;
    }

    @Override
    public MasterKeyCeremonyResultBean masterKeyCeremony(CardChannel channel, MasterKeyCeremonyBean masterKeyBean) throws MiddleWareException {
         MasterKeyCeremonyResultBean masterKeyResult = new MasterKeyCeremonyResultBean();
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "masterKeyCeremony");

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::masterKeyCeremony start time: "
                + startTime);


        MasterKeyCeremony msKey = new MasterKeyCeremony(masterKeyBean);
        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //			
         masterKeyResult = msKey.computeMasterKey() ;
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::masterKeyCeremony total time: "
                + totalTime);

        
        return masterKeyResult;
    }

    @Override
    public boolean verifySecureChannel(CardChannel channel, String secureChannel) throws MiddleWareException {
          
        boolean returnValue = false;
        byte[] cardData;
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "verifySecureChannel");

        

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::verifySecureChannel start time: "
                + startTime);

        //PivUtilInterface pivUtil = new PivUtil();

        //-----------------------------------------------------------//
        //get and verify local pin length
        //-----------------------------------------------------------//
        cardData = getPivUtil().getCardData(channel);

         if (Arrays.equals(cardData, UcmsMiddlewareConstants.SCP03)) {
             if (secureChannel.equalsIgnoreCase("SCP03")){
                 returnValue = true;
             }
         }else{
             if (secureChannel.equalsIgnoreCase("SCP01")){
                 returnValue = true;
             }
         }




        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::verifySecureChannel total time: "
                + totalTime);

        
        return returnValue;
    }
    
    @Override
    public boolean verifyCredentialKeys(CardChannel channel,CredentialKeys credentialKeys, InitializationBean initvalue,String secureChannel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "verifyCredentialKeys");
        
        boolean retStatus;        
        //PivUtil pivUtil = new PivUtil();    
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::verifyCredentialKeys start time: "
                + startTime);
        
        List requiredValueMissing = new ArrayList();
        CardReaderValidator validator = new CardReaderValidator(); 
        // ---------------------------------------------------------- //
        // check required values - Credential Keys
        // ---------------------------------------------------------- //        
        requiredValueMissing = validator.validateCredentialKeys(credentialKeys);
        if (!requiredValueMissing.isEmpty()) {
            StringBuilder sb = new StringBuilder();            
            sb.append(requiredValueMissing.toString());
            throw new MiddleWareException(sb.toString());
        }
        
        // ---------------------------------------------------------- //
        // check required values - Initialization Keys
        // ---------------------------------------------------------- //    
        requiredValueMissing = validator.validateInitValues(initvalue);
        if (!requiredValueMissing.isEmpty()) {
            StringBuilder sb = new StringBuilder();            
            sb.append(requiredValueMissing.toString());
            throw new MiddleWareException(sb.toString());
        }
               
        
        //-----------------------------------------------------------//
        //Diversify Credential Keys
        //-----------------------------------------------------------//    
        diversifiedKeys = new DiversifiedCredentialKeys();
        diversifiedKeys = diversifyCredentialKeys(channel, credentialKeys);
        
        
        CardUtilInterface lcard = new CardUtil();
        //-----------------------------------------------------------//
        //Check if card is locked
        //-----------------------------------------------------------//        
        retStatus = lcard.isCardLocked(channel);
        
        if(retStatus==true){
            // ---------------------------------------------------------- //
            // UNlock card if it was in locked state
            // ---------------------------------------------------------- //
            CardUtil unlcard = new CardUtil(credentialKeys, diversifiedKeys);
            retStatus = unlcard.CardUnlock(channel);
        }else{
            // ---------------------------------------------------------- //
            // Verify GP Keys
            // ---------------------------------------------------------- //
            if (secureChannel.equalsIgnoreCase("SCP03")){
                SecureChannel03 scp03 = new SecureChannel03(credentialKeys,diversifiedKeys);
                scp03.openSecureChannel03(channel, false);
            }else {
                SecureChannel01 scp01 = new SecureChannel01(credentialKeys);
                scp01.openSecureChannel01(channel);
            }           
        }
        
        GeneralAuth auth = new GeneralAuth();
        // ---------------------------------------------------------- //
        // Diversify Admin Key
        // ---------------------------------------------------------- //
        //cmnUtil = new CommonUtil();
        byte[] adminKeyByte = credentialKeys.getAdminKey();
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte);
        
        
        try {

            auth.generalAuth(channel, adminKeyByte);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, diversifiedAdminKey);
            } catch (MiddleWareException m) {
                throw new MiddleWareException("Invalid Admin Key.");
            }
        }
        
        
        //switch channel to channel 1
        //pivUtil.selectManager(channel, true);
        
        
        PinPolicyBean ppBean =new PinPolicyBean();
        //-----------------------------------------------------------//
        //get and verify PIN length
        //-----------------------------------------------------------//
        ppBean = getPivUtil().readPinPolicy(channel);
        if (initvalue.getLocalPin().length() < ppBean.getMinLocalPinLength() || initvalue.getLocalPin().length() > ppBean.getMaxLocalPinLength()) {
            requiredValueMissing.add("Invalid Local Pin Length.("+ppBean.getMinLocalPinLength()+'-'+ppBean.getMaxLocalPinLength()+')');
        }

        if (initvalue.getLocalPinPTC() <= 0 || initvalue.getLocalPinPTC() > 10) {
            requiredValueMissing.add("Invalid Local Pin Retry.");
        }

        if (initvalue.getGlobalPin().length() < ppBean.getMinGlobalPinLength() || initvalue.getGlobalPin().length() > ppBean.getMaxGlobalPinLength()) {
            requiredValueMissing.add("Invalid Global Pin Length.("+ppBean.getMinGlobalPinLength()+'-'+ ppBean.getMaxGlobalPinLength()+')');
        }

        if (initvalue.getGlobalPinPTC() <= 0 || initvalue.getGlobalPinPTC() > 10) {
            requiredValueMissing.add("Invalid Global Pin Retry.");
        }

        if (initvalue.getUnblockPin().length() < ppBean.getMinLocalPUKLength() || initvalue.getUnblockPin().length() > ppBean.getMaxLocalPUKLength()) {
            requiredValueMissing.add("Invalid Unblock Pin Length.("+ppBean.getMinLocalPUKLength()+'-'+ppBean.getMaxLocalPUKLength()+')');
        }
        
        if (initvalue.getUnblockPinPTC() <= 0 || initvalue.getUnblockPinPTC() > 10) {
            requiredValueMissing.add("Invalid Unblock Pin Retry.");
        }
        
        if (!requiredValueMissing.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(requiredValueMissing.toString());
            throw new MiddleWareException(sb.toString());
        }

        if (retStatus == true) {
            CardUtilInterface lockcard = new CardUtil(credentialKeys, diversifiedKeys);
            retStatus = lockcard.CardLock(channel);
        }
        return true;
    }
    
    @Override
    public Integer readPinLength(CardChannel channel, String pinType) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readPinLength");
        
        PinPolicyBean ppBean = new PinPolicyBean();
        Integer pinLength=0;
       


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readPinLength start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //
        //PivUtilInterface pivUtil = new PivUtil();
        ppBean = getPivUtil().readPinPolicy(channel) ;

        if(pinType.equalsIgnoreCase(UcmsMiddlewareConstants.LOCAL_PIN)){
            pinLength = ppBean.getMaxLocalPinLength();
        } else if(pinType.equalsIgnoreCase(UcmsMiddlewareConstants.GLOBAL_PIN)){
            pinLength = ppBean.getMaxGlobalPinLength();
        } else if(pinType.equalsIgnoreCase(UcmsMiddlewareConstants.PUK_PIN)){
            pinLength = ppBean.getMaxLocalPUKLength();
        }
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readPinLength total time: "
                + totalTime);



        return pinLength;
    }
    
    @Override
    public PinPolicyBean readPinPolicy(CardChannel channel) throws MiddleWareException {
        PinPolicyBean ppBean = new PinPolicyBean();

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readPinPolicy");


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readPinPolicy start time: "
                + startTime);

        //PivUtilInterface pivUtil = new PivUtil();

        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //			
        ppBean = getPivUtil().readPinPolicy(channel) ;

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readPinPolicy total time: "
                + totalTime);

       
        return ppBean;
    }

    /*******************************************************************************************************/
    
    /************ security Container Functions ***************************************************************/
    
    @Override
    public boolean loadSecurityContainer(CardChannel channel,String adminKey,String localPin,ContentCertificateBean contentCert,String algoType,Date signatureDate) throws MiddleWareException {
        boolean pinPolicyVerified;
        boolean retValue;
        byte[] printedInfo;
        byte[] chuid;
        byte[] facialImage;
        byte[] fingerPrint=null;
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "loadSecurityContainer");
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::loadSecurityContainer start time: "
                + startTime);
        
        cmnUtil = new CommonUtil();
        //-----------------------------------------------------------//
        //get and verify local pin length
        //-----------------------------------------------------------//
        pinPolicyVerified =verifyPinPolicy(channel,localPin,UcmsMiddlewareConstants.LOCAL_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }
        
        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //			
        if (getPivUtil().verifyLocalPin(channel, localPin, maxPinLength) == false) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.INVALID_LOCAL_PIN_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }
        
        // ---------------------------------------------------------- //
        // Read Printed Information
        // ---------------------------------------------------------- //
        ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();        
        printedInfo = cmnUtil.hex1ToByteArray(mngPrintedInfo.getPrintedinfo(channel));
        
        // ---------------------------------------------------------- //
        // Read CHUID
        // ---------------------------------------------------------- //
        ManageCHUID mngChuid = new ManageCHUID();
        chuid = cmnUtil.hex1ToByteArray(mngChuid.getChuid(channel));
        
        // ---------------------------------------------------------- //
        // Read Facial Image
        // ---------------------------------------------------------- //
         facialImage = cmnUtil.hex1ToByteArray(getPivUtil().getFacialinfo(channel));
        
        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //	
        byte[] adminKeyByte = cmnUtil.hex1ToByteArray(adminKey);
        byte[] diversifiedAdminKey = keyDiversify(channel, adminKeyByte); 
         
         
        // ---------------------------------------------------------- //
        // Load Security Container
        // ---------------------------------------------------------- //
        LoadSecurityCont loadSecContainer = new LoadSecurityCont();
        retValue = loadSecContainer.loadSecCont(channel,printedInfo,chuid,fingerPrint,facialImage,contentCert.getPrivateKey(),contentCert.getContentCertificate(),signatureDate,
               adminKeyByte,diversifiedAdminKey,algoType);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::loadSecurityContainer total time: "
                + totalTime);

       
        return retValue;
    }
    
    
    @Override
    public SecurityBean verifySecurityContainer(CardChannel channel,String localPin) throws MiddleWareException {
        SecurityBean secuBean = new SecurityBean();
        boolean pinPolicyVerified;
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "verifySecurityContainer");
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::verifySecurityContainer start time: "
                + startTime);
        
        //-----------------------------------------------------------//
        //get and verify local pin length
        //-----------------------------------------------------------//
        pinPolicyVerified =verifyPinPolicy(channel,localPin,UcmsMiddlewareConstants.LOCAL_PIN);
        if(!pinPolicyVerified){
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }
        
        // ---------------------------------------------------------- //
        // Check if the PIN is Valid
        // ---------------------------------------------------------- //			
        if (getPivUtil().verifyLocalPin(channel, localPin, maxPinLength) == false) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.INVALID_LOCAL_PIN_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }
        
        // ---------------------------------------------------------- //
        // Load Security Container
        // ---------------------------------------------------------- //
        verifySecudataSignature  verifySecudata = new verifySecudataSignature();
        
        secuBean = verifySecudata.readSecContainer(channel);
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::verifySecurityContainer total time: "
                + totalTime);

       
        return secuBean;
    }
    
    
    /*******************************************************************************************************/
    
    @Override
    public boolean CardKeysVerification(CardChannel channel, CredentialKeys credentialKeys) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "CardLock");
        boolean retStatus = false;

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::CardLock start time: "
                + startTime);

        // ---------------------------------------------------------- //
        // set default value for card manage AID & lock card
        // ---------------------------------------------------------- //
        diversifiedKeys = new DiversifiedCredentialKeys();
        diversifiedKeys = diversifyCredentialKeys(channel, credentialKeys);


        CardUtilInterface lcard = new CardUtil(credentialKeys, diversifiedKeys);
        retStatus = lcard.CardKeysVerification(channel);

        if (retStatus == false) {
            // Stop communication & return Error
            throw new MiddleWareException("Unable to Lock Card.");
        }

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::CardLock total time: "
                + totalTime);

        return retStatus;
    }
    
    
    
    
    
    //Misc supporting Functions
    

    
   
    @Override
    public List<String> getFPTerminalsID3(int ImgQuality, int ImgCount) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "getFPTerminalsID3");
        List deviceList = new ArrayList<String>();
        


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::getFPTerminalsID3 start time: "
                + startTime);

        //get or create instance of Id3BioCapture class
        Id3BioCapture fpid3 = null;
        try {
            fpid3 = Id3BioCapture.getInstance(ImgQuality, ImgCount);
        } catch (MiddleWareException ex) {
            throw new MiddleWareException(ex.getMessage());
        }
        //get terminal list
        deviceList = fpid3.getFGTerminals();

        return deviceList;
        
    }

    @Override
    public BiometricBean readFingerPrintID3(int ImgQuality, int ImgCount, String deviceName, Integer fgType) throws MiddleWareException {
        BiometricBean fintemp = null;

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readFingerPrintID3");

       


        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::readFingerPrintID3 start time: "
                + startTime);

        //get or create instance of Id3BioCapture class
        Id3BioCapture fpid3 = null;
        try {
            fpid3 = Id3BioCapture.getInstance(ImgQuality, ImgCount);
        } catch (MiddleWareException ex) {
            throw new MiddleWareException(ex.getMessage());
        }

        //get finger print
        fintemp = fpid3.startCapture(deviceName, fgType);

        if (fpid3.getImageCount() == ImgCount) {
            fintemp = fpid3.createCompactTemplate();

            System.out.println("Harpreet--Creating Template");
        }


        return fintemp;
        
    }

    

    
    public byte[] keyDiversify(CardChannel channel, byte[] key) throws MiddleWareException {
        // First of all read CUID from the card
        String str_cuid = null;
        

        //PivUtilInterface pivUtil = new PivUtil();
        cmnUtil = new CommonUtil();

        //read cuid to diversify PIV admin Key
        str_cuid = getPivUtil().readCUID(channel);

        //48 20 50 2B 00 00 00 00 00 0A
        //48 20 50 2B 00 00 00 00 00 0B
        //convert cuid to byte and Extract 8 bytes 
        byte[] byte_cuid_s = cmnUtil.hex1ToByteArray(str_cuid);
        if (byte_cuid_s.length > 8) {
            byte_cuid_s = cmnUtil.strip_data(byte_cuid_s, byte_cuid_s.length - 8);
        }

        //replicate CUID to make it 16 bytes required to diversify 16 byte piv admin key
        byte[] byte_cuid = cmnUtil.combine_data(byte_cuid_s, byte_cuid_s);

        byte[] res = null;
        // Create encrypter instance
        if (key.length == 16) {
            AesEncrypter encrypter = new AesEncrypter(key);

            // Encrypt
            res = encrypter.encrypt(byte_cuid);


        } else {
            byte_cuid = cmnUtil.combine_data(byte_cuid, byte_cuid_s);
            DesEncrypter encrypter = new DesEncrypter(key);
            // encrypt Diversified PIV admin Key 
            res = encrypter.encrypt(byte_cuid);
        }

        CardInit sinit = new CardInit();
        sinit.checkPIVInstance(channel);

        return res;
        

    }

    private DiversifiedCredentialKeys diversifyCredentialKeys(CardChannel channel, CredentialKeys credentialKeys) throws MiddleWareException {
        //Diversify Credential Keys       
        diversifiedKeys = new DiversifiedCredentialKeys();
        diversifiedKeys.setDiversifiedAdminKey(keyDiversify(channel, credentialKeys.getAdminKey()));

        if (credentialKeys.getEncKey()!=null && credentialKeys.getEncKey().length > 0 && 
            credentialKeys.getMacKey()!=null && credentialKeys.getMacKey().length > 0 &&
            credentialKeys.getDekKey()!=null && credentialKeys.getDekKey().length > 0    ) {
            diversifiedKeys.setDiversifiedEncKey(keyDiversify(channel, credentialKeys.getEncKey()));
            diversifiedKeys.setDiversifiedMacKey(keyDiversify(channel, credentialKeys.getMacKey()));
            diversifiedKeys.setDiversifiedDekKey(keyDiversify(channel, credentialKeys.getDekKey()));
        } else {
            diversifiedKeys.setDiversifiedEncKey(credentialKeys.getEncKey());
            diversifiedKeys.setDiversifiedMacKey(credentialKeys.getMacKey());
            diversifiedKeys.setDiversifiedDekKey(credentialKeys.getDekKey());
        }
        return diversifiedKeys;
    }

    
    
     
    
    

    
    
    


    
    private boolean verifyPin(CardChannel channel, String pin, String pinType) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "verifyPin");
        PinPolicyBean ppBean = new PinPolicyBean();
        String retValue;
        boolean pinVerified = false;

        boolean pinPolicyVerified = false;



        if(pinType.equalsIgnoreCase(UcmsMiddlewareConstants.LOCAL_PIN) || pinType.equalsIgnoreCase(UcmsMiddlewareConstants.GLOBAL_PIN)){
            pinPolicyVerified = verifyPinPolicy(channel, pin, pinType);
            if (!pinPolicyVerified) {
                // stop communication & return success            
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }
        }
        //PivUtil pivUtil = new PivUtil();
        //-----------------------------------------------------------//
        //verify PIN 
        //-----------------------------------------------------------//
        if (pinType.equalsIgnoreCase(UcmsMiddlewareConstants.LOCAL_PIN)) {
            if (getPivUtil().verifyLocalPin(channel, pin, maxPinLength) == false) {
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.INVALID_LOCAL_PIN_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }
            pinVerified = true;
        } else if (pinType.equalsIgnoreCase(UcmsMiddlewareConstants.GLOBAL_PIN)) {
            if (getPivUtil().verifyGlobalPin(channel, pin, maxPinLength) == false) {
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.INVALID_GLOBAL_PIN_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }
            pinVerified = true;
        } else if (pinType.equalsIgnoreCase(UcmsMiddlewareConstants.SO_PIN)) {

            //TODO Need to implement the SO PIN functionality
            pinVerified = false;
        } else if (pinType.equalsIgnoreCase(UcmsMiddlewareConstants.BIO)) {
            cmnUtil = new CommonUtil();

            byte[] bioPin = cmnUtil.hex1StringToByteArray(pin);

            //call verify finger Print
            if (getPivUtil().verifyFingerPrint(channel, bioPin, UcmsMiddlewareConstants.subImpType) == false) {
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.INVALID_GLOBAL_PIN_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }
            pinVerified = true;
        }

        
        return pinVerified;
    }

    private boolean verifyPinPolicy(CardChannel channel, String pin, String pinType) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "verifyPinPolicy");
        PinPolicyBean ppBean =new PinPolicyBean();
        boolean pinPolicyVerified = false;
        

        //PivUtil pivUtil = new PivUtil();
        //-----------------------------------------------------------//
        //get and verify PIN length
        //-----------------------------------------------------------//
        ppBean = getPivUtil().readPinPolicy(channel);
        if (pinType.equalsIgnoreCase(UcmsMiddlewareConstants.LOCAL_PIN)) {
            minPinLength = ppBean.getMinLocalPinLength();
            maxPinLength = ppBean.getMaxLocalPinLength();        

        } else if (pinType.equalsIgnoreCase(UcmsMiddlewareConstants.GLOBAL_PIN)) {
            minPinLength = ppBean.getMinGlobalPinLength();
            maxPinLength = ppBean.getMaxLocalPinLength();
        }else if (pinType.equalsIgnoreCase(UcmsMiddlewareConstants.PUK_PIN)) {
            minPinLength = ppBean.getMinLocalPUKLength();
            maxPinLength = ppBean.getMaxLocalPUKLength();
        }

        if (pin.length() > maxPinLength || pin.length() == 0 ||pin.length()< minPinLength ) {
            // stop communication & return success            
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }
        pinPolicyVerified = true;


            
            
        
        return pinPolicyVerified;
    }
    
    
    
    
    @Override
    public boolean EndOfPerso(CardChannel channel, CredentialKeys credentialKeys) throws MiddleWareException {
        List requiredValueMissing = new ArrayList();
        boolean retValue=false;
        LoggerWrapper.entering("MiddleWareServerImpl", "EndOfPerso");
        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long startTime = System.currentTimeMillis();
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::EndOfPerso start time: "
                + startTime);

        CardReaderValidator validator = new CardReaderValidator();  
        // ---------------------------------------------------------- //
        // check required values - Credential Keys
        // ---------------------------------------------------------- //        
        requiredValueMissing = validator.validateCredentialKeys(credentialKeys);
        if (!requiredValueMissing.isEmpty()) {
            StringBuilder sb = new StringBuilder();            
            sb.append(requiredValueMissing.toString());
            throw new MiddleWareException(sb.toString());
        }
        
        // ---------------------------------------------------------- //
        // diversify Credential Keys
        // ---------------------------------------------------------- //
        diversifiedKeys = new DiversifiedCredentialKeys();
        diversifiedKeys = diversifyCredentialKeys(channel, credentialKeys);

        
        
        EndOfPerso endPerso = new EndOfPerso(credentialKeys,diversifiedKeys);        
        // ---------------------------------------------------------- //
        // Read CHUID
        // ---------------------------------------------------------- //
        
        retValue = endPerso.endPerso(channel);

        // ---------------------------------------------------------- //
        // LOGGING INFORMATON
        // ---------------------------------------------------------- //
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        LoggerWrapper.info(client
                + " MiddleWareServerImpl::EndOfPerso total time: "
                + totalTime);

        return retValue;
    }

    
    
    
    private boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    
    
     
    
}