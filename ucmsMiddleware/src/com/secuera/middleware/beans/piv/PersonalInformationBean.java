/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.io.Serializable;

/**
 *
 * @author Satish K
 */
public class PersonalInformationBean  implements Serializable {
    
    PrintedInfoBean printedInfo;
    FacialBean facialBean;
    FingerPrintBean fingerPrintBean;
    SecurityBean securityBean;
    
    public FacialBean getFacialBean() {
        return facialBean;
    }

    public void setFacialBean(FacialBean facialBean) {
        this.facialBean = facialBean;
    }

    public PrintedInfoBean getPrintedInfo() {
        return printedInfo;
    }

    public void setPrintedInfo(PrintedInfoBean printedInfo) {
        this.printedInfo = printedInfo;
    }

    public FingerPrintBean getFingerPrintBean() {
        return fingerPrintBean;
    }

    public void setFingerPrintBean(FingerPrintBean fingerPrintBean) {
        this.fingerPrintBean = fingerPrintBean;
    }

    public SecurityBean getSecurityBean() {
        return securityBean;
    }

    public void setSecurityBean(SecurityBean securityBean) {
        this.securityBean = securityBean;
    }
    
    
    
}
