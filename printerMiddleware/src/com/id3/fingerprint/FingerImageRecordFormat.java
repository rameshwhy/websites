package com.id3.fingerprint;

public class FingerImageRecordFormat
{
  public static final int Unknown = 0;
  public static final int Incits381 = 1;
  public static final int Iso19794 = 2;
  public static final int Incits381_2009 = 3;
  public static final int Iso19794_2011 = 4;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerImageRecordFormat
 * JD-Core Version:    0.6.2
 */