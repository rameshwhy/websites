/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 *
 * @author Satish K
 */
public class UserContentCSR {
    private String contentCSR;
    private PublicKey publicKey;
    private PrivateKey privateKey;

    public String getContentCSR() {
        return contentCSR;
    }

    public void setContentCSR(String contentCSR) {
        this.contentCSR = contentCSR;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }
    
    
}
