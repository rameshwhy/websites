// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.ica;

import eu.gnome.morena.*;
import eu.gnome.morena.Scanner;
import java.util.*;

// Referenced classes of package eu.gnome.morena.ica:
//            ICADevice

public class ICAScanner extends ICADevice
    implements Scanner
{
     private int initialized;
    private int bitDepth;
    private ArrayList supportedBitDepths;
    private int resolution;
    private ArrayList supportedResolutions;
    private int x;
    private int y;
    private int width;
    private int height;
    private int duplexSupported;
    private int duplexEnabled;
    private int flatbedFU;
    private int feederFU;
    private int lastStatus;
    
    private class ProgressReporter extends TimerTask
    {

        public void run()
        {
            int i = (int)_getProgress(lastStatus);
            if(i != lastStatus)
            {
                ((TransferListener)this).transferProgress(i);
                lastStatus = i;
            }
        }

        public boolean cancel()
        {
            ((TransferListener)this).transferProgress(lastStatus = 100);
            return super.cancel();
        }

        private int lastStatus;
        final ICAScanner this$0;

        private ProgressReporter()
        {
           
            super();
             this$0 = ICAScanner.this;
            lastStatus = -1;
        }

    }


    private native void _initialize(long l);

    private native void _transfer(long l, int i)
        throws MorenaException;

    private native double _getProgress(long l);

    private native void _cancelTransfer(long l);

    ICAScanner(String s, long l)
    {
        super(s, l);
        initialized = 0;
        bitDepth = 0;
        resolution = 0;
        x = -1;
        y = -1;
        width = -1;
        height = -1;
        duplexSupported = 0;
        duplexEnabled = 0;
        flatbedFU = -1;
        feederFU = -1;
        _initialize(l);
    }

    public void startTransfer(TransferDoneListener transferdonelistener, int i)
        throws Exception
    {
        super.startTransfer(transferdonelistener, i);
        try
        {
            _transfer(handle, i);
        }
        catch(MorenaException morenaexception)
        {
            transferdonelistener.transferFailed(morenaexception.getErrorCode(), morenaexception.getMessage());
        }
        catch(Throwable throwable)
        {
            transferdonelistener.transferFailed(-1, throwable.toString());
        }
        if(transferdonelistener instanceof TransferListener)
        {
            if(timer == null)
                timer = new Timer(true);
            if(reporter != null)
                reporter.cancel();
            timer.scheduleAtFixedRate(reporter = new ProgressReporter(), 100L, 100L);
        }
    }

    public void setMode(int i)
    {
        bitDepth = i;
    }

    public int getMode()
    {
        return bitDepth;
    }

    private void addBitDepth(int i)
    {
        if(supportedBitDepths == null)
            supportedBitDepths = new ArrayList();
        supportedBitDepths.add(Integer.valueOf(i));
        if(i > 1)
            supportedBitDepths.add(Integer.valueOf(-i));
    }

    public List getSupportedModes()
    {
        return supportedBitDepths;
    }

    public void setResolution(int i)
    {
        resolution = i;
    }

    public int getResolution()
    {
        return resolution;
    }

    private void addResolution(int i)
    {
        if(supportedResolutions == null)
            supportedResolutions = new ArrayList();
        supportedResolutions.add(Integer.valueOf(i));
    }

    public List getSupportedResolutions()
    {
        return supportedResolutions;
    }

    public void setFrame(int i, int j, int k, int l)
    {
        x = i;
        y = j;
        width = k;
        height = l;
    }

    public void cancelTransfer()
    {
        if(reporter != null)
            reporter.cancel();
        _cancelTransfer(handle);
    }

    public int getFlatbedFunctionalUnit()
    {
        return flatbedFU;
    }

    public int getFeederFunctionalUnit()
    {
        return feederFU;
    }

    public boolean isDuplexSupported()
    {
        return duplexSupported != 0;
    }

    public boolean isDuplexEnabled()
    {
        return duplexEnabled != 0;
    }

    public void setDuplexEnabled(boolean flag)
    {
        duplexEnabled = flag ? 1 : 0;
    }

   




}
