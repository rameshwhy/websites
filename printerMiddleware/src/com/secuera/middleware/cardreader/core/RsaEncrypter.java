/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author admin
 */

public class RsaEncrypter {

	   Cipher cipher;
	    
	    
	    CommonUtil util= new CommonUtil();
	    //byte [] key_p
	   public  RsaEncrypter() { 
	        try {
	   
	          String transformation = "RSA";
	        	//SecretKeySpec keySpec = new SecretKeySpec(key_p, "RSA");
	 		    
	        	cipher = Cipher.getInstance(transformation);
	        //	ecipher.init(Cipher.ENCRYPT_MODE, key);
	        	
	        	
	        } catch (javax.crypto.NoSuchPaddingException e) {
	        } catch (java.security.NoSuchAlgorithmException e) {
	         
	        }
	    }

	       
	   public byte[] encrypt(PublicKey key,byte[] encr) throws InvalidKeyException {
		    
	    	try {
	            
	    		int key_len=128;
	    		
	    		cipher.init(Cipher.ENCRYPT_MODE, key);
	    		
	    		byte[] encrypted = blockCipher(encr,Cipher.ENCRYPT_MODE,key_len);
	        	
	    			    	
	    		// Encrypt
	            //byte[] enc = ecipher.doFinal(encr);
	                       
	            // Encode bytes to base64 to get a string
	            //return new sun.misc.BASE64Encoder().encode(enc);
	          return encrypted;  
	        } catch (javax.crypto.BadPaddingException e) {
	        } catch (IllegalBlockSizeException e) {
	        }
	        return null;
	    }

	       
	  
	   
	    public byte[] decrypt(PrivateKey key,byte [] dcr) throws InvalidKeyException {
	        try {
	            // Decode base64 to get bytes
	            //byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

	        	int key_len=256;
	        	
	        	cipher.init(Cipher.DECRYPT_MODE, key);
	            // Decrypt
	             byte[] decrypted = blockCipher(dcr,Cipher.DECRYPT_MODE,key_len);
	            
	            // Decode using utf-8
	            //return new String(utf8, "UTF8");
	            
	            return decrypted;
	            
	        } catch (javax.crypto.BadPaddingException e) {
	        } catch (IllegalBlockSizeException e) {
	        }
	        return null;
	    }

	    
	    
	    private byte[] blockCipher(byte[] bytes, int mode, int key_len) throws IllegalBlockSizeException, BadPaddingException{
	    	
	    	int block_size;
	    	
	    	if (key_len == 256)
	    	{
	    	
	    		if (bytes.length >= 232 ) 
	    		{
	    			block_size=232;
	    		}else
	    		{
	    			block_size=bytes.length;
	    		}
	    		
	    	}else
	    	{
	    		if (bytes.length >= 116 ) 
	    		{
	    			block_size=116;
	    		}else
	    		{
	    			block_size=bytes.length;
	    		}
	    		
	    	}
	    	
	    	
	    	// scrambled will hold intermediate results
	    	byte[] scrambled = new byte[0];

	    	//Result will hold the total result
	    	byte[] Result = new byte[0];
	    	// if we encrypt we use 132 byte long blocks. Decryption requires 256 byte long blocks (because of RSA 2048)
	    	int length = (mode == Cipher.ENCRYPT_MODE)? block_size : key_len;

	    	// another buffer. this one will hold the bytes that have to be modified in this step
	    	byte[] buffer = new byte[length];

	    	for (int i=0; i< bytes.length ; i++){

	    		// if we filled our buffer array we have our block ready for de- or encryption
	    		if ((i > 0) && (i % length == 0)){
	    			//execute the operation
	    			scrambled = cipher.doFinal(buffer);
	    			// add the result to our total result.
	    			Result = append(Result,scrambled);
	    			// here we calculate the length of the next buffer required
	    			int newlength = length;

	    			// if newlength would be longer than remaining bytes in the bytes array we shorten it.
	    			if (i + length > bytes.length) {
	    				 newlength = bytes.length - i;
	    			}
	    			// clean the buffer array
	    			buffer = new byte[newlength];
	    		}
	    		// copy byte into our buffer.
	    		buffer[i%length] = bytes[i];
	    	}

	    	// this step is needed if we had a trailing buffer. should only happen when encrypting.
	    	// example: we encrypt 232 bytes. 262 bytes per run means we "forgot" the last 30 bytes. they are in the buffer array
	    	scrambled = cipher.doFinal(buffer);

	    	// final step before we can return the modified data.
	    	Result = append(Result,scrambled);

	    	return Result;
	    }
	    
	    
	    
	    
	    private byte[] append(byte[] prefix, byte[] suffix){
	    	byte[] Result = new byte[prefix.length + suffix.length];
	    	for (int i=0; i< prefix.length; i++){
	    		Result[i] = prefix[i];
	    	}
	    	for (int i=0; i< suffix.length; i++){
	    		Result[i+prefix.length] = suffix[i];
	    	}
	    	return Result;
	    }
	    
	    
}

