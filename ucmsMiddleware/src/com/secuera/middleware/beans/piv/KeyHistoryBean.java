package com.secuera.middleware.beans.piv;

public class KeyHistoryBean {

    private int onCardCerts;
    private int offCardCerts;
    private String URL;

    public int getOnCardCerts() {
        return onCardCerts;
    }

    public void setOnCardCerts(int onCardCerts) {
        this.onCardCerts = onCardCerts;
    }

    public int getOffCardCerts() {
        return offCardCerts;
    }

    public void setOffCardCerts(int offCardCerts) {
        this.offCardCerts = offCardCerts;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }


}
