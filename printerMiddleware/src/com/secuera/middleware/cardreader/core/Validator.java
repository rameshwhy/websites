/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author admin
 */
public class Validator {
	/*
	* Checks for invalid characters
	* in email addresses
	*/
	public boolean isValidEmail (String input){

		  //Checks for email addresses starting with
	      //inappropriate symbols like dots or @ signs.
	      Pattern p = Pattern.compile("^\\.|^\\@");
	      Matcher m = p.matcher(input);
	      
	      if (m.find())
	      {
	    	  return false;
	      }
	         //System.err.println("Email addresses don't start" +
	          //                  " with dots or @ signs.");
	      //Checks for email addresses that start with
	      //www. and prints a message if it does.
	    	  
	      p = Pattern.compile("^www\\.");
	      m = p.matcher(input);
	      if (m.find()) {
	    	  
	    	  return false;
	        //System.out.println("Email addresses don't start" +
	          //      " with \"www.\", only web pages do.");
	      }
	      
	      p = Pattern.compile("[^A-Za-z0-9\\.\\@_\\-~#]+");
	      m = p.matcher(input);
	      
	      if (m.find()) {
	    	  
	    	  return false;
	        //System.out.println("Email addresses don't start" +
	          //      " with \"www.\", only web pages do.");
	      }
	      
	      		return true;
	   }

	
	


	public boolean isValidDate(String date)
	// date validation using SimpleDateFormat
	// it will take a string and make sure it's in the proper 
	// format as defined by you, and it will also make sure that
	// it's a legal date

	{
	    // set date format, this can be changed to whatever format
	    // you want, MM-dd-yyyy, MM.dd.yyyy, dd.MM.yyyy etc.
	    // you can read more about it here:
	    // http://java.sun.com/j2se/1.4.2/docs/api/index.html

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	    
		// declare and initialize testDate variable, this is what will hold
	    // our converted string

		Date testDate = null;

	    // we will now try to parse the string into date form
	    try
	    {
	      testDate = sdf.parse(date);
	    }
	    // if the format of the string provided doesn't match the format we 
	    // declared in SimpleDateFormat() we will get an exception
	    catch (ParseException e)

	    {

	    //  errorMessage = "the date you provided is in an invalid date" +
	   //                           " format.";
	      return false;
	    }

	    // dateformat.parse will accept any date as long as it's in the format
	    // you defined, it simply rolls dates over, for example, december 32 
	    // becomes jan 1 and december 0 becomes november 30
	    // This statement will make sure that once the string 
	    // has been checked for proper formatting that the date is still the 
	    // date that was entered, if it's not, we assume that the date is invalid

	    if (!sdf.format(testDate).equals(date)) 
	    {
	     // errorMessage = "The date that you provided is invalid.";
	      return false;
	    }

	    // if we make it to here without getting an error it is assumed that
	    // the date was a valid one and that it's in the proper format

	    return true;

	} // end isValidDate

	
	
	/** isPhoneNumberValid: Validate phone number using Java reg ex.
	* This method checks if the input string is a valid phone number.
	* @param email String. Phone number to validate
	* @return boolean: true if phone number is valid, false otherwise.
	*/
	public static boolean isPhoneNumberValid(String phoneNumber){
	boolean isValid = false;
	/* Phone Number formats: (nnn)nnn-nnnn; nnnnnnnnnn; nnn-nnn-nnnn
		^\\(? : May start with an option "(" .
		(\\d{3}): Followed by 3 digits.
		\\)? : May have an optional ")"
		[- ]? : May have an optional "-" after the first 3 digits or after optional ) character.
		(\\d{3}) : Followed by 3 digits.
		 [- ]? : May have another optional "-" after numeric digits.
		 (\\d{4})$ : ends with four digits.

	         Examples: Matches following phone numbers:
	         (123)456-7890, 123-456-7890, 1234567890, (123)-456-7890

	*/
	//Initialize reg ex for phone number. 
	String expression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
	CharSequence inputStr = phoneNumber;
	Pattern pattern = Pattern.compile(expression);
	Matcher matcher = pattern.matcher(inputStr);
	if(matcher.matches()){
	isValid = true;
	}
	return isValid;
	}

	
	
	/** isSSNValid: Validate Social Security number (SSN) using Java reg ex.
	* This method checks if the input string is a valid SSN.
	* @param email String. Social Security number to validate
	* @return boolean: true if social security number is valid, false otherwise.
	*/
	 public static boolean isSSNValid(String ssn){
	boolean isValid = false;
	 /*SSN format xxx-xx-xxxx, xxxxxxxxx, xxx-xxxxxx; xxxxx-xxxx:
	         ^\\d{3}: Starts with three numeric digits.
		[- ]?: Followed by an optional "-"
		\\d{2}: Two numeric digits after the optional "-"
		[- ]?: May contain an optional second "-" character.
		\\d{4}: ends with four numeric digits.

	        Examples: 879-89-8989; 869878789 etc.
	*/

	//Initialize reg ex for SSN. 
	String expression = "^\\d{3}[- ]?\\d{2}[- ]?\\d{4}$";
	CharSequence inputStr = ssn;
	Pattern pattern = Pattern.compile(expression);
	Matcher matcher = pattern.matcher(inputStr);
	if(matcher.matches()){
	isValid = true;
	}
	return isValid;
	}

	

	 /** isNumeric: Validate a number using Java regex.
	 * This method checks if the input string contains all numeric characters.
	 * @param email String. Number to validate
	 * @return boolean: true if the input is all numeric, false otherwise.
	 */

	 public static boolean isNumeric(String number){
	 boolean isValid = false;

	 /*Number: A numeric value will have following format:
	          ^[-+]?: Starts with an optional "+" or "-" sign.
	 	 [0-9]*: May have one or more digits.
	 	\\.? : May contain an optional "." (decimal point) character.
	 	[0-9]+$ : ends with numeric digit.
	 */

	 //Initialize reg ex for numeric data.
	 String expression = "^[-+]?[0-9]*\\.?[0-9]+$";
	 CharSequence inputStr = number;
	 Pattern pattern = Pattern.compile(expression);
	 Matcher matcher = pattern.matcher(inputStr);
	 if(matcher.matches()){
	 isValid = true;
	 }
	 return isValid;
	 }

	 
	 
	 
		 public static boolean isValidLength(String str,int len){
		 boolean isValid = false;

		 if (str.length()== len)
			 {
			 	isValid = true;
			 }
		 
		 
		 return isValid;
		 
		 }
	 
}
