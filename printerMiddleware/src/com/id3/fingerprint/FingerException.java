/*    */ package com.id3.fingerprint;
/*    */ 
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ 
/*    */ public class FingerException extends Exception
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */   private int _errorCode;
/* 17 */   private Map<Integer, String> _errorMessages = new HashMap();
/*    */ 
/*    */   private void init()
/*    */   {
/* 21 */     this._errorMessages.put(Integer.valueOf(0), "No error");
/* 22 */     this._errorMessages.put(Integer.valueOf(-1), "No license was found.");
/* 23 */     this._errorMessages.put(Integer.valueOf(-2), "License is not valid or a required module is not licensed.");
/* 24 */     this._errorMessages.put(Integer.valueOf(-3), "The license has expired.");
/* 25 */     this._errorMessages.put(Integer.valueOf(-4), "General licensing error. License protection tools may not be installed correctly.");
/* 26 */     this._errorMessages.put(Integer.valueOf(-5), "Maximum number of fingers reached.");
/* 27 */     this._errorMessages.put(Integer.valueOf(-6), "Maximum number of users reached.");
/* 28 */     this._errorMessages.put(Integer.valueOf(-10), "Invalid handle.");
/* 29 */     this._errorMessages.put(Integer.valueOf(-11), "Invalid parameter.");
/* 30 */     this._errorMessages.put(Integer.valueOf(-12), "Invalid format.");
/* 31 */     this._errorMessages.put(Integer.valueOf(-13), "Insufficient buffer.");
/* 32 */     this._errorMessages.put(Integer.valueOf(-14), "A required library could not be found.");
/* 33 */     this._errorMessages.put(Integer.valueOf(-20), "The maximum number of representations of a single finger has been reached.");
/* 34 */     this._errorMessages.put(Integer.valueOf(-21), "The finger record object is empty.");
/* 35 */     this._errorMessages.put(Integer.valueOf(-30), "No device available for the current capture mode.");
/* 36 */     this._errorMessages.put(Integer.valueOf(-1003), "An attempt was made to end a non-existent transaction.");
/* 37 */     this._errorMessages.put(Integer.valueOf(-1002), "The smart card has been removed, so that further communication is not possible.");
/* 38 */     this._errorMessages.put(Integer.valueOf(-1004), "The smart card cannot be accessed because of other connections outstanding.");
/* 39 */     this._errorMessages.put(Integer.valueOf(-1000), "The smart card has returned an error.");
/* 40 */     this._errorMessages.put(Integer.valueOf(-1001), "An internal error has been detected, but the source is unknown.");
/* 41 */     this._errorMessages.put(Integer.valueOf(-3000), "Data integrity check failed.");
/*    */   }
/*    */ 
/*    */   public FingerException()
/*    */   {
/* 49 */     init();
/*    */   }
/*    */ 
/*    */   public FingerException(int errorCode)
/*    */   {
/* 58 */     init();
/* 59 */     this._errorCode = errorCode;
/*    */   }
/*    */ 
/*    */   public int getErrorCode()
/*    */   {
/* 68 */     return this._errorCode;
/*    */   }
/*    */ 
/*    */   public String getMessage()
/*    */   {
/* 77 */     return (String)this._errorMessages.get(Integer.valueOf(this._errorCode));
/*    */   }
/*    */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerException
 * JD-Core Version:    0.6.2
 */