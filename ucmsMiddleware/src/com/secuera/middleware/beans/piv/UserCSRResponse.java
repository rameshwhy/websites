/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.util.List;

/**
 *
 * @author Satish K
 */
public class UserCSRResponse {
    private String csrPivAuthentication9A;
    private String csrDigSignature9C;
    private String csrKeyManagement9D;
    private String csrCardAuthentication9E;
    
    private List errorList;

    public String getCsrCardAuthentication9E() {
        return csrCardAuthentication9E;
    }

    public void setCsrCardAuthentication9E(String csrCardAuthentication9E) {
        this.csrCardAuthentication9E = csrCardAuthentication9E;
    }

    public String getCsrDigSignature9C() {
        return csrDigSignature9C;
    }

    public void setCsrDigSignature9C(String csrDigSignature9C) {
        this.csrDigSignature9C = csrDigSignature9C;
    }

    public String getCsrKeyManagement9D() {
        return csrKeyManagement9D;
    }

    public void setCsrKeyManagement9D(String csrKeyManagement9D) {
        this.csrKeyManagement9D = csrKeyManagement9D;
    }

    public String getCsrPivAuthentication9A() {
        return csrPivAuthentication9A;
    }

    public void setCsrPivAuthentication9A(String csrPivAuthentication9A) {
        this.csrPivAuthentication9A = csrPivAuthentication9A;
    }

    public List getErrorList() {
        return errorList;
    }

    public void setErrorList(List errorList) {
        this.errorList = errorList;
    }
    
    
    
}
