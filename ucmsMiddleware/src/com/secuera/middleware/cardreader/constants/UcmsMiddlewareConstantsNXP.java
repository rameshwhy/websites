/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.constants;

/**
 *
 * @author Harpreet Singh
 */
public class UcmsMiddlewareConstantsNXP {
                                                                              
    public static byte[] BAC_CARD_MANAGER_AID = {(byte) 0xA0,0x00,0x00,0x02,0x47,0x10,0x01};
    public static byte[] EPKI_CARD_MANAGER_AID = {(byte) 0xA0,0x00,0x00,0x00,0x63,0x50,0x4B,0x43,0x53,0x2D,0x31,0x35};
                      
    public static String USER_PIN = "U";
    
    public static String EACApplet = "E";
    public static String EPKIApplet = "P";
    public static String MOCLibrary = "M";
    
    public static byte[][] fp_id = {{0x02,0x10},{0x02,0x11},{0x02,0x12},{0x02,0x13},{0x02,0x14},{0x02,0x15},{0x02,0x16},{0x02,0x17},
                {0x02,0x18},{0x02,0x19},{0x01,0x1F}};
    
    public static byte[][] fp_upid = {{(byte)0x90},{(byte)0x91},{(byte)0x92},{(byte)0x93},{(byte)0x94},
                            {(byte)0x95},{(byte)0x96},{(byte)0x97},{(byte)0x98},{(byte)0x99},{(byte)0x9F}};    
        
}
