// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.wia;

import eu.gnome.morena.*;
import eu.gnome.morena.Scanner;
import java.util.*;

// Referenced classes of package eu.gnome.morena.wia:
//            WIADevice, WIAItem, SupportedPropertyValues, WIAManager

public class WIAScanner extends WIADevice
    implements Scanner, Runnable
{

    private native long _initialize(String s)
        throws Exception;

    private native void _transfer(long l, boolean flag, int i, boolean flag1, int ai[])
        throws MorenaException;

    private native void _cancelTransfer(long l);

    WIAScanner(String s, String s1)
    {
        super(s, s1);
        bitDepth = 8;
        resolution = 50;
        x = -1;
        y = -1;
        width = -1;
        height = -1;
        brightness = 0;
        contrast = 0;
        try
        {
            handle = _initialize(s1);
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            System.out.println((new StringBuilder()).append("Error while initializing scanner: ").append(s).toString());
        }
        available = true;
        extPropList = new ArrayList();
        if(functionalUnit >= 0 && functionalUnit < items.size())
        {
            WIAItem wiaitem = (WIAItem)items.get(functionalUnit);
            SupportedPropertyValues supportedpropertyvalues = wiaitem.getProperty(4104);
            if(supportedpropertyvalues != null && !supportedpropertyvalues.isNone())
                switch(supportedpropertyvalues.isCurrentValueSet() ? supportedpropertyvalues.getCurrentValue() : supportedpropertyvalues.getDefaultValue())
                {
                case 1: // '\001'
                    bitDepth = -1;
                    break;

                case 8: // '\b'
                    bitDepth = -8;
                    break;

                case 24: // '\030'
                    bitDepth = 8;
                    break;

                default:
                    bitDepth = 8;
                    break;
                }
            supportedpropertyvalues = wiaitem.getProperty(6147);
            if(supportedpropertyvalues != null && !supportedpropertyvalues.isNone())
                resolution = supportedpropertyvalues.isCurrentValueSet() ? supportedpropertyvalues.getCurrentValue() : supportedpropertyvalues.getDefaultValue();
        } else
        {
           System.out.println((new StringBuilder()).append("WIAScanner<init> wrong default FU ").append(functionalUnit).append(" > ").append(items.size()).toString());
        }
    }

    protected void addItem(String s, int i)
    {
        WIAItem wiaitem = new WIAItem(s, i);
        items.add(wiaitem);
        System.out.println((new StringBuilder()).append("Adding item : ").append(s).append("  category ").append(i).toString());
    }

    protected void addItemProperty(int i, int j, String s, boolean flag, int k, int l, int ai[])
    {
        WIAItem wiaitem = (WIAItem)items.get(i);
        wiaitem.addProperty(j, s, flag, k, l, ai);
    }

    protected void transfer()
    {
        try
        {
            _transfer(handle, (Configuration.getMode() & 1) > 0, functionalUnit, transferDoneListener instanceof TransferListener, scanProps);
        }
        catch(MorenaException morenaexception)
        {
            transferDoneListener.transferFailed(morenaexception.getErrorCode(), morenaexception.getMessage());
        }
        catch(Throwable throwable)
        {
            transferDoneListener.transferFailed(-1, throwable.toString());
        }
    }

    public void run()
    {
        transfer();
    }

    public void startTransfer(TransferDoneListener transferdonelistener, int i)
        throws Exception
    {
        super.startTransfer(transferdonelistener, i);
        ArrayList arraylist = new ArrayList(20);
        int ai[] = BASIC_PROPERTIES;
        int j = ai.length;
        for(int k = 0; k < j; k++)
        {
            int i1 = ai[k];
            arraylist.add(Integer.valueOf(i1));
        }

        if(x != -1)
            arraylist.add(Integer.valueOf(6149));
        if(y != -1)
            arraylist.add(Integer.valueOf(6150));
        if(width != -1)
            arraylist.add(Integer.valueOf(6151));
        if(height != -1)
            arraylist.add(Integer.valueOf(6152));
        arraylist.addAll(extPropList);
        ArrayList arraylist1 = new ArrayList(arraylist.size());
        WIAItem wiaitem = WIAManager.getVersion() != 1 ? (WIAItem)items.get(i) : (WIAItem)items.get(0);
        Iterator iterator = arraylist.iterator();
        do
        {
            if(!iterator.hasNext())
                break;
            int j1 = ((Integer)iterator.next()).intValue();
            SupportedPropertyValues supportedpropertyvalues = wiaitem.getProperty(j1);
            if(!Configuration.checkWriteable() || supportedpropertyvalues != null && supportedpropertyvalues.isWritable())
                arraylist1.add(Integer.valueOf(j1));
        } while(true);
        scanProps = new int[arraylist1.size()];
        int l = 0;
        for(Iterator iterator1 = arraylist1.iterator(); iterator1.hasNext();)
        {
            int k1 = ((Integer)iterator1.next()).intValue();
            scanProps[l++] = k1;
        }

        System.out.println((new StringBuilder()).append("Properties ").append(Arrays.toString(scanProps)).toString());
        (new Thread(this)).start();
    }

    public void setMode(int i)
    {
        bitDepth = i;
    }

    public int getMode()
    {
        return bitDepth;
    }

    public List getSupportedModes()
    {
        return ((List) (getCurrentItem() == null ? new ArrayList() : getCurrentItem().getSupportedModes()));
    }

    public void setResolution(int i)
    {
        resolution = i;
    }

    public int getResolution()
    {
        return resolution;
    }

    public List getSupportedResolutions()
    {
        return ((List) (getCurrentItem() == null ? new ArrayList() : getCurrentItem().getSupportedValues(6147)));
    }

    public void setFrame(int i, int j, int k, int l)
    {
        x = i;
        y = j;
        width = k;
        height = l;
    }

    public void cancelTransfer()
    {
        _cancelTransfer(handle);
    }

    public int getFlatbedFunctionalUnit()
    {
        if(WIAManager.getVersion() == 1 && (handlingCapab & 2) > 0)
            return 2;
        if(WIAManager.getVersion() == 2)
        {
            for(int i = 0; i < items.size(); i++)
            {
                WIAItem wiaitem = (WIAItem)items.get(i);
                if(wiaitem.isFlatbed())
                    return i;
            }

        }
        return -1;
    }

    public int getFeederFunctionalUnit()
    {
        if(WIAManager.getVersion() == 1 && (handlingCapab & 1) > 0)
            return 1;
        if(WIAManager.getVersion() == 2)
        {
            for(int i = 0; i < items.size(); i++)
            {
                WIAItem wiaitem = (WIAItem)items.get(i);
                if(wiaitem.isFeeder())
                    return i;
            }

        }
        return -1;
    }

    public boolean isDuplexSupported()
    {
        return (handlingCapab & 4) > 0;
    }

    public boolean isDuplexEnabled()
    {
        return duplex != 0;
    }

    public void setDuplexEnabled(boolean flag)
    {
        duplex = flag ? 1 : 0;
    }

    public int getBrightness()
    {
        return brightness;
    }

    public void setBrightness(int i)
    {
        brightness = i;
    }

    public int getContrast()
    {
        return contrast;
    }

    public void setContrast(int i)
    {
        contrast = i;
    }

    public int getHandlingCapab()
    {
        return handlingCapab;
    }

    public int getHandlingSelect()
    {
        return handlingSelect;
    }

    public SupportedPropertyValues getPropertyValues(int i)
    {
        return getCurrentItem() == null ? null : getCurrentItem().getProperty(i);
    }

    private WIAItem getCurrentItem()
    {
        int i = WIAManager.getVersion() != 1 || (handlingCapab & 1) <= 0 ? functionalUnit : 0;
        if(items.size() > i)
            return (WIAItem)items.get(i);
        else
            return null;
    }

    private int bitDepth;
    private int resolution;
    private int x;
    private int y;
    private int width;
    private int height;
    private int brightness;
    private int contrast;
    private int handlingCapab;
    private int handlingSelect;
    private int duplex;
    private int scanProps[];
    private List extPropList;
    private static final int BASIC_PROPERTIES[] = {
        6147, 6148, 4103, 4104, 6154, 6155
    };

}
