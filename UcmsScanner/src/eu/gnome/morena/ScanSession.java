package eu.gnome.morena;

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

import eu.gnome.morena.*;
import java.io.File;
import java.io.PrintStream;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ScanSession
{
    class MultiFileTransferHandler
        implements TransferListener
    {

        public void transferDone(File file)
        {
            String s = file.getParent();
            String s1 = ScanSession.getExt(file);
            File file1 = new File(s, getUniqueFileName("mi", s1));
            if(!file1.exists());
            boolean flag = file.renameTo(file1);
            if(!flag)
            {
                System.out.println((new StringBuilder()).append("Cannot rename the file ").append(file).append("  to  ").append(file1).toString());
                return;
            }
            try
            {
                queue.put(file1.getAbsolutePath());
            }
            catch(InterruptedException interruptedexception)
            {
                interruptedexception.printStackTrace();
            }
        }

        public void transferProgress(int i)
        {
            System.out.println((new StringBuilder()).append("transfer ").append(i).append("%").toString());
        }

        public void transferFailed(int i, String s)
        {
            code = i;
            error = s;
            transferFinished = true;
            try
            {
                queue.put("");
            }
            catch(InterruptedException interruptedexception)
            {
                interruptedexception.printStackTrace();
            }
        }

        private synchronized String getUniqueFileName(String s, String s1)
        {
            return new String((new StringBuilder()).append(s).append("_").append(time).append("_").append(++pcounter).append(s1.isEmpty() ? "" : (new StringBuilder()).append(".").append(s1).toString()).toString());
        }

        int code;
        String error;
        long time;
        int pcounter;
        final ScanSession this$0;

        public MultiFileTransferHandler()
        {
             super();
            this$0 = ScanSession.this;
           time = System.currentTimeMillis();
            pcounter = 0;
            code = -1;
            error = "No error";
        }
    }


    public ScanSession()
    {
        transferFinished = false;
    }

    public void startSession(Device device, int i)
        throws Exception
    {
        queue = new LinkedBlockingQueue();
        blockedThreadCount = new AtomicInteger(0);
        transferFinished = false;
        th = new MultiFileTransferHandler();
        ((DeviceBase)device).startTransfer(th, i);
    }

    public File getImageFile()
    {
        String s = (String)queue.poll();
        if(s == null && !transferFinished)
            try
            {
                blockedThreadCount.incrementAndGet();
                s = (String)queue.take();
                blockedThreadCount.decrementAndGet();
            }
            catch(InterruptedException interruptedexception) { }
        if(s.isEmpty())
            releaseBlockedThreads();
        return s != null && !s.isEmpty() ? new File(s) : null;
    }

    public boolean isEmptyFeeder()
    {
        return th == null ? false : th.code == 0;
    }

    public int getErrorCode()
    {
        return th.code;
    }

    public String getErrorMessage()
    {
        return th.error;
    }

    private void releaseBlockedThreads()
    {
        int i = blockedThreadCount.getAndSet(0);
        if(i > 0)
        {
            for(int j = 0; j < i; j++)
                try
                {
                    queue.put("");
                }
                catch(InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }

        }
    }

    public static String getExt(File file)
    {
        String s = file.getName();
        int i = s.lastIndexOf('.');
        if(i > 0 && i + 1 < s.length())
            return s.substring(i + 1);
        else
            return "";
    }

    MultiFileTransferHandler th;
    private LinkedBlockingQueue queue;
    boolean transferFinished;
    private AtomicInteger blockedThreadCount;
    public static final String EOP = "";

}
