/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.DesDecrypter;
import com.secuera.middleware.core.common.AesEncrypter;
import com.secuera.middleware.core.common.AesDecrypter;
import com.secuera.middleware.beans.piv.MasterKeyCeremonyBean;
import com.secuera.middleware.beans.piv.MasterKeyCeremonyResultBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.util.Arrays;

/**
 *
 * @author Harpreet Singh
 */
public class MasterKeyCeremony {

    // *****************************************************************
    // Create instances of other classes.
    // *****************************************************************
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    MasterKeyCeremonyBean ceremonyKeys = new MasterKeyCeremonyBean();
    MasterKeyCeremonyResultBean ceremonyResult = new MasterKeyCeremonyResultBean();
    byte[] transportKey;
    byte[] masterKey;

    // Constructor get initial class values
    public MasterKeyCeremony(MasterKeyCeremonyBean cerKeys) {
        ceremonyKeys = cerKeys;

    }

    public MasterKeyCeremonyResultBean computeMasterKey() throws MiddleWareException {
        try {
            byte[] compkcv1 = computeKeyCheck(ceremonyKeys.getCompKey1());
            byte[] compkcv2 = computeKeyCheck(ceremonyKeys.getCompKey2());
            byte[] compkcv3 = computeKeyCheck(ceremonyKeys.getCompKey3());
            boolean kcvError = false;

            // Now compare the Computed key check values with the provided values.
            if (!Arrays.equals(ceremonyKeys.getKcv1(), compkcv1)) {
                ceremonyResult.setKcv1Error(true);
                kcvError = true;
            }

            
            if (!Arrays.equals(ceremonyKeys.getKcv2(), compkcv2)) {
                ceremonyResult.setKcv2Error(true);
                kcvError = true;

            }

            if (!Arrays.equals(ceremonyKeys.getKcv3(), compkcv3)) {
                ceremonyResult.setKcv3Error(true);
                kcvError = true;
            }

            if (kcvError) {
                return ceremonyResult;
            }

                       
            // Now Compute Master Key.
            computeTransportKey();

            byte[] compZCMK = computeKeyCheck(transportKey);

            if (!Arrays.equals(compZCMK, ceremonyKeys.getZcmkcv())) {

                ceremonyResult.setZckmError(true);
                kcvError = true;

            }

            if (kcvError) {
                return ceremonyResult;
            }


            if (ceremonyKeys.getAlgo().equalsIgnoreCase(UcmsMiddlewareConstants.AES128ECB)) {
               
                masterKey = aesdecrypt(transportKey,ceremonyKeys.getCryptogram() );
                //Now compute KCV on Master key.
                 byte[] masterKcv = computeKeyCheck(masterKey);
                 
                 
                 if (!Arrays.equals(ceremonyKeys.getkcv(), masterKcv)) {
                    ceremonyResult.setMasterKcvError(true);

                } else {
                       ceremonyResult.setMasterKey(util.arrayToHex(masterKey));

                }


            } else if (ceremonyKeys.getAlgo().equalsIgnoreCase(UcmsMiddlewareConstants.TripleDESECB)) {

                masterKey = desdecrypt(transportKey,ceremonyKeys.getCryptogram());

                //Now compute KCV on Master key.
                 byte[] masterKcv = computeKeyCheck(masterKey);

                if (!Arrays.equals(ceremonyKeys.getkcv(), masterKcv)) {
                    ceremonyResult.setMasterKcvError(true);

                } else {

                    ceremonyResult.setMasterKey(util.arrayToHex(masterKey));

                }
            
            } else {

                ceremonyResult.setAlgoError(true);
                return ceremonyResult;

            }

        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        
        
        return ceremonyResult;
    }

    private void computeTransportKey() throws MiddleWareException {
        // XOR the keyComp1 with keyComp2.
        byte[] initResult = new byte[ceremonyKeys.getCompKey1().length];

        for (int i = 0; i < ceremonyKeys.getCompKey1().length; i++) {
            initResult[i] = (byte) (ceremonyKeys.getCompKey1()[i] ^ ceremonyKeys.getCompKey2()[i]);

        }

        // XOR with keyComp2.
        byte[] result = new byte[ceremonyKeys.compKey3.length];

        for (int i = 0; i < ceremonyKeys.getCompKey3().length; i++) {
            result[i] = (byte) (ceremonyKeys.getCompKey3()[i] ^ initResult[i]);

        }
        
        transportKey=result;
        

    }

    private byte[] computeKeyCheck(byte[] keyComp) throws MiddleWareException {

        byte[] key_verification_data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

        // COMPUTE Key Check Value.
        byte[] key_check = util.extract_data(aesencrypt(keyComp, key_verification_data, false), 0, 3);

        return key_check;

    }

    public byte[] aesencrypt(byte[] key, byte[] input_data, boolean vector) throws MiddleWareException {

        // Create encrypter/decrypter class
        AesEncrypter encrypter;
        byte[] res = null;
        try {

            if (vector) {
                encrypter = new AesEncrypter(key, UcmsMiddlewareConstants.IV_AES);
            }
            {
                encrypter = new AesEncrypter(key);
            }

            // Encrypt
            res = encrypter.encrypt(input_data);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return res;

    }

    public byte[] aesdecrypt(byte[] key, byte[] input_data) throws MiddleWareException {

        // Create encrypter/decrypter class
        AesDecrypter decrypter;
        byte[] res = null;
        try {
            decrypter = new AesDecrypter(key);
           
            // Decrypt
            res = decrypter.decrypt(input_data);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return res;

    }

    public byte[] desdecrypt(byte[] key, byte[] input_data) throws MiddleWareException {

        // Create encrypter/decrypter class
        DesDecrypter decrypter;
        byte[] res = null;
        try {
            decrypter = new DesDecrypter(key);
            
            res = decrypter.decrypt(input_data);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return res;

    }


    /* Adding secure pad to data to be encrypted for AES CBC Mode */
    byte[] add_securepad(byte[] data) {

        byte[] append_80 = {(byte) 0x80};
        byte[] append_00 = {0x00};

        int i_len = data.length;
        int k = i_len % 16; // Divide by 16, because one standard bye block of
        // data is 16.
        int i_cnt = 16 - k; // Calculate how many more bytes required to make a
        // block of 16 bytes.

        if (k == 0) {
            return data;
        } else {
            for (int j = 0; j < i_cnt; j++) {
                // For the first append, "use append_80"
                if (j == 0) {
                    data = util.combine_data(data, append_80);

                } else {
                    data = util.combine_data(data, append_00);
                }

            }

            return data;

        }

    }

    /*
     * Determine the offset position of last block of 16 byte data from byte[]
     * array.
     */
    int getOffset(byte[] data) {

        // Subtract by 16,because one standard bye block of data is 16.
        int i_len = data.length - 16;

        return i_len;

    }
}
