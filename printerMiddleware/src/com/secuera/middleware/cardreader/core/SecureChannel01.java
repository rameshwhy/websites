/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.InitializationBean;
import com.secuera.middleware.cardreader.beans.SecureChannelBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import java.security.SecureRandom;
import java.util.Arrays;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Satish K
 */
public class SecureChannel01 {

    //Global Platform Keys (Security Domain Keys)
    private byte[] ENC_KEY;
    private byte[] MAC_KEY;
    //Master Keys
    private byte[] MASTER_KEY;
    //PIV Admin Keys
    private byte[] IV = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    private byte[] enc_card_pad = {(byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00};
    //Derivation keys
    byte[] AUTH_ENC_KEY;
    byte[] AUTH_MAC_KEY;
    byte[] AUTH_DEK_KEY;
    byte[] S_ENC_KEY;
    byte[] S_MAC_KEY;
    byte[] PREV_MAC;
    byte[] card_challenge;
    byte[] host_challenge;
    byte[] card_init_reponse;
    // 7 bytes of pin pad.
    byte[] PIN_PAD = {(byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    GeneralAuth auth = new GeneralAuth();
    PivUtilInterface piv = new PivUtil();
    ExceptionMessages expMsg = new ExceptionMessages();

    public SecureChannel01() {
        super();
    }

    // Constructor get initial class values
    public SecureChannel01(InitializationBean InitValues) {
        //Global Platform Keys (Security Domain Keys)
        ENC_KEY = InitValues.getENC_KEY();
        MAC_KEY = InitValues.getMAC_KEY();
        MASTER_KEY = InitValues.getMASTER_KEY();
    }

    public SecureChannelBean openSecureChannel01(CardChannel channel) throws MiddleWareException {
        SecureChannelBean channelbean = new SecureChannelBean();
        try {
            card_init_reponse = initUpdate(channel);
            openSCP01(channel);


            channelbean.setPREV_MAC(PREV_MAC);
            channelbean.setS_MAC_KEY(S_MAC_KEY);
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getErrorMsg());
        }

        return channelbean;
    }

    private void openSCP01(CardChannel channel) throws MiddleWareException {

        // ************ COMPUTE AUTH/ENC KEY SESSION KEY
        // ******************************
        // First step: Initialize the card and perform AES encryption.
        StringBuffer sb = new StringBuffer();
        byte[] data_key;

        // *----------------------------------------------
        // * Retrieve security domain keys
        // *----------------------------------------------
        byte[] card_diverse_init = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00};
        byte[] card_diverse = util.extract_data(card_init_reponse, 0, 10);

        boolean comp_result = Arrays.equals(card_diverse_init, card_diverse);

        if (!comp_result) {
            // Calculate Diversified Authentication Key
            byte[] card_diverse_6 = util.extract_data(card_diverse, 4, 6);
            byte[] div_1 = {(byte) 0xF0, 0x01};
            byte[] div_2 = {(byte) 0x0F, 0x01};

            byte[] div_3 = {(byte) 0xF0, 0x02};
            byte[] div_4 = {(byte) 0x0F, 0x02};

            byte[] div_auth_key = util.combine_data(card_diverse_6, div_1);
            div_auth_key = util.combine_data(div_auth_key, card_diverse_6);
            div_auth_key = util.combine_data(div_auth_key, div_2);
            // div_auth_key=util.combine_data(div_auth_key,zero_card_pad);

            AUTH_ENC_KEY = desencrypt(MASTER_KEY, div_auth_key);

            // Calculate Diversified MAC Key
            byte[] div_mac_key = util.combine_data(card_diverse_6, div_3);
            div_mac_key = util.combine_data(div_mac_key, card_diverse_6);
            div_mac_key = util.combine_data(div_mac_key, div_4);

            AUTH_MAC_KEY = desencrypt(MASTER_KEY, div_mac_key);

        } else {
            // Otherwise Assign Static keys.
            AUTH_ENC_KEY = ENC_KEY;
            AUTH_MAC_KEY = MAC_KEY;
        }

        // Now Prepare Diversification Data.

        byte[] card_1st = util.extract_data(card_challenge, 0, 4);
        byte[] card_2nd = util.extract_data(card_challenge, 4, 4);

        byte[] host_1st = util.extract_data(host_challenge, 0, 4);
        byte[] host_2nd = util.extract_data(host_challenge, 4, 4);

        // Concatenate all the above extractions in the following combination.
        data_key = util.combine_data(card_2nd, host_1st);
        data_key = util.combine_data(data_key, card_1st);
        data_key = util.combine_data(data_key, host_2nd);

        // S_ENC_KEY: Now Compute Session Encryption key.
        byte[] AUTH_ENC_KEY_24 = util.combine_data(AUTH_ENC_KEY,
                util.extract_data(AUTH_ENC_KEY, 0, 8));
        S_ENC_KEY = desencrypt(AUTH_ENC_KEY_24, data_key);

        // MAC_ENC_KEY: Now Compute MAC Session key.
        byte[] AUTH_MAC_KEY_24 = util.combine_data(AUTH_MAC_KEY,
                util.extract_data(AUTH_MAC_KEY, 0, 8));
        S_MAC_KEY = desencrypt(AUTH_MAC_KEY_24, data_key);

        // /////////////////////////////////////////////////////////

        // *----------------------------------------------
        // * Check Card cryptogram to verify that we have the right Auth key
        // (optional)
        // *----------------------------------------------
        // .SET_DATA I(51;8) I(59;8) ; HOST_CHALLENGE CARD_CHALLENGE
        // .SET_KEY I(3:18) ; Authentication Session Key

        byte[] enc_card_test = util.combine_data(host_challenge, card_challenge);

        enc_card_test = util.combine_data(enc_card_test, enc_card_pad);

        AUTH_MAC_KEY_24 = util.combine_data(S_ENC_KEY,
                util.extract_data(S_ENC_KEY, 0, 8));
        //byte[] enc_card_test_1 = macencrypt(AUTH_MAC_KEY_24, IV, enc_card_test);

        // System.out.println("Card Test: " + util.arrayToHex(enc_card_test_1)
        // );

        // /// End of Card TEST

        // ////////////////////////////////////////////////////////////////////////////////
        // encrypt card challenge and Host challenge with Session key, using MAC
        // encryption
        byte[] enc_card_comb = util.combine_data(card_challenge, host_challenge);
        enc_card_comb = util.combine_data(enc_card_comb, enc_card_pad);

        byte[] enc_card_ext = macencrypt(AUTH_MAC_KEY_24, IV, enc_card_comb);

        byte[] enc_card = util.extract_data(enc_card_ext, 16, 8);

        // ==========================================================================================
        // STEP 3: PERFORM EXTERNAL AUTHENTICATION
        // ==========================================================================================

        // ** CALCULATE THE CRYPTOGRAM

        byte[] mac_buffer = {(byte) 0x84, (byte) 0x82, 0x01, 0x00, 0x10};

        mac_buffer = util.combine_data(mac_buffer, enc_card);

        // Now Using MAC session key, encrypt the mac buffer.
        byte[] mac_buffer_pad = {(byte) 0x80, 0x00, 0x00};

        byte[] enc_mac = util.combine_data(mac_buffer, mac_buffer_pad);

        AUTH_MAC_KEY_24 = util.combine_data(S_MAC_KEY,
                util.extract_data(S_MAC_KEY, 0, 8));
        enc_mac = macencrypt(AUTH_MAC_KEY_24, IV, enc_mac);

        byte[] enc_mac_8 = util.extract_data(enc_mac, 8, 8);

        // ///////////
        PREV_MAC = enc_mac_8; // Store the Mac for next comand to card.

        mac_buffer = util.combine_data(mac_buffer, enc_mac_8);

        CommandAPDU EXT_APDU = new CommandAPDU(mac_buffer);

        try {

            sendCard.sendCommand(channel, EXT_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessages.INIT_SECURE_CH101);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }




    }

    byte[] desencrypt(byte[] key, byte[] from_card) throws MiddleWareException {
        try {
            // Create encrypter/decrypter class
            DesEncrypter encrypter = new DesEncrypter(key);
            // Encrypt
            byte[] res = encrypter.encrypt(from_card);

            return res;
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    private byte[] macencrypt(byte[] key, byte[] vector, byte[] from_card) throws MiddleWareException {

        MacEncrypter encrypter = new MacEncrypter(key, vector);

        // Encrypt
        byte[] res = null;
        try {
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        return res;

    }

    /* Initialize Update Command method */
    private byte[] initUpdate(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] res = null;
        byte[] apdu_update;

        SecureRandom random = new SecureRandom();
        byte[] seed = random.generateSeed(20);
        random.setSeed(seed);
        host_challenge = new byte[8];
        random.nextBytes(host_challenge);

        byte[] apdu_init = {(byte) 0x80, 0x50, 0x00, 0x00, 0x08};

        // MAC MODEONLY
        // byte[] Le= {0x1C}; //Nbr of bytes expected back by this command

        apdu_update = util.combine_data(apdu_init, host_challenge);

        // apdu_update=util.combine_data(apdu_update,Le);
        CommandAPDU INIT_APDU = new CommandAPDU(apdu_update);

        try {
            res = sendCard.sendCommand(channel, INIT_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // Get the card challenge and store it.
        card_challenge = util.extract_data(res, 12, 8);
        return res;

    }
}
