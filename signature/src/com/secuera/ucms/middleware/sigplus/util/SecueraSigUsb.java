package com.secuera.ucms.middleware.sigplus.util;

import com.topaz.sigplus.util.SigUsb;
import java.io.File;
import java.io.PrintStream;
import java.security.AccessController;
import java.security.PrivilegedAction;

public class SecueraSigUsb 
{
  static
  {
    AccessController.doPrivileged(new PrivilegedAction()
    {
      public Object run()
      {
          File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
          String os = System.getProperty("os.name").toLowerCase();
          String arch = System.getProperty("os.arch").toLowerCase();
          if (os.indexOf("windows") >= 0)
            {
              if (arch.indexOf("64") >= 0) {
                  System.out.print("64 bit system >>>>>>>>>>  ");
                  Object localObject = new File(TMP_DIR, "SigUsb_64.dll");
                  System.load(((File)localObject).getAbsolutePath());
                 //System.load("SecueraSigUsb");
              }
              else {
                  System.out.print("32 bit system >>>>>>>>>>  ");
                  Object localObject = new File(TMP_DIR, "SigUsb_32.dll");
                  System.load(((File)localObject).getAbsolutePath());
                //System.load("SecueraSigUsb");
              }
            }
        
        return null;
      }
    });
  }

  public synchronized native boolean closeUsbPort();

  public synchronized native long getPointData();

  public synchronized native boolean openUsbPort(String paramString);

  public synchronized native boolean putCmdString(byte[] paramArrayOfByte);

  public synchronized native byte[] getSerialData();

  public static void main(String[] args)
  {
    int numPoints = 0;

    SecueraSigUsb testUsb = new SecueraSigUsb();
    testUsb.openUsbPort("COM1");
    do
    {
      long point = testUsb.getPointData();
      if (point != 0L)
      {
        numPoints++;
        System.out.println("Point = " + point + " numPoints = " + numPoints);
      }
    }
    while (numPoints != 10);
    testUsb.closeUsbPort();
  }
}
 