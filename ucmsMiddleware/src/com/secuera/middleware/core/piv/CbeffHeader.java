/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

/**
 *
 * @author admin
 */
public class CbeffHeader {
	 String headerVersion;
	    String securityOptions;
		int bdbLength;
	        int sbLength;

	   
		String bdbFormatOwner;
		String bdbFormatType;
		String biometricCreationDate;
		String validFrom;
		String validTo;
		String biometricType;
		String biometricDataType;
		String biometricDataQuality;
		String creator;
		String FASCN;

	    public String getFASCN() {
	        return FASCN;
	    }

	    public void setFASCN(String FASCN) {
	        this.FASCN = FASCN;
	    }

	    public String getBiometricDataQuality() {
	        return biometricDataQuality;
	    }

	    public void setBiometricDataQuality(String biometricDataQuality) {
	        this.biometricDataQuality = biometricDataQuality;
	    }

	    public String getBiometricDataType() {
	        return biometricDataType;
	    }

	    public void setBiometricDataType(String biometricDataType) {
	        this.biometricDataType = biometricDataType;
	    }

	    public String getBiometricType() {
	        return biometricType;
	    }

	    public void setBiometricType(String biometricType) {
	        this.biometricType = biometricType;
	    }

	    public String getCreator() {
	        return creator;
	    }

	    public void setCreator(String creator) {
	        this.creator = creator;
	    }

	    public String getValidFrom() {
	        return validFrom;
	    }

	    public void setValidFrom(String validFrom) {
	        this.validFrom = validFrom;
	    }

	    public String getValidTo() {
	        return validTo;
	    }

	    public void setValidTo(String validTo) {
	        this.validTo = validTo;
	    }
	        
	        

	    public String getBiometricCreationDate() {
	        return biometricCreationDate;
	    }

	    public void setBiometricCreationDate(String biometricCreationDate) {
	        this.biometricCreationDate = biometricCreationDate;
	    }
	        

	    public String getBdbFormatType() {
	        return bdbFormatType;
	    }

	    public void setBdbFormatType(String bdbFormatType) {
	        this.bdbFormatType = bdbFormatType;
	    }
	        

	    public String getBdbFormatOwner() {
	        return bdbFormatOwner;
	    }

	    public void setBdbFormatOwner(String bdbFormatOwner) {
	        this.bdbFormatOwner = bdbFormatOwner;
	    }
	        
	        
	        
	         public int getSbLength() {
	        return sbLength;
	    }

	    public void setSbLength(int sbLength) {
	        this.sbLength = sbLength;
	    }
	        
	        
	        
	        public int getBdbLength() {
	        return bdbLength;
	    }

	    public void setBdbLength(int bdbLength) {
	        this.bdbLength = bdbLength;
	    }

	    public String getSecurityOptions() {
	        return securityOptions;
	    }

	    public void setSecurityOptions(String securityOptions) {
	        this.securityOptions = securityOptions;
	    }

	    public String getHeaderVersion() {
	        return headerVersion;
	    }

	    public void setHeaderVersion(String headerVersion) {
	        this.headerVersion = headerVersion;
	    }
		
	        

	    public CbeffHeader() {
	        
	        
	    }
	    public boolean initWithByteString(String byteString)
	    {
	       boolean retval = false;
		int idx = 0;
		String chunk ="";

//		byteString = removeWhitespace(byteString);


		if (byteString.length() >= (88 * 2))
		{
			// Patron header version must be 0x03:
			String byt = byteString.substring(idx, idx + 2);

			if (byt.equals("03"));
			{
				// 1 byte for Header version
				this.setHeaderVersion(byt);
				idx += 2;

				// 2 bytes for 'SBH Security Options':
				// XXX could error check: securityOptions must be 0x0D or 0x0F:
				this.setSecurityOptions(byteString.substring(idx, idx + 2));
				idx += 2;

				// 4 bytes of BDB length:
				chunk = byteString.substring(idx, idx + 8);
				idx += 8;
				this.setBdbLength(Integer.parseInt(chunk, 16));

				// 2 bytes of SB length:
				chunk = byteString.substring(idx, idx + 4);
				idx += 4;
				this.setSbLength(Integer.parseInt(chunk, 16));

				// 2 bytes for 'BDB Format Owner':
				// XXX could error check: format owner must be 0x00B1:
				this.setBdbFormatOwner(byteString.substring(idx, idx + 4));
				idx += 4;

				// 2 bytes for 'BDB Format Type':
				// XXX could error check: format type for facial image must be
				// 	0x0501:
				this.setBdbFormatType(byteString.substring(idx, idx + 4));
				idx += 4;

				// 8 bytes for 'Biometric Creation Date':
				chunk = byteString.substring(idx, idx + 16);
				//this.setBiometricCreationDate(parseCBEFFDate(chunk));
	                        this.setBiometricCreationDate(chunk);
				idx += 16;

				// 8 bytes for 'Biometric Validity Period' FROM date:
				chunk = byteString.substring(idx, idx + 16);
				// this.setValidFrom(parseCBEFFDate(chunk));
	                        this.setValidFrom(chunk);
				idx += 16;

				// 8 bytes for 'Biometric Validity Period' WTO
				chunk = byteString.substring(idx, idx + 16);
				//this.setValidTo(parseCBEFFDate(chunk));
	                        this.setValidTo(chunk);
				idx += 16;

				
				this.setBiometricType(byteString.substring(idx, idx + 6));
				idx += 6;

				
				chunk = byteString.substring(idx, idx + 1);
				if (chunk.equals("2"))
					this.setBiometricDataType("Raw");
				if (chunk.equals("4"))
					this.setBiometricDataType("Intermediate");
				if (chunk.equals("8"))
					this.setBiometricDataType("Processed");
				idx += 2;

				this.setBiometricDataQuality(byteString.substring(idx, idx + 2));
				idx += 2;

				chunk = byteString.substring(idx, idx + 36);
				this.setCreator(hexStringToASCII(chunk));
				idx += 36;

				this.setFASCN(byteString.substring(idx, idx + 50));

				retval = true;
			}
		}

		return retval;
	    }
	    
	    
	  public  String hexStringToASCII(String hexString)
	{
	         String hx;
	         hx = hexString;
		// hexString = removeWhitespace(hexString);

		// remove the NULL terminator bytes: these aren't necessary for unescape,
		// 	and if we leave them in, unescape will leave us a string with extra
		// 	NULLs on the end which will screw up (terminate prematurely) any
		// 	strings with the result used as a substring.
		// The 1st regexp cuts off any characters after the first '00' byte, the
		// 	second HTTP-escapes the string by prepending '%' to every 2-char byte
		// 	hex substring. Note that the first regexp will drop any non '00' hex
		// 	strings after the initial '00' hex string.
		//hx = hexString.replace(/(0{2})*$/g, "");
		//hx = hx.replace(/(..)/g, "%$1");

		//return unescape(hexString);
	         return hx;
	}

	    
}
