package com.secuera.middleware.beans.piv;

import com.secuera.middleware.core.common.CommonUtil;
import java.io.Serializable;


public class PersonalizationBean implements Serializable  {
    CommonUtil cmnUtil = new CommonUtil();
    private ChuidBean chuid;
    private PrintedInfoBean printInfo;
    private FacialImageBean facialImage;
    private UserFingerPintBean fingerPrints;
    private byte[] pivAuthentication9A;
    private byte[] digSignature9C;
    private byte[] keyManagement9D;
    private byte[] cardAuthentication9E;
        
    public byte[] getCardAuthentication9E() {
        return cardAuthentication9E;
    }

    public void setCardAuthentication9E(String cardAuthentication9E) {
        this.cardAuthentication9E = cmnUtil.hex1StringToByteArray(cardAuthentication9E);
    }

    public ChuidBean getChuid() {
        return chuid;
    }

    public void setChuid(ChuidBean chuid) {
        this.chuid = chuid;
    }

    public byte[] getDigSignature9C() {
        return digSignature9C;
    }

    public void setDigSignature9C(String digSignature9C) {
        this.digSignature9C = cmnUtil.hex1StringToByteArray(digSignature9C);
    }

    public FacialImageBean getFacialImage() {
        return facialImage;
    }

    public void setFacialImage(FacialImageBean facialImage) {
        this.facialImage = facialImage;
    }

    
    public byte[] getKeyManagement9D() {
        return keyManagement9D;
    }

    public void setKeyManagement9D(String keyManagement9D) {
        this.keyManagement9D = cmnUtil.hex1StringToByteArray(keyManagement9D);
    }

    
    public byte[] getPivAuthentication9A() {
        return pivAuthentication9A;
    }
    

    public void setPivAuthentication9A(String pivAuthentication9A) {
        this.pivAuthentication9A = cmnUtil.hex1StringToByteArray(pivAuthentication9A);
    }

    public PrintedInfoBean getPrintInfo() {
        return printInfo;
    }

    public void setPrintInfo(PrintedInfoBean printInfo) {
        this.printInfo = printInfo;
    }

    
    public UserFingerPintBean getFingerPrints() {
        return fingerPrints;
    }

    public void setFingerPrints(UserFingerPintBean fingerPrints) {
        this.fingerPrints = fingerPrints;
    }

      /*sat123
    public InitializationBean getInitialization() {
        return initialization;
    }

    public void setInitialization(InitializationBean initialization) {
        this.initialization = initialization;
    }

 
    public boolean isInitializationFlag() {
        return initializationFlag;
    }

    public void setInitializationFlag(boolean initializationFlag) {
        this.initializationFlag = initializationFlag;
    }
     */ 

     
}
