/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.common;

import java.util.List;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardNotPresentException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;

/**
 *
 * @author admin
 */
public class CardComm {

    public Card card = null;
    public CardChannel channel = null;

    // Start the communication with Smart Card
    public CardChannel startComm(String deviceName) throws MiddleWareException {
        int retValue = 0;

        // show the list of available terminals
        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = null;
        try {
            terminals = factory.terminals().list();
        } catch (CardException e) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.NO_CARD_TERMINAL_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }

        for (int i = 0; i < terminals.size(); i++) {
            CardTerminal terminal = terminals.get(i);
            if (terminal.getName().equalsIgnoreCase(deviceName)) {
                System.out.println("name " + terminal.getName() + '/' + deviceName);
                try {
                    card = terminal.connect("T=1");
                    retValue = 1;
                } catch (CardNotPresentException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(ExceptionMessages.NO_CARD_EXCEPTION);
                    throw new MiddleWareException(sb.toString());
                } catch (CardException e) {
                    throw new MiddleWareException("Error communicating with card.");
                }
                break;
            }

        }

        //get channel & populate public variable
        if (retValue == 1) {
            channel = card.getBasicChannel();
        } else {
            throw new MiddleWareException("unable to connect to terminal.");
        }
        return channel;
    }

    //Disconnect Card to avoid exception when we try to start communication again.
    public void stopComm() {
        try {
            card.disconnect(false);
        } catch (CardException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to Disconnect Card.");
        }

    }

    // Start the communication with Smart Card
    public boolean isCardPresent(String deviceName) throws MiddleWareException {
        boolean cardPresent = false;
        // show the list of available terminals
        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = null;
        try {
            terminals = factory.terminals().list();
        } catch (CardException e) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.NO_CARD_TERMINAL_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }

        for (int i = 0; i < terminals.size(); i++) {
            CardTerminal terminal = terminals.get(i);
            if (terminal.getName().equalsIgnoreCase(deviceName)) {
                System.out.println("name " + terminal.getName() + '/' + deviceName);
                try {
                    card = terminal.connect("T=1");
                    cardPresent = true;
                } catch (CardNotPresentException e) {
                    cardPresent = false;
                } catch (CardException e) {
                    throw new MiddleWareException("Error communicating with card. "+e.getMessage());
                }
                break;
            }
        }
        return cardPresent;
    }
}
