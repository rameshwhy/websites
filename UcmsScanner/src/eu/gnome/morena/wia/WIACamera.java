// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.wia;

import eu.gnome.morena.*;

// Referenced classes of package eu.gnome.morena.wia:
//            WIADevice

public class WIACamera extends WIADevice
    implements Camera, Runnable
{

    private native long _initialize(String s)
        throws Exception;

    private native void _transfer(long l, boolean flag)
        throws MorenaException;

    WIACamera(String s, String s1)
    {
        super(s, s1);
        try
        {
            handle = _initialize(s1);
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            System.out.println((new StringBuilder()).append("Error while initializing scanner: ").toString());
        }
        available = true;
    }

    protected void transfer()
    {
        try
        {
            _transfer(handle, (Configuration.getMode() & 1) > 0);
        }
        catch(MorenaException morenaexception)
        {
            transferDoneListener.transferFailed(morenaexception.getErrorCode(), morenaexception.getMessage());
        }
        catch(Throwable throwable)
        {
            transferDoneListener.transferFailed(-1, throwable.toString());
        }
    }

    public void run()
    {
        transfer();
    }

    public void startTransfer(TransferDoneListener transferdonelistener)
        throws Exception
    {
        super.startTransfer(transferdonelistener);
        (new Thread(this)).start();
    }
}
