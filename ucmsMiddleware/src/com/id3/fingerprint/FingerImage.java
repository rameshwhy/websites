package com.id3.fingerprint;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;


public class FingerImage
{
  private long _handle;
  private boolean _disposed = false;
  private boolean _disposable = true;
  private long _templateHandle;
  private FingerTemplate _template;
  private long _templateRecordHandle;
  private FingerTemplateRecord _templateRecord;
  private FingerBounds[] _bounds;
  private byte[] _orientations;
  private FingerImage[] _imageArray;
  private byte[] _qualityMap = null;

  static
  {
    String tmpStr = "id3Finger.dll";
     File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
     File localFile = new File(TMP_DIR, tmpStr);


      System.load(localFile.getAbsolutePath());
  }

  public FingerImage()
    throws FingerException
  {
    long[] handle_ptr = new long[1];
    int result = nInitialize(handle_ptr);

    if (result != 0) {
      throw new FingerException(result);
    }
    this._handle = handle_ptr[0];
  }

  public FingerImage(long handle, boolean disposable)
    throws FingerException
  {
    this._handle = handle;
    this._disposable = disposable;
  }

  public void dispose()
  {
    nDispose(this._handle);
    this._handle = 0L;
    this._template = null;
    this._templateRecord = null;
    this._bounds = null;
    this._imageArray = null;
    this._qualityMap = null;
    this._disposed = true;
  }

  protected void finalize()
    throws Throwable
  {
    try
    {
      if (this._disposable)
        dispose();
    } catch (Exception localException) {
    }
    finally {
      super.finalize();
    }
  }

  public boolean equals(Object obj)
  {
    if ((obj == null) || (obj.getClass() != getClass())) {
      return false;
    }
    FingerImage device = (FingerImage)obj;
    return this._handle == device._handle;
  }

  public int hashCode()
  {
    return (int)this._handle;
  }

  public long getHandle()
  {
    return this._handle;
  }

  public FingerImage clone()
  {
    try
    {
      long[] hClonedImage = new long[1];
      int result = nClone(this._handle, hClonedImage);

      if (result == 0)
        return new FingerImage(hClonedImage[0], true);
    }
    catch (FingerException localFingerException) {
    }
    return null;
  }

  public static FingerImage fromFile(String filename)
    throws FingerException
  {
    long[] handle = new long[1];
    int result = nFromFile(filename, 0, handle);

    if (result != 0) {
      throw new FingerException(result);
    }
    return new FingerImage(handle[0], true);
  }

  public static FingerImage fromFile(String filename, int format)
    throws FingerException
  {
    long[] handle = new long[1];
    int result = nFromFile(filename, format, handle);

    if (result != 0) {
      throw new FingerException(result);
    }
    return new FingerImage(handle[0], true);
  }

  public static FingerImage fromBuffer(byte[] data, int format)
    throws FingerException
  {
    long[] handle = new long[1];
    int result = nFromBuffer(data, format, handle);

    if (result != 0) {
      throw new FingerException(result);
    }
    return new FingerImage(handle[0], true);
  }

  public static FingerImage fromRawBuffer(byte[] data, int width, int height)
    throws FingerException
  {
    long[] handle = new long[1];
    int result = nFromRawBuffer(data, width, height, handle);

    if (result != 0) {
      throw new FingerException(result);
    }
    return new FingerImage(handle[0], true);
  }

  public int computeNFIQ()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nComputeNFIQ(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public FingerTemplate createTemplate()
    throws FingerException
  {
    long[] hTemplate = new long[1];
    int result = nCreateTemplate(this._handle, hTemplate);

    if (result != 0) {
      throw new FingerException(result);
    }
    this._templateHandle = hTemplate[0];

    if (this._templateHandle == 0L)
      this._template = null;
    else {
      this._template = new FingerTemplate(this._templateHandle, false).clone();
    }
    return this._template;
  }

  public FingerTemplateRecord createTemplateRecord()
    throws FingerException
  {
    long[] hTemplateRecord = new long[1];
    int result = nCreateTemplateRecord(this._handle, hTemplateRecord);

    if (result != 0) {
      throw new FingerException(result);
    }
    this._templateRecordHandle = hTemplateRecord[0];

    if (this._templateRecordHandle == 0L)
      this._templateRecord = null;
    else {
      this._templateRecord = new FingerTemplateRecord(this._templateRecordHandle, 
        false).clone();
    }
    return this._templateRecord;
  }

  public int segmentSlap()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nSegmentSlap(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void save(String filename, int format, float compressionLevel)
    throws FingerException
  {
    int result = nSave(this._handle, format, compressionLevel, filename);

    if (result != 0)
      throw new FingerException(result);
  }

  public void save(String filename, int format)
    throws FingerException
  {
    save(filename, format, 0.0F);
  }

  public void save(String filename)
    throws FingerException
  {
    save(filename, 0, 0.0F);
  }

  public byte[] toBuffer(int format, float compressionLevel)
  {
    return nToBuffer(this._handle, format, compressionLevel);
  }

  public byte[] toBuffer(int format)
  {
    return toBuffer(format, 0.0F);
  }

  private static BufferedImage getGrayscale(int width, byte[] buffer)
  {
    int height = buffer.length / width;
    ColorSpace cs = ColorSpace.getInstance(1003);
    int[] nBits = { 8 };
    ColorModel cm = new ComponentColorModel(cs, nBits, false, true, 
      1, 0);
    SampleModel sm = cm.createCompatibleSampleModel(width, height);
    DataBufferByte db = new DataBufferByte(buffer, width * height);
    WritableRaster raster = Raster.createWritableRaster(sm, db, null);
    BufferedImage result = new BufferedImage(cm, raster, false, null);

    return result;
  }

  public java.awt.Image createImageAwt()
    throws FingerException
  {
    byte[] pixels = getPixels();

    if (pixels != null) {
      return getGrayscale(getWidth(), pixels);
    }
    return null;
  }

 /* public javafx.scene.image.Image createImageFx()
    throws FingerException
  {
    int width = getWidth();
    int height = getHeight();
    byte[] pixels = getPixels();
    byte[] colorPixels = new byte[width * 3 * height];

    Image image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    PixelWriter pixelWriter = image.getPixelWriter();

    PixelFormat pixelFormat = PixelFormat.getByteRgbInstance();
    int i = 0; for (int j = 0; i < width * height; i++) {
      colorPixels[(j++)] = pixels[i];
      colorPixels[(j++)] = pixels[i];
      colorPixels[(j++)] = pixels[i];
    }

    pixelWriter.setPixels(0, 0, width, height, pixelFormat, colorPixels, 0, 
      width * 3);

    return image;
  }
*/
  public InputStream toStream()
  {
    byte[] buffer = toBuffer(1);

    if (buffer != null) {
      return new ByteArrayInputStream(buffer);
    }
    return null;
  }

  public int getCbeffProductID()
    throws FingerException
  {
    int[] ptr = new int[1];

    int result = nGetCbeffProductID(this._handle, ptr);

    if (result != 0) {
      throw new FingerException(result);
    }
    return ptr[0];
  }

  public void setCbeffProductID(int value)
    throws FingerException
  {
    int result = nSetCbeffProductID(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getImpressionType()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetImpressionType(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setImpressionType(int value)
    throws FingerException
  {
    int result = nSetImpressionType(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getPosition()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetFingerPosition(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setPosition(int value)
    throws FingerException
  {
    int result = nSetFingerPosition(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getWidth()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetImageWidth(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public int getHeight()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetImageHeight(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public int getHorizontalResolution()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetHorizontalResolution(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setHorizontalResolution(int value)
    throws FingerException
  {
    int result = nSetHorizontalResolution(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getVerticalResolution()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetVerticalResolution(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setVerticalResolution(int value)
    throws FingerException
  {
    int result = nSetVerticalResolution(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getQuality()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetQuality(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setQuality(int value)
    throws FingerException
  {
    int result = nSetQuality(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public byte[] getQualityMap()
  {
    if (this._qualityMap == null) {
      this._qualityMap = nGetQualityMap(this._handle);
    }
    return this._qualityMap;
  }

  public int getCaptureDeviceCertification()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetCaptureDeviceCertification(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setCaptureDeviceCertification(int value)
    throws FingerException
  {
    int result = nSetCaptureDeviceCertification(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getCaptureDeviceTechnology()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetCaptureDeviceTechnology(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setCaptureDeviceTechnology(int value)
    throws FingerException
  {
    int result = nSetCaptureDeviceTechnology(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getCaptureDeviceVendor()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetCaptureDeviceVendor(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setCaptureDeviceVendor(int value)
    throws FingerException
  {
    int result = nSetCaptureDeviceVendor(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public int getCaptureDeviceType()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetCaptureDeviceType(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public void setCaptureDeviceType(int value)
    throws FingerException
  {
    int result = nSetCaptureDeviceType(this._handle, value);

    if (result != 0)
      throw new FingerException(result);
  }

  public byte[] getPixels()
  {
    return nGetPixels(this._handle);
  }

  public FingerTemplate getTemplate()
    throws FingerException
  {
    long[] handle_ptr = new long[1];
    int result = nGetTemplate(this._handle, handle_ptr);

    if (result != 0) {
      throw new FingerException(result);
    }
    if (handle_ptr[0] != this._templateHandle) {
      this._templateHandle = handle_ptr[0];

      if (this._templateHandle == 0L)
        this._template = null;
      else {
        this._template = new FingerTemplate(this._templateHandle, false).clone();
      }
    }
    return this._template;
  }

  public void setTemplate(FingerTemplate template)
    throws FingerException
  {
    int result = nSetTemplate(this._handle, template.getHandle());

    if (result != 0) {
      throw new FingerException(result);
    }
    this._template = null;
    this._templateHandle = 0L;
    this._templateRecord = null;
    this._templateRecordHandle = 0L;
  }

  public FingerTemplateRecord getTemplateRecord()
    throws FingerException
  {
    long[] handle_ptr = new long[1];
    int result = nGetTemplateRecord(this._handle, handle_ptr);

    if (result != 0) {
      throw new FingerException(result);
    }
    if (handle_ptr[0] != this._templateRecordHandle) {
      this._templateRecordHandle = handle_ptr[0];

      if (this._templateRecordHandle == 0L)
        this._templateRecord = null;
      else {
        this._templateRecord = new FingerTemplateRecord(
          this._templateRecordHandle, false).clone();
      }
    }
    return this._templateRecord;
  }

  public int getSegmentCount()
    throws FingerException
  {
    int[] value = new int[1];

    int result = nGetSegmentCount(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

  public FingerBounds[] getSegmentBounds()
    throws FingerException
  {
    if (this._bounds == null) {
      int count = getSegmentCount();
      int result = 0;

      if (count > 0) {
        this._bounds = new FingerBounds[count];

        for (int i = 0; i < count; i++) {
          this._bounds[i] = getSegmentBounds(i);
        }
      }
      if (result != 0) {
        throw new FingerException(result);
      }
    }
    return this._bounds;
  }

  public FingerImage[] getSegments()
    throws FingerException
  {
    if (this._imageArray == null) {
      int count = getSegmentCount();

      this._imageArray = new FingerImage[count];

      for (int i = 0; i < count; i++) {
        this._imageArray[i] = getSegmentImage(i).clone();
      }
    }
    return this._imageArray;
  }

  public byte[] getSegmentOrientations()
    throws FingerException
  {
    if (this._orientations == null) {
      int count = getSegmentCount();
      int result = 0;

      if (count > 0) {
        this._orientations = new byte[count];

        for (int i = 0; i < count; i++) {
          this._orientations[i] = getSegmentOrientation(i);
        }
      }
      if (result != 0) {
        throw new FingerException(result);
      }
    }
    return this._orientations;
  }

  public int getSpoofScore()
    throws FingerException
  {
    int[] value = new int[1];
    int result = nGetSpoofScore(this._handle, value);

    if (result != 0) {
      throw new FingerException(result);
    }
    return value[0];
  }

 /* public void drawMinutiae(Graphics g, java.awt.Color color)
  {
    drawMinutiae(g, color, 0, 0, 1.0F);
  }
*/
  /*public void drawMinutiae(Graphics g, java.awt.Color color, Rectangle size)
  {
    try
    {
      float zoomFactorX = size.width / getWidth();
      float zoomFactorY = size.height / getHeight();
      float zoomFactor = Math.min(zoomFactorX, zoomFactorY);
      int dx = (int)((size.width - getWidth() * zoomFactor) / 2.0F);
      int dy = (int)((size.height - getHeight() * zoomFactor) / 2.0F);

      drawMinutiae(g, color, dx, dy, zoomFactor);
    }
    catch (FingerException localFingerException)
    {
    }
  }*/

  public void drawMinutiae(Graphics g, java.awt.Color color, int dx, int dy, float zoomFactor)
  {
    try
    {
      if (getSegmentCount() > 0) {
        FingerTemplate[] alignedTemplates = getAlignedTemplates();

        if (alignedTemplates != null)
          for (int i = 0; i < alignedTemplates.length; i++) {
            FingerTemplate template = alignedTemplates[i];

            if (template != null)
              template.drawMinutiae(g, color, dx, dy, zoomFactor);
          }
      }
      else {
        FingerTemplate template = getTemplate();

        if (template != null)
          template.drawMinutiae(g, color, dx, dy, zoomFactor);
      }
    }
    catch (FingerException localFingerException)
    {
    }
  }

  public void drawSegmentBounds(Graphics g, java.awt.Color color, int dx, int dy, float zoomFactor)
  {
    try
    {
      if (getSegmentBounds() != null) {
        g.setColor(color);
        Graphics2D g2D = (Graphics2D)g;
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
          RenderingHints.VALUE_ANTIALIAS_ON);

        for (int i = 0; i < this._bounds.length; i++) {
          Polygon p = new Polygon();
          p.addPoint(
            (int)(this._bounds[i].bottomLeft.x * zoomFactor + dx), 
            (int)(this._bounds[i].bottomLeft.y * zoomFactor + dy));
          p.addPoint(
            (int)(this._bounds[i].bottomRight.x * zoomFactor + dx), 
            (int)(this._bounds[i].bottomRight.y * zoomFactor + dy));
          p.addPoint((int)(this._bounds[i].topRight.x * zoomFactor + dx), 
            (int)(this._bounds[i].topRight.y * zoomFactor + dy));
          p.addPoint((int)(this._bounds[i].topLeft.x * zoomFactor + dx), 
            (int)(this._bounds[i].topLeft.y * zoomFactor + dy));
          p.addPoint(p.xpoints[0], p.ypoints[0]);
          g.drawPolygon(p);
        }
      }
    }
    catch (FingerException localFingerException)
    {
    }
  }
/*
  public void drawSegmentBounds(Graphics g, java.awt.Color color, Rectangle size)
  {
    try
    {
      float zoomFactorX = size.width / getWidth();
      float zoomFactorY = size.height / getHeight();
      float zoomFactor = Math.min(zoomFactorX, zoomFactorY);
      int dx = (int)((size.width - getWidth() * zoomFactor) / 2.0F);
      int dy = (int)((size.height - getHeight() * zoomFactor) / 2.0F);

      drawSegmentBounds(g, color, dx, dy, zoomFactor);
    }
    catch (FingerException localFingerException)
    {
    }
  }

  public void drawMinutiae(GraphicsContext g, javafx.scene.paint.Color color, double width, double height)
  {
    try
    {
      float zoomFactorX = (float)width / getWidth();
      float zoomFactorY = (float)height / getHeight();
      float zoomFactor = Math.min(zoomFactorX, zoomFactorY);
      int dx = 0;
      int dy = 0;

      drawMinutiae(g, color, dx, dy, zoomFactor);
    }
    catch (FingerException localFingerException)
    {
    }
  }

  public void drawMinutiae(GraphicsContext g, javafx.scene.paint.Color color, int dx, int dy, float zoomFactor)
  {
    try
    {
      if (getSegmentCount() > 0) {
        FingerTemplate[] alignedTemplates = getAlignedTemplates();

        if (alignedTemplates != null)
          for (int i = 0; i < alignedTemplates.length; i++) {
            FingerTemplate template = alignedTemplates[i];

            if (template != null)
              template.drawMinutiae(g, color, dx, dy, zoomFactor);
          }
      }
      else {
        FingerTemplate template = getTemplate();

        if (template != null)
          template.drawMinutiae(g, color, dx, dy, zoomFactor);
      }
    }
    catch (FingerException localFingerException)
    {
    }
  }

  public void drawSegmentBounds(GraphicsContext g, javafx.scene.paint.Color color, int dx, int dy, float zoomFactor)
  {
    try
    {
      FingerBounds[] bounds = getSegmentBounds();

      if (bounds != null) {
        g.setStroke(color);

        for (int i = 0; i < bounds.length; i++) {
          g.moveTo(this._bounds[i].bottomLeft.x * zoomFactor + dx, 
            this._bounds[i].bottomLeft.y * zoomFactor + dy);
          g.lineTo(this._bounds[i].bottomRight.x * zoomFactor + dx, 
            this._bounds[i].bottomRight.y * zoomFactor + dy);
          g.lineTo(this._bounds[i].topRight.x * zoomFactor + dx, 
            this._bounds[i].topRight.y * zoomFactor + dy);
          g.lineTo(this._bounds[i].topLeft.x * zoomFactor + dx, 
            this._bounds[i].topLeft.y * zoomFactor + dy);
          g.lineTo(this._bounds[i].bottomLeft.x * zoomFactor + dx, 
            this._bounds[i].bottomLeft.y * zoomFactor + dy);
        }
      }
    }
    catch (FingerException localFingerException)
    {
    }
  }

  public void drawSegmentBounds(GraphicsContext g, javafx.scene.paint.Color color, double width, double height)
  {
    try
    {
      float zoomFactorX = (float)width / getWidth();
      float zoomFactorY = (float)height / getHeight();
      float zoomFactor = Math.min(zoomFactorX, zoomFactorY);
      int dx = (int)((width - getWidth() * zoomFactor) / 2.0D);
      int dy = (int)((height - getHeight() * zoomFactor) / 2.0D);

      drawSegmentBounds(g, color, dx, dy, zoomFactor);
    }
    catch (FingerException localFingerException)
    {
    }
  }
*/
  private FingerImage getSegmentImage(int index)
    throws FingerException
  {
    long[] handle_ptr = new long[1];
    int result = nGetSegment(this._handle, index, handle_ptr);

    if (result != 0) {
      throw new FingerException(result);
    }
    return new FingerImage(handle_ptr[0], false);
  }

  public FingerTemplate[] getAlignedTemplates()
    throws FingerException
  {
    FingerTemplate[] alignedTemplates = (FingerTemplate[])null;
    int count = getSegmentCount();

    if (count > 0) {
      alignedTemplates = new FingerTemplate[count];

      for (int i = 0; i < count; i++) {
        FingerTemplate template = getSegments()[i].getTemplate();

        if (template != null) {
          FingerTemplate rotatedTemplate = template
            .clone();
          byte orientation = (byte)(getSegmentOrientations()[i] - 64);

          if (rotatedTemplate != null)
          {
            rotatedTemplate.translateAndRotate(
              getSegmentBounds()[i].topLeft.x, 
              getSegmentBounds()[i].topLeft.y, 0, 0, 
              orientation);
          }
          alignedTemplates[i] = rotatedTemplate;
        }
      }
    }

    return alignedTemplates;
  }

  private FingerBounds getSegmentBounds(int index)
    throws FingerException
  {
    FingerBounds bounds = new FingerBounds();

    bounds.topLeft = new Point();
    bounds.topRight = new Point();
    bounds.bottomLeft = new Point();
    bounds.bottomRight = new Point();

    int result = nGetSegmentBounds(this._handle, index, bounds);

    if (result != 0) {
      throw new FingerException(result);
    }
    return bounds;
  }

  private byte getSegmentOrientation(int index)
    throws FingerException
  {
    int result = 0;
    byte[] angle = new byte[1];

    result = nGetSegmentOrientation(this._handle, index, angle);

    if (result != 0) {
      throw new FingerException(result);
    }
    return angle[0];
  }

  private static final native int nInitialize(long[] paramArrayOfLong);

  private static final native int nDispose(long paramLong);

  private static final native int nClone(long paramLong, long[] paramArrayOfLong);

  private static final native int nFromFile(String paramString, int paramInt, long[] paramArrayOfLong);

  private static final native int nFromBuffer(byte[] paramArrayOfByte, int paramInt, long[] paramArrayOfLong);

  private static final native int nFromRawBuffer(byte[] paramArrayOfByte, int paramInt1, int paramInt2, long[] paramArrayOfLong);

  private static final native int nSave(long paramLong, int paramInt, float paramFloat, String paramString);

  private static final native byte[] nToBuffer(long paramLong, int paramInt, float paramFloat);

  private static final native int nCreateTemplate(long paramLong, long[] paramArrayOfLong);

  private static final native int nCreateTemplateRecord(long paramLong, long[] paramArrayOfLong);

  private static final native int nComputeNFIQ(long paramLong, int[] paramArrayOfInt);

  private static final native int nSegmentSlap(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetCbeffProductID(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetFingerPosition(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetHorizontalResolution(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetImageHeight(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetImpressionType(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetImageWidth(long paramLong, int[] paramArrayOfInt);

  private static final native byte[] nGetPixels(long paramLong);

  private static final native int nGetQuality(long paramLong, int[] paramArrayOfInt);

  private static final native byte[] nGetQualityMap(long paramLong);

  private static final native int nGetVerticalResolution(long paramLong, int[] paramArrayOfInt);

  private static final native int nSetCbeffProductID(long paramLong, int paramInt);

  private static final native int nSetImpressionType(long paramLong, int paramInt);

  private static final native int nSetFingerPosition(long paramLong, int paramInt);

  private static final native int nSetHorizontalResolution(long paramLong, int paramInt);

  private static final native int nSetQuality(long paramLong, int paramInt);

  private static final native int nSetVerticalResolution(long paramLong, int paramInt);

  private static final native int nGetCaptureDeviceCertification(long paramLong, int[] paramArrayOfInt);

  private static final native int nSetCaptureDeviceCertification(long paramLong, int paramInt);

  private static final native int nGetCaptureDeviceVendor(long paramLong, int[] paramArrayOfInt);

  private static final native int nSetCaptureDeviceVendor(long paramLong, int paramInt);

  private static final native int nGetCaptureDeviceTechnology(long paramLong, int[] paramArrayOfInt);

  private static final native int nSetCaptureDeviceTechnology(long paramLong, int paramInt);

  private static final native int nGetCaptureDeviceType(long paramLong, int[] paramArrayOfInt);

  private static final native int nSetCaptureDeviceType(long paramLong, int paramInt);

  private static final native int nGetTemplate(long paramLong, long[] paramArrayOfLong);

  private static final native int nSetTemplate(long paramLong1, long paramLong2);

  private static final native int nGetTemplateRecord(long paramLong, long[] paramArrayOfLong);

  private static final native int nGetSegmentCount(long paramLong, int[] paramArrayOfInt);

  private static final native int nGetSegmentBounds(long paramLong, int paramInt, FingerBounds paramFingerBounds);

  private static final native int nGetSegmentOrientation(long paramLong, int paramInt, byte[] paramArrayOfByte);

  private static final native int nGetSegment(long paramLong, int paramInt, long[] paramArrayOfLong);

  private static final native int nGetSpoofScore(long paramLong, int[] paramArrayOfInt);
}
