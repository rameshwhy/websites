package com.id3.fingerprint;

public class FingerPosition
{
  public static final int Unknown = 0;
  public static final int RightThumb = 1;
  public static final int RightIndex = 2;
  public static final int RightMiddle = 3;
  public static final int RightRing = 4;
  public static final int RightLittle = 5;
  public static final int LeftThumb = 6;
  public static final int LeftIndex = 7;
  public static final int LeftMiddle = 8;
  public static final int LeftRing = 9;
  public static final int LeftLittle = 10;
  public static final int PlainRightFourFingers = 13;
  public static final int PlainLeftFourFingers = 14;
  public static final int LeftThumbRightThumb = 15;
  public static final int RightIndexMiddle = 40;
  public static final int RightMiddleRing = 41;
  public static final int RightRingLittle = 42;
  public static final int LeftIndexMiddle = 43;
  public static final int LeftMiddleRing = 44;
  public static final int LeftRingLittle = 45;
  public static final int RightIndexLeftIndex = 46;
  public static final int RightIndexMiddleRing = 47;
  public static final int RightMiddleRingLittle = 48;
  public static final int LeftIndexMiddleRing = 49;
  public static final int LeftMiddleRingLittle = 50;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerPosition
 * JD-Core Version:    0.6.2
 */