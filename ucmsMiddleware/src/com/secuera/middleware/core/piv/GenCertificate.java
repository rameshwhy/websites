/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.beans.piv.CSRBean;
import com.secuera.middleware.beans.piv.FascnBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import javax.smartcardio.CardChannel;

/**
 *
 * @author com_user
 */
public class GenCertificate {

    public byte[] genCert(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY, String cert_type,
            byte[] key_type, String str_cn, String str_ou,
            String str_org, String str_city, String str_state,
            String str_country, String str_email, int[] cert_template_oid, int majorVer, int minorVer,
            String caType, String inURL, String localPin, int localPinLength) throws Exception {

        String certCSR="";
        byte[] certficate = null;
        CommonUtil cmnUtil = new CommonUtil();
        /*sat
        //generate CSR
        GenCSR generateCSR = new GenCSR();
        certCSR = generateCSR.getCSR(channel, PRE_PERSO_PIV_ADMIN_KEY, PIV_ADMIN_KEY, localPin, localPinLength, cert_type, key_type, str_cn, str_ou, str_org,
                str_city, str_state, str_country, str_email, cert_template_oid, majorVer, minorVer, "");
         */ 


        //get microsoft certificate    
        if ("MS".equals(caType)) {
            certficate = getMScertificate(certCSR, inURL);
        }

        CommonUtil util = new CommonUtil();
        System.out.println(util.arrayToHex(certficate));
        return certficate;

        //load certificate on card
        //LoadCertificate loadCertificate = new LoadCertificate();
        //loadCertificate.loadCert(channel,PRE_PERSO_PIV_ADMIN_KEY,PIV_ADMIN_KEY,certficate,cert_type);



    }

    private byte[] getMScertificate(String certCSR, String inURL) throws MiddleWareException {

        byte[] certficate = null;
        //connect to microsoft 
        try {
            MicrosoftCertServer msCASrv = new MicrosoftCertServer();


            // Download the new certificate
            String approvalID = msCASrv.HttpGetApprovalID(certCSR, inURL + "certfnsh.asp");
            // Create the Certificate in .der format and get it in .pem format
            certficate = msCASrv.HttpGetUserCert(approvalID, inURL);


        } catch (Exception ex) {
            // TODO Auto-generated catch block

            throw new MiddleWareException(ex.getMessage());
        }


        return certficate;

    }

    public String generateCSR(CardChannel channel, byte[] adminKey, byte[] diversifiedAdminKey, String localPin, int localPinLength, CSRBean csrBean, FascnBean fascnBean) throws MiddleWareException {
        String certCSR = "";
        String Strfascn = "";
        byte[] fascn=null;
        CommonUtil cmnUtil = new CommonUtil();


        //Generate FASC 
        if (fascnBean != null) {
            Strfascn = cmnUtil.getFASCN(fascnBean.getAgencyCode(), fascnBean.getSystemCode(), fascnBean.getCredentialNumber(),
                    fascnBean.getCs(), fascnBean.getIci(), fascnBean.getPerson());
            fascn = cmnUtil.hex1ToByteArray(Strfascn);
        }
        //generate CSR
        GenCSR generateCSR = new GenCSR();
        try {
            certCSR = generateCSR.getCSR(channel, adminKey, diversifiedAdminKey, csrBean, fascn, localPin, localPinLength);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        //Return CSR
        return certCSR;


    }
    
    public byte[] generateUserCertificate(String certCSR, String inURL,String caType) throws MiddleWareException {
        byte[] certficate=null;
        
        //get microsoft certificate    
        if ("MS".equals(caType)) {
            certficate = getMScertificate(certCSR, inURL);
        }
        
        //Return CSR
        return certficate;


    }
    
}
