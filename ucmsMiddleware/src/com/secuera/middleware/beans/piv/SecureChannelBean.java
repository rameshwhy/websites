/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

/**
 *
 * @author Satish K
 */
public class SecureChannelBean {
    private byte[] S_MAC_KEY;
    private byte[] PREV_MAC;
    private byte[] AUTH_DEK_KEY;
    
    public byte[] getPREV_MAC() {
        return PREV_MAC;
    }

    public void setPREV_MAC(byte[] PREV_MAC) {
        this.PREV_MAC = PREV_MAC;
    }

    public byte[] getS_MAC_KEY() {
        return S_MAC_KEY;
    }

    public void setS_MAC_KEY(byte[] S_MAC_KEY) {
        this.S_MAC_KEY = S_MAC_KEY;
    }

    public byte[] getAUTH_DEK_KEY() {
        return AUTH_DEK_KEY;
    }

    public void setAUTH_DEK_KEY(byte[] AUTH_DEK_KEY) {
        this.AUTH_DEK_KEY = AUTH_DEK_KEY;
    }
    
    
}
