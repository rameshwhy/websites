/*     */ package com.id3.fingerprint;
/*     */ 
/*     */ public class FingerMatchCandidate
/*     */ {
/*     */   private long _handle;
/*     */ 
/*     */   public FingerMatchCandidate(long handle)
/*     */   {
/*  22 */     this._handle = handle;
/*     */   }
/*     */ 
/*     */   public boolean equals(Object obj)
/*     */   {
/*  32 */     if ((obj == null) || (obj.getClass() != getClass())) {
/*  33 */       return false;
/*     */     }
/*  35 */     FingerMatchCandidate object = (FingerMatchCandidate)obj;
/*  36 */     return this._handle == object._handle;
/*     */   }
/*     */ 
/*     */   public int hashCode()
/*     */   {
/*  45 */     return (int)this._handle;
/*     */   }
/*     */ 
/*     */   public long getHandle()
/*     */   {
/*  53 */     return this._handle;
/*     */   }
/*     */ 
/*     */   public int getIndex()
/*     */     throws FingerException
/*     */   {
/*  63 */     int[] value = new int[1];
/*  64 */     int result = nGetIndex(this._handle, value);
/*     */ 
/*  66 */     if (result != 0) {
/*  67 */       throw new FingerException(result);
/*     */     }
/*  69 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getScore()
/*     */     throws FingerException
/*     */   {
/*  80 */     int[] value = new int[1];
/*  81 */     int result = nGetScore(this._handle, value);
/*     */ 
/*  83 */     if (result != 0) {
/*  84 */       throw new FingerException(result);
/*     */     }
/*  86 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getSimilarityRate()
/*     */     throws FingerException
/*     */   {
/*  97 */     int[] value = new int[1];
/*  98 */     int result = nGetSimilarityRate(this._handle, value);
/*     */ 
/* 100 */     if (result != 0) {
/* 101 */       throw new FingerException(result);
/*     */     }
/* 103 */     return value[0];
/*     */   }
/*     */ 
/*     */   public int getDecision()
/*     */     throws FingerException
/*     */   {
/* 113 */     int[] value = new int[1];
/* 114 */     int result = nGetDecision(this._handle, value);
/*     */ 
/* 116 */     if (result != 0) {
/* 117 */       throw new FingerException(result);
/*     */     }
/* 119 */     return value[0];
/*     */   }
/*     */ 
/*     */   private static final native int nGetIndex(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetScore(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetSimilarityRate(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetDecision(long paramLong, int[] paramArrayOfInt);
/*     */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerMatchCandidate
 * JD-Core Version:    0.6.2
 */