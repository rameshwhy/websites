/*     */ package com.id3.fingerprint;
/*     */ 
/*     */ public class FingerMatchCandidateList
/*     */ {
/*     */   private long _handle;
/*  16 */   private boolean _disposed = false;
/*  17 */   private FingerMatchCandidate[] _candidatesArray = null;
/*     */ 
/*     */   public FingerMatchCandidateList(long handle)
/*     */   {
/*  24 */     this._handle = handle;
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/*  32 */     nDispose(this._handle);
/*  33 */     this._handle = 0L;
/*  34 */     this._disposed = true;
/*     */   }
/*     */ 
/*     */   protected void finalize()
/*     */     throws Throwable
/*     */   {
/*     */     try
/*     */     {
/*  44 */       dispose();
/*     */     }
/*     */     catch (Exception localException)
/*     */     {
/*     */     }
/*     */     finally
/*     */     {
/*  51 */       super.finalize();
/*     */     }
/*     */   }
/*     */ 
/*     */   public long getHandle()
/*     */   {
/*  60 */     return this._handle;
/*     */   }
/*     */ 
/*     */   public int getCount()
/*     */     throws FingerException
/*     */   {
/*  70 */     int[] value = new int[1];
/*  71 */     int result = nGetCount(this._handle, value);
/*     */ 
/*  73 */     if (result != 0) {
/*  74 */       throw new FingerException(result);
/*     */     }
/*  76 */     return value[0];
/*     */   }
/*     */ 
/*     */   private FingerMatchCandidate getCandidate(int index)
/*     */     throws FingerException
/*     */   {
/*  87 */     long[] handle = new long[1];
/*  88 */     int result = nGetCandidate(this._handle, index, handle);
/*     */ 
/*  90 */     if (result != 0) {
/*  91 */       throw new FingerException(result);
/*     */     }
/*  93 */     return new FingerMatchCandidate(handle[0]);
/*     */   }
/*     */ 
/*     */   public FingerMatchCandidate[] getCandidates()
/*     */     throws FingerException
/*     */   {
/* 103 */     if (this._candidatesArray == null)
/*     */     {
/* 105 */       int candidateCount = getCount();
/* 106 */       this._candidatesArray = new FingerMatchCandidate[candidateCount];
/*     */ 
/* 108 */       for (int i = 0; i < candidateCount; i++) {
/* 109 */         this._candidatesArray[i] = getCandidate(i);
/*     */       }
/*     */     }
/* 112 */     return this._candidatesArray;
/*     */   }
/*     */ 
/*     */   public FingerMatchCandidate get(int index)
/*     */     throws FingerException
/*     */   {
/* 122 */     return getCandidates()[index];
/*     */   }
/*     */ 
/*     */   private static final native int nDispose(long paramLong);
/*     */ 
/*     */   private static final native int nGetCount(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetCandidate(long paramLong, int paramInt, long[] paramArrayOfLong);
/*     */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerMatchCandidateList
 * JD-Core Version:    0.6.2
 */