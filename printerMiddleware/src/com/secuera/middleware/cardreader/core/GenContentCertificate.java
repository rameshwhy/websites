/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.PKIBean;
import java.security.PrivateKey;
import java.security.PublicKey;
import javax.smartcardio.CardChannel;

/**
 *
 * @author Satish K
 */
public class GenContentCertificate {

    public byte[] genContentCert(CardChannel channel, char cert_type,
            byte[] key_type, String str_cn, String str_ou,
            String str_org, String str_city, String str_state,
            String str_country, String str_email, int[] cert_template_oid, int majorVer, int minorVer,
            String caType, String inURL, PublicKey pubkey, PrivateKey privkey) throws Exception {



        PKIBean pki = new PKIBean();
        //generate publickey / private key if not supplied
        if (pubkey == null || pubkey.toString().length() == 0) {
            GenKeyPair keyPair = new GenKeyPair();
            pki = keyPair.keyPair();
        } else {
            pki.setPublicKey(pubkey);
            pki.setPrivateKey(privkey);
            
        }

        String certCSR;
        byte[] certficate = null;
        //generate CSR
        GenContentCSR generateCSR = new GenContentCSR();
        certCSR = generateCSR.getCSR(str_cn, str_ou, str_org,
                str_city, str_state, str_country, str_email, cert_template_oid, majorVer, minorVer, pki.getPublicKey(), pki.getPrivateKey());



        //get microsoft certificate    
        if ("MS".equals(caType)) {
            certficate = getMScertificate(certCSR, inURL);
        }

        CommonUtil util = new CommonUtil();
        System.out.println(util.arrayToHex(certficate));
        return certficate;

        //load certificate on card
        //LoadCertificate loadCertificate = new LoadCertificate();
        //loadCertificate.loadCert(channel,PRE_PERSO_PIV_ADMIN_KEY,PIV_ADMIN_KEY,certficate,cert_type);



    }

    private byte[] getMScertificate(String certCSR, String inURL) {

        byte[] certficate = null;
        //connect to microsoft 
        try {
            MicrosoftCertServer msCASrv = new MicrosoftCertServer();


            // Download the new certificate
            String approvalID = msCASrv.HttpGetApprovalID(certCSR, inURL);
            // Create the Certificate in .der format and get it in .pem format
            certficate = msCASrv.HttpGetUserCert(approvalID, inURL);


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return certficate;

    }
}
