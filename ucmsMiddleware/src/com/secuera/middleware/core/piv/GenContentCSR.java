package com.secuera.middleware.core.piv;

import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.cardreader.entities.MiddleWareException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.security.PrivateKey;
import sun.security.util.DerEncoder;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.util.ObjectIdentifier;
import sun.security.x509.X500Name;
import sun.misc.BASE64Encoder;
import sun.security.x509.AlgorithmId;
import java.security.PublicKey;

public class GenContentCSR {

    // Author: Harpreet Singh
    /*
     * 1. Registration Authority RA enters the SO PIN or Admin Key to get access
     * to the user’s smartcard.[KSS] Typically the RA has already inserted their
     * own card to authenticate to the system, they log into the Card being
     * issued as User, not SO. PIN change is forced on first user login 2. RA
     * generates a public/private key pair on the user’s smartcard with
     * appropriate key size and algorithm (RSA / ECC). 3. RA extracts the user’s
     * public key from the smartcard. 4. RA creates a certificate request
     * PKCS#10 with (Subject distinguish name, user’s public key, and additional
     * attributes listed in PKCS#9) 5. Then request gets signed by the user’s
     * private key on the smartcard that matches user’s public key in the
     * PKCS#10 request. 6. This signed request goes to the CA 7. Then the CA
     * generates a public key certificate using the information in the PKCS#10
     * request 8. Then the CA signs this certificate using the CA private key 9.
     * Then the certificate gets returned back to the requestor (RA) 10. RA will
     * load the certificate to the smartcard in the same logical container as
     * the public/private key pair and bind the certificate to the existing keys
     * by updating ??? attributes. [KSS] Typically the CKA_Label attribute I
     * think is used to match up the key pairs and the cert. 11. How does CA
     * certificate get loaded to the card I am not sure but I have seen that CA
     * certificate and user certificate on the same card. When I insert the card
     * windows will ask do I want to install the CA certificate. [KSS] This is
     * dependent on the CMS or infrastructure, to the card it’s just another
     * object that is stored. So as long as the client has access to the CA cert
     * it can store it on the card, this could be done by the RA.. 12. For
     * revocation I assume the RA just sends a revocation request with the
     * serial numbers for certificate it wants the CA to revoke and add to the
     * CRL.
     *
     *
     * CN: Common name / domain name / server name / FQDN: Indicate here your
     * SSL server name, such as "secure.company.com", "www.my-domain.com" or
     * "www.product.com". No IP address (learn more). No spaces nor blank
     * characters.
     *
     * Even if we do not advise so, intranet addresses can be listed in the CSR
     * (learn more)
     *
     * If you need to order a multiple-domain / SANs certificate, indicate the
     * main address only when generating your CSR. This address will remain the
     * same until the certificate expiration. Then enter the other addresses you
     * want to secure in the order form. Those ones will be changeable through
     * reissuances.
     *
     * O: Organisation / Company Name: indicate the corporate name of your
     * company (no trade name or acronym), in uppercase preferably.
     *
     * ST: State: in France indicate the name of the department where your
     * company headquarters are based (not the number).
     *
     * L: Location / City: indicate the city where your company headquarters are
     * based.
     *
     * C: Country: indicate FR if your company is in France, BE for Belgium,
     * etc, in uppercase preferably.
     *
     * OU: Organisational unit / Department / Branch : We advise not to fill in
     * this field or to enter a generic term such as "IT Department".
     *
     *
     *  A block type BT, a padding string PS, and the data D shall be
     formatted into an octet string EB, the encryption block.

     EB = 00 || BT || PS || 00 || D .           (1)

     The block type BT shall be a single octet indicating the structure of
     the encryption block. For this version of the document it shall have
     value 00, 01, or 02. For a private- key operation, the block type
     shall be 00 or 01. For a public-key operation, it shall be 02.

     The padding string PS shall consist of k-3-||D|| octets. For block
     type 00, the octets shall have value 00; for block type 01, they
     shall have value FF; and for block type 02, they shall be
     pseudorandomly generated and nonzero. This makes the length of the
     encryption block EB equal to k.

     Notes.

     1.   The leading 00 octet ensures that the encryption
     block, converted to an integer, is less than the modulus.

     2.   For block type 00, the data D must begin with a
     nonzero octet or have known length so that the encryption
     block can be parsed unambiguously. For block types 01 and
     02, the encryption block can be parsed unambiguously since
     the padding string PS contains no octets with value 00 and
     the padding string is separated from the data D by an octet
     with value 00.

     3.   Block type 01 is recommended for private-key
     operations. Block type 01 has the property that the
     encryption block, converted to an integer, is guaranteed to
     be large, which prevents certain attacks of the kind
     proposed by Desmedt and Odlyzko [DO86].

     4.   Block types 01 and 02 are compatible with PEM RSA
     encryption of content-encryption keys and message digests
     as described in RFC 1423.



     */
    public GenContentCSR() {
        super();
    }
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    GeneralAuth auth = new GeneralAuth();
    PivUtilInterface piv = new PivUtil();
    private String str_Email;
    private byte[] num_bytes;
    private byte[] bert_tag;
    private byte[] encoded;
    // Extension Request
    final int Extension_request_data[] = {1, 2, 840, 113549, 1, 9, 14};
    // Extended key Usage
    final int Extended_keyUsage_data[] = {2, 5, 29, 37};
    final int PIV_contentsigning_data[] = {2, 16, 840, 1, 101, 3, 6, 7};
    // Key Usage
    final int KeyUsage_data[] = {2, 5, 29, 15};
    // Os Version microsoft CA
    final int os_version_data[] = {1, 3, 6, 1, 4, 1, 311, 13, 2, 3};
    // Email Address
    final int emailAddress_data[] = {1, 2, 840, 113549, 1, 9, 1};
    final int certificateTemplate_data[] = {1, 3, 6, 1, 4, 1, 311, 21, 7};
    // Request Client Info
    final int requestClient_Info_data[] = {1, 3, 6, 1, 4, 1, 311, 21, 20};
    //enrolmentCSP
    final int enrolmentCSP_data[] = {1, 3, 6, 1, 4, 1, 311, 13, 2, 2};
    //Alternate Subject Information
    final int subject_altinfo_data[] = {2, 5, 29, 17};
    final int fascn_data[] = {2, 16, 840, 1, 101, 3, 6, 6};
    final int upn_data[] = {1, 3, 6, 1, 4, 1, 311, 20, 2, 3};
    int[] template_oid_data;
    int major_ver;
    int minor_ver;
    private int[] Signature_Algo;
    private PublicKey publicKey;
    private PrivateKey privatekey;

    // private static PKCS10Attributes attributeSet;
    public String getCSR(String str_cn, String str_ou,
            String str_org, String str_city, String str_state,
            String str_country, String str_email, String cert_template_oid, int majorVer, int minorVer,
            PublicKey pubkey, PrivateKey privkey, String algoType) throws MiddleWareException {

        String csr_data = null;
        str_Email = str_email;

        template_oid_data = util.stringToIntArray(cert_template_oid);
        major_ver = majorVer;
        minor_ver = minorVer;
        publicKey = pubkey;
        privatekey = privkey;

        // Generate CSR.
        try {
            csr_data = genPKCS10(str_cn, str_ou, str_org, str_city, str_state, str_country, algoType);
        } catch (Exception ex) {
            throw new MiddleWareException(ex.getMessage());
        }

        System.out.println("Final CSR " + csr_data);



        return csr_data;
    }

    private String genPKCS10(String str_cn, String str_ou, String str_org,
            String str_city, String str_state, String str_country, String algoType)
            throws MiddleWareException, Exception {

        // generate PKCS10 certificate request
        DerOutputStream out, certrequest;
        byte[] certificateRequestInfo;

        /*if (algoType.equalsIgnoreCase("SHA256")) {
         Encryption_data = sha256WithRSAEncryption_data;
         } else if (algoType.equalsIgnoreCase("SHA512")) {
         Encryption_data = sha512WithRSAEncryption_data;
         }*/


        if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC224)) {
            Signature_Algo = UcmsMiddlewareConstants.sha224WithECDSA_data;
        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC256)) {
            Signature_Algo = UcmsMiddlewareConstants.sha256WithECDSA_data;

        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC384)) {
            Signature_Algo = UcmsMiddlewareConstants.sha384WithECDSA_data;


        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.RSA1024)) {
            Signature_Algo = UcmsMiddlewareConstants.sha256WithRSAEncryption_data;


        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
            Signature_Algo = UcmsMiddlewareConstants.sha512WithRSAEncryption_data;

        }


        X500Name x500Name = new X500Name(str_cn, str_ou, str_org, str_city,
                str_state, str_country);

        System.out.println("X500  NAME " + x500Name.toString());

        certrequest = new DerOutputStream();
        certrequest.putInteger(BigInteger.ZERO);

        x500Name.encode(certrequest); // X.500 name

        certrequest.write(publicKey.getEncoded()); // public key

        // Now Encode the Extension Request
        certrequest = get_ExtendedParams(certrequest);


        out = new DerOutputStream();
        out.write(DerValue.tag_Sequence, certrequest); // wrap it!
        //encoded=certificateRequestInfo;
        certrequest = out;

        /**
         * *****************************************************************
         */
        // Now get the CSR data signed by smart card.
        byte[] signed_csr;

        certificateRequestInfo = certrequest.toByteArray();

        System.out.println("Before signature DER String " + util.arrayToHex(certificateRequestInfo));

        signed_csr = signCSR(certificateRequestInfo, algoType);

        System.out.println("Step_6_1..signature " + util.arrayToHex(signed_csr));


        // Now Encode the Algorithm
        ObjectIdentifier Signature_data_OID = ObjectIdentifier.newInternal(Signature_Algo);
        AlgorithmId algId = new AlgorithmId(Signature_data_OID);
        algId.encode(certrequest);

        certrequest.putBitString(signed_csr);  // signature

        /*
         * Wrap those guts in a sequence
         */
        out = new DerOutputStream();
        out.write(DerValue.tag_Sequence, certrequest);
        encoded = out.toByteArray();

        // Convert to Base64 format.

        System.out.println("CSR BEFORE BASE64 " + util.arrayToHex(encoded));

        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bs);
        convertToBase64(ps);

        String final_certificateRequestInfo = bs.toString();

        // Now close the streams.
        try {

            ps.close();
            bs.close();
        } catch (Throwable ex) {
            throw new MiddleWareException(ex.getMessage());
        }


        return final_certificateRequestInfo;

    }

    public void convertToBase64(PrintStream base64)
            throws IOException {
        /*if (encoded == null)
         throw new SignatureException("Cert request was not signed"); */

        BASE64Encoder encoder = new BASE64Encoder();

        //  base64.println("-----BEGIN NEW CERTIFICATE REQUEST-----");
        encoder.encodeBuffer(encoded, base64);
        //base64.println("-----END NEW CERTIFICATE REQUEST-----");
    }

    private void calcBertValue(Integer len, boolean binclude_bert) {
        // Determine value of Bert Tag
        // if (binclude_bert)
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for
        // size of data
        // bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size
        // of data bytes
        byte[] len_tag_small = {(byte) 0x81};
        byte[] len_tag_big = {(byte) 0x82};

        if (len > 255) {
            if (binclude_bert) {
                bert_tag = bert_tag_size_big;
            } else {
                bert_tag = len_tag_big;
            }

        } else {
            if (binclude_bert) {
                bert_tag = bert_tag_size_small;
            } else {
                bert_tag = len_tag_small;
            }

        }

        // Now calculate actual data size being stored.
        // Following condition makes sure that converted hex string is always
        // even.

        String tot_hex_size;

        if (len > 255 && len < 4096) {

            tot_hex_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_size = Integer.toHexString(len);

        }

        num_bytes = util.hex1ToByteArray(tot_hex_size);

    }

    public byte[] getEncoded() {

        if (encoded != null) {
            return encoded.clone();
        } else {
            return null;
        }

    }

    byte[] signCSR(byte[] csr_input, String algoType) throws MiddleWareException {

        byte[] csr_sig;
        GenDigitalSignature gensig = new GenDigitalSignature();

        csr_sig = gensig.GenSignature(csr_input, privatekey, algoType);


        return csr_sig;

    }

    DerOutputStream get_ExtendedParams(DerOutputStream certrequest) throws MiddleWareException, IOException {

        DerOutputStream out_os, out_client, out_extension;

        out_os = get_Os();
        out_client = get_ClientInfo();
        out_extension = get_extensions();


        byte[] byte_os = out_os.toByteArray();
        byte[] byte_client = out_client.toByteArray();
        byte[] byte_extension = out_extension.toByteArray();


        byte[][] arrays = new byte[3][];

        arrays[0] = byte_os;
        arrays[1] = byte_client;
        arrays[2] = byte_extension;


        byte[] combined_bytes = util.combine_data(arrays);
        int len = combined_bytes.length;

        byte[] tot_num_bytes = calcDerLen(len);
        byte[] tot_tag = {(byte) 0xa0};


        byte[] final_tag_Sequence = util.combine_data(tot_tag, tot_num_bytes);
        byte[] ext_params = util.combine_data(final_tag_Sequence, byte_os);
        ext_params = util.combine_data(ext_params, byte_client);
        ext_params = util.combine_data(ext_params, byte_extension);

        System.out.println("5th Step " + util.arrayToHex(ext_params));


        certrequest.write(ext_params);

        // Now close the streams.out_1, out_2, out_3, out_4
        try {

            out_os.close();
            out_client.close();
            out_extension.close();


        } catch (Throwable ex) {
            throw new MiddleWareException(ex.getMessage());
        }


        return certrequest;

    }

    DerOutputStream get_Os() throws MiddleWareException, IOException {

        DerOutputStream out_1, out_2, out_3, out_os;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_os = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier os_version_data_OID = ObjectIdentifier.newInternal(os_version_data);
        String os_version = "6.1.7600.2";

        out_1.putOID(os_version_data_OID);
        out_2.putIA5String(os_version);

        // Encode the values in Set
        DerEncoder[] y = {out_2};
        out_3.putOrderedSet(DerValue.tag_Set, y);


        DerValue attr_oid = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_3.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_os.putSequence(x);


        // Now close the streams.out_1, out_2, out_3, out_os
        try {


            out_1.close();
            out_2.close();
            out_3.close();
            out_os.close();

        } catch (Throwable ex) {
            throw new MiddleWareException(ex.getMessage());
        }

        return out_os;



    }

    byte[] get_Email() throws MiddleWareException, IOException {

        DerOutputStream out_email, out_upn_oid, out_email_info;
        out_email = new DerOutputStream();
        out_upn_oid = new DerOutputStream();
        out_email_info = new DerOutputStream();

        byte[] ext_cert_tag = {(byte) 0xa0};
        byte[] email_value = null;
        // Out put the OID and the value
        ObjectIdentifier upn_data_OID = ObjectIdentifier.newInternal(upn_data);

        if (str_Email.length() == 0) {
            return email_value;
        }

        //A0 20 06 0A 2B
        //06 01 04 01 82 37 14 02 03
        //A0 12 0C 10 72 68 6F
        //6C 6C 65 79 40 6D 79 69 64 2E 63 6F 6D 81

        // Encode UPN OID
        out_upn_oid.putOID(upn_data_OID);

        out_email.putUTF8String(str_Email);


        int len = out_email.toByteArray().length;
        byte[] email_num_bytes = util.hex1ToByteArray(Integer.toHexString(len));

        byte[][] email_arrays = new byte[3][];
        email_arrays[0] = ext_cert_tag;
        email_arrays[1] = email_num_bytes;
        email_arrays[2] = out_email.toByteArray();

        byte[] email_bytes = util.combine_data(email_arrays);

        //Now combine Fascan_OID and Fascan Value and calculate length of bytes.
        len = util.combine_data(out_upn_oid.toByteArray(), email_bytes).length;
        byte[] tot_num_bytes = util.hex1ToByteArray(Integer.toHexString(len));

        byte[][] tot_email_value = new byte[3][];
        tot_email_value[0] = ext_cert_tag;
        tot_email_value[1] = tot_num_bytes;
        tot_email_value[2] = util.combine_data(out_upn_oid.toByteArray(), email_bytes);

        email_value = util.combine_data(tot_email_value);

        // Now Combine OID and UPN Value and encode it on the stream
        // DerValue attr_upn_val = new DerValue(email_value);

        // DerValue[] x_email = {attr_upn_val};
        //  out_email_info.putSequence(x_email);


        // Now close the streams.
        try {

            out_email.close();
            out_upn_oid.close();
            out_email_info.close();

        } catch (Throwable ex) {
            throw new MiddleWareException(ex.getMessage());
        }

        return email_value;

    }

    DerOutputStream get_ClientInfo() throws MiddleWareException, IOException {

        DerOutputStream out_1, out_2, out_3, out_4, out_int, out_seq, out_set, out_client;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_4 = new DerOutputStream();
        out_int = new DerOutputStream();
        out_seq = new DerOutputStream();
        out_set = new DerOutputStream();
        out_client = new DerOutputStream();

        ObjectIdentifier requestClient_Info_data_OID = ObjectIdentifier.newInternal(requestClient_Info_data);
        out_1.putOID(requestClient_Info_data_OID);

        out_int.putInteger(5);

        //certBean.setCertURL("https://secuera-gst-01.ca.secuera.com/certsrv/" + "certfnsh.asp");
        //Now add String values
        //String str_1 = "WIN-EIE567GPLV2.myid.com";
        //String str_2 = "MYID" + "\\" + "com_user";
        //String str_3 = "CertificateServerWindows.vshost.exe";

        String str_1 = "secuera-gst-01.ca.secuera.com";
        String str_2 = "ca.secuera.com" + "\\" + "SEAdmin";
        String str_3 = "ABC";


        out_2.putUTF8String(str_1);
        out_3.putUTF8String(str_2);
        out_4.putUTF8String(str_3);

        // Now wrap these elements in Sequence
        DerValue attr_int = new DerValue(out_int.toByteArray());
        DerValue attr_2 = new DerValue(out_2.toByteArray());
        DerValue attr_3 = new DerValue(out_3.toByteArray());
        DerValue attr_4 = new DerValue(out_4.toByteArray());

        DerValue[] y = {attr_int, attr_2, attr_3, attr_4};
        out_seq.putSequence(y);

        // Now Wrap in Set
        DerEncoder[] set = {out_seq};
        out_set.putOrderedSet(DerValue.tag_Set, set);

        DerValue attr_oid = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_set.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_client.putSequence(x);


        System.out.println("CLient Info " + util.arrayToHex(out_client.toByteArray()));

        // Now close the streams.out_1, out_2, out_3,out_4,out_int, out_seq,out_set,out_client;
        try {

            out_1.close();
            out_2.close();
            out_3.close();
            out_4.close();
            out_int.close();
            out_seq.close();
            out_set.close();
            out_client.close();

        } catch (Throwable ex) {
            throw new MiddleWareException(ex.getMessage());
        }

        return out_client;

    }

    DerOutputStream get_extensions() throws MiddleWareException, IOException {

        DerOutputStream out_ext_oid, outer_seq, out_certTemplate, out_ext, out_set, out_altsubject_info, out_extKey;
        out_ext_oid = new DerOutputStream();
        outer_seq = new DerOutputStream();
        out_certTemplate = new DerOutputStream();
        out_set = new DerOutputStream();
        out_ext = new DerOutputStream();
        out_altsubject_info = new DerOutputStream();
        out_extKey = new DerOutputStream();

        ObjectIdentifier Extension_request_data_OID = ObjectIdentifier.newInternal(Extension_request_data);
        out_ext_oid.putOID(Extension_request_data_OID);

        //Get Cert Template
        out_certTemplate = get_certtemplate();
        // Get Extended Key Usage
        //out_extKey = get_extKeyUsage();
        // Get alt subject info
        out_altsubject_info = get_subjectAltInfo();

        if (out_altsubject_info.toByteArray().length == 0 || out_altsubject_info.toByteArray() == null) {
            // Now add one outer sequence for whole set of Parameters.
            DerValue attr_seq = new DerValue(out_certTemplate.toByteArray());
            //DerValue attr_seq_extkey = new DerValue(out_extKey.toByteArray());

            DerValue[] outer = {attr_seq};
            outer_seq.putSequence(outer);


        } else {
            // Now add one outer sequence for whole set of Parameters.
            DerValue attr_seq = new DerValue(out_certTemplate.toByteArray());
            //DerValue attr_seq_extkey = new DerValue(out_extKey.toByteArray());
            DerValue attr_seq_subject = new DerValue(out_altsubject_info.toByteArray());

            DerValue[] outer = {attr_seq, attr_seq_subject};
            outer_seq.putSequence(outer);

        }



        //*******Now Encode the whole thing in a Set*******************************************************//
        DerEncoder[] y = {outer_seq};
        out_set.putOrderedSet(DerValue.tag_Set, y);

        System.out.println("Second Step " + util.arrayToHex(out_set.toByteArray()));
        //System.out.println("3rd Step " + util.arrayToHex(out_ext.toByteArray()));

        byte[] xy = util.combine_data(out_ext_oid.toByteArray(), out_set.toByteArray());

        System.out.println("4th Step " + util.arrayToHex(xy));

        int len = xy.length;

        byte[] num_bytes = calcDerLen(len);
        byte[] tag_Sequence = {0x30};

        byte[] final_tag_Sequence = util.combine_data(tag_Sequence, num_bytes);


        xy = util.combine_data(final_tag_Sequence, xy);

        System.out.println("4th_1 Step " + util.arrayToHex(xy));


        out_ext.write(xy);

        // Now close the streams
        try {

            out_ext_oid.close();
            outer_seq.close();
            out_certTemplate.close();
            out_ext.close();
            out_set.close();

        } catch (Throwable ex) {
            throw new MiddleWareException(ex.getMessage());
        }

        return out_ext;

    }

    DerOutputStream get_certtemplate() throws MiddleWareException, IOException {

        DerOutputStream out_template, out_template_octet, out_cert_template_oid, out_temp_oid, out_major, out_minor;
        out_temp_oid = new DerOutputStream();
        out_major = new DerOutputStream();
        out_minor = new DerOutputStream();
        out_cert_template_oid = new DerOutputStream();
        out_template_octet = new DerOutputStream();
        out_template = new DerOutputStream();

        // 30 2F
        // 06 27
        // 2B 06 01 04 01 82 37 15 08 85 A5 CA 76 83 B8 8F
        // 4D 81 E5 8F 1B 84 B2 B1 61 86 FE C9 4B 81 70 82
        // D6 BF 2F 82 E0 94 0F
        // 02 01 64 02 01 04

        //06 24 2B 06 01 04 01 82 37 15 08 84 D6 EA 57 C0
        //                 F7 09 82 F5 85 14 D7 F0 44 86 E6 8F 20 65 84 BC
        //                 BB 13 84 B8 A1 16
        //02 01 64 02 01 64



        ObjectIdentifier cert_template_OID = ObjectIdentifier.newInternal(certificateTemplate_data);
        out_cert_template_oid.putOID(cert_template_OID);

        ObjectIdentifier template_OID = ObjectIdentifier.newInternal(template_oid_data);
        out_temp_oid.putOID(template_OID);

        System.out.println("Template Step 1 " + util.arrayToHex(out_temp_oid.toByteArray()));

        out_major.putInteger(major_ver);
        out_minor.putInteger(minor_ver);


        byte[][] template_arrays = new byte[3][];
        template_arrays[0] = out_temp_oid.toByteArray();
        template_arrays[1] = out_major.toByteArray();
        template_arrays[2] = out_minor.toByteArray();

        byte[] template_bytes = util.combine_data(template_arrays);

        //Now calculate the total length of bytes and add sequence tag to it.
        byte[] seq_tag = {0x30};
        int len = template_bytes.length;
        byte[] tot_num_bytes = util.hex1ToByteArray(Integer.toHexString(len));

        byte[][] tot_temp_value = new byte[3][];
        tot_temp_value[0] = seq_tag;
        tot_temp_value[1] = tot_num_bytes;
        tot_temp_value[2] = template_bytes;

        template_bytes = util.combine_data(tot_temp_value);


        out_template_octet.putOctetString(template_bytes);

        DerValue attr_oid = new DerValue(out_cert_template_oid.toByteArray());
        DerValue attr_value = new DerValue(out_template_octet.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_template.putSequence(x);

        // Now close the streams
        try {
            out_template_octet.close();
            out_cert_template_oid.close();
            out_temp_oid.close();
            out_major.close();
            out_minor.close();
            out_template.close();
        } catch (Throwable ex) {
            throw new MiddleWareException(ex.getMessage());
        }

        return out_template;

    }

    DerOutputStream get_subjectAltInfo() throws MiddleWareException, IOException {

        DerOutputStream out_1, out_2, out_subjectinfo;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_subjectinfo = new DerOutputStream();

        byte[] subject_alt_value;
        // Out put the OID and the value
        ObjectIdentifier subject_altinfo_data_OID = ObjectIdentifier.newInternal(subject_altinfo_data);

        //30 6D
        //A0 27 06 08 60 86 48 01 65 03 06 06 A0 1B
        //04 19 D3 61 52 59 B5 25 6C 1A 84 21 CD A1 68 58
        //      30 08 42 10 84 39 85 48 41 43 E4

        //A0 20 06 0A 2B
        //06 01 04 01 82 37 14 02 03
        //A0 12 0C 10 72 68 6F
        //6C 6C 65 79 40 6D 79 69 64 2E 63 6F 6D 81

        //30 29
        //A0 27 06 08 60 86 48 01 65 03 06 06 A0 1B
        //04 19
        //73 9C ED 39 CE 73 9D A1 68 5A 08 C9 2A DE
        //0A 61 84 E7 39 C3 E2 32 04 31 32
        //30 26
        //A0 24 06
        //0A 2B 06 01 04 01 82 37 14 02 03 A0 16 0C 14 48
        //61 72 70 72 65 65 74 40 73 65 63 75 65 72 61 2E
        //63 6F 6D

        //A0 27
        //06 08 60 86 48 01 65 03 06 06 A0 1B
        //04 19
        //:                 73 9C ED 39 CE 73 9D A1 68 5A 08 C9 2A DE 0A 61
        //:                 84 E7 39 C3 E2 32 04 31 32
        // A0 24 06 0A 2B 06 01
        //:                 04 01 82 37 14 02 03 A0 16 0C 14 48 61 72 70 72
        //:                 65 65 74 40 73 65 63 75 65 72 61 2E 63 6F 6D


        byte[] byte_email_info = get_Email();

        if (byte_email_info.length == 0 || byte_email_info == null) {
            return out_subjectinfo;
        } else {
            subject_alt_value = byte_email_info;
        }


        //Now wrap the Subject alt Value into Sequence

        //Now calculate the total length of bytes and add sequence tag to it.
        byte[] seq_tag = {0x30};
        int len = subject_alt_value.length;
        byte[] tot_num_bytes = util.hex1ToByteArray(Integer.toHexString(len));

        byte[][] tot_san_value = new byte[3][];
        tot_san_value[0] = seq_tag;
        tot_san_value[1] = tot_num_bytes;
        tot_san_value[2] = subject_alt_value;

        subject_alt_value = util.combine_data(tot_san_value);

        /**
         * ***************Envelop all the data into Outer SAN Sequence *
         */
        out_1.putOID(subject_altinfo_data_OID);
        out_2.putOctetString(subject_alt_value);

        // Encode the values in Sequence
        DerValue attr_oid_subject = new DerValue(out_1.toByteArray());

        // Encode the values in Sequence
        DerValue attr_value = new DerValue(out_2.toByteArray());

        System.out.println("Fascan : " + util.arrayToHex(out_2.toByteArray()));

        DerValue[] x = {attr_oid_subject, attr_value};

        out_subjectinfo.putSequence(x);

        System.out.println("Fascan_2 : " + util.arrayToHex(out_subjectinfo.toByteArray()));


        // Now close the streams.
        try {

            out_1.close();
            out_2.close();
            out_subjectinfo.close();

        } catch (Throwable ex) {
            throw new MiddleWareException(ex.getMessage());
        }

        return out_subjectinfo;


    }

    DerOutputStream get_extKeyUsage() throws MiddleWareException, IOException {

        DerOutputStream out_keyUsage, out_content_oid, out_extKeyinfo;
        out_keyUsage = new DerOutputStream();
        out_content_oid = new DerOutputStream();
        out_extKeyinfo = new DerOutputStream();

        // Out put the OID and the value
        ObjectIdentifier Extended_keyUsage_data_OID = ObjectIdentifier.newInternal(Extended_keyUsage_data);
        out_extKeyinfo.putOID(Extended_keyUsage_data_OID);

        //PIV_contentsigning_data
        ObjectIdentifier PIV_contentsigning_data_OID = ObjectIdentifier.newInternal(PIV_contentsigning_data);
        out_content_oid.putOID(PIV_contentsigning_data_OID);

        DerValue attr_oid = new DerValue(out_extKeyinfo.toByteArray());
        DerValue attr_value = new DerValue(out_content_oid.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_keyUsage.putSequence(x);

        // Now close the streams.
        try {
            out_content_oid.close();
            out_extKeyinfo.close();

        } catch (Throwable ex) {
            throw new MiddleWareException(ex.getMessage());
        }

        return out_keyUsage;


    }

    DerOutputStream get_keyUsgae(DerOutputStream certrequest) throws MiddleWareException, IOException {

        System.out.println("First Step "
                + util.arrayToHex(certrequest.toByteArray()));

        // ********************************************//
        // Now get the certificate template to be requested.

        ObjectIdentifier KeyUsage_data_OID = ObjectIdentifier.newInternal(KeyUsage_data);


        // String PIV="PIV-I Encryption";
        byte[] keyusage = {0x03, 0x02, 0x06, (byte) 0xC0};
        // byte[] keyusage={(byte) 0xC0};
        // Extension.newExtension(KeyUsage_data_OID,true,PIV.getBytes());

        // Extension.newExtension(KeyUsage_data_OID,true,keyusage);
        // CertificateExtensions exts = new CertificateExtensions();

        // exts.encode(certrequest);
        DerOutputStream out_1, out_2, out_3, out_4;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_4 = new DerOutputStream();

        out_1.putOID(KeyUsage_data_OID);
        out_2.putOctetString(keyusage);

        DerValue attr = new DerValue(out_1.toByteArray());
        DerValue attr1 = new DerValue(out_2.toByteArray());

        DerValue[] x = {attr, attr1};

        out_3.putSequence(x);

        // DerEncoder z= null;
        // z.derEncode(out_3);

        DerEncoder[] y = {out_3};
        out_4.putOrderedSet(DerValue.tag_Set, y);

        System.out.println("Second Step " + util.arrayToHex(out_4.toByteArray()));

        //System.out.println("3rd Step " + util.arrayToHex(out_ext.toByteArray()));

        //byte[] xy = util.combine_data(out_ext.toByteArray(),out_4.toByteArray());

        //System.out.println("4th Step " + util.arrayToHex(xy));

        //int len = xy.length;

        String tot_hex_size, hex_size;

        //hex_size = Integer.toHexString(len);
        //tot_hex_size = Integer.toHexString(len+2 );

        //	byte[] num_bytes = util.hex1ToByteArray(hex_size);
        byte[] tag_Sequence = {0x30};

        //byte[] tot_num_bytes=util.hex1ToByteArray(tot_hex_size);
        //byte[] tot_tag={(byte) 0xa0};


        //byte[] final_tag_Sequence=util.combine_data(tot_tag, tot_num_bytes);
        //final_tag_Sequence=util.combine_data(final_tag_Sequence,tag_Sequence );
        //	final_tag_Sequence=util.combine_data(final_tag_Sequence, num_bytes);

//		xy = util.combine_data(final_tag_Sequence,xy);

        //	System.out.println("4th_1 Step " + util.arrayToHex(xy));



        //	certrequest.write(xy);

        // Now close the streams.out_1, out_2, out_3, out_4
        try {
            out_1.close();
            out_2.close();
            out_3.close();
            out_4.close();

        } catch (Throwable ex) {
            throw new MiddleWareException(ex.getMessage());
        }

        return certrequest;
    }

    private byte[] calcDerLen(Integer len) {

        byte[] len_tag_small = {(byte) 0x81};
        byte[] len_tag_big = {(byte) 0x82};
        byte[] num_bytes_tag;

        String tot_hex_size;

        if (len <= 127) {
            tot_hex_size = Integer.toHexString(len);
            num_bytes_tag = util.hex1ToByteArray(tot_hex_size);

        } else if (len > 127 && len < 256) {
            tot_hex_size = Integer.toHexString(len);
            num_bytes_tag = util.combine_data(len_tag_small, util.hex1ToByteArray(tot_hex_size));

        } else {
            tot_hex_size = "0" + Integer.toHexString(len);
            num_bytes_tag = util.combine_data(len_tag_big, util.hex1ToByteArray(tot_hex_size));
        }


        return num_bytes_tag;
    }
    //-----BEGIN CERTIFICATE-----
	/* */
    //-----END CERTIFICATE-----
    /*Template display name: PIV-I Encryption
     Object identifier: 1.3.6.1.4.1.311.21.8.11101558.7210957.3753883.9214177.14656715.240.3258577.6401920
     Subject type: User
     OBJECT IDENTIFIER
     :                 certificateTemplate (1 3 6 1 4 1 311 21 7)
     694   49:               OCTET STRING
     :                 30 2F 06 27 2B 06 01 04 01 82 37 15 08 85 A5 CA
     :                 76 83 B8 8F 4D 81 E5 8F 1B 84 B2 B1 61 86 FE C9
     :                 4B 81 70 81 AB F9 23 87 94 B9 53 02 01 64 02 01
     :                 06
     */
    //Now we can save this CSR in PEM format in the file.
    // PEM format is base64 encoded form which is used when sending the CSR in
    // mail or upload to CA.

    /*public void toPEM(byte[] CSR){


     String type = "CERTIFICATE REQUEST";
     byte[] encoding = getEncoded();

     PemObject pemObject = new PemObject(type, encoding);

     StringWriter str = new StringWriter();
     PEMWriter pemWriter = new PEMWriter(str);
     try {
     pemWriter.writeObject(pemObject);
     } catch (IOException e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
     }
     try {
     pemWriter.close();
     } catch (IOException e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
     }
     try {
     str.close();
     } catch (IOException e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
     }

     System.out.println("In Conversion method" + str);


     /*	String  fileName= "c:/csr/csr.pem";
     File file_pem = new File(fileName);

     if (file_pem.getParentFile() != null) {
     file_pem.getParentFile().mkdirs();
     }

     try {
     file_pem.createNewFile();
     } catch (IOException e1) {
     // TODO Auto-generated catch block
     e1.printStackTrace();
     }

     FileWriter fcsr = null;

     try {
     fcsr = new FileWriter(file_pem);
     } catch (IOException e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
     }

     PEMWriter csr_pem = new PEMWriter(fcsr);

     try {
     csr_pem.writeObject(CSR);
     } catch (IOException e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
     }
     try {
     csr_pem.close();
     } catch (IOException e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
     } */
    //	}*/
}