package com.secuera.middleware.beans.piv;

import com.secuera.middleware.core.common.CommonUtil;

public class InitializationBean {

    CommonUtil cmnUtil = new CommonUtil();
    private String localPin;
    private String globalPin;
    private String unblockPin;
    private int localPinPTC;
    private int globalPinPTC;
    private int unblockPinPTC;
    
    private String insertGPKeys;
    private String diversifyGPKeys;
    private String gpKeysAlgorithmType;
    
    private byte[] encKey;
    private byte[] macKey;
    private byte[] dekKey;
    
    private String diversifyAdminKey;
    private String adminKeyAlgorithmType;
    private byte[] adminKey;

    /*
    public byte[] getLocalPin() {
        return localPin;
    }

    public void setLocalPin(String localPin) {
        this.localPin = cmnUtil.hexStringToByteArray(cmnUtil.stringToHex(localPin),8);
    }

    public byte[] getGlobalPin() {
        return globalPin;
    }

    public void setGlobalPin(String globalPin) {
        this.globalPin = cmnUtil.hexStringToByteArray(cmnUtil.stringToHex(globalPin),8);
    }

    public byte[] getUnblockPin() {
        return unblockPin;
    }

    public void setUnblockPin(String unblockPin) {
        this.unblockPin = cmnUtil.hexStringToByteArray(cmnUtil.stringToHex(unblockPin),8);
    }

    public byte[] getLocalPinPTC() {
        return localPinPTC;
    }

    public void setLocalPinPTC(Integer localPinPTC) {
        if (Integer.toHexString(localPinPTC).length() < 2) {
            this.localPinPTC = cmnUtil.hex1ToByteArray("0" + Integer.toHexString(localPinPTC));
        } else {
            this.localPinPTC = cmnUtil.hex1ToByteArray(Integer.toHexString(localPinPTC));
        }
    }

    public byte[] getGlobalPinPTC() {
        return globalPinPTC;
    }

    public void setGlobalPinPTC(Integer globalPinPTC) {
        if (Integer.toHexString(globalPinPTC).length() < 2) {
            this.globalPinPTC = cmnUtil.hex1ToByteArray("0" + Integer.toHexString(globalPinPTC));
        } else {
            this.globalPinPTC = cmnUtil.hex1ToByteArray(Integer.toHexString(globalPinPTC));
        }
    }

    public byte[] getUnblockPinPTC() {
        return unblockPinPTC;
    }

    public void setUnblockPinPTC(Integer unblockPinPTC) {
        if (Integer.toHexString(unblockPinPTC).length() < 2) {
            this.unblockPinPTC = cmnUtil.hex1ToByteArray("0" + Integer.toHexString(unblockPinPTC));
        } else {
            this.unblockPinPTC = cmnUtil.hex1ToByteArray(Integer.toHexString(unblockPinPTC));
        }
    }
     */ 
    public String getInsertGPKeys() {
        return insertGPKeys;
    }

    public void setInsertGPKeys(String insertGPKeys) {
        this.insertGPKeys = insertGPKeys;
    }

    public String getDiversifyGPKeys() {
        return diversifyGPKeys;
    }

    public void setDiversifyGPKeys(String diversifyGPKeys) {
        this.diversifyGPKeys = diversifyGPKeys;
    }
    
    public String getGpKeysAlgorithmType() {
        return gpKeysAlgorithmType;
    }

    public void setGpKeysAlgorithmType(String gpKeysAlgorithmType) {
        this.gpKeysAlgorithmType = gpKeysAlgorithmType;
    }

    public byte[] getEncKey() {
        return encKey;
    }

    public void setEncKey(String encKey) {
        this.encKey = cmnUtil.hex1ToByteArray(encKey);
    }

    public byte[] getMacKey() {
        return macKey;
    }

    public void setMacKey(String macKey) {
        this.macKey = cmnUtil.hex1ToByteArray(macKey);
    }

    public byte[] getDekKey() {
        return dekKey;
    }

    public void setDekKey(String dekKey) {
        this.dekKey = cmnUtil.hex1ToByteArray(dekKey);
    }

    public String getDiversifyAdminKey() {
        return diversifyAdminKey;
    }

    public void setDiversifyAdminKey(String diversifyAdminKey) {
        this.diversifyAdminKey = diversifyAdminKey;
    }

    public String getAdminKeyAlgorithmType() {
        return adminKeyAlgorithmType;
    }

    public void setAdminKeyAlgorithmType(String adminKeyAlgorithmType) {
        this.adminKeyAlgorithmType = adminKeyAlgorithmType;
    }

    public byte[] getAdminKey() {
        return adminKey;
    }

    public void setAdminKey(String adminKey) {
        this.adminKey = cmnUtil.hex1ToByteArray(adminKey);
    }


    
    public String getGlobalPin() {
        return globalPin;
    }

    public void setGlobalPin(String globalPin) {
        this.globalPin = globalPin;
    }

    public int getGlobalPinPTC() {
        return globalPinPTC;
    }

    public void setGlobalPinPTC(int globalPinPTC) {
        this.globalPinPTC = globalPinPTC;
    }

    public String getLocalPin() {
        return localPin;
    }

    public void setLocalPin(String localPin) {
        this.localPin = localPin;
    }

    public int getLocalPinPTC() {
        return localPinPTC;
    }

    public void setLocalPinPTC(int localPinPTC) {
        this.localPinPTC = localPinPTC;
    }

    public String getUnblockPin() {
        return unblockPin;
    }

    public void setUnblockPin(String unblockPin) {
        this.unblockPin = unblockPin;
    }

    public int getUnblockPinPTC() {
        return unblockPinPTC;
    }

    public void setUnblockPinPTC(int unblockPinPTC) {
        this.unblockPinPTC = unblockPinPTC;
    }

}