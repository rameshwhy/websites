package com.secuera.middleware.cardreader.beans;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;

public class ChuidBean {

	private String agencyCode;          //Bytes = 4
	private String systemCode;          //Bytes = 4
	private String credentialNumber;    //Bytes = 6
	private String cs;                  //Bytes = 1 (always set to 1)
	private String ici;                 //Bytes = 1 (always set to 1)
	private String person;              //Bytes = 16
	
	private String organizationID;      //Bytes = 4	
	private String duns;                // byte = 9
	private String GUID;                //Byte = 16
	private String expirationDate;      //Byte = 8  (09JAN2013)
	private String fascn;
        private PublicKey publicKey;
        private PrivateKey privateKey;
        byte[] contentCertificate;
        boolean SignatureVerified;
        Date signatureDate;
    
     //YYMMDDhhmmssZ
         
	public String getAgencyCode() {
		return agencyCode;
	}
	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}
	public String getSystemCode() {
		return systemCode;
	}
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}
	public String getCredentialNumber() {
		return credentialNumber;
	}
	public void setCredentialNumber(String credentialNumber) {
		this.credentialNumber = credentialNumber;
	}
	public String getCs() {
		return cs;
	}
	public void setCs(String cs) {
		this.cs = cs;
	}
	public String getIci() {
		return ici;
	}
	public void setIci(String ici) {
		this.ici = ici;
	}
	public String getPerson() {
		return person;
	}
	public void setPerson(String person) {
		this.person = person;
	}
	
	public String getOrganizationID() {
		return organizationID;
	}
	public void setOrganizationID(String organizationID) {
		this.organizationID = organizationID;
	}
	
	public String getDuns() {
		return duns;
	}
	public void setDuns(String duns) {
		this.duns = duns;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getGUID() {
		return GUID;
	}
	public void setGUID(String gUID) {
		GUID = gUID;
	}
	
        
	public String getFascn() {
		return fascn;
	}
	public void setFascn(String fascn) {
		this.fascn = fascn;
	}

    public byte[] getContentCertificate() {
        return contentCertificate;
    }

    public void setContentCertificate(byte[] contentCertificate) {
        this.contentCertificate = contentCertificate;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public boolean isSignatureVerified() {
        return SignatureVerified;
    }

    public void setSignatureVerified(boolean SignatureVerified) {
        this.SignatureVerified = SignatureVerified;
    }

    public Date getSignatureDate() {
        return signatureDate;
    }

    public void setSignatureDate(Date signatureDate) {
        this.signatureDate = signatureDate;
    }

    public String toString() {
        return "ChuidBean{" + "agencyCode=" + agencyCode + ", systemCode=" + systemCode + ", credentialNumber=" + credentialNumber + ", cs=" + cs + ", ici=" + ici + ", person=" + person + ", organizationID=" + organizationID + ", duns=" + duns + ", GUID=" + GUID + ", expirationDate=" + expirationDate + ", fascn=" + fascn + ", publicKey=" + publicKey + ", privateKey=" + privateKey + ", contentCertificate=" + contentCertificate + ", SignatureVerified=" + SignatureVerified + ", signatureDate=" + signatureDate + '}';
    }

    
        
	
	
	
	
}
