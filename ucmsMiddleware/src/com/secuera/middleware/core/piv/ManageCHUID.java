/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.beans.piv.ChuidBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import java.io.IOException;
import java.security.PrivateKey;
import java.util.Date;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class ManageCHUID {

    SendCommand sendCard = new SendCommand();
    CommonUtil util = new CommonUtil();
    GeneralAuth auth = new GeneralAuth();
    ExceptionMessages expMsg = new ExceptionMessages();
    ChuidBean chuid = new ChuidBean();
    String str_chuid;
    boolean signedData=false;
    public byte[] chuidDataSecu;
    public ManageCHUID() {
        super();
    }

    public ChuidBean readChuid(CardChannel channel) throws MiddleWareException{
        String str_result;
        boolean result=false;

        try {
            str_result = getChuid(channel);

            if (str_result == null || str_result.length() == 0) {
            } else {

                System.out.println(str_result);
                //parse CHUID
                parseChuid();
                
                if (signedData){
                //Verify Signature
                ParseAsmObject parseASm = new ParseAsmObject();
                   
                 result = parseASm.parseChuidObj(str_result);
                    

                chuid.setSignatureVerified(result);
                }else{
                    chuid.setSignatureVerified(false);
                }
                    
            }
            return chuid;

        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    public String getChuid(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String errorMsg;

        StringBuffer sb = new StringBuffer();
        byte[] apdu_chuid = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05, 0x5C,
            0x03, (byte) 0x5F, (byte) 0xC1, 0x02}; // BERT-TLV CHUID tag

        try {
            CommandAPDU CHUID_APDU = new CommandAPDU(apdu_chuid);

            res = sendCard.sendCommand(channel, CHUID_APDU, sb);

            //raise error if unable to Read CHUID
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "CHUID");
                throw new MiddleWareException(errorMsg);
            }
            if (res == null || res.length == 0) {
                str_chuid = "";

            } else {
                //convert byte array to string & remove parity bits
                str_chuid = util.arrayToHex(res);


                //remove leading characters
                if (str_chuid.substring(2, 4).equals("82")) {
                    str_chuid = str_chuid.substring(8);
                } else if (str_chuid.substring(2, 4).equals("81")) {
                    str_chuid = str_chuid.substring(6);
                } else {
                    str_chuid = str_chuid.substring(4);
                }

            }
            return str_chuid;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    private void parseChuid() throws MiddleWareException {
        // 	54 hex_fascn = "3019" + util.formatHexString(50, hex_fascn);
        //	12 hex_agncy_cde = "3104" + util.formatHexString(8, hex_agncy_cde);
        //	12 hex_org_iden = "3204" + util.formatHexString(8, hex_org_iden);
        //	22 hex_str_duns = "3309" + util.formatHexString(18, hex_str_duns);
        //	36 hex_str_guid = "3410" + hex_str_guid;
        //	20 hex_exp_date = "3508" + util.formatHexString(16, hex_exp_date);

        //3019 d4 e7 39 da 73 9c ed 39 ce 73 9d a1 68 5a 10 82 10 8c e7 39 84 11 92 57 fc --54
        //3104 39 39 39 39  --12
        //3204 31 32 33 20 -- 12
        //3309 31 32 32 20 20 20 20 20 20 --22
        //3410 ea 4d 40 aa 21 6b 45 d9 8b bb 4c 17 50 a6 af 62 --36 
        //3508 32 30 31 32 31 30 32 37 -- 20

        //define tag values for chuid
        String error_tag = "FE";
        String signed_object_tag = "3E";
        String str_tag;

        // Get the first tag.
        str_tag = str_chuid.substring(0, 2);

        while (!error_tag.equalsIgnoreCase(str_tag) && !signed_object_tag.equalsIgnoreCase(str_tag)) {
            parseChuidTag();
            str_tag = str_chuid.substring(0, 2);
            
            if (str_tag.equalsIgnoreCase(signed_object_tag)){
                signedData=true;
            }
            
        }

    }

    private void parseChuidTag() throws MiddleWareException {

        String str_tag_len;
        int tag_size = 0;

        if (str_chuid.substring(0, 2).equalsIgnoreCase("EE")) {
            // If CHUID tag 'EE' is present, then skip it and length value.
            str_chuid = str_chuid.substring(4);//sat123 changed bytes from 6 to 4
        } else if (str_chuid.substring(0, 2).equalsIgnoreCase("30")) {
            // Get fascn 
            str_tag_len = str_chuid.substring(2, 4);
            tag_size = Integer.valueOf(str_tag_len, 16).intValue();

            String str_fascn = str_chuid.substring(4, 4 + (tag_size * 2));
            String strfascn[] = util.parseFASCN(str_fascn);

            chuid.setFascn(str_fascn);
            chuid.setAgencyCode(strfascn[0]);
            chuid.setSystemCode(strfascn[1]);
            chuid.setCredentialNumber(strfascn[2]);
            chuid.setCs(strfascn[3]);
            chuid.setIci(strfascn[4]);
            chuid.setPerson(strfascn[5]);
            chuid.setOrganizationID(strfascn[5].substring(11, 15));
        } else if (str_chuid.substring(0, 2).equalsIgnoreCase("31")) {
            // Get Agency Code 
            str_tag_len = str_chuid.substring(2, 4);
            tag_size = Integer.valueOf(str_tag_len, 16).intValue();
            String agencyCode = util.hexToString(str_chuid.substring(4, 4 + (tag_size * 2)));
            chuid.setAgencyCode(agencyCode);
        } else if (str_chuid.substring(0, 2).equalsIgnoreCase("32")) {
            // Get Organization  
            str_tag_len = str_chuid.substring(2, 4);
            tag_size = Integer.valueOf(str_tag_len, 16).intValue();
            String organizationID = util.hexToString(str_chuid.substring(4, 4 + (tag_size * 2)));
            chuid.setOrganizationID(organizationID);
        } else if (str_chuid.substring(0, 2).equalsIgnoreCase("33")) {
            // Get DUNS  
            str_tag_len = str_chuid.substring(2, 4);
            tag_size = Integer.valueOf(str_tag_len, 16).intValue();
            String duns = util.hexToString(str_chuid.substring(4, 4 + (tag_size * 2)));
            chuid.setDuns(duns);
        } else if (str_chuid.substring(0, 2).equalsIgnoreCase("34")) {
            // Get GUID  
            str_tag_len = str_chuid.substring(2, 4);
            tag_size = Integer.valueOf(str_tag_len, 16).intValue();
            String guid = str_chuid.substring(4, 4 + (tag_size * 2));
            chuid.setGUID(guid);
        } else if (str_chuid.substring(0, 2).equalsIgnoreCase("35")) {
            // Get expirationDate  
            str_tag_len = str_chuid.substring(2, 4);
            tag_size = Integer.valueOf(str_tag_len, 16).intValue();
            String expirationDate = util.hexToString(str_chuid.substring(4, 4 + (tag_size * 2)));
            chuid.setExpirationDate(expirationDate);
        }
        
        /*
        switch (str_chuid.substring(0, 2).toUpperCase()) {
            case "EE":

                // If CHUID tag 'EE' is present, then skip it and length value.
                str_chuid = str_chuid.substring(4);//sat123 changed bytes from 6 to 4
                break;

            case "30":
                // Get fascn 
                str_tag_len = str_chuid.substring(2, 4);
                tag_size = Integer.valueOf(str_tag_len, 16).intValue();

                String str_fascn = str_chuid.substring(4, 4 + (tag_size * 2));
                String strfascn[] = util.parseFASCN(str_fascn);

                chuid.setFascn(str_fascn);
                chuid.setAgencyCode(strfascn[0]);
                chuid.setSystemCode(strfascn[1]);
                chuid.setCredentialNumber(strfascn[2]);
                chuid.setCs(strfascn[3]);
                chuid.setIci(strfascn[4]);
                chuid.setPerson(strfascn[5]);
                chuid.setOrganizationID(strfascn[5].substring(11, 15));

                break;

            case "31":
                // Get Agency Code 
                str_tag_len = str_chuid.substring(2, 4);
                tag_size = Integer.valueOf(str_tag_len, 16).intValue();
                String agencyCode = util.hexToString(str_chuid.substring(4, 4 + (tag_size * 2)));
                chuid.setAgencyCode(agencyCode);
                break;

            case "32":
                // Get Organization  
                str_tag_len = str_chuid.substring(2, 4);
                tag_size = Integer.valueOf(str_tag_len, 16).intValue();
                String organizationID = util.hexToString(str_chuid.substring(4, 4 + (tag_size * 2)));
                chuid.setOrganizationID(organizationID);
                break;

            case "33":
                // Get DUNS  
                str_tag_len = str_chuid.substring(2, 4);
                tag_size = Integer.valueOf(str_tag_len, 16).intValue();
                String duns = util.hexToString(str_chuid.substring(4, 4 + (tag_size * 2)));
                chuid.setDuns(duns);
                break;

            case "34":
                // Get GUID  
                str_tag_len = str_chuid.substring(2, 4);
                tag_size = Integer.valueOf(str_tag_len, 16).intValue();
                String guid = str_chuid.substring(4, 4 + (tag_size * 2));
                chuid.setGUID(guid);
                break;

            case "35":
                // Get expirationDate  
                str_tag_len = str_chuid.substring(2, 4);
                tag_size = Integer.valueOf(str_tag_len, 16).intValue();
                String expirationDate = util.hexToString(str_chuid.substring(4, 4 + (tag_size * 2)));
                chuid.setExpirationDate(expirationDate);
                break;
        }*/


        //Now reset the CHUID Stream to next tag.
        str_chuid = str_chuid.substring(4 + (tag_size * 2));

    }

    public boolean writeChuid(CardChannel channel, ChuidBean chuid, Date dt_signingtime, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY, String algoType) throws MiddleWareException {

        /*
         * Data Element		       Tag             Type Max.		Bytes         Mandatory
         * Buffer Length       		EE               Fixed                   2             Yes
         * FASC-N (SEIWG-012)  		30 		 Fixed 			 25	       Yes
         * Agency Code 			31 	         Fixed 			 4             No
         * Organization Identifier 	32 		 Fixed 			 4             No
         * DUNS 			33 		 Fixed 			 9	       No
         * GUID 			34 		 Fixed 			 16            Yes
         * Expiration Date 		35 		 Date (YYYYMMDD) 	 8             Yes
         * RFU 				38-
         * 				3C
         * Authentication Key Map 	3D 		Variable 		 512           No
         * Asymmetric Signature 	3E 		Variable 		 2816          Yes
         * Error Detection Code 	FE 		LRC 			 1             No
         * /
        
        
        /*
         * FASC-N Consists of following elements
         * AGENCY CODE = 4 bytes ----'9999' for PIV-I cards
         * SYSTEM CODE = 4 bytes ----'9999' for PIV-I cards
         * CREDENTIAL# = 6 bytes ----'999999' for PIV-I cards
         * CS = 1
         * ICI = 1
         * PI = 10 Bytes ---->1112223333
         * OC= 1
         * OI=1234
         * POA=5
         * LRC = 7
         */
        byte[] contentCertificate = null;
        PrivateKey privateKey = null;
        boolean signChuid = false;
        String agency_cde = chuid.getAgencyCode();
        String system_cde = chuid.getSystemCode();
        String cred_nbr = chuid.getCredentialNumber();
        String str_cs = chuid.getCs();
        String str_ici = chuid.getIci();
        String pi_iden = chuid.getPerson();
        String org_iden = chuid.getOrganizationID();
        String str_duns = chuid.getDuns();
        String exp_date = chuid.getExpirationDate();
        String str_guid = chuid.getGUID();


        if (chuid.getContentCertificateBean()!= null) {
            contentCertificate = chuid.getContentCertificateBean().getContentCertificate();
            privateKey = chuid.getContentCertificateBean().getPrivateKey();
            signChuid = true;
        }


        if (isEmpty(agency_cde)) {
            agency_cde = "";
        }
        if (isEmpty(system_cde)) {
            system_cde = "";
        }
        if (isEmpty(cred_nbr)) {
            cred_nbr = "";
        }
        if (isEmpty(str_cs)) {
            str_cs = "";
        }
        if (isEmpty(str_ici)) {
            str_ici = "";
        }
        if (isEmpty(pi_iden)) {
            pi_iden = "";
        }
        if (isEmpty(org_iden)) {
            org_iden = "";
        }
        if (isEmpty(str_duns)) {
            str_duns = "";
        }
        if (isEmpty(exp_date)) {
            exp_date = "";
        }
        if (isEmpty(str_guid)) {
            str_guid = "";
        }


        boolean rtnvalue = false;

        StringBuffer sb = new StringBuffer();
        byte[] byte_sig = null;

        /* Call method to calculate FASC-N */
        String hex_fascn = util.getFASCN(agency_cde, system_cde, cred_nbr,
                str_cs, str_ici, pi_iden);


        // Now convert to Hex.
        String hex_agncy_cde = util.stringToHex(agency_cde);
        String hex_org_iden = util.stringToHex(org_iden);
        String hex_str_duns = util.stringToHex(str_duns);
        //satish String hex_str_guid = util.genUUID();
        String hex_exp_date = util.stringToHex(exp_date);
        String hex_asm_tag = "3E"; // For asymetric certificate

        hex_fascn = "3019" + util.formatHexString(50, hex_fascn);
        hex_agncy_cde = "3104" + util.formatHexString(8, hex_agncy_cde);
        hex_org_iden = "3204" + util.formatHexString(8, hex_org_iden);
        hex_str_duns = "3309" + util.formatHexString(18, hex_str_duns);
        str_guid = "3410" + str_guid;
        hex_exp_date = "3508" + util.formatHexString(16, hex_exp_date);

        String hex_data = hex_fascn + hex_agncy_cde + hex_org_iden + hex_str_duns
                + str_guid + hex_exp_date;

        String hex_error = "FE00";
        byte[] byte_data = util.hex1ToByteArray(hex_data);

        byte[] chuid_data;
        byte[] asm_tag;
        Integer len_sig = 0;
        if (signChuid) {
            GenAsmSig chuidasmSig = new GenAsmSig();
            try {
                // Now generate Signature for the CHUID. >> generate content certfificate >>byte[]
                // For Signature, make attach hex_error to hex_data.
                String strSig = chuidasmSig.genAsmSig(hex_data + hex_error, privateKey, contentCertificate, "", dt_signingtime, 'C', algoType);

                byte_sig = util.hex1ToByteArray(strSig);

            } catch (IOException ex) {
                throw new MiddleWareException(ex.getMessage());
            }
            asm_tag = util.hex1ToByteArray(hex_asm_tag);
            // Now calculate length of signature data
            len_sig = byte_sig.length;


            String hex_len_sig;

            //System.out.println("Integer Length of Signature " + len_sig);

            // Following condition makes sure that converted hex string is always
            // even.
            if (len_sig > 255 && len_sig < 4096) {
                hex_len_sig = "0" + Integer.toHexString(len_sig);

            } else {
                hex_len_sig = Integer.toHexString(len_sig);

            }

            byte[] asm_sig_len = util.hex1ToByteArray(hex_len_sig);

            byte[] asm_sig_len_tag = util.hex1ToByteArray("82");

            chuid_data = util.combine_data(byte_data, asm_tag);
            chuid_data = util.combine_data(chuid_data, asm_sig_len_tag);
            chuid_data = util.combine_data(chuid_data, asm_sig_len);
            chuid_data = util.combine_data(chuid_data, byte_sig);
        }else{
            chuid_data = byte_data;
        }
        

        //*************************************

        
        
        byte[] error_byte = util.hex1ToByteArray(hex_error);

        chuid_data = util.combine_data(chuid_data, error_byte);
        
        //populate this variable for security container
        chuidDataSecu = chuid_data;
        
        // Now calculate length of the whole CHUID buffer.
        Integer len_chuid = chuid_data.length;
        String hex_chuid_len;

        System.out.println("CHUID Length  " + len_chuid);
        // Following condition makes sure that converted hex string is always
        // even.
        if (len_chuid > 255 && len_chuid < 4096) {
            hex_chuid_len = "0" + Integer.toHexString(len_chuid);

        } else {
            hex_chuid_len = Integer.toHexString(len_chuid);

        }

        byte[] chuid_data_len = util.hex1ToByteArray(hex_chuid_len);

        //************************************************************

        //byte[] hex_buf_len = util.hex1ToByteArray("EE02"); // Tag for CHUID
        // Buffer length

        //byte[] hex_buff = util.combine_data(hex_buf_len, chuid_data_len);

        // Now we make the final CHUID Data
        // chuid_data = util.combine_data(hex_buff, chuid_data);

        Integer len = chuid_data.length;


        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }

        byte[] apdu_chuid = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};
        byte[] cont_id = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x02}; // For
        // CHUID

        String tot_hex_size;
        String tot_hex_data_size;
        byte[] zero_size = {0x00};


        // Now Determine value of Bert Tag
        byte[] bert_tag = new byte[2];
        byte[] bert_tag_size = {0x53}; // identifier for size of data
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for size of data bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size  of data bytes

        if (len <= 127) {
            bert_tag = bert_tag_size;
        } else if (len > 255) {

            bert_tag = bert_tag_size_big;

        } else {
            bert_tag = bert_tag_size_small;

        }


        // Now calculate actual data size being stored
        // Following condition makes sure that converted hex string is always
        // even.
        if (len > 255 && len < 4096) {

            tot_hex_data_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_data_size = Integer.toHexString(len);

        }

        byte[] num_of_bytes = util.hex1ToByteArray(tot_hex_data_size);


        // Combine all info here.

        byte[] apdu_data = util.combine_data(cont_id, bert_tag);
        apdu_data = util.combine_data(apdu_data, num_of_bytes);
        apdu_data = util.combine_data(apdu_data, chuid_data);

        // Now Calculate the LC i-e size of apdu command being sent.
        // Following condition makes sure that converted hex string is always even.

        Integer len_apdu = apdu_data.length;

        if (len_apdu > 255 && len_apdu < 4096) {
            tot_hex_size = "0" + Integer.toHexString(len_apdu);

        } else {
            tot_hex_size = Integer.toHexString(len_apdu);

        }

        byte[] total_size = util.hex1ToByteArray(tot_hex_size);


        if (total_size.length > 1) {
            total_size = util.combine_data(zero_size, total_size);
        }

        apdu_chuid = util.combine_data(apdu_chuid, total_size);
        apdu_chuid = util.combine_data(apdu_chuid, apdu_data);

        //sat 

        System.out.println(util.arrayToHex(apdu_chuid));

        CommandAPDU CHUID_APDU = new CommandAPDU(apdu_chuid);

        try {

            sendCard.sendCommand(channel, CHUID_APDU, sb);
        } catch (CardException e) {

            throw new MiddleWareException(e.getMessage());
        }

        if (sb.toString().equals("9000")) {
            rtnvalue = true;
        }

        return rtnvalue;
    }

    private boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
}
