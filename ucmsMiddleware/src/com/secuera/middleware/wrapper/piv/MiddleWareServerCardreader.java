/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.wrapper.piv;

import com.secuera.middleware.beans.piv.BiometricBean;
import com.secuera.middleware.beans.piv.CardInfoBean;
import com.secuera.middleware.beans.piv.CSRBean;
import com.secuera.middleware.beans.piv.CertificateBean;
import com.secuera.middleware.beans.piv.ChuidBean;
import com.secuera.middleware.beans.piv.ContainerInfoBean;
import com.secuera.middleware.beans.piv.ContentCertificateBean;
import com.secuera.middleware.beans.piv.CredentialInformationBean;
import com.secuera.middleware.beans.piv.CredentialKeys;
import com.secuera.middleware.beans.piv.FacialBean;
import com.secuera.middleware.beans.piv.FacialImageBean;
import com.secuera.middleware.beans.piv.InitializationBean;
import com.secuera.middleware.beans.piv.LoadUserCertificates;
import com.secuera.middleware.beans.piv.MasterKeyCeremonyBean;
import com.secuera.middleware.beans.piv.MasterKeyCeremonyResultBean;
import com.secuera.middleware.beans.piv.PersonalInformationBean;
import com.secuera.middleware.beans.piv.PersonalizationBean;
import com.secuera.middleware.beans.piv.PinPolicyBean;
import com.secuera.middleware.beans.piv.PrintedInfoBean;
import com.secuera.middleware.beans.piv.SecurityBean;
import com.secuera.middleware.beans.piv.UserCSR;
import com.secuera.middleware.beans.piv.UserCSRResponse;
import com.secuera.middleware.beans.piv.UserCertificates;
import com.secuera.middleware.beans.piv.UserContentCSR;
import com.secuera.middleware.beans.piv.UserFingerPintBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.util.Date;
import java.util.List;
import javax.smartcardio.CardChannel;
import javax.swing.ImageIcon;

/**
 *
 * @author admin
 */
public interface MiddleWareServerCardreader {
    //basic Card Access Functions
    public  List<String> getTerminalsList() throws MiddleWareException;
    
    /************************ credential Information ***************************************/
    public CredentialInformationBean readCredentialInformation(CardChannel channel) throws MiddleWareException;
    
    //CHUID-FASCN Functions
    public byte[] readFASCN(CardChannel channel) throws MiddleWareException;
    public ChuidBean readChuid(CardChannel channel) throws MiddleWareException;
    
    //CIN/IIN/CUID/BAP
    public String readCIN(CardChannel channel) throws MiddleWareException;
    public String readIIN(CardChannel channel) throws MiddleWareException;
    public String readCUID(CardChannel channel) throws MiddleWareException;
    public String readBAP(CardChannel channel) throws MiddleWareException;
    public String readATR(CardChannel channel) throws MiddleWareException;
    public String readCardModel(CardChannel channel) throws MiddleWareException;
    
    public CardInfoBean readCardInfo(CardChannel channel) throws MiddleWareException;    
    /****************************************************************************************/
    
    /************************ User certificates *********************************************/
    public UserCertificates readUserCertificates(CardChannel channel) throws MiddleWareException;
    public CertificateBean readCertificate(CardChannel channel,String CertificateType) throws MiddleWareException;
    /****************************************************************************************/
    
    /************************ generate User CSR/Certificate  *******************************************/
    public UserCSR generateUserCSR(CardChannel channel,String adminKey, String localPin,UserCSR userCSR) throws MiddleWareException;
    public UserCSR generateUserCertificate(CardChannel channel, UserCSR userCSR, String inURL,String caType) throws MiddleWareException;
    /****************************************************************************************/
    
    /************************ generate Content CSR/Certificate  *******************************************/
    public UserContentCSR generateContentCSR(CardChannel channel,CSRBean certBean) throws MiddleWareException;
    public String generateContentCertificate(CardChannel channel, String contentCSR, String inURL,String caType) throws MiddleWareException;
    /****************************************************************************************/
    
    /**************************** Personal Information ***************************************/
    public PersonalInformationBean readPersonalInformation(CardChannel channel,String pin,String pinType) throws MiddleWareException;
    public PrintedInfoBean readPrintedInfo(CardChannel channel,String pin,String pinType) throws MiddleWareException;
    public FacialBean readFacialImage(CardChannel channel,String pin,String pinType) throws MiddleWareException;
    
    /****************************************************************************************/
    
    /*********************************** PIN Management Functions ******************************************/     
    //Local PIN Management Functions
    public boolean verifyLocalPin(CardChannel channel, String localPin) throws MiddleWareException;
    public boolean changeLocalPin(CardChannel channel, String oldPin, String newPin) throws MiddleWareException;
    public boolean resetLocalPin(CardChannel channel, String unblockPin, String newPin) throws MiddleWareException;

    //Global PIN Management Functions
    public boolean verifyGlobalPin(CardChannel channel, String globalPin) throws MiddleWareException;
    public boolean changeGlobalPin(CardChannel channel, String oldPin, String newPin) throws MiddleWareException;
    public boolean resetGlobalPin(CardChannel channel, CredentialKeys credentialKeys, String newPin,int globalPinRetry) throws MiddleWareException;

    //PUK functions Management Functions
    public boolean changePUK(CardChannel channel,String oldPUK, String newPUK) throws MiddleWareException;
    public boolean resetPUK(CardChannel channel, CredentialKeys credentialKeys, String newPUK,int pukRetry) throws MiddleWareException;
    
    /******************************************************************************************************/
    
    /********************************* card personalization ***********************************************/ 
    public List cardPersonalization(CardChannel channel, CredentialKeys credentialKeys, PersonalizationBean persoBean) throws MiddleWareException;
    public List cardInitialization(CardChannel channel,CredentialKeys credentialKeys,InitializationBean initvalue) throws MiddleWareException;
    public boolean writeChuid(CardChannel channel,ChuidBean chuid, String adminKey,String algoType) throws MiddleWareException;
    public boolean writePrintedInfo(CardChannel channel, PrintedInfoBean printInfo, String adminKey) throws MiddleWareException;
    public boolean writeFacialImage(CardChannel channel,FacialImageBean facialImage, String adminKey,String algoType) throws MiddleWareException;
    public boolean loadCertificate(CardChannel channel,String adminKey,byte[] certificate,String certType) throws MiddleWareException;
    public List loadUserCertificate(CardChannel channel,String adminKey,LoadUserCertificates userCertificate) throws MiddleWareException;
    
    /******************************************************************************************************/
    
    /**************************** Finger Print Function *****************************************************/
    public boolean enrollUserFingerPrints(CardChannel channel,UserFingerPintBean userFingerPrint, String adminKey) throws MiddleWareException;
    public boolean enrollFingerPrint(CardChannel channel,byte[] fingerPrint, String fingerID,String adminKey) throws MiddleWareException;
    public boolean verifyFingerPrint(CardChannel channel,String fingerPrint) throws MiddleWareException;
    
    /******************************************************************************************************/
    
    /*************************** Lock Card functions *********************************************************/
    public boolean cardLock(CardChannel channel,CredentialKeys credentialKeys) throws MiddleWareException;
    public boolean cardUnlock(CardChannel channel,CredentialKeys credentialKeys) throws MiddleWareException;
    public boolean isCardLocked(CardChannel channel) throws MiddleWareException;
    /*********************************************************************************************************/
    
    /**************************** Misc Function *****************************************************/  
    public ContainerInfoBean readContainerInfo(CardChannel channel, String adminKey) throws MiddleWareException;
    public boolean loadSecurityObject(CardChannel channel, ContentCertificateBean contentCertificate,Date signatureDate, String adminKey, String algoType) throws MiddleWareException;
    public MasterKeyCeremonyResultBean masterKeyCeremony(CardChannel channel,MasterKeyCeremonyBean masterKeyBean) throws MiddleWareException;
    public boolean verifySecureChannel(CardChannel channel, String secureChannel) throws MiddleWareException;
    public boolean verifyCredentialKeys(CardChannel channel,CredentialKeys credentialKeys, InitializationBean initvalue, String secureChannel) throws MiddleWareException;
    public Integer readPinLength(CardChannel channel,String pinType) throws MiddleWareException;
    public PinPolicyBean readPinPolicy(CardChannel channel) throws MiddleWareException;
    /*********************************************************************************************************/
    /************ security Container Functions ***************************************************************/
    public boolean loadSecurityContainer(CardChannel channel,String adminKey,String localPin,ContentCertificateBean contentCert,String algoType,Date signatureDate) throws MiddleWareException;
    public SecurityBean verifySecurityContainer(CardChannel channel,String localPin) throws MiddleWareException;
    
    
    /*********************************************************************************************************/
    
    //public ContentCertificateBean  generateContentCertificate(CardChannel channel,CSRBean certBean) throws MiddleWareException;
    public boolean CardKeysVerification(CardChannel channel,CredentialKeys credentialKeys) throws MiddleWareException;
    //Misc supporting Functions
    //finger print functions
    //depricated
    //public BiometricBean readFingerPrintSecu(CardChannel channel,String localPin, String globalPin) throws MiddleWareException;
    public List<String> getFPTerminalsID3( int ImgQuality,int ImgCount) throws MiddleWareException;
    public BiometricBean readFingerPrintID3( int ImgQuality,int ImgCount,String deviceName,Integer fgType) throws MiddleWareException;
    public boolean EndOfPerso(CardChannel channel,CredentialKeys credentialKeys) throws MiddleWareException;
    //public SecurityBean readSecContainer(CardChannel channel) throws MiddleWareException;
    //Unused functions
    //public UserCSRResponse cardPersoWithInitialization(CardChannel channel, CredentialKeys credentialKeys, InitializationBean initvalue, PersonalizationBean persoBean, UserCSR userCSR) throws MiddleWareException; 
    //public String loadCertificate(CardChannel channel,char certificateType,String algorithmType, String piv_admin_key, String commonName, String organisationalunit,String organisation, String city, String state,String country,String certificateTemplate, String caType)  throws MiddleWareException;
    //public UserCSR generateUserCertificateall(CardChannel channel,String adminKey,UserCSR userCSR, String localPin) throws MiddleWareException;
    //public byte[] generateCertificate(CardChannel channel,String adminKey,CSRBean certBean, String localPin) throws MiddleWareException;
    

}
