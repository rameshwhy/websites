/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import com.secuera.middleware.core.common.CommonUtil;

/**
 *
 * @author SEAdmin
 */
public class CredentialKeys {

    private byte[] encKey;
    private byte[] macKey;
    private byte[] dekKey;
    private byte[] masterKey;
    private byte[] adminKey;
    private String diversifyGPKeys;
    private String diversifyAdminKey;
    private String gpKeysAlgorithmType;
    private String adminKeyAlgorithmType;
    private String masterKeyAlgorithmType;
    //private byte[] piv9E08;    
    private byte[] cardManagerAid;
    CommonUtil cmnUtil = new CommonUtil();

    public byte[] getEncKey() {
        return encKey;
    }

    public void setEncKey(String encKey) {
        this.encKey = cmnUtil.hex1ToByteArray(encKey);
    }

    public byte[] getMacKey() {
        return macKey;
    }

    public void setMacKey(String macKey) {
        this.macKey = cmnUtil.hex1ToByteArray(macKey);
    }

    public byte[] getDekKey() {
        return dekKey;
    }

    public void setDekKey(String dekKey) {
        this.dekKey = cmnUtil.hex1ToByteArray(dekKey);
    }

    public byte[] getMasterKey() {
        return masterKey;
    }

    public void setMasterKey(String masterKey) {
        this.masterKey = cmnUtil.hex1ToByteArray(masterKey);
    }

    public byte[] getAdminKey() {
        return adminKey;
    }

    public void setAdminKey(String adminKey) {
        this.adminKey = cmnUtil.hex1ToByteArray(adminKey);
    }

    public String getDiversifyGPKeys() {
        return diversifyGPKeys;
    }

    public void setDiversifyGPKeys(String diversifyGPKeys) {
        this.diversifyGPKeys = diversifyGPKeys;
    }

    public byte[] getCardManagerAid() {
        return cardManagerAid;
    }

    public void setCardManagerAid(String cardManagerAid) {
        this.cardManagerAid = cmnUtil.hex1ToByteArray(cardManagerAid);
    }

    public String getDiversifyAdminKey() {
        return diversifyAdminKey;
    }

    public void setDiversifyAdminKey(String diversifyAdminKey) {
        this.diversifyAdminKey = diversifyAdminKey;
    }

    public String getGpKeysAlgorithmType() {
        return gpKeysAlgorithmType;
    }

    public void setGpKeysAlgorithmType(String gpKeysAlgorithmType) {
        this.gpKeysAlgorithmType = gpKeysAlgorithmType;
    }

    public String getAdminKeyAlgorithmType() {
        return adminKeyAlgorithmType;
    }

    public void setAdminKeyAlgorithmType(String adminKeyAlgorithmType) {
        this.adminKeyAlgorithmType = adminKeyAlgorithmType;
    }

    public String getMasterKeyAlgorithmType() {
        return masterKeyAlgorithmType;
    }

    public void setMasterKeyAlgorithmType(String masterKeyAlgorithmType) {
        this.masterKeyAlgorithmType = masterKeyAlgorithmType;
    }
}
