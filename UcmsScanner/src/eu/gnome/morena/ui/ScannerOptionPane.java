// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.ui;

import eu.gnome.morena.Scanner;
import eu.gnome.morena.wia.SupportedPropertyValues;
import eu.gnome.morena.wia.WIAScanner;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.JTextComponent;

public class ScannerOptionPane extends JPanel
{
    class SliderListenerAndVerifier extends InputVerifier
        implements ChangeListener
    {

        public void stateChanged(ChangeEvent changeevent)
        {
            JSlider jslider = (JSlider)changeevent.getSource();
            showComp.setText(String.valueOf(jslider.getValue()));
            showComp.setBackground(Color.WHITE);
        }

        public boolean verify(JComponent jcomponent)
        {
            int i = 0;
            try
            {
                i = Integer.parseInt(((JTextComponent)jcomponent).getText());
            }
            catch(NumberFormatException numberformatexception)
            {
                return false;
            }
            isCorrect = sliderComp.getMinimum() <= i && i <= sliderComp.getMaximum();
            if(isCorrect)
                showComp.setBackground(Color.WHITE);
            else
                showComp.setBackground(Color.PINK);
            return isCorrect;
        }

        private JSlider sliderComp;
        private JTextComponent showComp;
        private boolean isCorrect;
        final ScannerOptionPane this$0;


        public SliderListenerAndVerifier(JSlider jslider, JTextComponent jtextcomponent)
        {
            super();
            this$0 = ScannerOptionPane.this;
            sliderComp = jslider;
            showComp = jtextcomponent;
            isCorrect = true;
        }
    }

    class Mode
    {

        public int hashCode()
        {
            int i = 1;
            i = 31 * i + getOuterType().hashCode();
            i = 31 * i + value;
            return i;
        }

        public boolean equals(Object obj)
        {
            if(this == obj)
                return true;
            if(obj == null)
                return false;
            if(getClass() != obj.getClass())
                return false;
            Mode mode = (Mode)obj;
            if(!getOuterType().equals(mode.getOuterType()))
                return false;
            return value == mode.value;
        }

        public String toString()
        {
            return desc;
        }

        private ScannerOptionPane getOuterType()
        {
            return ScannerOptionPane.this;
        }

        private String desc;
        private int value;
        final ScannerOptionPane this$0;


        public Mode(int i, String s)
        {
            super();
            this$0 = ScannerOptionPane.this;
            value = i;
            desc = s;
        }
    }


    public ScannerOptionPane(Window window)
    {
        constraints = new GridBagConstraints();
        okAction = new AbstractAction("OK") {

            public void actionPerformed(ActionEvent actionevent)
            {
                if(wiaScanner != null && (!brightnessVerifier.isCorrect || !contrastVerifier.isCorrect))
                {
                    if(brightnessVerifier.isCorrect)
                        contrastField.setBackground(Color.PINK);
                    else
                        brightnessField.setBackground(Color.PINK);
                } else
                {
                    save();
                    parent.setVisible(false);
                }
            }

            final ScannerOptionPane this$0;

            
            {
                // super(s);
                this$0 = ScannerOptionPane.this;
               
            }
        }
;
        cancelAction = new AbstractAction("Cancel") {

            public void actionPerformed(ActionEvent actionevent)
            {
                parent.setVisible(false);
            }

            final ScannerOptionPane this$0;

            
            {
                this$0 = ScannerOptionPane.this;
               // super(s);
            }
        }
;
        parent = window;
        wiaScanner = null;
        modesMap = new HashMap();
        Mode mode = new Mode(-1, "Black & White (1 bit)");
        modesMap.put(Integer.valueOf(-1), mode);
        mode = new Mode(1, "Black & White (1 bit)");
        modesMap.put(Integer.valueOf(1), mode);
        mode = new Mode(-8, "Grayscale (8 bit)");
        modesMap.put(Integer.valueOf(-8), mode);
        mode = new Mode(-16, "Grayscale (16 bit)");
        modesMap.put(Integer.valueOf(-16), mode);
        mode = new Mode(8, "Color (8 bit)");
        modesMap.put(Integer.valueOf(8), mode);
        mode = new Mode(16, "Color (16 bit)");
        modesMap.put(Integer.valueOf(16), mode);
        setLayout(layout = new GridBagLayout());
        layout.setConstraints(this, constraints);
        setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
        constraints.insets = new Insets(2, 4, 2, 4);
        saved = false;
    }

    public void load(Scanner scanner1)
    {
        scanner = scanner1;
        if(scanner1 instanceof WIAScanner)
            wiaScanner = (WIAScanner)scanner1;
        Vector vector = new Vector();
        if(scanner1.getFlatbedFunctionalUnit() >= 0)
            vector.add("flatbed");
        if(scanner1.getFeederFunctionalUnit() >= 0)
            vector.add("feeder");
        if(vector.isEmpty())
            vector.add("default");
        funitComp = new JComboBox(vector);
        duplexComp = new JCheckBox("Duplex");
        duplexComp.setSelected(scanner1.isDuplexEnabled());
        java.util.List list = scanner1.getSupportedModes();
        int i = scanner1.getMode();
        Mode mode = (Mode)modesMap.get(Integer.valueOf(i));
        Vector vector1 = new Vector();
        Object obj = list.iterator();
        do
        {
            if(!((Iterator) (obj)).hasNext())
                break;
            Integer integer = (Integer)((Iterator) (obj)).next();
            Mode mode1 = (Mode)modesMap.get(integer);
            if(mode1 != null)
                vector1.add(mode1);
        } while(true);
        modesComp = new JComboBox(vector1);
        if(vector1.contains(mode))
            modesComp.setSelectedItem(mode);
        obj = scanner1.getSupportedResolutions();
        resolComp = new JComboBox(((java.util.List) (obj)).toArray(new Integer[((java.util.List) (obj)).size()]));
        resolComp.setEditable(true);
        resolComp.setSelectedItem(Integer.valueOf(scanner1.getResolution()));
        okButton = new JButton(okAction);
        cancelButton = new JButton(cancelAction);
        JPanel jpanel = new JPanel();
        jpanel.setBorder(BorderFactory.createEmptyBorder(20, 1, 10, 1));
        GridLayout gridlayout = new GridLayout(1, 2);
        gridlayout.setHgap(3);
        jpanel.setLayout(gridlayout);
        jpanel.add(okButton);
        jpanel.add(cancelButton);
        int j = 0;
        add(null, new JLabel("Source"), 0, j, 1, 1, 0, 17, 1, 1, true);
        add("fu", funitComp, 1, j, 1, 1, 0, 17, 100, 1, true);
        add("duplex", duplexComp, 2, j++, 1, 1, 0, 17, 100, 1, scanner1.isDuplexSupported());
        add(null, new JLabel(" "), 0, j++, 3, 1, 1, 10, 1, 1, true);
        add(null, new JLabel("Mode"), 0, j, 1, 1, 0, 17, 1, 1, true);
        add("mode", modesComp, 1, j++, 2, 1, 0, 17, 100, 1, true);
        add(null, new JLabel("Resolution"), 0, j, 1, 1, 0, 17, 1, 1, true);
        add("resolution", resolComp, 1, j++, 2, 1, 0, 17, 100, 1, true);
        if(wiaScanner != null)
        {
            add(null, new JLabel(" "), 0, j++, 3, 1, 1, 10, 1, 1, true);
            SupportedPropertyValues supportedpropertyvalues = wiaScanner.getPropertyValues(6154);
            if(supportedpropertyvalues != null && supportedpropertyvalues.isRange())
            {
                int k = supportedpropertyvalues.getMaxValue() - supportedpropertyvalues.getMinValue();
                brightnessComp = new JSlider(0, supportedpropertyvalues.getMinValue(), supportedpropertyvalues.getMaxValue(), wiaScanner.getBrightness());
                brightnessField = new JTextField(4);
                brightnessField.setText(String.valueOf(brightnessComp.getValue()));
                brightnessVerifier = new SliderListenerAndVerifier(brightnessComp, brightnessField);
                brightnessField.setInputVerifier(brightnessVerifier);
                brightnessComp.addChangeListener(brightnessVerifier);
                brightnessComp.setMajorTickSpacing(k / 2);
                brightnessComp.setPaintTicks(true);
                brightnessComp.setPaintLabels(true);
                add(null, new JLabel("Brightness", 4), 0, j++, 3, 1, 0, 17, 1, 1, true);
                add(null, brightnessComp, 0, j, 3, 1, 1, 17, 100, 1, true);
                add("brightness", brightnessField, 3, j++, 1, 1, 0, 11, 1, 1, true);
            }
            supportedpropertyvalues = wiaScanner.getPropertyValues(6155);
            if(supportedpropertyvalues != null && supportedpropertyvalues.isRange())
            {
                int l = supportedpropertyvalues.getMaxValue() - supportedpropertyvalues.getMinValue();
                contrastComp = new JSlider(0, supportedpropertyvalues.getMinValue(), supportedpropertyvalues.getMaxValue(), wiaScanner.getContrast());
                contrastField = new JTextField(4);
                contrastField.setText(String.valueOf(contrastComp.getValue()));
                contrastVerifier = new SliderListenerAndVerifier(contrastComp, contrastField);
                contrastField.setInputVerifier(contrastVerifier);
                contrastComp.addChangeListener(contrastVerifier);
                contrastComp.setMajorTickSpacing(l / 2);
                contrastComp.setPaintTicks(true);
                contrastComp.setPaintLabels(true);
                add(null, new JLabel("Contrast", 4), 0, j++, 3, 1, 0, 17, 1, 1, true);
                add(null, contrastComp, 0, j, 3, 1, 1, 17, 100, 1, true);
                add("contrast", contrastField, 3, j++, 1, 1, 0, 10, 1, 1, true);
            }
        }
        add(null, jpanel, 0, j++, 4, 1, 0, 10, 1, 1, true);
    }

    public void save()
    {
        String s = (String)funitComp.getSelectedItem();
        if(s.equals("flatbed"))
            scanner.setFunctionalUnit(scanner.getFlatbedFunctionalUnit());
        else
        if(s.equals("feeder"))
        {
            scanner.setFunctionalUnit(scanner.getFeederFunctionalUnit());
            scanner.setDuplexEnabled(duplexComp.isSelected());
        } else
        {
            scanner.setFunctionalUnit(0);
        }
        if(modesComp.getItemCount() > 0)
            scanner.setMode(((Mode)modesComp.getSelectedItem()).value);
        if(resolComp.getItemCount() > 0)
            scanner.setResolution(((Integer)resolComp.getSelectedItem()).intValue());
        if(wiaScanner != null)
        {
            wiaScanner.setBrightness(Integer.parseInt(brightnessField.getText()));
            wiaScanner.setContrast(Integer.parseInt(contrastField.getText()));
        }
        saved = true;
    }

    private Component add(String s, Component component, int i, int j, int k, int l, int i1, 
            int j1, int k1, int l1, boolean flag)
    {
        component.setName(s);
        constraints.gridx = i;
        constraints.gridy = j;
        constraints.gridwidth = k;
        constraints.gridheight = l;
        constraints.fill = i1;
        constraints.anchor = j1;
        constraints.weightx = k1;
        constraints.weighty = l1;
        add(component, constraints);
        if(!flag)
            component.setEnabled(false);
        super.invalidate();
        return component;
    }

    public boolean isSaved()
    {
        return saved;
    }

    private static final long serialVersionUID = 0xbcd597cb1518a75dL;
    private static final String FU_FLATBED = "flatbed";
    private static final String FU_FEEDER = "feeder";
    private static final String FU_DEFAULT = "default";
    private Window parent;
    private Scanner scanner;
    private WIAScanner wiaScanner;
    private boolean saved;
    private Map modesMap;
    private JComboBox funitComp;
    private JCheckBox duplexComp;
    private JComboBox modesComp;
    private JComboBox resolComp;
    private JSlider brightnessComp;
    private JTextField brightnessField;
    private SliderListenerAndVerifier brightnessVerifier;
    private JSlider contrastComp;
    private JTextField contrastField;
    private SliderListenerAndVerifier contrastVerifier;
    private JButton okButton;
    private JButton cancelButton;
    protected GridBagLayout layout;
    protected GridBagConstraints constraints;
    private Action okAction;
    private Action cancelAction;






}
