/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.biometrics;

/**
 *
 * @author secuera
 */
public class BiometricError {
    private String errorCode;
    private String errorMsg;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
    
     @Override
    public String toString() {
        return "BiometricError [errorCode=" + errorCode + ", errorMsg="
                + errorMsg +  "]";
    }
}
