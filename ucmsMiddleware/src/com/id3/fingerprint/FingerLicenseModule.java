package com.id3.fingerprint;

public class FingerLicenseModule
{
  public static final int Capture = 1;
  public static final int Extract = 2;
  public static final int Enroll = 3;
  public static final int Match = 4;
  public static final int WSQ = 5;
  public static final int SlapSeg = 6;
  public static final int Card = 7;
  public static final int Barcode = 8;
  public static final int Crypto = 9;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerLicenseModule
 * JD-Core Version:    0.6.2
 */