package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.beans.piv.ContainerInfoBean;
import com.secuera.middleware.beans.piv.PIVObjectsBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import java.util.ArrayList;
import java.util.List;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class DiscoverContainers {

    private CommonUtil util = new CommonUtil();
    private SendCommand sendCard = new SendCommand();
    private GeneralAuth auth = new GeneralAuth();
    ExceptionMessages expMsg = new ExceptionMessages();
    String errorMsg;
    
    public DiscoverContainers() {
        super();
    }

    public ContainerInfoBean getContainerInfo(CardChannel channel, byte[] adminKey, byte[] diversifiedAdminKey)
            throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] res = null;

        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, adminKey);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, diversifiedAdminKey);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }

        // Command to read discovery container
        byte[] command_data = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x04,
            0x5C, 0x02, 0x3F, (byte) 0xF6};

        //prepare APDU command for authorisation
        CommandAPDU DISCOVER_CONT_APDU = new CommandAPDU(command_data);

        try {

            res = sendCard.sendCommand(channel, DISCOVER_CONT_APDU, sb);
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "getContainerInfo");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException e1) {
            throw new MiddleWareException("Card Exception.");
        }




        String returnValue = util.arrayToHex(res);

        if (returnValue.substring(2, 4).equals("82")) {
            returnValue = returnValue.substring(8);
        } else if (returnValue.substring(2, 4).equals("81")) {
            returnValue = returnValue.substring(6);
        } else {
            returnValue = returnValue.substring(4);
        }


        ContainerInfoBean containerInfo = new ContainerInfoBean();
        List myList = new ArrayList();
        String containerID = "";
        String containerName = "";
        try {
            while (returnValue.length() > 0) {
                containerID = returnValue.substring(0, 6);
                containerName = getContainerName(containerID);
                if (containerName != null && containerName.length() > 0) {
                    PIVObjectsBean PIVObjects = new PIVObjectsBean();

                    PIVObjects.setPIVObjectID(containerID);
                    PIVObjects.setPIVObjectName(containerName);
                    PIVObjects.setDataFormat("NIST");
                    PIVObjects.setContainerSize(Integer.valueOf(returnValue.substring(6, 10), 16).intValue());
                    PIVObjects.setContactRead(getAccessType(returnValue.substring(10, 12)));
                    PIVObjects.setContactUpdate(getAccessType(returnValue.substring(12, 14)));
                    PIVObjects.setContactLessRead(getAccessType(returnValue.substring(14, 16)));
                    PIVObjects.setContactLessUpdate(getAccessType(returnValue.substring(16, 18)));

                    myList.add(PIVObjects);
                }
                returnValue = returnValue.substring(18);
            }
            containerInfo.setPIVObjectsBean(myList);
        } catch (Exception e) {
            throw new MiddleWareException(e.getMessage());
        }
        return containerInfo;
    }

    private static String getContainerName(String containerID) {
        // show the list of available terminals
        String containerName = null;
        if (containerID.equalsIgnoreCase("5FC107")) {
            containerName = "Card Capabilities Container";
        } else if (containerID.equalsIgnoreCase("5FC102")) {
            containerName = "Card Holder Unique Identifier (CHUID)";
        } else if (containerID.equalsIgnoreCase("5FC105")) {
            containerName = "X.509 Certificate for PIV Authentication";
        } else if (containerID.equalsIgnoreCase("5FC103")) {
            containerName = "Card Holder Fingerprints";
        } else if (containerID.equalsIgnoreCase("5FC106")) {
            containerName = "Security Object";
        } else if (containerID.equalsIgnoreCase("5FC108")) {
            containerName = "Card Holder Facial Image";
        } else if (containerID.equalsIgnoreCase("5FC109")) {
            containerName = "Printed Information";
        } else if (containerID.equalsIgnoreCase("5FC10A")) {
            containerName = "X.509 Certificate for Digital Signature";
        } else if (containerID.equalsIgnoreCase("5FC10B")) {
            containerName = "X.509 Certificate for Key management";
        } else if (containerID.equalsIgnoreCase("5FC101")) {
            containerName = "X.509 Certificate for Card Authentication";
        } else if (containerID.equalsIgnoreCase("7E")) {
            containerName = "Discovery Object";
        } else if (containerID.equalsIgnoreCase("5FC10C")) {
            containerName = "Key History Object";
        } else if (containerID.equalsIgnoreCase("5FC10D")) {
            containerName = "Retired X.509 Certificates for Key Management 1";
        } else if (containerID.equalsIgnoreCase("5FC10E")) {
            containerName = "Retired X.509 Certificates for Key Management 2";
        } else if (containerID.equalsIgnoreCase("5FC10F")) {
            containerName = "Retired X.509 Certificates for Key Management 3";
        } else if (containerID.equalsIgnoreCase("5FC110")) {
            containerName = "Retired X.509 Certificates for Key Management 4";
        } else if (containerID.equalsIgnoreCase("5FC111")) {
            containerName = "Retired X.509 Certificates for Key Management 5";
        } else if (containerID.equalsIgnoreCase("5FC112")) {
            containerName = "Retired X.509 Certificates for Key Management 6";
        } else if (containerID.equalsIgnoreCase("5FC113")) {
            containerName = "Retired X.509 Certificates for Key Management 7";
        } else if (containerID.equalsIgnoreCase("5FC114")) {
            containerName = "Retired X.509 Certificates for Key Management 8";
        } else if (containerID.equalsIgnoreCase("5FC115")) {
            containerName = "Retired X.509 Certificates for Key Management 9";
        } else if (containerID.equalsIgnoreCase("5FC116")) {
            containerName = "Retired X.509 Certificates for Key Management 10";
        } else if (containerID.equalsIgnoreCase("5FC117")) {
            containerName = "Retired X.509 Certificates for Key Management 11";
        } else if (containerID.equalsIgnoreCase("5FC118")) {
            containerName = "Retired X.509 Certificates for Key Management 12";
        } else if (containerID.equalsIgnoreCase("5FC119")) {
            containerName = "Retired X.509 Certificates for Key Management 13";
        } else if (containerID.equalsIgnoreCase("5FC11A")) {
            containerName = "Retired X.509 Certificates for Key Management 14";
        } else if (containerID.equalsIgnoreCase("5FC11B")) {
            containerName = "Retired X.509 Certificates for Key Management 15";
        } else if (containerID.equalsIgnoreCase("5FC11C")) {
            containerName = "Retired X.509 Certificates for Key Management 16";
        } else if (containerID.equalsIgnoreCase("5FC11D")) {
            containerName = "Retired X.509 Certificates for Key Management 17";
        } else if (containerID.equalsIgnoreCase("5FC11F")) {
            containerName = "Retired X.509 Certificates for Key Management 18";
        } else if (containerID.equalsIgnoreCase("5FC11F")) {
            containerName = "Retired X.509 Certificates for Key Management 19";
        } else if (containerID.equalsIgnoreCase("5FC120")) {
            containerName = "Retired X.509 Certificates for Key Management 20";
        } else if (containerID.equalsIgnoreCase("5FC121")) {
            containerName = "Iris Image container";
        }



        return containerName;
    }

    private static String getAccessType(String accessTypeID) {
        // show the list of available terminals
        String accessType = null;

        if (accessTypeID.equalsIgnoreCase("53")) {
            accessType = "ALW";
        } else if (accessTypeID.equalsIgnoreCase("B2")) {
            accessType = "NEV";
        } else if (accessTypeID.equalsIgnoreCase("18")) {
            accessType = "PIN";
        } else if (accessTypeID.equalsIgnoreCase("3D")) {
            accessType = "PIN ^ BIO";
        } else if (accessTypeID.equalsIgnoreCase("05")) {
            accessType = "PIN ALWAYS";
        } else if (accessTypeID.equalsIgnoreCase("35")) {
            accessType = "PIV_ADM";
        } else if (accessTypeID.equalsIgnoreCase("29")) {
            accessType = "MUTUAL_AUTH";
        } else if (accessTypeID.equalsIgnoreCase("8F")) {
            accessType = "PIN & BIO";
        } else if (accessTypeID.equalsIgnoreCase("19")) {
            accessType = "PIN OR BIO ALWAYS";
        }
        return accessType;

    }
}
