/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import com.secuera.middleware.core.common.CommonUtil;

/**
 *
 * @author Satish K
 */
public class LoadUserCertificates {
    
    CommonUtil cmnUtil = new CommonUtil();
    private byte[] pivAuthentication9A;
    private byte[] digSignature9C;
    private byte[] keyManagement9D;
    private byte[] cardAuthentication9E;
    
    
    public byte[] getCardAuthentication9E() {
        return cardAuthentication9E;
    }

    public void setCardAuthentication9E(String cardAuthentication9E) {
        this.cardAuthentication9E = cmnUtil.hex1StringToByteArray(cardAuthentication9E);
    }
    
    public byte[] getDigSignature9C() {
        return digSignature9C;
    }

    public void setDigSignature9C(String digSignature9C) {
        this.digSignature9C = cmnUtil.hex1StringToByteArray(digSignature9C);
    }
    
    public byte[] getKeyManagement9D() {
        return keyManagement9D;
    }

    public void setKeyManagement9D(String keyManagement9D) {
        this.keyManagement9D = cmnUtil.hex1StringToByteArray(keyManagement9D);
    }

    
    public byte[] getPivAuthentication9A() {
        return pivAuthentication9A;
    }
    

    public void setPivAuthentication9A(String pivAuthentication9A) {
        this.pivAuthentication9A = cmnUtil.hex1StringToByteArray(pivAuthentication9A);
    }
    
}
