package com.secuera.ucms.middleware.applet;

import java.applet.Applet;
import java.awt.GridLayout;
import java.beans.Beans;
import com.topaz.sigplus.SigPlus;
import com.topaz.sigplus.SigPlusEvent0;
import com.topaz.sigplus.SigPlusListener;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.commons.io.FileUtils; 
import org.apache.commons.io.IOUtils; 

/**
 *
 * @author Nilay Singh
 */
public class SigPlusApplet extends Applet {

    /**
     *
     */
    SigPlus sigObj = null;
    protected static String arch;
    protected static String os;
    protected static String vendor;
    protected static String userHome;
    public static final File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
    String mySigString;
    private URL documentBase = null;
    
    static {

        arch = System.getProperty("os.arch").toLowerCase();
        os = System.getProperty("os.name").toLowerCase();
        vendor = System.getProperty("java.vm.vendor").toLowerCase();
        userHome = System.getProperty("user.home");
        String str = null;
        byte[] arrayOfByte;
        File localFile = null;

        if (os.indexOf("windows") >= 0) {
            if (arch.indexOf("64") >= 0) {
                str = System.mapLibraryName("SigUsb_64");
                localFile = new File(TMP_DIR, str);
            } else {
                str = System.mapLibraryName("SigUsb_32");
                localFile = new File(TMP_DIR, str);
            }
        }
        System.out.println("Machine Architecture :  " + arch);
        System.out.println("Machine OS :  " + os);
        System.out.println("Machine Vendor :  " + vendor);
        System.out.println("Machine User Home :  " + userHome);
        System.out.println("Library Path :  " + str);
        try {

            //File localFile = new File(TMP_DIR + "/32bit/", str);

            
            System.out.println("File Object >>>>>>>>>>   :  " + localFile);


            if (localFile != null) {
                System.out.println("Is DLL File Exists? >>>>>>>>>>   :  " + localFile.exists());
            }
            System.out.println(localFile.getAbsolutePath() + "  >>>>>>>.   new path ...");

            if (!localFile.exists()) {
                
                InputStream is = SigPlusApplet.class.getClassLoader().getResourceAsStream(str);

                System.out.println("Local Object :  " + is);


                 byte[] bytes = IOUtils.toByteArray(is);
                 System.out.println(">>>>>>>>> " + bytes.length);
                 FileUtils.writeByteArrayToFile(localFile, bytes);
                  
                System.out.println("Before Loading File >>>>>>>>>>   :  ");

                System.load(localFile.getAbsolutePath());

                System.out.println(localFile.getAbsolutePath() + " \nloaded...");
            } else {
                System.out.println("Before Loading File >>>>>>>>>>   :  ");

                System.load(localFile.getAbsolutePath());

                System.out.println(localFile.getAbsolutePath() + " \nloaded...");
            }

        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Error:  " + e.getMessage());
            try {
                Object localObject = new File(TMP_DIR, str);
                System.load(((File) localObject).getAbsolutePath());
                System.out.println(((File) localObject).getAbsolutePath() + " \nloaded...");
            } catch (Exception ex) {
                // ex.printStackTrace();
                System.err.println(str + " not found...");
            }
        }

    }

    public void init() {
        // TODO Auto-generated method stub
        super.init();
    }

    public void start() {
        // TODO Auto-generated method stub
        super.start();
        try {
            ClassLoader cl = (com.topaz.sigplus.SigPlus.class).getClassLoader();
            sigObj = (SigPlus) Beans.instantiate(cl, "com.topaz.sigplus.SigPlus");

            boolean b = sigObj.getTabletComTest();
            
            System.out.print("\n*****************Tablet Connected >>>>>>>>>>>>  " + b);
            setLayout(new GridLayout(1, 1));
            add(sigObj);


            sigObj.addSigPlusListener(new SigPlusListener() {
                public void handleTabletTimerEvent(SigPlusEvent0 evt) {
                }

                public void handleNewTabletData(SigPlusEvent0 evt) {
                }

                public void handleKeyPadData(SigPlusEvent0 evt) {
                }
            });


            setSize(500, 100);
            show();

            System.out.print("After Loading the applet......................");
            sigObj.setTabletModel("SignatureGemLCD1X5"); //SPECIFY YOUR TABLETMODEL HERE    
            sigObj.setTabletComPort("HID1"); //SPECIFY YOU CONNECTION TYPE HERE
            //SEE TableModel_TabletComPort_options.txt FOR DETAILS ON SETTABLETMODEL() and SETTABLETCOMPORT() OPTIONS  

            sigObj.setTabletState(1);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

    }

    public void saveSign() {
        sigObj.autoKeyStart();
        sigObj.setAutoKeyData("Sample Encryption Data");
        sigObj.autoKeyFinish();
        sigObj.setEncryptionMode(2);
        sigObj.setSigCompressionMode(1);
        mySigString = sigObj.getSigString();
       try {
           documentBase = getDocumentBase();
           URL uploadURL = new URL(documentBase, "/secuera/capture");
                
          System.out.println("Server url for upload >>>>>>>> " + uploadURL);
          HttpURLConnection connection = (HttpURLConnection) uploadURL.openConnection();
          connection.setRequestMethod("POST");
          connection.setDoOutput(true);
          connection.setUseCaches(false);
          connection.setDefaultUseCaches(false);
          connection.setRequestProperty("content-type", "text/plain; charset=utf-8");
          connection.setRequestProperty("content-length", String.valueOf(mySigString.length()));
          OutputStream out = connection.getOutputStream();
          out.write(mySigString.getBytes());
          out.close();
          InputStream in = connection.getInputStream();
          int c;
          while ((c = in.read()) != -1)
             System.err.write(c);
           in.close();
        System.out.print("\n Sign String >>>>>>>>>  " + mySigString);
        sigObj.clearTablet(); //clear signature
        //reset SigPlus
        sigObj.setSigCompressionMode(0);
        sigObj.setEncryptionMode(0);
        sigObj.setKeyString("0000000000000000");
       } catch (Throwable exception) {
           
          exception.printStackTrace();
           
        } 
    }
    
     public void clearSign() {
       sigObj.clearTablet();

    }
    
    
    public void loadSign() {
        if(mySigString != "")
        {
           sigObj.autoKeyStart();
           sigObj.setAutoKeyData("Sample Encryption Data");
           sigObj.autoKeyFinish();
           sigObj.setEncryptionMode(2);
           sigObj.setSigCompressionMode(1);
           sigObj.setSigString(mySigString);
        if (sigObj.numberOfTabletPoints() > 0)
             {
            System.out.println("Signature returned successfully");
             }
         }
         else
         {
            System.out.println("Signature not returned successfully!");
         }
	
    }

    public void destroy() {
        if (sigObj != null) {
            sigObj.setTabletState(0);
            remove(sigObj);
        }
        sigObj = null;
        System.gc();
    }
}
