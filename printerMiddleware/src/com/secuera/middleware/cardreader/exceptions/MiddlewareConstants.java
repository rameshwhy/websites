/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.exceptions;

/**
 *
 * @author com_user
 */
public class MiddlewareConstants {
    // certficate

    public static String digitalSignature = "digitalSignature";
    public static String nonRepudiation = "nonRepudiation";
    public static String keyEncipherment = "keyEncipherment";
    public static String dataEncipherment = "dataEncipherment";
    public static String keyAgreement = "keyAgreement";
    public static String keyCertSign = "keyCertSign";
    public static String cRLSign = "cRLSign";
    public static String encipherOnly = "encipherOnly";
    public static String decipherOnly = "decipherOnly";
    //finger print status messages
    public static String CaptureStopped = "Capture stopped.";
    public static String PlaceFinger = "Place finger";
    public static String RemoveFinger = "Remove finger";
    public static String RollFinger = "Roll your finger";
    public static String FingerPlaced = "Finger placed";
    public static String FingerRemoved = "Finger removed";
    public static String FingerDownBorder = "Finger significantly displaced to the down border";
    public static String FingerTopBorder = "Finger goes out on the top border";
    public static String FingerRightBorder = "Finger goes out on the right border";
    public static String FingerLeftBorder = "Finger goes out on the left border";
    public static String PressFingerHarder = "Press finger harder";
    public static String SwipeTooFast = "The finger swipe was too fast";
    public static String SwipeTooSlow = "The finger swipe was too slow";
    public static String SwipeTooSkewed = "The finger swipe was too skewed";
    public static String InapSpeed = "The finger was swiped with inapropriate speed";
    public static String ImageSmall = "The captured image is too small";
    public static String PoorQuality = "The captured image is of poor quality";
    public static String FingerLifted = "A part of the finger is lifted";
    public static String RollingFast = "Rolling speed is too fast";
    public static String DirtySensor = "The sensor surface is dirty, or more than one finger is detected";
    public static String FingerShifted = "The rolling finger is shifted";
    public static String RollingTooShort = "Rolling time is too short";
    public static String FewerFinger = "Fewer fingers detected";
    public static String TooManyFinger = "Too many fingers detected";
    public static String WrongHand = "Wrong hand";
    //certtificate extended key usage
    public static String AnyPurpose = "Any Purpose (2.5.29.37.0)";
    public static String ClientAuthentication = "Client Authentication (1.3.6.1.5.5.7.3.2)";
    public static String SecureEmail = "Secure Email (1.3.6.1.5.5.7.3.4)";
    public static String SmartCardLogon = "Smart Card Logon (1.3.6.1.4.1.311.20.2.2)";
    public static String PIVContentSigning = "PIV Content Signing(2.16.840.1.101.3.6.7)";
    
    
    
    
    //constants for discovery container
    public static String ID5FC107 = "Card Capabilities Container";
    public static String ID5FC102 = "Card Holder Unique Identifier (CHUID)";
    public static String ID5FC105 = "X.509 Certificate for PIV Authentication";
    public static String ID5FC103 = "Card Holder Fingerprints";
    public static String ID5FC106 = "Security Object";
    public static String ID5FC108 = "Card Holder Facial Image";
    public static String ID5FC109 = "Printed Information";
    public static String ID5FC10A = "X.509 Certificate for Digital Signature";
    public static String ID5FC10B = "X.509 Certificate for Key management";
    public static String ID5FC101 = "X.509 Certificate for Card Authentication";
    public static String ID7E = "Discovery Object";
    public static String ID5FC10C = "Key History Object";
    public static String ID5FC10D = "Retired X.509 Certificates for Key Management 1";
    public static String ID5FC10E = "Retired X.509 Certificates for Key Management 2";
    public static String ID5FC10F = "Retired X.509 Certificates for Key Management 3";
    public static String ID5FC110 = "Retired X.509 Certificates for Key Management 4";
    public static String ID5FC111 = "Retired X.509 Certificates for Key Management 5";
    public static String ID5FC112 = "Retired X.509 Certificates for Key Management 6";
    public static String ID5FC113 = "Retired X.509 Certificates for Key Management 7";
    public static String ID5FC114 = "Retired X.509 Certificates for Key Management 8";
    public static String ID5FC115 = "Retired X.509 Certificates for Key Management 9";
    public static String ID5FC116 = "Retired X.509 Certificates for Key Management 10";
    public static String ID5FC117 = "Retired X.509 Certificates for Key Management 11";
    public static String ID5FC118 = "Retired X.509 Certificates for Key Management 12";
    public static String ID5FC119 = "Retired X.509 Certificates for Key Management 13";
    public static String ID5FC11A = "Retired X.509 Certificates for Key Management 14";
    public static String ID5FC11B = "Retired X.509 Certificates for Key Management 15";
    public static String ID5FC11C = "Retired X.509 Certificates for Key Management 16";
    public static String ID5FC11D = "Retired X.509 Certificates for Key Management 17";
    public static String ID5FC11E = "Retired X.509 Certificates for Key Management 18";
    public static String ID5FC11F = "Retired X.509 Certificates for Key Management 19";
    public static String ID5FC120 = "Retired X.509 Certificates for Key Management 20";
    public static String ID5FC121 = "Iris Image container";
}
