/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.beans;

import java.math.BigInteger;

/**
 *
 * @author Harpreet Singh
 */
public class CertificateBean {

    private String version;
    private String serialNumber;
    private String signatureAlgorithm;
    private String issuer;
    private String validFrom;
    private String validTo;
    private String subject;
    private String publicKey;
    private String keyUsage;
    private String ExtnkeyUsage;
    /*Issuer information */
    private String issuerCountry;
    private String issuerCommonName;
    private String issuerOrganizationUnit;
    private String issuerOrganizationName;
    private BigInteger BGserialNumber;
    /*Subject Information  */
    private String subjectCommonName;
    private String subjectOrganizationUnit;
    private String subjectOrganizationName;
    private String subjectCity;
    private String subjectState;
    private String subjectCountry;
    private String subjectEmail;

    public String getSubjectOrganizationUnit() {
        return subjectOrganizationUnit;
    }

    public void setSubjectOrganizationUnit(String subjectOrganizationUnit) {
        this.subjectOrganizationUnit = subjectOrganizationUnit;
    }

    public String getSubjectCity() {
        return subjectCity;
    }

    public void setSubjectCity(String subjectCity) {
        this.subjectCity = subjectCity;
    }

    public String getSubjectState() {
        return subjectState;
    }

    public void setSubjectState(String subjectState) {
        this.subjectState = subjectState;
    }

    public String getSubjectEmail() {
        return subjectEmail;
    }

    public void setSubjectEmail(String subjectEmail) {
        this.subjectEmail = subjectEmail;
    }

    public String getIssuerCountry() {
        return issuerCountry;
    }

    public void setIssuerCountry(String issuerCountry) {
        this.issuerCountry = issuerCountry;
    }

    public String getIssuerCommonName() {
        return issuerCommonName;
    }

    public void setIssuerCommonName(String issuerCommonName) {
        this.issuerCommonName = issuerCommonName;
    }

    public String getIssuerOrganizationName() {
        return issuerOrganizationName;
    }

    public void setIssuerOrganizationName(String issuerOrganizationName) {
        this.issuerOrganizationName = issuerOrganizationName;
    }

    public String getSubjectCountry() {
        return subjectCountry;
    }

    public void setSubjectCountry(String subjectCountry) {
        this.subjectCountry = subjectCountry;
    }

    public String getSubjectCommonName() {
        return subjectCommonName;
    }

    public void setSubjectCommonName(String subjectCommonName) {
        this.subjectCommonName = subjectCommonName;
    }

    public String getSubjectOrganizationName() {
        return subjectOrganizationName;
    }

    public void setSubjectOrganizationName(String subjectOrganizationName) {
        this.subjectOrganizationName = subjectOrganizationName;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getKeyUsage() {
        return keyUsage;
    }

    public void setKeyUsage(String keyUsage) {
        this.keyUsage = keyUsage;
    }

    public String getExtnkeyUsage() {
        return ExtnkeyUsage;
    }

    public void setExtnkeyUsage(String ExtnkeyUsage) {
        this.ExtnkeyUsage = ExtnkeyUsage;
    }

    public String getIssuerOrganizationUnit() {
        return issuerOrganizationUnit;
    }

    public void setIssuerOrganizationUnit(String issuerOrganizationUnit) {
        this.issuerOrganizationUnit = issuerOrganizationUnit;
    }

    public BigInteger getBGserialNumber() {
        return BGserialNumber;
    }

    public void setBGserialNumber(BigInteger BGserialNumber) {
        this.BGserialNumber = BGserialNumber;
    }

    @Override
    public String toString() {
        return "CertificateBean{" + "version=" + version + ", serialNumber=" + serialNumber + 
                ", signatureAlgorithm=" + signatureAlgorithm + ", issuer=" + issuer + ", validFrom=" + 
                validFrom + ", validTo=" + validTo + ", subject=" + subject + ", publicKey=" + publicKey 
                + ", keyUsage=" + keyUsage + ", ExtnkeyUsage=" + ExtnkeyUsage + ", issuerCountry=" + 
                issuerCountry + ", issuerCommonName=" + issuerCommonName + ", issuerOrganizationUnit=" + 
                issuerOrganizationUnit + ", issuerOrganizationName=" + issuerOrganizationName + 
                ", BGserialNumber=" + BGserialNumber + ", subjectCommonName=" + 
                subjectCommonName + ", subjectOrganizationUnit=" + subjectOrganizationUnit + 
                ", subjectOrganizationName=" + subjectOrganizationName + ", subjectCity=" + 
                subjectCity + ", subjectState=" + subjectState + ", subjectCountry=" + 
                subjectCountry + ", subjectEmail=" + subjectEmail + '}';
    }
}
