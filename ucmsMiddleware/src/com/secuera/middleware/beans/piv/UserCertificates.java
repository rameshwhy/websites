/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.io.Serializable;

/**
 *
 * @author Satish K
 */
public class UserCertificates implements Serializable {
    private CertificateBean pivAuthentication9A;
    private CertificateBean digSignature9C;
    private CertificateBean keyManagement9D;
    private CertificateBean cardAuthentication9E;

    
    public CertificateBean getCardAuthentication9E() {
        return cardAuthentication9E;
    }

    public void setCardAuthentication9E(CertificateBean cardAuthentication9E) {
        this.cardAuthentication9E = cardAuthentication9E;
    }

    public CertificateBean getDigSignature9C() {
        return digSignature9C;
    }

    public void setDigSignature9C(CertificateBean digSignature9C) {
        this.digSignature9C = digSignature9C;
    }

    public CertificateBean getKeyManagement9D() {
        return keyManagement9D;
    }

    public void setKeyManagement9D(CertificateBean keyManagement9D) {
        this.keyManagement9D = keyManagement9D;
    }

    public CertificateBean getPivAuthentication9A() {
        return pivAuthentication9A;
    }

    public void setPivAuthentication9A(CertificateBean pivAuthentication9A) {
        this.pivAuthentication9A = pivAuthentication9A;
    }
    
    
}
