/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.beans.piv.CSRBean;
import com.secuera.middleware.beans.piv.ContentCertificateBean;
import com.secuera.middleware.beans.piv.UserContentCSR;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.security.PrivateKey;
import java.security.PublicKey;
import javax.smartcardio.CardChannel;

/**
 *
 * @author Satish K
 */
public class GenContentCertificate {

    public ContentCertificateBean genContentCert(CardChannel channel, String cert_type,
            byte[] key_type, String str_cn, String str_ou,
            String str_org, String str_city, String str_state,
            String str_country, String str_email, String cert_template_oid, int majorVer, int minorVer,
            String caType, String inURL, PublicKey pubkey, PrivateKey privkey,String algoType) throws Exception {



        ContentCertificateBean contentCertificate = new ContentCertificateBean();
        //generate publickey / private key if not supplied
        if (pubkey == null || pubkey.toString().length() == 0) {
            GenKeyPair keyPair = new GenKeyPair();
            contentCertificate = keyPair.keyPair(algoType);
        } else {
            contentCertificate.setPublicKey(pubkey);
            contentCertificate.setPrivateKey(privkey);

        }

        String certCSR;
        byte[] certficate = null;
        //generate CSR
        GenContentCSR generateCSR = new GenContentCSR();
        certCSR = generateCSR.getCSR(str_cn, str_ou, str_org,
                str_city, str_state, str_country, str_email, cert_template_oid, majorVer, minorVer, contentCertificate.getPublicKey(), contentCertificate.getPrivateKey(),algoType);



        //get microsoft certificate
        if ("MS".equals(caType)) {
            certficate = getMScertificate(certCSR, inURL);
        }

        CommonUtil util = new CommonUtil();
        System.out.println(util.arrayToHex(certficate));
        contentCertificate.setStrContentCertificateHex(util.arrayToHex(certficate));
        contentCertificate.setContentCertificate(certficate);
        return contentCertificate;




    }

    private byte[] getMScertificate(String certCSR, String inURL) throws MiddleWareException {

        byte[] certficate = null;
        //connect to microsoft
        try {
            MicrosoftCertServer msCASrv = new MicrosoftCertServer();


            // Download the new certificate
            String approvalID = msCASrv.HttpGetApprovalID(certCSR, inURL+"certfnsh.asp");
            // Create the Certificate in .der format and get it in .pem format
            certficate = msCASrv.HttpGetUserCert(approvalID, inURL);


        } catch (Exception ex) {
            // TODO Auto-generated catch block
            throw new MiddleWareException(ex.getMessage());
        }


        return certficate;

    }


    public UserContentCSR generateContentCSR(CardChannel channel, CSRBean certBean) throws MiddleWareException {
        UserContentCSR userContentCSR = new UserContentCSR();
        PublicKey pubicKey;
        PrivateKey privateKey;

        pubicKey = certBean.getPublicKey();
        privateKey = certBean.getPrivateKey();


        ContentCertificateBean contentCertificate = new ContentCertificateBean();
        //generate publickey / private key if not supplied
        if (pubicKey == null || pubicKey.toString().length() == 0) {
            GenKeyPair keyPair = new GenKeyPair();
            
            contentCertificate = keyPair.keyPair(certBean.getKeyAlgorithmType());
            
            pubicKey = contentCertificate.getPublicKey();
            privateKey = contentCertificate.getPrivateKey();
        }


        String certCSR;
        //generate CSR
        GenContentCSR generateCSR = new GenContentCSR();
        certCSR = generateCSR.getCSR(certBean.getCommonName(), certBean.getOrganisationalunit(),certBean.getOrganisation(),
                certBean.getCity(), certBean.getState(),certBean.getCountry(), certBean.getEmailId(),certBean.getCert_template_oid(),
                certBean.getMajorVer(),certBean.getMinorVer(), pubicKey,
                privateKey,certBean.getKeyAlgorithmType());


        userContentCSR.setContentCSR(certCSR);
        userContentCSR.setPublicKey(pubicKey);
        userContentCSR.setPrivateKey(privateKey);

        return userContentCSR;


    }

    public byte[] generateContentCertificate(String certCSR, String inURL,String caType) throws MiddleWareException {
        byte[] certficate=null;

        //get microsoft certificate
        if ("MS".equals(caType)) {
            certficate = getMScertificate(certCSR, inURL);
        }

        //Return CSR
        return certficate;


    }


}
