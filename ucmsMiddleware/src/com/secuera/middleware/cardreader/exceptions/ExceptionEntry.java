/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.exceptions;

/**
 *
 * @author admin
 */
public class ExceptionEntry {
	  private static final long serialVersionUID = -9060808123950774739L;
	  private int errorCode;
	  private int severity;
	  private String errorMsg;
	  private String errorLocation;
	  private String errorDigest;

	  public ExceptionEntry()
	  {}
	  public ExceptionEntry(int errNum, int sev, String errLoc, String errMsg, String theDigest)
	  {}
	  public ExceptionEntry(int errNum, int sev, String errLoc, String errMsg, String theDigest, int adjustmentIn)
	  {}
	  
	  public void setErrorCode(int value)
	  {
		  this.errorCode = value;
	  }
	  
	  public int getErrorCode()
	  {
		  return this.errorCode;
	  }
	  
	  public void setSeverity(int value)
	  {
		  this.severity = value;
	  }
	  
	  public int getSeverity()
	  {
		  return this.severity;
	  }

	  public void setErrorMsg(String value)
	  {
		  this.errorMsg = value;
	  }
	  
	  public String getErrorMsg()
	  {
		  return this.errorMsg;
	  }

	  public void setErrorLocation(String value)
	  {
		  this.errorLocation = value;
	  }
	  
	  public String getErrorLocation()
	  {
		  return this.errorLocation;
	  }
	  
	  public void setErrorDigest(String value)
	  {
		  this.errorDigest = value;
	  }
	  
	  public String getErrorDigest()
	  {
		  return this.errorDigest;
	  }

}
