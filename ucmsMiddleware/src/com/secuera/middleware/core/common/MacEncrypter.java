/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.common;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.*;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
/**
 *
 * @author admin
 */

public class MacEncrypter {
	Cipher ecipher;

	String transformation = "DESede/CBC/NoPadding";

	public MacEncrypter(byte[] key_p, byte[] IV) throws MiddleWareException {
		try {

			// Create encrypt decrypt class			
			SecretKey keySpec = new SecretKeySpec(key_p, "DESede");
			IvParameterSpec iv = new IvParameterSpec(IV);
			
			//get a Cipher object that implements the specified transformation
			ecipher = Cipher.getInstance(transformation);
			
			//Initializes this cipher with the public key from the given certificate
			ecipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);

		} catch (javax.crypto.NoSuchPaddingException e) {
			throw new MiddleWareException(e.getMessage());
		} catch (java.security.NoSuchAlgorithmException e) {
			throw new MiddleWareException(e.getMessage());
		} catch (java.security.InvalidKeyException e) {
			throw new MiddleWareException(e.getMessage());
		} catch (InvalidAlgorithmParameterException e) {
			throw new MiddleWareException(e.getMessage());	
		}
	}

	public byte[] encrypt(byte[] encr) throws MiddleWareException {
		byte[] enc= null;
		try {
			enc = ecipher.doFinal(encr);
		} catch (IllegalBlockSizeException e){
			throw new MiddleWareException(e.getMessage());
		}catch (BadPaddingException e){
		
			throw new MiddleWareException(e.getMessage());
		}
		// Encode bytes to base64 to get a string
		// return new sun.misc.BASE64Encoder().encode(enc);
		return enc;

	}

}
