package com.id3.fingerprint;

public class FingerMinutiaType
{
  public static final int Unknown = 0;
  public static final int RidgeEnding = 1;
  public static final int RidgeBifurcation = 2;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerMinutiaType
 * JD-Core Version:    0.6.2
 */