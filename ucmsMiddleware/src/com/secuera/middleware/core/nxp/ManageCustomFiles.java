package com.secuera.middleware.core.nxp;

import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessagesNXP;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.SendCommand;

public class ManageCustomFiles {
	CommonUtil util = new CommonUtil();
	 SendCommand sendCard = new SendCommand();
	 StringBuffer sb = new StringBuffer();
	 ExceptionMessages expMsg = new ExceptionMessages();
	
	public boolean insertDataToCustomFile(CardChannel channel, byte[] data_tobe_inserted) throws MiddleWareException
	{
		// ex:  String insert_data = "00D60000184c383938393032433c333639303830363139343036323336";
		 String errorMsg;
		 boolean rtnvalue = false;	 
		 
		 int data_len  = data_tobe_inserted.length;
		 String data_len_hexstr =  Integer.toString(data_len, 16);
		 
		 
		 byte[] command_header = {0x00, (byte) 0xD6, 0x00, 0x00};
		 byte[] len_command_data = util.hex1StringToByteArray(data_len_hexstr);
		 byte[] data = data_tobe_inserted;
		
		 byte[] byte_insert_data = util.combine_data(command_header, len_command_data);
		 byte_insert_data = util.combine_data(byte_insert_data, data);
		
		// Inserting data on to container file
     	CommandAPDU binary_data_update = new CommandAPDU(byte_insert_data);
     	try {
     		 sendCard.sendCommand(channel, binary_data_update, sb);
     		  if (!sb.toString().equals("9000")) {
                  errorMsg = expMsg.GetErrorMsg(sb.toString(), "Insert_data");
                  throw new MiddleWareException(errorMsg);
              } else {
            	  rtnvalue = true;
              }
     		
     		 return rtnvalue;
     		
     	   	} catch (CardException e) {
     	   	throw new MiddleWareException(e.getMessage());
     	   	}
   
	}
	
	////////////////////////////////////////////////////////// Select EF File////////////////////////////////////////
	public boolean selectEFFile(CardChannel channel, String hex_file_id) throws MiddleWareException
	{
		 String errorMsg;
		 boolean rtnvalue = false;	
		// ex: String select_DG1_file =  "00A4020C020102"; //Select file   00A4020002010200 select file which returns fcp 
		//p1-02 && p2-0C -- refer ISO7816-4 page number 48 : table 46 and 47
    	
    	byte[] cmnd_header = {0x00, (byte) 0xA4, 0x02, 0x0C};
    	byte[] Lc = {0x02};
    	byte[] file_id = util.hex1StringToByteArray(hex_file_id);
    	
    	byte[] select_file = util.combine_data(cmnd_header, Lc);
    	select_file = util.combine_data(select_file, file_id);
  
    	CommandAPDU cmd_file_create = new CommandAPDU(select_file);
    	try {
    		 sendCard.sendCommand(channel, cmd_file_create, sb);
    		  if (!sb.toString().equals("9000")) {
                 errorMsg = expMsg.GetErrorMsg(sb.toString(), "selectFile");
                 throw new MiddleWareException(errorMsg);
             } else {
           	  rtnvalue = true;
             }
    		 return rtnvalue;
    		
    	   	} catch (CardException e) {
    	   	throw new MiddleWareException(e.getMessage());
    	   	}
     	
	}
	
	
public boolean deleteFile(CardChannel channel, String hex_file_id) throws MiddleWareException
{
	// ex :: String  delete_file = "00E4020C020101"; // delete
	 String errorMsg;
	 boolean rtnvalue = false;	
 	byte[] cmnd_header = {0x00, (byte) 0xE4, 0x02, 0x0C};
 	byte[] Lc = {0x02};
 	byte[] file_id = util.hex1StringToByteArray(hex_file_id);
 	
 	byte[] delete_file = util.combine_data(cmnd_header, Lc);
 	delete_file = util.combine_data(delete_file, file_id);
	 
	
	CommandAPDU cmd_file_create = new CommandAPDU(delete_file);
	try {
		 sendCard.sendCommand(channel, cmd_file_create, sb);
		  if (!sb.toString().equals("9000")) {
             errorMsg = expMsg.GetErrorMsg(sb.toString(), "deleteFile");
             throw new MiddleWareException(errorMsg);
         } else {
       	  rtnvalue = true;
         }
		 return rtnvalue;
		
	   	} catch (CardException e) {
	   	throw new MiddleWareException(e.getMessage());
	   	}
	
	}

public byte[] readBinaryData(CardChannel channel) throws MiddleWareException
{
	// This method changes based on the access rules as of now we used 00 as access rules which means always we have access
	// Business should decide what access rules should we use
	// in order to read binary data we should select file by invoking selectFile method
	
	   //read binary data	String  read_binary = "00B0000000";  //00  means 256 bytes: after actual data all '0' are appended till the length becomes 256bytes
	 String errorMsg;
	 String  read_binary = "00B0000000";
	   byte[] byte_read_data = util.hex1StringToByteArray(read_binary);
	   
		
    	CommandAPDU binary_data_read = new CommandAPDU(byte_read_data);
   	byte[] response_data_of_read = null;
    	try {
    		response_data_of_read = sendCard.sendCommand(channel, binary_data_read, sb);
    		 if (!sb.toString().equals("9000")) {
                 errorMsg = expMsg.GetErrorMsg(sb.toString(), "deleteFile");
                 throw new MiddleWareException(errorMsg);
             } else {
           	  
            	 return response_data_of_read;
             }
    		 
    		
    	   	} catch (CardException e) {
    	   	throw new MiddleWareException(e.getMessage());
    	   	}
 
}
	
	
}
