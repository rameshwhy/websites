package com.id3.fingerprint;

public class FingerCryptoMode
{
  public static final int SharedSecret = 0;
  public static final int PublicKey = 1;
  public static final int PublicKey_TPM = 2;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCryptoMode
 * JD-Core Version:    0.6.2
 */