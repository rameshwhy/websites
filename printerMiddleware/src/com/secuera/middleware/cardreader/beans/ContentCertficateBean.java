/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.beans;

import java.security.PrivateKey;
import java.security.PublicKey;


/**
 *
 * @author SEAdmin
 */
public class ContentCertficateBean {
    private PublicKey publicKey;
    private PrivateKey privateKey;
    byte[] contentCertificate;
    
    public byte[] getContentCertificate() {
        return contentCertificate;
    }

    public void setContentCertificate(byte[] contentCertificate) {
        this.contentCertificate = contentCertificate;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public String toString() {
        return "ContentCertficateBean{" + "publicKey=" + publicKey + ", privateKey=" + privateKey + ", contentCertificate=" + contentCertificate + '}';
    }
    
}
