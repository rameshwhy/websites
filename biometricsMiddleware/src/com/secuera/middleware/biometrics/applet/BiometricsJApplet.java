package com.secuera.middleware.biometrics.applet;

import com.id3.fingerprint.FingerCapture;
import com.id3.fingerprint.FingerCaptureDevice;
import com.id3.fingerprint.FingerCaptureListener;
import com.id3.fingerprint.FingerEnroll;
import com.id3.fingerprint.FingerEnrollStatus;
import com.id3.fingerprint.FingerException;
import com.id3.fingerprint.FingerImage;
import com.id3.fingerprint.FingerImageRecord;
import com.id3.fingerprint.FingerImageRecordFormat;
import com.id3.fingerprint.FingerImpression;
import com.id3.fingerprint.FingerLicense;
import com.id3.fingerprint.FingerTemplate;
import com.id3.fingerprint.FingerTemplateRecord; 
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException; 
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.piv.Id3BioCapture;
import com.secuera.middleware.wrapper.piv.MiddleWareServerCardreader;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Nilay Singh
 */
public class BiometricsJApplet extends javax.swing.JApplet implements FingerCaptureListener {

    /**
     * *****************************************************************************************************************************
     * Static Variables
     * *****************************************************************************************************************************
     */
    private static BiometricsJAppletHelper appletHelper = BiometricsJAppletHelper.getInstance();
    private static Logger logger = null;
    java.util.List<String> terminals = null;
    BiometricError biometricError = null;
    BiometricResponseBean biometricResponse = null;
    java.util.List<String> deviceList = null;
    java.util.List<FingerCaptureDevice> deviceHandleList = new ArrayList<>();
    protected static FingerCaptureDevice selectedDeviceHandle = null;
    private String defaultValue = "------------Select-------------";
    byte[] sub_imp_type = {0x00};
    int sub_bio; //Finger Type.
    int i_capture = 0;
    int image_count = 3;
    int image_quality = 50;
    int minImageCount=0;
    boolean template_failed = false;
    //MiddleWareServerCardreader mws = null;
    private BufferedImage bufferedImage = null;
    private String deviceName = null;
    private FingerCaptureDevice[] devices;
    private FingerCapture capture;
    private FingerEnroll fgEnroll;
    public int imageCount;
    private int fgType;
    int lpinkyCount = 0; 	
    int lringCount = 0;		
    int lmiddleCount = 0;
    int lindexCount = 0;
    int lthumbCount = 0;
    int rpinkyCount = 0;
    int rringCount = 0;
    int rmiddleCount = 0;	
    int rindexCount = 0;	
    int rthumbCount = 0;
    //For chkng Image quality of each finger
    int lpinkyImgQauality = 0; 	
    int lringImgQauality = 0;		
    int lmiddleImgQauality = 0;
    int lindexImgQauality = 0;
    int lthumbImgQauality = 0;
    int rpinkyImgQauality = 0;
    int rringImgQauality = 0;
    int rmiddleImgQauality = 0;	
    int rindexImgQauality = 0;	
    int rthumbImgQauality = 0;
	String methodName = null;
    
    HashMap<Integer, String> fingerImageMap = new HashMap<Integer, String>();
       
    String[][] fingerArray = new String[2][11];
    
    private String lpinky = ""; 	
    private String lring =  "";		
    private String lmiddle =  "";	
    private String lindex =  "";	
    private String lthumb =  "";	
    private String rpinky =  "";	
    private String rring = "";	
    private String rmiddle =  "";	
    private String rindex =  "";	
    private String rthumb =  "";
    private String lpinkyT = ""; 	
    private String lringT =  "";		
    private String lmiddleT =  "";	
    private String lindexT =  "";	
    private String lthumbT =  "";	
    private String rpinkyT =  "";	
    private String rringT = "";	
    private String rmiddleT =  "";	
    private String rindexT =  "";	
    private String rthumbT =  "";
    
    
    public BiometricsJApplet() {
        
        trace("====================== INSIDE CONSTRUCTOR =============================");
        
        if(capture != null){
            try {
                capture.dispose();
               
            } catch (Exception ex) {
                Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(fgEnroll != null){
            fgEnroll.dispose();
        }
        appletHelper.initialiseTheme();
        deviceList = new ArrayList<>();
        initComponents();
        initVariables();
        initComponentsCustom();
        
        
        

    }

    public void trace(String paramString) {
        System.out.println("INFO: " + paramString);
    }

    
    @Override
  public void start() {
         //deviceList.add(defaultValue);

         //populateDevicesList();
         
  }
    
   @Override
    public void init() {
         
        
         lpinky = this.getParameter("lpinky");
	 lring = this.getParameter("lring");
	 lmiddle = this.getParameter("lmiddle");
	 lindex = this.getParameter("lindex");
	 lthumb = this.getParameter("lthumb");
	 rpinky = this.getParameter("rpinky");
	 rring = this.getParameter("rring");
	 rmiddle = this.getParameter("rmiddle");
	 rindex = this.getParameter("rindex");
	 rthumb = this.getParameter("rthumb");
         
         lpinkyT = this.getParameter("lpinkyT");
	 lringT = this.getParameter("lringT");
	 lmiddleT = this.getParameter("lmiddleT");
	 lindexT = this.getParameter("lindexT");
	 lthumbT = this.getParameter("lthumbT");
	 rpinkyT = this.getParameter("rpinkyT");
	 rringT = this.getParameter("rringT");
	 rmiddleT = this.getParameter("rmiddleT");
	 rindexT = this.getParameter("rindexT");
	 rthumbT = this.getParameter("rthumbT");
         
         if(this.getParameter("imageQuality") != null){
             image_quality = new Integer(this.getParameter("imageQuality"));
         }
         if(this.getParameter("imageCount") != null){
           image_count = new Integer(this.getParameter("imageCount"));
           
         }
         if(this.getParameter("minImageCount") != null){
           minImageCount = new Integer(this.getParameter("minImageCount"));
           
         }  
         trace("=======================================imageCount========"+imageCount);
         trace("=======================================numberOfFingersToEnroll========"+minImageCount);
         checkLicence(image_count);
        
          
         deviceList.add(defaultValue);

         populateDevicesList();
         
         setButtons(false);

        if(!isEmpty(lpinky) || !isEmpty(lring) || !isEmpty(lmiddle) || !isEmpty(lindex) || !isEmpty(lthumb)
                || !isEmpty(rpinky) || !isEmpty(rring) || !isEmpty(rmiddle) || !isEmpty(rindex) || !isEmpty(rthumb)){
           
           
            trace("===============================  BEFORE POPULATING THE FINGER IMAGES =================================");
            populateFingerImages(lpinky, lring, lmiddle, lindex, lthumb, rpinky, rring, rmiddle, rindex, rthumb,
                                 lpinkyT, lringT, lmiddleT, lindexT, lthumbT, rpinkyT, rringT, rmiddleT, rindexT, rthumbT);
            //setButtons(true);
        
            trace("===============================  AFTER POPULATING THE FINGER IMAGES =================================");
             
            
        } 
         
         
     }
    private void initVariables() {
        this.setSize(800, 600);
        this.setBackground(new Color(12, 50, 97));

        Jbutton_rightRing.setOpaque(false);
        Jbutton_rightRing.setContentAreaFilled(false);
        Jbutton_rightRing.setBorderPainted(false);

        Jbutton_leftMiddle.setOpaque(false);
        Jbutton_leftMiddle.setContentAreaFilled(false);
        Jbutton_leftMiddle.setBorderPainted(false);

        Jbutton_rightThumb.setOpaque(false);
        Jbutton_rightThumb.setContentAreaFilled(false);
        Jbutton_rightThumb.setBorderPainted(false);

        Jbutton_rightIndex.setOpaque(false);
        Jbutton_rightIndex.setContentAreaFilled(false);
        Jbutton_rightIndex.setBorderPainted(false);

        Jbutton_leftThumb.setOpaque(false);
        Jbutton_leftThumb.setContentAreaFilled(false);
        Jbutton_leftThumb.setBorderPainted(false);


        Jbutton_rightMiddle.setOpaque(false);
        Jbutton_rightMiddle.setContentAreaFilled(false);
        Jbutton_rightMiddle.setBorderPainted(false);

        Jbutton_rightPinky.setOpaque(false);
        Jbutton_rightPinky.setContentAreaFilled(false);
        Jbutton_rightPinky.setBorderPainted(false);

        Jbutton_leftPinky.setOpaque(false);
        Jbutton_leftPinky.setContentAreaFilled(false);
        Jbutton_leftPinky.setBorderPainted(false);

        Jbutton_leftRing.setOpaque(false);
        Jbutton_leftRing.setContentAreaFilled(false);
        Jbutton_leftRing.setBorderPainted(false);

        Jbutton_leftIndex.setOpaque(false);
        Jbutton_leftIndex.setContentAreaFilled(false);
        Jbutton_leftIndex.setBorderPainted(false);

        jButtonOk.setEnabled(false);
        jLabel_errorMessage.setVisible(false);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    public void setButtons(boolean argValue) {
        Jbutton_leftThumb.setEnabled(argValue);
        Jbutton_leftIndex.setEnabled(argValue);
        Jbutton_leftMiddle.setEnabled(argValue);
        Jbutton_leftRing.setEnabled(argValue);
        Jbutton_leftPinky.setEnabled(argValue);
        Jbutton_rightPinky.setEnabled(argValue);
        Jbutton_rightRing.setEnabled(argValue);
        Jbutton_rightMiddle.setEnabled(argValue);
        Jbutton_rightIndex.setEnabled(argValue);
        Jbutton_rightThumb.setEnabled(argValue);
        reStartButton.setEnabled(argValue);
        deleteLPinky.setEnabled(argValue);
        deleteLMiddle.setEnabled(argValue);
        deleteLRing.setEnabled(argValue);
        deleteLIndex.setEnabled(argValue);
        deleteLThumb1.setEnabled(argValue);
        deleteRPinky.setEnabled(argValue);
        deleteRRing.setEnabled(argValue);
        deleteRMiddle.setEnabled(argValue);
        deleteRIndex.setEnabled(argValue);
        deleteRThumb.setEnabled(argValue);    

    }

    public void clearStatusfields() {
        jLabel_scan1.setIcon(null);

        jLabel_rightScanThumb.setIcon(null);
        jLabel_rightScanIndex.setIcon(null);
        jLabel_rightScanMiddle.setIcon(null);
        jLabel_rightScanRing.setIcon(null);
        jLabel_rightScanPinky.setIcon(null);
        jLabel_leftScanThumb.setIcon(null);
        jLabel_leftScanIndex.setIcon(null);
        jLabel_leftScanMiddle.setIcon(null);
        jLabel_leftScanRing.setIcon(null);
        jLabel_leftScanPinky.setIcon(null);
        
        deleteLThumb7.setVisible(false);
        deleteLThumb7.setIcon(null);
        deleteLThumb2.setVisible(false);
        deleteLThumb2.setIcon(null);
        deleteLThumb3.setVisible(false);
        deleteLThumb3.setIcon(null);
        deleteLThumb4.setVisible(false);
        deleteLThumb4.setIcon(null);
        deleteLThumb5.setVisible(false);
        deleteLThumb5.setIcon(null);
        deleteLThumb6.setVisible(false);
        deleteLThumb6.setIcon(null);
        jLabel3.setVisible(false);
        
    }

    public final void initScreen() {

        System.out.println("Inside initScreen >>>>>>>>  ");

        clearStatusfields();
        initializeFingers();
        setButtons(true);
        //jLabel_status.setText("Please select finger to start enrollment process.");
    }

    public final void initComponentsCustom() {

        trace("Inside initComponentsCustom >>>>>>>>  ");
        clearStatusfields();
        setButtons(false);
        jLabel_status.setText("Please select a device first to start enrollment process.");

    }

    private void initializeFingers() {

        trace("Inside initializeFingers >>>>>>>>  ");

        for (int i = 0; i <= 10; i++) {
            sub_bio = i;
            setFingerStatus("C", sub_bio);

        }


        String[] scannedFingers = null;
        if (scannedFingers != null) {
            for (int i = 0; i < scannedFingers.length; i++) {
                setFingerStatus("Y", sub_bio);

            }
        }

        sub_bio = 0;
    }

    public void setFingerStatus(String str_accept, int sub_bio_st) {
        String fingerImage;
        String str;
        if (str_accept.equalsIgnoreCase("Y")) {
            fingerImage = "/images/accept.png";
        } else if (str_accept.equalsIgnoreCase("N")) {
            fingerImage = "/images/reject.png";
        } else {
            fingerImage = "";
        }

        switch (sub_bio_st) {
            case 1:
                /*if(rthumbCount==image_count)
                {
                    this.jLabel_status.setText("ERROR: No .of times to capture a fingerprint exceeded..");
                    resetProgressBar();
                    setButtons(true);
                    break;
                }
                else
                { */           
                   if (str_accept.equalsIgnoreCase("Y")) {
                        rthumbCount++;
                   }
                   if(rthumbCount<=image_count)
                    {
                        populateCapturedFPCountImages(rthumbCount);
                    }
                   jLabel_rightThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                   /*if(rthumbCount==image_count)
                    {
                        jDialog2.
                        setButtons(true);                        
                    }
                   else */
                   if(rthumbCount>=image_count)
                    {
                        setButtons(true);                        
                    }
                   else{
                   setButtons(false);
                   Jbutton_rightThumb.setEnabled(true);
                   }
                   break;
                //} //break;
            case 2:
                
               // jLabel_rightIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                /*if(rindexCount==image_count)
                {
                    this.jLabel_status.setText("ERROR: No .of times to capture a fingerprint exceeded..");
                    resetProgressBar();
                    setButtons(true);
                    break;
                }
                else
                {   */         
                   if (str_accept.equalsIgnoreCase("Y")) {
                        rindexCount++;
                   }
                   if(rindexCount<=image_count)
                    {                        
                     populateCapturedFPCountImages(rindexCount);
                    }
                   jLabel_rightIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                   if(rindexCount>=image_count)
                    {
                        setButtons(true);
                    }
                   else{
                   setButtons(false);
                   Jbutton_rightIndex.setEnabled(true);
                   }
                   break;
                //} //break;
            case 3:
                //jLabel_rightMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                /*if(rmiddleCount==image_count)
                {
                    this.jLabel_status.setText("ERROR: No .of times to capture a fingerprint exceeded..");
                    resetProgressBar();
                    setButtons(true);
                    break;
                }
                else
                { */           
                   if (str_accept.equalsIgnoreCase("Y")) {
                        rmiddleCount++;
                   }
                   if(rmiddleCount<=image_count)
                   {
                    populateCapturedFPCountImages(rmiddleCount);
                   }
                   jLabel_rightMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                   if(rmiddleCount>=image_count)
                    {
                        setButtons(true);
                    }
                   else{
                   setButtons(false);
                   Jbutton_rightMiddle.setEnabled(true);
                   }
                   break;
               // }
            case 4:
                //jLabel_rightRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                /*if(rringCount==image_count)
                {
                    this.jLabel_status.setText("ERROR: No .of times to capture a fingerprint exceeded..");
                    resetProgressBar();
                    setButtons(true);
                    break;
                }
                else
                {           */ 
                   if (str_accept.equalsIgnoreCase("Y")) {
                        rringCount++;
                   }
                   if(rringCount<=image_count)
                    {
                   populateCapturedFPCountImages(rringCount);
                    }
                   jLabel_rightRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                   if(rringCount>=image_count)
                    {
                        setButtons(true);
                    }
                   else{
                   setButtons(false);
                   Jbutton_rightRing.setEnabled(true);
                   }
                   break;
               // }
            case 5:
              //  jLabel_rightPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                 /*if(rpinkyCount==image_count)
                {
                    this.jLabel_status.setText("ERROR: No .of times to capture a fingerprint exceeded..");
                    resetProgressBar();
                    setButtons(true);
                    break;
                }
                else
                {  */          
                   if (str_accept.equalsIgnoreCase("Y")) {
                        rpinkyCount++;
                   }
                   if(rpinkyCount<=image_count)
                    {
                   populateCapturedFPCountImages(rpinkyCount);
                    }
                   jLabel_rightPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                   if(rpinkyCount>=image_count)
                    {
                        setButtons(true);
                    }
                   else{
                   setButtons(false);
                   Jbutton_rightPinky.setEnabled(true);
                   }
                   break;
                //}
            case 6:
                 //  jLabel_leftThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                 /*if(lthumbCount==image_count)
                {
                    this.jLabel_status.setText("ERROR: No .of times to capture a fingerprint exceeded..");
                    resetProgressBar();
                    setButtons(true);
                    break;
                }
                else
                {  */          
                   if (str_accept.equalsIgnoreCase("Y")) {
                        lthumbCount++;
                   }
                   if(lthumbCount<=image_count)
                    {
                   populateCapturedFPCountImages(lthumbCount);
                    }
                   jLabel_leftThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                   if(lthumbCount>=image_count)
                    {
                        setButtons(true);
                    }
                   else{
                   setButtons(false);
                   Jbutton_leftThumb.setEnabled(true);
                   }
                   break;
                //}
        
            case 7:
               // jLabel_leftIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                 /*if(lindexCount==image_count)
                {
                    this.jLabel_status.setText("ERROR: No .of times to capture a fingerprint exceeded..");
                    resetProgressBar();
                    setButtons(true);
                    break;
                }
                else
                {  */          
                   if (str_accept.equalsIgnoreCase("Y")) {
                        lindexCount++;
                   }
                  if(lindexCount<=image_count)
                    {
                   populateCapturedFPCountImages(lindexCount);
                    }
                   jLabel_leftIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                   if(lindexCount>=image_count)
                    {
                        setButtons(true);
                    }
                   else{
                   setButtons(false);
                   Jbutton_leftIndex.setEnabled(true);
                   }
                   break;
                //}
            case 8:
               // jLabel_leftMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                /*if(lmiddleCount==image_count)
                {
                    this.jLabel_status.setText("ERROR: No .of times to capture a fingerprint exceeded..");
                    resetProgressBar();
                    setButtons(true);
                    break;
                }
                else
                { */           
                   if (str_accept.equalsIgnoreCase("Y")) {
                        lmiddleCount++;
                   }
                   if(lmiddleCount<=image_count)
                    {
                   populateCapturedFPCountImages(lmiddleCount);
                    }
                   jLabel_leftMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                   if(lmiddleCount>=image_count)
                    {
                        setButtons(true);
                    }
                   else{
                   setButtons(false);
                   Jbutton_leftMiddle.setEnabled(true);
                   }
                   break;
                //}
            case 9:
            //    jLabel_leftRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                 /*if(lringCount==image_count)
                {
                    this.jLabel_status.setText("ERROR: No .of times to capture a fingerprint exceeded..");
                    resetProgressBar();
                    setButtons(true);
                    break;
                }
                else
                {  */          
                   if (str_accept.equalsIgnoreCase("Y")) {
                        lringCount++;
                   }
                if(lringCount<=image_count)
                {
                   populateCapturedFPCountImages(lringCount);
                }
                   jLabel_leftRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                   if(lringCount>=image_count)
                    {
                        setButtons(true);
                    }
                   else{
                   setButtons(false);
                   Jbutton_leftRing.setEnabled(true);
                   }
                   break;
                //}
            case 10:
             //   jLabel_leftPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                /*if(lpinkyCount==image_count)
                {
                    this.jLabel_status.setText("ERROR: No .of times to capture a fingerprint exceeded..");
                    resetProgressBar();
                    setButtons(true);
                    break;
                }
                else
                { */           
                   if (str_accept.equalsIgnoreCase("Y")) {
                        lpinkyCount++;
                   }
                   if(lpinkyCount<=image_count)
                {
                   populateCapturedFPCountImages(lpinkyCount);
                }
                   jLabel_leftPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImage)));
                   if(lpinkyCount>=image_count)
                    {
                        setButtons(true);
                    }
                   else{
                   setButtons(false);
                   Jbutton_leftPinky.setEnabled(true);
                   }
                   break;
                //}

            default:
                break;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jLabel_scannerimage = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jComboBox_userSelection = new javax.swing.JComboBox();
        jButtonOk = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jDialog2 = new javax.swing.JDialog();
        jPanel1 = new javax.swing.JPanel();
        Jbutton_leftIndex = new javax.swing.JButton();
        Jbutton_leftRing = new javax.swing.JButton();
        Jbutton_leftPinky = new javax.swing.JButton();
        Jbutton_rightPinky = new javax.swing.JButton();
        Jbutton_rightMiddle = new javax.swing.JButton();
        Jbutton_leftThumb = new javax.swing.JButton();
        Jbutton_rightIndex = new javax.swing.JButton();
        Jbutton_rightThumb = new javax.swing.JButton();
        Jbutton_rightRing = new javax.swing.JButton();
        Jbutton_leftMiddle = new javax.swing.JButton();
        jLabel_leftThumb = new javax.swing.JLabel();
        jLabel_leftMiddle = new javax.swing.JLabel();
        jLabel_leftIndex = new javax.swing.JLabel();
        jLabel_leftRing = new javax.swing.JLabel();
        jLabel_leftPinky = new javax.swing.JLabel();
        jLabel_rightThumb = new javax.swing.JLabel();
        jLabel_rightPinky = new javax.swing.JLabel();
        jLabel_rightRing = new javax.swing.JLabel();
        jLabel_rightMiddle = new javax.swing.JLabel();
        jLabel_rightIndex = new javax.swing.JLabel();
        jLabel_right = new javax.swing.JLabel();
        JLabel_LthumbCount = new javax.swing.JLabel();
        jLabel_fgImage = new javax.swing.JLabel();
        masterLabel = new javax.swing.JLabel();
        jLabel_errorMessage = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jButton_selectDevice = new javax.swing.JButton();
        jComboBox_deviceList = new javax.swing.JComboBox();
        reStartButton = new javax.swing.JButton();
        jLabel_status = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel_leftScanPinky = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane2 = new javax.swing.JTextPane();
        jPanel6 = new javax.swing.JPanel();
        jLabel_leftScanRing = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel_leftScanMiddle = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel_leftScanIndex = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel_leftScanThumb = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel_rightScanMiddle = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel_rightScanRing = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel_rightScanIndex = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel_rightScanPinky = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel_rightScanThumb = new javax.swing.JLabel();
        jLabel_left = new javax.swing.JLabel();
        jLabel_rightIndexImg = new javax.swing.JLabel();
        jLabel_leftRingImg = new javax.swing.JLabel();
        jLabel_rightThumbImg = new javax.swing.JLabel();
        jLabel_leftMiddleImg = new javax.swing.JLabel();
        jLabel_rightPinkyImg = new javax.swing.JLabel();
        jLabel_rightMiddleImg = new javax.swing.JLabel();
        jLabel_leftPinkyImg = new javax.swing.JLabel();
        jLabel_leftThumbImg = new javax.swing.JLabel();
        jLabel_rightRingImg = new javax.swing.JLabel();
        jLabel_leftIndexImg = new javax.swing.JLabel();
        jProgressBar = new javax.swing.JProgressBar();
        jLabel3 = new javax.swing.JLabel();
        imageQuality = new javax.swing.JLabel();
        jLabel_scan1 = new javax.swing.JLabel();
        jLabel_leftPinkyImg1 = new javax.swing.JLabel();
        jLabel_leftRingImg1 = new javax.swing.JLabel();
        jLabel_leftMiddleImg1 = new javax.swing.JLabel();
        jLabel_leftIndexImg1 = new javax.swing.JLabel();
        jLabel_leftThumbImg1 = new javax.swing.JLabel();
        jLabel_rightThumbImg1 = new javax.swing.JLabel();
        jLabel_rightIndexImg1 = new javax.swing.JLabel();
        jLabel_rightMiddleImg1 = new javax.swing.JLabel();
        jLabel_rightRingImg1 = new javax.swing.JLabel();
        jLabel_rightPinkyImg1 = new javax.swing.JLabel();
        deleteLPinky = new javax.swing.JButton();
        deleteLMiddle = new javax.swing.JButton();
        deleteLRing = new javax.swing.JButton();
        deleteLIndex = new javax.swing.JButton();
        deleteRPinky = new javax.swing.JButton();
        deleteRRing = new javax.swing.JButton();
        deleteRMiddle = new javax.swing.JButton();
        deleteRIndex = new javax.swing.JButton();
        deleteRThumb = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        deleteLThumb1 = new javax.swing.JButton();
        deleteLThumb2 = new javax.swing.JButton();
        deleteLThumb3 = new javax.swing.JButton();
        deleteLThumb4 = new javax.swing.JButton();
        deleteLThumb5 = new javax.swing.JButton();
        deleteLThumb6 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        deleteLThumb7 = new javax.swing.JButton();

        jDialog1.setAlwaysOnTop(true);
        jDialog1.setIconImage(null);
        jDialog1.setMinimumSize(new java.awt.Dimension(350, 201));
        jDialog1.setModalityType(java.awt.Dialog.ModalityType.DOCUMENT_MODAL);
        jDialog1.setUndecorated(true);

        jLabel_scannerimage.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel_scannerimage.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/finger_scaner.png"))); // NOI18N

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        //jLabel1.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/terminalDialog.png"))); // NOI18N
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(204, 204, 204), new java.awt.Color(0, 0, 0), new java.awt.Color(204, 204, 204)));
        jLabel1.setMaximumSize(new java.awt.Dimension(350, 201));
        jLabel1.setMinimumSize(new java.awt.Dimension(350, 201));
        jLabel1.setOpaque(true);
        jLabel1.setPreferredSize(new java.awt.Dimension(350, 201));

        jComboBox_userSelection.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jComboBox_userSelection.setModel(new javax.swing.DefaultComboBoxModel(deviceList.toArray()));
        jComboBox_userSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_userSelectionActionPerformed(evt);
            }
        });

        jButtonOk.setText("CONTINUE");
        jButtonOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOkActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Please select a terminal device first.");

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel_scannerimage)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox_userSelection, 0, 219, Short.MAX_VALUE)
                            .addGroup(jDialog1Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(jButtonOk, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jDialog1Layout.createSequentialGroup()
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 6, Short.MAX_VALUE)))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addComponent(jComboBox_userSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addComponent(jButtonOk, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel_scannerimage, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(40, Short.MAX_VALUE))
            .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jDialog1Layout.createSequentialGroup()
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Jbutton_leftIndex.setToolTipText("Left Hand Index Finger");
        Jbutton_leftIndex.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_leftIndex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_leftIndexActionPerformed(evt);
            }
        });
        jPanel1.add(Jbutton_leftIndex, new org.netbeans.lib.awtextra.AbsoluteConstraints(282, 150, 43, 50));

        Jbutton_leftRing.setToolTipText("Left Hand Ring Finger");
        Jbutton_leftRing.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_leftRing.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_leftRingActionPerformed(evt);
            }
        });
        jPanel1.add(Jbutton_leftRing, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 170, 40, 50));

        Jbutton_leftPinky.setToolTipText("Left Hand Pinky Finger");
        Jbutton_leftPinky.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_leftPinky.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_leftPinkyActionPerformed(evt);
            }
        });
        jPanel1.add(Jbutton_leftPinky, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 230, 50, 43));

        Jbutton_rightPinky.setToolTipText("Right Hand Pinky Finger");
        Jbutton_rightPinky.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Jbutton_rightPinky.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_rightPinky.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        Jbutton_rightPinky.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_rightPinkyActionPerformed(evt);
            }
        });
        jPanel1.add(Jbutton_rightPinky, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 230, 50, 50));

        Jbutton_rightMiddle.setToolTipText("Right Hand Middle Finger");
        Jbutton_rightMiddle.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Jbutton_rightMiddle.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Jbutton_rightMiddle.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_rightMiddle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_rightMiddleActionPerformed(evt);
            }
        });
        jPanel1.add(Jbutton_rightMiddle, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 140, 50, 50));

        Jbutton_leftThumb.setToolTipText("Left Hand Thumb");
        Jbutton_leftThumb.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Jbutton_leftThumb.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_leftThumb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_leftThumbActionPerformed(evt);
            }
        });
        jPanel1.add(Jbutton_leftThumb, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 253, 60, 60));

        Jbutton_rightIndex.setToolTipText("Right Hand Index Finger");
        Jbutton_rightIndex.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        Jbutton_rightIndex.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Jbutton_rightIndex.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        Jbutton_rightIndex.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_rightIndex.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        Jbutton_rightIndex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_rightIndexActionPerformed(evt);
            }
        });
        jPanel1.add(Jbutton_rightIndex, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 150, 50, 50));

        Jbutton_rightThumb.setToolTipText("Right Hand Thumb");
        Jbutton_rightThumb.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_rightThumb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_rightThumbActionPerformed(evt);
            }
        });
        jPanel1.add(Jbutton_rightThumb, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 260, 60, 60));

        Jbutton_rightRing.setToolTipText("Right Hand Ring Finger");
        Jbutton_rightRing.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_rightRing.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_rightRingActionPerformed(evt);
            }
        });
        jPanel1.add(Jbutton_rightRing, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 170, 45, 50));

        Jbutton_leftMiddle.setToolTipText("Left Hand Middle Finger");
        Jbutton_leftMiddle.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_leftMiddle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_leftMiddleActionPerformed(evt);
            }
        });
        jPanel1.add(Jbutton_leftMiddle, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 140, 40, 50));

        jLabel_leftThumb.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_leftThumb.setToolTipText("Thumb");
        jPanel1.add(jLabel_leftThumb, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 390, 50, 55));

        jLabel_leftMiddle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_leftMiddle.setToolTipText("Middle Finger");
        jPanel1.add(jLabel_leftMiddle, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 257, 50, 55));

        jLabel_leftIndex.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_leftIndex.setToolTipText("Index Finger");
        jPanel1.add(jLabel_leftIndex, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 324, 50, 55));

        jLabel_leftRing.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_leftRing.setToolTipText("Ring Finger");
        jPanel1.add(jLabel_leftRing, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 122, 50, 55));

        jLabel_leftPinky.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_leftPinky.setToolTipText("Pinky Finger");
        jPanel1.add(jLabel_leftPinky, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 190, 50, 55));

        jLabel_rightThumb.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_rightThumb.setToolTipText("Thumb");
        jPanel1.add(jLabel_rightThumb, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 393, 45, 52));

        jLabel_rightPinky.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_rightPinky.setToolTipText("Pinky Finger");
        jPanel1.add(jLabel_rightPinky, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 190, 45, 55));

        jLabel_rightRing.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_rightRing.setToolTipText("Ring Finger");
        jPanel1.add(jLabel_rightRing, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 123, 45, 54));

        jLabel_rightMiddle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_rightMiddle.setToolTipText("Middle Finger");
        jPanel1.add(jLabel_rightMiddle, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 260, 45, 52));

        jLabel_rightIndex.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_rightIndex.setToolTipText("Index Finger");
        jPanel1.add(jLabel_rightIndex, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 328, 45, 50));

        jLabel_right.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_right.setText("Right Hand");
        jPanel1.add(jLabel_right, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 90, 80, -1));

        JLabel_LthumbCount.setBackground(new java.awt.Color(27, 30, 39));
        JLabel_LthumbCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel_LthumbCount.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/lefthand.png"))); // NOI18N
        JLabel_LthumbCount.setToolTipText("");
        JLabel_LthumbCount.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel1.add(JLabel_LthumbCount, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 540, 458));

        jLabel_fgImage.setBackground(new java.awt.Color(255, 255, 255));
        jLabel_fgImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_fgImage.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Finger Image View", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 3, 14), java.awt.Color.darkGray)); // NOI18N
        jLabel_fgImage.setOpaque(true);
        jPanel1.add(jLabel_fgImage, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 95, 270, 250));

        masterLabel.setBackground(new java.awt.Color(27, 30, 39));
        masterLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        masterLabel.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/righthand.png"))); // NOI18N
        jPanel1.add(masterLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 70, 510, 458));

        jLabel_errorMessage.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_errorMessage.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel_errorMessage.setForeground(new java.awt.Color(163, 24, 0));
        jLabel_errorMessage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_errorMessage.setText("Error Message");
        jLabel_errorMessage.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.add(jLabel_errorMessage, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 740, 50));

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setOpaque(false);

        jButton_selectDevice.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/busy.png"))); // NOI18N
        jButton_selectDevice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_selectDeviceActionPerformed(evt);
            }
        });

        jComboBox_deviceList.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jComboBox_deviceList.setModel(new javax.swing.DefaultComboBoxModel(deviceList.toArray()));
        jComboBox_deviceList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_deviceListActionPerformed(evt);
            }
        });

        reStartButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        reStartButton.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        reStartButton.setText("CLEAR ALL");
        reStartButton.setToolTipText("CLEAR ALL");
        reStartButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        reStartButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        reStartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reStartButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jComboBox_deviceList, 0, 276, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_selectDevice, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(reStartButton)
                .addGap(8, 8, 8))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(reStartButton, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox_deviceList, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_selectDevice, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29))
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 10, 480, 50));

        jLabel_status.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel_status.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_status.setText("Please select a finger and click on \"Capture\" button.");
        jLabel_status.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.add(jLabel_status, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 740, 50));

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setOpaque(false);
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel_leftScanPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel3.add(jLabel_leftScanPinky, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 520, 120, 160));

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setForeground(new java.awt.Color(204, 204, 204));

        jTextPane1.setEditable(false);
        jTextPane1.setBackground(new java.awt.Color(125, 208, 231));
        jTextPane1.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jTextPane1.setForeground(new java.awt.Color(255, 255, 255));
        jTextPane1.setText("LEFT HAND");
        jTextPane1.setToolTipText("");
        jTextPane1.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        jTextPane1.setEnabled(false);
        jScrollPane1.setViewportView(jTextPane1);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, 600, -1));

        jScrollPane2.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane2.setForeground(new java.awt.Color(204, 204, 204));

        jTextPane2.setEditable(false);
        jTextPane2.setBackground(new java.awt.Color(125, 208, 231));
        jTextPane2.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        jTextPane2.setText("RIGHT HAND");
        jTextPane2.setToolTipText("");
        jTextPane2.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        jTextPane2.setEnabled(false);
        jScrollPane2.setViewportView(jTextPane2);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 490, 600, -1));

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.setOpaque(false);
        jPanel6.setLayout(new java.awt.GridBagLayout());

        jLabel_leftScanRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel6.add(jLabel_leftScanRing, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 520, 120, 160));

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setOpaque(false);
        jPanel7.setLayout(new java.awt.GridBagLayout());

        jLabel_leftScanMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel7.add(jLabel_leftScanMiddle, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 520, 120, 160));

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.setOpaque(false);
        jPanel8.setLayout(new java.awt.GridBagLayout());

        jLabel_leftScanIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel8.add(jLabel_leftScanIndex, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 520, 120, 160));

        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel9.setOpaque(false);
        jPanel9.setLayout(new java.awt.GridBagLayout());

        jLabel_leftScanThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel9.add(jLabel_leftScanThumb, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 520, 120, 160));

        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.setOpaque(false);
        jPanel10.setLayout(new java.awt.GridBagLayout());

        jLabel_rightScanMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel10.add(jLabel_rightScanMiddle, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 520, 120, 160));

        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel11.setOpaque(false);
        jPanel11.setLayout(new java.awt.GridBagLayout());

        jLabel_rightScanRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel11.add(jLabel_rightScanRing, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 520, 120, 160));

        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel12.setOpaque(false);
        jPanel12.setLayout(new java.awt.GridBagLayout());

        jLabel_rightScanIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel12.add(jLabel_rightScanIndex, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 520, 120, 160));

        jPanel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel13.setOpaque(false);
        jPanel13.setLayout(new java.awt.GridBagLayout());

        jLabel_rightScanPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel13.add(jLabel_rightScanPinky, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 520, 120, 160));

        jPanel14.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel14.setOpaque(false);
        jPanel14.setLayout(new java.awt.GridBagLayout());

        jLabel_rightScanThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel14.add(jLabel_rightScanThumb, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 520, 120, 160));

        jLabel_left.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_left.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_left.setText("Left Hand");
        jPanel1.add(jLabel_left, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 90, 90, -1));

        jLabel_rightIndexImg.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_rightIndexImg.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel_rightIndexImg.setText("Index");
        jPanel1.add(jLabel_rightIndexImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 680, 90, 25));

        jLabel_leftRingImg.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_leftRingImg.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel_leftRingImg.setText("Ring");
        jPanel1.add(jLabel_leftRingImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 680, 90, 25));

        jLabel_rightThumbImg.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_rightThumbImg.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel_rightThumbImg.setText("Thumb");
        jLabel_rightThumbImg.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel1.add(jLabel_rightThumbImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 680, 100, 25));

        jLabel_leftMiddleImg.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_leftMiddleImg.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel_leftMiddleImg.setText("Middle");
        jPanel1.add(jLabel_leftMiddleImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 680, 90, 25));

        jLabel_rightPinkyImg.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_rightPinkyImg.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel_rightPinkyImg.setText("Pinky");
        jPanel1.add(jLabel_rightPinkyImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 680, 100, 25));

        jLabel_rightMiddleImg.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_rightMiddleImg.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel_rightMiddleImg.setText("Middle");
        jPanel1.add(jLabel_rightMiddleImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 680, 90, 25));

        jLabel_leftPinkyImg.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_leftPinkyImg.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel_leftPinkyImg.setText("Pinky");
        jPanel1.add(jLabel_leftPinkyImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 680, 90, 25));

        jLabel_leftThumbImg.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_leftThumbImg.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel_leftThumbImg.setText("Thumb");
        jPanel1.add(jLabel_leftThumbImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 680, 100, 25));

        jLabel_rightRingImg.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_rightRingImg.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel_rightRingImg.setText("Ring");
        jPanel1.add(jLabel_rightRingImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 680, 100, 25));

        jLabel_leftIndexImg.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_leftIndexImg.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel_leftIndexImg.setText("Index");
        jPanel1.add(jLabel_leftIndexImg, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 680, 100, 25));

        jProgressBar.setBackground(new java.awt.Color(149, 204, 212));
        jProgressBar.setForeground(new java.awt.Color(18, 20, 20));
        jProgressBar.setMinimum(50);
        jProgressBar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jProgressBar.setStringPainted(true);
        jPanel1.add(jProgressBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 360, 280, 30));
        jProgressBar.getAccessibleContext().setAccessibleName("");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel3.setText("Fingerprint Captured Count :");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 430, 160, 60));

        imageQuality.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        imageQuality.setText("0");
        jPanel1.add(imageQuality, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 390, 40, 40));

        jLabel_scan1.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/reject.png"))); // NOI18N
        jPanel1.add(jLabel_scan1, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 390, -1, 40));

        jLabel_leftPinkyImg1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_leftPinkyImg1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_leftPinkyImg1.setText("Pinky");
        jPanel1.add(jLabel_leftPinkyImg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 210, 50, -1));

        jLabel_leftRingImg1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_leftRingImg1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_leftRingImg1.setText("Ring");
        jPanel1.add(jLabel_leftRingImg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, 50, -1));

        jLabel_leftMiddleImg1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_leftMiddleImg1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_leftMiddleImg1.setText("Middle");
        jPanel1.add(jLabel_leftMiddleImg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 280, 60, -1));

        jLabel_leftIndexImg1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_leftIndexImg1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_leftIndexImg1.setText("Index");
        jPanel1.add(jLabel_leftIndexImg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 340, 60, -1));

        jLabel_leftThumbImg1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_leftThumbImg1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_leftThumbImg1.setText("Thumb");
        jPanel1.add(jLabel_leftThumbImg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 420, 60, -1));

        jLabel_rightThumbImg1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_rightThumbImg1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_rightThumbImg1.setText("Thumb");
        jPanel1.add(jLabel_rightThumbImg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1190, 410, 60, -1));

        jLabel_rightIndexImg1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_rightIndexImg1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_rightIndexImg1.setText("Index");
        jPanel1.add(jLabel_rightIndexImg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1190, 340, 60, -1));

        jLabel_rightMiddleImg1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_rightMiddleImg1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_rightMiddleImg1.setText("Middle");
        jPanel1.add(jLabel_rightMiddleImg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1190, 280, 60, -1));

        jLabel_rightRingImg1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_rightRingImg1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_rightRingImg1.setText("Ring");
        jPanel1.add(jLabel_rightRingImg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1190, 140, 60, -1));

        jLabel_rightPinkyImg1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_rightPinkyImg1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel_rightPinkyImg1.setText("Pinky");
        jPanel1.add(jLabel_rightPinkyImg1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1190, 210, 60, -1));

        deleteLPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLPinky.setToolTipText("Delete Pinky Finger");
        deleteLPinky.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLPinkyActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLPinky, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 680, 30, -1));

        deleteLMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLMiddle.setToolTipText("Delete Middle Finger");
        deleteLMiddle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLMiddleActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLMiddle, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 680, 30, -1));

        deleteLRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLRing.setToolTipText("Delete Ring Finger");
        deleteLRing.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLRingActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLRing, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 680, 30, -1));

        deleteLIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLIndex.setToolTipText("Delete Index Finger");
        deleteLIndex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLIndexActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLIndex, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 680, 30, -1));

        deleteRPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteRPinky.setToolTipText("Delete Pinky Finger");
        deleteRPinky.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteRPinkyActionPerformed(evt);
            }
        });
        jPanel1.add(deleteRPinky, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 680, 30, -1));

        deleteRRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteRRing.setToolTipText("Delete Ring Finger");
        deleteRRing.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteRRingActionPerformed(evt);
            }
        });
        jPanel1.add(deleteRRing, new org.netbeans.lib.awtextra.AbsoluteConstraints(1080, 680, 30, -1));

        deleteRMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteRMiddle.setToolTipText("Delete Middle Finger");
        deleteRMiddle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteRMiddleActionPerformed(evt);
            }
        });
        jPanel1.add(deleteRMiddle, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 680, 30, -1));

        deleteRIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteRIndex.setToolTipText("Delete Index Finger");
        deleteRIndex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteRIndexActionPerformed(evt);
            }
        });
        jPanel1.add(deleteRIndex, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 680, 30, -1));

        deleteRThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteRThumb.setToolTipText("Delete Thumb");
        deleteRThumb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteRThumbActionPerformed(evt);
            }
        });
        jPanel1.add(deleteRThumb, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 680, 30, -1));
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 320, -1, -1));
        jLabel4.getAccessibleContext().setAccessibleDescription("No.Of times FP to be captured");

        deleteLThumb1.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLThumb1.setToolTipText("Delete Thumb");
        deleteLThumb1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLThumb1ActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLThumb1, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 680, 30, -1));

        deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLThumb2.setToolTipText("");
        deleteLThumb2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLThumb2ActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLThumb2, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 430, 30, -1));

        deleteLThumb3.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLThumb3.setToolTipText("");
        deleteLThumb3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLThumb3ActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLThumb3, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 430, 30, -1));

        deleteLThumb4.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLThumb4.setToolTipText("");
        deleteLThumb4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLThumb4ActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLThumb4, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 460, 30, -1));

        deleteLThumb5.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLThumb5.setToolTipText("");
        deleteLThumb5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLThumb5ActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLThumb5, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 460, 30, -1));

        deleteLThumb6.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLThumb6.setToolTipText("");
        deleteLThumb6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLThumb6ActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLThumb6, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 460, 30, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setText("Image Quality :");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 390, 170, 40));

        deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png"))); // NOI18N
        deleteLThumb7.setToolTipText("");
        deleteLThumb7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLThumb7ActionPerformed(evt);
            }
        });
        jPanel1.add(deleteLThumb7, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 430, 30, -1));

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1270, 710);
    }// </editor-fold>//GEN-END:initComponents

private void jButton_selectDeviceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_cancelActionPerformed
    // populateDevicesList();
}//GEN-LAST:event_jButton_cancelActionPerformed

private void Jbutton_rightThumbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_rightThumbActionPerformed
    i_capture = 0;
    sub_bio = 1;
    //jLabel_rightThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
    //jLabel_status.setText("Place Your Right Hand Thumb on Scanner & Click Capture");
//    resetProgressBar();
//    setButtons(false);  
//    populateFPCountImages(image_count,rthumbCount);
//    startCapture();
    
    if(image_count != rthumbCount){
       jLabel_rightThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //jLabel_status.setText("Place Your Left Pinky Finger on Scanner & Click Capture");
        //setButtons(false); 
       
       if(rthumbCount > 0){
           this.jLabel_status.setText("Please remove finger and place again.");
           this.jLabel_status.setVisible(true);
           
       }
       
       try{
           Thread.sleep(1000);
            resetProgressBar();
            setButtons(false);  
            populateFPCountImages(image_count,rthumbCount);

            startCapture();
            methodName = "Jbutton_rightThumbActionPerformed";
       } catch(Exception e){
           
       }
      
       
   } else {
       methodName = null;
   }
}//GEN-LAST:event_Jbutton_rightThumbActionPerformed

private void Jbutton_rightIndexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_rightIndexActionPerformed
    i_capture = 0;
    sub_bio = 2;

//    jLabel_rightIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
//    //jLabel_status.setText("Place Your Right Index Finger on Scanner & Click Capture");
//    //setButtons(false); 
//    resetProgressBar();
//    setButtons(false);  
//    populateFPCountImages(image_count,rindexCount);
//    startCapture();
//    
    if(image_count != rindexCount){
       jLabel_rightIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //jLabel_status.setText("Place Your Left Pinky Finger on Scanner & Click Capture");
        //setButtons(false); 
       
       if(rindexCount > 0){
           this.jLabel_status.setText("Please remove finger and place again.");
           this.jLabel_status.setVisible(true);
           
       }
       
       try{
           Thread.sleep(1000);
            resetProgressBar();
            setButtons(false);  
            populateFPCountImages(image_count,rindexCount);

            startCapture();
            methodName = "Jbutton_rightIndexActionPerformed";
       } catch(Exception e){
           
       }
      
       
   } else {
       methodName = null;
   }
}//GEN-LAST:event_Jbutton_rightIndexActionPerformed

private void Jbutton_rightMiddleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_rightMiddleActionPerformed
    i_capture = 0;
    sub_bio = 3;

//    jLabel_rightMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
//    //jLabel_status.setText("Place Your Right Middle Finger on Scanner & Click Capture");
//    //setButtons(false); 
//    resetProgressBar();
//    setButtons(false);  
//    populateFPCountImages(image_count,rmiddleCount);
//    startCapture();
    
    if(image_count != rmiddleCount){
       jLabel_rightMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //jLabel_status.setText("Place Your Left Pinky Finger on Scanner & Click Capture");
        //setButtons(false); 
       
       if(rmiddleCount > 0){
           this.jLabel_status.setText("Please remove finger and place again.");
           this.jLabel_status.setVisible(true);
           
       }
       
       try{
           Thread.sleep(1000);
            resetProgressBar();
            setButtons(false);  
            populateFPCountImages(image_count,rmiddleCount);

            startCapture();
            methodName = "Jbutton_rightMiddleActionPerformed";
       } catch(Exception e){
           
       }
      
       
   } else {
       methodName = null;
   }
}//GEN-LAST:event_Jbutton_rightMiddleActionPerformed

private void Jbutton_rightRingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_rightRingActionPerformed
    i_capture = 0;
    sub_bio = 4;

//    jLabel_rightRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
//    //jLabel_status.setText("Place Your Right Ring Finger on Scanner & Click Capture");
//    //setButtons(false);
//    resetProgressBar();
//    setButtons(false);  
//    populateFPCountImages(image_count,rringCount);
//    startCapture();
    
     
    if(image_count != rringCount){
       jLabel_rightRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //jLabel_status.setText("Place Your Left Pinky Finger on Scanner & Click Capture");
        //setButtons(false); 
       
       if(rringCount > 0){
           this.jLabel_status.setText("Please remove finger and place again.");
           this.jLabel_status.setVisible(true);
           
       }
       
       try{
           Thread.sleep(1000);
            resetProgressBar();
            setButtons(false);  
            populateFPCountImages(image_count,rringCount);

            startCapture();
            methodName = "Jbutton_rightRingActionPerformed";
       } catch(Exception e){
           
       }
      
       
   } else {
       methodName = null;
   }
}//GEN-LAST:event_Jbutton_rightRingActionPerformed

private void Jbutton_rightPinkyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_rightPinkyActionPerformed
    i_capture = 0;
    sub_bio = 5;

//    jLabel_rightPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
//    //jLabel_status.setText("Place Your Right Pinky Finger on Scanner & Click Capture");
//    //setButtons(false); 
//     resetProgressBar();
//    setButtons(false);  
//    populateFPCountImages(image_count,rpinkyCount);
//    startCapture();
    
      
    if(image_count != rpinkyCount){
       jLabel_rightPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //jLabel_status.setText("Place Your Left Pinky Finger on Scanner & Click Capture");
        //setButtons(false); 
       
       if(rpinkyCount > 0){
           this.jLabel_status.setText("Please remove finger and place again.");
           this.jLabel_status.setVisible(true);
           
       }
       
       try{
           Thread.sleep(1000);
            resetProgressBar();
            setButtons(false);  
            populateFPCountImages(image_count,rpinkyCount);

            startCapture();
            methodName = "Jbutton_rightPinkyActionPerformed";
       } catch(Exception e){
           
       }
      
       
   } else {
       methodName = null;
   }
}//GEN-LAST:event_Jbutton_rightPinkyActionPerformed

private void Jbutton_leftThumbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_leftThumbActionPerformed
    i_capture = 0;
    sub_bio = 6;

//    jLabel_leftThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
//    //jLabel_status.setText("Place Your Left Hand Thumb on Scanner & Click Capture");
//    //setButtons(false); 
//     resetProgressBar();
//    setButtons(false);  
//    populateFPCountImages(image_count,lthumbCount);
//    startCapture();
    
          
    if(image_count != lthumbCount){
       jLabel_leftThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //jLabel_status.setText("Place Your Left Pinky Finger on Scanner & Click Capture");
        //setButtons(false); 
       
       if(lthumbCount > 0){
           this.jLabel_status.setText("Please remove finger and place again.");
           this.jLabel_status.setVisible(true);
           
       }
       
       try{
           Thread.sleep(1000);
            resetProgressBar();
            setButtons(false);  
            populateFPCountImages(image_count,lthumbCount);

            startCapture();
            methodName = "Jbutton_leftThumbActionPerformed";
       } catch(Exception e){
           
       }
      
       
   } else {
       methodName = null;
   }

}//GEN-LAST:event_Jbutton_leftThumbActionPerformed

private void Jbutton_leftIndexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_leftIndexActionPerformed
    i_capture = 0;
    sub_bio = 7;

//    jLabel_leftIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
//    //jLabel_status.setText("Place Your Left Index Finger on Scanner & Click Capture");
//    //setButtons(false); 
//    resetProgressBar();
//    setButtons(false);  
//    populateFPCountImages(image_count,lindexCount);
//    startCapture();
    
           
    if(image_count != lindexCount){
       jLabel_leftIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //jLabel_status.setText("Place Your Left Pinky Finger on Scanner & Click Capture");
        //setButtons(false); 
       
       if(lindexCount > 0){
           this.jLabel_status.setText("Please remove finger and place again.");
           this.jLabel_status.setVisible(true);
           
       }
       
       try{
           Thread.sleep(1000);
            resetProgressBar();
            setButtons(false);  
            populateFPCountImages(image_count,lindexCount);

            startCapture();
            methodName = "Jbutton_leftIndexActionPerformed";
       } catch(Exception e){
           
       }
      
       
   } else {
       methodName = null;
   }

}//GEN-LAST:event_Jbutton_leftIndexActionPerformed

private void Jbutton_leftMiddleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_leftMiddleActionPerformed
    i_capture = 0;
    sub_bio = 8;

//    jLabel_leftMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
//    //jLabel_status.setText("Place Your Left Middle Finger on Scanner & Click Capture");
//    //setButtons(false); 
//   resetProgressBar();
//    setButtons(false);  
//    populateFPCountImages(image_count,lmiddleCount);
//    startCapture();
    
            
    if(image_count != lmiddleCount){
       jLabel_leftMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //jLabel_status.setText("Place Your Left Pinky Finger on Scanner & Click Capture");
        //setButtons(false); 
       
       if(lmiddleCount > 0){
           this.jLabel_status.setText("Please remove finger and place again.");
           this.jLabel_status.setVisible(true);
           
       }
       
       try{
           Thread.sleep(1000);
            resetProgressBar();
            setButtons(false);  
            populateFPCountImages(image_count,lmiddleCount);

            startCapture();
            methodName = "Jbutton_leftMiddleActionPerformed";
       } catch(Exception e){
           
       }
      
       
   } else {
       methodName = null;
   }
}//GEN-LAST:event_Jbutton_leftMiddleActionPerformed

private void Jbutton_leftRingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_leftRingActionPerformed
    i_capture = 0;
    sub_bio = 9;

//    jLabel_leftRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
//    //jLabel_status.setText("Place Your Left Ring Finger on Scanner & Click Capture");
//    //setButtons(false); 
//     resetProgressBar();
//    setButtons(false);  
//    populateFPCountImages(image_count,lringCount);
//    startCapture();
    
    if(image_count != lringCount){
       jLabel_leftRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //jLabel_status.setText("Place Your Left Pinky Finger on Scanner & Click Capture");
        //setButtons(false); 
       
       if(lringCount > 0){
           this.jLabel_status.setText("Please remove finger and place again.");
           this.jLabel_status.setVisible(true);
           
       }
       
       try{
           Thread.sleep(1000);
            resetProgressBar();
            setButtons(false);  
            populateFPCountImages(image_count,lringCount);

            startCapture();
            methodName = "Jbutton_leftRingActionPerformed";
       } catch(Exception e){
           
       }
      
       
   } else {
       methodName = null;
   }
}//GEN-LAST:event_Jbutton_leftRingActionPerformed

private void Jbutton_leftPinkyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_leftPinkyActionPerformed
    i_capture = 0;
    sub_bio = 10;
    
   if(image_count != lpinkyCount){
       jLabel_leftPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //jLabel_status.setText("Place Your Left Pinky Finger on Scanner & Click Capture");
        //setButtons(false); 
       
       if(lpinkyCount > 0){
           this.jLabel_status.setText("Please remove finger and place again.");
           this.jLabel_status.setVisible(true);
           
       }
       
       try{
           Thread.sleep(1000);
            resetProgressBar();
            setButtons(false);  
            populateFPCountImages(image_count,lpinkyCount);

            startCapture();
            methodName = "Jbutton_leftPinkyActionPerformed";
       } catch(Exception e){
           
       }
      
       
   } else {
       methodName = null;
   }
    
}//GEN-LAST:event_Jbutton_leftPinkyActionPerformed

    private void jComboBox_userSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_userSelectionActionPerformed
        JComboBox comboBox = jComboBox_userSelection;
        String value = comboBox.getSelectedItem().toString();

        if (!defaultValue.equalsIgnoreCase(value)) {
            jButtonOk.setEnabled(true);
        } else {
            jButtonOk.setEnabled(false);
        }
    }//GEN-LAST:event_jComboBox_userSelectionActionPerformed

    private void jButtonOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOkActionPerformed

        JComboBox comboBox = this.jComboBox_userSelection;
        String value = comboBox.getSelectedItem().toString();
        System.out.println("Selected Value is ==========================    " + value);

        System.out.println("Selected Finger Device Terminal =====================>   " + value);

        for (FingerCaptureDevice device : deviceHandleList) {
            try {
                System.out.println("Selected Finger Device Terminal Name >>>>>  =====================>   " + device.getName());

                if (value.equalsIgnoreCase(device.getName())) {
                    selectedDeviceHandle = device;
                    break;
                }
            } catch (FingerException ex) {
                Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //jLabel_device_status.setText(value);


        this.jDialog1.dispose();


    }//GEN-LAST:event_jButtonOkActionPerformed

    private void jButton_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_cancel1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton_cancel1ActionPerformed

    private void jComboBox_deviceListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_deviceListActionPerformed

        JComboBox comboBox = jComboBox_deviceList;
        String value = comboBox.getSelectedItem().toString();

        if (!defaultValue.equalsIgnoreCase(value)) {
            setButtons(true);
            deviceName = value;
            jLabel_status.setText("Please select finger to start enrollment process.");
        } else {
            setButtons(false);
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(this, "All the captured data will be lost. \n Do you really want to re-start the process.", "Re-Start Process", dialogButton);
            if (dialogResult == 0) {
                resetScreenComponents();
                jLabel_status.setText("Please select a device first to start enrollment process.");
            } else {
                comboBox.setSelectedItem(deviceName);
                trace("No Option");
            }

        }


    }//GEN-LAST:event_jComboBox_deviceListActionPerformed

    private void reStartButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reStartButtonActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "All the captured data will be lost. \n Do you really want to re-start the process.", "Re-Start Process", dialogButton);
        if (dialogResult == 0) {
            resetScreenComponents();
        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_reStartButtonActionPerformed

    private void deleteLPinkyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLPinkyActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Do you really want to delete the finger image.", "Delet Finger Image", dialogButton);
        if (dialogResult == 0) {
            jLabel_leftScanPinky.setIcon(null);
            jLabel_leftPinky.setIcon(null);
            deleteFPImagesFromArray(10);
            jLabel_status.setText("Finger Image deleted successfully.");
            resetProgressBar();

            


        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_deleteLPinkyActionPerformed

    private void deleteLRingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLRingActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Do you really want to delete the finger image.", "Delet Finger Image", dialogButton);
        if (dialogResult == 0) {
            jLabel_leftScanRing.setIcon(null);
            jLabel_leftRing.setIcon(null);
            deleteFPImagesFromArray(9);
            jLabel_status.setText("Finger Image deleted successfully.");
            resetProgressBar();
        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_deleteLRingActionPerformed

    private void deleteLMiddleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLMiddleActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Do you really want to delete the finger image.", "Delet Finger Image", dialogButton);
        if (dialogResult == 0) {
            jLabel_leftScanMiddle.setIcon(null);
            jLabel_leftMiddle.setIcon(null);
            deleteFPImagesFromArray(8);
            jLabel_status.setText("Finger Image deleted successfully.");
            resetProgressBar();
        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_deleteLMiddleActionPerformed

    private void deleteLIndexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLIndexActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Do you really want to delete the finger image.", "Delet Finger Image", dialogButton);
        if (dialogResult == 0) {
            jLabel_leftScanIndex.setIcon(null);
            jLabel_leftIndex.setIcon(null);
            deleteFPImagesFromArray(7);
            jLabel_status.setText("Finger Image deleted successfully.");
            resetProgressBar();
        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_deleteLIndexActionPerformed

    private void deleteRPinkyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteRPinkyActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Do you really want to delete the finger image.", "Delet Finger Image", dialogButton);
        if (dialogResult == 0) {
            jLabel_rightScanPinky.setIcon(null);
            jLabel_rightPinky.setIcon(null);
            deleteFPImagesFromArray(5);
            jLabel_status.setText("Finger Image deleted successfully.");
            resetProgressBar();
        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_deleteRPinkyActionPerformed

    private void deleteRRingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteRRingActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Do you really want to delete the finger image.", "Delet Finger Image", dialogButton);
        if (dialogResult == 0) {
            jLabel_rightScanRing.setIcon(null);
            jLabel_rightRing.setIcon(null);
            deleteFPImagesFromArray(4);
            jLabel_status.setText("Finger Image deleted successfully.");
            resetProgressBar();
        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_deleteRRingActionPerformed

    private void deleteRMiddleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteRMiddleActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Do you really want to delete the finger image.", "Delet Finger Image", dialogButton);
        if (dialogResult == 0) {
            jLabel_rightScanMiddle.setIcon(null);
            jLabel_rightMiddle.setIcon(null);
            deleteFPImagesFromArray(3);
            jLabel_status.setText("Finger Image deleted successfully.");
            resetProgressBar();
        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_deleteRMiddleActionPerformed

    private void deleteRIndexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteRIndexActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Do you really want to delete the finger image.", "Delet Finger Image", dialogButton);
        if (dialogResult == 0) {
            jLabel_rightScanIndex.setIcon(null);
            jLabel_rightIndex.setIcon(null);
            deleteFPImagesFromArray(2);
            jLabel_status.setText("Finger Image deleted successfully.");
            resetProgressBar();
        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_deleteRIndexActionPerformed

    private void deleteRThumbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteRThumbActionPerformed
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Do you really want to delete the finger image.", "Delet Finger Image", dialogButton);
        if (dialogResult == 0) {
            jLabel_rightScanThumb.setIcon(null);
            jLabel_rightThumb.setIcon(null);
            deleteFPImagesFromArray(1);
            jLabel_status.setText("Finger Image deleted successfully.");
            resetProgressBar();
        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_deleteRThumbActionPerformed

    private void deleteLThumb1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLThumb1ActionPerformed
         int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Do you really want to delete the finger image.", "Delet Finger Image", dialogButton);
        if (dialogResult == 0) {
            jLabel_leftScanThumb.setIcon(null);
            jLabel_leftThumb.setIcon(null);
            deleteFPImagesFromArray(6);
            jLabel_status.setText("Finger Image deleted successfully.");
            resetProgressBar();
        } else {
            trace("No Option");
        }
    }//GEN-LAST:event_deleteLThumb1ActionPerformed

    private void deleteLThumb2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLThumb2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleteLThumb2ActionPerformed

    private void deleteLThumb3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLThumb3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleteLThumb3ActionPerformed

    private void deleteLThumb4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLThumb4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleteLThumb4ActionPerformed

    private void deleteLThumb5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLThumb5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleteLThumb5ActionPerformed

    private void deleteLThumb6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLThumb6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleteLThumb6ActionPerformed

    private void deleteLThumb7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLThumb7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_deleteLThumb7ActionPerformed
    /**
     * ********************** END Right Hand code ********************
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel JLabel_LthumbCount;
    private javax.swing.JButton Jbutton_leftIndex;
    private javax.swing.JButton Jbutton_leftMiddle;
    private javax.swing.JButton Jbutton_leftPinky;
    private javax.swing.JButton Jbutton_leftRing;
    private javax.swing.JButton Jbutton_leftThumb;
    private javax.swing.JButton Jbutton_rightIndex;
    private javax.swing.JButton Jbutton_rightMiddle;
    private javax.swing.JButton Jbutton_rightPinky;
    private javax.swing.JButton Jbutton_rightRing;
    private javax.swing.JButton Jbutton_rightThumb;
    private javax.swing.JButton deleteLIndex;
    private javax.swing.JButton deleteLMiddle;
    private javax.swing.JButton deleteLPinky;
    private javax.swing.JButton deleteLRing;
    private javax.swing.JButton deleteLThumb1;
    private javax.swing.JButton deleteLThumb2;
    private javax.swing.JButton deleteLThumb3;
    private javax.swing.JButton deleteLThumb4;
    private javax.swing.JButton deleteLThumb5;
    private javax.swing.JButton deleteLThumb6;
    private javax.swing.JButton deleteLThumb7;
    private javax.swing.JButton deleteRIndex;
    private javax.swing.JButton deleteRMiddle;
    private javax.swing.JButton deleteRPinky;
    private javax.swing.JButton deleteRRing;
    private javax.swing.JButton deleteRThumb;
    private javax.swing.JLabel imageQuality;
    private javax.swing.JButton jButtonOk;
    private javax.swing.JButton jButton_selectDevice;
    private javax.swing.JComboBox jComboBox_deviceList;
    private javax.swing.JComboBox jComboBox_userSelection;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel_errorMessage;
    private javax.swing.JLabel jLabel_fgImage;
    private javax.swing.JLabel jLabel_left;
    private javax.swing.JLabel jLabel_leftIndex;
    private javax.swing.JLabel jLabel_leftIndexImg;
    private javax.swing.JLabel jLabel_leftIndexImg1;
    private javax.swing.JLabel jLabel_leftMiddle;
    private javax.swing.JLabel jLabel_leftMiddleImg;
    private javax.swing.JLabel jLabel_leftMiddleImg1;
    private javax.swing.JLabel jLabel_leftPinky;
    private javax.swing.JLabel jLabel_leftPinkyImg;
    private javax.swing.JLabel jLabel_leftPinkyImg1;
    private javax.swing.JLabel jLabel_leftRing;
    private javax.swing.JLabel jLabel_leftRingImg;
    private javax.swing.JLabel jLabel_leftRingImg1;
    private javax.swing.JLabel jLabel_leftScanIndex;
    private javax.swing.JLabel jLabel_leftScanMiddle;
    private javax.swing.JLabel jLabel_leftScanPinky;
    private javax.swing.JLabel jLabel_leftScanRing;
    private javax.swing.JLabel jLabel_leftScanThumb;
    private javax.swing.JLabel jLabel_leftThumb;
    private javax.swing.JLabel jLabel_leftThumbImg;
    private javax.swing.JLabel jLabel_leftThumbImg1;
    private javax.swing.JLabel jLabel_right;
    private javax.swing.JLabel jLabel_rightIndex;
    private javax.swing.JLabel jLabel_rightIndexImg;
    private javax.swing.JLabel jLabel_rightIndexImg1;
    private javax.swing.JLabel jLabel_rightMiddle;
    private javax.swing.JLabel jLabel_rightMiddleImg;
    private javax.swing.JLabel jLabel_rightMiddleImg1;
    private javax.swing.JLabel jLabel_rightPinky;
    private javax.swing.JLabel jLabel_rightPinkyImg;
    private javax.swing.JLabel jLabel_rightPinkyImg1;
    private javax.swing.JLabel jLabel_rightRing;
    private javax.swing.JLabel jLabel_rightRingImg;
    private javax.swing.JLabel jLabel_rightRingImg1;
    private javax.swing.JLabel jLabel_rightScanIndex;
    private javax.swing.JLabel jLabel_rightScanMiddle;
    private javax.swing.JLabel jLabel_rightScanPinky;
    private javax.swing.JLabel jLabel_rightScanRing;
    private javax.swing.JLabel jLabel_rightScanThumb;
    private javax.swing.JLabel jLabel_rightThumb;
    private javax.swing.JLabel jLabel_rightThumbImg;
    private javax.swing.JLabel jLabel_rightThumbImg1;
    private javax.swing.JLabel jLabel_scan1;
    private javax.swing.JLabel jLabel_scannerimage;
    private javax.swing.JLabel jLabel_status;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JProgressBar jProgressBar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTextPane jTextPane2;
    private javax.swing.JLabel masterLabel;
    private javax.swing.JButton reStartButton;
    // End of variables declaration//GEN-END:variables

    private void setImageCaptureStatus(Integer i_capture_cnt, String iconFile) {
        switch (i_capture_cnt) {
            case 1:
                jLabel_scan1.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(iconFile)));
                break;

            default:
                break;
        }

    }

    public void getDeviceHandle() {
        JComboBox comboBox = this.jComboBox_userSelection;
        String value = comboBox.getSelectedItem().toString();
        for (FingerCaptureDevice device : deviceHandleList) {
            try {
                System.out.println("Selected Finger Device Terminal Name >>>>> 2222222 =====================>   " + device.getName());

                if (value.equalsIgnoreCase(device.getName())) {
                    BiometricsJApplet.selectedDeviceHandle = device;
                    break;
                }
            } catch (FingerException ex) {
                Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void main(String args[]) {

        try {
            BiometricsJApplet bp = new BiometricsJApplet();


            bp.jComboBox_userSelection.setSelectedItem("Secugen Hamster FDU04");

        } catch (Exception ex) {
            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void setFingerprintArray(String templateHexBytes, String imageString)
    {
        trace("sub_bio::::::::::::::::::::::::"+sub_bio);   
        fingerImageMap.put(sub_bio, imageString);
                
                if(sub_bio <= 5){
                     fingerArray[0][sub_bio] = imageString;
                     fingerArray[0][sub_bio + 5] = templateHexBytes;
                } else {
                    fingerArray[1][sub_bio] = imageString;
                    fingerArray[1][sub_bio - 5] = templateHexBytes;
                }
    }
    
    private void setImageSections(BufferedImage img, int imageQuality, String templateHexBytes, String imageString) {
        switch (sub_bio) {
            case 1:
                if(rthumbImgQauality==0)
                {
                    rthumbImgQauality=imageQuality;
                    jLabel_rightScanThumb.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_rightThumbImg.setText("Thumb ["+rthumbImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                else if(rthumbImgQauality!=0 && rthumbImgQauality<imageQuality)
                {
                    rthumbImgQauality=imageQuality;
                    jLabel_rightScanThumb.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                     jLabel_rightThumbImg.setText("Thumb ["+rthumbImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                break;
            case 2:
               if(rindexImgQauality==0)
                {
                    rindexImgQauality=imageQuality;
                    jLabel_rightScanIndex.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_rightIndexImg.setText("Index ["+rindexImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                else if(rindexImgQauality!=0 && rindexImgQauality<imageQuality)
                {
                    rindexImgQauality=imageQuality;
                    jLabel_rightScanThumb.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_rightIndexImg.setText("Index ["+rindexImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                break;
            case 3:
                if(rmiddleImgQauality==0)
                {
                    rmiddleImgQauality=imageQuality;
                    jLabel_rightScanMiddle.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_rightMiddleImg.setText("Middle ["+rmiddleImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                else if(rmiddleImgQauality!=0 && rmiddleImgQauality<imageQuality)
                {
                    rmiddleImgQauality=imageQuality;
                    jLabel_rightScanMiddle.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_rightMiddleImg.setText("Middle ["+rmiddleImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                break;
            case 4:
                if(rringImgQauality==0)
                {
                     rringImgQauality=imageQuality;
                     jLabel_rightScanRing.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                     jLabel_rightRingImg.setText("Ring ["+rringImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                else if(rringImgQauality!=0 && rringImgQauality<imageQuality)
                {
                    rringImgQauality=imageQuality;
                    jLabel_rightScanRing.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_rightRingImg.setText("Ring ["+rringImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                break;
            case 5:
               if(rpinkyImgQauality==0)
                {
                     rpinkyImgQauality=imageQuality;
                     jLabel_rightScanPinky.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                     jLabel_rightPinkyImg.setText("Pinky ["+rpinkyImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                else if(rpinkyImgQauality!=0 && rpinkyImgQauality<imageQuality)
                {
                    rpinkyImgQauality=imageQuality;
                    jLabel_rightScanPinky.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_rightPinkyImg.setText("Pinky ["+rpinkyImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                break;
            case 6:
                if(lthumbImgQauality==0)
                {
                    lthumbImgQauality=imageQuality;
                    jLabel_leftScanThumb.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_leftThumbImg.setText("Thumb ["+lthumbImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                else if(lthumbImgQauality!=0 && lthumbImgQauality<imageQuality)
                {
                    lthumbImgQauality=imageQuality;
                    jLabel_leftScanThumb.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_leftThumbImg.setText("Thumb ["+lthumbImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                break;
            case 7:
                if(lindexImgQauality==0)
                {
                    lindexImgQauality=imageQuality;
                    jLabel_leftScanIndex.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_leftIndexImg.setText("Index ["+lindexImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                else if(lindexImgQauality!=0 && lindexImgQauality<imageQuality)
                {
                    lindexImgQauality=imageQuality;
                    jLabel_leftScanIndex.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_leftIndexImg.setText("Index ["+lindexImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                break;
            case 8:
                if(lmiddleImgQauality==0)
                {
                    lmiddleImgQauality=imageQuality;
                    jLabel_leftScanMiddle.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_leftMiddleImg.setText("Middle ["+lmiddleImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                else if(lmiddleImgQauality!=0 && lmiddleImgQauality<imageQuality)
                {
                    lmiddleImgQauality=imageQuality;
                    jLabel_leftScanMiddle.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_leftMiddleImg.setText("Middle ["+lmiddleImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                break;
            case 9:
                if(lringImgQauality==0)
                {
                    lringImgQauality=imageQuality;
                    jLabel_leftScanRing.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_leftRingImg.setText("Ring ["+lringImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                else if(lringImgQauality!=0 && lringImgQauality<imageQuality)
                {
                    lringImgQauality=imageQuality;
                    jLabel_leftScanRing.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_leftRingImg.setText("Ring ["+lringImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                break;
            case 10:
                if(lpinkyImgQauality==0)
                {
                    lpinkyImgQauality=imageQuality;
                    jLabel_leftScanPinky.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_leftPinkyImg.setText("Pinky ["+lpinkyImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                else if(lpinkyImgQauality!=0 && lpinkyImgQauality<imageQuality)
                {
                    lpinkyImgQauality=imageQuality;
                    jLabel_leftScanPinky.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                    jLabel_leftPinkyImg.setText("Pinky ["+lpinkyImgQauality+"]");
                    setFingerprintArray(templateHexBytes, imageString);
                }
                break;

            default:
                break;
        }

        this.jLabel_fgImage.setIcon(new javax.swing.ImageIcon(img.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));

    }
    private void deleteFPImagesFromArray(int bio_count)
    {
        fingerImageMap.remove(bio_count);
                
                if(bio_count <= 5){
                     fingerArray[0][bio_count] = null;
                     fingerArray[0][bio_count + 5] = null;
                } else {
                    fingerArray[1][bio_count] = null;
                    fingerArray[1][bio_count - 5] = null;
                }
    }
    
    private void populateFPCountImages(int count,int fingerImageCount) {


        switch (count) {
            case 1:
                jLabel3.setVisible(true);
                deleteLThumb7.setVisible(true);
                if(fingerImageCount == 0)
                {
                    deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                } // NOI18N
                break;
            case 2:
               jLabel3.setVisible(true);
               deleteLThumb7.setVisible(true);
               deleteLThumb2.setVisible(true);
                if(fingerImageCount == 0)
                {
                    deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                }
                break;
            case 3:
               jLabel3.setVisible(true);
               deleteLThumb7.setVisible(true);
               deleteLThumb2.setVisible(true);
               deleteLThumb3.setVisible(true);
               if(fingerImageCount == 0)
                {
                    deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb3.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                }
               break;
            case 4:
               jLabel3.setVisible(true);
               deleteLThumb7.setVisible(true);
               deleteLThumb2.setVisible(true);
               deleteLThumb3.setVisible(true);
               deleteLThumb4.setVisible(true);
               if(fingerImageCount == 0)
                {
                    deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb3.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb4.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                }
               break;
            case 5:
                 jLabel3.setVisible(true);
               deleteLThumb7.setVisible(true);
               deleteLThumb2.setVisible(true);
               deleteLThumb3.setVisible(true);
               deleteLThumb4.setVisible(true);
               deleteLThumb5.setVisible(true);
               if(fingerImageCount == 0)
                { 
                    deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb3.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb4.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb5.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                }
               break;
            case 6:
                jLabel3.setVisible(true);
               deleteLThumb7.setVisible(true);
               deleteLThumb2.setVisible(true);
               deleteLThumb3.setVisible(true);
               deleteLThumb4.setVisible(true);
               deleteLThumb5.setVisible(true);
               deleteLThumb6.setVisible(true);
               if(fingerImageCount == 0)
                {
                    deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb3.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb4.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb5.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                    deleteLThumb6.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
                }
                break;
            default:
                break;
        }
    }
    private void populateCapturedFPCountImages(int count) {


        switch (count) {
            case 1:
                jLabel3.setVisible(true);
                deleteLThumb7.setVisible(true);
                deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                break;
            case 2:
               jLabel3.setVisible(true);
               deleteLThumb7.setVisible(true);
               deleteLThumb2.setVisible(true);
               deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
               deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                 break;
            case 3:
               jLabel3.setVisible(true);
               deleteLThumb7.setVisible(true);
               deleteLThumb2.setVisible(true);
               deleteLThumb3.setVisible(true);
                deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb3.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
               break;
            case 4:
               jLabel3.setVisible(true);
               deleteLThumb7.setVisible(true);
               deleteLThumb2.setVisible(true);
               deleteLThumb3.setVisible(true);
               deleteLThumb4.setVisible(true);
               deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb3.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb4.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                break;
            case 5:
                 jLabel3.setVisible(true);
               deleteLThumb7.setVisible(true);
               deleteLThumb2.setVisible(true);
               deleteLThumb3.setVisible(true);
               deleteLThumb4.setVisible(true);
               deleteLThumb5.setVisible(true);
                deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb3.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb4.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb5.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
               break;
            case 6:
                jLabel3.setVisible(true);
               deleteLThumb7.setVisible(true);
               deleteLThumb2.setVisible(true);
               deleteLThumb3.setVisible(true);
               deleteLThumb4.setVisible(true);
               deleteLThumb5.setVisible(true);
               deleteLThumb6.setVisible(true);
                 deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb3.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb4.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb5.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
                deleteLThumb6.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/btn_success.png")));
               break;
            default:
                break;
        }
    }

    @Override
    public void deviceAdded(FingerCapture paramFingerCapture, FingerCaptureDevice device) {

        if (device != null) {
            try {
                System.out.print(device.getName() + " : Device Added =========================================.\n");
                System.out.print(" : Device Added =========================================  " + deviceList.contains(device.getName()));
                if (!deviceList.contains(device.getName())) {

                    deviceList.add(device.getName());
                    jComboBox_deviceList.setModel(new javax.swing.DefaultComboBoxModel(deviceList.toArray()));
                    System.out.println("Newly Added device name ...  " + device.getName());
                }
            } catch (FingerException ex) {
                Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void deviceRemoved(FingerCapture paramFingerCapture, String deviceName) {
        System.out.print(deviceName + " : Device removed.\n");
        if (deviceName != null) {

            if (deviceList.contains(deviceName)) {

                deviceList.remove(deviceName);
                jComboBox_deviceList.setModel(new javax.swing.DefaultComboBoxModel(deviceList.toArray()));

                System.out.println("Newly removed device name ...  " + deviceName);
            }

        }

    }
    private static String[] statusMessage = {
        UcmsMiddlewareConstants.CaptureStopped,
        "Please place your finger on the device........",
        "Please remove your finger from the device........",
        UcmsMiddlewareConstants.RollFinger,
        UcmsMiddlewareConstants.FingerPlaced,
        UcmsMiddlewareConstants.FingerRemoved,
        UcmsMiddlewareConstants.FingerDownBorder,
        UcmsMiddlewareConstants.FingerTopBorder,
        UcmsMiddlewareConstants.FingerRightBorder,
        UcmsMiddlewareConstants.FingerLeftBorder,
        UcmsMiddlewareConstants.PressFingerHarder,
        UcmsMiddlewareConstants.SwipeTooFast,
        UcmsMiddlewareConstants.SwipeTooSlow,
        UcmsMiddlewareConstants.SwipeTooSkewed,
        UcmsMiddlewareConstants.InapSpeed,
        UcmsMiddlewareConstants.ImageSmall,
        UcmsMiddlewareConstants.PoorQuality,
        UcmsMiddlewareConstants.FingerLifted,
        UcmsMiddlewareConstants.RollingFast,
        UcmsMiddlewareConstants.DirtySensor,
        UcmsMiddlewareConstants.FingerShifted,
        UcmsMiddlewareConstants.RollingTooShort,
        UcmsMiddlewareConstants.FewerFinger,
        UcmsMiddlewareConstants.TooManyFinger,
        UcmsMiddlewareConstants.WrongHand};

    @Override
    public void statusChanged(FingerCapture sender, FingerCaptureDevice device, int status) {
        System.out.print("\n- " + statusMessage[status] + " - \n");


        System.out.print("\n- " + status + " - \n");


        if (status == 4) {
            //this.jProgressBar.setValue(100);
            //this.jLabel_status.setText("Fingure Print Captured successfully.");
            //this.jLabel_status.setVisible(true);
        } else {
            // this.jProgressBar.setValue(10);
            this.jLabel_status.setText(statusMessage[status]);
            this.jLabel_status.setVisible(true);

        }

        if (status == 1) {
        }
        String errorCode = null;

        switch (status) {
            case 0:
                errorCode = "";
                break;
            case 1:
                errorCode = "";

                break;
            case 2:
                errorCode = "-1";

                break;
            case 3:
                errorCode = "-1";
                break;
            case 4:
                errorCode = "";

                break;
            case 5:
                errorCode = "-1";
                break;
            case 6:
                errorCode = "-1";
                break;
            case 7:
                errorCode = "-1";
                break;
            case 8:
                errorCode = "-1";
                break;
            case 9:
                errorCode = "-1";
                break;
            case 10:
                errorCode = "-1";
                break;
            case 11:
                errorCode = "-1";
                break;
            case 12:
                errorCode = "-1";
                break;
            case 13:
                errorCode = "-1";
                break;
            case 14:
                errorCode = "-1";
                break;
            case 15:
                errorCode = "-1";
                break;
            case 16:
                errorCode = "-1";
                break;
            case 17:
                errorCode = "-1";
                break;
            case 18:
                errorCode = "-1";
                break;
            case 19:
                errorCode = "-1";
                break;
            case 20:
                errorCode = "-1";
                break;
            case 21:
                errorCode = "-1";
                break;
            case 22:
                errorCode = "-1";
                break;
            case 23:
                errorCode = "-1";
                break;
            case 24:
                errorCode = "-1";
                break;
        }


    }

    @Override
    public void imagePreview(FingerCapture paramFingerCapture, FingerCaptureDevice paramFingerCaptureDevice, FingerImage image) {
        try {
            System.out.println("   - %d finger(s) detected.\n" + image.getSegmentCount());

            image.setImpressionType(FingerImpression.LiveScanPlain);


            this.jLabel_fgImage.setIcon(new javax.swing.ImageIcon(image.createImageAwt().getScaledInstance(98, 128, Image.SCALE_DEFAULT)));
            this.jLabel_fgImage.revalidate();
            this.jLabel_fgImage.repaint();
            this.jLabel_fgImage.setVisible(true);


        } catch (FingerException ex) {
            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void imageCaptured(FingerCapture paramFingerCapture, FingerCaptureDevice device, FingerImage image) {
        try {
            
            if (device != null) {
                System.out.print(device.getName() + " : ");
            }

            if (image.getSegmentCount() > 0) {
                System.out.printf("   - %d finger(s) detected.\n", image.getSegmentCount());
            }

        } catch (FingerException e) {
        }


    }

    @Override
    public void imageProcessed(FingerCapture paramFingerCapture, FingerCaptureDevice paramFingerCaptureDevice, FingerImage image) {
        try {
            // display image information

            displayImageInformation(image);
        } catch (FingerException ex) {
            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
        }


        try {
            addImage(image);

        } catch (Exception ex) {
            // Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    private void displayImageInformation(FingerImage image) throws FingerException {
        trace("   - capture device vendor : " + image.getCaptureDeviceVendor() + "\n");
        trace("   - image width : " + image.getWidth() + " pixels\n");
        trace("   - image height : " + image.getHeight() + " pixels\n");
        trace("   - impression type : " + image.getImpressionType() + "\n");
        trace("   - Horizontal res. : " + image.getHorizontalResolution() + " dpi\n");
        trace("   - Vertical res. : " + image.getVerticalResolution() + " dpi\n");
        trace("   - NFIQ : " + image.getQuality() + "\n");


        String iconFile;

        if (image != null) {

            counter = 0;
            while (counter <= 100) {
                jProgressBar.setValue(counter);
                counter++;
                try {
                    Thread.sleep(10L);
                } catch (Exception ex) {
                }
            }


            this.jLabel_status.setText("Finger Print Captured successfully.");
            this.jLabel_status.setVisible(true);


            if (image.getQuality() >= image_quality) {
                bufferedImage = (BufferedImage) image.createImageAwt();

                FingerTemplate template = image.createTemplate();
                 
                trace("=========  Template  ================ [ " + template + " ]");
                
                
                byte[] templateBytes = template.toIso19794CompactCard(template.getMinutiaCount());
                
                CommonUtil util = new CommonUtil();
                
                FingerImageRecord fpr = new FingerImageRecord();
                
                FingerImage fpi = image.clone();
                fpr.add(fpi);
                
                byte[] ansiFormat = fpr.toBuffer(FingerImageRecordFormat.Incits381_2009);
                 
                String ansitemplateHexBytes = util.arrayToHex(ansiFormat);
                  
                trace("=========  ANSI Template bytes ================ [ " + ansitemplateHexBytes + " ]");
                
                String templateHexBytes = util.arrayToHex(templateBytes);
                trace("=========  Template bytes ================ [ " + templateHexBytes + " ]");
                //bufferedImage = createCompactTemplate();
                this.imageQuality.setText("" + image.getQuality());

                String imageString = imageToString(bufferedImage);
                
                /*fingerImageMap.put(sub_bio, imageString);
                
                if(sub_bio <= 5){
                     fingerArray[0][sub_bio] = imageString;
                     fingerArray[0][sub_bio + 5] = templateHexBytes;
                } else {
                    fingerArray[1][sub_bio] = imageString;
                    fingerArray[1][sub_bio - 5] = templateHexBytes;
                }*/
               
                
                setImageSections(bufferedImage,image.getQuality(),templateHexBytes,imageString );
                this.jLabel_fgImage.setIcon(new javax.swing.ImageIcon(bufferedImage.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                this.jLabel_fgImage.revalidate();
                this.jLabel_fgImage.repaint();
                this.jLabel_fgImage.setVisible(true);

                iconFile = "/images/accept.png";
                setFingerStatus("Y", sub_bio);
                setImageCaptureStatus(i_capture, iconFile);
                this.jLabel_scan1.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(iconFile)));
                this.jLabel_scan1.setVisible(true);
              //  setButtons(true);
            } else {

                this.imageQuality.setText(new Integer(image.getQuality()).toString());

                iconFile = "/images/reject.png";
                setImageCaptureStatus(i_capture, iconFile);
                this.jLabel_status.setText("ERROR: Poor Image Quality. Please try again.");
                // this.jLabel_status.setForeground(new java.awt.Color(163, 24, 0));
                this.jLabel_scan1.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(iconFile)));
                this.jLabel_scan1.setVisible(true);
                setFingerStatus("N", sub_bio);

            }



        } else {
            iconFile = "/images/reject.png";
            setImageCaptureStatus(i_capture, iconFile);
            setFingerStatus("N", sub_bio);
            this.jLabel_status.setText("ERROR: Image is not captured. Please try again.");
            //this.jLabel_status.setForeground(new java.awt.Color(163, 24, 0));

        }
      //  setButtons(true);
    }

    
    public String imageToString(BufferedImage bImage)   {

        String imageString = null;

        //image to bytes
 
        try {

            System.out.println("================= ENCODED STRING VALUE START ============================ \n" + bImage + "\n");
              
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bImage, "jpg", baos );
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
             
            //bytes to string

            BASE64Encoder enc = new BASE64Encoder();
             
            imageString =  enc.encode(imageInByte);
            
            //System.out.println("================ IMAGE STRING NEW ===============\n" + imageString + "\n");
           
            System.out.println("================= ENCODED STRING VALUE ENDS ============================ \n");
            

        } catch (Exception ex) {

            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);

        }

        return imageString;

    }
    
    
    public void addImage(FingerImage image) throws Exception {

        int fgImgStatus = 0;
        String errorCode = "";
        String errorMsg = "";
        if (fgEnroll == null) {
            try {

                fgEnroll = new FingerEnroll();
            } catch (FingerException ex) {
                throw new Exception(ex.getMessage());
                //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //add image
        try {
            fgImgStatus = fgEnroll.addImage(image);
        } catch (FingerException ex) {

            throw new Exception(ex.getMessage());
            //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
        }


        //check image status
        switch (fgImgStatus) {
            case FingerEnrollStatus.InProgress:
                errorCode = "";
                errorMsg = "In Progress";
                break;
            case FingerEnrollStatus.WrongFinger:
                errorCode = "-1";
                errorMsg = "Wrong Finger Placed";
                break;
            case FingerEnrollStatus.UnknownFinger:
                errorCode = "-1";
                errorMsg = "Unknown Finger Placed";
                break;
            case FingerEnrollStatus.NonMatch:
                errorCode = "-1";
                errorMsg = "The presented finger does not match with the previous presentations.";
                break;
            case FingerEnrollStatus.Completed:
                errorCode = "";
                errorMsg = "Compelete.";
                break;
            case FingerEnrollStatus.Failed:
                errorCode = "-1";
                errorMsg = "Finger Catpure Failed.";
                break;

        }

        //return if there is an error
        if (errorCode.equalsIgnoreCase("-1")) {
        } else {

            imageCount++;
        }



    }

    public BufferedImage createCompactTemplate() {

        trace("========= BEFORE CREATING THE IMAGE TEMPLATE ====================");
        BufferedImage fgImage = null;
        FingerTemplate[] fgTemplates = null;
        FingerTemplate fgTemplate = null;
        FingerTemplateRecord fgTemplateRecord = null;
        FingerImage image = null;


        try {
            image = fgEnroll.getBestImage(sub_bio);
            trace("========= BEST IMAGE QUALITY IS ====================>   " + image.getQuality());
        
        } catch (FingerException ex) {
            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            fgTemplateRecord = fgEnroll.createTemplateRecord(1);
            
            trace("========= TEMPLATE COUNTS ====================>  " + fgTemplateRecord.getFingerCount());
        
        } catch (FingerException ex) {
            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
        }


        fgTemplates = fgTemplateRecord.getTemplates();
        fgTemplate = fgTemplates[0];
        int minuCount = 0;

        try {
            minuCount = fgTemplate.getMinutiaCount();
            
            trace("========= MINUTIA COUNT ====================>  " + minuCount);
        
        } catch (FingerException ex) {
            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {

            byte[] templateImage =  fgTemplate.toIso19794CompactCard(minuCount);
            
            InputStream in = new ByteArrayInputStream(templateImage);
	    BufferedImage bImageFromConvert = ImageIO.read(in);
            fgImage = bImageFromConvert;
            
        } catch (FingerException ex) {
            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
        }
  
        imageCount = 0;

        fgEnroll.dispose();

        trace("========= AFTER CREATING THE IMAGE TEMPLATE ====================");
        
        return fgImage;

    }

    private void checkLicence(Integer imageCount) {
        try {
            FingerLicense.checkLicense();
            fgEnroll = new FingerEnroll();
            fgEnroll.setMinimumImageCount(imageCount);
            fgEnroll.setImageQualityThreshold(90);
            trace("License is valid\n");
            trace(" - path : " + FingerLicense.getLicensePath() + "\n");
            trace(" - owner : " + FingerLicense.getLicenseOwner() + "\n");
            trace(" - count : " + FingerLicense.getMaxFingerCount() + "\n");
            trace(" - imageCount :"+image_count);
            trace(" - fgEnroll.imageCount :"+fgEnroll.getMinimumImageCount());
        } catch (FingerException ex) {

            trace("\nAn error has occured !");

        }
    }

    private void populateDevicesList() {
        try {
            capture = new FingerCapture(this, false);
            fgEnroll = new FingerEnroll();
            fgEnroll.setMinimumImageCount(image_count);
           /* lpinkyCount = image_count; 	
            lringCount = image_count;		
            lmiddleCount = image_count;
            lindexCorthumbCountunt = image_count;
            lthumbCount = image_count;
            rpinkyCount = image_count;
            rringCount = image_count;
            rmiddleCount = image_count;	
            rindexCount = image_count;	
             = image_count;*/
            Thread.sleep(60L);
            devices = capture.getDevices();
            trace("=========== List Of Devices ====================  " + devices.length);
            trace(" ============= fgEnroll.imageCount rmiddleCount ==================:"+rmiddleCount);
            trace(" ============= fgEnroll.imageCount populateDevicesList ==================:"+fgEnroll.getMinimumImageCount());
            if (devices != null && devices.length > 0) {


                for (FingerCaptureDevice device : devices) {
                    trace("Device Name is >>>>>>>>>>>  " + device);
                    if (!deviceList.contains(device.getName())) {
                        deviceList.add(device.getName());
                    }
                }
                jComboBox_deviceList.setModel(new javax.swing.DefaultComboBoxModel(deviceList.toArray()));
                this.jLabel_errorMessage.setVisible(false);
                this.jLabel_status.setVisible(true);
                initScreen();

            } else {
                trace("InSide Else >>>>>>>>>>>  " + devices);
                this.jLabel_errorMessage.setText("ERROR: Please attach a finger capturing terminal first.");
                this.jLabel_status.setVisible(false);
                this.jLabel_errorMessage.setVisible(true);

            }


        } catch (InterruptedException | FingerException ex) {
            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
            if (ex instanceof FingerException) {
                this.jLabel_status.setText(ex.getMessage());
                this.jLabel_status.setForeground(new java.awt.Color(163, 24, 0));
            }
        }

    }

    public void startCapture() {
        JComboBox comboBox = this.jComboBox_deviceList;
        String value = comboBox.getSelectedItem().toString();
        FingerCaptureDevice captureDevice = null;


        try {
            for (FingerCaptureDevice fcd : devices) {

                if (value.equalsIgnoreCase(fcd.getName())) {
                    captureDevice = fcd;
                    break;
                }

            }
            trace("Finger Number ===================  " + sub_bio);

            captureDevice.startCapture(sub_bio);
            //captureDevice.stopCapture();
            //fgEnroll.dispose();
        } catch (FingerException ex) {
            Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
            this.jLabel_status.setText(ex.getMessage());

        }


    }
    protected int counter = 0;

    private void resetProgressBar() {
        this.jProgressBar.setValue(0);
        this.jLabel_fgImage.setIcon(null);
        this.jLabel_scan1.setVisible(false);
        this.jLabel_scan1.setIcon(null);
        this.imageQuality.setText(new Integer(0).toString());
    }

    private void resetScreenComponents() {
        jLabel_rightScanThumb.setIcon(null);
        jLabel_rightScanIndex.setIcon(null);
        jLabel_rightScanRing.setIcon(null);
        jLabel_rightScanMiddle.setIcon(null);
        jLabel_rightScanPinky.setIcon(null);
        jLabel_leftScanThumb.setIcon(null);
        jLabel_leftScanIndex.setIcon(null);
        jLabel_leftScanMiddle.setIcon(null);
        jLabel_leftScanRing.setIcon(null);
        jLabel_leftScanPinky.setIcon(null);
        jLabel_leftIndex.setIcon(null);
        jLabel_leftMiddle.setIcon(null);
        jLabel_leftPinky.setIcon(null);
        jLabel_leftRing.setIcon(null);
        jLabel_leftThumb.setIcon(null);
        jLabel_rightIndex.setIcon(null);
        jLabel_rightMiddle.setIcon(null);
        jLabel_rightPinky.setIcon(null);
        jLabel_rightRing.setIcon(null);
        jLabel_rightThumb.setIcon(null);
        jLabel_fgImage.setIcon(null);
        deleteLThumb7.setVisible(false);
        deleteLThumb7.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
        deleteLThumb2.setVisible(false);
        deleteLThumb2.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
        deleteLThumb3.setVisible(false);
        deleteLThumb3.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
        deleteLThumb4.setVisible(false);
        deleteLThumb4.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
        deleteLThumb5.setVisible(false);
        deleteLThumb5.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
        deleteLThumb6.setVisible(false);
        deleteLThumb6.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/forbidden.png")));
        jProgressBar.setValue(0);
        lpinkyCount = 0; 	
        lringCount = 0;		
        lmiddleCount = 0;
        lindexCount = 0;
        lthumbCount = 0;
        rpinkyCount = 0;
        rringCount = 0;
        rmiddleCount = 0;	
        rindexCount = 0;	
        rthumbCount = 0;
        imageQuality.setText("0");
        jLabel_status.setText("Please select finger to start enrollment process.");
        jLabel_scan1.setIcon(null);
    }
    
    public HashMap<Integer, String> getFingerImages(){
         
        return fingerImageMap;
    }
    
     public String[][] getFingerImagesArray(){
         trace("lpinkyCount:::::::::::"+lpinkyCount);
         trace("lringCount:::::::::::"+lringCount);
         trace("lmiddleCount:::::::::::"+lmiddleCount);
         trace("lindexCount:::::::::::"+lindexCount);
         trace("lthumbCount:::::::::::"+lthumbCount);
         trace("rthumbCount:::::::::::"+rthumbCount);
         trace("rindexCount:::::::::::"+rindexCount);
         trace("rmiddleCount:::::::::::"+rmiddleCount);
         trace("rringCount:::::::::::"+rringCount);
         trace("rpinkyCount:::::::::::"+rpinkyCount);
         trace("fingerImageMap.size():::::::::::::"+fingerImageMap.size());
         trace("imageCount::::::::::::::"+imageCount);
         boolean flag=false;
         if((lpinkyCount!=0 && lpinkyCount<imageCount)||(lringCount!=0 && lringCount<imageCount)
            ||(lmiddleCount!=0 && lmiddleCount<imageCount)||(lindexCount!=0 && lindexCount<imageCount)
            ||(lthumbCount!=0 && lthumbCount<imageCount)||(rthumbCount!=0 && rthumbCount<imageCount)
            ||(rindexCount!=0 && rindexCount<imageCount)||(rmiddleCount!=0 && rmiddleCount<imageCount)
            ||(rringCount!=0 && rringCount<imageCount)||(rpinkyCount!=0 && rpinkyCount<imageCount))
                 {
                     flag=true;
                 }
        trace("flag :::::::::::::::::::"+flag);
         if((fingerImageMap.size()<minImageCount))
         {
             return null;
         }
         else
            return fingerArray;
    }
    
    public String validateFingerprints(){
        
        return null;
    }    
     private boolean isEmpty(String str) {
        return str == null || "".equalsIgnoreCase(str) || str.length() == 0;
    }

    private void populateFingerImages(String lpinky, String lring, String lmiddle, String lindex, String lthumb, String rpinky, 
            String rring, String rmiddle, String rindex, String rthumb,String lpinkyT, String lringT, String lmiddleT, String lindexT, String lthumbT,
            String rpinkyT,String rringT, String rmiddleT, String rindexT, String rthumbT) {
           
        String fingerImageCaptured = "/images/accept.png";
        String fingerImageNotCaptured = "/images/reject.png";
   
        try {         
            if(rthumb != null && rthumb.length() > 0){

                BASE64Decoder decoder = new BASE64Decoder();

                byte[] output = decoder.decodeBuffer(rthumb);

                ByteArrayInputStream in = new ByteArrayInputStream(output);
                
                BufferedImage rthumbImg = ImageIO.read(in);
                trace("jLabel_rightScanThumb : " + jLabel_rightScanThumb);
                
                trace("rthumbImg Image : " + rthumbImg);
                
                
                Image img = rthumbImg.getScaledInstance(117, 155, Image.SCALE_DEFAULT);
                
                trace("jLabel_rightScanThumb Image : " + img);
                
                
                jLabel_rightScanThumb.setIcon(new javax.swing.ImageIcon(img));
                
                jLabel_rightThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageCaptured)));
                fingerArray[0][1]=rthumb;
                fingerArray[0][6]=rthumbT;
                fingerImageMap.put(1,rthumb);
       
              } else {
                jLabel_rightThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageNotCaptured)));
            }
            if(rindex != null && rindex.length() > 0){

                BASE64Decoder decoder = new BASE64Decoder();

                byte[] output = decoder.decodeBuffer(rindex);

                ByteArrayInputStream in = new ByteArrayInputStream(output);
                
                BufferedImage rindexImg = ImageIO.read(in);
                jLabel_rightScanIndex.setIcon(new javax.swing.ImageIcon(rindexImg.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                jLabel_rightIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageCaptured)));
                fingerArray[0][2]=rindex;
                fingerArray[0][7]=rindexT;
                fingerImageMap.put(2,rindex);
            } else {
                jLabel_rightIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageNotCaptured)));
            }
            if(rmiddle != null && rmiddle.length() > 0){

                BASE64Decoder decoder = new BASE64Decoder();

                byte[] output = decoder.decodeBuffer(rmiddle);

                ByteArrayInputStream in = new ByteArrayInputStream(output);
                
                BufferedImage rmiddleImg = ImageIO.read(in);
                jLabel_rightScanMiddle.setIcon(new javax.swing.ImageIcon(rmiddleImg.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                jLabel_rightMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageCaptured)));
                fingerArray[0][3]=rmiddle;
                fingerArray[0][8]=rmiddleT;                
                fingerImageMap.put(3,rmiddle);

            } else {
                jLabel_rightMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageNotCaptured)));
            }
            if(rring != null && rring.length() > 0){

                BASE64Decoder decoder = new BASE64Decoder();

                byte[] output = decoder.decodeBuffer(rring);

                ByteArrayInputStream in = new ByteArrayInputStream(output);
                
                BufferedImage rringImg = ImageIO.read(in);
                jLabel_rightScanRing.setIcon(new javax.swing.ImageIcon(rringImg.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                jLabel_rightRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageCaptured)));
                fingerArray[0][4]=rring;
                fingerArray[0][9]=rringT;                
                fingerImageMap.put(4,rring);
            } else {
                 jLabel_rightRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageNotCaptured)));
            }
            if(rpinky != null && rpinky.length() > 0){

                BASE64Decoder decoder = new BASE64Decoder();

                byte[] output = decoder.decodeBuffer(rpinky);

                ByteArrayInputStream in = new ByteArrayInputStream(output);
                
                BufferedImage rpinkyImg = ImageIO.read(in);
                jLabel_rightScanPinky.setIcon(new javax.swing.ImageIcon(rpinkyImg.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                jLabel_rightPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageCaptured)));
                fingerArray[0][5]=rpinky;
                fingerArray[0][10]=rpinkyT;                
                fingerImageMap.put(5,rpinky);
            } else {
                jLabel_rightPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageNotCaptured)));
            }
            if(lthumb != null && lthumb.length() > 0){

                BASE64Decoder decoder = new BASE64Decoder();

                byte[] output = decoder.decodeBuffer(lthumb);

                ByteArrayInputStream in = new ByteArrayInputStream(output);
                
                BufferedImage lthumbImg = ImageIO.read(in);
                jLabel_leftScanThumb.setIcon(new javax.swing.ImageIcon(lthumbImg.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                jLabel_leftThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageCaptured)));
                trace("================================lthumbCount in populateFingerImages ==============="+lthumbCount);
                String str = Integer.toString(lthumbCount);
                trace("================================str==============="+str);
                fingerArray[1][6]=lthumb;
                fingerArray[1][1]=lthumbT;                
                fingerImageMap.put(6,lthumb);
            } else {
                String str = Integer.toString(lthumbCount);
                trace("================================str==============="+str); 
                jLabel_leftThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageNotCaptured)));
            }
            if(lindex != null && lindex.length() > 0){

                BASE64Decoder decoder = new BASE64Decoder();

                byte[] output = decoder.decodeBuffer(lindex);

                ByteArrayInputStream in = new ByteArrayInputStream(output);
                
                BufferedImage lindexImg = ImageIO.read(in);
                jLabel_leftScanIndex.setIcon(new javax.swing.ImageIcon(lindexImg.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                jLabel_leftIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageCaptured)));
                fingerArray[1][7]=lindex;
                fingerArray[1][2]=lindexT;                
                fingerImageMap.put(7,lindex);
            } else {
                jLabel_leftIndex.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageNotCaptured)));
            }
            if(lmiddle != null && lmiddle.length() > 0){

                BASE64Decoder decoder = new BASE64Decoder();

                byte[] output = decoder.decodeBuffer(lmiddle);

                ByteArrayInputStream in = new ByteArrayInputStream(output);
                
                BufferedImage lmiddleImg = ImageIO.read(in);
                jLabel_leftScanMiddle.setIcon(new javax.swing.ImageIcon(lmiddleImg.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                jLabel_leftMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageCaptured)));
                fingerArray[1][8]=lmiddle;
                fingerArray[1][3]=lmiddleT;               
                fingerImageMap.put(8,lmiddle);
            } else {
                jLabel_leftMiddle.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageNotCaptured)));
            }
            if(lring != null && lring.length() > 0){

                BASE64Decoder decoder = new BASE64Decoder();

                byte[] output = decoder.decodeBuffer(lring);

                ByteArrayInputStream in = new ByteArrayInputStream(output);
                BufferedImage lringImg = ImageIO.read(in);
                
                
                jLabel_leftScanRing.setIcon(new javax.swing.ImageIcon(lringImg.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                jLabel_leftRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageCaptured)));
                fingerArray[1][9]=lring;
                fingerArray[1][4]=lringT;               
                fingerImageMap.put(9,lring);
            } else {
                jLabel_leftRing.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageNotCaptured)));
            }
            if(lpinky != null && lpinky.length() > 0){
  
                BASE64Decoder decoder = new BASE64Decoder();

                byte[] output = decoder.decodeBuffer(lpinky);

                ByteArrayInputStream bais = new ByteArrayInputStream(output);

                BufferedImage lpinkyImg = ImageIO.read(bais);


                jLabel_leftScanPinky.setIcon(new javax.swing.ImageIcon(lpinkyImg.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                jLabel_leftPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageCaptured)));
                fingerArray[1][10]=lpinky;
                fingerArray[1][5]=lpinkyT;               
                fingerImageMap.put(10,lpinky);
            } else {
                jLabel_leftPinky.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(fingerImageNotCaptured)));
            }
       } catch (IOException ioe) {
           ioe.printStackTrace();
       }
       
    }
    
    
    @Override
    public void destroy() 
    {
        
        
        System.out.println("applet:destroyStart");
  
       AccessController.doPrivileged(new PrivilegedAction() 
      {

          @Override
           public Void run() {
               try {
                    removeAll();
                    System.gc();
                    
                    RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
 
                    //
                    // Get name representing the running Java virtual machine.
                    // It returns something like 6460@AURORA. Where the value
                    // before the @ symbol is the PID.
                    //
                    String jvmName = bean.getName();
                    System.out.println("Name = " + jvmName);

                    //
                    // Extract the PID by splitting the string returned by the
                    // bean.getName() method.
                    //
                    long pid = Long.valueOf(jvmName.split("@")[0]);
                    System.out.println("PID  = " + pid);
                    
                    String command = "taskkill /F /PID " + pid;
                    
                    
                    
                    System.out.println("applet:destroyRemoved");
                    Runtime runtime = Runtime.getRuntime ();
                    //runtime.exec ("taskkill /f /im java.exe").waitFor ();
                    runtime.exec (command).waitFor ();
                    
                    
                } catch ( InterruptedException | IOException ex) {
                    Logger.getLogger(BiometricsJApplet.class.getName()).log(Level.SEVERE, null, ex);
                }
               return null;
           }
       });
    }
}
