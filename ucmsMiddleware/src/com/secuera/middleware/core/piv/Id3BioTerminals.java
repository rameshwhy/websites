/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.id3.fingerprint.FingerCapture;
import com.id3.fingerprint.FingerCaptureDevice;
import com.id3.fingerprint.FingerCaptureListener;
import com.id3.fingerprint.FingerCaptureStatus;
import com.id3.fingerprint.FingerException;
import com.id3.fingerprint.FingerImage;
import com.id3.fingerprint.FingerLicense;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Harpreet Singh
 */


public class Id3BioTerminals implements FingerCaptureListener {

    FingerCapture capture;
    
    boolean stopCapture = false;
    boolean deviceCapture = false;
  
    /*Define Device Handle and Finger Type   */
    long DeviceHandle;
    int FingerType;
  

    public Id3BioTerminals() {
        try {
            FingerLicense.checkLicense();

            System.out.print("License is valid\n");
            System.out.print(" - path : " + FingerLicense.getLicensePath() + "\n");
            System.out.print(" - owner : " + FingerLicense.getLicenseOwner() + "\n");

        } catch (FingerException ex) {
            Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            System.out.print("\nAn error has occured !");
            // TODO Auto-generated catch block
            ex.printStackTrace();
        }

    }

    public List<String> getFGTerminals() {
        FingerCaptureDevice[] devices;
        List deviceList = new ArrayList<String>();

        try {
            System.out.print("\nGetting List of capture devices...\n");
            devices = getFPDevice();


            int device_cnt = capture.getDeviceCount();
            for (int i = 0; i < device_cnt; i++) {
                deviceList.add(devices[i].getName());
            }



        } catch (FingerException e) {
            System.out.print("\nAn error has occured !");

            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        capture.dispose();
        return deviceList;

    }

    public FingerCaptureDevice[] getFPDevice() {
        FingerCaptureDevice[] devices = null;

        try {
            System.out.print("\nGetting List of capture devices...\n");

            capture = new FingerCapture(this, true);

            while (!deviceCapture) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            devices = capture.getDevices();




        } catch (FingerException e) {
            System.out.print("\nAn error has occured !");

            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return devices;

    }


    @Override
    public void deviceAdded(FingerCapture sender, FingerCaptureDevice device) {
        try {
            if (device != null) {
                deviceCapture = true;
                System.out.println("In Device added " + device.getName() + " : ");


            }

        } catch (FingerException e) {
        }
    }

    @Override
    public void deviceRemoved(FingerCapture sender, String deviceName) {
        System.out.print(deviceName + " : Device removed.\n");
    }
    static String[] statusMessage = {
        "Capture stopped.",
        "Place finger",
        "Remove finger",
        "Roll your finger",
        "Finger placed",
        "Finger removed",
        "Finger significantly displaced to the down border",
        "Finger goes out on the top border",
        "Finger goes out on the right border",
        "Finger goes out on the left border",
        "Press finger harder",
        "The finger swipe was too fast",
        "The finger swipe was too slow",
        "The finger swipe was too skewed",
        "The finger was swiped with inapropriate speed",
        "The captured image is too small",
        "The captured image is of poor quality",
        "A part of the finger is lifted",
        "Rolling speed is too fast",
        "The sensor surface is dirty, or more than one finger is detected",
        "The rolling finger is shifted",
        "Rolling time is too short",
        "Fewer fingers detected",
        "Too many fingers detected",
        "Wrong hand",};

    @Override
    public void statusChanged(FingerCapture sender, FingerCaptureDevice device, int status) {
        System.out.print("\n- " + statusMessage[status] + " - \n");

        if (status == FingerCaptureStatus.Stopped) {
            stopCapture = true;
        }
    }

    @Override
    public void imagePreview(FingerCapture sender, FingerCaptureDevice device,
            FingerImage image) {
    }

    @Override
    public void imageCaptured(FingerCapture sender, FingerCaptureDevice device,
            FingerImage image) {
        try {
            if (device != null) {
                System.out.print(device.getName() + " : ");
            }

            System.out.print("Image captured.\n");

            if (image.getSegmentCount() > 0) {
                System.out.printf("   - %d finger(s) detected.\n", image.getSegmentCount());
            }

        } catch (FingerException e) {
        }



    }

    @Override
    public void imageProcessed(FingerCapture sender,
            FingerCaptureDevice device, FingerImage image) {
        try {
            if (device != null) {
                System.out.print(device.getName() + " : ");
            }

            System.out.print("Image processed.\n");

            // display image information
            displayImageInformation(image);


        } catch (FingerException e) {
            e.printStackTrace();
        }





    }

    private void displayImageInformation(FingerImage image) throws FingerException {
        System.out.print("   - capture device vendor : " + image.getCaptureDeviceVendor() + "\n");
        System.out.print("   - image width : " + image.getWidth() + " pixels\n");
        System.out.print("   - image height : " + image.getHeight() + " pixels\n");
        System.out.print("   - impression type : " + image.getImpressionType() + "\n");
        System.out.print("   - Horizontal res. : " + image.getHorizontalResolution() + " dpi\n");
        System.out.print("   - Vertical res. : " + image.getVerticalResolution() + " dpi\n");
        System.out.print("   - NFIQ : " + image.getQuality() + "\n");
    }


}
