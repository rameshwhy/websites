package com.id3.fingerprint;

public class FingerImageScaleUnit
{
  public static final int PixelsPerInch = 1;
  public static final int PixelsPerCentimeter = 2;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerImageScaleUnit
 * JD-Core Version:    0.6.2
 */