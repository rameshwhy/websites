package eu.gnome.morena;

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

import eu.gnome.morena.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class SynchronousHelper
{
    static class FileTransferHandler
        implements TransferDoneListener
    {

        public void transferDone(File file)
        {
            imageFile = file;
            notifyRequestor();
        }

        public void transferFailed(int i, String s)
        {
            code = i;
            error = s;
            notifyRequestor();
        }

        private synchronized void notifyRequestor()
        {
            transferDone = true;
            notify();
        }

        File imageFile;
        int code;
        String error;
        boolean transferDone;

        FileTransferHandler()
        {
            transferDone = false;
        }
    }

    static class ImageTransferHandler
        implements TransferDoneListener
    {

        public void transferDone(File file)
        {
            if(file != null)
                try
                {
                    image = ImageIO.read(file);
                }
                catch(IOException ioexception)
                {
                    error = ioexception.getLocalizedMessage();
                }
            notifyRequestor();
        }

        public void transferFailed(int i, String s)
        {
            code = i;
            error = s;
            notifyRequestor();
        }

        private synchronized void notifyRequestor()
        {
            transferDone = true;
            notify();
        }

        BufferedImage image;
        int code;
        String error;
        boolean transferDone;

        ImageTransferHandler()
        {
            transferDone = false;
        }
    }


    public SynchronousHelper()
    {
    }

    public static BufferedImage scanImage(Device device)
        throws Exception
    {
        return scanImage(device, 0);
    }

    public static BufferedImage scanImage(Device device, int i)
        throws Exception
    {
        ImageTransferHandler imagetransferhandler = new ImageTransferHandler();
        synchronized(imagetransferhandler)
        {
            ((DeviceBase)device).startTransfer(imagetransferhandler, i);
            while(!imagetransferhandler.transferDone) 
                imagetransferhandler.wait();
        }
        if(imagetransferhandler.image != null)
            return imagetransferhandler.image;
        else
            throw new Exception(imagetransferhandler.error);
    }

    public static File scanFile(Device device)
        throws Exception
    {
        return scanFile(device, 0);
    }

    public static File scanFile(Device device, int i)
        throws Exception
    {
        FileTransferHandler filetransferhandler = new FileTransferHandler();
        synchronized(filetransferhandler)
        {
            ((DeviceBase)device).startTransfer(filetransferhandler, i);
            while(!filetransferhandler.transferDone) 
                filetransferhandler.wait();
        }
        if(filetransferhandler.imageFile != null)
            return filetransferhandler.imageFile;
        else
            throw new Exception(filetransferhandler.error);
    }

    public static final int WIA_ERROR_PAPER_EMPTY = 417;
}
