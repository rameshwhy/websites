/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.security.PrivateKey;
import java.security.PublicKey;
import javax.smartcardio.CardChannel;

/**
 * @author Harpreet Singh
 */
public class GenContentCert {

    public byte[] genCert(String str_cn, String str_ou,
            String str_org, String str_city, String str_state,
            String str_country,String str_email,int[] cert_template_oid,int majorVer,int minorVer,
            PublicKey pubkey,PrivateKey privkey,
            String caType,String inURL) throws Exception {

        String certCSR;
        byte[] certficate=null;
        //generate CSR
        GenContentCSR generateCSR = new GenContentCSR();
        
        /*
        certCSR = generateCSR.getCSR(str_cn, str_ou, str_org, str_city, str_state,
                            str_country, str_email, cert_template_oid, majorVer, minorVer, 
                            pubkey, privkey);
        */
        //System.out.println(util.arrayToHex(certficate));
        //get microsoft certificate    
       /* if ("MS".equals(caType)) {
            certficate = getMScertificate(certCSR,inURL);
        }
        
        CommonUtil util = new CommonUtil();
        System.out.println(util.arrayToHex(certficate)); 
        return certficate;
        */
        //load certificate on card
        //LoadCertificate loadCertificate = new LoadCertificate();
        //loadCertificate.loadCert(channel,PRE_PERSO_PIV_ADMIN_KEY,PIV_ADMIN_KEY,certficate,cert_type);
        
          return certficate;

    }

    private byte[] getMScertificate(String certCSR,String inURL) throws MiddleWareException {
  
        byte[] certficate= null;
        //connect to microsoft 
        try {
            MicrosoftCertServer msCASrv = new MicrosoftCertServer();


            // Download the new certificate
            String approvalID = msCASrv.HttpGetApprovalID(certCSR,inURL);
            // Create the Certificate in .der format and get it in .pem format
            certficate = msCASrv.HttpGetUserCert(approvalID,inURL);


        } catch (Exception ex) {
            // TODO Auto-generated catch block
            throw new MiddleWareException(ex.getMessage());
        }


        return certficate;

    }
}
