/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;
import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.RsaEncrypter;
import com.secuera.middleware.core.common.CommonUtil;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.smartcardio.CardChannel;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;

/**
 *
 * @author admin
 */
public class PivCrypto {

	SendCommand sendCard = new SendCommand();
	CommonUtil util = new CommonUtil();
	GeneralAuth auth = new GeneralAuth();
	LoadCertificate rc= new LoadCertificate();
	ExceptionMessages expMsg = new ExceptionMessages();
	
	
	
	public String encrypt(CardChannel channel, String str_algo,String str_key_ref,String str_data) 
	throws MiddleWareException {
		
		String ls_result = null;
		
		/*First Step--> Get the certificate and parse the public key out of it*/
		
		byte[] cert_bytes=null; //sat123 =rc.readCert(channel,'K') ;
		
		
		
		CertificateFactory certFactory = null;
		try {
			 certFactory = CertificateFactory.getInstance("X.509");
		} catch (CertificateException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		InputStream in = new ByteArrayInputStream(cert_bytes);
		X509Certificate certX509 = null;
		
		try {
			certX509= (X509Certificate)certFactory.generateCertificate(in);
		} catch (CertificateException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			
		
	/*	X509Certificate certX509 = null;
		GenDigitalSignature gen= new GenDigitalSignature();
		try {
			certX509=gen.getX509();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		
		PublicKey publicKey;
		publicKey=certX509.getPublicKey();
		
		
		System.out.println("cert reading "+ certX509);
		
		
		
		// Get the bytes from the pLain text.
		byte[] MesageBytes = str_data.getBytes();
		
		// Perform the RSA Encryption.
			byte[] result = null;
			
		  RsaEncrypter rsa = new RsaEncrypter();
	      try {
			result=rsa.encrypt(publicKey,MesageBytes);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      
		ls_result= new String (result);
		return ls_result;
		
	}
	
	
	
	public String decrypt(CardChannel channel, String str_algo,String str_key_ref,String str_data) 
	throws MiddleWareException {
	
		String ls_result;
		byte[] result = null;
		
		
		
		
		
		ls_result= new String (result);
		return ls_result;
		
	}
	
	
	
	
	
}
