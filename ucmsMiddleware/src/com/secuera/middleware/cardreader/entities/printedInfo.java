package com.secuera.middleware.cardreader.entities;

import java.util.Date;

public class printedInfo {

	String name;
	String affiliation;
	Date expirationDate;
	String cardSerialNumber;
	String issuerID;
	String organisationLine1;
	String organisationLine2;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAffiliation() {
		return affiliation;
	}
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getCardSerialNumber() {
		return cardSerialNumber;
	}
	public void setCardSerialNumber(String cardSerialNumber) {
		this.cardSerialNumber = cardSerialNumber;
	}
	public String getIssuerID() {
		return issuerID;
	}
	public void setIssuerID(String issuerID) {
		this.issuerID = issuerID;
	}
	public String getOrganisationLine1() {
		return organisationLine1;
	}
	public void setOrganisationLine1(String organisationLine1) {
		this.organisationLine1 = organisationLine1;
	}
	public String getOrganisationLine2() {
		return organisationLine2;
	}
	public void setOrganisationLine2(String organisationLine2) {
		this.organisationLine2 = organisationLine2;
	}
	
}
