package com.secuera.middleware.core.nxp;

import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

import com.secuera.middleware.core.common.*;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.core.common.SendCommand;

public class ManageMRZData {
	
	 SendCommand sendCard = new SendCommand();
	 CommonUtil cmnUtil = new CommonUtil();
     StringBuffer sb = new StringBuffer();
	private String ErrorMsg;
	
     // Updating data on to the card
	public  boolean updateBinaryData(CardChannel channel , byte[] MRZData) throws MiddleWareException
    {
    	// 00D68100 5F 83Update command
    	// 61 5D  Template for MRZ Data Group EF.DG1
    	// 5F1F 5A The MRZ data object as a composite Data Element. 5A hex is 90 decimal ie 180 length of data 
    	// 493C443C3C414243443132333435393C3C3C3C3C3C3C3C3C3C3C3C3C3C3C383030383139344632303036333035443C3C3C3C3C3C3C3C3C3C3C3C3C30434F534D4F504F4C4954414E3C3C434C414952453C434543494C453C3C3C" Data
   	
         byte[] res = null;
         int data_length = MRZData.length;
         String actual_data_len = Integer.toString(data_length, 16);
         
         int efdg_tag_len = data_length+ 3;
         String efdg1_tag_len_hex = Integer.toString(efdg_tag_len, 16);
         
         int apdu_key_len = efdg_tag_len + 2; 
         String apdu_key_len_hex = Integer.toString(apdu_key_len, 16);
         
        
         byte[] apdu_key = {(byte)0x00 , (byte)0xD6, (byte)0x81, (byte)0x00};
         byte[] apdu_key_len_byte = cmnUtil.hex1StringToByteArray(apdu_key_len_hex);
          //we have to calculate CLS 00 INS D6 P1-P2 81 (10000001)
         /*If p1-p2 is 0000 it indicates current file: 
          *If you ant to refer to another Binary file file use respective short file identifier(SFI) then calculate p1 value as stated below 
         *If bit 8 is set to one in parameter P1, then bits 7 and 6 of P1 are set to 00 (RFU bits), bits 5 to 1 of P1 code a
         *short EF identifier and bits 8 to 1 of parameter P2 code the offset from zero to 255 (eight bits).
         **/
         
         byte[] efdg1_tag = {(byte)0x61};
         byte[] efdg1_tag_len = cmnUtil.hex1StringToByteArray(efdg1_tag_len_hex);
         byte[] mrz_comp_data = {(byte)0X5F, (byte)0x1F};
         byte[] mrz_comp_data_len = cmnUtil.hex1StringToByteArray(actual_data_len);
         //later we should calculate lenght value that should replace the 5A in above command 
         byte[] data_hex = MRZData;
         
         byte[][] enc_arrays = new byte[7][];
         enc_arrays[0] = apdu_key;
         enc_arrays[1] = apdu_key_len_byte;
         enc_arrays[2] = efdg1_tag;
         enc_arrays[3] = efdg1_tag_len;
         enc_arrays[4] = mrz_comp_data;
         enc_arrays[5] = mrz_comp_data_len;
         enc_arrays[6] = data_hex;
 
       

         byte[] enc = cmnUtil.combine_data(enc_arrays);
         System.out.println(":::::::::::::::::::::MRZDATA::::::::::::: "+cmnUtil.arrayToHex(enc));
        
         CommandAPDU ENC_APDU = new CommandAPDU(enc);

         try {
             sendCard.sendCommand(channel, ENC_APDU, sb);
         } catch (CardException e) {
             throw new MiddleWareException(e.getMessage());
         }

         if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
             ErrorMsg = "Unable to update the binary data";
             errMsg.append(ErrorMsg).append("  ");
             errMsg.append(" (").append(sb.toString()).append(")");
             throw new MiddleWareException(errMsg.toString());

         } else {
             return true;
         }	
    }

  
}
