/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.beans;

import com.secuera.middleware.cardreader.core.CommonUtil;

/**
 *
 * @author SEAdmin
 */
public class CredentialKeys {
    private byte[] encKey;
    private byte[] macKey;
    private byte[] dekKey;
    
    private byte[] masterKey;  
    private byte[] masterKeyAES;
    private byte[] adminKey;
    
    private String diversifyGPKeys;
    private String diversifyAdminKey;
    
    private String gpKeysAlgorithmType;
    private String adminKeyAlgorithmType;
    
    //private boolean useGPKeys; 
    //private byte[] defaultAdminKey;
    
    private byte[] piv9E08;
    
    private byte[] cardManagerAid;
            
    CommonUtil cmnUtil = new CommonUtil();

    public byte[] getCardManagerAid() {
        return cardManagerAid;
    }

    public void setCardManagerAid(String cardManagerAid) {
        this.cardManagerAid = cmnUtil.hex1ToByteArray(cardManagerAid);
    }

    public byte[] getDekKey() {
        return dekKey;
    }

    public void setDekKey(String dekKey) {
        this.dekKey = cmnUtil.hex1ToByteArray(dekKey);
    }

    public byte[] getEncKey() {
        return encKey;
    }

    public void setEncKey(String encKey) {
        this.encKey = cmnUtil.hex1ToByteArray(encKey);
    }

    public byte[] getMacKey() {
        return macKey;
    }

    public void setMacKey(String macKey) {
        this.macKey = cmnUtil.hex1ToByteArray(macKey);
    }

    public byte[] getMasterKey() {
        return masterKey;
    }

    public void setMasterKey(String masterKey) {
        this.masterKey = cmnUtil.hex1ToByteArray(masterKey);
    }

    public byte[] getMasterKeyAES() {
        return masterKeyAES;
    }

    public void setMasterKeyAES(String masterKeyAES) {
        this.masterKeyAES = cmnUtil.hex1ToByteArray(masterKeyAES);
    }

    public byte[] getPiv9E08() {
        return piv9E08;
    }

    public void setPiv9E08(byte[] piv9E08) {
        this.piv9E08 = piv9E08;
    }

    

    

    public byte[] getAdminKey() {
        return adminKey;
    }

    public void setAdminKey(String adminKey) {
        this.adminKey = cmnUtil.hex1ToByteArray(adminKey);
    }

    /*
    public byte[] getDiversifiedAdminKey() {
        return diversifiedAdminKey;
    }

    public void setDiversifiedAdminKey(byte[] diversifiedAdminKey) {
        this.diversifiedAdminKey = diversifiedAdminKey;
    }
     
    public byte[] getDefaultAdminKey() {
        return defaultAdminKey;
    }

    public void setDefaultAdminKey(String defaultAdminKey) {
        this.defaultAdminKey = cmnUtil.hex1ToByteArray(defaultAdminKey);
    }
    */ 
    public String getDiversifyGPKeys() {
        return diversifyGPKeys;
    }

    public void setDiversifyGPKeys(String diversifyGPKeys) {
        this.diversifyGPKeys = diversifyGPKeys;
    }
    /*
    public boolean isUseGPKeys() {
        return useGPKeys;
    }

    public void setUseGPKeys(boolean useGPKeys) {
        this.useGPKeys = useGPKeys;
    }

    public byte[] getDefaultAdminKey() {
        return defaultAdminKey;
    }

    public void setDefaultAdminKey(byte[] defaultAdminKey) {
        this.defaultAdminKey = defaultAdminKey;
    }
     */ 
    public String getDiversifyAdminKey() {
        return diversifyAdminKey;
    }

    public void setDiversifyAdminKey(String diversifyAdminKey) {
        this.diversifyAdminKey = diversifyAdminKey;
    }

    public String getAdminKeyAlgorithmType() {
        return adminKeyAlgorithmType;
    }

    public void setAdminKeyAlgorithmType(String adminKeyAlgorithmType) {
        this.adminKeyAlgorithmType = adminKeyAlgorithmType;
    }

    public String getGpKeysAlgorithmType() {
        return gpKeysAlgorithmType;
    }

    public void setGpKeysAlgorithmType(String gpKeysAlgorithmType) {
        this.gpKeysAlgorithmType = gpKeysAlgorithmType;
    }

    
    
   
    
    
}
