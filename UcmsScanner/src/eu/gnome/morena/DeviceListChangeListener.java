// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena;


// Referenced classes of package eu.gnome.morena:
//            Device

public interface DeviceListChangeListener
{

    /**
     * @deprecated Method listChanged is deprecated
     */

    public abstract void listChanged();

    public abstract void deviceConnected(Device device);

    public abstract void deviceDisconnected(Device device);
}
