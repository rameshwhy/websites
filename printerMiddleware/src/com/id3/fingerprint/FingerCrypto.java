/*     */ package com.id3.fingerprint;

import java.io.File;

/*     */ 
/*     */ public class FingerCrypto
/*     */ {
/*     */   private long _handle;
/*  16 */   private boolean _disposed = false;
/*     */ 
/*     */   static
/*     */   {
/* 403 */     String tmpStr = "id3Finger.dll";
                File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
                File localFile = new File(TMP_DIR, tmpStr);


                 System.load(localFile.getAbsolutePath());
/*     */   }
/*     */ 
/*     */   public FingerCrypto()
/*     */     throws FingerException
/*     */   {
/*  24 */     long[] handle_ptr = new long[1];
/*  25 */     int result = nInitialize(handle_ptr);
/*     */ 
/*  27 */     if (result != 0) {
/*  28 */       throw new FingerException(result);
/*     */     }
/*  30 */     this._handle = handle_ptr[0];
/*     */   }
/*     */ 
/*     */   public FingerCrypto(byte[] passphrase, int size)
/*     */     throws FingerException
/*     */   {
/*  41 */     long[] handle_ptr = new long[1];
/*  42 */     int result = nInitializeEx(handle_ptr, passphrase, size);
/*     */ 
/*  44 */     if (result != 0) {
/*  45 */       throw new FingerException(result);
/*     */     }
/*  47 */     this._handle = handle_ptr[0];
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/*  55 */     nDispose(this._handle);
/*  56 */     this._handle = 0L;
/*  57 */     this._disposed = true;
/*     */   }
/*     */ 
/*     */   protected void finalize()
/*     */     throws Throwable
/*     */   {
/*     */     try
/*     */     {
/*  67 */       dispose();
/*     */     }
/*     */     catch (Exception localException)
/*     */     {
/*     */     }
/*     */     finally
/*     */     {
/*  74 */       super.finalize();
/*     */     }
/*     */   }
/*     */ 
/*     */   public byte[] computeHash(int algorithm, byte[] data)
/*     */     throws FingerException
/*     */   {
/*  87 */     byte[] hashCode = new byte[16];
/*  88 */     int result = nComputeHash(this._handle, algorithm, data, hashCode);
/*     */ 
/*  90 */     if (result != 0) {
/*  91 */       throw new FingerException(result);
/*     */     }
/*  93 */     return hashCode;
/*     */   }
/*     */ 
/*     */   public byte[] generateRandom(int size)
/*     */     throws FingerException
/*     */   {
/* 104 */     byte[] random = new byte[size];
/* 105 */     int result = nGenerateRandom(this._handle, random);
/*     */ 
/* 107 */     if (result != 0) {
/* 108 */       throw new FingerException(result);
/*     */     }
/* 110 */     return random;
/*     */   }
/*     */ 
/*     */   public void generatePrivateKey(int size)
/*     */     throws FingerException
/*     */   {
/* 120 */     int result = nGeneratePrivateKey(this._handle, size);
/*     */ 
/* 122 */     if (result != 0)
/* 123 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public void setKey(byte[] data)
/*     */     throws FingerException
/*     */   {
/* 133 */     int result = nSetKey(this._handle, data);
/*     */ 
/* 135 */     if (result != 0)
/* 136 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public void setKeyWithIV(byte[] data, byte[] iv)
/*     */     throws FingerException
/*     */   {
/* 147 */     int result = nSetKeyWithIV(this._handle, data, iv);
/*     */ 
/* 149 */     if (result != 0)
/* 150 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public void loadPublicKey(String filename)
/*     */     throws FingerException
/*     */   {
/* 160 */     int result = nLoadPublicKey(this._handle, filename);
/*     */ 
/* 162 */     if (result != 0)
/* 163 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public void loadPrivateKey(String filename, String password)
/*     */     throws FingerException
/*     */   {
/* 174 */     int result = nLoadPrivateKey(this._handle, filename, password);
/*     */ 
/* 176 */     if (result != 0)
/* 177 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public void savePublicKey(String filename)
/*     */     throws FingerException
/*     */   {
/* 187 */     int result = nSavePublicKey(this._handle, filename);
/*     */ 
/* 189 */     if (result != 0)
/* 190 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public void savePrivateKey(String filename, String password)
/*     */     throws FingerException
/*     */   {
/* 201 */     int result = nSavePrivateKey(this._handle, filename, password);
/*     */ 
/* 203 */     if (result != 0)
/* 204 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public byte[] getPublicKey()
/*     */     throws FingerException
/*     */   {
/* 214 */     int[] result = new int[1];
/* 215 */     byte[] output = nGetPublicKey(this._handle, result);
/*     */ 
/* 217 */     if (result[0] != 0) {
/* 218 */       throw new FingerException(result[0]);
/*     */     }
/* 220 */     return output;
/*     */   }
/*     */ 
/*     */   public void setPublicKey(byte[] key)
/*     */     throws FingerException
/*     */   {
/* 230 */     int result = nSetPublicKey(this._handle, key);
/*     */ 
/* 232 */     if (result != 0)
/* 233 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public byte[] getPrivateKey()
/*     */     throws FingerException
/*     */   {
/* 243 */     int[] result = new int[1];
/* 244 */     byte[] output = nGetPrivateKey(this._handle, result);
/*     */ 
/* 246 */     if (result[0] != 0) {
/* 247 */       throw new FingerException(result[0]);
/*     */     }
/* 249 */     return output;
/*     */   }
/*     */ 
/*     */   public void setPrivateKey(byte[] key)
/*     */     throws FingerException
/*     */   {
/* 259 */     int result = nSetPrivateKey(this._handle, key);
/*     */ 
/* 261 */     if (result != 0)
/* 262 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public int getEncryptionMode()
/*     */     throws FingerException
/*     */   {
/* 273 */     int[] mode = new int[1];
/* 274 */     int result = nGetEncryptionMode(this._handle, mode);
/*     */ 
/* 276 */     if (result != 0) {
/* 277 */       throw new FingerException(result);
/*     */     }
/* 279 */     return mode[0];
/*     */   }
/*     */ 
/*     */   public void setEncryptionMode(int mode)
/*     */     throws FingerException
/*     */   {
/* 290 */     int result = nSetEncryptionMode(this._handle, mode);
/*     */ 
/* 292 */     if (result != 0)
/* 293 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public byte[] encrypt(byte[] data)
/*     */     throws FingerException
/*     */   {
/* 304 */     int[] result = new int[1];
/* 305 */     byte[] output = nEncrypt(this._handle, data, result);
/*     */ 
/* 307 */     if (result[0] != 0) {
/* 308 */       throw new FingerException(result[0]);
/*     */     }
/* 310 */     return output;
/*     */   }
/*     */ 
/*     */   public byte[] encrypt(int algorithm, byte[] data)
/*     */     throws FingerException
/*     */   {
/* 322 */     int[] result = new int[1];
/* 323 */     byte[] output = nEncryptEx(this._handle, algorithm, data, result);
/*     */ 
/* 325 */     if (result[0] != 0) {
/* 326 */       throw new FingerException(result[0]);
/*     */     }
/* 328 */     return output;
/*     */   }
/*     */ 
/*     */   public byte[] decrypt(byte[] data)
/*     */     throws FingerException
/*     */   {
/* 339 */     int[] result = new int[1];
/* 340 */     byte[] output = nDecrypt(this._handle, data, result);
/*     */ 
/* 342 */     if (result[0] != 0) {
/* 343 */       throw new FingerException(result[0]);
/*     */     }
/* 345 */     return output;
/*     */   }
/*     */ 
/*     */   public byte[] decrypt(int algorithm, byte[] data)
/*     */     throws FingerException
/*     */   {
/* 357 */     int[] result = new int[1];
/* 358 */     byte[] output = nDecryptEx(this._handle, algorithm, data, result);
/*     */ 
/* 360 */     if (result[0] != 0) {
/* 361 */       throw new FingerException(result[0]);
/*     */     }
/* 363 */     return output;
/*     */   }
/*     */ 
/*     */   public byte[] sign(byte[] data)
/*     */     throws FingerException
/*     */   {
/* 374 */     int[] result = new int[1];
/* 375 */     byte[] output = nSign(this._handle, data, result);
/*     */ 
/* 377 */     if (result[0] != 0) {
/* 378 */       throw new FingerException(result[0]);
/*     */     }
/* 380 */     return output;
/*     */   }
/*     */ 
/*     */   public boolean verify(byte[] data)
/*     */     throws FingerException
/*     */   {
/* 391 */     int result = nVerify(this._handle, data);
/*     */ 
/* 393 */     if (result == -3000) {
/* 394 */       return false;
/*     */     }
/* 396 */     if (result != 0) {
/* 397 */       throw new FingerException(result);
/*     */     }
/* 399 */     return true;
/*     */   }
/*     */ 
/*     */   private static final native int nInitialize(long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nInitializeEx(long[] paramArrayOfLong, byte[] paramArrayOfByte, int paramInt);
/*     */ 
/*     */   private static final native int nDispose(long paramLong);
/*     */ 
/*     */   private static final native int nComputeHash(long paramLong, int paramInt, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2);
/*     */ 
/*     */   private static final native int nGenerateRandom(long paramLong, byte[] paramArrayOfByte);
/*     */ 
/*     */   private static final native int nGeneratePrivateKey(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nSetKey(long paramLong, byte[] paramArrayOfByte);
/*     */ 
/*     */   private static final native int nSetKeyWithIV(long paramLong, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2);
/*     */ 
/*     */   private static final native int nLoadPublicKey(long paramLong, String paramString);
/*     */ 
/*     */   private static final native int nLoadPrivateKey(long paramLong, String paramString1, String paramString2);
/*     */ 
/*     */   private static final native int nSavePublicKey(long paramLong, String paramString);
/*     */ 
/*     */   private static final native int nSavePrivateKey(long paramLong, String paramString1, String paramString2);
/*     */ 
/*     */   private static final native byte[] nGetPublicKey(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native byte[] nGetPrivateKey(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nSetPublicKey(long paramLong, byte[] paramArrayOfByte);
/*     */ 
/*     */   private static final native int nSetPrivateKey(long paramLong, byte[] paramArrayOfByte);
/*     */ 
/*     */   private static final native byte[] nEncrypt(long paramLong, byte[] paramArrayOfByte, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native byte[] nEncryptEx(long paramLong, int paramInt, byte[] paramArrayOfByte, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native byte[] nDecrypt(long paramLong, byte[] paramArrayOfByte, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native byte[] nDecryptEx(long paramLong, int paramInt, byte[] paramArrayOfByte, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native byte[] nSign(long paramLong, byte[] paramArrayOfByte, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nVerify(long paramLong, byte[] paramArrayOfByte);
/*     */ 
/*     */   private static final native int nGetEncryptionMode(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nSetEncryptionMode(long paramLong, int paramInt);
/*     */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCrypto
 * JD-Core Version:    0.6.2
 */