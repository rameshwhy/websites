/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import com.secuera.middleware.core.common.CommonUtil;

/**
 *
 * @author Harpreet Singh
 */
public class MasterKeyCeremonyResultBean {
 
    public boolean algoError;
    
    public boolean masterKcvError;
    public String masterKey;
    
    public boolean kcv1Error;
    public boolean kcv2Error;
    public boolean kcv3Error;
    public boolean zckmError;

    CommonUtil cmnUtil = new CommonUtil();
    
    public boolean isMasterKcvError() {
        return masterKcvError;
    }

    public void setMasterKcvError(boolean masterKcvError) {
        this.masterKcvError = masterKcvError;
    }

    public String getMasterKey() {
        return masterKey;
    }

    public void setMasterKey(String masterKey) {
        this.masterKey = masterKey;
    }
    
        
    
    public boolean isKcv1Error() {
        return kcv1Error;
    }

    public void setKcv1Error(boolean kcv1Error) {
        this.kcv1Error = kcv1Error;
    }

    public boolean isKcv2Error() {
        return kcv2Error;
    }

    public void setKcv2Error(boolean kcv2Error) {
        this.kcv2Error = kcv2Error;
    }

    public boolean isKcv3Error() {
        return kcv3Error;
    }

    public void setKcv3Error(boolean kcv3Error) {
        this.kcv3Error = kcv3Error;
    }

    public boolean isAlgoError() {
        return algoError;
    }

    public void setAlgoError(boolean algoError) {
        this.algoError = algoError;
    }

    public boolean isZckmError() {
        return zckmError;
    }

    public void setZckmError(boolean zckmError) {
        this.zckmError = zckmError;
    }
    
    
    
    
    
}
