/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.BiometricBean;
import com.secuera.middleware.cardreader.beans.CardInfoBean;
import com.secuera.middleware.cardreader.beans.CSRBean;
import com.secuera.middleware.cardreader.beans.CertificateBean;
import com.secuera.middleware.cardreader.beans.PrintedInfoBean;
import com.secuera.middleware.cardreader.beans.SecurityBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import com.secuera.middleware.cardreader.exceptions.LoggerWrapper;
import java.text.Format;
import java.text.SimpleDateFormat;
import javax.smartcardio.CardChannel;
import com.secuera.middleware.cardreader.beans.ChuidBean;
import com.secuera.middleware.cardreader.beans.ContainerInfoBean;
import com.secuera.middleware.cardreader.beans.FacialBean;
import com.secuera.middleware.cardreader.beans.FacialImageBean;
import com.secuera.middleware.cardreader.beans.FascnBean;
import com.secuera.middleware.cardreader.beans.InitializationBean;
import com.secuera.middleware.cardreader.beans.PersonalizationBean;

import com.secuera.middleware.cardreader.exceptions.ExceptionUtilities;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.List;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.codec.binary.Base64;

/*
 * 
 * 
import com.secuera.beans.PrintedInfoBean;
import com.secuera.core.CardComm;
import com.secuera.core.CardUtil;
import com.secuera.core.DesEncrypter;
import com.secuera.core.PivUtil;
import com.secuera.core.CommonUtil;
import com.secuera.core.defaultInitValues;
import com.secuera.entities.MiddleWareException;
import com.secuera.beans.BiometricBean;
import com.secuera.core.VerifyFP;
import com.secuera.core.PivUtilInterface;
import com.secuera.core.CardInit;
 */
/**
 *
 * @author admin
 */
public class MiddleWareServerCardreaderImpl implements MiddleWareServerCardreader {

    private String client = "";
    //private PivUtilInterface pivUtil = null;
    private CommonUtil cmnUtil = null;
    private VerifyFP fpUtil = null;
    private PrintedInfoBean prntInfo = null;
    private defaultInitValues defvalue = null;
    private void registerThread() {
    }


    /*
     * Function : verifyLocalPin Purpose : verify local pin with card 
     * Date : 09/11/2012 
     * version : 1.0
     */
    //basic Card Access Functions
    public List<String> getTerminalsList() throws MiddleWareException {
        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = null;
        List terminalName = new ArrayList<String>();
        try {
            terminals = factory.terminals().list();
        } catch (CardException e) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.NO_CARD_TERMINAL_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }

        int term_cnt = terminals.size();
        for (int i = 0; i < term_cnt; i++) {
            CardTerminal terminal = terminals.get(i);
            terminalName.add(terminal.getName().toString());
        }



        return terminalName;
    }

    //Local PIN Management Functions
    @Override
    public boolean verifyLocalPin(CardChannel channel, String localPin) throws MiddleWareException {
        Integer pinLength;
        String retValue;

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "verifyLocalPin");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::verifyLocalPin start time: "
                    + startTime);

            PivUtilInterface pivUtil = new PivUtil();

            //-----------------------------------------------------------//
            //get and verify local pin length
            //-----------------------------------------------------------//
            retValue = pivUtil.readPinLength(channel, 'L');
            pinLength = Integer.parseInt(retValue, 16);

            if (localPin.length() > pinLength || localPin.length() == 0) {
                // stop communication & return success            
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }


            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //			
            if (pivUtil.verifyLocalPin(channel, localPin, pinLength) == false) {
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.INVALID_LOCAL_PIN_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //

            System.out.println("true");
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::verifyLocalPin total time: "
                    + totalTime);

        } catch (MiddleWareException | NumberFormatException t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;
    }

    @Override
    public boolean changeLocalPin(CardChannel channel, String oldPin, String newPin) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "changeLocalPin");

        String retValue;
        Integer pinLength;

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::changeLocalPin start time: "
                    + startTime);

            PivUtilInterface pivUtil = new PivUtil();
            //-----------------------------------------------------------//
            //get and verify local pin length
            //-----------------------------------------------------------//
            retValue = pivUtil.readPinLength(channel, 'L');
            pinLength = Integer.parseInt(retValue, 16);

            if (oldPin.length() > pinLength || oldPin.length() == 0 || newPin.length() > pinLength || newPin.length() == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }

            // ---------------------------------------------------------- //
            // Change Local PIN
            // ---------------------------------------------------------- //			
            if (pivUtil.changeLocalPin(channel, oldPin, newPin, pinLength) == false) {
                throw new MiddleWareException("Invalid Pin.");
            }

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::changeLocalPin total time: "
                    + totalTime);

        } catch (MiddleWareException | NumberFormatException t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;
    }

    @Override
    public boolean resetLocalPin(CardChannel channel, String unblockPin, String newPin) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "resetLocalPin");
        String retValue;
        Integer pinLength;
        Integer pukLength;

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::resetLocalPin start time: "
                    + startTime);

            PivUtilInterface pivUtil = new PivUtil();
            //-----------------------------------------------------------//
            //get and verify PUK length
            //-----------------------------------------------------------//
            retValue = pivUtil.readPinLength(channel, 'P');
            pukLength = Integer.parseInt(retValue, 16);

            if (unblockPin.length() > pukLength || unblockPin.length() == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }

            //-----------------------------------------------------------//
            //get and verify local pin length
            //-----------------------------------------------------------//
            retValue = pivUtil.readPinLength(channel, 'L');
            pinLength = Integer.parseInt(retValue, 16);

            if (newPin.length() > pinLength || newPin.length() == 0) {
                // stop communication & return success
                //CardConnect.stopComm();
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }

            // ---------------------------------------------------------- //
            // rest Local PIN
            // ---------------------------------------------------------- //			
            if (pivUtil.resetLockPin(channel, unblockPin, newPin, pinLength) == false) {
                // Stop communication & return Error
                //CardConnect.stopComm();
                throw new MiddleWareException("Error Resting Local PIN.");
            }

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::resetLocalPin total time: "
                    + totalTime);

        } catch (MiddleWareException | NumberFormatException t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;
    }

    //Global PIN Management Functions
    @Override
    public boolean verifyGlobalPin(CardChannel channel, String globalPin) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "verifyGlobalPin");

        String retValue;
        Integer pinLength;
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::verifyGlobalPin start time: "
                    + startTime);

            PivUtilInterface pivUtil = new PivUtil();
            //-----------------------------------------------------------//
            //get and verify local pin length
            //-----------------------------------------------------------//
            retValue = pivUtil.readPinLength(channel, 'G');
            pinLength = Integer.parseInt(retValue, 16);

            if (globalPin.length() > pinLength || globalPin.length() == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }


            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //			
            if (pivUtil.verifyGlobalPin(channel, globalPin, pinLength) == false) {
                // Stop communication & return Error
                //CardConnect.stopComm();
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.INVALID_GLOBAL_PIN_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::verifyGlobalPin total time: "
                    + totalTime);

        } catch (MiddleWareException | NumberFormatException t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;
    }

    @Override
    public boolean changeGlobalPin(CardChannel channel, String oldPin, String newPin) throws MiddleWareException {
        String retValue;
        Integer pinLength;
        LoggerWrapper.entering("MiddleWareServerImpl", "changeGlobalPin");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::changeGlobalPin start time: "
                    + startTime);

            PivUtilInterface pivUtil = new PivUtil();
            //-----------------------------------------------------------//
            //get and verify local pin length
            //-----------------------------------------------------------//
            retValue = pivUtil.readPinLength(channel, 'G');
            pinLength = Integer.parseInt(retValue, 16);

            if (oldPin.length() > pinLength || oldPin.length() == 0 || newPin.length() > pinLength || newPin.length() == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //

            if (pivUtil.changeGlobalPin(channel, oldPin, newPin, pinLength) == false) {
                // Stop communication & return Error
                //CardConnect.stopComm();
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.INVALID_GLOBAL_PIN_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::changeGlobalPin total time: "
                    + totalTime);

        } catch (MiddleWareException | NumberFormatException t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;
    }

    @Override
    public boolean resetGlobalPin(CardChannel channel, InitializationBean initvalue, String newPin) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "resetGlobalPin");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + "MiddleWareServerImpl::resetGlobalPin start time: "
                    + startTime);

            // ---------------------------------------------------------- //
            // create instance & initialize the card
            // ---------------------------------------------------------- //

            CardInit sinit = new CardInit(initvalue);

            if (sinit.initCard(channel, "G") == false) {
                throw new MiddleWareException("Error initializing PIV Card.");

            }


            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::resetGlobalPin total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;
    }

    //CHUID Functions
    @Override
    public ChuidBean readChuid(CardChannel channel) throws MiddleWareException {

        LoggerWrapper.entering("MiddleWareServerImpl", "readChuid");

        ChuidBean chuid = new ChuidBean();
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readChuid start time: "
                    + startTime);


            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            ManageCHUID mngCHUID = new ManageCHUID();

            
            chuid = mngCHUID.readChuid(channel);

            // stop communication & return success
            //CardConnect.stopComm();

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readChuid total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);

        }

        return chuid;



    }

    
    @Override
    public byte[] readFASCN(CardChannel channel) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "readFASCN");
        byte[] FASCN =null;
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readFASCN start time: "
                    + startTime);


            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            PivUtilInterface pivUtil = new PivUtil();

            FASCN = pivUtil.getFASCN(channel);

            // stop communication & return success
            //CardConnect.stopComm();

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readFASCN total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);

        }

        return FASCN;

    }
    
    
    
    @Override
    public boolean writeChuid(CardChannel channel, ChuidBean chuid, String piv_admin_key) throws MiddleWareException {

        LoggerWrapper.entering("MiddleWareServerImpl", "writeChuid");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + "MiddleWareServerImpl::writeChuid start time: "
                    + startTime);



            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            FascnBean inFascn = null;
            //inFascn = chuid.getFascn();

            /*
            // format Date in 01JAN2011
            Format formatter;
            formatter = new SimpleDateFormat("yyyyMMdd");
            String strExpDate = formatter.format(chuid.getExpirationDate()).toUpperCase();
             */
            cmnUtil = new CommonUtil();
            //convert PIV_admin_key to hex
            //byte[] key_p = cmnUtil.hex1ToByteArray(piv_admin_key);
            
            
            
            
            byte[] pre_perso_admin_key = cmnUtil.hex1ToByteArray(piv_admin_key);

            //diversify PIV_admin_key
            byte[] pivAdminKey = adminDiversify(channel, pre_perso_admin_key);

            
            ManageCHUID mngCHUID = new ManageCHUID();
            if (mngCHUID.writeChuid(channel,chuid,chuid.getSignatureDate(), pre_perso_admin_key,pivAdminKey)== false) {
                // Stop communication & return Error

                throw new MiddleWareException("Error writing CHUID.");
            }



            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::writeChuid total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;
    }

    //printed information functions
    @Override
    public PrintedInfoBean readPrintedInfo(CardChannel channel, String localPin) throws MiddleWareException {
        PrintedInfoBean printedInfo = new PrintedInfoBean();
        Integer pinLength;
        String retValue;
        Integer defLocalPinLength;


        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readPrintedInfo");
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readPrintedInfo start time: "
                    + startTime);


            PivUtilInterface pivUtil = new PivUtil();

            //get and verify local pin length            
            defLocalPinLength = Integer.parseInt(pivUtil.readPinLength(channel, 'L'), 16);


            //-----------------------------------------------------------//
            //get and verify local pin length
            //-----------------------------------------------------------//
            retValue = pivUtil.readPinLength(channel, 'L');
            pinLength = Integer.parseInt(retValue, 16);

            if (localPin.length() > pinLength || localPin.length() == 0) {
                // stop communication & return success            
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }

            //verify local PIN 
            if (pivUtil.verifyLocalPin(channel, localPin, pinLength) == false) {
                // Stop communication & return Error
                //CardConnect.stopComm();
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.INVALID_LOCAL_PIN_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }
            
            ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();
            // ---------------------------------------------------------- //
            // Read printed information
            // ---------------------------------------------------------- //			
            printedInfo = mngPrintedInfo.readPrintedinfo(channel);

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readPrintedinfo total time: "
                    + totalTime);

        } catch (MiddleWareException | NumberFormatException t) {

            throw new MiddleWareException(t.getMessage());
            //throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return printedInfo;
    }

    @Override
    public boolean writePrintedInfo(CardChannel channel, PrintedInfoBean printInfo, String piv_admin_key) throws MiddleWareException {
        boolean returnValue = false;

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "writePrintedInfo");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::writePrintedInfo start time: "
                    + startTime);



            PivUtilInterface pivUtil = new PivUtil();

            cmnUtil = new CommonUtil();
            byte[] pre_perso_admin_key = cmnUtil.hex1ToByteArray(piv_admin_key);

            //diversify PIV_admin_key
            byte[] pivAdminKey = adminDiversify(channel, pre_perso_admin_key);

            ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();

            // write printed information on card
            returnValue = mngPrintedInfo.writePrintedinfo(channel, pre_perso_admin_key, pivAdminKey, printInfo.getName(),
                    printInfo.getAffiliation(), printInfo.getExpirationDate(),
                    printInfo.getCardSerialNumber(), printInfo.getIssuerID(),
                    printInfo.getOrganisationLine1(),
                    printInfo.getOrganisationLine2());

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::WritePrinted Info total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return returnValue;






    }

    //card information functions
    @Override
    public CardInfoBean readCardInfo(CardChannel channel, String localPin) throws MiddleWareException {
        CardInfoBean cardInfo = new CardInfoBean();
        PrintedInfoBean printedInfo = new PrintedInfoBean();
        ChuidBean chuid = null;

        Integer pinLength;
        String retValue;
        String returnValue;

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readCardInfo");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readCardInfo start time: "
                    + startTime);

            PivUtilInterface pivUtil = new PivUtil();
            ManageCHUID mngCHUID = new ManageCHUID();
            //-----------------------------------------------------------//
            //get and verify local pin length
            //-----------------------------------------------------------//
            retValue = pivUtil.readPinLength(channel, 'L');
            pinLength = Integer.parseInt(retValue, 16);

            if (localPin.length() > pinLength || localPin.length() == 0) {
                // stop communication & return success            
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //			
            if (pivUtil.verifyLocalPin(channel, localPin, pinLength) == false) {
                // Stop communication & return Error
                //CardConnect.stopComm();
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.INVALID_LOCAL_PIN_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }


            chuid = mngCHUID.readChuid(channel);

            ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();
            // ---------------------------------------------------------- //
            // Read printed information
            // ---------------------------------------------------------- //			
            printedInfo = mngPrintedInfo.readPrintedinfo(channel);



            cardInfo.setChuid(chuid);
            cardInfo.setPrintedInfo(printedInfo);

            // stop communication & return success
            //CardConnect.stopComm();

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readCardInfo total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return cardInfo;
    }

    @Override
    public boolean writeCardInfo(CardChannel channel, CardInfoBean cardInfo, String piv_admin_key) throws MiddleWareException {

        boolean returnValue;

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "writeCardInfo");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::writeCardInfo start time: "
                    + startTime);


            cmnUtil = new CommonUtil();
            ManageCHUID mngCHUID = new ManageCHUID();

            byte[] pre_perso_admin_key = cmnUtil.hex1ToByteArray(piv_admin_key);

            //diversify PIV_admin_key
            byte[] pivAdminKey = adminDiversify(channel, pre_perso_admin_key);




            PivUtilInterface pivUtil = new PivUtil();
            returnValue = mngCHUID.writeChuid(channel,cardInfo.getChuid(),cardInfo.getChuid().getSignatureDate(),pre_perso_admin_key,pivAdminKey);
            if (returnValue == false) {
                throw new MiddleWareException("Error writing CHUID.");
            }

            ManagePrintedInfo mngPrintedInfo = new ManagePrintedInfo();

            // write printed information on card
            returnValue = mngPrintedInfo.writePrintedinfo(channel, pre_perso_admin_key, pivAdminKey,
                    cardInfo.getPrintedInfo().getName(),
                    cardInfo.getPrintedInfo().getAffiliation(), cardInfo.getPrintedInfo().getExpirationDate(),
                    cardInfo.getPrintedInfo().getCardSerialNumber(), cardInfo.getPrintedInfo().getIssuerID(),
                    cardInfo.getPrintedInfo().getOrganisationLine1(),
                    cardInfo.getPrintedInfo().getOrganisationLine2());

            if (returnValue == false) {
                throw new MiddleWareException("Error writing CHUID.");
            }


            // stop communication & return success
            //CardConnect.stopComm();

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::writeCardInfo total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return returnValue;
    }

    //CIN/IIN/CUID/BAP
    @Override
    public String readCIN(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readCIN");
        String retValue = "";
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readCIN start time: "
                    + startTime);


            /*
            // ---------------------------------------------------------- //
            // Start communication with card
            // ---------------------------------------------------------- //
            LoggerWrapper.info(client
            + " MiddleWareServerImpl::readCIN start communication with card");
            CardComm CardConnect = new CardComm();
            if (CardConnect.startComm() != 1) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.CARD_COMMUNICATION_EXCEPTION);
            throw new MiddleWareException(sb.toString());
            }
             */

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            PivUtilInterface pivUtil = new PivUtil();

            retValue = pivUtil.readCIN(channel);

            // stop communication & return success
            //CardConnect.stopComm();

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readCIN total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return retValue.toUpperCase();
    }

    @Override
    public String readIIN(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readIIN");
        String retValue = "";
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readIIN start time: "
                    + startTime);


            /*        
            // ---------------------------------------------------------- //
            // Start communication with card
            // ---------------------------------------------------------- //
            LoggerWrapper.info(client
            + " MiddleWareServerImpl::readCIN start communication with card");
            CardComm CardConnect = new CardComm();
            if (CardConnect.startComm() != 1) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.CARD_COMMUNICATION_EXCEPTION);
            throw new MiddleWareException(sb.toString());
            }
             */

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            PivUtilInterface pivUtil = new PivUtil();
            retValue = pivUtil.readIIN(channel);

            // stop communication & return success
            //CardConnect.stopComm();

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readIIN total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return retValue.toUpperCase();
    }

    @Override
    public String readCUID(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readCUID");
        String retValue = "";
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readCUID start time: "
                    + startTime);


            /*
            // ---------------------------------------------------------- //
            // Start communication with card
            // ---------------------------------------------------------- //
            LoggerWrapper.info(client
            + " MiddleWareServerImpl::readCUID start communication with card");
            CardComm CardConnect = new CardComm();
            if (CardConnect.startComm() != 1) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.CARD_COMMUNICATION_EXCEPTION);
            throw new MiddleWareException(sb.toString());
            }
             */

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            PivUtilInterface pivUtil = new PivUtil();

            retValue = pivUtil.readCUID(channel);

            // stop communication & return success
            //CardConnect.stopComm();

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readCUID total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return retValue.toUpperCase();
    }

    public String readBAP(CardChannel channel) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readBAP");
        String retValue = "";
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readBAP start time: "
                    + startTime);


            /*
            // ---------------------------------------------------------- //
            // Start communication with card
            // ---------------------------------------------------------- //
            LoggerWrapper.info(client
            + " MiddleWareServerImpl::readBAP start communication with card");
            CardComm CardConnect = new CardComm();
            if (CardConnect.startComm() != 1) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.CARD_COMMUNICATION_EXCEPTION);
            throw new MiddleWareException(sb.toString());
            }
             */

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            PivUtilInterface pivUtil = new PivUtil();

            retValue = pivUtil.readBAP(channel);

            // stop communication & return success
            //CardConnect.stopComm();

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readBAP total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return retValue.toUpperCase();
    }

    //Lock Card functions
    @Override
    public boolean cardLock(CardChannel channel, InitializationBean initvalue) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "CardLock");
        boolean retStatus = false;
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::CardLock start time: "
                    + startTime);

            // ---------------------------------------------------------- //
            // set default value for card manage AID & lock card
            // ---------------------------------------------------------- //
            initvalue.setCARD_MANAGER_AID("a0000001510000");

            CardUtilInterface lcard = new CardUtil(initvalue);
            retStatus = lcard.CardLock(channel);

            if (retStatus == false) {
                // Stop communication & return Error
                throw new MiddleWareException("Unable to Lock Card.");
            }

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::CardLock total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return retStatus;
    }

    @Override
    public boolean cardUnlock(CardChannel channel, InitializationBean initvalue) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "cardUnlock");
        boolean retStatus;
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::cardUnlock start time: "
                    + startTime);

            // ---------------------------------------------------------- //
            // set default value for card manage AID & UNlock card
            // ---------------------------------------------------------- //
            initvalue.setCARD_MANAGER_AID("a0000001510000");

            CardUtil unlcard = new CardUtil(initvalue);
            retStatus = unlcard.CardUnlock(channel);
            if (retStatus == false) {
                throw new MiddleWareException("Unable to UnLock Card.");
            }


            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::cardUnlock total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return retStatus;
    }

    //Misc supporting Functions
    @Override
    public boolean changePUK(CardChannel channel, String oldPUK, String newPUK) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "changePUK");

        String retValue;
        Integer pukLength;
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::changePUK start time: "
                    + startTime);



            PivUtilInterface pivUtil = new PivUtil();
            //-----------------------------------------------------------//
            //get and verify local pin length
            //-----------------------------------------------------------//
            retValue = pivUtil.readPinLength(channel, 'G');
            pukLength = Integer.parseInt(retValue, 16);

            if (oldPUK.length() > pukLength || oldPUK.length() == 0 || newPUK.length() > pukLength || newPUK.length() == 0) {

                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //			
            if (pivUtil.changePUK(channel, oldPUK, newPUK, pukLength) == false) {

                throw new MiddleWareException("Error Unblocking PIN.");
            }


            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::changePUK total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;

    }

    
    @Override
    public boolean resetPUK(CardChannel channel, InitializationBean initvalue, String newPUK) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "resetPUK");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + "MiddleWareServerImpl::resetPUK start time: "
                    + startTime);

            // ---------------------------------------------------------- //
            // create instance & initialize the card
            // ---------------------------------------------------------- //

            CardInit sinit = new CardInit(initvalue);

            if (sinit.initCard(channel, "P") == false) {
                throw new MiddleWareException("Error initializing PIV Card.");

            }


            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::resetPUK total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;
    }
    
    @Override
    public Integer readPinLength(CardChannel channel, char pinType) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readPinLength");

        Integer pinLength;
        String retValue;
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readPinLength start time: "
                    + startTime);

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            PivUtilInterface pivUtil = new PivUtil();
            retValue = pivUtil.readPinLength(channel, pinType);
            pinLength = Integer.parseInt(retValue, 16);



            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readPinLength total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }

        return pinLength;
    }

    @Override
    public FacialBean readFacialImage(CardChannel channel, String localPin) throws MiddleWareException {
        Integer pinLength;
        String retValue;

        LoggerWrapper.entering("MiddleWareServerImpl", "readFacialImage");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readFacialImage start time: "
                    + startTime);



            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            PivUtilInterface pivUtil = new PivUtil();
            FacialBean facialBean = new FacialBean();
            //-----------------------------------------------------------//
            //get and verify local pin length
            //-----------------------------------------------------------//
            retValue = pivUtil.readPinLength(channel, 'L');
            pinLength = Integer.parseInt(retValue, 16);

            if (localPin.length() > pinLength || localPin.length() == 0) {
                // stop communication & return success            
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.PIN_LENGTH_EXCEPTION);
                throw new MiddleWareException(sb.toString());
            }


            if (pivUtil.verifyLocalPin(channel, localPin, pinLength) == false) {

                throw new MiddleWareException("Invalid Pin.");
            }
            
            facialBean = pivUtil.readFacialinfo(channel);
            
            

            String encodedBytes = Base64.encodeBase64String(facialBean.getFacial_data());

            facialBean.setFacialImage(encodedBytes);
         



            return facialBean;
        } catch (MiddleWareException | NumberFormatException t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }

    }

    @Override
    public boolean writeFacialImage(CardChannel channel, String piv_admin_key,FacialImageBean facialImage) throws MiddleWareException {
        String stogo;
        boolean retValue = false;
        LoggerWrapper.entering("MiddleWareServerImpl", "writeFacialImage");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::writeFacialImage start time: "
                    + startTime);


            cmnUtil = new CommonUtil();
            byte[] byteimage = facialImage.getFacialData();
            if (byteimage.length > 12000) {
                throw new MiddleWareException("Size of Picture is to big.");
            }

            //stogo = cmnUtil.arrayToHex(byteimage);

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            PivUtilInterface pivUtil = new PivUtil();

            //convert PIV_admin_key to hex
            byte[] pre_perso_admin_key = cmnUtil.hex1ToByteArray(piv_admin_key);

            //diversify PIV_admin_key
            byte[] pivAdminKey = adminDiversify(channel, pre_perso_admin_key);
            byte[] FASCN = pivUtil.getFASCN(channel);
            
            
            String fascn=null;
            fascn = cmnUtil.arrayToHex(FASCN);
            retValue = pivUtil.writeFacialinfo(channel,pre_perso_admin_key,pivAdminKey,byteimage,facialImage.getCreationDate(),facialImage.getExpirationDate(),
                  facialImage.getValidFromDate(),  facialImage.getCreator(),fascn,facialImage.getSignatureDate(),facialImage.getContentCertificate(),facialImage.getPrivateKey());
            
            
            
            //pivUtil.writeFacialinfo(channel, key_p, stogo, null, null, null, null, null);
            //retValue = pivUtil.writeFacialinfo(channel, pre_perso_admin_key, pivAdminKey, byteimage);

            return retValue;
        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
    }

    @Override
    public BiometricBean readFingerPrintSecu(CardChannel channel, String localPin, String globalPin) throws MiddleWareException {
        BiometricBean fintemp;
        //create instance of FP utility class & start finger print decice
        SecuBioCapture secuFingerPrint = new SecuBioCapture();

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readFingerPrintSecu");

        try {

            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readFingerPrintSecu start time: "
                    + startTime);



            if (!secuFingerPrint.startDevice()) {
                throw new MiddleWareException(
                        "Unable to initialize Finger Print Device.");
            }

            //get finger print template
            fintemp = secuFingerPrint.getTemplateSecu();

            secuFingerPrint.closeDevice();
            return fintemp;
        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        } finally {
            secuFingerPrint.closeDevice();
        }
    }

    @Override
    public boolean verifyFingerPrint(CardChannel channel, byte[] fingerPrint) throws MiddleWareException {
        byte[] sub_imp_type = {0x00};

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "verifyFingerPrint");

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::verifyFingerPrint start time: "
                    + startTime);



            //create instance of utility class
            PivUtilInterface pivUtil = new PivUtil();

            //call verify finger Print
            if (pivUtil.verifyFingerPrint(channel, fingerPrint, sub_imp_type) == false) {

                throw new MiddleWareException("Invalid Finger Print.");
            }

            return true;

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }

    }

    @Override
    public boolean enrollFingerPrint(CardChannel channel, byte[] fingerPrint, String fingerID, String piv_admin_key) throws MiddleWareException {
        boolean retValue = false;
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "enrollFingerPrint");
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::enrollFingerPrint start time: "
                    + startTime);


            cmnUtil = new CommonUtil();
            //convert PIV_admin_key to hex
            byte[] pre_perso_admin_key = cmnUtil.hex1ToByteArray(piv_admin_key);

            //diversify PIV_admin_key
            byte[] pivAdminKey = adminDiversify(channel, pre_perso_admin_key);


            byte[] sub_imp_type = {0x00};

            PivUtil pivUtil = new PivUtil();
            try {
                retValue = pivUtil.updateFingerPrint(channel, pre_perso_admin_key, pivAdminKey, fingerPrint, fingerID, sub_imp_type);

            } catch (MiddleWareException e) {
                throw new MiddleWareException(e.getMessage());
            }

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return retValue;
    }

    @Override
    public List<String> getFPTerminalsID3( int ImgQuality,int ImgCount) throws MiddleWareException {
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "getFPTerminalsID3");
        List deviceList = new ArrayList<String>();
        final int imgQuality = ImgQuality;
        final int imgCount = ImgCount;
        try {

            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::getFPTerminalsID3 start time: "
                    + startTime);

            
            
                //get terminal list
                deviceList = (List<String>) AccessController.doPrivileged(new PrivilegedAction() {
                    public Object run() {
                       //get or create instance of Id3BioCapture class
                        Id3BioCapture fpid3 = null;
                        List deviceList = new ArrayList<String>();
                       
                        try {
                            fpid3 = Id3BioCapture.getInstance(imgQuality,imgCount);
                
                            deviceList = fpid3.getFGTerminals();
                         } catch (MiddleWareException ex) {
                              ex.printStackTrace();
                         }

                        return deviceList;
                    }
        
                });

           
            return deviceList;
        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        } finally {
        }
    }

    @Override
    public BiometricBean readFingerPrintID3(int ImgQuality,int ImgCount, String deviceName, Integer fgType) throws MiddleWareException {
        BiometricBean fintemp = null;

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readFingerPrintID3");

        try {

            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readFingerPrintID3 start time: "
                    + startTime);

            //get or create instance of Id3BioCapture class
            Id3BioCapture fpid3 = null;
            try {
                fpid3 = Id3BioCapture.getInstance(ImgQuality,ImgCount);
            } catch (MiddleWareException ex) {
                throw new MiddleWareException(ex.getMessage());
            }

            //get finger print
            fintemp = fpid3.startCapture(deviceName, fgType);

            if (fpid3.getImageCount() == ImgCount) {
                fintemp = fpid3.createCompactTemplate();

                System.out.println("Harpreet--Creating Template");
            }


            return fintemp;
        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        } finally {
        }
    }
    
    

    @Override
    public boolean cardPersonalization(CardChannel channel, PersonalizationBean perso, String piv_admin_key) throws MiddleWareException {
        String oldLocalPin;
        String oldGlobalPin;
        ChuidBean inChuid;
        FascnBean inFascn;

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "cardPersonalization");
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::cardPersonalization start time: "
                    + startTime);



            PivUtilInterface pivUtil = new PivUtil();
            ManageCHUID mngCHUID = new ManageCHUID();


            //get Default values from initialization default values
            defvalue = new defaultInitValues();
            oldLocalPin = defvalue.getLocalPin();
            oldGlobalPin = defvalue.getGlobalPin();

            //get personalization values from user
            inChuid = perso.getChuid();
            //inFascn = inChuid.getFascn();




            //---------------------------------------------------------- //
            // Change Global PIN
            // ---------------------------------------------------------- //
            if (pivUtil.changeGlobalPin(channel, oldGlobalPin, perso.getGlobalPin(), 8) == false) {

                throw new MiddleWareException("Invalid Pin.");
            }



            // format Date in 01JAN2011
            Format formatter;
            formatter = new SimpleDateFormat("yyyyMMdd");
            String strExpDate = formatter.format(inChuid.getExpirationDate()).toUpperCase();

            byte[] pre_perso_admin_key = cmnUtil.hex1ToByteArray(piv_admin_key);

            //diversify PIV_admin_key
            byte[] pivAdminKey = adminDiversify(channel, pre_perso_admin_key);


            //---------------------------------------------------------- //
            // Write CHUID
            // ---------------------------------------------------------- //
            if (mngCHUID.writeChuid(channel,inChuid,inChuid.getSignatureDate(),pre_perso_admin_key,pivAdminKey) == false) {
                throw new MiddleWareException("Error writing CHUID.");
            }



        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;
    }

    @Override
    public boolean cardInitialization(CardChannel channel, InitializationBean initvalue) throws MiddleWareException {
        LoggerWrapper.entering("MiddleWareServerImpl", "cardInitialization");
        
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + "MiddleWareServerImpl::cardInitialization start time: "
                    + startTime);

            byte[] pre_perso_admin_key = null;
            byte[] pivAdminKey = null;        
            if (initvalue.getDiversifyKey().equalsIgnoreCase("Y")){
                pre_perso_admin_key = initvalue.getPRE_PERSO_PIV_ADMIN_KEY();
                pivAdminKey = adminDiversify(channel, pre_perso_admin_key);
            }else{
                pre_perso_admin_key = initvalue.getPRE_PERSO_PIV_ADMIN_KEY();
                pivAdminKey = pre_perso_admin_key;
            };
        
            
             
            initvalue.setPIV_ADMIN_KEY(pivAdminKey);

            // ---------------------------------------------------------- //
            // create instance & initialize the card
            // ---------------------------------------------------------- //

            CardInit sinit = new CardInit(initvalue);

            if (sinit.initCard(channel, "A") == false) {

                throw new MiddleWareException("Error initializing PIV Card.");

            }


            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::cardInitialization total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return true;




    }

    public byte[] adminDiversify(CardChannel channel, byte[] piv_admin_key) throws MiddleWareException {
        // First of all read CUID from the card
        String str_cuid = null;
        try {

            PivUtilInterface pivUtil = new PivUtil();
            cmnUtil = new CommonUtil();

            //read cuid to diversify PIV admin Key
            str_cuid = pivUtil.readCUID(channel);

            //convert cuid to hex and take 8 characters
            byte[] byte_cuid_s = cmnUtil.hex1ToByteArray(str_cuid);
            if (byte_cuid_s.length > 8) {
                byte_cuid_s = cmnUtil.strip_data(byte_cuid_s, byte_cuid_s.length - 8);
            }

            //replicate CUID to make it 24 bytes required to diversify 24 byte piv admin key
            byte[] byte_cuid = cmnUtil.combine_data(byte_cuid_s, byte_cuid_s);
             
            

            byte[] res = null;
            // Create encrypter instance
            if (piv_admin_key.length == 16) {
                AesEncrypter encrypter = new AesEncrypter(piv_admin_key);

                // Encrypt
                res = encrypter.encrypt(byte_cuid);


            } else {
                byte_cuid = cmnUtil.combine_data(byte_cuid, byte_cuid_s);
                DesEncrypter encrypter = new DesEncrypter(piv_admin_key);
                // encrypt Diversified PIV admin Key 
                res = encrypter.encrypt(byte_cuid);
            }
                
            

            


            CardInit sinit = new CardInit();
            sinit.checkPIVInstance(channel);


            return res;
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    //public String loadCertificate(CardChannel channel, char certificateType, String algorithmType, String piv_admin_key, String commonName, String organisationalunit,
    //        String organisation, String city, String state, String country, String certificateTemplate, String caType) throws MiddleWareException {
    
    

    @Override
    public CertificateBean readCertificate(CardChannel channel, char CertificateType) throws MiddleWareException {
         LoggerWrapper.entering("MiddleWareServerImpl", "readCertificate");

         System.out.println("Reading the Certificate >>>>>>>>>>>>>> ");
        CertificateBean certBean = new CertificateBean();
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readCertificate start time: "
                    + startTime);


            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            ReadCertificate rc = new ReadCertificate();

            certBean = rc.readCert(channel,CertificateType);

            // stop communication & return success
            //CardConnect.stopComm();

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readCertificate total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);

        }

        return certBean;
    }

    @Override
    public byte[] generateCertificate(CardChannel channel, String piv_admin_key, CSRBean certBean) throws MiddleWareException {
          
        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "generateCertificate");
        byte[] certificate=null;
        byte[] algorithmTypeId = null;

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::generateCertificate start time: "
                    + startTime);

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            GenCertificate gencert = new GenCertificate();

            cmnUtil = new CommonUtil();
            //convert PIV_admin_key to hex
            byte[] pre_perso_admin_key = cmnUtil.hex1ToByteArray(piv_admin_key);

            //diversify PIV_admin_key
            byte[] pivAdminKey = adminDiversify(channel, pre_perso_admin_key);

            
            
            byte[] algo_rsa2048 = {0x07};



            if ("RSA2048".equalsIgnoreCase(certBean.getAlgorithmType())) {
                algorithmTypeId = algo_rsa2048;
            }


            certificate = gencert.genCert(channel,pre_perso_admin_key,pivAdminKey,certBean.getCertificateType(), algorithmTypeId, certBean.getCommonName(),certBean.getOrganisationalunit(),
                  certBean.getOrganisation(),certBean.getCity() ,certBean.getState(),certBean.getCountry(),certBean.getEmailId(),
                  certBean.getCert_template_oid(),certBean.getMajorVer(),certBean.getMinorVer(),certBean.getCaType(),certBean.getCertURL());

         

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::generateCertificate total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }

        return certificate;
    }
    
    @Override
    public boolean loadCertificate(CardChannel channel, String piv_admin_key, byte[] certificate, char certType) throws MiddleWareException {

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "loadCertificate");
        boolean returnStatus = false;
        
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::loadCertificate start time: "
                    + startTime);

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            if (certificate.length >2000){
                // stop communication & return success            
                StringBuilder sb = new StringBuilder();
                sb.append(ExceptionMessages.INVALID_CERTIFICATE_LENGTH);
                throw new MiddleWareException(sb.toString());
            }
            
            cmnUtil = new CommonUtil();
            //convert PIV_admin_key to hex
            byte[] pre_perso_admin_key = cmnUtil.hex1ToByteArray(piv_admin_key);

            //diversify PIV_admin_key
            byte[] pivAdminKey = adminDiversify(channel, pre_perso_admin_key);



            //load certificate on card
            LoadCertificate loadCertificate = new LoadCertificate();
            returnStatus = loadCertificate.loadCert(channel, pre_perso_admin_key, pivAdminKey, certificate, certType);

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::loadCertificate total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }

        return returnStatus;
    }

    @Override
    public ContainerInfoBean readContainerInfo(CardChannel channel, String piv_admin_key) throws MiddleWareException {
         // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readContainerInfo");
        String retValue;
        ContainerInfoBean containerInfo = new ContainerInfoBean();
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readContainerInfo start time: "
                    + startTime);

            DiscoverContainers discContainer = new DiscoverContainers();

            //-----------------------------------------------------------//
            //get and verify local pin length
            //-----------------------------------------------------------//
            cmnUtil = new CommonUtil();
            
            byte[] pre_perso_admin_key = cmnUtil.hex1ToByteArray(piv_admin_key);

            //diversify PIV_admin_key
            byte[] pivAdminKey = adminDiversify(channel, pre_perso_admin_key);
            
            
            containerInfo = discContainer.getContainerInfo(channel, pre_perso_admin_key,pivAdminKey);
            

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //

            System.out.println("true");
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readContainerInfo total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return containerInfo;
    }

    @Override
    public byte[] generateContentCertificate(CardChannel channel,  CSRBean certBean) throws MiddleWareException {
         // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "generateContentCertificate");
        byte[] certificate=null;
        byte[] algorithmTypeId = null;

        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::generateCertificate start time: "
                    + startTime);

            // ---------------------------------------------------------- //
            // Check if the PIN is Valid
            // ---------------------------------------------------------- //
            GenContentCertificate genContentcert = new GenContentCertificate();

            //cmnUtil = new CommonUtil();
            //convert PIV_admin_key to hex
            //byte[] pre_perso_admin_key = cmnUtil.hex1ToByteArray(piv_admin_key);

            //diversify PIV_admin_key
            //byte[] pivAdminKey = adminDiversify(channel, pre_perso_admin_key);

            
            
            byte[] algo_rsa2048 = {0x07};



            if ("RSA2048".equalsIgnoreCase(certBean.getAlgorithmType())) {
                algorithmTypeId = algo_rsa2048;
            }


            certificate = genContentcert.genContentCert(channel,certBean.getCertificateType(), algorithmTypeId, certBean.getCommonName(),certBean.getOrganisationalunit(),
                  certBean.getOrganisation(),certBean.getCity() ,certBean.getState(),certBean.getCountry(),certBean.getEmailId(),
                  certBean.getCert_template_oid(),certBean.getMajorVer(),certBean.getMinorVer(),certBean.getCaType(),certBean.getCertURL(),
                  certBean.getPublicKey(),certBean.getPrivateKey());

         

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::generateCertificate total time: "
                    + totalTime);

        } catch (Throwable t) {

            throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }

        return certificate;
    }

    @Override
    public SecurityBean readSecContainer(CardChannel channel) throws MiddleWareException {
        
        SecurityBean sec = new SecurityBean();

        // Logging information
        LoggerWrapper.entering("MiddleWareServerImpl", "readSecContainer");
        try {
            registerThread();
            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long startTime = System.currentTimeMillis();
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readSecContainer start time: "
                    + startTime);


            verifySecudataSignature verifysecu = new verifySecudataSignature();

            // ---------------------------------------------------------- //
            // Read security container
            // ---------------------------------------------------------- //			
            sec = verifysecu.readSecContainer(channel);

            // ---------------------------------------------------------- //
            // LOGGING INFORMATON
            // ---------------------------------------------------------- //
            long stopTime = System.currentTimeMillis();
            long totalTime = stopTime - startTime;
            LoggerWrapper.info(client
                    + " MiddleWareServerImpl::readSecContainer total time: "
                    + totalTime);

        } catch (MiddleWareException | NumberFormatException t) {

            throw new MiddleWareException(t.getMessage());
            //throw ExceptionUtilities.createIfNotMiddleWareException(t);
        }
        return sec;
    }

    @Override
    public void disposeFPCapture(Integer imgQuality, Integer imgCount) throws MiddleWareException {
         Id3BioCapture fpid3 = Id3BioCapture.getInstance(imgQuality, imgCount);
    }

    

    
}