/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.AesEncrypter;
import com.secuera.middleware.beans.piv.CredentialKeys;
import com.secuera.middleware.beans.piv.DiversifiedCredentialKeys;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import com.secuera.middleware.beans.piv.SecureChannelBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;

/**
 *
 * @author: Harpreet Singh
 */
public class InjectAssymetricKey {

    // declare class variable for different keys
    private byte[] S_MAC_KEY;
    private byte[] PREV_MAC;
    private byte[] AUTH_DEK_KEY;
    private byte[] IV_AES = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    ExceptionMessages expMsg = new ExceptionMessages();
    // *****************************************************************
    // Create instances of other classes.
    // *****************************************************************
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    GeneralAuth auth = new GeneralAuth();
    CredentialKeys credentialKeys = new CredentialKeys();
    DiversifiedCredentialKeys diversifiedCredentialKeys = new DiversifiedCredentialKeys();
    SecureChannelBean scbean = new SecureChannelBean();
    String errorMsg;
    CardChannel channel;
    String keyId;
    String keyAlgo;
    PrivateKey privateKey;
    PublicKey publicKey;
    byte[] pubExpo, pubModulus, privP, privQ, privDP1, privDQ1, privPQ;
    byte[] pubExpoTag = {0x40};
    byte[] pubModTag = {0x41};
    byte[] privPTag = {0x42};
    byte[] privQTag = {0x43};
    byte[] privDP1Tag = {0x44};
    byte[] privDQ1Tag = {0x45};
    byte[] privPQTag = {0x46};
    byte[] apduCmd = {0x04, (byte) 0xDB, 0x3F, (byte) 0xFF};
    byte[] apduCmdChain = {0x14, (byte) 0xDB, 0x3F, (byte) 0xFF};
    byte[] apduKeyRef = {0x5C, 0x02};
    byte[] dataLength = {0x53, 0x02};
    byte[] startTagECC = {0x50, 0x00};
    byte[] endTagECC = {0x53, 0x00};
    byte[] startTagRSA = {0x39, 0x00};
    byte[] endTagRSA = {0x47, 0x00};
    byte[] pubKeyTag = {0x51};
    byte[] privKeyTag = {0x52};
    String startTag = ("0281");

    public InjectAssymetricKey() {
    }

    //Constructor get initial class values

    public InjectAssymetricKey(CredentialKeys crKeys,DiversifiedCredentialKeys dCrKeys) {
        credentialKeys = crKeys;
        diversifiedCredentialKeys = dCrKeys;

    }

    public boolean InjectAsmKey(CardChannel ch, String strkeyId, String strkeyAlgo, PrivateKey privkey,
            PublicKey pubkey) throws MiddleWareException {

        channel = ch;
        keyId = strkeyId;
        keyAlgo = strkeyAlgo;
        privateKey = privkey;
        publicKey = pubkey;

        selectSecurityDomain(channel);

        try {

            auth.generalAuth(channel, credentialKeys.getAdminKey());
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, diversifiedCredentialKeys.getDiversifiedAdminKey());
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }

        try {

            //open secure channel
            SecureChannel03 scp03 = new SecureChannel03(credentialKeys, diversifiedCredentialKeys);
            scbean = scp03.openSecureChannel03(channel, false);

            //get values from secure channel 03
            S_MAC_KEY = scbean.getS_MAC_KEY();
            PREV_MAC = scbean.getPREV_MAC();
            AUTH_DEK_KEY = scbean.getAUTH_DEK_KEY();

        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        //util.arrayToHex(PREV_MAC)

        if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.ECC256) || keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.ECC384)) {
            loadKey_ECC();
        } else if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.RSA1024) || keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
            loadKey_RSA();
        } else {
            throw new MiddleWareException("Unsupported Algorithm Type for Admin Key.");
        }



        return true;

    }

    private boolean loadKey_ECC() throws MiddleWareException {
        //Signal Start of Injection.
        injectKeyTag(false, true);

        // First of all inject Public part of the key.
        injectKeyECC(false);
        //Now inject Private part of the key.
        injectKeyECC(true);

        // Signal end of Injection.
        injectKeyTag(true, true);

        return true;
    }

    private void loadKey_RSA() throws MiddleWareException {

        if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
            parseRSAKey2048();
        } else {
            parseRSAKey1024();
        }

        // Signal start of injection.
        injectKeyTag(false, false);

        // First of all inject,Public Exponent
        injectKeyRSA(pubExpoTag);
        // First of all inject,Public Modulus

        if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
            injectKeyChainRSA(pubModTag);
        } else {
            injectKeyRSA(pubModTag);
        }


        // First of all inject,Private P
        injectKeyRSA(privPTag);
        // First of all inject,Private Q
        injectKeyRSA(privQTag);
        // First of all inject,Private DP1
        injectKeyRSA(privDP1Tag);
        // First of all inject,Private DP2
        injectKeyRSA(privDQ1Tag);
        // First of all inject,Private PQ
        injectKeyRSA(privPQTag);

        // Signal end of injection.
        injectKeyTag(true, false);

    }

    private void injectKeyTag(boolean lbFlag, boolean lbkeyFlag) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();

        byte[] importTag;

        if (lbkeyFlag) {
            if (lbFlag) {
                importTag = endTagECC;
            } else {
                importTag = startTagECC;
            }
        } else {
            if (lbFlag) {
                importTag = endTagRSA;
            } else {
                importTag = startTagRSA;
            }
        }

        int len = apduKeyRef.length + util.hex1StringToByteArray(keyId).length
                + util.hex1StringToByteArray(keyAlgo).length
                + dataLength.length + importTag.length + 8;


        byte[] apdu_len = util.calcBertValue(len, false, true);

        byte[][] arraysCmd = new byte[9][];
        arraysCmd[0] = PREV_MAC;
        arraysCmd[1] = UcmsMiddlewareConstants.ZERO_CARD_PAD;
        arraysCmd[2] = apduCmd;
        arraysCmd[3] = apdu_len;
        arraysCmd[4] = apduKeyRef;
        arraysCmd[5] = util.hex1ToByteArray(keyId);
        arraysCmd[6] = util.hex1ToByteArray(keyAlgo);
        arraysCmd[7] = dataLength;
        arraysCmd[8] = importTag;

        byte[] combinedData = util.combine_data(arraysCmd);

        // Now add secure pad according to encryption requirement.
        combinedData = add_securepad(combinedData);

        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, combinedData, true);

//        // Now store the new MAC. Get the first 8 bytes from last block of
//        // data.(Each block is of 16 bytes)
        int i_offset = getOffset(encrypt_data);

        PREV_MAC = util.extract_data(encrypt_data, i_offset, 8);

        // System.out.println("NUm bytes :" + util.arrayToHex(num_bytes) );

        byte[][] arrays = new byte[8][];
        arrays[0] = apduCmd;
        arrays[1] = apdu_len;
        arrays[2] = apduKeyRef;
        arrays[3] = util.hex1ToByteArray(keyId);
        arrays[4] = util.hex1ToByteArray(keyAlgo);
        arrays[5] = dataLength;
        arrays[6] = importTag;
        arrays[7] = PREV_MAC;

        byte[] combined_bytes = util.combine_data(arrays);

        //System.out.println("Start Tag :" + util.arrayToHex(combined_bytes) );

        util.arrayToHex(combined_bytes);
        CommandAPDU KEY_APDU = new CommandAPDU(combined_bytes);


        try {

            sendCard.sendCommand(channel, KEY_APDU, sb);

            // check if no success then throw exception
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();
                errMsg.append("Error while trying to inject start/end tag for ECC Key");
                errMsg.append(" (").append(sb.toString()).append(")");
                throw new MiddleWareException(errMsg.toString());
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }


    }

    /* Load keys method */
    private void injectKeyECC(boolean lbPrivateKey) throws MiddleWareException {
        StringBuffer sb = new StringBuffer();

        byte[] keyTag;
        byte[] loadKey;

        //System.out.println("Public Key Value : " + util.arrayToHex(publicKey.getEncoded()));
        //System.out.println("Private Value total: " + util.arrayToHex(privateKey.getEncoded()));

        if (lbPrivateKey) {
            keyTag = privKeyTag;
            if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.ECC256)) {
                loadKey = util.getEndBytes(privateKey.getEncoded(), UcmsMiddlewareConstants.ECC256KeySize);
            } else {
                loadKey = util.getEndBytes(privateKey.getEncoded(), UcmsMiddlewareConstants.ECC384KeySize);
            }

        } else {
            keyTag = pubKeyTag;
            if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.ECC256)) {
                loadKey = util.getEndBytes(publicKey.getEncoded(), (UcmsMiddlewareConstants.ECC256KeySize * 2) + 1);
            } else {

                loadKey = util.getEndBytes(publicKey.getEncoded(), (UcmsMiddlewareConstants.ECC384KeySize * 2) + 1);
            }

        }


        //System.out.println("Key Value :" + util.arrayToHex(loadKey) );


        // **----------------------------------------------
        // ** Secure injection of Keys
        // **----------------------------------------------
        // First of all format key according to secure encryption requirement.
        loadKey = add_securepad(loadKey);

        //util.arrayToHex(loadKey)
        // Now Encrypt Key value with KEK in CBC Mode.
        byte[] key_encrypt = aesencrypt(AUTH_DEK_KEY, loadKey, true);

        //.arrayToHex(key_encrypt)
        // Now compute new MAC and prepare APDU command for Key Injection
        // *****************************************************//
        // 04 ---> 00 is changed to 04 to indicate Secure messaging by setting
        // bit # 3.


        // Calculate length of encrypted key for ber_tlv tag value.
        byte[] key_len = util.calcBertValue(key_encrypt.length, false, false);

        byte[] data_len = util.calcBertValue(key_encrypt.length + keyTag.length + key_len.length, true, false);

        int len = apduKeyRef.length + util.hex1ToByteArray(keyId).length
                + util.hex1ToByteArray(keyAlgo).length + data_len.length
                + keyTag.length + key_len.length + key_encrypt.length + 8;


        byte[] apdu_len = util.calcBertValue(len, false, true);

        byte[][] arraysCmd = new byte[11][];
        arraysCmd[0] = PREV_MAC;
        arraysCmd[1] = UcmsMiddlewareConstants.ZERO_CARD_PAD;
        arraysCmd[2] = apduCmd;
        arraysCmd[3] = apdu_len;
        arraysCmd[4] = apduKeyRef;
        arraysCmd[5] = util.hex1ToByteArray(keyId);
        arraysCmd[6] = util.hex1ToByteArray(keyAlgo);
        arraysCmd[7] = data_len;
        arraysCmd[8] = keyTag;
        arraysCmd[9] = key_len;
        arraysCmd[10] = key_encrypt;

        byte[] set_key_data = util.combine_data(arraysCmd);


        // Now add secure pad according to encryption requirement.
        set_key_data = add_securepad(set_key_data);

        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, set_key_data, true);

//        // Now store the new MAC. Get the first 8 bytes from last block of
//        // data.(Each block is of 16 bytes)
        int i_offset = getOffset(encrypt_data);

        PREV_MAC = util.extract_data(encrypt_data, i_offset, 8);

        // Now prepare command for the card with the MAC.
        byte[][] cmdData = new byte[10][];
        cmdData[0] = apduCmd;
        cmdData[1] = apdu_len;
        cmdData[2] = apduKeyRef;
        cmdData[3] = util.hex1ToByteArray(keyId);
        cmdData[4] = util.hex1ToByteArray(keyAlgo);
        cmdData[5] = data_len;
        cmdData[6] = keyTag;
        cmdData[7] = key_len;
        cmdData[8] = key_encrypt;
        cmdData[9] = PREV_MAC;

        byte[] key_command_data = util.combine_data(cmdData);

        //System.out.println("key_command_data :" + util.arrayToHex(key_command_data) );

        CommandAPDU KEY_APDU = new CommandAPDU(key_command_data);

        try {

            sendCard.sendCommand(channel, KEY_APDU, sb);

            // check if no success then throw exception
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();

                if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.ECC256)) {
                    errMsg.append(ExceptionMessages.ECCKEY256);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());

                } else if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.ECC384)) {
                    errMsg.append(ExceptionMessages.ECCKEY384);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());
                }
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }


    /* Load keys method */
    private void injectKeyRSA(byte[] keyTag) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] loadKey = null;


        if (Arrays.equals(keyTag, pubExpoTag)) {
            loadKey = pubExpo;

        } else if (Arrays.equals(keyTag, pubModTag)) {
            loadKey = pubModulus;

        } else if (Arrays.equals(keyTag, privPTag)) {
            loadKey = privP;

        } else if (Arrays.equals(keyTag, privQTag)) {
            loadKey = privQ;

        } else if (Arrays.equals(keyTag, privDP1Tag)) {
            loadKey = privDP1;

        } else if (Arrays.equals(keyTag, privDQ1Tag)) {
            loadKey = privDQ1;

        } else if (Arrays.equals(keyTag, privPQTag)) {
            loadKey = privPQ;
        }


        //System.out.println("Key Value :" + util.arrayToHex(loadKey) );

        // **----------------------------------------------
        // ** Secure injection of Keys
        // **----------------------------------------------
        // First of all format key according to secure encryption requirement.
        loadKey = add_securepad(loadKey);

        //util.arrayToHex(loadKey)
        // Now Encrypt Key value with KEK in CBC Mode.
        byte[] key_encrypt = aesencrypt(AUTH_DEK_KEY, loadKey, true);

        //util.arrayToHex(key_encrypt)
        // Now compute new MAC and prepare APDU command for Key Injection
        // *****************************************************//
        // 04 ---> 00 is changed to 04 to indicate Secure messaging by setting
        // bit # 3.


        // Calculate length of encrypted key for ber_tlv tag value.
        byte[] key_len = util.calcBertValue(key_encrypt.length, false, false);

        byte[] data_len = util.calcBertValue(key_encrypt.length + keyTag.length + key_len.length, true, false);

        ///util.arrayToHex(data_len)
        int len = apduKeyRef.length + util.hex1ToByteArray(keyId).length
                + util.hex1ToByteArray(keyAlgo).length + data_len.length
                + keyTag.length + key_len.length + key_encrypt.length + 8;


        byte[] apdu_len = util.calcBertValue(len, false, true);

        byte[][] arraysCmd = new byte[11][];
        arraysCmd[0] = PREV_MAC;
        arraysCmd[1] = UcmsMiddlewareConstants.ZERO_CARD_PAD;
        arraysCmd[2] = apduCmd;
        arraysCmd[3] = apdu_len;
        arraysCmd[4] = apduKeyRef;
        arraysCmd[5] = util.hex1ToByteArray(keyId);
        arraysCmd[6] = util.hex1ToByteArray(keyAlgo);
        arraysCmd[7] = data_len;
        arraysCmd[8] = keyTag;
        arraysCmd[9] = key_len;
        arraysCmd[10] = key_encrypt;

        byte[] set_key_data = util.combine_data(arraysCmd);


        // Now add secure pad according to encryption requirement.
        set_key_data = add_securepad(set_key_data);

        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, set_key_data, true);

        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        int i_offset = getOffset(encrypt_data);

        PREV_MAC = util.extract_data(encrypt_data, i_offset, 8);

        // Now prepare command for the card with the MAC.
        byte[][] cmdData = new byte[10][];
        cmdData[0] = apduCmd;
        cmdData[1] = apdu_len;
        cmdData[2] = apduKeyRef;
        cmdData[3] = util.hex1ToByteArray(keyId);
        cmdData[4] = util.hex1ToByteArray(keyAlgo);
        cmdData[5] = data_len;
        cmdData[6] = keyTag;
        cmdData[7] = key_len;
        cmdData[8] = key_encrypt;
        cmdData[9] = PREV_MAC;

        byte[] key_command_data = util.combine_data(cmdData);

        System.out.println("key_command_data :" + util.arrayToHex(key_command_data));

        CommandAPDU KEY_APDU = new CommandAPDU(key_command_data);

        try {

            sendCard.sendCommand(channel, KEY_APDU, sb);

            // check if no success then throw exception
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();

                if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
                    errMsg.append(ExceptionMessages.RSAKEY2048);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());

                } else if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.RSA1024)) {
                    errMsg.append(ExceptionMessages.RSAKEY1024);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());
                }
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }


    /* Load keys method */
    private void injectKeyChainRSA(byte[] keyTag) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] key_encrypt_1;
        byte[] key_encrypt_2;
        int len;
        // **----------------------------------------------
        // ** Secure injection of Keys
        // **----------------------------------------------
        // First of all format key according to secure encryption requirement.
        pubModulus = add_securepad(pubModulus);

        //util.arrayToHex(loadKey)
        // Now Encrypt Key value with KEK in CBC Mode.
        byte[] key_encrypt = aesencrypt(AUTH_DEK_KEY, pubModulus, true);


        // System.out.println("Key Encrypt :" + util.arrayToHex(key_encrypt));
        //util.arrayToHex(key_encrypt)
        // Now compute new MAC and prepare APDU command for Key Injection
        // *****************************************************//
        // 04 ---> 00 is changed to 04 to indicate Secure messaging by setting
        // bit # 3.

        // Calculate length of encrypted key for ber_tlv tag value.
        byte[] key_len = util.calcBertValue(key_encrypt.length, false, false);
        byte[] data_len = util.calcBertValue(key_encrypt.length + keyTag.length + key_len.length, true, false);

        ///util.arrayToHex(data_len)
        //int len = apduKeyRef.length + util.hex1ToByteArray(keyId).length
        //       + util.hex1ToByteArray(keyAlgo).length + data_len.length
        //      + keyTag.length + key_len.length + key_encrypt.length + 8;

        key_encrypt_1 = util.extract_data(key_encrypt, 0, 20);

        len = apduKeyRef.length + util.hex1ToByteArray(keyId).length
                + util.hex1ToByteArray(keyAlgo).length + data_len.length
                + keyTag.length + key_len.length + key_encrypt_1.length + 8;

        byte[] apdu_len = util.calcBertValue(len, false, true);


        byte[][] arraysCmd = new byte[11][];
        arraysCmd[0] = PREV_MAC;
        arraysCmd[1] = UcmsMiddlewareConstants.ZERO_CARD_PAD;
        arraysCmd[2] = apduCmdChain;
        arraysCmd[3] = apdu_len;
        arraysCmd[4] = apduKeyRef;
        arraysCmd[5] = util.hex1ToByteArray(keyId);
        arraysCmd[6] = util.hex1ToByteArray(keyAlgo);
        arraysCmd[7] = data_len;
        arraysCmd[8] = keyTag;
        arraysCmd[9] = key_len;
        arraysCmd[10] = key_encrypt_1;

        byte[] set_key_data = util.combine_data(arraysCmd);

        // Now add secure pad according to encryption requirement.
        set_key_data = add_securepad(set_key_data);

        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, set_key_data, true);

        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        int i_offset = getOffset(encrypt_data);

        PREV_MAC = util.extract_data(encrypt_data, i_offset, 8);

        // Now prepare command for the card with the MAC for the first part of chain.
        //key_encrypt_1 = util.extract_data(key_encrypt, 0, 8);

        len = apduKeyRef.length + util.hex1ToByteArray(keyId).length
                + util.hex1ToByteArray(keyAlgo).length + data_len.length
                + keyTag.length + key_len.length + key_encrypt_1.length + 8;

        apdu_len = util.calcBertValue(len, false, true);


        byte[][] cmdData = new byte[10][];
        cmdData[0] = apduCmdChain;
        cmdData[1] = apdu_len;
        cmdData[2] = apduKeyRef;
        cmdData[3] = util.hex1ToByteArray(keyId);
        cmdData[4] = util.hex1ToByteArray(keyAlgo);
        cmdData[5] = data_len;
        cmdData[6] = keyTag;
        cmdData[7] = key_len;
        cmdData[8] = key_encrypt_1;
        cmdData[9] = PREV_MAC;

        byte[] key_command_data = util.combine_data(cmdData);

        //System.out.println("key_command_data_1 :" + util.arrayToHex(key_command_data));

        CommandAPDU KEY_APDU = new CommandAPDU(key_command_data);

        try {

            sendCard.sendCommand(channel, KEY_APDU, sb);

            // check if no success then throw exception
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();

                if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
                    errMsg.append(ExceptionMessages.RSAKEY2048);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());

                }

            }
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // Now prepare command for the card with the MAC for the 2nd part of chain.
        key_encrypt_2 = util.strip_data(key_encrypt, 20);

        len = key_encrypt_2.length + 8;
        apdu_len = util.calcBertValue(len, false, true);

        cmdData = new byte[5][];
        cmdData[0] = PREV_MAC;
        cmdData[1] = UcmsMiddlewareConstants.ZERO_CARD_PAD;
        cmdData[2] = apduCmd;
        cmdData[3] = apdu_len;
        cmdData[4] = key_encrypt_2;

        set_key_data = util.combine_data(cmdData);

        // Now add secure pad according to encryption requirement.
        set_key_data = add_securepad(set_key_data);

        // Now Encrypt the command data field.
        encrypt_data = aesencrypt(S_MAC_KEY, set_key_data, true);

        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        i_offset = getOffset(encrypt_data);

        PREV_MAC = util.extract_data(encrypt_data, i_offset, 8);

        // Now prepare the command.
        cmdData = new byte[4][];
        cmdData[0] = apduCmd;
        cmdData[1] = apdu_len;
        cmdData[2] = key_encrypt_2;
        cmdData[3] = PREV_MAC;

        key_command_data = util.combine_data(cmdData);

        // System.out.println("key_command_data :" + util.arrayToHex(key_command_data));

        KEY_APDU = new CommandAPDU(key_command_data);

        try {

            sendCard.sendCommand(channel, KEY_APDU, sb);

            // check if no success then throw exception
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();

                if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
                    errMsg.append(ExceptionMessages.RSAKEY2048);
                    errMsg.append(" (").append(sb.toString()).append(")");
                    throw new MiddleWareException(errMsg.toString());

                }

            }


        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }



    }

    public void parseRSAKey2048() {
        //First of all get,Public Modulus.
        String startTag = ("0282");
        String str_modulus, str_expo, str_temp, strP = null, strQ = null, strDP1 = null, strDQ1 = null, strPQ = null;
        int i_start, str_tempLen;

        String strPrivKey = util.arrayToHex(privateKey.getEncoded());

        //substring(int beginIndex, int endIndex)

        i_start = strPrivKey.toLowerCase().indexOf(startTag.toLowerCase()) + 4;

        if (strPrivKey.substring(i_start, i_start + 4).equals("0101")) {
            str_modulus = strPrivKey.substring(i_start + 6, i_start + 6 + UcmsMiddlewareConstants.RSA2048KeySize * 2);
            strPrivKey = strPrivKey.substring(i_start + 6 + UcmsMiddlewareConstants.RSA2048KeySize * 2);

        } else {
            str_modulus = strPrivKey.substring(i_start + 4, i_start + 4 + UcmsMiddlewareConstants.RSA2048KeySize * 2);
            strPrivKey = strPrivKey.substring(i_start + 4 + UcmsMiddlewareConstants.RSA2048KeySize * 2);
        }

        pubModulus = util.hex1StringToByteArray(str_modulus);


        //Get, Public Exponent. 02 03 01 00 01 02 82
        if (strPrivKey.substring(2, 4).equals("03")) {
            str_expo = strPrivKey.substring(4, 10);
            strPrivKey = strPrivKey.substring(10);

        } else {
            str_expo = strPrivKey.substring(4, 12);
            strPrivKey = strPrivKey.substring(12);
        }

        pubExpo = util.hex1StringToByteArray(str_expo);


        //Get, Private P.
        // First step is to skip next 256 bytes. not required.

        if (strPrivKey.substring(0, 4).equals("0282")) {
            str_temp = strPrivKey.substring(4, 8);
            str_tempLen = Integer.valueOf(str_temp, 16).intValue();

            strPrivKey = strPrivKey.substring(8 + str_tempLen * 2);

        }


        if (strPrivKey.substring(0, 4).equals("0281")) {
            str_temp = strPrivKey.substring(4, 6);

            if (str_temp.equals("81")) {
                strP = strPrivKey.substring(8, 8 + UcmsMiddlewareConstants.RSA2048KeySize);
                strPrivKey = strPrivKey.substring(8 + UcmsMiddlewareConstants.RSA2048KeySize);
            } else {
                strP = strPrivKey.substring(6, 6 + UcmsMiddlewareConstants.RSA2048KeySize);
                strPrivKey = strPrivKey.substring(6 + UcmsMiddlewareConstants.RSA2048KeySize);
            }

        }


        privP = util.hex1StringToByteArray(strP);

        // Now get Private Q.
        if (strPrivKey.substring(0, 4).equals("0281")) {
            str_temp = strPrivKey.substring(4, 6);

            if (str_temp.equals("81")) {
                strQ = strPrivKey.substring(8, 8 + UcmsMiddlewareConstants.RSA2048KeySize);
                strPrivKey = strPrivKey.substring(8 + UcmsMiddlewareConstants.RSA2048KeySize);
            } else {
                strQ = strPrivKey.substring(6, 6 + UcmsMiddlewareConstants.RSA2048KeySize);
                strPrivKey = strPrivKey.substring(6 + UcmsMiddlewareConstants.RSA2048KeySize);
            }

        }

        privQ = util.hex1StringToByteArray(strQ);



        // Now get Private DP1.
        if (strPrivKey.substring(0, 4).equals("0281")) {
            str_temp = strPrivKey.substring(4, 6);

            if (str_temp.equals("81")) {
                strDP1 = strPrivKey.substring(8, 8 + UcmsMiddlewareConstants.RSA2048KeySize);
                strPrivKey = strPrivKey.substring(8 + UcmsMiddlewareConstants.RSA2048KeySize);
            } else {
                strDP1 = strPrivKey.substring(6, 6 + UcmsMiddlewareConstants.RSA2048KeySize);
                strPrivKey = strPrivKey.substring(6 + UcmsMiddlewareConstants.RSA2048KeySize);
            }

        }

        privDP1 = util.hex1StringToByteArray(strDP1);


        //Now get Private DQ1.
        if (strPrivKey.substring(0, 4).equals("0281")) {
            str_temp = strPrivKey.substring(4, 6);

            if (str_temp.equals("81")) {
                strDQ1 = strPrivKey.substring(8, 8 + UcmsMiddlewareConstants.RSA2048KeySize);
                strPrivKey = strPrivKey.substring(8 + UcmsMiddlewareConstants.RSA2048KeySize);
            } else {
                strDQ1 = strPrivKey.substring(6, 6 + UcmsMiddlewareConstants.RSA2048KeySize);
                strPrivKey = strPrivKey.substring(6 + UcmsMiddlewareConstants.RSA2048KeySize);
            }

        }

        privDQ1 = util.hex1StringToByteArray(strDQ1);


        //Now get Private PQ.
        if (strPrivKey.substring(0, 4).equals("0281")) {
            str_temp = strPrivKey.substring(4, 6);

            if (str_temp.equals("81")) {
                strPQ = strPrivKey.substring(8, 8 + UcmsMiddlewareConstants.RSA2048KeySize);

            } else {
                strPQ = strPrivKey.substring(6, 6 + UcmsMiddlewareConstants.RSA2048KeySize);

            }

        }

        privPQ = util.hex1StringToByteArray(strPQ);


        /*  System.out.println("str_modulus: " +  str_modulus);
        System.out.println("str_expo: " +  str_expo);
        System.out.println("str_p: " +  strP);
        System.out.println("str_Q: " +  strQ);
        System.out.println("str_DP1: " +  strDP1);
        System.out.println("str_DP2: " +  strDQ1);
        System.out.println("str_PQ: " +  strPQ);
        System.out.println("strPrivKey: " +  strPrivKey);
         */
    }

    public void parseRSAKey1024() {
        //First of all get,Public Modulus.

        String str_modulus, str_expo, str_temp, strP = null, strQ = null, strDP1 = null, strDQ1 = null, strPQ = null;
        int i_start, str_tempLen;

        String strPrivKey = util.arrayToHex(privateKey.getEncoded());

        //System.out.println("Private Key : " +  privateKey.toString());
        //System.out.println("Public Key : " +  publicKey.toString());

        System.out.println("strPrivKey: " + strPrivKey);
        //substring(int beginIndex, int endIndex)

        i_start = strPrivKey.toLowerCase().indexOf(startTag.toLowerCase()) + 4;

        if (strPrivKey.substring(i_start, i_start + 2).equals("81")) {
            str_modulus = strPrivKey.substring(i_start + 4, i_start + 4 + UcmsMiddlewareConstants.RSA1024KeySize * 2);
            strPrivKey = strPrivKey.substring(i_start + 4 + UcmsMiddlewareConstants.RSA1024KeySize * 2);

        } else {
            str_modulus = strPrivKey.substring(i_start + 2, i_start + 2 + UcmsMiddlewareConstants.RSA1024KeySize * 2);
            strPrivKey = strPrivKey.substring(i_start + 2 + UcmsMiddlewareConstants.RSA1024KeySize * 2);
        }

        pubModulus = util.hex1StringToByteArray(str_modulus);


        //Get, Public Exponent. 02 03 01 00 01 02 82
        if (  strPrivKey.substring(2, 4).equals("03")) {
            str_expo = strPrivKey.substring(4, 10);
            strPrivKey = strPrivKey.substring(10);

        } else {
            str_expo = strPrivKey.substring(4, 12);
            strPrivKey = strPrivKey.substring(12);
        }

        pubExpo = util.hex1StringToByteArray(str_expo);


        //Get, Private P.
        // First step is to skip next 256 bytes. not required.

        if (strPrivKey.substring(0, 4).equals("0281")) {
            str_temp = strPrivKey.substring(4, 6);
            str_tempLen = Integer.valueOf(str_temp, 16).intValue();

            strPrivKey = strPrivKey.substring(6 + str_tempLen * 2);

        }


        if (strPrivKey.substring(0, 4).equals("0241")) {
            strP = strPrivKey.substring(6, 6 + UcmsMiddlewareConstants.RSA1024KeySize);
            strPrivKey = strPrivKey.substring(6 + UcmsMiddlewareConstants.RSA1024KeySize);

        } else {
            strP = strPrivKey.substring(4, 4 + UcmsMiddlewareConstants.RSA1024KeySize);
            strPrivKey = strPrivKey.substring(4 + UcmsMiddlewareConstants.RSA1024KeySize);
        }

        privP = util.hex1StringToByteArray(strP);

        // Now get Private Q.
        if (strPrivKey.substring(0, 4).equals("0241")) {
            strQ = strPrivKey.substring(6, 6 + UcmsMiddlewareConstants.RSA1024KeySize);
            strPrivKey = strPrivKey.substring(6 + UcmsMiddlewareConstants.RSA1024KeySize);

        } else {
            strQ = strPrivKey.substring(4, 4 + UcmsMiddlewareConstants.RSA1024KeySize);
            strPrivKey = strPrivKey.substring(4 + UcmsMiddlewareConstants.RSA1024KeySize);
        }

        privQ = util.hex1StringToByteArray(strQ);



        // Now get Private DP1.
        if (strPrivKey.substring(0, 4).equals("0241")) {
            strDP1 = strPrivKey.substring(6, 6 + UcmsMiddlewareConstants.RSA1024KeySize);
            strPrivKey = strPrivKey.substring(6 + UcmsMiddlewareConstants.RSA1024KeySize);

        } else {
            strDP1 = strPrivKey.substring(4, 4 + UcmsMiddlewareConstants.RSA1024KeySize);
            strPrivKey = strPrivKey.substring(4 + UcmsMiddlewareConstants.RSA1024KeySize);
        }

        privDP1 = util.hex1StringToByteArray(strDP1);


        //Now get Private DQ1.
        if (strPrivKey.substring(0, 4).equals("0241")) {
            strDQ1 = strPrivKey.substring(6, 6 + UcmsMiddlewareConstants.RSA1024KeySize);
            strPrivKey = strPrivKey.substring(6 + UcmsMiddlewareConstants.RSA1024KeySize);

        } else {
            strDQ1 = strPrivKey.substring(4, 4 + UcmsMiddlewareConstants.RSA1024KeySize);
            strPrivKey = strPrivKey.substring(4 + UcmsMiddlewareConstants.RSA1024KeySize);
        }

        privDQ1 = util.hex1StringToByteArray(strDQ1);


        //Now get Private PQ.
        if (strPrivKey.substring(0, 4).equals("0241")) {
            strPQ = strPrivKey.substring(6, 6 + 128);

        } else {
            strPQ = strPrivKey.substring(4, 4 + UcmsMiddlewareConstants.RSA1024KeySize);
        }

        privPQ = util.hex1StringToByteArray(strPQ);


        /*System.out.println("str_modulus: " + str_modulus);
        System.out.println("str_expo: " + str_expo);
        System.out.println("str_p: " + strP);
        System.out.println("str_Q: " + strQ);
        System.out.println("str_DP1: " + strDP1);
        System.out.println("str_DP2: " + strDQ1);
        System.out.println("str_PQ: " + strPQ);*/


    }


    /* select Security Domain method */
    void selectSecurityDomain(CardChannel channel) throws MiddleWareException {

        // **----------------------------------------------
        // ** Check presence and status of PIV-SD
        // **----------------------------------------------
        // ; Select PIV Application Security Domain using Oberthur registered
        // AID
        // 00 A4 04 00 10 A0 00 00 00 77 01 00 00 06 10 00 FD 00 00 00 27 (9000,
        // 6A82)
        // 00 A4 04 00
        //Recv : 6F 3C 84 07
        //A0 00 00 01 51 00 00
        //A5 31 9F 6E 2A 48 20 50 2B 82 31 80 30 00 63 31 16 00 00 00 0C 00 00 14 32 31 16 14 33 31 16 14 34 31 16 00 00 00 00 14 35 31 16 00 00 00 00 9F 65 01 FF


        StringBuffer sb = new StringBuffer();
        byte[] apdu_security_piv = {0x00, (byte) 0xA4, 0x04, 0x00, 0x10,
            (byte) 0xA0, 0x00, 0x00, 0x00, 0x77, 0x01, 0x00, 0x00, 0x06,
            0x10, 0x00, (byte) 0xFD, 0x00, 0x00, 0x00, 0x27};

        CommandAPDU SEC_APDU_PIV = new CommandAPDU(apdu_security_piv);

        try {
            sendCard.sendCommand(channel, SEC_APDU_PIV, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // ; Select PIV Application Issuer Security Domain
        // 00 A4 04 00 07 A0 00 00 01 51 00 00 (9000, 6A82)

        byte[] apdu_security = {0x00, (byte) 0xA4, 0x04, 0x00, 0x07,
            (byte) 0xA0, 0x00, 0x00, 0x01, 0x51, 0x00, 0x00};

        CommandAPDU SEC_APDU = new CommandAPDU(apdu_security);

        try {
            sendCard.sendCommand(channel, SEC_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public byte[] aesencrypt(byte[] key, byte[] from_card, boolean vector) throws MiddleWareException {

        // Create encrypter/decrypter class
        AesEncrypter encrypter;
        byte[] res = null;
        try {

            if (vector) {
                encrypter = new AesEncrypter(key, IV_AES);
            }
            {
                encrypter = new AesEncrypter(key);
            }

            // Encrypt
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return res;

    }

    /* Adding secure pad to data to be encrypted for AES CBC Mode */
    byte[] add_securepad(byte[] data) {

        byte[] append_80 = {(byte) 0x80};
        byte[] append_00 = {0x00};

        int i_len = data.length;
        int k = i_len % 16; // Divide by 16, because one standard bye block of
        // data is 16.
        int i_cnt = 16 - k; // Calculate how many more bytes required to make a
        // block of 16 bytes.

        if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.ECC256) || keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.ECC384)) {
            if (k == 0) {
                i_cnt = 16;
            }
        } else if (keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.RSA1024) || keyAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
            if (k == 0) {
                i_cnt = 0;
            }

        }



        for (int j = 0; j < i_cnt; j++) {
            // For the first append, "use append_80"
            if (j == 0) {
                data = util.combine_data(data, append_80);

            } else {
                data = util.combine_data(data, append_00);
            }

        }


        return data;

    }


    /*
     * Determine the offset position of last block of 16 byte data from byte[]
     * array.
     */
    int getOffset(byte[] data) {

        // Subtract by 16,because one standard bye block of data is 16.
        int i_len = data.length - 16;

        return i_len;

    }


}