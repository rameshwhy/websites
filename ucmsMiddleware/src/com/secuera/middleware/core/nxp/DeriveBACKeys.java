package com.secuera.middleware.core.nxp;

import java.security.MessageDigest;

import com.secuera.middleware.core.common.CommonUtil;

public class DeriveBACKeys {
	CommonUtil util = new CommonUtil();
	   
	//computing encryption session key 
     // counter value for encryption key is 00000001
     // counter value for mac key is        00000002
	 	  
	  public  byte[] computeENCKey(String MRZ_Information) throws Exception
	  {	
		  byte[] KS_ENC = null;
		  try
		  {
		  byte[] sha1_hash_mrz_inf = computeSHA1(MRZ_Information.getBytes());
		    
		  // take most significant 16bytes to form the kseed 
		   byte[] k_seed = util.extract_data(sha1_hash_mrz_inf, 0, 16);

	       byte[] enc_counter = {(byte)0x00, 0x00, 0x00, 0x01};
	       byte[] combine_seed_c =  util.combine_data(k_seed, enc_counter);
		       
	       // creating sha1 hash of concatinated value
	       byte[] seedHash =  computeSHA1(combine_seed_c);
	       
	       //forming keys from above value
	       byte[] ka = util.extract_data(seedHash, 0, 8);
	       byte[] kb = util.extract_data(seedHash, 8, 8);

	       ka =  adjustDESParity(ka);
	       kb =  adjustDESParity(kb);
       
	        KS_ENC = util.combine_data(ka, kb);
		  }
		  catch(Exception e)
		  {
			  throw new Exception("Error creating BAC ENC Key" + e.getMessage());
		  }
	        
	       
	       
	       return KS_ENC;
	  }
	    
	   //Computing MAC session key
	  public  byte[] computeMACKey(String MRZ_Information) throws Exception
	  {
		  byte[] KS_MAC = null;
		  
		 try{
		  
		  byte[] sha1_hash_mrz_inf = computeSHA1(MRZ_Information.getBytes());
		  
		  // take most significant 16bytes to form the kseed 
		   byte[] k_seed = util.extract_data(sha1_hash_mrz_inf, 0, 16);
		  
	      byte[] mac_counter = {(byte)0x00, 0x00, 0x00, 0x02};
	      byte[] combine_seed_macc =  util.combine_data(k_seed, mac_counter);
	      
	      // creating sha1 hash of concatinated value
	      byte[] seedHashMAC =  computeSHA1(combine_seed_macc);
	      
	      //forming keys from above value
	      byte[] ka_mac = util.extract_data(seedHashMAC, 0, 8);
	      byte[] kb_mac = util.extract_data(seedHashMAC, 8, 8);
	      
	      //adjust the parity bits :  DES uses odd parity
	      ka_mac =  adjustDESParity(ka_mac);
	      kb_mac =  adjustDESParity(kb_mac);
      
	       KS_MAC = util.combine_data(ka_mac, kb_mac);
		 }
		 catch(Exception e)
		 {
			 throw new Exception("Error creating BAC MAC Key" + e.getMessage());
		 }
	      
	      return KS_MAC;
	      
	  }
	  /**
	 	  * DES Keys use the LSB as the odd parity bit. This method can
	 	  * be used enforce correct parity.
	 	  *
	 	  * @param bytes the byte array to set the odd parity on.
	 	  */
	  public byte[] adjustDESParity(byte[] bytes)
	  {
	 	 for (int i = 0; i < bytes.length; i++) {
	 	   int b = bytes[i];
	 	   bytes[i] = (byte)((b & 0xfe) | ((((b >> 1) ^ (b >> 2) ^ (b >> 3) ^ (b >> 4) ^ (b >> 5) ^ (b >> 6) ^ (b >> 7)) ^ 0x01) & 0x01));
	 	  }
	 	 	  return bytes;
	  }
	  
	  // computing hash of the given data
	  public byte[] computeSHA1(byte[] value) throws Exception
	    {
	 	   byte[] digest = null ;  
	 	   try
	 		  {
	 		  MessageDigest md = MessageDigest.getInstance("SHA-1");
	 		  md.reset();
	 		  md.update(value);
	 		  digest  =  md.digest();		  
	 		  }
	 		  catch(Exception e)
	 		  {
	 			 throw new Exception("Error creating SHA-1 hash " + e.getMessage());
	 		  }
	 	   
	 	      return digest;
	    }
	
	
	
}
