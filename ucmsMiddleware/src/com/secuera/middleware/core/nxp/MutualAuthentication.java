/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.nxp;

import com.secuera.middleware.beans.nxp.CredentialKeysNxp;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstantsNXP;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.MacEncrypter;
import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessagesNXP;
import com.secuera.middleware.core.common.RetailMacCalc;
import java.security.SecureRandom;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class MutualAuthentication {
    
    // Create instances of other classes.
    // *************************************************************
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    ExceptionMessagesNXP expMsg = new ExceptionMessagesNXP();
    CredentialKeysNxp credentialKeys = new CredentialKeysNxp();
    ManageApplet ma= new ManageApplet();
    
    
   private byte[] IV = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    
    
    public MutualAuthentication() {
        super();
    }
    
    
     public MutualAuthentication(CredentialKeysNxp crKeys) {
        credentialKeys = crKeys;
        
     }
    
    
    public boolean mutualAuthentication(CardChannel channel ) throws MiddleWareException
    {
       boolean authenticated;
       ma.selectApplet(channel, credentialKeys.getCardManagerAid());
    
       manageSecurityEnv(channel);
       byte[] card_challenge=getChallenge(channel);
       
       authenticated=mutualAuth(channel,card_challenge);
            
        return authenticated; 
    }
    
    
    /* Initialize Update Command method */
    private byte[] getChallenge(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] card_challenge;
        byte[] host_challenge;
        byte[] host_challenge16;
        
        SecureRandom random = new SecureRandom();
        byte[] seed = random.generateSeed(20);
        random.setSeed(seed);
        host_challenge = new byte[8];
        random.nextBytes(host_challenge);

        SecureRandom random16 = new SecureRandom();
        byte[] seed16 = random16.generateSeed(30);
        random16.setSeed(seed16);
        host_challenge16 = new byte[16];
        random.nextBytes(host_challenge16);

        
        byte[] apdu_challenge = {0x00,(byte) 0x84, 0x00, 0x00, 0x08};
        // MAC MODEONLY
               
        CommandAPDU INIT_APDU = new CommandAPDU(apdu_challenge);

        try {
            card_challenge = sendCard.sendCommand(channel, INIT_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessagesNXP.INVALID_CHALLENGE);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }
        
        // Now concatenate card response and host random numbers.
          card_challenge = util.combine_data(host_challenge,card_challenge) ;
          card_challenge = util.combine_data(card_challenge,host_challenge16) ;
        
        
        return card_challenge;

               
    }

  
    private void  manageSecurityEnv(CardChannel channel) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
       
       byte[] apdu_sec1=null;
       byte[] apdu_sec2=null;
        
       byte[] apdu_bac1={0x00, 0x22,0x31,(byte)0xA4,0x06,(byte)0x80, 0x01,0x01,(byte)0x84, 0x01,0x08};
       byte[] apdu_bac2={0x00, 0x22,0x31,(byte)0xB8,0x06,(byte)0x80, 0x01,0x01,(byte)0x84, 0x01,0x08};

       byte[] apdu_epki1={0x00, 0x22,0x31,(byte)0xA4,0x06,(byte)0x80, 0x01,0x40,(byte)0x84, 0x01,0x05};
       byte[] apdu_epki2={0x00, 0x22,0x31,(byte)0xB8,0x06,(byte)0x80, 0x01,0x40,(byte)0x84, 0x01,0x05};
      
       
       byte[] apdu_moc1={0x00,0x22,0x31,(byte)0xA4,0x06,(byte)0x80,0x01,0x40,(byte)0x84,0x01,0x04};
       byte[] apdu_moc2={0x00,0x22,0x31,(byte)0xB8,0x06,(byte)0x80,0x01,0x40,(byte)0x84,0x01,0x04};
       
    
       
       if (credentialKeys.getStr_appletType().equalsIgnoreCase(UcmsMiddlewareConstantsNXP.EACApplet))
       {
           apdu_sec1=apdu_bac1;
           apdu_sec2=apdu_bac2;
       }
       
       if (credentialKeys.getStr_appletType().equalsIgnoreCase(UcmsMiddlewareConstantsNXP.EPKIApplet))
       {
           apdu_sec1=apdu_epki1;
           apdu_sec2=apdu_epki2;
       }
       
       if (credentialKeys.getStr_appletType().equalsIgnoreCase(UcmsMiddlewareConstantsNXP.MOCLibrary))
       {
           apdu_sec1=apdu_moc1;
           apdu_sec2=apdu_moc2;
       }
       
       
        CommandAPDU APDU_SEC = new CommandAPDU(apdu_sec1);
        
        
        try {
            sendCard.sendCommand(channel, APDU_SEC, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        
        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessagesNXP.INVALID_SECUREENV1);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }
        
        CommandAPDU APDU_SEC2 = new CommandAPDU(apdu_sec2);

        //util.arrayToHex(apdu_sec2)
        try {
            sendCard.sendCommand(channel, APDU_SEC2, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        
        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessagesNXP.INVALID_SECUREENV2);
            errMsg.append(" (").append(sb.toString()).append(")");
            throw new MiddleWareException(errMsg.toString());
        }
        
        

    }

    
    private boolean mutualAuth(CardChannel channel,byte[] card_challenge) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] S_ENC_KEY;
        byte[] S_MAC_KEY;
        byte[] res;
    
                                                
        byte[] ENC_KEY_24 = util.combine_data(credentialKeys.getEncKey(),util.extract_data(credentialKeys.getEncKey(), 0, 8));
        S_ENC_KEY = macencrypt(ENC_KEY_24,IV, card_challenge);
        
        
        // MAC_ENC_KEY: Now Compute MAC Session key.
        byte[] MAC_KEY = credentialKeys.getMacKey();
        S_MAC_KEY=retailMac(MAC_KEY, S_ENC_KEY);
        
       byte[] cmd_data= util.combine_data(S_ENC_KEY, S_MAC_KEY);
        
        //00h 82h 00h 00h 28h cmd_data 28h
       byte[] apdu_mutual = {0x00,(byte) 0x82, 0x00, 0x00, 0x28};
       byte [] Le= {0x00};
        
        byte[][] mutual_arrays = new byte[3][];
        mutual_arrays[0] = apdu_mutual;
        mutual_arrays[1] = cmd_data;
        mutual_arrays[2] = Le;

        byte[] mutual = util.combine_data(mutual_arrays);

        CommandAPDU MUTUAL_APDU = new CommandAPDU(mutual);

        try {
            res = sendCard.sendCommand(channel, MUTUAL_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
       
         if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(expMsg.GetErrorMsg(sb.toString(), ""));
            throw new MiddleWareException(errMsg.toString());
            
        }else
         {
             return true;
         }
        
    }    
    
    
    private byte[] macencrypt(byte[] key, byte[] vector, byte[] from_card) throws MiddleWareException {

        MacEncrypter encrypter = new MacEncrypter(key, vector);

        // Encrypt
        byte[] res = null;
        try {
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        return res;

    }

    private byte[] retailMac(byte[] key, byte[] data) throws MiddleWareException {
        byte[] res = null;
        
        RetailMacCalc encrypter = new RetailMacCalc();
        try {
            res = encrypter.retailMac(key, data);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }

        return res;

    }
    
    
    
    
    
    
}
