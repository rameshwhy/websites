/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.validate;

import com.secuera.middleware.cardreader.beans.PrintedInfoBean;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class CardReaderValidator {
     public List validatePrintedInfo(PrintedInfoBean printInfo) {
         
        List myList = new ArrayList(); 
        if (printInfo.getName().length() > 32) {
            myList.add("Invalid Length - Name") ;
        }
        
        if (printInfo.getAffiliation().length() > 20) {
            myList.add("Invalid Length - Affiliation") ;
            
        }
        
        if (printInfo.getCardSerialNumber().length() > 10) {
            myList.add("Invalid Length - Card Serial Number") ;
        }
        
        if (printInfo.getIssuerID().length() > 15) {
            myList.add("Invalid Length - Issuer ID") ;
        }
        
        if (printInfo.getOrganisationLine1().length() > 20) {
            myList.add("Invalid Length - Organization 1") ;
        }
        if (printInfo.getOrganisationLine2().length() > 20) {
            myList.add("Invalid Length - Organization 2") ;
        }
        return myList;
    }
}
