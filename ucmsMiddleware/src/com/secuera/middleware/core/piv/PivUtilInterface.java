/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;
import com.secuera.middleware.beans.piv.FacialBean;
import com.secuera.middleware.beans.piv.FingerPrintBean;
import com.secuera.middleware.beans.piv.PinPolicyBean;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import com.secuera.middleware.beans.piv.PrintedInfoBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.security.PrivateKey;
import java.util.Date;
/**
 *
 * @author admin
 */
public interface PivUtilInterface {
        
	//PIN Management Functions
	public boolean verifyLocalPin(CardChannel channel, String data,Integer pinLength)  throws MiddleWareException;
	public boolean changeLocalPin(CardChannel channel, String oldPin, String newPin,Integer pinLength)  throws MiddleWareException;
	public boolean resetLockPin(CardChannel channel,String unblockPin,String newPin,Integer pinLength )  throws MiddleWareException;
	
	
	public boolean verifyGlobalPin(CardChannel channel, String data,Integer pinLength)  throws MiddleWareException;
	public boolean changeGlobalPin(CardChannel channel, String oldPin, String newPin,Integer pinLength)  throws MiddleWareException;
	
	
	public boolean changePUK(CardChannel channel, String oldPUK, String newPUK,Integer pukLength) throws MiddleWareException;
	
	public byte[] getFASCN(CardChannel channel) throws MiddleWareException;
	
       
	//Facial Image Functions
	public FacialBean readFacialinfo(CardChannel channel) throws MiddleWareException;
	public boolean writeFacialinfo(CardChannel channel, byte[] adminKey, byte[] diversifiedAdminKey, byte[] facialData,
            String str_hex_creation_dt, String str_hex_exp_dt,String valid_from_date, String str_hex_creator,String str_fascn,Date dt_signingtime, byte[] cert,
             PrivateKey privatekey,String algoType) throws MiddleWareException ;
	public String getFacialinfo (CardChannel channel) throws MiddleWareException ;
       
       
	
	//FingerPrint Functions
	public boolean verifyFingerPrint(CardChannel channel, byte[] fingerInfo,byte[] sub_imp_type) throws MiddleWareException;
	
	
	
	public boolean writeCustomInfo(String strhex_data, char container_type,CardChannel channel, byte[] adminKey, byte[] diversifiedAdminKey) throws MiddleWareException;
	public void writeCustomInfo(CardChannel channel, char record_type,String[] str_data_array, byte[] adminKey, byte[] diversifiedAdminKey) throws MiddleWareException;
	public byte[] readCustomInfo(CardChannel channel, char record_type) throws MiddleWareException;
	
	
	public void select(CardChannel channel, byte[] data) throws CardException;
	public void deleteInstance(CardChannel channel) throws MiddleWareException;
	public FingerPrintBean getBioInfo(CardChannel channel) throws MiddleWareException;
	public boolean updateFingerPrint(CardChannel channel, byte[] adminKey, byte[] diversifiedAdminKey, byte[] fingerInfo,String fingerID, byte[] sub_imp_type) throws MiddleWareException;	
	
	

	public String readCIN(CardChannel channel) throws MiddleWareException;
	public String readIIN(CardChannel channel) throws MiddleWareException;	
	public String readCUID(CardChannel channel) throws MiddleWareException;
	
	public PinPolicyBean readPinPolicy (CardChannel channel) throws MiddleWareException;
	public String readBAP(CardChannel channel) throws MiddleWareException;
        public byte[] getCardData(CardChannel channel) throws MiddleWareException ;
        public byte[] getfacialImageDataSecu();
	
}
