/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.common;

import java.security.InvalidAlgorithmParameterException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import com.secuera.middleware.cardreader.entities.MiddleWareException;

/**
 *
 * @author Harpreet Singh
 */
public class AesEncrypter {

    private String ECB_TRANSFORM_STRING = "AES/ECB/NoPadding";
    private String CBC_TRANSFORM_STRING = "AES/CBC/NoPadding";
    Cipher ecipher;
    
    CommonUtil util = new CommonUtil();

    public AesEncrypter(byte[] key_p) throws MiddleWareException {
        try {


            SecretKeySpec keySpec = new SecretKeySpec(key_p, "AES");

            ecipher = Cipher.getInstance(ECB_TRANSFORM_STRING);
            ecipher.init(Cipher.ENCRYPT_MODE, keySpec);

        } catch (javax.crypto.NoSuchPaddingException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (java.security.InvalidKeyException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    public AesEncrypter(byte[] key_p, byte[] IV) throws MiddleWareException {
        try {



            SecretKeySpec keySpec = new SecretKeySpec(key_p, "AES");

            // AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
            IvParameterSpec iv = new IvParameterSpec(IV);

            ecipher = Cipher.getInstance(CBC_TRANSFORM_STRING);
            ecipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);

        } catch (javax.crypto.NoSuchPaddingException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (java.security.InvalidKeyException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (InvalidAlgorithmParameterException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    public byte[] encrypt(byte[] encr) throws MiddleWareException {
        byte[] enc = null;
        byte[] temp_enc = null;

        try {
            for (int i = 0; i < encr.length; i = i + 16) {
                // Encrypt 
                if (i == 0) {
                    //(get the first 16 blocks of 16 bytes)
                    temp_enc = ecipher.doFinal(util.extract_data(encr, i, 16));
                }

                if (i > 0) {
                    byte[] next_block;
                    byte[] res = new byte[16];

                    next_block = util.extract_data(encr, i, 16);

                    for (int j = 0; j < next_block.length; j++) {
                        res[j] = (byte) (next_block[j] ^ temp_enc[j]);
                    }

                    temp_enc = ecipher.doFinal(res);

                }

                if (i > 0) {
                    enc = util.combine_data(enc, temp_enc);
                } else {
                    enc = temp_enc;
                }
            }

            // Encode bytes to base64 to get a string
            // return new sun.misc.BASE64Encoder().encode(enc);

        } catch (javax.crypto.BadPaddingException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (IllegalBlockSizeException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return enc;
    }

   
}
