package com.id3.fingerprint;

import java.io.File;

public final class FingerLicense
{
  static
  {
    String tmpStr = "id3Finger.dll";
       File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
       File localFile = new File(TMP_DIR, tmpStr);
       
    
        System.load(localFile.getAbsolutePath());
  }

  public static String getInstallationCode()
  {
    return nGetInstallationCode();
  }

  public static String getLicensePath()
  {
    return nGetLicensePath();
  }

  public static String getLicenseOwner()
  {
    return nGetLicenseOwner();
  }

  public static int getRemainingDays()
  {
    return nGetRemainingDays();
  }

  public static int getMaxFingerCount()
  {
    return nGetMaxFingerCount();
  }

  public int getMaxUserCount()
  {
    return nGetMaxUserCount();
  }

  public static void checkLicense()
    throws FingerException
  {
    int result = nCheckLicense(null);

    if (result != 0)
      throw new FingerException(result);
  }

  public static void checkLicense(String path)
    throws FingerException
  {
    int result = nCheckLicense(path);

    if (result != 0)
      throw new FingerException(result);
  }

  public static boolean checkModule(int module)
  {
    int result = nCheckModule(module);
    return result == 0;
  }

  private static final native int nInitialize();

  private static final native int nInitializeEx(String paramString);

  private static final native int nDispose();

  private static final native int nCheckLicense(String paramString);

  private static final native int nCheckModule(int paramInt);

  private static final native String nGetInstallationCode();

  private static final native String nGetLicensePath();

  private static final native String nGetLicenseOwner();

  private static final native int nGetRemainingDays();

  private static final native int nGetMaxFingerCount();

  private static final native int nGetMaxUserCount();
}
