package com.secuera.middleware.cardreader.beans;

import com.secuera.middleware.cardreader.core.CommonUtil;
import java.util.Arrays;

public class InitializationBean {

    private byte[] Local_Pin;
    private byte[] Global_Pin;
    private byte[] Unblock_Pin;
    private byte[] Local_PIN_PTC;
    private byte[] Global_PIN_PTC;
    private byte[] Unblock_PIN_PTC;
    private byte[] CARD_MANAGER_AID;
    private byte[] ENC_KEY;
    private byte[] MAC_KEY;
    private byte[] DEK_KEY;
    private byte[] MASTER_KEY;
    private byte[] MASTER_KEY_AES;
    private byte[] PRE_PERSO_PIV_ADMIN_KEY;
    private byte[] PIV_ADMIN_KEY;
    private byte[] PIV_9E08;
    CommonUtil cmnUtil = new CommonUtil();
    private String DiversifyKey;
    
    public byte[] getLocal_Pin() {
        return Local_Pin;
    }

    public void setLocal_Pin(String local_Pin) {
       Local_Pin = cmnUtil.hexStringToByteArray(cmnUtil.stringToHex(local_Pin),8);
        
    }

    public byte[] getGlobal_Pin() {
        return Global_Pin;
    }

    public void setGlobal_Pin(String global_Pin) {
        Global_Pin = cmnUtil.hexStringToByteArray(cmnUtil.stringToHex(global_Pin),8);
      }

    public byte[] getUnblock_Pin() {
        return Unblock_Pin;
    }

    public void setUnblock_Pin(String unblock_Pin) {
          Unblock_Pin = cmnUtil.hexStringToByteArray(cmnUtil.stringToHex(unblock_Pin),8);
       

    }

    public byte[] getLocal_PIN_PTC() {

        return Local_PIN_PTC;
    }

    public void setLocal_PIN_PTC(Integer local_PIN_PTC) {
        if (Integer.toHexString(local_PIN_PTC).length() < 2) {
            Local_PIN_PTC = cmnUtil.hex1ToByteArray("0" + Integer.toHexString(local_PIN_PTC));
        } else {
            Local_PIN_PTC = cmnUtil.hex1ToByteArray(Integer.toHexString(local_PIN_PTC));
        }

    }

    public byte[] getGlobal_PIN_PTC() {
        return Global_PIN_PTC;
    }

    public void setGlobal_PIN_PTC(Integer global_PIN_PTC) {
        if (Integer.toHexString(global_PIN_PTC).length() < 2) {
            Global_PIN_PTC = cmnUtil.hex1ToByteArray("0" + Integer.toHexString(global_PIN_PTC));
        } else {
            Global_PIN_PTC = cmnUtil.hex1ToByteArray(Integer.toHexString(global_PIN_PTC));
        }


    }

    public byte[] getUnblock_PIN_PTC() {
        return Unblock_PIN_PTC;
    }

    public void setUnblock_PIN_PTC(Integer unblock_PIN_PTC) {
        if (Integer.toHexString(unblock_PIN_PTC).length() < 2) {
            Unblock_PIN_PTC = cmnUtil.hex1ToByteArray("0" + Integer.toHexString(unblock_PIN_PTC));
        } else {
            Unblock_PIN_PTC = cmnUtil.hex1ToByteArray(Integer.toHexString(unblock_PIN_PTC));
        }

    }

    public byte[] getCARD_MANAGER_AID() {
        return CARD_MANAGER_AID;
    }

    public void setCARD_MANAGER_AID(String cARD_MANAGER_AID) {
        CARD_MANAGER_AID = cmnUtil.hex1ToByteArray(cARD_MANAGER_AID);
    }

    public byte[] getENC_KEY() {
        return ENC_KEY;
    }

    public void setENC_KEY(String eNC_KEY) {
        ENC_KEY = cmnUtil.hex1ToByteArray(eNC_KEY);
    }

    public byte[] getMAC_KEY() {
        return MAC_KEY;
    }

    public void setMAC_KEY(String mAC_KEY) {
        MAC_KEY = cmnUtil.hex1ToByteArray(mAC_KEY);
    }

    public byte[] getDEK_KEY() {
        return DEK_KEY;
    }

    public void setDEK_KEY(String dEK_KEY) {
        DEK_KEY = cmnUtil.hex1ToByteArray(dEK_KEY);
    }

    public byte[] getMASTER_KEY() {
        return MASTER_KEY;
    }

    public void setMASTER_KEY(String Master_Key) {
        MASTER_KEY = cmnUtil.hex1ToByteArray(Master_Key);
    }

    public byte[] getMASTER_KEY_AES() {
        return MASTER_KEY_AES;
    }

    public void setMASTER_KEY_AES(String mASTER_KEY_AES) {
        MASTER_KEY_AES = cmnUtil.hex1ToByteArray(mASTER_KEY_AES);
    }

    public byte[] getPRE_PERSO_PIV_ADMIN_KEY() {
        return PRE_PERSO_PIV_ADMIN_KEY;
    }

    public void setPRE_PERSO_PIV_ADMIN_KEY(String pRE_PERSO_PIV_ADMIN_KEY) {
        PRE_PERSO_PIV_ADMIN_KEY = cmnUtil.hex1ToByteArray(pRE_PERSO_PIV_ADMIN_KEY);
    }

    public byte[] getPIV_9E08() {
        return PIV_9E08;
    }

    public void setPIV_9E08(byte[] pIV_9E08) {
        PIV_9E08 = pIV_9E08;
    }

    public byte[] getPIV_ADMIN_KEY() {
        return PIV_ADMIN_KEY;
    }

    public void setPIV_ADMIN_KEY(byte[] pIV_ADMIN_KEY) {
        PIV_ADMIN_KEY = pIV_ADMIN_KEY;
    }

    public String getDiversifyKey() {
        return DiversifyKey;
    }

    public void setDiversifyKey(String DiversifyKey) {
        this.DiversifyKey = DiversifyKey;
    }

    @Override
    public String toString() {
        return "InitializationBean [Local_Pin=" + Arrays.toString(Local_Pin)
                + ", Global_Pin=" + Arrays.toString(Global_Pin)
                + ", Unblock_Pin=" + Arrays.toString(Unblock_Pin)
                + ", Local_PIN_PTC=" + Arrays.toString(Local_PIN_PTC)
                + ", Global_PIN_PTC=" + Arrays.toString(Global_PIN_PTC)
                + ", Unblock_PIN_PTC=" + Arrays.toString(Unblock_PIN_PTC)
                + ", CARD_MANAGER_AID=" + Arrays.toString(CARD_MANAGER_AID)
                + ", ENC_KEY=" + Arrays.toString(ENC_KEY) + ", MAC_KEY="
                + Arrays.toString(MAC_KEY) + ", DEK_KEY="
                + Arrays.toString(DEK_KEY) + ", MASTER_KEY="
                + Arrays.toString(MASTER_KEY) + ", MASTER_KEY_AES="
                + Arrays.toString(MASTER_KEY_AES)
                + ", PRE_PERSO_PIV_ADMIN_KEY="
                + Arrays.toString(PRE_PERSO_PIV_ADMIN_KEY) + ", PIV_ADMIN_KEY="
                + Arrays.toString(PIV_ADMIN_KEY) + ", PIV_9E08="
                + Arrays.toString(PIV_9E08) + ", Diversify Key = "
                + DiversifyKey + "]"
                ;
    }
}