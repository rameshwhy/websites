/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 *
 * @author Satish K
 */
public class RetiredKeysBean implements Serializable {
    private String keyId;
    private String algoType;
    private PrivateKey privatekey;
    private PublicKey publickey;
    private byte[] retiredCertificate;
    private String certificateOnCard; // Y means on card N means off card 
   

    public String getAlgoType() {
        return algoType;
    }

    public void setAlgoType(String algoType) {
        this.algoType = algoType;
    }

    public byte[] getRetiredCertificate() {
        return retiredCertificate;
    }

    public void setRetiredCertificate(byte[] retiredCertificate) {
        this.retiredCertificate = retiredCertificate;
    }

   

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public PrivateKey getPrivatekey() {
        return privatekey;
    }

    public void setPrivatekey(PrivateKey privatekey) {
        this.privatekey = privatekey;
    }

    public PublicKey getPublickey() {
        return publickey;
    }

    public void setPublickey(PublicKey publickey) {
        this.publickey = publickey;
    }

    public String getCertificateOnCard() {
        return certificateOnCard;
    }

    public void setCertificateOnCard(String certificateOnCard) {
        this.certificateOnCard = certificateOnCard;
    }

     
    
    
    
}
