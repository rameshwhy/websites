/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.FacialBean;
import java.io.IOException;
import java.util.Arrays;
import javax.smartcardio.*;
import com.secuera.middleware.cardreader.beans.PrintedInfoBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import java.security.PrivateKey;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Harpreet Singh
 */
public class PivUtil implements PivUtilInterface {

    String End_of_record = "EOR";
    //byte[] key_p = { 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23,0x24, 0x25, 0x26, 0x27 };
    String[] next_record = {"01", "02", "03", "04", "05", "06", "07", "08",
        "09", "0A"};
    String[] insurance_meta = {"01", "02", "03", "04", "05", "06", "07", "08",
        "09", "0A"};
    Integer[] insurance_meta_size = {80, 18, 6, 50, 50, 50, 50, 50, 50, 50};
    private byte[] bert_tag = new byte[2];
    private byte[] num_bytes;
    SendCommand sendCard = new SendCommand();
    CommonUtil util = new CommonUtil();
    GeneralAuth auth = new GeneralAuth();
    ExceptionMessages expMsg = new ExceptionMessages();

    public boolean verifyLocalPin(CardChannel channel, String data, Integer pinLength) throws MiddleWareException {

        String errorMsg;
        byte[] pinData;
        boolean rtnvalue = false;

        StringBuffer sb = new StringBuffer();
        byte[] command_data = {0x00, 0x20, 0x00, (byte) 0x80};

        // convert local PIN to Hex
        String hexstr = util.stringToHex(data);
        pinData = util.hexStringToByteArray(hexstr, pinLength);

        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 16) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);

        } else {
            hex_len_pin = Integer.toHexString(len_pin);
        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare verify PIN command
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, pinData);
        CommandAPDU VERIFY_PIN_APDU = new CommandAPDU(command_data);

        // send verify PIN command
        try {
            sendCard.sendCommand(channel, VERIFY_PIN_APDU, sb);

            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean changeLocalPin(CardChannel channel, String oldPin, String newPin, Integer pinLength) throws MiddleWareException {
        byte[] oldpinData;
        byte[] newpinData;
        byte[] pinData;
        String errorMsg;
        boolean rtnvalue = false;
        byte[] command_data = {0x00, 0x24, 0x00, (byte) 0x80};
        StringBuffer sb = new StringBuffer();

        //convert old PIN to Hex
        String hexstr = util.stringToHex(oldPin);
        oldpinData = util.hexStringToByteArray(hexstr, pinLength);

        hexstr = "";
        //convert New PIN to Hex
        hexstr = util.stringToHex(newPin);
        newpinData = util.hexStringToByteArray(hexstr, pinLength);

        //Calculate length of Data to be sent.
        pinData = util.combine_data(oldpinData, newpinData);
        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);

        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare verify PIN Change commands
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, oldpinData);
        command_data = util.combine_data(command_data, newpinData);
        CommandAPDU CHANGE_PIN_APDU = new CommandAPDU(command_data);

        System.out.println("Command " + util.arrayToHex(command_data));

        // send PIN Change command
        try {
            sendCard.sendCommand(channel, CHANGE_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean resetLockPin(CardChannel channel, String unblockPin, String newPin, Integer pinLength) throws MiddleWareException {
        boolean rtnvalue = false;
        String errorMsg;
        byte[] unBlockData;
        byte[] newpinData;
        byte[] pinData;
        byte[] command_data = {0x00, 0x2C, 0x00, (byte) 0x80};
        StringBuffer sb = new StringBuffer();

        // convert Unblock PIN to Hex
        String hexstr = util.stringToHex(unblockPin);
        unBlockData = util.hexStringToByteArray(hexstr, pinLength);

        hexstr = "";
        // convert New PIN to Hex
        hexstr = util.stringToHex(newPin);
        newpinData = util.hexStringToByteArray(hexstr, pinLength);

        //Calculate length of Data to be sent.
        pinData = util.combine_data(unBlockData, newpinData);
        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);
        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare Unblock PIN commands
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, unBlockData);
        command_data = util.combine_data(command_data, newpinData);
        CommandAPDU CHANGE_PIN_APDU = new CommandAPDU(command_data);

        // send PIN Change command
        try {
            sendCard.sendCommand(channel, CHANGE_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean verifyGlobalPin(CardChannel channel, String data, Integer pinLength) throws MiddleWareException {
        byte[] pinData;
        String errorMsg;
        boolean rtnvalue = false;
        byte[] command_data = {0x00, 0x20, 0x00, 0x00};
        StringBuffer sb = new StringBuffer();

        // convert Global PIN to Hex
        String hexstr = util.stringToHex(data);
        pinData = util.hexStringToByteArray(hexstr, pinLength);

        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);
        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare verify PIN command
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, pinData);
        CommandAPDU VERIFY_PIN_APDU = new CommandAPDU(command_data);


        // send verify PIN command
        try {

            sendCard.sendCommand(channel, VERIFY_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;
        } catch (CardException e) {

            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean changeGlobalPin(CardChannel channel, String oldPin, String newPin, Integer pinLength) throws MiddleWareException {
        byte[] oldpinData;
        byte[] newpinData;
        byte[] pinData;
        String errorMsg;
        boolean rtnvalue = false;

        byte[] command_data = {0x00, 0x24, 0x00, 0x00};
        StringBuffer sb = new StringBuffer();

        // convert old PIN to Hex
        String hexstr = util.stringToHex(oldPin);
        oldpinData = util.hexStringToByteArray(hexstr, pinLength);

        hexstr = "";
        // convert New PIN to Hex
        hexstr = util.stringToHex(newPin);
        newpinData = util.hexStringToByteArray(hexstr, pinLength);

        //Calculate length of Data to be sent.
        pinData = util.combine_data(oldpinData, newpinData);
        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);

        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare verify PIN Change commands
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, oldpinData);
        command_data = util.combine_data(command_data, newpinData);
        CommandAPDU CHANGE_PIN_APDU = new CommandAPDU(command_data);

        // send PIN Change command
        try {
            sendCard.sendCommand(channel, CHANGE_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean changePUK(CardChannel channel, String oldPUK, String newPUK, Integer pukLength) throws MiddleWareException {
        byte[] oldpinData;
        byte[] newpinData;
        byte[] pinData;
        String errorMsg;
        boolean rtnvalue;
        byte[] command_data = {0x00, 0x24, 0x00, (byte) 0x81};
        StringBuffer sb = new StringBuffer();

        // convert old PUK PIN to Hex
        String hexstr = util.stringToHex(oldPUK);
        oldpinData = util.hexStringToByteArray(hexstr, pukLength);

        hexstr = "";
        // convert New PUK PIN to Hex
        hexstr = util.stringToHex(newPUK);
        newpinData = util.hexStringToByteArray(hexstr, pukLength);

        //Calculate length of Data to be sent.
        pinData = util.combine_data(oldpinData, newpinData);
        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);

        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare Change PUK commands
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, oldpinData);
        command_data = util.combine_data(command_data, newpinData);

        System.out.println(util.arrayToHex(command_data));

        CommandAPDU CHANGE_PIN_APDU = new CommandAPDU(command_data);


        // send Change PUK command
        try {

            sendCard.sendCommand(channel, CHANGE_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    /*
    
    public static String convertFascn(String binValue) {
    String retValue="";
    
    if (binValue.matches("00001")) {
    retValue = "0";
    } else if (binValue.matches("10000")) {
    retValue = "1";
    } else if (binValue.matches("01000")) {
    retValue = "2";
    } else if (binValue.matches("11001")){
    retValue = "3";
    } else if (binValue.matches("00100")) {
    retValue = "4";
    } else if (binValue.matches("10101")) {
    retValue = "5";			
    } else if (binValue.matches("01101")) {
    retValue = "6";			
    } else if (binValue.matches("11100")) {
    retValue = "7";			
    } else if (binValue.matches("00010")) {
    retValue = "8";			
    } else if (binValue.matches("10011")) {
    retValue = "9";	
    }
    return retValue;
    
    }
    
    public static String getFASCN(String infascn) {
    // d4e739da739ced39ce739da1685a1082108ce73984119257fc
    //String hex = "d4e739da739ced39ce739da1685a1082108ce73984119257fc";
    String bin = "";
    String binFragment = "";
    int iHex;
    infascn = infascn.trim();
    infascn = infascn.replaceFirst("0x", "");
    
    for (int i = 0; i < infascn.length(); i++) {
    iHex = Integer.parseInt("" + infascn.charAt(i), 16);
    binFragment = Integer.toBinaryString(iHex);
    
    while (binFragment.length() < 4) {
    binFragment = "0" + binFragment;
    }
    bin += binFragment;
    }
    String miscfascn = "";
    
    String fascn = "";
    while (bin.length()> 0) {
    miscfascn = bin.substring(0,5);
    bin=bin.substring(5);
    
    fascn = fascn+convertFascn(miscfascn);
    
    }
    return fascn.substring(0,fascn.length()-1);
    }
     */
    
    /*
    public ChuidBean readChuid(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String returnValue;
        String errorMsg;
        ChuidBean chuid;
        boolean result;

        StringBuffer sb = new StringBuffer();
        byte[] apdu_chuid = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05, 0x5C,
            0x03, (byte) 0x5F, (byte) 0xC1, 0x02}; // BERT-TLV CHUID tag

        try {
            CommandAPDU CHUID_APDU = new CommandAPDU(apdu_chuid);

            res = sendCard.sendCommand(channel, CHUID_APDU, sb);

            //raise error if unable to Read CHUID
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "CHUID");
                throw new MiddleWareException(errorMsg);
            }
            if (res == null || res.length == 0) {
                chuid = new ChuidBean();

            } else {
                //convert byte array to string & remove parity bits
                returnValue = util.arrayToHex(res);
                System.out.println(returnValue);

                if (returnValue.substring(2, 4).equals("82")) {
                    returnValue = returnValue.substring(16);
                } else if (returnValue.substring(2, 4).equals("81")) {
                    returnValue = returnValue.substring(12);
                } else {
                    returnValue = returnValue.substring(10);
                }
                //53 82  09 ba ee02 09b6               
                //bert tag = 53   length type = 82   lenght = 09 ba  chuid tag= ee02   length after chuid tag 09b6

                //parse CHUID
                chuid = parseChuid(returnValue);

                //Verify Signature
                ParseAsmObject parseASm = new ParseAsmObject();
                result = parseASm.parseChuidObj(returnValue);
                
                chuid.setSignatureVerified(result);
            }
            return chuid;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }
     
     */
    
    

    
    @Override
    public byte[] getFASCN(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String returnValue;
        String errorMsg;
        byte[] FASCN = null;


        StringBuffer sb = new StringBuffer();
        byte[] apdu_chuid = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05, 0x5C,
            0x03, (byte) 0x5F, (byte) 0xC1, 0x02}; // BERT-TLV CHUID tag



        try {
            CommandAPDU CHUID_APDU = new CommandAPDU(apdu_chuid);

            res = sendCard.sendCommand(channel, CHUID_APDU, sb);

            //raise error if unable to Read FASCN
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "FASCN");
                throw new MiddleWareException(errorMsg);
            }
            if (res == null || res.length == 0) {
                FASCN = null;

            } else {
                //convert byte array to string & remove parity bits
                returnValue = util.arrayToHex(res);

                if (returnValue.substring(2, 4).equals("82")) {
                    returnValue = returnValue.substring(16);
                } else if (returnValue.substring(2, 4).equals("81")) {
                    returnValue = returnValue.substring(12);
                } else {
                    returnValue = returnValue.substring(10);
                }



                FASCN = util.hex1StringToByteArray(returnValue.substring(4, 54));


            }
            return FASCN;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    public FacialBean readFacialinfo(CardChannel channel) throws MiddleWareException {
        // Send : 00 CB 3F FF 00 00 05
        // 5C 03 5F C1 08
        // Recv :
        // 53 82 19 94 BC 82 19 8E 03 0D 00 00 16 CA 02 6C 00 1B 05 01 14 07 04
        // 13 0B 16 21 5A 14 07
        // 04 13 0B 16 21 5A 14 0B 04 13 0B 16 21 5A 00 00 02 20 FE 4E 49 53 54
        // 20 43 72 65 61 74 6F
        // 72 00 00 00 00 00 00 D4 E7 39 DA 73 9C ED 39 CE 73 9D A1 68 5A 08 C9
        // 2A DE 0A 61 84 E7 39
        // C3 E2 00 00 00 00 // 96 bytes till this point
        // 46 41 43 00 30 31 30 00 00 00 16 CA 00 01 00 00 16
        // BC 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 01 01 00 01 00
        // 01 02 00 00
        //byte[] jpegimg_start = { (byte) 0xFF, (byte) 0xD8 };
        //byte[] jpegimg_end = { (byte) 0xFF, (byte) 0xD9 };
        // 78 bytes CBEFF Header.

        FacialBean facialBean = new FacialBean();
        String str_facialInfo;

        try {
                str_facialInfo = getFacialinfo(channel);
            
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
            
        }
        
        
        ManageCHUID chuid= new ManageCHUID();
        String strChuid = chuid.getChuid(channel);
        
        verifyFacialSignature verifyFacial = new verifyFacialSignature();
        facialBean = verifyFacial.verifySig(str_facialInfo,strChuid);
        
            
        return facialBean;
    }
    @Override
    public boolean writeFacialinfo(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY, byte[] facialData,
            String str_hex_creation_dt, String str_hex_exp_dt,String valid_from_date, String str_hex_creator, String str_fascn,Date dt_signingtime, byte[] cert,
            PrivateKey privatekey) throws MiddleWareException {


        boolean rtnvalue = true;
        StringBuffer sb = new StringBuffer();
        byte[] facial_cont_data = null;
        //byte[] facialData = util.hex1ToByteArray(facial_data);
        byte[] creator = util.hex1ToByteArray(str_hex_creator);
        byte[] error_tag = {(byte) 0xFE, 0x00};
        //byte[] image_tag = {(byte) 0xBC};

        GenFacCbeffHeader cbeff = new GenFacCbeffHeader();
        
        try {
            facial_cont_data = cbeff.getFacialHeader(facialData, str_hex_creation_dt, str_hex_exp_dt, valid_from_date,creator, str_fascn, dt_signingtime,privatekey, cert);
        } catch (IOException ex) {
            Logger.getLogger(PivUtil.class.getName()).log(Level.SEVERE, null, ex);
        }


       // Integer len = facial_cont_data.length;

        //System.out.println("Length of facial data " + len);

        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        System.out.println("after auth");


        //////////////////////////////////////////////////////	
        byte[] cont_id = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x08}; //For Facial Container
        byte[] apdu_facial = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};
        ///////////////////////////////////////////////////////


        //Calculate size of the facial record to be stored, with Image Tag

       byte[] apdu_facial_data = util.combine_data(facial_cont_data,error_tag);
        
        //Now Calculate size of the facial record to be stored
        Integer len_total_bert = apdu_facial_data.length;

        calcBertValue(len_total_bert, true);

        byte[] bert_tag_total = bert_tag;
        byte[] byte_fac_len = num_bytes;

        apdu_facial_data = util.combine_data(byte_fac_len, apdu_facial_data);
        apdu_facial_data = util.combine_data(bert_tag_total, apdu_facial_data);


        apdu_facial_data = util.combine_data(cont_id, apdu_facial_data);

        Integer len_total_apdu = apdu_facial_data.length;

        calcBertValue(len_total_apdu, false);

        byte[] total_size = num_bytes;
        byte[] zero_size = {0x00};

        if (total_size.length > 1) {
            total_size = util.combine_data(zero_size, total_size);
        }


        apdu_facial = util.combine_data(apdu_facial, total_size);
        apdu_facial = util.combine_data(apdu_facial, apdu_facial_data);


        System.out.println(util.arrayToHex(apdu_facial));

        CommandAPDU READ_APDU = new CommandAPDU(apdu_facial);

        try {

            sendCard.sendCommand(channel, READ_APDU, sb);

            //Raise Error if APDU command is not successful
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();
                errMsg.append("Unable to write Facial Image");
                errMsg.append(" (").append(sb.toString()).append(")");
                throw new MiddleWareException(errMsg.toString());
            }
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        return rtnvalue;
    }
    
    public String getFacialinfo(CardChannel channel) throws MiddleWareException {
        // Send : 00 CB 3F FF 00 00 05
        // 5C 03 5F C1 08
        // Recv :
        // 53 82 19 94 BC 82 19 8E 03 0D 00 00 16 CA 02 6C 00 1B 05 01 14 07 04
        // 13 0B 16 21 5A 14 07
        // 04 13 0B 16 21 5A 14 0B 04 13 0B 16 21 5A 00 00 02 20 FE 4E 49 53 54
        // 20 43 72 65 61 74 6F
        // 72 00 00 00 00 00 00 D4 E7 39 DA 73 9C ED 39 CE 73 9D A1 68 5A 08 C9
        // 2A DE 0A 61 84 E7 39
        // C3 E2 00 00 00 00 // 96 bytes till this point
        // 46 41 43 00 30 31 30 00 00 00 16 CA 00 01 00 00 16
        // BC 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 01 01 00 01 00
        // 01 02 00 00
        //byte[] jpegimg_start = { (byte) 0xFF, (byte) 0xD8 };
        //byte[] jpegimg_end = { (byte) 0xFF, (byte) 0xD9 };
        // 78 bytes CBEFF Header.

        byte[] res = null;
        String returnValue;
        byte[] apdu_facial = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00,
            0x00, 0x05, 0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x08};

        StringBuffer sb = new StringBuffer();

        try {
            // Create APDU Object
            CommandAPDU READ_APDU = new CommandAPDU(apdu_facial);
            
            res = sendCard.sendCommand(channel, READ_APDU, sb);

            //Raise Error if APDU command is not successful
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();
                errMsg.append("Unable to read Facial Image");
                errMsg.append(" (").append(sb.toString()).append(")");
                throw new MiddleWareException(errMsg.toString());
            }
            
            if (res == null || res.length == 0) {
                returnValue ="";

            } else {
                //convert byte array to string & remove parity bits
                returnValue = util.arrayToHex(res);
                
                //remove leading characters
                if (returnValue.substring(2, 4).equals("82")) {
                    returnValue = returnValue.substring(8);
                } else if (returnValue.substring(2, 4).equals("81")) {
                    returnValue = returnValue.substring(6);
                } else {
                    returnValue = returnValue.substring(4);
                }
                
            }
            return returnValue;
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }
    
    
    

    

    

    @Override
    public boolean verifyFingerPrint(CardChannel channel, byte[] fingerInfo, byte[] sub_imp_type) throws MiddleWareException {
        boolean rtnvalue = false;
        StringBuffer sb = new StringBuffer();
        byte[] command_data;
        byte[] zero_size = {0x00};

        byte[] apdu_cls = {0x00, 0x21, 0x00, 0x00};
        byte[] var_len_1;
        byte[] apdu_tag = {0x7F, 0x2E};
        byte[] var_len_2;
        byte[] minutiae_tag = {(byte) 0x81};
        byte[] apdu_ber_tag = {(byte) 0x81};
        byte[] var_len_3;
        String tot_hex_data_size;
        int add_len_1;

        // byte[] apdu_biotype = {(byte)0x95, 0x03, 0x08} ;
        // byte[] sub_bio_type ={0x09};

        if ((fingerInfo.length) > 255 && (fingerInfo.length) < 4096) {

            tot_hex_data_size = "0" + Integer.toHexString((fingerInfo.length));
            add_len_1 = 3;
        } else {
            tot_hex_data_size = Integer.toHexString((fingerInfo.length));
            add_len_1 = 2;
        }

        var_len_3 = util.hex1ToByteArray(tot_hex_data_size);

        // Var_len_2 is greater than 127, then ber-tlv tag 81 must be added.

        if (fingerInfo.length + add_len_1 > 127) {

            var_len_3 = util.combine_data(apdu_ber_tag, var_len_3);
            add_len_1 += 1;
        }

        // Following condition makes sure that converted hex string is always
        // even.
        String hex_var_len_2;
        int add_len_2; // This is for tag "7F2E"...
        if ((fingerInfo.length + add_len_1) > 255
                && (fingerInfo.length + add_len_1) < 4096) {

            hex_var_len_2 = "0"
                    + Integer.toHexString((fingerInfo.length + add_len_1));
            add_len_2 = 4;
        } else {
            hex_var_len_2 = Integer.toHexString((fingerInfo.length + add_len_1));
            add_len_2 = 3;
        }

        var_len_2 = util.hex1ToByteArray(hex_var_len_2);

        // Var_len_2 is greater than 127, then ber-tlv tag 81 must be added.
        int add_len_3 = 0;
        if (fingerInfo.length + add_len_1 > 127) {

            var_len_2 = util.combine_data(apdu_ber_tag, var_len_2);
            add_len_3 = 1;
        }

        // Now calculate the LC Value of commands

        String hex_var_len_1;

        if ((fingerInfo.length + add_len_1 + add_len_2 + add_len_3) > 255
                && (fingerInfo.length + add_len_1 + add_len_2 + add_len_3) < 4096) {

            hex_var_len_1 = "0"
                    + Integer.toHexString((fingerInfo.length + add_len_1
                    + add_len_2 + add_len_3));

        } else {
            hex_var_len_1 = Integer.toHexString((fingerInfo.length + add_len_1
                    + add_len_2 + add_len_3));

        }

        var_len_1 = util.hex1ToByteArray(hex_var_len_1);

        if (var_len_1.length > 1) {
            var_len_1 = util.combine_data(zero_size, var_len_1);
        }

        command_data = util.combine_data(apdu_cls, var_len_1);
        command_data = util.combine_data(command_data, apdu_tag);
        command_data = util.combine_data(command_data, var_len_2);
        command_data = util.combine_data(command_data, minutiae_tag);
        command_data = util.combine_data(command_data, var_len_3);
        command_data = util.combine_data(command_data, fingerInfo);
        // command_data = util.combine_data(command_data, apdu_biotype);
        // command_data = util.combine_data(command_data, sub_bio_type);
        // command_data = util.combine_data(command_data, sub_imp_type);

        // System.out.println("Final Command " + util.arrayToHex(command_data
        // ));

        CommandAPDU VERIFY_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, VERIFY_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (sb.toString().equals("9000")) {
            // sat123 Credential.setfinger_print(fingerInfo);
            rtnvalue = true;
        }
        return rtnvalue;

    }

    @Override
    public boolean writeCustomInfo(String strhex_data, char container_type,
            CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY) throws MiddleWareException {
        // TO write Customised data in the card Container.
        boolean rtnvalue = false;
        byte[] cont_id;
        byte[] newData;
        byte[] command_data;
        StringBuffer sb = new StringBuffer();
        System.out.println("received Hex  " + strhex_data);
        System.out.println("received Hex string length " + strhex_data.length());

        int str_len = 520; // Minimum data String has to be of this length.
        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {
            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        Integer hex_len = strhex_data.length(); // For Bert Tag.

        if (hex_len < str_len) {
            strhex_data = util.formatHexString(str_len, strhex_data);
        }

        newData = util.hex1ToByteArray(strhex_data);

        int len = newData.length;
        System.out.println("Length of byte newData " + len);

        byte[] person_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0D}; // Personal
        // info
        // container
        // identifier.
        byte[] med_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0E}; // Medical
        // container
        // identifier.
        byte[] ins_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0F}; // Insurance
        // container
        // identifier.
        byte[] fam_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x10}; // Family
        // History
        // container
        // identifier.

        if (container_type == 'P') {
            cont_id = person_id;
        } else if (container_type == 'M') {
            cont_id = med_id;
        } else if (container_type == 'I') {
            cont_id = ins_id;
        } else {
            cont_id = fam_id;
        }

        // UPDATE Container APDU Command
        String tot_hex_size;
        String tot_hex_data_size;
        byte[] zero_size = {0x00};
        Integer add_len;

        byte[] apdu_command = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};

        // Now Determine value of Bert Tag
        byte[] bert_tag = new byte[2];
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for
        // size of data
        // bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size
        // of data bytes

        if (len > 255) {

            bert_tag = bert_tag_size_big;
            add_len = 9; // For larger data we need one extra byte to store
            // length.
        } else {
            bert_tag = bert_tag_size_small;
            add_len = 7;
        }

        // Following condition makes sure that converted hex string is always
        // even.
        if ((len + add_len) > 255 && (len + add_len) < 4096) {
            tot_hex_size = "0" + Integer.toHexString(len + add_len);

        } else {
            tot_hex_size = Integer.toHexString(len + add_len);

        }

        byte[] total_size = util.hex1ToByteArray(tot_hex_size);

        // Now calculate actual data size being stored
        // Following condition makes sure that converted hex string is always
        // even.
        if (len > 255 && len < 4096) {

            tot_hex_data_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_data_size = Integer.toHexString(len);

        }

        byte[] num_of_bytes = util.hex1ToByteArray(tot_hex_data_size);

        // System.out.println("total_hex_size: " + tot_hex_size);
        // System.out.println("total_Byte_size: " +
        // util.arrayToHex(total_size));

        if (total_size.length > 1) {
            total_size = util.combine_data(zero_size, total_size);
        }

        command_data = util.combine_data(apdu_command, total_size);
        command_data = util.combine_data(command_data, cont_id);
        command_data = util.combine_data(command_data, bert_tag);
        command_data = util.combine_data(command_data, num_of_bytes);
        command_data = util.combine_data(command_data, newData);

        System.out.println("Final Command " + util.arrayToHex(command_data));

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (sb.toString().equals("9000")) {
            rtnvalue = true;
        }
        return rtnvalue;

    }

    @Override
    public void writeCustomInfo(CardChannel channel, char record_type,
            String[] str_data_array, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY) throws MiddleWareException {

        // TO write Insurance Container.
        // 5FC10D
        // 5FC10E
        int j = 0;
        int n = 0;
        int k = 0;

        byte[] cont_id;
        String str_data;
        String hex_data = null;
        String total_hex_data = null;
        byte[] newData;
        byte[] command_data;
        StringBuffer sb = new StringBuffer();
        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        for (int i = 0; i < str_data_array.length; i += 1) {

            str_data = str_data_array[i];

            if (str_data == End_of_record) {
                n = j; // Store the counter j in n before incrementing.
                j++; // Increment to get to next record identifier.
                k = 0; // Initialize the meta data array pointer, so that next
                // record's
                // field identifier's can be appended.
                continue;
            }

            hex_data = util.stringToHex(str_data);
            hex_data = util.formatHexString(insurance_meta_size[k], hex_data);

            if (j == 0 && i == 0) {
                hex_data = next_record[j] + insurance_meta[k] + hex_data;
            } else if (j != n) {
                hex_data = next_record[j] + insurance_meta[k] + hex_data;
                n = j; // again assign j to n, so that next record field only
                // gets
                // appended, when next record is detected.
            } else {
                hex_data = insurance_meta[k] + hex_data;
            }

            if (i == 0) {
                total_hex_data = hex_data;
            } else {
                total_hex_data = total_hex_data + hex_data;
            }

            k++; // Increment meta data counter to get the meta data for next
            // field.

        }

        // total_hex_data:
        // 01010148617270726565742053696e67682020202020202020202020202020202020202020202020202020
        // 02303831353139373220
        // 03333920
        // 020152616a696e6465722053696e67682020202020202020202020202020202020202020202020202020
        // 02303931353139363120
        // 03343520

        System.out.println("total_hex_data: " + total_hex_data);

        newData = util.hexToByteArray(total_hex_data);

        Integer len = newData.length;

        byte[] person_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0D}; // Personal
        // info
        // container
        // identifier.
        byte[] med_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0E}; // Medical
        // container
        // identifier.
        byte[] ins_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0F}; // Insurance
        // container
        // identifier.

        if (record_type == 'P') {
            cont_id = person_id;
        } else if (record_type == 'M') {
            cont_id = med_id;
        } else {
            cont_id = ins_id;
        }

        // UPDATE Container APDU Command
        byte[] apdu_command = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF, 0x00};
        byte[] total_size = util.hexToByteArray(Integer.toHexString(len + 8));
        byte[] bert_tag = {0x53, (byte) 0x81}; // identifier for size of data
        // bytes
        byte[] num_of_bytes = util.hexToByteArray(Integer.toHexString(len));

        command_data = util.combine_data(apdu_command, total_size);
        command_data = util.combine_data(command_data, cont_id);
        command_data = util.combine_data(command_data, bert_tag);
        command_data = util.combine_data(command_data, num_of_bytes);
        command_data = util.combine_data(command_data, newData);

        // System.out.println("Final Command " + util.arrayToHex(command_data
        // ));

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {

            throw new MiddleWareException(e.getMessage());
        }

    }

    @Override
    public byte[] readCustomInfo(CardChannel channel, char record_type) throws MiddleWareException {

        // TO read Insurance Container.
        // 5FC10D
        // 5FC10E
        StringBuffer sb = new StringBuffer();
        byte[] res = null;
        byte[] cont_id;
        byte[] command_data;

        byte[] person_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0D}; // Personal
        // info
        // container
        // identifier.
        byte[] med_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0E}; // Medical
        // container
        // identifier.
        byte[] ins_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0F}; // Insurance
        // container
        // identifier.

        if (record_type == 'P') {
            cont_id = person_id;
        } else if (record_type == 'M') {
            cont_id = med_id;
        } else {
            cont_id = ins_id;
        }

        // Read Container APDU Command

        // byte[] apdu_read = { 0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05 }
        // 0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x0D }; // BERT-TLV

        byte[] apdu_command = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05};

        command_data = util.combine_data(apdu_command, cont_id);

        CommandAPDU READ_APDU = new CommandAPDU(command_data);

        try {

            res = sendCard.sendCommand(channel, READ_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        System.out.println("Final read " + util.arrayToHex(res));
        return res;

    }

    @Override
    public boolean updateFingerPrint(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY, byte[] fingerInfo,
            String fgType, byte[] sub_imp_type) throws MiddleWareException {

        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        // byte[] sub_bio_type = getFingerType(fgType);
        byte[] sub_bio_type = getFingerID(fgType);

        boolean rtnvalue = false;
        StringBuffer sb = new StringBuffer();
        byte[] command_data;
        byte[] zero_size = {0x00};

        byte[] apdu_cls = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};
        byte[] var_len_1;
        byte[] apdu_tag = {0x7F, 0x2E};
        byte[] var_len_2;
        byte[] minutiae_tag = {(byte) 0x81};

        byte[] apdu_ber_tag = {(byte) 0x81};
        byte[] var_len_3;

        byte[] apdu_biotype = {(byte) 0x95, 0x03, 0x08};
        // byte[] sub_imp_type = {0x00} ;

        // Now calculate actual data size being stored
        // Following condition makes sure that converted hex string is always
        // even.

        String tot_hex_data_size;
        int add_len_1 = 0;
        if ((fingerInfo.length) > 255 && (fingerInfo.length) < 4096) {
            tot_hex_data_size = "0" + Integer.toHexString((fingerInfo.length));
            add_len_1 = 2;
        } else {
            tot_hex_data_size = Integer.toHexString((fingerInfo.length));
            add_len_1 = 1;
        }

        var_len_3 = util.hex1ToByteArray(tot_hex_data_size);

        // Var_len_2 is greater than 127, then ber-tlv tag 81 must be added.

        if (fingerInfo.length > 127) {
            var_len_3 = util.combine_data(apdu_ber_tag, var_len_3);
            add_len_1 += 1;
        }

        // Following condition makes sure that converted hex string is always
        // even.
        String hex_var_len_2;
        int add_len_2; // This is for tag "7F2E"...
        if ((fingerInfo.length + 5 + add_len_1) > 255
                && (fingerInfo.length + 5 + add_len_1) < 4096) {

            hex_var_len_2 = "0"
                    + Integer.toHexString((fingerInfo.length + 5 + add_len_1));
            add_len_2 = 4;
        } else {
            hex_var_len_2 = Integer.toHexString((fingerInfo.length + 5 + add_len_1));
            add_len_2 = 3;
        }

        var_len_2 = util.hex1ToByteArray(hex_var_len_2);

        // Var_len_2 is greater than 127, then ber-tlv tag 81 must be added.
        int add_len_3 = 0;
        if (fingerInfo.length + 5 + add_len_1 > 127) {

            var_len_2 = util.combine_data(apdu_ber_tag, var_len_2);
            add_len_3 = 1;
        }

        // Now calculate the LC Value of commands

        String hex_var_len_1;

        if ((fingerInfo.length + 5 + add_len_1 + add_len_2 + add_len_3) > 255
                && (fingerInfo.length + 5 + add_len_1 + add_len_2 + add_len_3) < 4096) {

            hex_var_len_1 = "0"
                    + Integer.toHexString((fingerInfo.length + 5 + add_len_1
                    + add_len_2 + add_len_3));

        } else {
            hex_var_len_1 = Integer.toHexString((fingerInfo.length + 5
                    + add_len_1 + add_len_2 + add_len_3));

        }

        var_len_1 = util.hex1ToByteArray(hex_var_len_1);

        if (var_len_1.length > 1) {
            var_len_1 = util.combine_data(zero_size, var_len_1);
        }

        command_data = util.combine_data(apdu_cls, var_len_1);
        command_data = util.combine_data(command_data, apdu_tag);
        command_data = util.combine_data(command_data, var_len_2);
        command_data = util.combine_data(command_data, minutiae_tag);
        command_data = util.combine_data(command_data, var_len_3);
        command_data = util.combine_data(command_data, fingerInfo);
        command_data = util.combine_data(command_data, apdu_biotype);
        command_data = util.combine_data(command_data, sub_bio_type);
        command_data = util.combine_data(command_data, sub_imp_type);

        System.out.println("Final COmmand " + util.arrayToHex(command_data));

        // 00 db 3f ff
        // 73
        // 7f 2e 70 81 69
        // 3e115f70227858257a2c29654a337b683658503e5b3c43664d485d22506b41546e44547046547318576b6159b06a5a70785e704860504662965a63ac3165722967b03d6ab9236daf4f6e655c746b1c78af4a78a2177aaf657bac2b7cb471886d4f8da8298e7558956a
        // 950308
        // 0000

        // 00 db 3f ff
        // 87 7f2e 81 83
        // 81 81 7b
        // 4e1c795d20572428654229784232582a3b80343d5e523d55364a9b2e4e716150714e51702b53721e55af5a57b0385b54365d96555faf32657e4965ae2a69b5196c72416d681670b12770b47672704c7aab427d89627d6f6d7eb12a83775283ac268776408b60718b8f2d8f9534917a5891ad3c9a80479d6236a17d9503080a00

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (sb.toString().equals("9000")) {
            rtnvalue = true;
        }

        return rtnvalue;

    }

    public int getFingerType(String str_fgType) {
        int sub_bio;
        /*
         * Finger Number Finger Type Button on each finger flag for each finger
         * Code HEX
         * --------------------------------------------------------------
         * ------------------------------ 1 RT Jbutton_rightThumb
         * jLabel_rightThumb ‘05’ {0x05} 2 RI Jbutton_rightIndex
         * jLabel_rightIndex ‘09’ {0x09} 3 RM Jbutton_rightMiddle
         * jLabel_rightMiddle ‘0D’ {0x0D} 4 RR Jbutton_rightRing
         * jLabel_rightRing ‘11’ {0x11} 5 RP Jbutton_rightPinky
         * jLabel_rightPinky ‘15’ {0x15}
         * 
         * 6 LT Jbutton_leftThumb jLabel_leftThumb ‘06’ {0x06} 7 LI
         * Jbutton_leftIndex jLabel_leftIndex ‘0A’ {(byte) 0x0A} 8 LM
         * Jbutton_leftMiddle jLabel_leftMiddle ‘0E’ {0x0E} 9 LR
         * Jbutton_leftRing jLabel_leftRing ‘12’ {0x12} 10 LP Jbutton_leftPinky
         * jLabel_leftPinky ‘16’ {0x16}
         */

        if ("05".equalsIgnoreCase(str_fgType)) {
            sub_bio = 1;
        } else if ("09".equalsIgnoreCase(str_fgType)) {
            sub_bio = 2;
        } else if ("0D".equalsIgnoreCase(str_fgType)) {
            sub_bio = 3;
        } else if ("11".equalsIgnoreCase(str_fgType)) {
            sub_bio = 4;
        } else if ("15".equalsIgnoreCase(str_fgType)) {
            sub_bio = 5;
        } else if ("06".equalsIgnoreCase(str_fgType)) {
            sub_bio = 6;
        } else if ("0A".equalsIgnoreCase(str_fgType)) {
            sub_bio = 7;
        } else if ("0E".equalsIgnoreCase(str_fgType)) {
            sub_bio = 8;
        } else if ("12".equalsIgnoreCase(str_fgType)) {
            sub_bio = 9;
        } else if ("16".equalsIgnoreCase(str_fgType)) {
            sub_bio = 10;
        } else {
            sub_bio = 0;
        }

        return sub_bio;

    }

    public byte[] getFingerType(int fgType) {
        byte[] sub_bio = new byte[1];

        byte[] RT = {0x05};
        byte[] RI = {0x09};
        byte[] RM = {0x0D};
        byte[] RR = {0x11};
        byte[] RL = {0x15};
        byte[] LT = {0x06};
        byte[] LI = {(byte) 0x0A};
        byte[] LM = {0x0E};
        byte[] LR = {0x12};
        byte[] LL = {0x16};

        // ‘05’ Right thumb
        // ‘09’ Right index
        // ‘0D’ Right middle
        // ‘11’ Right ring
        // ‘15’ Right little
        // ‘06’ Left thumb
        // ‘0A’ Left index
        // ‘0E’ Left middle
        // ‘12’ Left ring
        // ‘16’ Left little

        switch (fgType) {
            case 1:
                sub_bio = RT;
                break;
            case 2:
                sub_bio = RI;
                break;
            case 3:
                sub_bio = RM;
                break;
            case 4:
                sub_bio = RR;
                break;
            case 5:
                sub_bio = RL;
                break;
            case 6:
                sub_bio = LT;
                break;
            case 7:
                sub_bio = LI;
                break;
            case 8:
                sub_bio = LM;
                break;
            case 9:
                sub_bio = LR;
                break;
            case 10:
                sub_bio = LL;
                break;

            default:
                break;
        }

        return sub_bio;

    }

    public byte[] getFingerID(String fgType) {
        byte[] sub_bio = new byte[1];

        byte[] RT = {0x05}; // Right thumb
        byte[] RI = {0x09}; // Right index
        byte[] RM = {0x0D}; // Right middle
        byte[] RR = {0x11}; // Right ring
        byte[] RL = {0x15}; // Right little
        byte[] LT = {0x06}; // Left thumb
        byte[] LI = {(byte) 0x0A}; // Left index
        byte[] LM = {0x0E}; // Left middle
        byte[] LR = {0x12}; // Left ring
        byte[] LL = {0x16}; // Left little

        if (fgType == "RT") {
            sub_bio = RT;
        } else if (fgType == "RI") {
            sub_bio = RI;
        } else if (fgType == "RM") {
            sub_bio = RM;
        } else if (fgType == "RR") {
            sub_bio = RR;
        } else if (fgType == "RL") {
            sub_bio = RL;
        } else if (fgType == "LT") {
            sub_bio = LT;
        } else if (fgType == "LI") {
            sub_bio = LI;
        } else if (fgType == "LM") {
            sub_bio = LM;
        } else if (fgType == "LR") {
            sub_bio = LR;
        } else if (fgType == "LL") {
            sub_bio = LL;
        }
        return sub_bio;

    }

    public void deleteInstance(CardChannel channel) throws MiddleWareException {

        // First of all Perform general Authentication.
        // scard.generalAuth(channel,key_p); //get the Admin Authentication

        // NOw Send Delete Instance command
        // 5C 02 3F F5 53 00
        StringBuffer sb = new StringBuffer();
        byte[] apdu_delete = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF, 0x06,
            0x5C, 0x02, 0x3F, (byte) 0xF5, 0x53, 0x00};

        CommandAPDU READ_APDU = new CommandAPDU(apdu_delete);

        try {

            sendCard.sendCommand(channel, READ_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean getBioInfo(CardChannel channel) throws MiddleWareException {

        byte[] res = null;
        boolean rtnvalue = false;
        StringBuffer sb = new StringBuffer();

        String[] str_finger = new String[10];

        byte[] apdu_cls = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00,
            0x04, 0x5C, 0x02, 0x7F, 0x61};

        CommandAPDU VERIFY_APDU = new CommandAPDU(apdu_cls);

        try {

            res = sendCard.sendCommand(channel, VERIFY_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // NOw Store Finger Print Information in the Credential file.
        String ss = util.arrayToHex(res);
        String end_tag = "87";
        int start_loc = 0;
        int end_pos;

        end_pos = util.findtagLocation(start_loc, ss, end_tag);

        String tag = "82";
        int tag_pos = 0;
        int j = 0;
        for (int i = 0; i < end_pos;) {
            tag_pos = util.findtagLocation(i, ss, tag);

            if (tag_pos == -1 || tag_pos > end_pos) {
                break;
            }

            // str_finger= str_finger.append(ss.substring(tag_pos+4,tag_pos+6));

            str_finger[j] = ss.substring(tag_pos + 4, tag_pos + 6);
            i = tag_pos + 6;
            j++;
        }

        // sat123 Credential.setStr_fingerType(str_finger);

        if (sb.toString().equals("9000")) {
            rtnvalue = true;
        }
        return rtnvalue;
    }

    

    public void select(CardChannel channel, byte[] data) throws CardException {

        StringBuffer sb = new StringBuffer();

        byte cla = 0x00, ins = (byte) (0xA4 & 0xFF), p1 = 0x04, p2 = 0x00, de = 0x00;

        CommandAPDU SELECT_APDU = new CommandAPDU(cla, ins, p1, p2, data, de);

        System.out.println("SELECT_AID: " + channel);

        try {
            sendCard.sendCommand(channel, SELECT_APDU, sb);
        } catch (CardException e) {
            throw e;
        }

    }

    private void calcBertValue(Integer len, boolean binclude_bert) {
        // Determine value of Bert Tag
        // if (binclude_bert)
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for
        // size of data
        // bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size
        // of data bytes
        byte[] len_tag_small = {(byte) 0x81};
        byte[] len_tag_big = {(byte) 0x82};

        if (len > 255) {
            if (binclude_bert) {
                bert_tag = bert_tag_size_big;
            } else {
                bert_tag = len_tag_big;
            }

        } else {
            if (binclude_bert) {
                bert_tag = bert_tag_size_small;
            } else {
                bert_tag = len_tag_small;
            }

        }

        // Now calculate actual data size being stored.
        // Following condition makes sure that converted hex string is always
        // even.

        String tot_hex_size;

        if (len > 255 && len < 4096) {

            tot_hex_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_size = Integer.toHexString(len);

        }

        num_bytes = util.hex1ToByteArray(tot_hex_size);

    }

    /* Close Logical Channel */
    void closeManager(CardChannel channel) throws CardException {

        StringBuffer sb = new StringBuffer();
        byte[] log_ch1 = {0x00, 0x70, (byte) 0x80, 0x01, 0x00};

        CommandAPDU CLOSE_APDU = new CommandAPDU(log_ch1);

        try {
            sendCard.sendCommand(channel, CLOSE_APDU, sb);
        } catch (CardException e) {
            throw e;
        }

    }

    public String readCIN(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String errorMsg;
        StringBuffer sb = new StringBuffer();

        // First of all open Logical Channel '01' with card manager.

        try {
            selectManager(channel, true);
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getMessage());
        }


        byte[] apdu_CIN = {(byte) 0x80, (byte) 0xCA, 0x00, 0x45};
        CommandAPDU CIN_APDU = new CommandAPDU(apdu_CIN);

        try {

            res = sendCard.sendCommand(channel, CIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // Leave the first 2 bytes or 4 characters of the result.
        return util.arrayToHex(res).substring(4);
    }

    public String readIIN(CardChannel channel) throws MiddleWareException {

        // 01 A4 0400 00 ; this opens a logical channel “01” with the card
        // manager.
        // · 80 CA 00 42 00 ; Get Data to read the “Issuer Identification
        // Number” over the newly open logical
        // channel.
        // · 80 CA 0045 0A ; Get Data to read the “Card Image Number” over the
        // newly open logical
        // channel.
        // · 00 70 8001 00 ; Close the logical channel “01”.
        // First of all open Logical Channel '01' with card manager.

        byte[] res = null;

        String errorMsg;
        StringBuffer sb = new StringBuffer();

        try {
            selectManager(channel, true);
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getMessage());
        }

        byte[] apdu_IIN = {(byte) 0x80, (byte) 0xCA, 0x00, 0x42};

        CommandAPDU IIN_APDU = new CommandAPDU(apdu_IIN);

        try {
            res = sendCard.sendCommand(channel, IIN_APDU, sb);

            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // Leave the first 2 bytes or 4 characters of the result.
        return util.arrayToHex(res).substring(4);

    }

    public String readBAP(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String errorMsg;
        StringBuffer sb = new StringBuffer();

        // 80 CA 00 EE 
        // EE 05 14 34 08 72 82  

        // First of all open Logical Channel '01' with card manager.

        try {
            selectManager(channel, true);
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getMessage());
        }


        byte[] apdu_BAP = {(byte) 0x80, (byte) 0xCA, 0x00, (byte) 0xEE};
        CommandAPDU CUID_BAP = new CommandAPDU(apdu_BAP);

        try {

            res = sendCard.sendCommand(channel, CUID_BAP, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // Leave the first 3 bytes.
        String str_result = util.arrayToHex(util.extract_data(res, 2, 4));
        return str_result;

    }

    public String readCUID(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String errorMsg;
        StringBuffer sb = new StringBuffer();

        // First of all open Logical Channel '01' with card manager.

        try {
            selectManager(channel, true);
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getMessage());
        }

        //80 CA 9F 7F 
        // 9F 7F 2A 48 20 50 2B 82 31 80 30 00 63 13 26 00 00 00 11 00 00
        //14 32 13 26 14 33 13 26 14 34 13 26 00 00 00 00 14 35 13 26 00 00 00 00 

        byte[] apdu_CUID = {(byte) 0x80, (byte) 0xCA, (byte) 0x9F, 0x7F};
        CommandAPDU CUID_APDU = new CommandAPDU(apdu_CUID);

        try {

            res = sendCard.sendCommand(channel, CUID_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // Leave the first 2 bytes or 4 characters of the result.
        String str_res = util.arrayToHex(res);
        System.out.println(str_res);

        String str_res_1 = util.arrayToHex(util.extract_data(res, 3, 4));
        String str_res_2 = util.arrayToHex(util.extract_data(res, 19, 2));
        String str_res_3 = util.arrayToHex(util.extract_data(res, 15, 4));

        String str_result = str_res_1 + str_res_2 + str_res_3;

        return str_result;


    }

    /* select Card Manager method */
    private void selectManager(CardChannel channel, Boolean logicalChannel) throws MiddleWareException {
        // Card manager selection on logical channel 0
        // 00 A4 04 00 00 (6CXX, 6283, 9000) ; Status 6283 is returned when the
        // CM is locked

        String errorMsg;
        StringBuffer sb = new StringBuffer();
        byte[] log_ch0 = {0x00, (byte) 0xA4, 0x04, 0x00, 0x00};
        byte[] log_ch1 = {0x01, (byte) 0xA4, 0x04, 0x00, 0x00};

        byte[] apdu_manager;

        if (logicalChannel) {
            apdu_manager = log_ch1;

        } else {
            apdu_manager = log_ch0;
        }

        CommandAPDU INIT_APDU = new CommandAPDU(apdu_manager);

        try {
            sendCard.sendCommand(channel, INIT_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    public String readPinLength(CardChannel channel, char pin) throws MiddleWareException {
        byte[] res = null;
        byte[] apdu_pin;
        StringBuffer sb = new StringBuffer();
        String errorMsg;

        //set APDU command for local PIN / Global PIN / PUK 
        byte[] apdu_LP = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04,
            0x5C, 0x02, 0x7F, 0x72};
        byte[] apdu_GP = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04,
            0x5C, 0x02, 0x7F, 0x71};

        byte[] apdu_PUK = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04,
            0x5C, 0x02, 0x7F, 0x73};

        //Send : 00 CB 3F FF 00 00 04 5C 02 7F 72 
        //Recv : 7F 72 0B 5F 01 01 08 9F 17 01 02 93 01 02 
        //Send : 00 CB 3F FF 00 00 04 5C 02 7F 71 
        //Recv : 7F 71 0B 5F 01 01 08 9F 17 01 02 93 01 02 
        // Leave the first 6 bytes or 12 characters of the result.
        //7f 72 0b 5f 01 01 08 9f 17 01 02 93 01 02         


        if (pin == 'L') {
            apdu_pin = apdu_LP;
        } else if (pin == 'G') {
            apdu_pin = apdu_GP;
        } else {
            apdu_pin = apdu_PUK;
        }

        CommandAPDU LP_APDU = new CommandAPDU(apdu_pin);

        try {

            res = sendCard.sendCommand(channel, LP_APDU, sb);
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

            return util.arrayToHex(res).substring(12, 14);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }
    
   
    
}