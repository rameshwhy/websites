/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.beans.piv.ContentCertificateBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;

import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;

/**
 *
 * @author Harpreet Singh
 */
public class GenKeyPair {

    ContentCertificateBean contentCertificate = new ContentCertificateBean();
    CommonUtil util = new CommonUtil();

    public GenKeyPair() {
        super();
    }

    public ContentCertificateBean keyPair(String algoType) throws MiddleWareException{

       /* ECC: Curve P-224 (ECDSA, ECDH) 3 (SP800-73 Algorithm ‘0E’)
        ECC: Curve P-256 (ECDSA, ECDH) (SP800-78-2 Algorithm ‘11’)
        ECC: Curve P-384 (ECDSA, ECDH) (SP800-78-2 Algorithm ‘14’)
          RSA 1024 (SP800-78-2 Algorithm ‘06’)
        RSA 2048 (SP800-78-2 Algorithm ‘07’) */


        String eccSpec;
        int keyLen;
        try{
        if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC224)) {
            eccSpec = "secp224r1";
            contentCertificate = keyPairECC(eccSpec);            
        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC256)) {
            eccSpec = "secp256r1";
            contentCertificate = keyPairECC(eccSpec);
        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC384)) {
            eccSpec = "secp384r1";
            contentCertificate = keyPairECC(eccSpec);

        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.RSA1024)) {
            keyLen = 1024;
            contentCertificate = keyPairRSA(keyLen);
        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
            keyLen = 2048;
            contentCertificate = keyPairRSA(keyLen);
        }
        } catch (NoSuchAlgorithmException ex) {
            throw new MiddleWareException(ex.getMessage());
        } catch (NoSuchProviderException ex) {
            throw new MiddleWareException(ex.getMessage());
        }catch (InvalidAlgorithmParameterException ex) {
            throw new MiddleWareException(ex.getMessage());
        }


        return contentCertificate;

    }



    private ContentCertificateBean keyPairRSA(int keyLen) throws MiddleWareException, NoSuchAlgorithmException {

        PublicKey publicKey;
        PrivateKey privateKey;
        KeyPairGenerator keyGen = null;

        keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(keyLen, new SecureRandom());

        KeyPair keypair = keyGen.generateKeyPair();

        publicKey = keypair.getPublic();
        privateKey = keypair.getPrivate();

        contentCertificate.setPublicKey(publicKey);
        contentCertificate.setPrivateKey(privateKey);

        contentCertificate.setStrPublickeyHex(util.arrayToHex(publicKey.getEncoded()));
        contentCertificate.setStrPrivatekeyHex(util.arrayToHex(privateKey.getEncoded()));

        return contentCertificate;

    }

     private ContentCertificateBean keyPairECC(String eccSpec) throws MiddleWareException, NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
        ECPublicKey publicKey;
        ECPrivateKey privateKey;

       KeyPairGenerator keyGen = null;
       keyGen = KeyPairGenerator.getInstance("EC", "SunEC");
       ECGenParameterSpec ecsp;

       ecsp= new ECGenParameterSpec(eccSpec);
       keyGen.initialize(ecsp);

       KeyPair keyPair = keyGen.generateKeyPair();


       publicKey = (ECPublicKey) keyPair.getPublic();
       privateKey = (ECPrivateKey) keyPair.getPrivate();

      // System.out.println("Public Key: " +  publicKey);
       //System.out.println("Private Key: " +  privateKey);

        contentCertificate.setPublicKey(publicKey);
        contentCertificate.setPrivateKey(privateKey);

        contentCertificate.setStrPublickeyHex(util.arrayToHex(publicKey.getEncoded()));
        contentCertificate.setStrPrivatekeyHex(util.arrayToHex(privateKey.getEncoded()));

        return contentCertificate;

    }

}
