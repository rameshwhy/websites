package com.secuera.middleware.cardreader.core;

import java.io.IOException;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

import sun.misc.BASE64Decoder;
import com.secuera.middleware.cardreader.entities.MiddleWareException;

public class LoadCertificate {

    SendCommand sendCard = new SendCommand();
    CommonUtil util = new CommonUtil();
    GeneralAuth auth = new GeneralAuth();
    private byte[] bert_tag = new byte[2];
    private byte[] num_bytes;

    //public boolean loadCert(CardChannel channel, String cert_data, char cert_type,byte[] admin_key)
    public boolean loadCert(CardChannel channel,byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY, byte[] cert, char cert_type)
            throws MiddleWareException {

        boolean rtnvalue = false;
        StringBuffer sb = new StringBuffer();
        byte[] cert_id;

        /*
        byte[] cert = null;
        try {
        cert = convertToByteArray(cert_data);
        } catch (IOException e2) {
        // TODO Auto-generated catch block
        e2.printStackTrace();
        }
         */
        Integer cert_len = cert.length;

        System.out.println("Length of Cert data " + cert_len);

        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }

        //X.509 Certificate for	PIV Authentication		NIST ‘5FC105’---> '9A'
        //X.509 Certificate for	Digital Signature  	    NIST ‘5FC10A’ ---> '9C'
        //X.509 Certificate for 	Key management  	NIST ‘5FC10B’   ----> '9D'
        //X.509 Certificate for	Card Authentication	NIST ‘5FC101    -----> '9E'

        byte[] apdu_cert = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};

        byte[] piv_auth = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x05};
        byte[] card_digi = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x0A};
        byte[] key_manage = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x0B};
        byte[] card_auth = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x01};

        byte[] cert_tag = {0x70};
        byte[] cert_info_tag = {0x71};
        byte[] cert_info_len = {0x01};
        byte[] cert_info = {0x00};
        byte[] error_cde = {(byte) 0xFE,0x00};



        if (cert_type == 'P') {
            cert_id = piv_auth;
        } else if (cert_type == 'C') {
            cert_id = card_digi;
        } else if (cert_type == 'K') {
            cert_id = key_manage;
        } else {
            cert_id = card_auth;    //'A'
        }

        //Calculate size of the certificate to be stored
        calcBertValue(cert_len, false);

        byte[] bert_tag_cert = bert_tag;
        byte[] byte_cert_len = num_bytes;

        /***************************************************************/
        //Now calculate size of the whole data to be stored
        byte[] cont_data = util.combine_data(cert_tag, bert_tag_cert);
        cont_data = util.combine_data(cont_data, byte_cert_len);
        cont_data = util.combine_data(cont_data, cert);
        cont_data = util.combine_data(cont_data, cert_info_tag);
        cont_data = util.combine_data(cont_data, cert_info_len);
        cont_data = util.combine_data(cont_data, cert_info);
        cont_data = util.combine_data(cont_data, error_cde);

        Integer cont_len = cont_data.length;

        calcBertValue(cont_len, true);
        byte[] bert_tag_cont = bert_tag;
        byte[] byte_cont_len = num_bytes;

        /***********************************************************/
        cont_data = util.combine_data(byte_cont_len, cont_data);
        cont_data = util.combine_data(bert_tag_cont, cont_data);
        /***********************************************************/
        cont_data = util.combine_data(cert_id, cont_data);

        Integer apdu_len = cont_data.length;

        calcBertValue(apdu_len, false);
        byte[] total_size = num_bytes;

        byte[] zero_size = {0x00};

        if (total_size.length > 1) {
            total_size = util.combine_data(zero_size, total_size);
        }

        // Now form final APDU Command.
        apdu_cert = util.combine_data(apdu_cert, total_size);
        apdu_cert = util.combine_data(apdu_cert, cont_data);

        System.out.println(util.arrayToHex(apdu_cert));

        CommandAPDU WRITE_APDU = new CommandAPDU(apdu_cert);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e1) {

            e1.printStackTrace();
        }



        if (sb.toString().equals("9000")) {
            rtnvalue = true;
        }

        System.out.println("Result for CERT LOAD " + sb);

        return rtnvalue;
    }

    private void calcBertValue(Integer len, boolean binclude_bert) {

        //Determine value of Bert Tag
        //if (binclude_bert)
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for size of data bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size of data bytes
        byte[] len_tag_small = {(byte) 0x81};
        byte[] len_tag_big = {(byte) 0x82};

        if (len > 255) {
            if (binclude_bert) {
                bert_tag = bert_tag_size_big;
            } else {
                bert_tag = len_tag_big;
            }

        } else {

            if (binclude_bert) {
                bert_tag = bert_tag_size_small;
            } else {
                bert_tag = len_tag_small;
            }


        }

        // Now calculate actual data size being stored.
        // Following condition makes sure that converted hex string is always
        // even.

        String tot_hex_size;

        if (len > 255 && len < 4096) {

            tot_hex_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_size = Integer.toHexString(len);

        }


        num_bytes = util.hex1ToByteArray(tot_hex_size);

    }

    public byte[] convertToByteArray(String base64)
            throws IOException {

        BASE64Decoder decoder = new BASE64Decoder();

        return decoder.decodeBuffer(base64);


    }
}
