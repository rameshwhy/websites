// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena;

import eu.gnome.morena.ui.ScannerUIDialog;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Configuration
{

    public Configuration()
    {
    }

    public static void addDeviceType(String s, boolean flag)
    {
        if(deviceTypes == null)
            deviceTypes = new HashMap();
        deviceTypes.put(s, Boolean.valueOf(flag));
    }

    public static boolean isScanner(String s, boolean flag)
    {
label0:
        {
            if(deviceTypes == null)
                break label0;
            Iterator iterator = deviceTypes.keySet().iterator();
            String s1;
            do
            {
                if(!iterator.hasNext())
                    break label0;
                s1 = (String)iterator.next();
            } while(!s.matches(s1));
            return ((Boolean)deviceTypes.get(s1)).booleanValue();
        }
        return flag;
    }

    public static int getMode()
    {
        return mode;
    }

    public static void setMode(int i)
    {
        mode = i;
    }

    public static long getPollTime()
    {
        return pollTime;
    }

    public static void setPollTime(long l)
    {
        pollTime = l;
    }

    public static void setLogLevel(Level level)
    {
        logLevel = level;
        Logger.getLogger("morena").setLevel(level);
    }

    public static Level getLogLevel()
    {
        return logLevel;
    }

    public static Class getScannerUIClass()
    {
        return scannerUIClazz;
    }

    public static void setScannerUIClass(Class class1)
    {
        scannerUIClazz = class1;
    }

    public static boolean checkWriteable()
    {
        return checkWriteable;
    }

    public static void setCheckWriteable(boolean flag)
    {
        checkWriteable = flag;
    }

    private static Map deviceTypes = null;
    public static final int MODE_NATIVE_UI = 1;
    public static final int MODE_WIA1_POLL_ENABLED = 2;
    private static int mode = 0;
    private static long pollTime = 5000L;
    private static Level logLevel;
    private static Class scannerUIClazz = ScannerUIDialog.class;
    private static boolean checkWriteable = true;

    static 
    {
        logLevel = Level.INFO;
    }
}
