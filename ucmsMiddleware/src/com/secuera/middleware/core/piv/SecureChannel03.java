/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.AesEncrypter;
import com.secuera.middleware.beans.piv.CredentialKeys;
import com.secuera.middleware.beans.piv.DiversifiedCredentialKeys;
import com.secuera.middleware.beans.piv.SecureChannelBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import java.security.SecureRandom;
import java.util.Arrays;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class SecureChannel03 {

      
    //Global Platform Keys (Security Domain Keys)
    CredentialKeys credentialKey;
    DiversifiedCredentialKeys diversifiedCredentialKeys;
    
    
    //Master Keys
    private byte[] MASTER_KEY;
    
    byte[] AUTH_ENC_KEY;
    byte[] AUTH_MAC_KEY;
    byte[] AUTH_DEK_KEY;
    byte[] S_ENC_KEY;
    byte[] S_MAC_KEY;
    byte[] PREV_MAC;
    byte[] card_challenge;
    byte[] host_challenge;
    byte[] card_init_reponse;
    private boolean useGPKeys;
        
    // *************************************************************
    // Create instances of other classes.
    // *************************************************************
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    ExceptionMessages expMsg = new ExceptionMessages();

    public SecureChannel03() {
        super();
    }

    public SecureChannel03(CredentialKeys crKeys,DiversifiedCredentialKeys dCrKeys) {
        
        credentialKey = crKeys ;
        diversifiedCredentialKeys = dCrKeys;
        
        
        //Master Keys
        MASTER_KEY = crKeys.getMasterKey();
    }

    public SecureChannelBean openSecureChannel03(CardChannel channel,boolean data_load) throws MiddleWareException {
        SecureChannelBean channelbean = new SecureChannelBean();
        //try {
           
            card_init_reponse = initUpdate(channel,data_load);
            try{
                computeKeys(MASTER_KEY,credentialKey.getEncKey(),credentialKey.getMacKey(),credentialKey.getDekKey());
                openSCP03(channel);
            } catch (MiddleWareException e) {
                try {
                    
                    computeKeys(MASTER_KEY,diversifiedCredentialKeys.getDiversifiedEncKey(),diversifiedCredentialKeys.getDiversifiedMacKey(),diversifiedCredentialKeys.getDiversifiedDekKey());
                    openSCP03(channel);
                } catch (MiddleWareException m) {
                    throw new MiddleWareException(m.getMessage());
                }
            }
            
            channelbean.setPREV_MAC(PREV_MAC);
            channelbean.setS_MAC_KEY(S_MAC_KEY);
            channelbean.setAUTH_DEK_KEY(AUTH_DEK_KEY);
        //}catch (MiddleWareException m1) {
        //    throw new MiddleWareException(m1.getErrorMsg());
       // }

        return channelbean;

    }
    
    
    private void computeKeys(byte[] MASTER_KEY, byte[] ENC_KEY, byte[] MAC_KEY, byte[] DEK_KEY) throws MiddleWareException {

        // *----------------------------------------------
        // * Retrieve security domain keys
        // *----------------------------------------------

        if (MASTER_KEY.length > 0) {

            byte[] card_diverse_init = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00};
            byte[] card_diverse = util.extract_data(card_init_reponse, 0, 10);

            boolean comp_result = Arrays.equals(card_diverse_init, card_diverse);

            if (!comp_result) {
                // Calculate Diversified Authentication Key
                byte[] card_diverse_6 = util.extract_data(card_diverse, 4, 6);
                byte[] div_1 = {(byte) 0xF0, 0x01};
                byte[] div_2 = {(byte) 0x0F, 0x01};

                byte[] div_3 = {(byte) 0xF0, 0x02};
                byte[] div_4 = {(byte) 0x0F, 0x02};

                byte[] div_5 = {(byte) 0xF0, 0x03};
                byte[] div_6 = {(byte) 0x0F, 0x03};

                byte[] div_auth_key = util.combine_data(card_diverse_6, div_1);
                div_auth_key = util.combine_data(div_auth_key, card_diverse_6);
                div_auth_key = util.combine_data(div_auth_key, div_2);

                AUTH_ENC_KEY = aesencrypt(MASTER_KEY, div_auth_key, false);

                // Calculate Diversified MAC Key
                byte[] div_mac_key = util.combine_data(card_diverse_6, div_3);
                div_mac_key = util.combine_data(div_mac_key, card_diverse_6);
                div_mac_key = util.combine_data(div_mac_key, div_4);

                AUTH_MAC_KEY = aesencrypt(MASTER_KEY, div_mac_key, false);

                // Calculate Diversified DATA_ENCRIPTION Key
                // .SET_DATA I(71:76) F003 I(71:76) 0F03
                // .AES I(35:50) /128 /ECB ; Diversified DEK Key

                byte[] div_dek_key = util.combine_data(card_diverse_6, div_5);
                div_dek_key = util.combine_data(div_dek_key, card_diverse_6);
                div_dek_key = util.combine_data(div_dek_key, div_6);

                AUTH_DEK_KEY = aesencrypt(MASTER_KEY, div_dek_key, false);

            } else {
                // Otherwise Assign Static keys.
                AUTH_ENC_KEY = ENC_KEY;
                AUTH_MAC_KEY = MAC_KEY;
                AUTH_DEK_KEY = DEK_KEY;
            }

        } else {
            AUTH_ENC_KEY = ENC_KEY;
            AUTH_MAC_KEY = MAC_KEY;
            AUTH_DEK_KEY = DEK_KEY;

        }


    }
    
    
    /*
    private void computeKeys(byte[] MASTER_KEY, byte[] ENC_KEY, byte[] MAC_KEY,byte[] DEK_KEY ) throws MiddleWareException {
    
        // *----------------------------------------------
        // * Retrieve security domain keys
        // *----------------------------------------------
        
        if (!useGPKeys)
        {
        
                byte[] card_diverse_init = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00};
                byte[] card_diverse = util.extract_data(card_init_reponse, 0, 10);

                boolean comp_result = Arrays.equals(card_diverse_init, card_diverse);

                if (!comp_result) {
                // Calculate Diversified Authentication Key
                byte[] card_diverse_6 = util.extract_data(card_diverse, 4, 6);
                byte[] div_1 = {(byte) 0xF0, 0x01};
                byte[] div_2 = {(byte) 0x0F, 0x01};

                byte[] div_3 = {(byte) 0xF0, 0x02};
                byte[] div_4 = {(byte) 0x0F, 0x02};

                byte[] div_5 = {(byte) 0xF0, 0x03};
                byte[] div_6 = {(byte) 0x0F, 0x03};

                byte[] div_auth_key = util.combine_data(card_diverse_6, div_1);
                div_auth_key = util.combine_data(div_auth_key, card_diverse_6);
                div_auth_key = util.combine_data(div_auth_key, div_2);

                AUTH_ENC_KEY = aesencrypt(MASTER_KEY, div_auth_key, false);

                // Calculate Diversified MAC Key
                byte[] div_mac_key = util.combine_data(card_diverse_6, div_3);
                div_mac_key = util.combine_data(div_mac_key, card_diverse_6);
                div_mac_key = util.combine_data(div_mac_key, div_4);

                AUTH_MAC_KEY = aesencrypt(MASTER_KEY, div_mac_key, false);

                // Calculate Diversified DATA_ENCRIPTION Key
                // .SET_DATA I(71:76) F003 I(71:76) 0F03
                // .AES I(35:50) /128 /ECB ; Diversified DEK Key

                byte[] div_dek_key = util.combine_data(card_diverse_6, div_5);
                div_dek_key = util.combine_data(div_dek_key, card_diverse_6);
                div_dek_key = util.combine_data(div_dek_key, div_6);

                AUTH_DEK_KEY = aesencrypt(MASTER_KEY, div_dek_key, false);

            } else {
                // Otherwise Assign Static keys.
                AUTH_ENC_KEY = ENC_KEY;
                AUTH_MAC_KEY = MAC_KEY;
                AUTH_DEK_KEY = DEK_KEY;
            }
                
        }else
        {  
                AUTH_ENC_KEY = ENC_KEY;
                AUTH_MAC_KEY = MAC_KEY;
                AUTH_DEK_KEY = DEK_KEY;
                
        }
        
        
    }
    */
    
    private void openSCP03(CardChannel channel) throws MiddleWareException {
        StringBuffer sb = new StringBuffer();
        
        // ************ COMPUTE AUTH/ENC KEY SESSION KEY
        // ******************************

        byte[] data_key = util.combine_data(card_challenge, host_challenge);

        // S_ENC_KEY: Now Compute Session Encryption key.
        byte[] res_1 = aesencrypt(AUTH_ENC_KEY, data_key, false);

        // XOR the result from first step with Static Derivation data.
        byte[] res_2 = new byte[UcmsMiddlewareConstants.DER_ENC_KEY.length];

        for (int i = 0; i < UcmsMiddlewareConstants.DER_ENC_KEY.length; i++) {
            res_2[i] = (byte) (res_1[i] ^ UcmsMiddlewareConstants.DER_ENC_KEY[i]);
        }

        // System.out.println("res_2_after 16byte XOR: " +
        // util.arrayToHex(res_2) );
        // STORE THE SESSION ENCRYPTION KEY, after performing encyrption on
        // res_2.

        S_ENC_KEY = aesencrypt(AUTH_ENC_KEY, res_2, false);

        // System.out.println("Compare host crypto " +
        // util.arrayToHex(util.extract_data(s_enc_aesencrypt(util.combine_data(host_challenge,card_challenge)),0,8)));

        // MAC_ENC_KEY: Now Compute MAC Session key.

        byte[] res_3 = aesencrypt(AUTH_MAC_KEY, data_key, false);

        // XOR the result from first step with Static Derivation data.
        byte[] res_4 = new byte[UcmsMiddlewareConstants.DER_MAC_KEY.length];

        for (int i = 0; i < UcmsMiddlewareConstants.DER_MAC_KEY.length; i++) {
            res_4[i] = (byte) (res_3[i] ^ UcmsMiddlewareConstants.DER_MAC_KEY[i]);
        }

        // STORE THE SESSION MAC KEY, after performing encyrption on res_4.

        S_MAC_KEY = aesencrypt(AUTH_MAC_KEY, res_4, false);

        // *** Compute DEK session key
        // *The DEK session key is the static KEY-DEK NO FURTHER PROCESSING
        // REQUIRED.

        // ==========================================================================================
        // STEP 3: PERFORM EXTERNAL AUTHENTICATION
        // ==========================================================================================
        // *****COMPUTE HOST CRYPTOGRAM = AES Enc ( S-Enc,CARD CHALLENGE| HOST
        // CHALLENGE)
        // Encrypt and get first 8 bytes.
        byte[] com_host_crypto = util.extract_data(
                aesencrypt(S_ENC_KEY, data_key, false), 0, 8);

        // PREVIOUS MAC+ COMMAND + PADDING
        // SINCE THIS IS FIST COMMAND IN THE CHAIN, NO PREVIOUS MAC IS PRESENT
        // ONLY COMMAND IS USED
        // *--- COMPUTE MAC FOR THE COMMAND apdu.
        // SET_BUFFER K 84 82 01 00 10 L(49;8) 80 00 00 ;PREPARE BUFFER

        byte[] mac_buffer = {(byte) 0x84, (byte) 0x82, 0x01, 0x00, 0x10};
        byte[] mac_end = {(byte) 0x80, 0x00, 0x00};

        mac_buffer = util.combine_data(mac_buffer, com_host_crypto);
        mac_buffer = util.combine_data(mac_buffer, mac_end);

        // Now perform AES encryption with Session MAC key and get the first 8
        // bytes.
        PREV_MAC = util.extract_data(aesencrypt(S_MAC_KEY, mac_buffer, true),
                0, 8);

        byte[] mac_apdu = {(byte) 0x84, (byte) 0x82, 0x01, 0x00, 0x10};

        mac_apdu = util.combine_data(mac_apdu, com_host_crypto);
        mac_apdu = util.combine_data(mac_apdu, PREV_MAC);

        CommandAPDU EXT_APDU = new CommandAPDU(mac_apdu);

        try {

            sendCard.sendCommand(channel, EXT_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (!sb.toString().equals("9000")) {
            StringBuilder errMsg = new StringBuilder();
            errMsg.append(ExceptionMessages.INIT_SECURE_CH103);
            errMsg.append(" (").append(sb.toString()).append(")").append(" - Invalid Master/GP Keys.");
            throw new MiddleWareException(errMsg.toString());
        }

    }

    /* Initialize Update Command method */
    private byte[] initUpdate(CardChannel channel, boolean data_load) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] res = null;
        byte[] apdu_update;

        SecureRandom random = new SecureRandom();
        byte[] seed = random.generateSeed(20);
        random.setSeed(seed);
        host_challenge = new byte[8];
        random.nextBytes(host_challenge);

        byte[] apdu_init = {(byte) 0x80, 0x50, 0x00, 0x00, 0x08};

        // MAC MODEONLY
        
        if (!data_load) {
            //Nbr of bytes expected back by this command
            byte[] Le = {0x1C};
            byte[][] arrays = new byte[3][];
            arrays[0] = apdu_init;
            arrays[1] = host_challenge;
            arrays[2] = Le;

            apdu_update = util.combine_data(arrays) ;

        } else {
            byte[][] arrays = new byte[2][];
            arrays[0] = apdu_init;
            arrays[1] = host_challenge;
            
            apdu_update = util.combine_data(arrays) ;
            
            
        }
        
        // apdu_update=util.combine_data(apdu_update,Le);
        CommandAPDU INIT_APDU = new CommandAPDU(apdu_update);

        try {
            res = sendCard.sendCommand(channel, INIT_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        
        // Get the card challenge and store it.
        card_challenge = util.extract_data(res, 12, 8);
        
        return res;

    }

    public byte[] aesencrypt(byte[] key, byte[] from_card, boolean vector) throws MiddleWareException {

        // Create encrypter/decrypter class
        AesEncrypter encrypter;
        byte[] res = null;
        try {

            if (vector) {
                encrypter = new AesEncrypter(key,UcmsMiddlewareConstants.IV_AES);
            }
            {
                encrypter = new AesEncrypter(key);
            }

            // Encrypt
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return res;

    }

    /* select Card Manager method */
    private boolean selectManager(CardChannel channel, Boolean logicalChannel) throws MiddleWareException {

        // Card manager selection on logical channel 0
        // 00 A4 04 00 00 (6CXX, 6283, 9000) ; Status 6283 is returned when the
        // CM is locked

        StringBuffer sb = new StringBuffer();
        boolean retValue = false;

        byte[] log_ch0 = {0x00, (byte) 0xA4, 0x04, 0x00, 0x00};
        byte[] log_ch1 = {0x01, (byte) 0xA4, 0x04, 0x00, 0x00};

        byte[] apdu_manager;

        if (logicalChannel) {
            apdu_manager = log_ch1;
        } else {
            apdu_manager = log_ch0;
        }


        CommandAPDU INIT_APDU = new CommandAPDU(apdu_manager);


        try {
            sendCard.sendCommand(channel, INIT_APDU, sb);

            // check if success return true
            if (sb.toString().equals(UcmsMiddlewareConstants.CARDUNLOCK)) {
                retValue = true;
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return retValue;
    }

    public void checkPIVInstance(CardChannel channel) throws MiddleWareException {

        // ; Check PIV instance FCI
        // 00 A4 04 00 09 A0 00 00 03 08 00 00 10 00 00 [ \

        StringBuffer sb = new StringBuffer();
        byte[] apdu_security_piv = {0x00, (byte) 0xA4, 0x04, 0x00, 0x09,
            (byte) 0xA0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00,
            0x00};

        CommandAPDU SEC_APDU_PIV = new CommandAPDU(apdu_security_piv);

        try {
            sendCard.sendCommand(channel, SEC_APDU_PIV, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }
}
