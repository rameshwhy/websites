/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.util.Date;

/**
 *
 * @author HSingh.
 * This class would compute and define header for the facial image.
 */

/*                                      Length             PIV Values
 * 1. Patron Header Version              1        UINT      0x03
2. SBH Security Options               1        Bitfield  0x0D  ---denotes that data will be digitally signed.
3. BDB Length                         4        UINT      -->Length, in bytes, of the biometric data CBEFF_BIOMETRIC_RECORD
4. SB Length                          2        UINT      -->Length, in bytes, of the CBEFF_SIGNATURE_BLOCK. See Note 3
5. BDB Format Owner (5.2.1.17)        2        UINT      For Facial and Fingerprint..value is 0x001b
6. BDB Format Type (5.2.1.17)         2        UINT      For fingerprint image data defined above the Format Type shall be 0x0401.
For the mandatory fingerprint minutiae data this value shall be 0x0201.
For face data this value shall be 0x0501.
7. Biometric Creation Date (5.2.1.10) 8                  This is the date that the biometric sample was acquired. For processed samples
(e.g. templates) this data should be the date of acquisition of the parent sample.
Creation Date shall be encoded in eight bytes using a binary representation
of "YYYYMMDDhhmmssZ". Each pair of characters (for example, "DD") is coded in 8 bits
as an unsigned integer. Thus 17:35:30 December 15, 2005 is represented as:
00010100 00000101 00001100 00001111 00010001 00100011 00011110 01011010
where the last byte is the binary representation of the ASCII character Z which is included
to indicate that the time is represented in Coordinated Universal Time (UTC).
The field "hh" shall code a 24 hour clock value.

8.Validity Period (5.2.1.11)           16                 Contains 2 dates. And Date is represented as explained above.
9. Biometric Type (5.2.1.5)            3       UINT       For fingerprint images and any kind of fingerprint template the type shall be 0x000008,
for facial images the type shall be 0x000002
10.Biometric Data Type (5.2.1.7)       1       Bitfield   establishes three categories for the degree to which biometric data has been processed.
For the mandatory [MINUSTD] PIV Card templates this value shall be b100xxxxx.
else b001xxxxx. (For Non Mandatory, eg for Facial)
11. Biometric Data Quality (5.2.1.9)   1      SINT       value required by [FACESTD] shall be coded in this CBEFF field as -2.
12. Creator (5.2.1.12)                 18                For PIV the Creator field has length 18 bytes of which the first K  17 bytes shall be
printable ASCII characters, and the first of the remaining 18-K shall be a null terminator (zero).
13.FASC-N                              25                This field shall contain the 25 bytes of the FASC-N component of the CHUID identifier, per [800-73, 1.8.{3,4}].
14. Reserved for future use            4              0x00000000

 * END OF */
public class GenFacCbeffHeader {

    CommonUtil util = new CommonUtil();
    GenAsmSig sig = new GenAsmSig();
    private byte[] num_bytes;
    private byte[] rec_len;
    private byte[] image_block;
    private byte[] format_owner = {0x00, 0x1b};
    private byte[] format_type = {0x05, 0x01};
    private byte[] bio_type = {0x00, 0x00, 0x02};
    private byte[] bio_data_type = {0x20};
    private byte[] bio_data_qualty = {(byte) 0xFE};
    private byte[] creator_terminator = {0x00};
    private byte[] reserved = {0x00, 0x00, 0x00, 0x00};
    private byte[] format_id = {0x46, 0x41, 0x43, 0x00};
    private byte[] ver_nbr = {0x30, 0x31, 0x30, 0x00};
    private byte[] num_facial = {0x00, 0x01};
    // Facial Info
    private byte[] feature_points = {0x00, 0x00};
    private byte[] gender = {0x00};
    private byte[] eye_color = {0x00};
    private byte[] hair_color = {0x00};
    private byte[] feature_mask = {0x00, 0x00, 0x00};
    private byte[] expression = {0x00, 0x00};
    private byte[] pose_angle = {0x00, 0x00, 0x00};
    private byte[] pose_angle_uncertainity = {0x00, 0x00, 0x00};
    //Image Info.
    private byte[] image_type = {0x01};
    private byte[] image_data_type = {0x01};
    private byte[] width = {0x01, 0x00};
    private byte[] height = {0x01, 0x00};
    private byte[] image_color = {0x01};
    private byte[] source_type = {0x02};
    private byte[] device_type = {0x00, 0x00};
    private byte[] quality = {0x00, 0x00};
    private int i_header_len = 46;
    private int i_creator_len = UcmsMiddlewareConstants.FACIAL_IMAGE_CREATOR_LENGTH + 1;
    private byte[] len_tag_small = {(byte) 0x81};
    private byte[] len_tag_big = {(byte) 0x82};

    public GenFacCbeffHeader() {
        super();
    }

    public byte[] getFacialHeader(byte[] bioData, String creation_date, String expiry_date, String valid_from_date, byte[] creator,
            String str_fascn, Date dt_signingtime, PrivateKey privatekey, byte[] cert, String algoType) throws MiddleWareException, IOException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException {

        byte[] bioMetricData = null;
        byte[] signature = null;
        byte[] FASC;
        //For the first round...dummy hard coded length will be defined for Signature block.
        byte[] sb_len = {0x00, 0x00};
        byte[] image_tag = {(byte) 0xBC};

        FASC = util.hex1ToByteArray(str_fascn);

        //Since Signature length is not known for the first time, hence we have to run this process twice.
        //First run is made with dummy Signature length and then final run is made to compute the facial header.
        for (int i = 1; i <= 2; i = i + 1) {

            byte[] cbeff = computeCbeff(bioData, sb_len, creation_date, expiry_date, valid_from_date, creator, FASC);
            byte[] header = formatHeader();
            byte[] feature = getFeature();

            bioMetricData = util.combine_data(cbeff, header);
            bioMetricData = util.combine_data(bioMetricData, feature);
            bioMetricData = util.combine_data(bioMetricData, bioData);

            if (cert != null) {

                signature = util.hex1StringToByteArray(sig.genAsmSig(util.arrayToHex((bioMetricData)), privatekey, cert, str_fascn, dt_signingtime, 'F', algoType));


                if (i == 1) {
                    int sblen = signature.length;
                    byte[] block_len = {(byte) (sblen >> 8 & 0xff), (byte) (sblen & 0xff)};
                    sb_len = block_len;
                }
                
            }
           
        }
        if (cert != null){
            bioMetricData = util.combine_data(bioMetricData, signature);
        }
        

        Integer len = bioMetricData.length;

        //Calculate size of the facial record to be stored, with Image Tag
        calcBertValue(len);

        bioMetricData = util.combine_data(num_bytes, bioMetricData);
        bioMetricData = util.combine_data(image_tag, bioMetricData);


        return bioMetricData;

    }

    private byte[] computeCbeff(byte[] bioData, byte[] sb_len, String creation_date, String expiry_date, String valid_from_date,
            byte[] creator, byte[] FASC) {

        /*This is the date that the biometric sample was acquired. For processed samples (e.g. templates)
         * this data should be the date of acquisition of the parent sample. Creation Date shall be encoded 
         * in eight bytes using a binary representation of "YYYYMMDDhhmmssZ". Each pair of characters 
         * (for example, "DD") is coded in 8 bits as an unsigned integer. Thus 17:35:30 December 15, 2005 is 
         * represented as: 00010100 00000101 00001100 00001111 00010001 00100011 00011110 01011010 where the
         * last byte is the binary representation of the ASCII character Z which is included to indicate that
         * the time is represented in Coordinated Universal Time (UTC). The field "hh" shall code a 24 hour clock value.
        When multiple samples (e.g. two single finger minutiae views) are included in one record 
         * (e.g. an INCITS 378 record) and the Creation Dates are different, the Creation Date shall be the
         * earliest of the multiple views
         */

        byte[] cbeff_header = null;
        byte[] header_ver = {0x03};
        byte[] sec_option = {0x0D};

        // Here 46 bytes are for the facial/format header and features.
        int biolen = bioData.length + i_header_len;

        byte[] bio_len = {(byte) (biolen >>> 24), (byte) (biolen >> 16 & 0xff), (byte) (biolen >> 8 & 0xff), (byte) (biolen & 0xff)};
        rec_len = bio_len;

        // Following block len is calculated to be used in the getFeature method.
        byte[] block_len = {(byte) (biolen - 14 >>> 24), (byte) (biolen - 14 >> 16 & 0xff), (byte) (biolen - 14 >> 8 & 0xff), (byte) (biolen - 14 & 0xff)};
        image_block = block_len;



        byte[] creation_dt = util.hex1ToByteArray(util.stringPairToHex(creation_date) + "5A");
        byte[] valid_from_dt = util.hex1ToByteArray(util.stringPairToHex(valid_from_date) + "5A");
        byte[] expiry_dt = util.hex1ToByteArray(util.stringPairToHex(expiry_date) + "5A");



        //byte[] creator={0x52,0x56,0x41,0x4c,0x49,0x44,0x45,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
        // Creator consists of 18 bytes, 18th byte must be zero. 
        int creatorLen = creator.length;
        if (creatorLen < i_creator_len) {

            for (int i = 1; i <= i_creator_len - creatorLen; i++) {
                creator = util.combine_data(creator, creator_terminator);
            }

        }

        cbeff_header = util.combine_data(header_ver, sec_option);

        cbeff_header = util.combine_data(cbeff_header, bio_len);
        cbeff_header = util.combine_data(cbeff_header, sb_len);

        cbeff_header = util.combine_data(cbeff_header, format_owner);
        cbeff_header = util.combine_data(cbeff_header, format_type);

        cbeff_header = util.combine_data(cbeff_header, creation_dt);
        cbeff_header = util.combine_data(cbeff_header, valid_from_dt);
        cbeff_header = util.combine_data(cbeff_header, expiry_dt);

        cbeff_header = util.combine_data(cbeff_header, bio_type);
        cbeff_header = util.combine_data(cbeff_header, bio_data_type);
        cbeff_header = util.combine_data(cbeff_header, bio_data_qualty);

        cbeff_header = util.combine_data(cbeff_header, creator);
        cbeff_header = util.combine_data(cbeff_header, FASC);
        cbeff_header = util.combine_data(cbeff_header, reserved);

        //System.out.println("Final Data "+ util.arrayToHex(cbeff_header));

        return cbeff_header;

    }

    //public byte[] computeSignature(byte[] bioData){
    public byte[] formatHeader() {

        /*Facial Header
        Format Identifier (5.4.1)          MF 	MV 	0x46414300 	i.e. ASCII "FAC\0"
        Version Number (5.4.2)             MF 	MV 	0x30313000 	i.e. ASCII "010\0"
        Record Length (5.4.3)              MF 	MV 	MIT 	See Note 1
        Number of Facial Images (5.4.4) 	MF 	MV 	>= 1 	2 bytes One or more images
         */

        byte[] rec_header = null;

        rec_header = util.combine_data(format_id, ver_nbr);
        rec_header = util.combine_data(rec_header, rec_len);
        rec_header = util.combine_data(rec_header, num_facial);


        //System.out.println("Final Data "+ util.arrayToHex(rec_header));

        return rec_header;

    }

    public byte[] getFeature() {

        /*Following fields populated with meaningful values at agency discretion, otherwise 0 for Unspecified.
        
        Facial image Block Length (5.5.1)            MF MV MIT       4 bytes
        Number of Feature Points (5.5.2)             MF MV           2 bytes
        Gender (5.5.3) 				MF OV OIT       1 Bytes
        Eye color (5.5.4) 				MF OV OIT       1 bytes
        Hair color (5.5.5) 				MF OV OIT       1 bytes
        Feature Mask (5.5.6) 			MF OV OIT       3 bytes
        Expression (5.5.7) 				MF OV  1        2 bytes
        Pose Angles (5.5.8)                          MF OV  0        3 bytes	Unspecified = Frontal
        Pose Angle Uncertainty (5.5.9)               MF OV  0        3 bytes             */



        //00 00  16 BC
        //00 00  00 00  00 00  00 00  00 00 00 00 00 00 00 00

        /*Features:
        MPEG4 Features (5.6.1) 				NC OIT
        Center of Facial Features (5.6.2)                   NC OIT
        The Facial Feature Block Encoding (5.6.3)           OF OV 	OIT
        
        Image Info. Each instance has image-specific info:
        
        Facial Image Type (5.7.1)       MF  MV 	1 	  1 byte
        Image Data Type (5.7.2)         MF  MV 	0 or 1    1 byte Compression algorithm.
        Width (5.7.3)                   MF  MV 	MIT 	  2 bytes See Note 7.
        Height (5.7.4)                  MF MV 	MIT 	  2 bytes
        Image Color Space (5.7.5)       MF MV 	1 	  1 byte sRGB. See Note 8.
        Source Type (5.7.6)             MF MV 	2 or 6    1 byte Digital Pic or digital Vid
        Device Type  		    MF MV 	MIT       1 byte
        (vendor supplied device ID) (5.7.7)
        Quality (5.7.8)                 MF MV 	A 	  2 byte [FACESTD] requires 0 											(unspecified)
        
        01
        01
        01 00
        01 00
        01
        02
        00
        00 00
        
         *00*/
        /***************************************************************/
        byte[] feature_header = null;




        feature_header = util.combine_data(image_block, feature_points);
        feature_header = util.combine_data(feature_header, gender);
        feature_header = util.combine_data(feature_header, eye_color);
        feature_header = util.combine_data(feature_header, hair_color);
        feature_header = util.combine_data(feature_header, feature_mask);
        feature_header = util.combine_data(feature_header, expression);
        feature_header = util.combine_data(feature_header, pose_angle);
        feature_header = util.combine_data(feature_header, pose_angle_uncertainity);
        feature_header = util.combine_data(feature_header, image_type);

        feature_header = util.combine_data(feature_header, image_data_type);
        feature_header = util.combine_data(feature_header, width);
        feature_header = util.combine_data(feature_header, height);
        feature_header = util.combine_data(feature_header, image_color);

        feature_header = util.combine_data(feature_header, source_type);
        feature_header = util.combine_data(feature_header, device_type);
        feature_header = util.combine_data(feature_header, quality);


        //System.out.println("Final Data "+ util.arrayToHex(rec_header));

        return feature_header;


    }

    private void calcBertValue(Integer len) {
        // Determine value of Bert Tag
        byte[] bert_tag;

        if (len > 255) {
            bert_tag = len_tag_big;

        } else {
            bert_tag = len_tag_small;

        }

        // Now calculate actual data size being stored.
        // Following condition makes sure that converted hex string is always
        // even.

        String tot_hex_size;

        if (len > 255 && len < 4096) {

            tot_hex_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_size = Integer.toHexString(len);

        }

        num_bytes = util.combine_data(bert_tag, util.hex1ToByteArray(tot_hex_size));

    }
}
