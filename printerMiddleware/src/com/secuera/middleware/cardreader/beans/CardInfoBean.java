/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.beans;

/**
 *
 * @author Satish K
 */
public class CardInfoBean {

    ChuidBean chuid;
    PrintedInfoBean printedInfo;

    public ChuidBean getChuid() {
        return chuid;
    }

    public PrintedInfoBean getPrintedInfo() {
        return printedInfo;
    }

    public void setChuid(ChuidBean chuid) {
        this.chuid = chuid;
    }

    public void setPrintedInfo(PrintedInfoBean printedInfo) {
        this.printedInfo = printedInfo;
    }

    @Override
    public String toString() {
        return "CardInfoBean [chuid=" + chuid + ", printedInfo="
                + printedInfo + "]";
    }
}
