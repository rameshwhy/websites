/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.io.Serializable;

/**
 *
 * @author satishkansal
 */
public class UserCSR implements Serializable  {
    private CSRBean reqCSRPivAuthentication9A;
    private CSRBean reqCSRDigSignature9C;
    private CSRBean reqCSRKeyManagement9D;
    private CSRBean reqCSRCardAuthentication9E;

    private String csrPivAuthentication9A;
    private String csrDigSignature9C;
    private String csrKeyManagement9D;
    private String csrCardAuthentication9E;

    
    private String respCertPivAuthentication9A;
    private String respCertDigSignature9C;
    private String respCertKeyManagement9D;
    private String respCertCardAuthentication9E;
    private FascnBean fascn;

    public String getCsrCardAuthentication9E() {
        return csrCardAuthentication9E;
    }

    public void setCsrCardAuthentication9E(String csrCardAuthentication9E) {
        this.csrCardAuthentication9E = csrCardAuthentication9E;
    }

    public String getCsrDigSignature9C() {
        return csrDigSignature9C;
    }

    public void setCsrDigSignature9C(String csrDigSignature9C) {
        this.csrDigSignature9C = csrDigSignature9C;
    }

    public String getCsrKeyManagement9D() {
        return csrKeyManagement9D;
    }

    public void setCsrKeyManagement9D(String csrKeyManagement9D) {
        this.csrKeyManagement9D = csrKeyManagement9D;
    }

    public String getCsrPivAuthentication9A() {
        return csrPivAuthentication9A;
    }

    public void setCsrPivAuthentication9A(String csrPivAuthentication9A) {
        this.csrPivAuthentication9A = csrPivAuthentication9A;
    }

    public FascnBean getFascn() {
        return fascn;
    }

    public void setFascn(FascnBean fascn) {
        this.fascn = fascn;
    }

    public CSRBean getReqCSRCardAuthentication9E() {
        return reqCSRCardAuthentication9E;
    }

    public void setReqCSRCardAuthentication9E(CSRBean reqCSRCardAuthentication9E) {
        this.reqCSRCardAuthentication9E = reqCSRCardAuthentication9E;
    }

    public CSRBean getReqCSRDigSignature9C() {
        return reqCSRDigSignature9C;
    }

    public void setReqCSRDigSignature9C(CSRBean reqCSRDigSignature9C) {
        this.reqCSRDigSignature9C = reqCSRDigSignature9C;
    }

    public CSRBean getReqCSRKeyManagement9D() {
        return reqCSRKeyManagement9D;
    }

    public void setReqCSRKeyManagement9D(CSRBean reqCSRKeyManagement9D) {
        this.reqCSRKeyManagement9D = reqCSRKeyManagement9D;
    }

    public CSRBean getReqCSRPivAuthentication9A() {
        return reqCSRPivAuthentication9A;
    }

    public void setReqCSRPivAuthentication9A(CSRBean reqCSRPivAuthentication9A) {
        this.reqCSRPivAuthentication9A = reqCSRPivAuthentication9A;
    }

    public String getRespCertCardAuthentication9E() {
        return respCertCardAuthentication9E;
    }

    public void setRespCertCardAuthentication9E(String respCertCardAuthentication9E) {
        this.respCertCardAuthentication9E = respCertCardAuthentication9E;
    }

    public String getRespCertDigSignature9C() {
        return respCertDigSignature9C;
    }

    public void setRespCertDigSignature9C(String respCertDigSignature9C) {
        this.respCertDigSignature9C = respCertDigSignature9C;
    }

    public String getRespCertKeyManagement9D() {
        return respCertKeyManagement9D;
    }

    public void setRespCertKeyManagement9D(String respCertKeyManagement9D) {
        this.respCertKeyManagement9D = respCertKeyManagement9D;
    }

    public String getRespCertPivAuthentication9A() {
        return respCertPivAuthentication9A;
    }

    public void setRespCertPivAuthentication9A(String respCertPivAuthentication9A) {
        this.respCertPivAuthentication9A = respCertPivAuthentication9A;
    }
 
    
}
