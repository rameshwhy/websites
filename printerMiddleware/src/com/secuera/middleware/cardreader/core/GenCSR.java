package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.entities.MiddleWareException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Arrays;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import sun.security.util.DerEncoder;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.util.ObjectIdentifier;
import sun.security.x509.X500Name;
import sun.misc.BASE64Encoder;
import sun.security.x509.AlgorithmId;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;


public class GenCSR {

    // Author: Harpreet Singh
    /*
     * 1. Registration Authority RA enters the SO PIN or Admin Key to get access
     * to the user’s smartcard.[KSS] Typically the RA has already inserted their
     * own card to authenticate to the system, they log into the Card being
     * issued as User, not SO. PIN change is forced on first user login 2. RA
     * generates a public/private key pair on the user’s smartcard with
     * appropriate key size and algorithm (RSA / ECC). 3. RA extracts the user’s
     * public key from the smartcard. 4. RA creates a certificate request
     * PKCS#10 with (Subject distinguish name, user’s public key, and additional
     * attributes listed in PKCS#9) 5. Then request gets signed by the user’s
     * private key on the smartcard that matches user’s public key in the
     * PKCS#10 request. 6. This signed request goes to the CA 7. Then the CA
     * generates a public key certificate using the information in the PKCS#10
     * request 8. Then the CA signs this certificate using the CA private key 9.
     * Then the certificate gets returned back to the requestor (RA) 10. RA will
     * load the certificate to the smartcard in the same logical container as
     * the public/private key pair and bind the certificate to the existing keys
     * by updating ??? attributes. [KSS] Typically the CKA_Label attribute I
     * think is used to match up the key pairs and the cert. 11. How does CA
     * certificate get loaded to the card I am not sure but I have seen that CA
     * certificate and user certificate on the same card. When I insert the card
     * windows will ask do I want to install the CA certificate. [KSS] This is
     * dependent on the CMS or infrastructure, to the card it’s just another
     * object that is stored. So as long as the client has access to the CA cert
     * it can store it on the card, this could be done by the RA.. 12. For
     * revocation I assume the RA just sends a revocation request with the
     * serial numbers for certificate it wants the CA to revoke and add to the
     * CRL.
     *
     *
     * CN: Common name / domain name / server name / FQDN: Indicate here your
     * SSL server name, such as "secure.company.com", "www.my-domain.com" or
     * "www.product.com". No IP address (learn more). No spaces nor blank
     * characters.
     *
     * Even if we do not advise so, intranet addresses can be listed in the CSR
     * (learn more)
     *
     * If you need to order a multiple-domain / SANs certificate, indicate the
     * main address only when generating your CSR. This address will remain the
     * same until the certificate expiration. Then enter the other addresses you
     * want to secure in the order form. Those ones will be changeable through
     * reissuances.
     *
     * O: Organisation / Company Name: indicate the corporate name of your
     * company (no trade name or acronym), in uppercase preferably.
     *
     * ST: State: in France indicate the name of the department where your
     * company headquarters are based (not the number).
     *
     * L: Location / City: indicate the city where your company headquarters are
     * based.
     *
     * C: Country: indicate FR if your company is in France, BE for Belgium,
     * etc, in uppercase preferably.
     *
     * OU: Organisational unit / Department / Branch : We advise not to fill in
     * this field or to enter a generic term such as "IT Department".
     *
     *
     *  A block type BT, a padding string PS, and the data D shall be
    formatted into an octet string EB, the encryption block.

    EB = 00 || BT || PS || 00 || D .           (1)

    The block type BT shall be a single octet indicating the structure of
    the encryption block. For this version of the document it shall have
    value 00, 01, or 02. For a private- key operation, the block type
    shall be 00 or 01. For a public-key operation, it shall be 02.

    The padding string PS shall consist of k-3-||D|| octets. For block
    type 00, the octets shall have value 00; for block type 01, they
    shall have value FF; and for block type 02, they shall be
    pseudorandomly generated and nonzero. This makes the length of the
    encryption block EB equal to k.

    Notes.

    1.   The leading 00 octet ensures that the encryption
    block, converted to an integer, is less than the modulus.

    2.   For block type 00, the data D must begin with a
    nonzero octet or have known length so that the encryption
    block can be parsed unambiguously. For block types 01 and
    02, the encryption block can be parsed unambiguously since
    the padding string PS contains no octets with value 00 and
    the padding string is separated from the data D by an octet
    with value 00.

    3.   Block type 01 is recommended for private-key
    operations. Block type 01 has the property that the
    encryption block, converted to an integer, is guaranteed to
    be large, which prevents certain attacks of the kind
    proposed by Desmedt and Odlyzko [DO86].

    4.   Block types 01 and 02 are compatible with PEM RSA
    encryption of content-encryption keys and message digests
    as described in RFC 1423.



     */
    public GenCSR() {
        super();
    }
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    GeneralAuth auth = new GeneralAuth();
    PivUtilInterface piv=new PivUtil();

    private String str_Email;
    private byte[] num_bytes;
    private byte[] bert_tag;
    private byte[] Exponent;
    private byte[] Modulus;
    private byte[] encoded;
    private byte[] RSA_SHA512 = {0x07};
    private byte[] RSA_SHA256 = {0x07};
    private byte[] SHA512_digest_info;

    /* Declare OIDS here*/
    final int sha512WithRSAEncryption_data[] = {1, 2, 840, 113549, 1, 1, 13};
    // Extension Request
    final int Extension_request_data[] = {1, 2, 840, 113549, 1, 9, 14};
    // Key Usage
    final int KeyUsage_data[] = {2, 5, 29, 15};
    // Os Version microsoft CA
    final int os_version_data[] = {1, 3, 6, 1, 4, 1, 311, 13, 2, 3};
    // Email Address
    final int emailAddress_data[] = {1, 2, 840, 113549, 1, 9, 1};

    final int certificateTemplate_data[]= {1,3, 6, 1, 4, 1, 311, 21, 7};
    // Request Client Info
    final int requestClient_Info_data[] = {1, 3, 6, 1, 4, 1, 311, 21, 20};
    //enrolmentCSP
    final int enrolmentCSP_data[] = {1, 3, 6, 1, 4, 1, 311, 13, 2, 2};
    //Alternate Subject Information
    final int subject_altinfo_data[] = {2, 5, 29, 17};
    final int fascn_data[] = {2, 16, 840, 1, 101, 3, 6, 6};
    final int upn_data[] = {1,3,6,1,4,1,311,20,2,3};
    int[] template_oid_data;
    int major_ver;
    int minor_ver;
    
    
    char certType;
    
   
    public void populateTemplateVar() {

        /*
        //PIV-IAuthentication
        byte[] pivAuthentication9A = {(byte) 0x30, (byte) 0x2F, (byte) 0x06, (byte) 0x27, (byte) 0x2B, (byte) 0x06, (byte) 0x01,
        (byte) 0x04, (byte) 0x01, (byte) 0x82, (byte) 0x37, (byte) 0x15, (byte) 0x08, (byte) 0x85, (byte) 0xA5, (byte) 0xCA,
        (byte) 0x76, (byte) 0x83, (byte) 0xB8, (byte) 0x8F, (byte) 0x4D, (byte) 0x81, (byte) 0xE5, (byte) 0x8F, (byte) 0x1B,
        (byte) 0x84, (byte) 0xB2, (byte) 0xB1, (byte) 0x61, (byte) 0x86, (byte) 0xFE, (byte) 0xC9,
        (byte) 0x4B, (byte) 0x81, (byte) 0x70,
        (byte) 0x81, (byte) 0xAB, (byte) 0xF9, (byte) 0x23, (byte) 0x87, (byte) 0x94, (byte) 0xB9, (byte) 0x53,
        (byte) 0x02, (byte) 0x01, (byte) 0x64, (byte) 0x02, (byte) 0x01,
        (byte) 0x07};


        byte[] digSignature9C = {(byte) 0x30, (byte) 0x2F, (byte) 0x06, (byte) 0x27, (byte) 0x2B, (byte) 0x06, (byte) 0x01,
        (byte) 0x04, (byte) 0x01, (byte) 0x82, (byte) 0x37, (byte) 0x15, (byte) 0x08, (byte) 0x85, (byte) 0xA5, (byte) 0xCA,
        (byte) 0x76, (byte) 0x83, (byte) 0xB8, (byte) 0x8F, (byte) 0x4D, (byte) 0x81, (byte) 0xE5, (byte) 0x8F, (byte) 0x1B,
        (byte) 0x84, (byte) 0xB2, (byte) 0xB1, (byte) 0x61, (byte) 0x86, (byte) 0xFE, (byte) 0xC9,
        (byte) 0x4B, (byte) 0x81, (byte) 0x70,
        (byte) 0x85, (byte) 0xD9, (byte) 0xAF, (byte) 0x37, (byte) 0x82, (byte) 0xA2, (byte) 0xEF, (byte) 0x6D,
        (byte) 0x02, (byte) 0x01, (byte) 0x64, (byte) 0x02, (byte) 0x01,
        (byte) 0x09};


        byte[] keyManagement9D = {(byte) 0x30, (byte) 0x2F, (byte) 0x06, (byte) 0x27, (byte) 0x2B, (byte) 0x06, (byte) 0x01, (byte) 0x04, (byte) 0x01, (byte) 0x82, (byte) 0x37, (byte) 0x15, (byte) 0x08, (byte) 0x85, (byte) 0xA5, (byte) 0xCA, (byte) 0x76, (byte) 0x83, (byte) 0xB8, (byte) 0x8F, (byte) 0x4D, (byte) 0x81, (byte) 0xE5, (byte) 0x8F, (byte) 0x1B, (byte) 0x84, (byte) 0xB2, (byte) 0xB1, (byte) 0x61, (byte) 0x86, (byte) 0xFE, (byte) 0xC9, (byte) 0x4B, (byte) 0x81, (byte) 0x70, (byte) 0x85, (byte) 0xE8, (byte) 0x90, (byte) 0x52, (byte) 0x81, (byte) 0xAC, (byte) 0x93, (byte) 0x1D, (byte) 0x02, (byte) 0x01, (byte) 0x64, (byte) 0x02, (byte) 0x01, (byte) 0x0B};

        //
        byte[] cardAuthentication9E = {(byte) 0x30, (byte) 0x2F, (byte) 0x06, (byte) 0x27, (byte) 0x2B, (byte) 0x06, (byte) 0x01,
        (byte) 0x04, (byte) 0x01, (byte) 0x82, (byte) 0x37, (byte) 0x15, (byte) 0x08, (byte) 0x85, (byte) 0xA5, (byte) 0xCA,
        (byte) 0x76, (byte) 0x83, (byte) 0xB8, (byte) 0x8F, (byte) 0x4D, (byte) 0x81, (byte) 0xE5, (byte) 0x8F, (byte) 0x1B,
        (byte) 0x84, (byte) 0xB2, (byte) 0xB1, (byte) 0x61, (byte) 0x86, (byte) 0xFE, (byte) 0xC9,
        (byte) 0x4B, (byte) 0x81, (byte) 0x70, (byte) 0x86, (byte) 0x98, (byte) 0xF9, (byte) 0x54, (byte) 0x86, (byte) 0xCD,
        (byte) 0xB7, (byte) 0x1A, (byte) 0x02, (byte) 0x01, (byte) 0x64, (byte) 0x02, (byte) 0x01,
        (byte) 0x08};

         */
        //PIV-IAuthentication
        /*byte[] pivAuthentication9A = {(byte) 0x30, (byte) 0x2C, 
         * (byte) 0x06, (byte) 0x24, 
         * (byte) 0x2B, (byte) 0x06, (byte) 0x01, (byte) 0x04, (byte) 0x01, (byte) 0x82, (byte) 0x37, (byte) 0x15,
         * (byte) 0x08, (byte) 0x84, (byte) 0xD6, (byte) 0xEA, (byte) 0x57, (byte) 0xC0, (byte) 0xF7, (byte) 0x09,
         * (byte) 0x82, (byte) 0xF5, (byte) 0x85, (byte) 0x14, (byte) 0xD7, (byte) 0xF0, (byte) 0x44, (byte) 0x86, (byte) 0xE6, 
         * (byte) 0x8F, (byte) 0x20, (byte) 0x65, (byte) 0x84, (byte) 0xBC, (byte) 0xBB, (byte) 0x13, (byte) 0x84,
         * (byte) 0xB8, (byte) 0xA1, (byte) 0x16, 
         * (byte) 0x02, (byte) 0x01, (byte) 0x64, (byte) 0x02, (byte) 0x01, (byte) 0x07};
        
        * 
        * 
        * 
        * byte[] digSignature9C = {(byte) 0x30, (byte) 0x2C, (byte) 0x06, (byte) 0x24, (byte) 0x2B, (byte) 0x06, (byte) 0x01, (byte) 0x04, (byte) 0x01, (byte) 0x82, (byte) 0x37, (byte) 0x15, (byte) 0x08, (byte) 0x84, (byte) 0xD6, (byte) 0xEA, (byte) 0x57, (byte) 0xC0, (byte) 0xF7, (byte) 0x09, (byte) 0x82, (byte) 0xF5, (byte) 0x85, (byte) 0x14, (byte) 0xD7, (byte) 0xF0, (byte) 0x44, (byte) 0x86, (byte) 0xE6, (byte) 0x8F, (byte) 0x20, (byte) 0x65, (byte) 0x85, (byte) 0x86, (byte) 0x8D, (byte) 0x18, (byte) 0x82, (byte) 0xF6, (byte) 0x9B, (byte) 0x22, (byte) 0x02, (byte) 0x01, (byte) 0x64, (byte) 0x02, (byte) 0x01, (byte) 0x06};
        byte[] keyManagement9D = {(byte) 0x30, (byte) 0x2C, (byte) 0x06, (byte) 0x24, (byte) 0x2B, (byte) 0x06, (byte) 0x01, (byte) 0x04, (byte) 0x01, (byte) 0x82, (byte) 0x37, (byte) 0x15, (byte) 0x08, (byte) 0x84, (byte) 0xD6, (byte) 0xEA, (byte) 0x57, (byte) 0xC0, (byte) 0xF7, (byte) 0x09, (byte) 0x82, (byte) 0xF5, (byte) 0x85, (byte) 0x14, (byte) 0xD7, (byte) 0xF0, (byte) 0x44, (byte) 0x86, (byte) 0xE6, (byte) 0x8F, (byte) 0x20, (byte) 0x65, (byte) 0x87, (byte) 0xC4, (byte) 0x8B, (byte) 0x15, (byte) 0x83, (byte) 0xE6, (byte) 0xFC, (byte) 0x23, (byte) 0x02, (byte) 0x01, (byte) 0x64, (byte) 0x02, (byte) 0x01, (byte) 0x09};
        //
        byte[] cardAuthentication9E = {(byte) 0x30, (byte) 0x2A, (byte) 0x06, (byte) 0x22, (byte) 0x2B, (byte) 0x06, (byte) 0x01, (byte) 0x04, (byte) 0x01, (byte) 0x82, (byte) 0x37, (byte) 0x15, (byte) 0x08, (byte) 0x84, (byte) 0xD6, (byte) 0xEA, (byte) 0x57, (byte) 0xC0, (byte) 0xF7, (byte) 0x09, (byte) 0x82, (byte) 0xF5, (byte) 0x85, (byte) 0x14, (byte) 0xD7, (byte) 0xF0, (byte) 0x44, (byte) 0x86, (byte) 0xE6, (byte) 0x8F, (byte) 0x20, (byte) 0x65, (byte) 0xA5, (byte) 0xCB, (byte) 0x6B, (byte) 0xF9, (byte) 0x91, (byte) 0x6D, (byte) 0x02, (byte) 0x01, (byte) 0x64, (byte) 0x02, (byte) 0x01, (byte) 0x06};
        */
        //X.509 Certificate for	PIV Authentication		NIST ‘5FC105’---> '9A' (P)
        //X.509 Certificate for	Digital Signature               NIST ‘5FC10A’ ---> '9C' (C)
        //X.509 Certificate for Key management              NIST ‘5FC10B’   ----> '9D' (K)
        //X.509 Certificate for	Card Authentication             NIST ‘5FC101    -----> '9E' ()

       /* if (certType == 'P') {
            pivTemplateName = pivAuthentication9A;
        } else if (certType == 'C') {
            pivTemplateName = digSignature9C;
        } else if (certType == 'K') {
            pivTemplateName = keyManagement9D;
        } else {
            pivTemplateName = cardAuthentication9E;    //'A'
        }
         */

    }

    // private static PKCS10Attributes attributeSet;
    public String getCSR(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY, char cert_type,
            byte[] key_type, String str_cn, String str_ou,
            String str_org, String str_city, String str_state,
            String str_country, String str_email,int[] cert_template_oid,int majorVer,int minorVer) throws MiddleWareException, CardException {

        String csr_data = null;
        str_Email=str_email;

        certType = cert_type;
        
        template_oid_data=cert_template_oid;
        major_ver=majorVer;
        minor_ver=minorVer;
        
        populateTemplateVar();

        genPublicKey(channel, PRE_PERSO_PIV_ADMIN_KEY, PIV_ADMIN_KEY, key_type);

        // Generate CSR.
        try {
            csr_data = genPKCS10(str_cn, str_ou, str_org, str_city, str_state,
                    str_country, channel, key_type);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("Final CSR " + csr_data);

        //System.out.println("In String  " +new String(csr_data));

        //Now we can save this CSR in PEM format in the file.
        // PEM format is base64 encoded form which is used when sending the CSR in
        // mail or upload to CA.



        return csr_data;
    }

    void genPublicKey(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY, byte[] key_type) throws MiddleWareException {

        // Cert_type ---> Means Type of certificate...'9A','9C','9D','9E'
        // Key Type ---> Type of algo, like 3DES,AES,EDCH, RSA

        StringBuffer sb = new StringBuffer();
        byte[] result = null;
        // *****************************************************//

        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }




        //X.509 Certificate for	PIV Authentication		NIST ‘5FC105’---> '9A'
        //X.509 Certificate for	Digital Signature  	    NIST ‘5FC10A’ ---> '9C'
        //X.509 Certificate for 	Key management  	NIST ‘5FC10B’   ----> '9D'
        //X.509 Certificate for	Card Authentication	NIST ‘5FC101    -----> '9E'



        byte[] piv_auth = {(byte) 0x9A};
        byte[] card_digi = {(byte) 0x9C};
        byte[] key_manage = {(byte) 0x9D};
        byte[] card_auth = {(byte) 0x9E};

        byte[] cert_id;


        if (certType == 'P') {
            cert_id = piv_auth;
        } else if (certType == 'C') {
            cert_id = card_digi;
        } else if (certType == 'K') {
            cert_id = key_manage;
        } else {
            cert_id = card_auth;    //'A'
        }



        // Key Type Indicates ECC 256 or RSA key generation.

        byte[] apdu_class = {(byte) 0x00, (byte) 0x47, (byte) 0x00};
        byte[] apdu_lc = {0x00, 0x00, 0x05};
        byte[] key_gen = {(byte) 0xAC, 0x03, (byte) 0x80, 0x01};

        byte[] command_data = util.combine_data(apdu_class, cert_id);
        command_data = util.combine_data(command_data, apdu_lc);
        command_data = util.combine_data(command_data, key_gen);
        command_data = util.combine_data(command_data, key_type);

        System.out.println("APDU KEY COMMAND  " + util.arrayToHex(command_data));
        CommandAPDU KEY_APDU = new CommandAPDU(command_data);

        try {

            result = sendCard.sendCommand(channel, KEY_APDU, sb);
            System.out.println("Returned Public Key step 5_5:"
                    + util.arrayToHex(result));
        } catch (CardException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        //Public key data objects for RSA
        //‘82’ Var. Public exponent
        //‘81’ Var. Modulus


        //7f 49
        //82 01 09
        //82 03 01 00 01
        //81 82 01 00
        //92 65 92 7a
        //27d43a0e97ff0dff894ca1be5afa11a7476cd9139588a745d7041e9239b671847ccd9c9191e6bab2094ddd8b4ee86ac97615f75288eea4577f8bec3405d85560aec2cee2453d2cf03653668da00588ea52abf6738d78a594cce8eae3a15cc712bf474fe988d629d8871b748795b4dd4a8094f84d998cb13d91d48e855693d503ced395a0caa3a251b217999fb07ae313818a4f7dae5cb15ac1735cf7e9ec7b06f1b72282b9a44b826a6c9f1a0b8c4be71fed2916de12a5c44ef3050f3284fed9211064462f7027cf55ee8bea4cc373476ed3f9792fa25ceedf76e34656e114f695d6eeb3ead156b4590afb6982a845fef00932352ff677bd4e0c21df

        Exponent = util.extract_data(result, 7, 3);
        Modulus = util.strip_data(result, 14);


        System.out.println("Returned Public Key step 5_6, Exponent :"
                + util.arrayToHex(Exponent));
        System.out.println("Returned Public Key step 5_7 Modulus :"
                + util.arrayToHex(Modulus));

    }

    private String genPKCS10(String str_cn, String str_ou, String str_org,
            String str_city, String str_state, String str_country, CardChannel channel,
            byte[] key_type)
            throws Exception {

        // generate PKCS10 certificate request
         DerOutputStream out, certrequest;
        byte[] certificateRequestInfo;
        // PKCS10 pkcs10 = new PKCS10(PublicKey);

        X500Name x500Name = new X500Name(str_cn, str_ou, str_org, str_city,
                str_state, str_country);


        System.out.println("X500  NAME " + x500Name.toString());


        certrequest = new DerOutputStream();
        certrequest.putInteger(BigInteger.ZERO);

        x500Name.encode(certrequest); // X.500 name


        BigInteger intmodulus = new BigInteger(Modulus);
        BigInteger intexponent = new BigInteger(Exponent);


        RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(intmodulus, intexponent);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(pubKeySpec);

        certrequest.write(publicKey.getEncoded()); // public key

        // Now Encode the Extension Request
        certrequest = get_ExtendedParams(certrequest,channel);


        out = new DerOutputStream();
        out.write(DerValue.tag_Sequence, certrequest); // wrap it!
        //encoded=certificateRequestInfo;
        certrequest = out;

        /********************************************************************/
        // Now get the CSR data signed by smart card.
        byte[] signed_csr;

        certificateRequestInfo = certrequest.toByteArray();

        System.out.println("Before signature DER String " + util.arrayToHex(certificateRequestInfo));

        signed_csr = signCSR(channel, key_type, certificateRequestInfo);

        System.out.println("Step_6_1..signature " + util.arrayToHex(signed_csr));


        // Now Encode the Algotithm
        ObjectIdentifier sha512WithRSAEncryption_OID = ObjectIdentifier.newInternal(sha512WithRSAEncryption_data);
        AlgorithmId algId = new AlgorithmId(sha512WithRSAEncryption_OID);
        algId.encode(certrequest);

        certrequest.putBitString(signed_csr);  // signature

        /*
         * Wrap those guts in a sequence
         */
        out = new DerOutputStream();
        out.write(DerValue.tag_Sequence, certrequest);
        encoded = out.toByteArray();

        // Convert to Base64 format.

        System.out.println("CSR BEFORE BASE64 " + util.arrayToHex(encoded));

        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bs);
        convertToBase64(ps);

        String final_certificateRequestInfo = bs.toString();

        // Now close the streams.
        try {

            ps.close();
            bs.close();
        } catch (Throwable th) {
        }


        return final_certificateRequestInfo;

    }

    public void convertToBase64(PrintStream base64)
            throws IOException {
        /*if (encoded == null)
        throw new SignatureException("Cert request was not signed"); */

        BASE64Encoder encoder = new BASE64Encoder();

        //  base64.println("-----BEGIN NEW CERTIFICATE REQUEST-----");
        encoder.encodeBuffer(encoded, base64);
        //base64.println("-----END NEW CERTIFICATE REQUEST-----");
    }

    private void calcBertValue(Integer len, boolean binclude_bert) {
        // Determine value of Bert Tag
        // if (binclude_bert)
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for
        // size of data
        // bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size
        // of data bytes
        byte[] len_tag_small = {(byte) 0x81};
        byte[] len_tag_big = {(byte) 0x82};

        if (len > 255) {
            if (binclude_bert) {
                bert_tag = bert_tag_size_big;
            } else {
                bert_tag = len_tag_big;
            }

        } else {
            if (binclude_bert) {
                bert_tag = bert_tag_size_small;
            } else {
                bert_tag = len_tag_small;
            }

        }

        // Now calculate actual data size being stored.
        // Following condition makes sure that converted hex string is always
        // even.

        String tot_hex_size;

        if (len > 255 && len < 4096) {

            tot_hex_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_size = Integer.toHexString(len);

        }

        num_bytes = util.hex1ToByteArray(tot_hex_size);

    }

    public byte[] getEncoded() {

        if (encoded != null) {
            return encoded.clone();
        } else {
            return null;
        }

    }

    byte[] signCSR(CardChannel channel, byte[] algo_type,
            byte[] csr_input) throws MiddleWareException {

        StringBuffer sb = new StringBuffer();
        byte[] res = null;
        byte[] csr_data = null;



        //X.509 Certificate for	PIV Authentication		NIST ‘5FC105’---> '9A'
        //X.509 Certificate for	Digital Signature  	    NIST ‘5FC10A’ ---> '9C'
        //X.509 Certificate for 	Key management  	NIST ‘5FC10B’   ----> '9D'
        //X.509 Certificate for	Card Authentication	NIST ‘5FC101    -----> '9E'



        byte[] piv_auth = {(byte) 0x9A};
        byte[] card_digi = {(byte) 0x9C};
        byte[] key_manage = {(byte) 0x9D};
        byte[] card_auth = {(byte) 0x9E};

        byte[] cert_id;


        if (certType == 'P') {
            cert_id = piv_auth;
        } else if (certType == 'C') {
            cert_id = card_digi;
        } else if (certType == 'K') {
            cert_id = key_manage;
        } else {
            cert_id = card_auth;    //'A'
        }




        // Generate the Hash for the csr data.
        if (Arrays.equals(algo_type, RSA_SHA512)) {
            genHash_RSA512(csr_input);
            csr_data = SHA512_digest_info;
        } else if (Arrays.equals(algo_type, RSA_SHA256)) {
            genHash_RSA256(csr_input);
        } else {
            genHash_ECC(csr_input);
        }

        //System.out.println("Length of FINAL HASH :" + csr_data.length);



        PivUtil mwServer = new PivUtil();
        boolean retValue;
        try {
            retValue = mwServer.verifyLocalPin(channel, "123456", 8);
            System.out.println("Local Pin Verfied " + retValue);
        } catch (MiddleWareException e) {
            System.out.println(e.getMessage());
        }


        /*
         * set the signature mode.
         *
         * Field Value CLA ‘00’ INS ‘87’: Authenticate Service P1 Algorithm
         * Identifier P2 Key Reference Lc Data field Le Empty
         */

        // 00 87 06 9c
        //7c
        //24 82 00 81 20
        //14 b9 af 2a 99 47 1a 75 a8 45 1e b1 db ed eb bf
        //3c dd fa 21 33 da f9 6e 70 6c 39 d3 aa 81 ed e9
        //00 87 07 9A 8A
        //7C 82 01 06 82 00 81 82 01 00 00 01 FF FF 00 01

        //00 87 07 9c 54
        //8a 7c 51 82 00 81 4d
        //0001ffffffffffffffffffff001d18aa89b4a58e4b363856e63337d80df5b70a6e3b9c10a4d891b4ddc63c08f487344ffbbced9e3f6c01d93b5be58a33a2edc4c918c540516f626d2643a7192c

        byte[] apdu_class_sig = {0x00, (byte) 0x87};

        byte[] cont_tag = {0x7C};

        byte[] tag_sig_data = {(byte) 0x82, 0x00, (byte) 0x81};

        Integer len = csr_data.length;

        // Calculate size of the CSR Data
        calcBertValue(len, false);

        byte[] byte_csr_len_2 = num_bytes;

        byte[] csr_data_buf = util.combine_data(bert_tag, byte_csr_len_2);
        csr_data_buf = util.combine_data(csr_data_buf, csr_data);
        csr_data_buf = util.combine_data(tag_sig_data, csr_data_buf);

        // Now Calculate Length of data after adding tag_sig_data.

        Integer csr_len = csr_data_buf.length;
        calcBertValue(csr_len, false);

        byte[] byte_csr_len = num_bytes;

        byte[] len_buf = util.combine_data(bert_tag, byte_csr_len);
        csr_data_buf = util.combine_data(len_buf, csr_data_buf);
        csr_data_buf = util.combine_data(cont_tag, csr_data_buf);


        // Now Calculate Total Length of data, i-e LC Value.

        Integer Lc_len = csr_data_buf.length;
        calcBertValue(Lc_len, false);

        byte[] total_size = num_bytes;

        byte[] zero_size = {0x00};

        if (total_size.length > 1) {
            total_size = util.combine_data(zero_size, total_size);
        }



        byte[] command_data_final = util.combine_data(apdu_class_sig, algo_type);
        command_data_final = util.combine_data(command_data_final, cert_id);
        command_data_final = util.combine_data(command_data_final, total_size);
        command_data_final = util.combine_data(command_data_final, csr_data_buf);

        System.out.println("Step_10 :" + util.arrayToHex(command_data_final));

        CommandAPDU KEY_APDU_SIGN = new CommandAPDU(command_data_final);




        try {

            res = sendCard.sendCommand(channel, KEY_APDU_SIGN, sb);
        } catch (CardException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


        //7c 82 01 04 82 82 01 00
        //3c09cca

        System.out.println("Returned Signed CSR Status :" + sb.toString());
        System.out.println("Returned Signed CSR :" + util.arrayToHex(res));

        res = util.strip_data(res, 8);

        System.out.println("Return to main method: CSR :" + util.arrayToHex(res));

        return res;

    }

    void genHash_RSA512(byte[] csr_data) throws MiddleWareException {


        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        digest.update(csr_data);
        csr_data = digest.digest();


        byte[] SHA512_digest = {0x30, 0x51, 0x30, 0x0d, 0x06, 0x09, 0x60, (byte) 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x03, 0x05, 0x00, 0x04, 0x40};

        byte[] sha512_pad = {0x00, 0x01};
        byte[] sha_zero = {0x00};

        for (int i = 1; i <= 170; i++) {

            byte[] ff_pad = {(byte) 0xff};
            sha512_pad = util.combine_data(sha512_pad, ff_pad);

        }

        sha512_pad = util.combine_data(sha512_pad, sha_zero);
        SHA512_digest_info = util.combine_data(sha512_pad, SHA512_digest);
        SHA512_digest_info = util.combine_data(SHA512_digest_info, csr_data);


    }

    void genHash_RSA256(byte[] csr_data) throws MiddleWareException {
    }

    void genHash_ECC(byte[] csr_data) throws MiddleWareException {
    }

    DerOutputStream get_ExtendedParams(DerOutputStream certrequest,CardChannel channel) throws MiddleWareException, IOException {

        DerOutputStream out_os, out_client,out_extension;

        out_os = get_Os();
        out_client = get_ClientInfo();
        out_extension=get_extensions(channel);


        byte[] byte_os = out_os.toByteArray();
        byte[] byte_client = out_client.toByteArray();
        byte[] byte_extension = out_extension.toByteArray();


        byte[][] arrays = new byte[3][];

        arrays[0] = byte_os;
        arrays[1] = byte_client;
        arrays[2] = byte_extension;


        byte[] combined_bytes = util.combine_data(arrays);
        int len = combined_bytes.length;

        byte[] tot_num_bytes = calcDerLen(len);
        byte[] tot_tag = {(byte) 0xa0};


        byte[] final_tag_Sequence = util.combine_data(tot_tag, tot_num_bytes);
        byte[] ext_params = util.combine_data(final_tag_Sequence, byte_os);
               ext_params = util.combine_data(ext_params, byte_client);
               ext_params = util.combine_data(ext_params, byte_extension);

        System.out.println("5th Step " + util.arrayToHex(ext_params));


        certrequest.write(ext_params);

        // Now close the streams.out_1, out_2, out_3, out_4
        try {

            out_os.close();
            out_client.close();
            out_extension.close();


        } catch (Throwable th) {
        }


        return certrequest;

    }

    DerOutputStream get_Os() throws MiddleWareException, IOException {

        DerOutputStream out_1, out_2, out_3, out_os;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_os = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier os_version_data_OID = ObjectIdentifier.newInternal(os_version_data);
        String os_version = "6.1.7600.2";

        out_1.putOID(os_version_data_OID);
        out_2.putIA5String(os_version);

        // Encode the values in Set
        DerEncoder[] y = {out_2};
        out_3.putOrderedSet(DerValue.tag_Set, y);


        DerValue attr_oid = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_3.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_os.putSequence(x);


        // Now close the streams.out_1, out_2, out_3, out_os
        try {


                out_1.close();
                out_2.close();
                out_3.close();
                out_os.close();

        } catch (Throwable th) {
        }

        return out_os;



    }

    byte[] get_Email() throws MiddleWareException, IOException {

        DerOutputStream out_email,out_upn_oid,out_email_info ;
        out_email = new DerOutputStream();
        out_upn_oid=new DerOutputStream();
        out_email_info=new DerOutputStream();

        byte[] ext_cert_tag = {(byte) 0xa0};
        byte[] email_value=null;
        // Out put the OID and the value
        ObjectIdentifier upn_data_OID = ObjectIdentifier.newInternal(upn_data);

        if (str_Email.length()==0)
        {
            return email_value;
        }

        //A0 20 06 0A 2B
        //06 01 04 01 82 37 14 02 03
        //A0 12 0C 10 72 68 6F
        //6C 6C 65 79 40 6D 79 69 64 2E 63 6F 6D 81

        // Encode UPN OID
        out_upn_oid.putOID(upn_data_OID);

        out_email.putUTF8String(str_Email);


        int len = out_email.toByteArray().length;
        byte[] email_num_bytes = util.hex1ToByteArray(Integer.toHexString(len));

        byte[][] email_arrays = new byte[3][];
        email_arrays[0] = ext_cert_tag;
        email_arrays[1] = email_num_bytes;
        email_arrays[2] = out_email.toByteArray();

        byte[] email_bytes = util.combine_data(email_arrays);

         //Now combine Fascan_OID and Fascan Value and calculate length of bytes.
        len = util.combine_data(out_upn_oid.toByteArray(),email_bytes).length;
        byte[] tot_num_bytes = util.hex1ToByteArray(Integer.toHexString(len));

        byte[][] tot_email_value = new byte[3][];
        tot_email_value[0] = ext_cert_tag;
        tot_email_value[1] = tot_num_bytes;
        tot_email_value[2] = util.combine_data(out_upn_oid.toByteArray(),email_bytes);

        email_value = util.combine_data(tot_email_value);

        // Now Combine OID and UPN Value and encode it on the stream
        // DerValue attr_upn_val = new DerValue(email_value);

       // DerValue[] x_email = {attr_upn_val};
      //  out_email_info.putSequence(x_email);


        // Now close the streams.
        try {

            out_email.close();
            out_upn_oid.close();
            out_email_info.close();

        } catch (Throwable th)
        {

        }

        return email_value;

    }

    DerOutputStream get_ClientInfo() throws MiddleWareException, IOException {

        DerOutputStream out_1, out_2, out_3, out_4, out_int, out_seq, out_set, out_client;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_4 = new DerOutputStream();
        out_int = new DerOutputStream();
        out_seq = new DerOutputStream();
        out_set = new DerOutputStream();
        out_client = new DerOutputStream();

        ObjectIdentifier requestClient_Info_data_OID = ObjectIdentifier.newInternal(requestClient_Info_data);
        out_1.putOID(requestClient_Info_data_OID);

        out_int.putInteger(5);

        //certBean.setCertURL("https://secuera-gst-01.ca.secuera.com/certsrv/" + "certfnsh.asp");
        //Now add String values
        //String str_1 = "WIN-EIE567GPLV2.myid.com";
        //String str_2 = "MYID" + "\\" + "com_user";
        //String str_3 = "CertificateServerWindows.vshost.exe";

        String str_1 = "secuera-gst-01.ca.secuera.com";
        String str_2 = "ca.secuera.com" + "\\" + "SEAdmin";
        String str_3 = "ABC";


        out_2.putUTF8String(str_1);
        out_3.putUTF8String(str_2);
        out_4.putUTF8String(str_3);

        // Now wrap these elements in Sequence
        DerValue attr_int = new DerValue(out_int.toByteArray());
        DerValue attr_2 = new DerValue(out_2.toByteArray());
        DerValue attr_3 = new DerValue(out_3.toByteArray());
        DerValue attr_4 = new DerValue(out_4.toByteArray());

        DerValue[] y = {attr_int, attr_2, attr_3, attr_4};
        out_seq.putSequence(y);

        // Now Wrap in Set
        DerEncoder[] set = {out_seq};
        out_set.putOrderedSet(DerValue.tag_Set, set);

        DerValue attr_oid = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_set.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_client.putSequence(x);


        System.out.println("CLient Info " + util.arrayToHex(out_client.toByteArray()));

        // Now close the streams.out_1, out_2, out_3,out_4,out_int, out_seq,out_set,out_client;
        try {

            out_1.close();
            out_2.close();
            out_3.close();
            out_4.close();
            out_int.close();
            out_seq.close();
            out_set.close();
            out_client.close();

        } catch (Throwable th) {
        }

        return out_client;

    }


    DerOutputStream get_extensions(CardChannel channel) throws MiddleWareException, IOException {

        DerOutputStream out_ext_oid,outer_seq,out_certTemplate,out_ext,out_set,out_altsubject_info;
        out_ext_oid = new DerOutputStream();
        outer_seq = new DerOutputStream();
        out_certTemplate= new DerOutputStream();
        out_set= new DerOutputStream();
        out_ext= new DerOutputStream();
        out_altsubject_info=new DerOutputStream();

        ObjectIdentifier Extension_request_data_OID = ObjectIdentifier.newInternal(Extension_request_data);
        out_ext_oid.putOID(Extension_request_data_OID);

        //Get Cert Template
        out_certTemplate=get_certtemplate();
        // Get alt subject info
        out_altsubject_info=get_subjectAltInfo(channel);

        if (out_altsubject_info.toByteArray().length==0|| out_altsubject_info.toByteArray()==null)
        {
            // Now add one outer sequence for whole set of Parameters.
            DerValue attr_seq = new DerValue(out_certTemplate.toByteArray());

            DerValue[] outer = {attr_seq};
            outer_seq.putSequence(outer);


        }else
        {
            // Now add one outer sequence for whole set of Parameters.
            DerValue attr_seq = new DerValue(out_certTemplate.toByteArray());
            DerValue attr_seq_subject = new DerValue(out_altsubject_info.toByteArray());

            DerValue[] outer = {attr_seq,attr_seq_subject};
            outer_seq.putSequence(outer);

        }



        //*******Now Encode the whole thing in a Set*******************************************************//
        DerEncoder[] y = {outer_seq};
        out_set.putOrderedSet(DerValue.tag_Set, y);

        System.out.println("Second Step " + util.arrayToHex(out_set.toByteArray()));
        //System.out.println("3rd Step " + util.arrayToHex(out_ext.toByteArray()));

        byte[] xy = util.combine_data(out_ext_oid.toByteArray(), out_set.toByteArray());

        System.out.println("4th Step " + util.arrayToHex(xy));

        int len = xy.length;

        byte[] num_bytes = calcDerLen(len);
        byte[] tag_Sequence = {0x30};

        byte[] final_tag_Sequence = util.combine_data(tag_Sequence, num_bytes);


        xy = util.combine_data(final_tag_Sequence, xy);

        System.out.println("4th_1 Step " + util.arrayToHex(xy));


        out_ext.write(xy);

        // Now close the streams
        try {

            out_ext_oid.close();
            outer_seq.close();
            out_certTemplate.close();
            out_ext.close();
            out_set.close();

        } catch (Throwable th) {
        }

        return out_ext;

    }

    DerOutputStream get_certtemplate() throws MiddleWareException, IOException {
    
        DerOutputStream out_template,out_template_octet,out_cert_template_oid,out_temp_oid,out_major,out_minor ;
        out_temp_oid = new DerOutputStream();
        out_major = new DerOutputStream();
        out_minor = new DerOutputStream();
        out_cert_template_oid= new DerOutputStream();
        out_template_octet=new DerOutputStream();
        out_template = new DerOutputStream();
        
        // 30 2F
        // 06 27
        // 2B 06 01 04 01 82 37 15 08 85 A5 CA 76 83 B8 8F
        // 4D 81 E5 8F 1B 84 B2 B1 61 86 FE C9 4B 81 70 82
        // D6 BF 2F 82 E0 94 0F 
        // 02 01 64 02 01 04
        
        //06 24 2B 06 01 04 01 82 37 15 08 84 D6 EA 57 C0
        //                 F7 09 82 F5 85 14 D7 F0 44 86 E6 8F 20 65 84 BC
        //                 BB 13 84 B8 A1 16 
        //02 01 64 02 01 64

        
        
        ObjectIdentifier cert_template_OID = ObjectIdentifier.newInternal(certificateTemplate_data);
        out_cert_template_oid.putOID(cert_template_OID);
        
        ObjectIdentifier template_OID = ObjectIdentifier.newInternal(template_oid_data);
        out_temp_oid.putOID(template_OID);

        System.out.println("Template Step 1 " + util.arrayToHex(out_temp_oid.toByteArray()));

        out_major.putInteger(major_ver);
        out_minor.putInteger(minor_ver);
        
        
        byte[][] template_arrays = new byte[3][];
        template_arrays[0] = out_temp_oid.toByteArray();
        template_arrays[1] = out_major.toByteArray();
        template_arrays[2] = out_minor.toByteArray();

        byte[] template_bytes = util.combine_data(template_arrays);

        //Now calculate the total length of bytes and add sequence tag to it.
         byte[] seq_tag={0x30};
         int len = template_bytes.length;
         byte[] tot_num_bytes = util.hex1ToByteArray(Integer.toHexString(len));

         byte[][] tot_temp_value = new byte[3][];
	         tot_temp_value[0] = seq_tag;
	         tot_temp_value[1] = tot_num_bytes;
	         tot_temp_value[2] = template_bytes;

        template_bytes = util.combine_data(tot_temp_value);

        
        out_template_octet.putOctetString(template_bytes);
        
        DerValue attr_oid = new DerValue(out_cert_template_oid.toByteArray());
        DerValue attr_value = new DerValue(out_template_octet.toByteArray());
        
        DerValue[] x = {attr_oid,attr_value};
        out_template.putSequence(x);

        // Now close the streams
        try {
            out_template_octet.close();
            out_cert_template_oid.close();
            out_temp_oid.close();
            out_major.close();
            out_minor.close();
            out_template.close();
        } catch (Throwable th) {
        }

        return out_template;

    }

    DerOutputStream get_subjectAltInfo(CardChannel channel) throws MiddleWareException, IOException{

        DerOutputStream out_1, out_2,out_subjectinfo ;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_subjectinfo = new DerOutputStream();

        byte[] subject_alt_value;
        // Out put the OID and the value
        ObjectIdentifier subject_altinfo_data_OID = ObjectIdentifier.newInternal(subject_altinfo_data);

        //30 6D
        //A0 27 06 08 60 86 48 01 65 03 06 06 A0 1B
        //04 19 D3 61 52 59 B5 25 6C 1A 84 21 CD A1 68 58
        //      30 08 42 10 84 39 85 48 41 43 E4

        //A0 20 06 0A 2B
        //06 01 04 01 82 37 14 02 03
        //A0 12 0C 10 72 68 6F
        //6C 6C 65 79 40 6D 79 69 64 2E 63 6F 6D 81

        //30 29
        //A0 27 06 08 60 86 48 01 65 03 06 06 A0 1B
        //04 19
        //73 9C ED 39 CE 73 9D A1 68 5A 08 C9 2A DE
        //0A 61 84 E7 39 C3 E2 32 04 31 32
        //30 26
        //A0 24 06
        //0A 2B 06 01 04 01 82 37 14 02 03 A0 16 0C 14 48
        //61 72 70 72 65 65 74 40 73 65 63 75 65 72 61 2E
        //63 6F 6D

        //A0 27
        //06 08 60 86 48 01 65 03 06 06 A0 1B
        //04 19
         //:                 73 9C ED 39 CE 73 9D A1 68 5A 08 C9 2A DE 0A 61
         //:                 84 E7 39 C3 E2 32 04 31 32
         // A0 24 06 0A 2B 06 01
         //:                 04 01 82 37 14 02 03 A0 16 0C 14 48 61 72 70 72
         //:                 65 65 74 40 73 65 63 75 65 72 61 2E 63 6F 6D




        byte[] byte_fascan_info=get_fascanInfo(channel);
        byte[] byte_email_info=get_Email();

        if (byte_fascan_info.length==0 || byte_fascan_info==null){
            if(byte_email_info.length==0 || byte_email_info==null){
                 return out_subjectinfo;
            }else{
                 subject_alt_value=byte_email_info;
            }
                        
        }else{
            if(byte_email_info.length==0 || byte_email_info==null){
                subject_alt_value=byte_fascan_info;
            }else {
                subject_alt_value=util.combine_data(byte_fascan_info,byte_email_info);
            }
        }
        
        
       

        //Now wrap the Subject alt Value into Sequence

        //Now calculate the total length of bytes and add sequence tag to it.
         byte[] seq_tag={0x30};
         int len = subject_alt_value.length;
         byte[] tot_num_bytes = util.hex1ToByteArray(Integer.toHexString(len));

		         byte[][] tot_san_value = new byte[3][];
		         tot_san_value[0] = seq_tag;
		         tot_san_value[1] = tot_num_bytes;
		         tot_san_value[2] = subject_alt_value;

        subject_alt_value = util.combine_data(tot_san_value);

        /*****************Envelop all the data into Outer SAN Sequence **/
        out_1.putOID(subject_altinfo_data_OID);
        out_2.putOctetString(subject_alt_value);

        // Encode the values in Sequence
        DerValue attr_oid_subject = new DerValue(out_1.toByteArray());

        // Encode the values in Sequence
        DerValue attr_value = new DerValue(out_2.toByteArray());

        System.out.println("Fascan : " + util.arrayToHex(out_2.toByteArray()));

        DerValue[] x = {attr_oid_subject,attr_value};

        out_subjectinfo.putSequence(x);

        System.out.println("Fascan_2 : " + util.arrayToHex(out_subjectinfo.toByteArray()));


        // Now close the streams.
        try {

            out_1.close();
            out_2.close();
            out_subjectinfo.close();

        } catch (Throwable th)
        {

        }

        return out_subjectinfo;


    }



    byte[] get_fascanInfo(CardChannel channel) throws MiddleWareException, IOException{

        DerOutputStream out_fascan,out_fascan_oid,out_fascan_info ;
        out_fascan = new DerOutputStream();
        out_fascan_oid=new DerOutputStream();
        out_fascan_info=new DerOutputStream();

        byte[] ext_cert_tag = {(byte) 0xa0};
        byte[] fascan_value=null;

        // Out put the OID and the value
        ObjectIdentifier fascn_data_OID = ObjectIdentifier.newInternal(fascn_data);

        //30 6D
        //A0 27 06 08 60 86 48 01 65 03 06 06 A0 1B
        //04 19 D3 61 52 59 B5 25 6C 1A 84 21 CD A1 68 58
        //30 08 42 10 84 39 85 48 41 43 E4

        //A0 20 06 0A 2B
        //06 01 04 01 82 37 14 02 03
        //A0 12 0C 10 72 68 6F
        //6C 6C 65 79 40 6D 79 69 64 2E 63 6F 6D 81

        // Encode Fascan OID
        out_fascan_oid.putOID(fascn_data_OID);


        fascan_value=piv.getFASCN(channel);

        if (fascan_value.length==0|| fascan_value==null)
        {
            return fascan_value;
        }

        out_fascan.putOctetString(fascan_value);

        int len = out_fascan.toByteArray().length;
        byte[] fascan_num_bytes = util.hex1ToByteArray(Integer.toHexString(len));

        byte[][] fascan_arrays = new byte[3][];
        fascan_arrays[0] = ext_cert_tag;
        fascan_arrays[1] = fascan_num_bytes;
        fascan_arrays[2] = out_fascan.toByteArray();

        byte[] fascan_bytes = util.combine_data(fascan_arrays);

        //Now combine Fascan_OID and Fascan Value and calculate length of bytes.
        len = util.combine_data(out_fascan_oid.toByteArray(),fascan_bytes).length;
        byte[] tot_num_bytes = util.hex1ToByteArray(Integer.toHexString(len));

        byte[][] tot_fascan_value = new byte[3][];
        tot_fascan_value[0] = ext_cert_tag;
        tot_fascan_value[1] = tot_num_bytes;
        tot_fascan_value[2] = util.combine_data(out_fascan_oid.toByteArray(),fascan_bytes);

         fascan_value = util.combine_data(tot_fascan_value);

        // Now Combine OID and Fascan Value and encode it on the stream
        // DerValue attr_fascn_val = new DerValue(fascan_value);

       // DerValue[] x_fascn = {attr_fascn_val};
       // out_fascan_info.putSequence(x_fascn);


        // Now close the streams.
        try {

            out_fascan.close();
            out_fascan_oid.close();
            out_fascan_info.close();

        } catch (Throwable th)
        {

        }

        return fascan_value;

    }




    DerOutputStream get_keyUsgae(DerOutputStream certrequest) throws MiddleWareException, IOException {

        System.out.println("First Step "
                + util.arrayToHex(certrequest.toByteArray()));

        // ********************************************//
        // Now get the certificate template to be requested.

        ObjectIdentifier KeyUsage_data_OID = ObjectIdentifier.newInternal(KeyUsage_data);


        // String PIV="PIV-I Encryption";
        byte[] keyusage = {0x03, 0x02, 0x06, (byte) 0xC0};
        // byte[] keyusage={(byte) 0xC0};
        // Extension.newExtension(KeyUsage_data_OID,true,PIV.getBytes());

        // Extension.newExtension(KeyUsage_data_OID,true,keyusage);
        // CertificateExtensions exts = new CertificateExtensions();

        // exts.encode(certrequest);
        DerOutputStream out_1, out_2, out_3, out_4;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_4 = new DerOutputStream();

        out_1.putOID(KeyUsage_data_OID);
        out_2.putOctetString(keyusage);

        DerValue attr = new DerValue(out_1.toByteArray());
        DerValue attr1 = new DerValue(out_2.toByteArray());

        DerValue[] x = {attr, attr1};

        out_3.putSequence(x);

        // DerEncoder z= null;
        // z.derEncode(out_3);

        DerEncoder[] y = {out_3};
        out_4.putOrderedSet(DerValue.tag_Set, y);

        System.out.println("Second Step " + util.arrayToHex(out_4.toByteArray()));

        //System.out.println("3rd Step " + util.arrayToHex(out_ext.toByteArray()));

        //byte[] xy = util.combine_data(out_ext.toByteArray(),out_4.toByteArray());

        //System.out.println("4th Step " + util.arrayToHex(xy));

        //int len = xy.length;

        String tot_hex_size, hex_size;

        //hex_size = Integer.toHexString(len);
        //tot_hex_size = Integer.toHexString(len+2 );

        //	byte[] num_bytes = util.hex1ToByteArray(hex_size);
        byte[] tag_Sequence = {0x30};

        //byte[] tot_num_bytes=util.hex1ToByteArray(tot_hex_size);
        //byte[] tot_tag={(byte) 0xa0};


        //byte[] final_tag_Sequence=util.combine_data(tot_tag, tot_num_bytes);
        //final_tag_Sequence=util.combine_data(final_tag_Sequence,tag_Sequence );
        //	final_tag_Sequence=util.combine_data(final_tag_Sequence, num_bytes);

//		xy = util.combine_data(final_tag_Sequence,xy);

        //	System.out.println("4th_1 Step " + util.arrayToHex(xy));



        //	certrequest.write(xy);

        // Now close the streams.out_1, out_2, out_3, out_4
        try {
            out_1.close();
            out_2.close();
            out_3.close();
            out_4.close();

        } catch (Throwable th) {
        }


        return certrequest;




    }

    private byte[] calcDerLen(Integer len) {

        byte[] len_tag_small = {(byte) 0x81};
        byte[] len_tag_big = {(byte) 0x82};
        byte[] num_bytes_tag;

        String tot_hex_size;

        if (len <= 127) {
            tot_hex_size = Integer.toHexString(len);
            num_bytes_tag = util.hex1ToByteArray(tot_hex_size);

        } else if (len > 127 && len < 256) {
            tot_hex_size = Integer.toHexString(len);
            num_bytes_tag = util.combine_data(len_tag_small, util.hex1ToByteArray(tot_hex_size));

        } else {
            tot_hex_size = "0" + Integer.toHexString(len);
            num_bytes_tag = util.combine_data(len_tag_big, util.hex1ToByteArray(tot_hex_size));
        }


        return num_bytes_tag;
    }
    //-----BEGIN CERTIFICATE-----
	/* */
    //-----END CERTIFICATE-----
    /*Template display name: PIV-I Encryption
    Object identifier: 1.3.6.1.4.1.311.21.8.11101558.7210957.3753883.9214177.14656715.240.3258577.6401920
    Subject type: User
    OBJECT IDENTIFIER
    :                 certificateTemplate (1 3 6 1 4 1 311 21 7)
    694   49:               OCTET STRING
    :                 30 2F 06 27 2B 06 01 04 01 82 37 15 08 85 A5 CA
    :                 76 83 B8 8F 4D 81 E5 8F 1B 84 B2 B1 61 86 FE C9
    :                 4B 81 70 81 AB F9 23 87 94 B9 53 02 01 64 02 01
    :                 06
     */
    //Now we can save this CSR in PEM format in the file.
    // PEM format is base64 encoded form which is used when sending the CSR in
    // mail or upload to CA.

    /*public void toPEM(byte[] CSR){


    String type = "CERTIFICATE REQUEST";
    byte[] encoding = getEncoded();

    PemObject pemObject = new PemObject(type, encoding);

    StringWriter str = new StringWriter();
    PEMWriter pemWriter = new PEMWriter(str);
    try {
    pemWriter.writeObject(pemObject);
    } catch (IOException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
    }
    try {
    pemWriter.close();
    } catch (IOException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
    }
    try {
    str.close();
    } catch (IOException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
    }

    System.out.println("In Conversion method" + str);


    /*	String  fileName= "c:/csr/csr.pem";
    File file_pem = new File(fileName);

    if (file_pem.getParentFile() != null) {
    file_pem.getParentFile().mkdirs();
    }

    try {
    file_pem.createNewFile();
    } catch (IOException e1) {
    // TODO Auto-generated catch block
    e1.printStackTrace();
    }

    FileWriter fcsr = null;

    try {
    fcsr = new FileWriter(file_pem);
    } catch (IOException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
    }

    PEMWriter csr_pem = new PEMWriter(fcsr);

    try {
    csr_pem.writeObject(CSR);
    } catch (IOException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
    }
    try {
    csr_pem.close();
    } catch (IOException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
    } */
    //	}*/
    
    
    
    
}
