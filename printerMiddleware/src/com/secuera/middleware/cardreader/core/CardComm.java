/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardNotPresentException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;

/**
 *
 * @author admin
 */
public class CardComm {

    public Card card = null;
    public CardChannel channel = null;

    public boolean isCardpresent(String deviceName) throws MiddleWareException {
        boolean rtnval = false;

        // get list of available terminals
        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = null;
        try {
            terminals = factory.terminals().list();
        } catch (CardException e) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.NO_CARD_TERMINAL_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }

        int term_cnt = terminals.size();
        System.out.println("Terminal Count " + term_cnt);

        for (int i = 0; i < term_cnt; i++) {
            CardTerminal terminal = terminals.get(i);
            if (terminal.getName().equalsIgnoreCase(deviceName)) {
                try {
                    rtnval = terminal.isCardPresent();
                } catch (CardException ex) {
                    //Logger.getLogger(CardComm.class.getName()).log(Level.SEVERE, null,ex);
                    rtnval = false;
                }
                 break;
            }
        }
        return rtnval;
        /*
        // get the first terminal
        CardTerminal terminal = terminals.get(0);
        try {
        rtnval = terminal.isCardPresent();
        } catch (CardException ex) {
        Logger.getLogger(CardComm.class.getName()).log(Level.SEVERE, null,
        ex);
        rtnval = false;
        }
        return rtnval;
        
         */
    }

    // Start the communication with Smart Card
    public CardChannel startComm(String deviceName) throws MiddleWareException {
        int retValue = 0;

        // show the list of available terminals
        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminals = null;
        try {
            terminals = factory.terminals().list();
        } catch (CardException e) {
            StringBuilder sb = new StringBuilder();
            sb.append(ExceptionMessages.NO_CARD_TERMINAL_EXCEPTION);
            throw new MiddleWareException(sb.toString());
        }

        int term_cnt = terminals.size();
        System.out.println("Terminal Count " + term_cnt);

        for (int i = 0; i < term_cnt; i++) {

            // System.out.println("In terminal loop " + i);
            // get the first terminal
            CardTerminal terminal = terminals.get(i);
            // System.out.println("In terminal loop " + terminal.getName());
            // establish a connection with the card

            System.out.println("name " + terminal.getName() + '/' + deviceName);

            if (terminal.getName().equalsIgnoreCase(deviceName)) {
                System.out.println("name " + terminal.getName() + '/' + deviceName);
                try {
                    card = terminal.connect("T=1");
                    retValue = 1;
                } catch (CardNotPresentException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(ExceptionMessages.NO_CARD_EXCEPTION);
                    throw new MiddleWareException(sb.toString());
                } catch (CardException e) {
                    throw new MiddleWareException("Error communicating with card.");
                    //retValue = -3; // Some other card Exception
                    // System.out.println("Get Out Now " + retValue);

                }
                break;
            }

        }

        //get channel & populate public variable
        if (retValue == 1) {
            channel = card.getBasicChannel();
        } else {
            throw new MiddleWareException("unable to connect to terminal.");
        }

        // System.out.println("Final Return Value " + retValue);
        return channel;

    }

    //Disconnect Card to avoid exception when we try to start communication again.
    public void stopComm() {
        try {
            card.disconnect(false);
        } catch (CardException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to Disconnect Card.");
            //throw new MiddleWareException (sb.toString());	
        }

    }
}
