// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.ica;


public interface ICAConstants
{

    public static final int ICScannerPixelDataTypeBW = 0;
    public static final int ICScannerPixelDataTypeGray = 1;
    public static final int ICScannerPixelDataTypeRGB = 2;
    public static final int ICScannerPixelDataTypePalette = 3;
    public static final int ICScannerPixelDataTypeCMY = 4;
    public static final int ICScannerPixelDataTypeCMYK = 5;
    public static final int ICScannerPixelDataTypeYUV = 6;
    public static final int ICScannerPixelDataTypeYUVK = 7;
    public static final int ICScannerPixelDataTypeCIEXYZ = 8;
    public static final int ICScannerMeasurementUnitInches = 0;
    public static final int ICScannerMeasurementUnitCentimeters = 1;
    public static final int ICScannerMeasurementUnitPicas = 2;
    public static final int ICScannerMeasurementUnitPoints = 3;
    public static final int ICScannerMeasurementUnitTwips = 4;
    public static final int ICScannerMeasurementUnitPixels = 5;
    public static final int ICScannerFunctionalUnitTypeFlatbed = 0;
    public static final int ICScannerFunctionalUnitTypePositiveTransparency = 1;
    public static final int ICScannerFunctionalUnitTypeNegativeTransparency = 2;
    public static final int ICScannerFunctionalUnitTypeDocumentFeeder = 3;
    public static final int ICDeviceTypeCamera = 1;
    public static final int ICDeviceTypeScanner = 2;
}
