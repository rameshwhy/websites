/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

/**
 *
 * @author admin
 */
public class FacialInfo {
	 int imageBlockLength;
		int featurePointCount;
		String gender;
		String eyeColor;
		String hairColor;
		String featureMask;
		int expression;
		int poseAngles;
		int poseAngleUncertainty;

	    public int getExpression() {
	        return expression;
	    }

	    public void setExpression(int expression) {
	        this.expression = expression;
	    }

	    public String getEyeColor() {
	        return eyeColor;
	    }

	    public void setEyeColor(String eyeColor) {
	        this.eyeColor = eyeColor;
	    }

	    public String getFeatureMask() {
	        return featureMask;
	    }

	    public void setFeatureMask(String featureMask) {
	        this.featureMask = featureMask;
	    }

	    public int getFeaturePointCount() {
	        return featurePointCount;
	    }

	    public void setFeaturePointCount(int featurePointCount) {
	        this.featurePointCount = featurePointCount;
	    }

	    public String getGender() {
	        return gender;
	    }

	    public void setGender(String gender) {
	        this.gender = gender;
	    }

	    public String getHairColor() {
	        return hairColor;
	    }

	    public void setHairColor(String hairColor) {
	        this.hairColor = hairColor;
	    }

	    public int getImageBlockLength() {
	        return imageBlockLength;
	    }

	    public void setImageBlockLength(int imageBlockLength) {
	        this.imageBlockLength = imageBlockLength;
	    }

	    public int getPoseAngleUncertainty() {
	        return poseAngleUncertainty;
	    }

	    public void setPoseAngleUncertainty(int poseAngleUncertainty) {
	        this.poseAngleUncertainty = poseAngleUncertainty;
	    }

	    public int getPoseAngles() {
	        return poseAngles;
	    }

	    public void setPoseAngles(int poseAngles) {
	        this.poseAngles = poseAngles;
	    }
	        
	        
	    public boolean initWithByteString(String byteString)
	    {
	        boolean retval = false;

		//byteString = removeWhitespace(byteString);

		// 20 bytes of Facial Image Information:
		// XXX vet len
		if (byteString.length() >= (20 * 2))
		{
			// 4 bytes for Facial Image Block length:
			// XXX vet len
			this.setImageBlockLength(Integer.parseInt(byteString.substring(0, 8), 16));

			// 2 bytes for Number of Feature Points:
			// XXX vet len
			this.setFeaturePointCount(Integer.parseInt(byteString.substring(8, 12), 16));

			// 2 bytes for Gender
			// XXX vet len
			// XXX figure out encoding
			this.setGender(byteString.substring(12, 16));

			// 2 bytes for Eye Color
			// XXX vet len
			// XXX figure out encoding
			this.setEyeColor(byteString.substring(16, 20));

			// 2 bytes for Hair Color
			// XXX vet len
			// XXX figure out encoding
			this.setHairColor(byteString.substring(20, 24));

			// 2 bytes for Feature Mask
			// XXX vet len
			// XXX figure out encoding
			this.setFeatureMask(byteString.substring(24, 28));

			// 2 bytes for Expression
			// XXX vet len
			// XXX figure out encoding
			// XXX should always be 1 for "Neutral"
			this.setExpression(Integer.parseInt(byteString.substring(28, 32)));

			// 2 bytes for Pose Angles
			// XXX vet len
			// XXX figure out encoding
			// XXX should always be 0 for "Unspecified = Neutral"
			this.setPoseAngles(Integer.parseInt(byteString.substring(32, 36)));

			// 2 bytes for Pose Angle Uncertainty
			// XXX vet len
			// XXX figure out encoding
			this.setPoseAngleUncertainty(Integer.parseInt(byteString.substring(36, 40)));

			retval = true;
		}

		return retval;
	    }
}
