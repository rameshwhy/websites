/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import javax.smartcardio.CardChannel;

/**
 *
 * @author com_user
 */
public class GenCertificate {

    public byte[] genCert(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY,char cert_type,
            byte[] key_type,  String str_cn, String str_ou,
            String str_org, String str_city, String str_state,
            String str_country,String str_email,int[] cert_template_oid,int majorVer,int minorVer,
            String caType,String inURL) throws Exception {

        String certCSR;
        byte[] certficate=null;
        //generate CSR
        GenCSR generateCSR = new GenCSR();
        certCSR = generateCSR.getCSR(channel,PRE_PERSO_PIV_ADMIN_KEY,PIV_ADMIN_KEY, cert_type, key_type,  str_cn, str_ou, str_org,
                str_city, str_state, str_country,str_email, cert_template_oid,majorVer, minorVer);

        
        
        //get microsoft certificate    
        if ("MS".equals(caType)) {
            certficate = getMScertificate(certCSR,inURL);
        }
        
        CommonUtil util = new CommonUtil();
        System.out.println(util.arrayToHex(certficate)); 
        return certficate;
        
        //load certificate on card
        //LoadCertificate loadCertificate = new LoadCertificate();
        //loadCertificate.loadCert(channel,PRE_PERSO_PIV_ADMIN_KEY,PIV_ADMIN_KEY,certficate,cert_type);
        
  

    }

    private byte[] getMScertificate(String certCSR,String inURL) {
  
        byte[] certficate= null;
        //connect to microsoft 
        try {
            MicrosoftCertServer msCASrv = new MicrosoftCertServer();


            // Download the new certificate
            String approvalID = msCASrv.HttpGetApprovalID(certCSR,inURL+"certfnsh.asp");
            // Create the Certificate in .der format and get it in .pem format
            certficate = msCASrv.HttpGetUserCert(approvalID,inURL);


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return certficate;

    }
    
    
    

    
    
}
