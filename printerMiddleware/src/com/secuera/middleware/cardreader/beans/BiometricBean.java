package com.secuera.middleware.cardreader.beans;

import java.awt.image.BufferedImage;

import com.secuera.middleware.biometrics.BiometricError;

/*FPStatus  
 *      Complete >> get value of fingerPrint & fingerImage
 *      Success  >> image read complete try next capture   
 * 
 */
public class BiometricBean {
    
    private byte[] fingerPrint;
    private BufferedImage fingerImage;
    private String FPStatus;    
   
    private BiometricError bioError;

    public byte[] getFingerPrint() {
        return fingerPrint;
    }

    public void setFingerPrint(byte[] fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public BufferedImage getFingerImage() {
        return fingerImage;
    }

    public void setFingerImage(BufferedImage fingerImage) {
        this.fingerImage = fingerImage;
    }

   
    public BiometricError getBioError() {
        return bioError;
    }

    public void setBioError(BiometricError bioError) {
        this.bioError = bioError;
    }

    public String getFPStatus() {
        return FPStatus;
    }

    public void setFPStatus(String FPStatus) {
        this.FPStatus = FPStatus;
    }
   
    @Override
    public String toString(){
        return "FingerImage [" + fingerImage + "] " + " FPStatus [" + FPStatus+ " ]  BioError Code [" + bioError.getErrorCode() + " ]  Bio Error Message [ " + bioError.getErrorMsg() + " ]  FingerPrint [" + fingerPrint+ "]" ;
    }
}
