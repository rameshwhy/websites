package com.secuera.middleware.beans.piv;

public class FascnBean {

    private String agencyCode;				//Bytes = 4
    private String systemCode;				//Bytes = 4
    private String credentialNumber;			//Bytes = 6
    private String cs;					//Bytes = 1 (always set to 1)
    private String ici;					//Bytes = 1 (always set to 1)
    private String person;				//Bytes = 16
    /*
    private String organisationCategory;		//Bytes = 1
    private String organisationaIdentifier; 		//Bytes = 4
    private String personOrganizationAssociation; 	//Bytes = 1
    */ 
    

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getCredentialNumber() {
        return credentialNumber;
    }

    public void setCredentialNumber(String credentialNumber) {
        this.credentialNumber = credentialNumber;
    }

    public String getCs() {
        return cs;
    }

    public void setCs(String cs) {
        this.cs = cs;
    }

    public String getIci() {
        return ici;
    }

    public void setIci(String ici) {
        this.ici = ici;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

   
   
    
}
