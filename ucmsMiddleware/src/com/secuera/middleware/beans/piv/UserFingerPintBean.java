/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import com.secuera.middleware.core.common.CommonUtil;
import java.io.Serializable;

/**
 *
 * @author Satish K
 */
public class UserFingerPintBean implements Serializable {
    private String rightThumb;
    private String rightIndex;
    private String rightMiddle;
    private String rightRing;
    private String rightLittle;
    private String leftThumb;
    private String leftIndex;
    private String leftMiddle;
    private String leftRing;
    private String leftLittle;

    public String getLeftIndex() {
        return leftIndex;
    }

    public void setLeftIndex(String leftIndex) {
        this.leftIndex = leftIndex;
    }

    public String getLeftLittle() {
        return leftLittle;
    }

    public void setLeftLittle(String leftLittle) {
        this.leftLittle = leftLittle;
    }

    public String getLeftMiddle() {
        return leftMiddle;
    }

    public void setLeftMiddle(String leftMiddle) {
        this.leftMiddle = leftMiddle;
    }

    public String getLeftRing() {
        return leftRing;
    }

    public void setLeftRing(String leftRing) {
        this.leftRing = leftRing;
    }

    public String getLeftThumb() {
        return leftThumb;
    }

    public void setLeftThumb(String leftThumb) {
        this.leftThumb = leftThumb;
    }

    public String getRightIndex() {
        return rightIndex;
    }

    public void setRightIndex(String rightIndex) {
        this.rightIndex = rightIndex;
    }

    public String getRightLittle() {
        return rightLittle;
    }

    public void setRightLittle(String rightLittle) {
        this.rightLittle = rightLittle;
    }

    public String getRightMiddle() {
        return rightMiddle;
    }

    public void setRightMiddle(String rightMiddle) {
        this.rightMiddle = rightMiddle;
    }

    public String getRightRing() {
        return rightRing;
    }

    public void setRightRing(String rightRing) {
        this.rightRing = rightRing;
    }

    public String getRightThumb() {
        return rightThumb;
    }

    public void setRightThumb(String rightThumb) {
        this.rightThumb = rightThumb;
    }

  
    
}
