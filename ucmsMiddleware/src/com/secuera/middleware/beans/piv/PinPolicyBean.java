/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

/**
 *
 * @author Harpreet Singh
 */
public class PinPolicyBean {
    
    private int maxLocalPinLength;
    private int minLocalPinLength;
    private String localPinType;
    
    private int maxLocalPUKLength;
    private int minLocalPUKLength;
    private String localPUKType;
    
    private int maxGlobalPinLength;
    private int minGlobalPinLength;
    private String globalPinType;
    
    private int maxGlobalPUKLength;
    private int minGlobalPUKLength;
    private String globalPUKType;

    public String getGlobalPUKType() {
        return globalPUKType;
    }

    public void setGlobalPUKType(String globalPUKType) {
        this.globalPUKType = globalPUKType;
    }

    public String getGlobalPinType() {
        return globalPinType;
    }

    public void setGlobalPinType(String globalPinType) {
        this.globalPinType = globalPinType;
    }

    public String getLocalPUKType() {
        return localPUKType;
    }

    public void setLocalPUKType(String localPUKType) {
        this.localPUKType = localPUKType;
    }

    public String getLocalPinType() {
        return localPinType;
    }

    public void setLocalPinType(String localPinType) {
        this.localPinType = localPinType;
    }

    public int getMaxGlobalPUKLength() {
        return maxGlobalPUKLength;
    }

    public void setMaxGlobalPUKLength(int maxGlobalPUKLength) {
        this.maxGlobalPUKLength = maxGlobalPUKLength;
    }

    public int getMaxGlobalPinLength() {
        return maxGlobalPinLength;
    }

    public void setMaxGlobalPinLength(int maxGlobalPinLength) {
        this.maxGlobalPinLength = maxGlobalPinLength;
    }

    public int getMaxLocalPUKLength() {
        return maxLocalPUKLength;
    }

    public void setMaxLocalPUKLength(int maxLocalPUKLength) {
        this.maxLocalPUKLength = maxLocalPUKLength;
    }

    public int getMaxLocalPinLength() {
        return maxLocalPinLength;
    }

    public void setMaxLocalPinLength(int maxLocalPinLength) {
        this.maxLocalPinLength = maxLocalPinLength;
    }

    public int getMinGlobalPUKLength() {
        return minGlobalPUKLength;
    }

    public void setMinGlobalPUKLength(int minGlobalPUKLength) {
        this.minGlobalPUKLength = minGlobalPUKLength;
    }

    public int getMinGlobalPinLength() {
        return minGlobalPinLength;
    }

    public void setMinGlobalPinLength(int minGlobalPinLength) {
        this.minGlobalPinLength = minGlobalPinLength;
    }

    public int getMinLocalPUKLength() {
        return minLocalPUKLength;
    }

    public void setMinLocalPUKLength(int minLocalPUKLength) {
        this.minLocalPUKLength = minLocalPUKLength;
    }

    public int getMinLocalPinLength() {
        return minLocalPinLength;
    }

    public void setMinLocalPinLength(int minLocalPinLength) {
        this.minLocalPinLength = minLocalPinLength;
    }

   
}
