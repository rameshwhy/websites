/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.entities.MiddleWareException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author admin
 */
public class DesEncrypter {

    Cipher ecipher;
    Cipher dcipher;
    private String transformation = "DESede/ECB/NoPadding";

    /**
     * Default Constructor
     */
    public DesEncrypter() {
        super();
        System.out.println(">>>>>>>> Called Constructor");

    }

    // private String transformation = "DES/CBC/NoPadding";
    // byte [] key_p={0x10,0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
    // 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23,
    // 0x24, 0x25, 0x26, 0x27 };
    // private static byte[] key_p = {0x70,0x71,0x72,0x73,0x74,0x75,0x76,
    // 0x77, 0x78, 0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F } ;
    public DesEncrypter(byte[] key_p) throws MiddleWareException {
        try {

            SecretKeySpec myKey = new SecretKeySpec(key_p, "DESede");
            // SecretKeySpec myKey = new SecretKeySpec(key_p, "DES");
            ecipher = Cipher.getInstance(transformation);
            dcipher = Cipher.getInstance(transformation);

            ecipher.init(Cipher.ENCRYPT_MODE, myKey);
            dcipher.init(Cipher.DECRYPT_MODE, myKey);

        } catch (javax.crypto.NoSuchPaddingException e) {
           throw new MiddleWareException(e.getMessage());
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (java.security.InvalidKeyException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    public byte[] encrypt(byte[] encr) throws MiddleWareException {

        try {

            // Encrypt
            byte[] enc = ecipher.doFinal(encr);

            // Encode bytes to base64 to get a string
            // return new sun.misc.BASE64Encoder().encode(enc);
            return enc;
        } catch (javax.crypto.BadPaddingException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (IllegalBlockSizeException e) {
            throw new MiddleWareException(e.getMessage());
        }        
    }

    public byte[] decrypt(byte[] dcr) throws MiddleWareException {
        try {
            // Decode base64 to get bytes
            // byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

            // Decrypt
            byte[] dcry = dcipher.doFinal(dcr);

            // Decode using utf-8
            // return new String(utf8, "UTF8");

            return dcry;

        } catch (javax.crypto.BadPaddingException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (IllegalBlockSizeException e) {
            throw new MiddleWareException(e.getMessage());
        }       
    }
}
