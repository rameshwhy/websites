/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 *
 * @author Satish K
 */
public class ContentCertificateBean {
    private PublicKey publicKey;
    private PrivateKey privateKey;
    private String strPrivatekeyHex;
    private String strPublickeyHex;
    
    private byte[] contentCertificate;
    private String strContentCertificateHex;

    
    public byte[] getContentCertificate() {
        return contentCertificate;
    }

    public void setContentCertificate(byte[] contentCertificate) {
        this.contentCertificate = contentCertificate;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public String getStrContentCertificateHex() {
        return strContentCertificateHex;
    }

    public void setStrContentCertificateHex(String strContentCertificateHex) {
        this.strContentCertificateHex = strContentCertificateHex;
    }

    public String getStrPrivatekeyHex() {
        return strPrivatekeyHex;
    }

    public void setStrPrivatekeyHex(String strPrivatekeyHex) {
        this.strPrivatekeyHex = strPrivatekeyHex;
    }

    public String getStrPublickeyHex() {
        return strPublickeyHex;
    }

    public void setStrPublickeyHex(String strPublickeyHex) {
        this.strPublickeyHex = strPublickeyHex;
    }
   
    
}
