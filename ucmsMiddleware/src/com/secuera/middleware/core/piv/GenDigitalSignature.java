package com.secuera.middleware.core.piv;

import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.core.common.CommonUtil;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;

public class GenDigitalSignature {

    CommonUtil util = new CommonUtil();

    public GenDigitalSignature() {
        super();
    }

    public byte[] GenSignature(byte[] input_data, PrivateKey privatekey, String algoType)
            throws MiddleWareException {

        byte[] signature = null;
        String algo = null;

     
        Signature sig = null;
        try {
            if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC224)) {
                algo = "SHA224withECDSA";
                sig = Signature.getInstance(algo, "SunEC");

            } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC256)) {
                algo = "SHA256withECDSA";
                sig = Signature.getInstance(algo, "SunEC");
            } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC384)) {
                algo = "SHA384withECDSA";
                sig = Signature.getInstance(algo, "SunEC");

            } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.RSA1024)) {
                algo = "SHA256withRSA";
                sig = Signature.getInstance(algo);

            } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
                algo = "SHA512withRSA";
                sig = Signature.getInstance(algo);
            }

            sig.initSign(privatekey);
            sig.update(input_data);
            signature = sig.sign();

        } catch (NoSuchAlgorithmException ex) {
            throw new MiddleWareException(ex.getMessage());
        }catch (NoSuchProviderException ex1) {
            throw new MiddleWareException(ex1.getMessage());
        }catch (InvalidKeyException ex2) {
            throw new MiddleWareException(ex2.getMessage());
        }catch (SignatureException ex3) {
            throw new MiddleWareException(ex3.getMessage());
        }

        //System.out.println("Signature : " +  arrayToHex(signature) );

        return signature;

    }
}
