package com.secuera.middleware.cardreader.core;


import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.plugins.jpeg.JPEGImageReadParam;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.spi.ImageReaderSpi;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

//import com.id3.fingerprint.FingerCapture;
//import com.id3.fingerprint.FingerCaptureListener;
//import com.id3.fingerprint.FingerCaptureStatus;
//import com.id3.fingerprint.FingerEnroll;
//import com.id3.fingerprint.FingerException;
//import com.id3.fingerprint.FingerImage;
//import com.id3.fingerprint.FingerLicense;
//import com.id3.fingerprint.FingerMatch;
//import com.id3.fingerprint.FingerScanner;
//import com.id3.fingerprint.FingerScannerInfo;
//import com.id3.fingerprint.FingerTemplate;
//import com.id3.fingerprint.FingerTemplateRecord;
import com.sun.imageio.plugins.jpeg.JPEGImageReaderSpi;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.sun.media.imageio.plugins.jpeg2000.J2KImageReadParam;
//import com.sun.media.imageioimpl.plugins.jpeg2000.*;

//import jj2000.j2k.codestream.reader.*;



//FingerCaptureListener,
//
public  class BioCapture implements  ActionListener  {

	private static final boolean seekForwardOnly = false;
	//FingerLicense license = null;
	//FingerCapture _capture = null;
	int intScannerCount = 0;
	int intLicenceStatus = 0;
	//FingerImage currentFingerImage = null;
	//FingerTemplate currentTemplate = null;
	Image currentImage = null;
	//FingerTemplateRecord currentReference = null;
	//FingerEnroll _enroll = null;
	boolean boolCaptureStatus = false;
	boolean boolMatch = false;
	boolean boolCapture = false;
	boolean boolEnroll = false;
	//FingerMatch matcher = null;
	byte [] facial_read;

	// JButton buttonCapture = new JButton("Capture");
	// JPanel panelScanner = new JPanel();
	static Button Submit = new Button("Submit");
	JButton button_enroll = new JButton("Enroll");
	JButton button_readimg = new JButton("Read Image");
	
	Panel p = new Panel();
	Panel p1 = new Panel();

	public BioCapture() {
		/*try {
			license = new FingerLicense();
			intLicenceStatus = 1;
			_capture = new FingerCapture((FingerCaptureListener) this);
		} catch (FingerException e) {
			e.printStackTrace();
		} */

		// System.loadLibrary("sgfpamx");
		// System.loadsnerInfo info=new FingerScannerInfo();
		// FingerScannerModel model = null ;
		// info.setModel(model.Secugen);
		openForm();
		//setPanelScanner();
		System.out.println(" Bio Capture instance loaded");
	}

/*	public void dispose() {
		try {
			_capture.dispose();
			license.getMaxUserCo
                                } catch (Exception e) {
		}
                
	} */
        

/*	public void start() {
		try {
			System.out.println("Start Capture");
	//		_capture.start();

		} catch (Exception e) {
		}
	}

	/**
	 * Scanner added event
	 */
	/*public void scannerAdded(FingerCapture sender, FingerScanner scanner) {
		setPanelScanner();
	}*/

	/**
	 * Scanner removed event
	 */
/*	public void scannerRemoved(FingerCapture sender, String scannerName) {
		setPanelScanner();
	}

	/**
	 * Finger capture status changed event
	 */
/*	public void statusChanged(FingerCapture sender, FingerCaptureStatus status,
			FingerImage image) {
		System.out.println("Status Changed");
		// MAIN PROCESSES
                int test = FingerCaptureStatus.FingerPlaced;
	/*if (1==1) {
			currentFingerImage = image;

			System.out.println("Image_capture_1");
			if (image==null)
			{
				System.out.println("Image_capture_Null");
			}
			System.out.println("Image_capture_1");

			// CAPTURE AND MATCH
			if (boolEnroll != true) {

				try {
					currentImage = currentFingerImage.toImage();
					currentTemplate = currentFingerImage.createTemplate();

					System.out.println("Image_capture_2 :" + currentTemplate);
					
					byte[] res = currentTemplate.toIso19794CompactCard(currentTemplate.getMinutiaCount());

					CommonUtil util= new CommonUtil();
					PivUtil piv = new PivUtil();
					CardComm CardConnect = new CardComm();
					String str = util.arrayToHex(res);

					System.out.println("Image_capture_3 :" + str);

					CardConnect.startComm();
					piv.verifyFingerPrint(CardConnect.channel,res , "0E" );
					CardConnect.stopComm();

					
				} catch (FingerException e) {
					e.printStackTrace();
				}
			} else {
				// ENROLL
				System.out.println("Enroll_capture_1");
				try {
					currentImage = currentFingerImage.toImage();
					
					System.out.println("Enroll_capture_2");
					
					currentTemplate = _enroll.addImage(image);
					
					System.out.println("Enroll_capture_3");
					
					if (_enroll.getQuality() < 100) {
						_capture.start();
					} else {

						//currentTemplate = ((FingerImage) currentTemplate).createTemplate();
						byte[] res = currentTemplate.toIso19794CompactCard(currentTemplate.getMinutiaCount());

						CommonUtil util= new CommonUtil();
						PivUtil piv = new PivUtil();
						CardComm CardConnect = new CardComm();
						
						String str = util.arrayToHex(res);

						System.out.println("Enroll_Image_capture_3 :" + str);

						CardConnect.startComm();

						piv.updateFingerPrint(CardConnect.channel,res, "0E");
						CardConnect.stopComm();

					}
				} catch (FingerException e) {
					e.printStackTrace();
				}
				// menuItemSaveImage.setEnabled(true);
				// menuItemSaveTemplate.setEnabled(true);
			}
		}
		// BOUTONS
		if (status == FingerCaptureStatus.FingerRemoved
				|| status == FingerCaptureStatus.Stopped) {
			// buttonCapture.setText("Capture");
			// buttonMatch.setText("Verify");
			// buttonEnroll.setText("Enroll");
			// buttonCapture.setEnabled(true);
			// buttonEnroll.setEnabled(true);
			boolCaptureStatus = false;
		} else if (status == FingerCaptureStatus.WaitingForFinger
				&& boolCapture == true) {
			// buttonCapture.setText("Cancel");
			// buttonEnroll.setEnabled(false);
			// buttonEnroll.repaint();
			// buttonMatch.setEnabled(false);
			boolCaptureStatus = true;
		} else if (status == FingerCaptureStatus.WaitingForFinger
				&& boolMatch == true) {
			// buttonMatch.setText("Cancel");
			// buttonCapture.setEnabled(false);
			// buttonEnroll.setEnabled(false);
			boolCaptureStatus = true;
		} else if (status == FingerCaptureStatus.WaitingForFinger
				&& boolEnroll == true) {
			// buttonEnroll.setText("Cancel");
			// buttonCapture.setEnabled(false);
			// buttonMatch.setEnabled(false);
			boolCaptureStatus = true;
		}

		// PANELINFOFP
		if (status == FingerCaptureStatus.ImageCaptured) {
			// setPanelInfoFP(image);
		} else if (image != null && status != FingerCaptureStatus.FingerRemoved) {
			// setPanelInfoFPMessage(status.toString());
		}

		// MENUS
		if (status == FingerCaptureStatus.ImageCaptured) {
			// menuItemSaveImage.setEnabled(true);
			// menuItemSaveTemplate.setEnabled(true);
		}
*/
		//System.out.println(status.toString());
//	}

	/*private void setPanelScanner() {

	/*	String stringLicencePath = new String();
		List<FingerScanner> scannerList = null;
		try {
			stringLicencePath = license.getLicensePath();
			intScannerCount = _capture.getScannerCount();
			scannerList = _capture.getScannerList();
		} catch (FingerException e) {
			e.printStackTrace();
		}

		System.out.println("Scanner List  " + scannerList.toString());
		System.out.println("Scanner Count  " + intScannerCount);

		if (intLicenceStatus == 1) {

			for (int i = 0; i < intScannerCount; i++) {
				// gbc.gridy = GridBagConstraints.RELATIVE;
				FingerScanner scanner = scannerList.get(i);
				FingerScannerInfo scannerInfo;
				try {
					scannerInfo = scanner.getScannerInfo();
					// panelScanner.add(new
					// JLabel("- "+scannerInfo.getName()),gbc);
				} catch (FingerException e) {
					e.printStackTrace();
				}
			}

		}
*/
//	}

/*	public void actionPerformed(ActionEvent e) {
		// CAPTURE
	/*	if (e.getSource() == Submit) {
			try {
				if (boolCaptureStatus == false) {
					boolMatch = true;
					boolEnroll = false;
					boolCapture = true;
					_capture.start();
				} else {
					_capture.stop();
				}
			} catch (FingerException e1) {
				e1.printStackTrace();
			}
		}
		if (e.getSource() == button_enroll) {
			try {
				if (boolCaptureStatus == false) {
					boolMatch = false;
					boolEnroll = true;
					boolCapture = true;
					_enroll = new FingerEnroll();
					_capture.start();
				} else {
					_capture.stop();
				}
			} catch (FingerException e1) {
				e1.printStackTrace();
			}
		}
		
		/*if (e.getSource() == button_readimg)
		{
			
			readFacialinfo();
				
		}
*/
//	}

	
	
	/*private void readFacialinfo() {
		// TODO Auto-generated method stub

		CommonUtil util= new CommonUtil();
		CardComm CardConnect = new CardComm();
		PivUtil piv=new PivUtil();
		
		CardConnect.startComm();

		String lpdata="123456";
		piv.verifyLocalPin(CardConnect.channel,lpdata);
		
		System.out.println("Local Pin Verified");
		
		facial_read=piv.readFacialinfo(CardConnect.channel);
		CardConnect.stopComm();
		
		String str = util.arrayToHex(facial_read);
	
		System.out.println("Facial Image data from PIV : " + str);
		
		 //img = null;
		BufferedImage img = null;
		
		try {
		
			 //img = ImageIO.read((ImageInputStream) new ByteArrayInputStream(facial_read));
			// ImageReaderSpi ImgSpi = new ImageReaderSpi(); 

			
			J2KImageReader reader = new J2KImageReader(null); 
			 
			 
			 reader = (J2KImageReader) new J2KImageReaderSpi().createReaderInstance();

			 
			 ImageInputStream inputStream = ImageIO.createImageInputStream(new ByteArrayInputStream(facial_read));

			 J2KImageReadParam param = new J2KImageReadParam();
			 
			 System.out.println("TEST_1 : ");
			 
			 reader.setInput(inputStream);
			 
			 System.out.println("TEST_2 : ");
			 
			 param.setSourceSubsampling(32, 32, 0, 0);
			 param.setSourceRegion(new Rectangle(0,0,2048,2048));

			 RenderedImageSrc image = (RenderedImageSrc) reader.readAsRenderedImage(0, param);

			 img = reader.read(0, param);

			// reader.setInput(facial_read, seekForwardOnly);
			 //int imageIndex=0;
			 
			 //img=reader.read(reader.getMinIndex());
			 
			 System.out.println("TEST_3 : ");
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		 ImageIcon icon = new ImageIcon(img);
	     JLabel label = new JLabel(icon, JLabel.CENTER);

		 
		 p.add(label);
	}
*/


	
	
	public void openForm() {
		Frame frm = new Frame("DataEntry frame");

                Panel p = new Panel();
        	Panel p1 = new Panel();

		// Label lbl = new Label("Please fill this blank:");
		// frm.add(lbl);

		frm.setSize(500, 500);
		frm.setVisible(true);
		frm.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		// Label jFirstName = new Label("First Name");
		// TextField lFirstName = new TextField(20);
		// Label jLastName =new Label("Last Name");
		// TextField lLastName=new TextField(20);
		//JLabel jImg = new JLabel("Image");
		p.setLayout(new GridLayout(3, 1));
		
		p.add(Submit);
		p.add(button_enroll);
		p.add(button_readimg);
		//p.add(jImg);
		p1.add(p);

		frm.add(p1, BorderLayout.NORTH);

		Submit.addActionListener(this);
		//button_enroll.addActionListener(this);
		//button_readimg.addActionListener(this);
		
		frm.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				System.out
						.println("Dispose FingerCapture and FingerLicense objects before closing.");
				//dispose();
			}
		});

	} 

    public void actionPerformed(ActionEvent e) {
        
        
        
         System.out.println("Harpreet Test.");
        
      MiddleWareServerCardreader mws = new MiddleWareServerCardreaderImpl();
            try {
                mws.readFingerPrintID3(1, 50,"AuthenTec TCS1C",1);
            } catch (MiddleWareException ex) {
                Logger.getLogger(BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            }
           
   

    
    }

}
