/*     */ package com.id3.fingerprint;
/*     */ 
import java.io.File;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ public class FingerTemplateRecord
/*     */ {
/*  18 */   private long _handle = 0L;
/*  19 */   private boolean _disposed = false;
/*  20 */   private boolean _disposable = true;
/*  21 */   private FingerTemplate[] _templateArray = null;
/*  22 */   private List<FingerTemplate> _templates = new ArrayList();
/*     */ 
/*     */   static
/*     */   {
/* 367 */     String tmpStr = "id3Finger.dll";
                File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
                File localFile = new File(TMP_DIR, tmpStr);


                 System.load(localFile.getAbsolutePath());
/*     */   }
/*     */ 
/*     */   public FingerTemplateRecord()
/*     */     throws FingerException
/*     */   {
/*  30 */     long[] handle_ptr = new long[1];
/*     */ 
/*  32 */     int result = nInitialize(handle_ptr);
/*     */ 
/*  34 */     if (result != 0) {
/*  35 */       throw new FingerException(result);
/*     */     }
/*  37 */     this._handle = handle_ptr[0];
/*     */   }
/*     */ 
/*     */   public FingerTemplateRecord(FingerTemplate template)
/*     */     throws FingerException
/*     */   {
/*  47 */     long[] handle_ptr = new long[1];
/*  48 */     int result = nInitialize(handle_ptr);
/*     */ 
/*  50 */     if (result == 0)
/*     */     {
/*  52 */       this._handle = handle_ptr[0];
/*  53 */       add(template);
/*     */     }
/*     */ 
/*  56 */     if (result != 0)
/*  57 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public FingerTemplateRecord(long handle, boolean disposable)
/*     */     throws FingerException
/*     */   {
/*  67 */     this._handle = handle;
/*  68 */     this._disposable = disposable;
/*     */ 
/*  70 */     int count = getTemplateCount();
/*     */ 
/*  72 */     for (int i = 0; i < count; i++)
/*     */     {
/*  74 */       long[] hTemplate = new long[1];
/*     */ 
/*  76 */       int result = nGetTemplate(this._handle, i, hTemplate);
/*     */ 
/*  78 */       if (result != 0) {
/*  79 */         throw new FingerException(result);
/*     */       }
/*  81 */       this._templates.add(new FingerTemplate(hTemplate[0], disposable));
/*     */     }
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/*  90 */     nDispose(this._handle, 0);
/*  91 */     this._handle = 0L;
/*  92 */     this._disposed = true;
/*     */   }
/*     */ 
/*     */   protected void finalize()
/*     */     throws Throwable
/*     */   {
/*     */     try
/*     */     {
/* 102 */       this._templateArray = null;
/* 103 */       this._templates.clear();
/*     */ 
/* 105 */       if (this._disposable)
/* 106 */         dispose();
/*     */     }
/*     */     catch (Exception localException)
/*     */     {
/*     */     }
/*     */     finally
/*     */     {
/* 113 */       super.finalize();
/*     */     }
/*     */   }
/*     */ 
/*     */   public long getHandle()
/*     */   {
/* 123 */     return this._handle;
/*     */   }
/*     */ 
/*     */   public FingerTemplateRecord clone()
/*     */   {
/* 132 */     long[] hClone = new long[1];
/* 133 */     int result = nClone(this._handle, hClone);
/*     */ 
/* 135 */     if (result == 0)
/*     */     {
/*     */       try
/*     */       {
/* 139 */         return new FingerTemplateRecord(hClone[0], true);
/*     */       }
/*     */       catch (FingerException localFingerException)
/*     */       {
/*     */       }
/*     */     }
/* 145 */     return null;
/*     */   }
/*     */ 
/*     */   public static FingerTemplateRecord fromFile(String filename, int format)
/*     */     throws FingerException
/*     */   {
/* 157 */     long[] handle = new long[1];
/* 158 */     int result = nFromFile(filename, format, handle);
/*     */ 
/* 160 */     if (result != 0) {
/* 161 */       throw new FingerException(result);
/*     */     }
/* 163 */     return new FingerTemplateRecord(handle[0], true);
/*     */   }
/*     */ 
/*     */   public static FingerTemplateRecord fromBuffer(byte[] data, int format)
/*     */     throws FingerException
/*     */   {
/* 175 */     long[] handle = new long[1];
/* 176 */     int result = nFromBuffer(data, format, handle);
/*     */ 
/* 178 */     if (result != 0) {
/* 179 */       throw new FingerException(result);
/*     */     }
/* 181 */     return new FingerTemplateRecord(handle[0], true);
/*     */   }
/*     */ 
/*     */   public void save(String filename, int format)
/*     */     throws FingerException
/*     */   {
/* 192 */     int result = nSave(this._handle, format, filename);
/*     */ 
/* 194 */     if (result != 0)
/* 195 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public byte[] toBuffer(int format)
/*     */     throws FingerException
/*     */   {
/* 206 */     byte[] data = (byte[])null;
/* 207 */     long[] dataSize = new long[1];
/*     */ 
/* 211 */     int result = nToBuffer(this._handle, format, data, dataSize);
/*     */ 
/* 213 */     if (result == -13)
/*     */     {
/* 215 */       data = new byte[(int)dataSize[0]];
/*     */ 
/* 217 */       result = nToBuffer(this._handle, format, data, dataSize);
/*     */     }
/*     */ 
/* 220 */     if (result != 0) {
/* 221 */       throw new FingerException(result);
/*     */     }
/* 223 */     return data;
/*     */   }
/*     */ 
/*     */   public void add(FingerTemplate template)
/*     */     throws FingerException
/*     */   {
/* 233 */     int result = nAddTemplate(this._handle, template.getHandle());
/*     */ 
/* 235 */     if (result != 0) {
/* 236 */       throw new FingerException(result);
/*     */     }
/*     */ 
/* 239 */     this._templates.add(template);
/*     */ 
/* 242 */     this._templateArray = null;
/*     */   }
/*     */ 
/*     */   public void add(FingerTemplateRecord templateRecord)
/*     */     throws FingerException
/*     */   {
/* 252 */     int result = nAddTemplateRecord(this._handle, templateRecord.getHandle());
/*     */ 
/* 254 */     if (result != 0) {
/* 255 */       throw new FingerException(result);
/*     */     }
/*     */ 
/* 258 */     for (int i = 0; i < templateRecord.getTemplateCount(); i++)
/*     */     {
/* 260 */       FingerTemplate template = templateRecord.getTemplates()[i];
/* 261 */       this._templates.add(template);
/*     */     }
/*     */ 
/* 265 */     this._templateArray = null;
/*     */   }
/*     */ 
/*     */   public void remove(FingerTemplate template)
/*     */     throws FingerException
/*     */   {
/* 275 */     int result = nRemoveTemplate(this._handle, template.getHandle());
/*     */ 
/* 277 */     if (result != 0) {
/* 278 */       throw new FingerException(result);
/*     */     }
/* 280 */     this._templates.remove(template);
/*     */ 
/* 283 */     this._templateArray = null;
/*     */   }
/*     */ 
/*     */   public void remove(int index)
/*     */     throws FingerException
/*     */   {
/* 293 */     int result = nRemoveTemplateAt(this._handle, index);
/*     */ 
/* 295 */     if (result != 0) {
/* 296 */       throw new FingerException(result);
/*     */     }
/* 298 */     this._templates.remove(index);
/*     */ 
/* 301 */     this._templateArray = null;
/*     */   }
/*     */ 
/*     */   public boolean equals(Object obj)
/*     */   {
/* 311 */     if ((obj == null) || (obj.getClass() != getClass())) {
/* 312 */       return false;
/*     */     }
/* 314 */     FingerTemplateRecord record = (FingerTemplateRecord)obj;
/* 315 */     return this._handle == record._handle;
/*     */   }
/*     */ 
/*     */   public int hashCode()
/*     */   {
/* 324 */     return (int)this._handle;
/*     */   }
/*     */ 
/*     */   public int getFingerCount()
/*     */   {
/* 333 */     return nGetFingerCount(this._handle);
/*     */   }
/*     */ 
/*     */   public int getTemplateCount()
/*     */     throws FingerException
/*     */   {
/* 343 */     int[] count = new int[1];
/* 344 */     int result = nGetTemplateCount(this._handle, count);
/*     */ 
/* 346 */     if (result != 0) {
/* 347 */       throw new FingerException(result);
/*     */     }
/* 349 */     return count[0];
/*     */   }
/*     */ 
/*     */   public FingerTemplate[] getTemplates()
/*     */   {
/* 358 */     if (this._templateArray == null)
/*     */     {
/* 360 */       this._templateArray = new FingerTemplate[this._templates.size()];
/* 361 */       this._templates.toArray(this._templateArray);
/*     */     }
/*     */ 
/* 364 */     return this._templateArray;
/*     */   }
/*     */ 
/*     */   private static final native int nInitialize(long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nDispose(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nClone(long paramLong, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nFromFile(String paramString, int paramInt, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nFromBuffer(byte[] paramArrayOfByte, int paramInt, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nToBuffer(long paramLong, int paramInt, byte[] paramArrayOfByte, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nSave(long paramLong, int paramInt, String paramString);
/*     */ 
/*     */   private static final native int nGetFingerCount(long paramLong);
/*     */ 
/*     */   private static final native int nGetTemplateCount(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nGetTemplate(long paramLong, int paramInt, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nAddTemplate(long paramLong1, long paramLong2);
/*     */ 
/*     */   private static final native int nAddTemplateRecord(long paramLong1, long paramLong2);
/*     */ 
/*     */   private static final native int nRemoveTemplate(long paramLong1, long paramLong2);
/*     */ 
/*     */   private static final native int nRemoveTemplateAt(long paramLong, int paramInt);
/*     */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerTemplateRecord
 * JD-Core Version:    0.6.2
 */