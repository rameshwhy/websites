/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.CredentialKeys;
import com.secuera.middleware.cardreader.beans.DiversifiedCredentialKeys;
import com.secuera.middleware.cardreader.beans.InitializationBean;
import com.secuera.middleware.cardreader.beans.SecureChannelBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class EndOfPerso {

    // declare class variable for different keys
    //private static byte[] CARD_MANAGER_AID;

    int macLength=8;
    private byte[] S_MAC_KEY;
    private byte[] PREV_MAC;

    // *****************************************************************
    // Create instances of other classes.
    // *****************************************************************
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    CredentialKeys credentialKeys = new CredentialKeys();
    DiversifiedCredentialKeys diversifiedCredentialKeys = new DiversifiedCredentialKeys();
    SecureChannelBean scbean = new SecureChannelBean();

    // Constructor get initial class values
    public EndOfPerso(CredentialKeys crKeys,DiversifiedCredentialKeys dCrKeys) {
         credentialKeys = crKeys;
         diversifiedCredentialKeys = dCrKeys;

       }


    public boolean endPerso(CardChannel channel) throws MiddleWareException {
      try {
               selectSecurityDomain(channel);
                //open secure channel
                SecureChannel03 scp03 = new SecureChannel03(credentialKeys,diversifiedCredentialKeys);
                scbean = scp03.openSecureChannel03(channel, false);

                //get values from secure channel 03
                S_MAC_KEY = scbean.getS_MAC_KEY();
                PREV_MAC = scbean.getPREV_MAC();

                setUsePhase(channel);

        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return true;
    }

    private void setUsePhase(CardChannel channel) throws MiddleWareException
     {

		// Now compute new MAC and prepare APDU command.
        StringBuffer sb = new StringBuffer();

        // *****************************************************//
        byte[] apdu_data = {(byte) 0x04, (byte) 0xD8, 0x3F, (byte) 0xFF};
        byte[] usePhaseCmd={0x5C,0x02,0x3F,(byte)0xFF,0x53,0x00};

        byte[] apdu_len = util.calcBertValue(usePhaseCmd.length + macLength ,false,true);

		byte[][] mac_arrays = new byte[5][];
        mac_arrays[0] = PREV_MAC;
        mac_arrays[1] = UcmsMiddlewareConstants.ZERO_CARD_PAD;
        mac_arrays[2] = apdu_data;
        mac_arrays[3] = apdu_len;
        mac_arrays[4] = usePhaseCmd;

	   byte[] mac_calc_data = util.combine_data(mac_arrays);

        // Now add secure pad according to encryption requirement.
        mac_calc_data = add_securepad(mac_calc_data);

        // Now Encrypt the command data field.
        byte[] encrypt_data = aesencrypt(S_MAC_KEY, mac_calc_data, true);

        // Now store the new MAC. Get the first 8 bytes from last block of
        // data.(Each block is of 16 bytes)
        int i_offset = getOffset(encrypt_data);

        PREV_MAC = util.extract_data(encrypt_data, i_offset, 8);

	// Now Prepare the command data for the card.
	    byte[][] apdu_command = new byte[4][];
	    	apdu_command[0] = apdu_data;
       	    apdu_command[1] = apdu_len;
       	    apdu_command[2] = usePhaseCmd;
       	    apdu_command[3] = PREV_MAC;


	   byte[] apdu_command_data = util.combine_data(apdu_command);


        System.out.println("Use Phase command_data :" + util.arrayToHex(apdu_command_data) );

        CommandAPDU USE_PHASE_APDU = new CommandAPDU(apdu_command_data);

        try {

            sendCard.sendCommand(channel, USE_PHASE_APDU, sb);

            // check if no success then throw exception
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();
	        errMsg.append(ExceptionMessages.USE_PHASE_COMMAND);
                errMsg.append(" (").append(sb.toString()).append(")");
                throw new MiddleWareException(errMsg.toString());

            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
     }





    /* select Security Domain method */
    void selectSecurityDomain(CardChannel channel) throws MiddleWareException {

        // **----------------------------------------------
        // ** Check presence and status of PIV-SD
        // **----------------------------------------------
        // ; Select PIV Application Security Domain using Oberthur registered
        // AID
        // 00 A4 04 00 10 A0 00 00 00 77 01 00 00 06 10 00 FD 00 00 00 27 (9000,
        // 6A82)
        // 00 A4 04 00
        //Recv : 6F 3C 84 07
        //A0 00 00 01 51 00 00
        //A5 31 9F 6E 2A 48 20 50 2B 82 31 80 30 00 63 31 16 00 00 00 0C 00 00 14 32 31 16 14 33 31 16 14 34 31 16 00 00 00 00 14 35 31 16 00 00 00 00 9F 65 01 FF


        StringBuffer sb = new StringBuffer();
        byte[] apdu_security_piv = {0x00, (byte) 0xA4, 0x04, 0x00, 0x10,
            (byte) 0xA0, 0x00, 0x00, 0x00, 0x77, 0x01, 0x00, 0x00, 0x06,
            0x10, 0x00, (byte) 0xFD, 0x00, 0x00, 0x00, 0x27};

        CommandAPDU SEC_APDU_PIV = new CommandAPDU(apdu_security_piv);

        try {
            sendCard.sendCommand(channel, SEC_APDU_PIV, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // ; Select PIV Application Issuer Security Domain
        // 00 A4 04 00 07 A0 00 00 01 51 00 00 (9000, 6A82)

        byte[] apdu_security = {0x00, (byte) 0xA4, 0x04, 0x00, 0x07,
            (byte) 0xA0, 0x00, 0x00, 0x01, 0x51, 0x00, 0x00};

        CommandAPDU SEC_APDU = new CommandAPDU(apdu_security);

        try {
            sendCard.sendCommand(channel, SEC_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }





    public byte[] aesencrypt(byte[] key, byte[] from_card, boolean vector) throws MiddleWareException {

        // Create encrypter/decrypter class
        AesEncrypter encrypter;
        byte[] res = null;
        try {

            if (vector) {
                encrypter = new AesEncrypter(key, UcmsMiddlewareConstants.IV_AES);
            }
            {
                encrypter = new AesEncrypter(key);
            }

            // Encrypt
            res = encrypter.encrypt(from_card);
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return res;

    }



    /* Adding secure pad to data to be encrypted for AES CBC Mode */
    byte[] add_securepad(byte[] data) {

        byte[] append_80 = {(byte) 0x80};
        byte[] append_00 = {0x00};

        int i_len = data.length;
        int k = i_len % 16; // Divide by 16, because one standard bye block of
        // data is 16.
        int i_cnt = 16 - k; // Calculate how many more bytes required to make a
        // block of 16 bytes.

        if (k == 0) {
            return data;
        } else {
            for (int j = 0; j < i_cnt; j++) {
                // For the first append, "use append_80"
                if (j == 0) {
                    data = util.combine_data(data, append_80);

                } else {
                    data = util.combine_data(data, append_00);
                }

            }

            return data;

        }

    }

    /*
     * Determine the offset position of last block of 16 byte data from byte[]
     * array.
     */
    int getOffset(byte[] data) {

        // Subtract by 16,because one standard bye block of data is 16.
        int i_len = data.length - 16;

        return i_len;

    }


}

