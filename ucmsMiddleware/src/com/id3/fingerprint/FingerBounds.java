package com.id3.fingerprint;

import java.awt.Point;

public class FingerBounds
{
  public Point topLeft;
  public Point topRight;
  public Point bottomLeft;
  public Point bottomRight;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerBounds
 * JD-Core Version:    0.6.2
 */