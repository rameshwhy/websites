// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.ui;

import eu.gnome.morena.*;
import java.awt.*;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

// Referenced classes of package eu.gnome.morena.ui:
//            ScannerOptionPane

public class ScannerUIDialog extends JDialog
    implements DeviceUIDialog
{

    public static ScannerUIDialog createDeviceUI(Object obj, Device device)
        throws Exception
    {
        if((obj instanceof Component) && (device instanceof Scanner))
            return new ScannerUIDialog((Component)obj, (Scanner)device);
        else
            throw new Exception((new StringBuilder()).append("Wrong argument types used in createDeviceUI(").append(obj.getClass().getName()).append(", ").append(device.getClass().getName()).toString());
    }

    private ScannerUIDialog(Component component, Scanner scanner1)
    {
        super(getOwnerWindowForComponent(component), java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        scanner = scanner1;
        setTitle("Scanning options");
        location = getOwnerWindowForComponent(component).getLocation();
        setDefaultCloseOperation(2);
    }

    public boolean display()
    {
        ScannerOptionPane scanneroptionpane = new ScannerOptionPane(this);
        scanneroptionpane.load(scanner);
        add(scanneroptionpane);
        pack();
        location.translate(50, 50);
        setLocation(location);
        setVisible(true);
        return scanneroptionpane.isSaved();
    }

    private static Window getOwnerWindowForComponent(Component component)
    {
        if(component == null)
            JOptionPane.getRootFrame();
        if(component instanceof Window)
            return (Window)component;
        else
            return getOwnerWindowForComponent(((Component) (component.getParent())));
    }

    private static final long serialVersionUID = 0x8d6da82f9ece834bL;
    Scanner scanner;
    Point location;
}
