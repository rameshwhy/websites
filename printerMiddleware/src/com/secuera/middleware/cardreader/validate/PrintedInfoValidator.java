/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.validate;

import com.secuera.middleware.cardreader.beans.PrintedInfoBean;

/**
 *
 * @author admin
 */
public class PrintedInfoValidator {

    public String validatePin(PrintedInfoBean printInfo) {
        String retValue="";
        if (printInfo.getName().length() > 32) {
            retValue = "Invalid Length - Name";
        }
        
        if (printInfo.getAffiliation().length() > 20) {
            retValue = "Invalid Length - Affiliation";
        }
        
        if (printInfo.getCardSerialNumber().length() > 10) {
            retValue = "Invalid Length - Card Serial Number";
        }
        
        if (printInfo.getIssuerID().length() > 15) {
            retValue = "Invalid Length - Issuer ID";
        }
        
        if (printInfo.getOrganisationLine1().length() > 20) {
            retValue = "Invalid Length - Organization 1";
        }
        if (printInfo.getOrganisationLine2().length() > 20) {
            retValue = "Invalid Length - Organization 2";
        }
        return retValue;
    }
}
