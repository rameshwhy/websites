package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.CertificateBean;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.secuera.middleware.cardreader.exceptions.MiddlewareConstants;

import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

import sun.security.util.ObjectIdentifier;
import sun.security.x509.X509CertImpl;

//import com.secuera.entities.MiddleWareException;s
public class ReadCertificate {

    SendCommand sendCard = new SendCommand();
    CommonUtil util = new CommonUtil();

    public ReadCertificate() {
        super();
    }
    //String Str_cert_hex="3082069b30820583a003020102020a6112ad3000000000011c300d06092a864886f70d01010b0500304d31133011060a0992268993f22c6401191603636f6d31143012060a0992268993f22c64011916046d7969643120301e060355040313176d7969642d57494e2d48304532434534304450302d4341301e170d3133303433303232333030345a170d3136303432393232333030345a3061310b3009060355040613025553310b3009060355040813024e43310d300b0603550407130463697479310c300a060355040a13036f7267310b3009060355040b13024f55311b3019060355040313124469676974616c5369676e6174757265394330820121300d06092a864886f70d01010105000382010e003082010902820100b0cb03b92f0bdc81f28b5cfa903d75da944f011ba1b4a76118c8e9e51ffbbb6bfb9d581a6ea48e8e8f5ab900cc46fc6f4323f5b4cfa2147883d02dba85373cf3982ed372960ff538043acfec97549e9cf6805acf207442187beacf8471a14b2a374a5a49a6c9fea343911ed53e2a5a74fd090b0f3111275d8117e35bdfd001248a368638480011fbd166b80e49a0212a6b94bea2160c696a058731e26c501dd1558102466f7c071a3cf0f5db8cb9cd44d4e7fc97474e11491122b608494c63d85787a9c6db8d3dfe29411d85a6246daa8135012da265394b729cbeda0edf1ad860961ef7e20330e74ae26ce6ffed7c2fd507b50485718da988b781fe4a85505f0203010001a382036830820364303e06092b06010401823715070431302f06272b060104018237150885a5ca7683b88f4d81e58f1b84b2b16186fec94b817085d9af3782a2ef6d020164020109301d0603551d0e04160414224b2154274625c75a6b2df2d976d0c2eb6ee621301f0603551d23041830168014951816d679270dc70e88321ee93b9904bb05026a308201270603551d1f0482011e3082011a30820116a0820112a082010e8681c36c6461703a2f2f2f434e3d6d7969642d57494e2d48304532434534304450302d43412c434e3d57494e2d48304532434534304450302c434e3d4344502c434e3d5075626c69632532304b657925323053657276696365732c434e3d53657276696365732c434e3d436f6e66696775726174696f6e2c44433d6d7969642c44433d636f6d3f63657274696669636174655265766f636174696f6e4c6973743f626173653f6f626a656374436c6173733d63524c446973747269627574696f6e506f696e748646687474703a2f2f77696e2d68306532636534306470302e6d7969642e636f6d2f43657274456e726f6c6c2f6d7969642d57494e2d48304532434534304450302d43412e63726c3082013506082b0601050507010104820127308201233081b306082b060105050730028681a66c6461703a2f2f2f434e3d6d7969642d57494e2d48304532434534304450302d43412c434e3d4149412c434e3d5075626c69632532304b657925323053657276696365732c434e3d53657276696365732c434e3d436f6e66696775726174696f6e2c44433d6d7969642c44433d636f6d3f634143657274696669636174653f626173653f6f626a656374436c6173733d63657274696669636174696f6e417574686f72697479306b06082b06010505073001865f687474703a2f2f77696e2d68306532636534306470302e6d7969642e636f6d2f43657274456e726f6c6c2f57494e2d48304532434534304450302e6d7969642e636f6d5f6d7969642d57494e2d48304532434534304450302d43412e637274300e0603551d0f0101ff0404030206c0302f0603551d25042830260604551d250006082b0601050507030206082b06010505070304060a2b060104018237140202303d06092b060104018237150a0430302e30060604551d2500300a06082b06010505070302300a06082b06010505070304300c060a2b060104018237140202300d06092a864886f70d01010b05000382010100a505c98bb337541d18b9b2df2feab5a81345cf0b4908f931b19c638c019ac5901f516336972d619e8a91dd9edf8357335ef300c645abf4017410707c6aa2b9bb3a6871afa3a44576bfa8ec43cd3f7b56f5defcd40d0f75a47ac651ee35707187a644b2751d6ff9aa34c022a7f8f76a83c54925a5cb785f465682257ed4cbf61ae28097b47136f85c1f2c705357e53b8c1aeb865d28076a09bcc526f5ce57af489a4224e086c6d256fc3fdfe01fbe0993a4d49588d65d567eff29f395ef5449b6d2f2115d93ffb15c4bf1caddb0d75b62ff2076186da6af55cf2759fe1710e89862507a8ec7fca3ed5d352157c9f56305aebc47f76bfaaf4d233acb790150c5e5";

    public CertificateBean readCert(CardChannel channel, char cert_type) {

        byte[] res = null;
        byte[] cert_id;
        byte[] cert_tag = {0x70};
        byte[] encoded = null;
        StringBuffer sb = new StringBuffer();
        CertificateBean certBean = new CertificateBean();
        byte[] apdu_cert = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0X00, 0X00, 0x05};


        byte[] piv_auth = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x05};
        byte[] card_digi = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x0A};
        byte[] key_manage = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x0B};
        byte[] card_auth = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x01};

        if (cert_type == 'P') {
            cert_id = piv_auth;
        } else if (cert_type == 'C') {
            cert_id = card_digi;
        } else if (cert_type == 'K') {
            cert_id = key_manage;
        } else {
            cert_id = card_auth;    //'A'
        }


        //53 82 05 18
        //70 82 05 0d 30820509308203f1a003020102020104300d06092a864886f70d01010b05003032310b3009060355040613025553310d300b060355040a13044e495354311430120603550403130b5049562054657374204341301e170d3133303132373030303030305a170d3134303132373233353935395a3065310b3009060355040613025553311b3019060355040a13124f626572747572546563686e6f6c6f676965311a3018060355040b13114964656e74697479204469766973696f6e311d301b0603550403131449442d4f6e65205049562054657374204361726430819f300d06092a864886f70d010101050003818d0030818902818100e44c79b95af1eada96e385ca16364afcc2c756f2a8c76af2c768be3ae2562405a3f03b57f3d3fd310ba1300500094090b782b061d7a3c4977e05f0bf7e95eba0a617a5f72fa847da4ec44ed1a48892c19094c50d77be0623ce691621c322b07c7fa00fc4f39ae1ee8ce4f477f9e524d447aee04d2a823bbb806122dc618450bf0203010001a382027930820275301f0603551d23041830168014ebda19d208428df4de258769c9bbab0cd396300130290603551d0e0422042053f8f95b031da5009a89c7060be26dbdb33310b04c2d3d83cc6cd45597f4f7cf300e0603551d0f0101ff04040302052030170603551d200410300e300c060a6086480165030201030d3081b40603551d1f0481ac3081a93081a6a081a3a081a08644687474703a2f2f666963746974696f75732e6e6973742e676f762f666963746974696f757343524c6469726563746f72792f666963746974696f757343524c312e63726c86586c6461703a2f2f736d696d65322e6e6973742e676f762f636e3d476f6f6425323043412c6f3d546573742532304365727469666963617465732c633d55533f63657274696669636174655265766f636174696f6e4c6973743082012106082b06010505070101048201133082010f303e06082b060105050730018632687474703a2f2f666963746974696f75732e6e6973742e676f762f666963746974696f75734f4353504c6f636174696f6e2f305e06082b060105050730028652687474703a2f2f666963746974696f75732e6e6973742e676f762f666963746974696f757343657274734f6e6c79434d536469726563746f72792f6365727473497373756564546f476f6f6443412e703763306d06082b0601050507300286616c6461703a2f2f736d696d65322e6e6973742e676f762f636e3d476f6f6425323043412c6f3d546573742532304365727469666963617465732c633d55533f634143657274696669636174652c63726f737343657274696669636174655061697230220603551d11041b30198117636f6d6d6f6e5f6e616d654070697664656d6f2e6f7267300d06092a864886f70d01010b0500038201010017314c6a18e5d60f546f10329b948c432221afce72c73e36c4acf1380202ae7f512437cfd4984111bf37ada33cab63852fb4eb3dab255995d9925884d0a7a546c7a7e6c0189b308bdf86e7be8a0af0a7edef2dc879e3691a48bc0ae3389080a27c7f95e7c07439a64fa9dc81aca6beab582cd60dcbcbdebd2f13de78be524fb9a2760bcdea210934098004fc204c88f89d7e3794fa8bd0c268b768958f9c0b0cd70b0e68247ae624ae7eb65a964892f8275d9f699ff1ed3e3676fc8b6894453014c53d12f78ade20984f973cad5fd965473813090a4c704c3cd15183bd207d8ba38181c78200be242b30ee771f63919c2ab0f56ad6aecfa2e42203949b9c73ff
        //710100
        //7200fe00
        //00 CB 3F FF 
        //00 00 05
        //5C 03 5F C1 0B 
        //
        //1776 

        //53 82 06 ec 

        //70 82 06 e4
        // 1764
        //3082 06df 308205c7a003020102020a13beda3f0000000000e6300d06092a864886f70d01010b0500304d31133011060a0992268993f22c6401191603636f6d31143012060a0992268993f22c64011916046d7969643120301e060355040313176d7969642d57494e2d48304532434534304450302d4341301e170d3133303332343132343633305a170d3136303332333132343633305a3075310b30090603550406130255533110300e060355040a13075365637565726131153013060355040b130c5049562d49204167656e6379311a301806035504031311537572656e647261204368696c6b6f74693121301f06092a864886f70d0109011612534368696c6b6f7469406d7969642e636f6d30820122300d06092a864886f70d01010105000382010f003082010a0282010100ca38503407abcb92c5e0edee0426473f3e704c0906f64c5876b47a02a3cf24d91bfddf5af722684d3cc690f8c4c41e71fd778378f4c4df7b9e9d73c7fb44c6aff2ff0399637abe1473a7c93e87545ac301c1cf419fd7864247ebb33c1640d926e5f4d57d3b67ccec600c3c9e716d4c750fa4f6b186d7d13949d4d4869117a7f66ce3345eb4ab03dacd9cc48d86843e8da12b14e885c2ab128928efee6e5d3ff1bae880c5c15a6abe21bcad324303c23b4320f0af931fe71d5f7c8d0017b8b7fc1d046447f437b2e0ae92b07df4ba5378bc0d06dcb1ae3f39dea8ddc86d9ce50ad8ee846797cd9a134505dcac8b38d64dbe422c74e0975bbf85b5e9bc5e97c400cc080c0400068e080e5cc2080e4cc0f81824ac1804100608dc541c10c4c0bc189cac1804100608dc5422169729da0ee23d3607963c6e12cac5861bfb252e05c206afe48e1e52e54c080590080418c0e4180d54749410c8c0c01820ac180414141c0c101820ac180414141c0c081828ac1804100608dc500808181154749400182182192005940c1818c038180d54743c0407fc10100c081b00c05c180d5474801040c038c030182982192005940c08040c34c1241824ac1804100608dc542810f0c0e8c0281820ac180414141c0c10c0281820ac180414141c0c08c0301828ac1804100608dc500808c018181154749400c028182182192005940c1818c074180d547438105810526b46ebda7a23b2ae497912f7dc6bd4413215144cc07c180d54748c1060c05a005254605b59e49c371c3a20c87ba4ee6412ec1409a8c208049c180d54747c12080478c2080468c208045a8208044a8208043a1a070db19185c0e8bcbcbd0d38f5b5e5a590b55d2538b520c114c90d14d0c11140c0b50d04b10d38f55d2538b520c114c90d14d0c11140c0b10d38f50d1140b10d38f541d589b1a58c94c8c12d95e494c8c14d95c9d9a58d95ccb10d38f54d95c9d9a58d95ccb10d38f50dbdb999a59dd5c985d1a5bdb8b1110cf5b5e5a590b1110cf58dbdb4fd8d95c9d1a599a58d85d1954995d9bd8d85d1a5bdb931a5cdd0fd8985cd94fdbd89a9958dd10db185cdccf58d493111a5cdd1c9a589d5d1a5bdb941bda5b9d21919a1d1d1c0e8bcbddda5b8b5a0c194c98d94d0c191c0c0b9b5e5a590b98dbdb4bd0d95c9d115b9c9bdb1b0bdb5e5a590b55d2538b520c114c90d14d0c11140c0b50d04b98dc9b0c20804d41820ac180414141c04041208049cc208048cc206cc1820ac180414141cc00a1a0699b19185c0e8bcbcbd0d38f5b5e5a590b55d2538b520c114c90d14d0c11140c0b50d04b10d38f5052504b10d38f541d589b1a58c94c8c12d95e494c8c14d95c9d9a58d95ccb10d38f54d95c9d9a58d95ccb10d38f50dbdb999a59dd5c985d1a5bdb8b1110cf5b5e5a590b1110cf58dbdb4fd8d050d95c9d1a599a58d85d194fd8985cd94fdbd89a9958dd10db185cdccf58d95c9d1a599a58d85d1a5bdb905d5d1a1bdc9a5d1e4c1ac1820ac180414141cc006197da1d1d1c0e8bcbddda5b8b5a0c194c98d94d0c191c0c0b9b5e5a590b98dbdb4bd0d95c9d115b9c9bdb1b0bd5d2538b520c114c90d14d0c11140c0b9b5e5a590b98dbdb57db5e5a590b55d2538b520c114c90d14d0c11140c0b50d04b98dc9d0c0341824aa19221bdc3404042c14000e08040400c04e248162d11783fed53d2d1aa4273472e12a129669e05fa2e695c94c999f30f3e1a94477660165e2a108d39a63fdbbbd0feff3d85b20ecb8e81d0d4e4b90985679915fc1fa8f3a06e3371d10b544b0d22ddf21835b936e8c2d7b3133918f42c7063ed3a3d90112c051eee7c89d63d2c8068db14a92ba4671c5f7da72ec947b73d8638066c1c55a625dcf66880f15b3ff19f74cfea9100572d33793f6de66ff4e284eb5b5c46466d964145a76e9a85ec32d7494d8167f6e83d689946c672e644d4abd8f7907f424e67fd74dad654f922d428a7d86f0393b06a364fbc8db273b03a4b64cba3c79d40aa586701a87d6efd84102902f430850bbc99605715ef318
        //71 01 00 fe

        //53 82 06 ec --1772
        //70 82 06 e4 
        //3082 06df 308205c7a003020102020a13beda3f0000000000e6300d06092a864886f70d01010b0500304d31133011060a0992268993f22c6401191603636f6d31143012060a0992268993f22c64011916046d7969643120301e060355040313176d7969642d57494e2d48304532434534304450302d4341301e170d3133303332343132343633305a170d3136303332333132343633305a3075310b30090603550406130255533110300e060355040a13075365637565726131153013060355040b130c5049562d49204167656e6379311a301806035504031311537572656e647261204368696c6b6f74693121301f06092a864886f70d0109011612534368696c6b6f7469406d7969642e636f6d30820122300d06092a864886f70d01010105000382010f003082010a0282010100ca38503407abcb92c5e0edee0426473f3e704c0906f64c5876b47a02a3cf24d91bfddf5af722684d3cc690f8c4c41e71fd778378f4c4df7b9e9d73c7fb44c6aff2ff0399637abe1473a7c93e87545ac301c1cf419fd7864247ebb33c1640d926e5f4d57d3b67ccec600c3c9e716d4c750fa4f6b186d7d13949d4d4869117a7f66ce3345eb4ab03dacd9cc48d86843e8da12b14e885c2ab128928efee6e5d3ff1bae880c5c15a6abe21bcad324303c23b4320f0af931fe71d5f7c8d0017b8b7fc1d046447f437b2e0ae92b07df4ba5378bc0d06dcb1ae3f39dea8ddc86d9ce50ad8ee846797cd9a134505dcac8b38d64dbe422c74e0975bbf85b5e9bc5e97c400cc080c0400068e080e5cc2080e4cc0f81824ac1804100608dc541c10c4c0bc189cac1804100608dc5422169729da0ee23d3607963c6e12cac5861bfb252e05c206afe48e1e52e54c080590080418c0e4180d54749410c8c0c01820ac180414141c0c101820ac180414141c0c081828ac1804100608dc500808181154749400182182192005940c1818c038180d54743c0407fc10100c081b00c05c180d5474801040c038c030182982192005940c08040c34c1241824ac1804100608dc542810f0c0e8c0281820ac180414141c0c10c0281820ac180414141c0c08c0301828ac1804100608dc500808c018181154749400c028182182192005940c1818c074180d547438105810526b46ebda7a23b2ae497912f7dc6bd4413215144cc07c180d54748c1060c05a005254605b59e49c371c3a20c87ba4ee6412ec1409a8c208049c180d54747c12080478c2080468c208045a8208044a8208043a1a070db19185c0e8bcbcbd0d38f5b5e5a590b55d2538b520c114c90d14d0c11140c0b50d04b10d38f55d2538b520c114c90d14d0c11140c0b10d38f50d1140b10d38f541d589b1a58c94c8c12d95e494c8c14d95c9d9a58d95ccb10d38f54d95c9d9a58d95ccb10d38f50dbdb999a59dd5c985d1a5bdb8b1110cf5b5e5a590b1110cf58dbdb4fd8d95c9d1a599a58d85d1954995d9bd8d85d1a5bdb931a5cdd0fd8985cd94fdbd89a9958dd10db185cdccf58d493111a5cdd1c9a589d5d1a5bdb941bda5b9d21919a1d1d1c0e8bcbddda5b8b5a0c194c98d94d0c191c0c0b9b5e5a590b98dbdb4bd0d95c9d115b9c9bdb1b0bdb5e5a590b55d2538b520c114c90d14d0c11140c0b50d04b98dc9b0c20804d41820ac180414141c04041208049cc208048cc206cc1820ac180414141cc00a1a0699b19185c0e8bcbcbd0d38f5b5e5a590b55d2538b520c114c90d14d0c11140c0b50d04b10d38f5052504b10d38f541d589b1a58c94c8c12d95e494c8c14d95c9d9a58d95ccb10d38f54d95c9d9a58d95ccb10d38f50dbdb999a59dd5c985d1a5bdb8b1110cf5b5e5a590b1110cf58dbdb4fd8d050d95c9d1a599a58d85d194fd8985cd94fdbd89a9958dd10db185cdccf58d95c9d1a599a58d85d1a5bdb905d5d1a1bdc9a5d1e4c1ac1820ac180414141cc006197da1d1d1c0e8bcbddda5b8b5a0c194c98d94d0c191c0c0b9b5e5a590b98dbdb4bd0d95c9d115b9c9bdb1b0bd5d2538b520c114c90d14d0c11140c0b9b5e5a590b98dbdb57db5e5a590b55d2538b520c114c90d14d0c11140c0b50d04b98dc9d0c0341824aa19221bdc3404042c14000e08040400c04e248162d11783fed53d2d1aa4273472e12a129669e05fa2e695c94c999f30f3e1a94477660165e2a108d39a63fdbbbd0feff3d85b20ecb8e81d0d4e4b90985679915fc1fa8f3a06e3371d10b544b0d22ddf21835b936e8c2d7b3133918f42c7063ed3a3d90112c051eee7c89d63d2c8068db14a92ba4671c5f7da72ec947b73d8638066c1c55a625dcf66880f15b3ff19f74cfea9100572d33793f6de66ff4e284eb5b5c46466d964145a76e9a85ec32d7494d8167f6e83d689946c672e644d4abd8f7907f424e67fd74dad654f922d428a7d86f0393b06a364fbc8db273b03a4b64cba3c79d40aa586701a87d6efd84102902f430850bbc99605715ef318
        //71 01 00 fe


        apdu_cert = util.combine_data(apdu_cert, cert_id);
        CommandAPDU CERT_APDU = new CommandAPDU(apdu_cert);

        try {
            res = sendCard.sendCommand(channel, CERT_APDU, sb);


        } catch (CardException ex) {
            Logger.getLogger(ReadCertificate.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(util.arrayToHex(res));
        if (res.length > 0) {

            /*Now Extract Certificate data out of response from the card */
            byte[] res_cert_tag = util.extract_data(res, 4, 1);

            if (Arrays.equals(res_cert_tag, cert_tag)) {
                int cert_length = util.byteToInt(util.extract_data(res, 6, 2));

                encoded = util.extract_data(res, 8, cert_length);

                certBean = parseCert(encoded);


            } else {
                //error. throww exception >> .
            }
        }

        return certBean;

    }

    public CertificateBean parseCert(byte[] encoded) {

        X509CertImpl cert = null;
        try {
            cert = new X509CertImpl(encoded);
        } catch (CertificateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        /*
        try {
        cert.verify(cert.getPublicKey());
        } catch (InvalidKeyException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        } catch (CertificateException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        } catch (NoSuchProviderException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        } catch (SignatureException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        } 
         */

        String[] keyUsageType = {
            MiddlewareConstants.digitalSignature,
            MiddlewareConstants.nonRepudiation,
            MiddlewareConstants.keyEncipherment,
            MiddlewareConstants.dataEncipherment,
            MiddlewareConstants.keyAgreement,
            MiddlewareConstants.keyCertSign,
            MiddlewareConstants.cRLSign,
            MiddlewareConstants.encipherOnly,
            MiddlewareConstants.decipherOnly,};

        StringBuilder keyUsage = new StringBuilder();

        boolean[] keyUsageVal;
        CertificateBean certBean = new CertificateBean();
        keyUsageVal = cert.getKeyUsage();
        if (keyUsageVal != null) {
            for (int i = 0; i < keyUsageVal.length; i++) {
                if (keyUsageVal[i]) {
                    if (i == 0) {
                        keyUsage.append(keyUsageType[i]);
                    } else {
                        keyUsage.append(",");
                        keyUsage.append(keyUsageType[i]);
                    }


                }

            }
            certBean.setVersion('V' + Integer.toString(cert.getVersion()));
            certBean.setSerialNumber(cert.getSerialNumber().toString());
            certBean.setSignatureAlgorithm(cert.getSigAlgName());
            certBean.setIssuer(cert.getIssuerDN().toString());
            certBean.setValidFrom(cert.getNotBefore().toString());
            certBean.setValidTo(cert.getNotAfter().toString());




            Collection subjectNames = null;
            try {
                subjectNames = cert.getSubjectAlternativeNames();
            } catch (CertificateParsingException ex) {
                ex.printStackTrace();
            }


            //for (Iterator iter = subjectNames.iterator(); iter.hasNext();) {
            //    List lst = (List) iter.next();
            //    System.out.println("000000000000000000000");
            //   System.out.println(lst.get(1).toString());
            //   System.out.println("000000000000000000000");

            //  List lst1 = (List) iter.next();
            //   /*
            //   if (((Integer) (lst.get(0))).intValue() == RFC822NAME_ID) {
            //      String str = (String) lst.get(1);
            //     if (pattern.matcher(str).matches()) {
            //         // check succeeds!
            //         return certs;
            //     }*/
            //  }



            certBean.setSubject(cert.getSubjectDN().toString());
            certBean.setPublicKey(cert.getPublicKey().toString());
            certBean.setKeyUsage(keyUsage.toString());

            System.out.println(cert.getElements().toString());


            // List<String> ExtendedKeyUsage = new String ;
            StringBuilder ExtnkeyUsage = new StringBuilder();
            String retValue = "";
            /*
            public static String AnyPurpose  = "Any Purpose (2.5.29.37.0)";
            public static String ClientAuthentication  = "Client Authentication (1.3.6.1.5.5.7.3.2)";
            public static String SecureEmail  = "Secure Email (1.3.6.1.5.5.7.3.4)";
            public static String SmartCardLogon  = "Smart Card Logon (1.3.6.1.4.1.311.20.2.2)";
            MiddlewareConstants
             */

            try {
                List<String> ExtndedkeyUsage = cert.getExtendedKeyUsage();



                for (int i = 0; i < ExtndedkeyUsage.size(); i++) {


                    switch (ExtndedkeyUsage.get(i)) {
                        case "2.16.840.1.101.3.6.7":
                            retValue = MiddlewareConstants.PIVContentSigning;
                            break;
                        case "2.5.29.37.0":
                            retValue = MiddlewareConstants.AnyPurpose;
                            break;
                        case "1.3.6.1.5.5.7.3.2":
                            retValue = MiddlewareConstants.ClientAuthentication;
                            break;

                        case "1.3.6.1.5.5.7.3.4":
                            retValue = MiddlewareConstants.SecureEmail;
                            break;
                        case "1.3.6.1.4.1.311.20.2.2":
                            retValue = MiddlewareConstants.SmartCardLogon;
                            break;
                    }

                    if (i == 0) {


                        ExtnkeyUsage.append(retValue);
                    } else {
                        keyUsage.append(",");
                        ExtnkeyUsage.append(retValue);
                    }

                }



            } catch (CertificateParsingException ex) {
                ex.printStackTrace();
            }
            certBean.setExtnkeyUsage(ExtnkeyUsage.toString());




        }

        certBean.setIssuerCommonName("iscn");
        certBean.setIssuerOrganizationUnit("ou");
        certBean.setIssuerOrganizationName("ion");
        certBean.setIssuerCountry("ic");
        certBean.setBGserialNumber(BigInteger.valueOf(1));


        certBean.setSubjectCommonName("CN");
        certBean.setSubjectOrganizationUnit("OU");
        certBean.setSubjectOrganizationName("ON");
        certBean.setSubjectCity("Ct");
        certBean.setSubjectState("NC");
        certBean.setSubjectCountry("US");
        certBean.setSubjectEmail("test@yahoo.com");



        return certBean;
    }
}
