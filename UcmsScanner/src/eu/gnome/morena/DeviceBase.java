// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena;

import java.io.File;
import java.lang.reflect.Method;

// Referenced classes of package eu.gnome.morena:
//            Device, DeviceUIDialog, Manager, TransferDoneListener, 
//            Configuration

public class DeviceBase
    implements Device
{

    protected DeviceBase(String s, long l)
    {
        description = s;
        handle = l;
        functionalUnit = 0;
    }

    protected void fireTransferDone(String s)
    {
        try
        {
            File file = new File(s);
            file = new File(s);
            System.out.println((new StringBuilder()).append("* file transfered -> ").append(s).toString());
            transferDoneListener.transferDone(file);
           file.deleteOnExit();
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }

    protected void fireTransferFailed(int i, String s)
    {
        try
        {
           System.out.println(i != 0 ? (new StringBuilder()).append("* transfer failed - ").append(s).toString() : "* feeder empty");
            transferDoneListener.transferFailed(i, s);
        }
        catch(Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }

    public boolean setupDevice(Object obj)
    {
        if((Configuration.getMode() & 1) > 0)
            return true;
        DeviceUIDialog deviceuidialog = null;
        try
        {
            Class class1 = Configuration.getScannerUIClass();
            if(class1 != null)
            {
                Method method = class1.getMethod("createDeviceUI", new Class[] {
                    Object.class, Device.class
                });
                deviceuidialog = (DeviceUIDialog)method.invoke(null, new Object[] {
                    obj, this
                });
            }
        }
        catch(Throwable throwable)
        {
            StringBuilder stringbuilder = new StringBuilder(1024);
            StackTraceElement astacktraceelement[] = throwable.getStackTrace();
            StackTraceElement astacktraceelement1[] = astacktraceelement;
            int i = astacktraceelement1.length;
            for(int j = 0; j < i; j++)
            {
                StackTraceElement stacktraceelement = astacktraceelement1[j];
                stringbuilder.append(stacktraceelement.toString()).append("\n\t\t");
            }

            System.out.println((new StringBuilder()).append("ERROR: Device dialog cannot be created : \n").append(throwable).append("\n\t\t").append(stringbuilder).toString());
        }
        if(deviceuidialog != null)
        {
            boolean flag = deviceuidialog.display();
            deviceuidialog.dispose();
            return flag;
        } else
        {
            return true;
        }
    }

    public void startTransfer(TransferDoneListener transferdonelistener)
        throws Exception
    {
        startTransfer(transferdonelistener, functionalUnit);
    }

    public void startTransfer()
        throws Exception
    {
        if(transferDoneListener == null)
        {
            throw new Exception("No previous call of startTransfer(): parameters not available");
        } else
        {
            startTransfer(transferDoneListener, functionalUnit);
            return;
        }
    }

    public void startTransfer(TransferDoneListener transferdonelistener, int i)
        throws Exception
    {
        transferDoneListener = transferdonelistener;
        functionalUnit = i;
    }

    public int getFunctionalUnit()
    {
        return functionalUnit;
    }

    public void setFunctionalUnit(int i)
    {
        functionalUnit = i;
    }

    public String toString()
    {
        return description;
    }

    protected static final String IMAGE_DIR;
    protected static String FILE_NAME = "morena_img";
    protected long handle;
    protected String description;
    protected TransferDoneListener transferDoneListener;
    protected int functionalUnit;

    static 
    {
        IMAGE_DIR = (new File(TMP_DIR, "morena_images")).getAbsolutePath();
        (new File(IMAGE_DIR)).mkdirs();
    }
}
