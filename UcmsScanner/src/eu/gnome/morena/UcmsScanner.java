package eu.gnome.morena;


import eu.gnome.morena.Configuration;
import eu.gnome.morena.Device;
import eu.gnome.morena.Manager;
import eu.gnome.morena.Version;
import static eu.gnome.morena.Version.TMP_DIR;
import eu.gnome.morena.wia.WIAScanner;
import java.applet.Applet;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import netscape.javascript.JSObject;
import org.apache.commons.codec.binary.Base64;
import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author satish.mudimbi
 */

public class UcmsScanner extends Applet {
 private static final long serialVersionUID = 0xa873f987410830a0L;
    private static Manager manager;
    private static Map scannerList = new HashMap();
    private static String responseString = "";
    private static String key ;
     private static final Logger log = Logger.getLogger("UcmsScanner");
     
     /**
      * Init method of Java applet. On load the method sends the list of scanners
      * available
      */
    @Override
    public void init()
    {
        try
        {
            SwingUtilities.invokeAndWait(new Runnable() {

                public void run()
                {
                    log.addHandler(new ConsoleHandler()); // ADD THIS LINE TO YOUR CODE
                    log.info("run starting");
                    log.info("Get list of scanners");
                     String msg =  getScannersAsString();
                    try {
                        getAppletContext().showDocument(new URL("javascript:doAlert(\"" + msg +"\")"));
                    } catch (MalformedURLException ex) {
                        Logger.getLogger(UcmsScanner.class.getName()).log(Level.SEVERE, null, ex);
                      
                    }
                }
            }
            );
        }
        catch(Exception ex)
        {
            Logger.getLogger(UcmsScanner.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("Init didn't complete successfully");
        }
    }

    public String getScannersAsString()
    {
       return getScanners("Initial Call");
    }
    
  
    /**
     * This method is core for loading the scanner list and convert to String
     * @param str
     * @return 
     */ 
    public String getScanners(String str)
    {
        log.info("Get Scanners " +  str + " START");
        try
        {
        responseString =  (String) AccessController.doPrivileged(new PrivilegedAction() {
              public Object run() {
                  manager = Manager.getInstance();
                  StringBuilder sb = new StringBuilder();
                  //Fix for HP Office Jet printers
                  Configuration.addDeviceType(".*fficejet.*", true);
                    log.info("Call manager for OS Call to  load list | START");
                    List devices = manager.listDevices();
                     log.info("Call manager for OS Call to  load list | END");
                    if(devices.size() > 0)
                    {
                         log.info("Received " + devices.size() + " from manager");
                        for(int i = 0; i < devices.size(); i++)
                            if(devices.get(i) instanceof WIAScanner)
                            {
                                WIAScanner aScan = (WIAScanner)devices.get(i);
                                 UcmsScanner.scannerList.put(aScan.toString(), aScan);
                            }

                    }
                    if(UcmsScanner.scannerList.size() > 0)
                    {
                       for(Iterator iter = UcmsScanner.scannerList.keySet().iterator(); iter.hasNext(); sb.append((String)iter.next()).append(","));
                         log.info("Build map completed");
                    } else
                    {
                        log.info("No Scanners found send error response");
                        sb.append("Error");
                    }
                    System.out.println((new StringBuilder()).append(">>>>>>>> Sending response ").append(sb.toString()).toString());
                    return sb.toString();
              }
                });
            
        }
        catch(Exception exp)
        {
            Logger.getLogger(UcmsScanner.class.getName()).log(Level.SEVERE, null, exp);
        }
       return responseString;
    }

    /**
     * Method to acquire image
     * @param key
     * @return 
     */
    public String acquireImage(String key)
    {
        if (manager == null)manager = Manager.getInstance();
          log.info(">>>>>> Scanner received for scanner " + key );
        UcmsScanner.key = key;
        String imageAsString =  (String) AccessController.doPrivileged(new PrivilegedAction() {
              public Object run() {
                  String tmpImageString ="";
                   try
                    {
                    /*
                        For 75 dpi A4 (0,0,622,874)
                        For 300 dpi A4 (0,0,2481,3508) - Not working
                        For 150 dpi A4(0,0,1241,1753) - Not working
                        For 600 dpi A4(0,0,4961,7016) - Not working
                    Horizontal size = ((210mm) / (25.4mm/in)) x (300 pixels/in) = 2480.3150 ≅ 2481
                    Vertical size   = ((297mm) / (25.4mm/in)) x (300 pixels/in) = 3507.8740 ≅ 3508
                    */
                        //*** Do not modify these values ****//
                    Device aDevice = (Device)scannerList.get(UcmsScanner.key);
                    Scanner scanner = (Scanner) aDevice;
                    scanner.setMode(Scanner.RGB_8);
                    scanner.setResolution(75);
                    scanner.setFrame(0, 0, 622, 874);  
                    
                    java.awt.image.BufferedImage bimage = AcquireImage.scanImage(scanner);
                    log.info("scanned image info: size=(" + bimage.getWidth() + ", " + bimage.getHeight() + ")   bit mode=" + bimage.getColorModel().getPixelSize());
                    
                    ByteArrayOutputStream bs = new ByteArrayOutputStream();
                    ImageIO.write(bimage, "png", bs);
                     bs.flush();
                     bs.close();
                    
                     log.info("Size of Image ::" + bs.size());
                    byte[] imageAsRawBytes = bs.toByteArray();
                   log.info("After Flush byte size " + imageAsRawBytes.length);
                    BASE64Encoder b64enc = new BASE64Encoder();
                     tmpImageString = b64enc.encode(imageAsRawBytes);
                  
                     }   catch(Exception e)
                    {
                 e.printStackTrace();
                 tmpImageString =  "Error";
              
                    }
                   return tmpImageString;
             }
              
        });
         log.info("******************************************");
                 log.info("Base64 Encoded String : " + imageAsString);
                 if (!imageAsString.equalsIgnoreCase("Error")){
                     //Convert Base64 to Image and write to temp directory
                     convertToImage(imageAsString);
                 }
          log.info("******************************************");
        return imageAsString;
    }

    private void convertToImage(final String imageString){
        AccessController.doPrivileged(new PrivilegedAction() {
              public Object run() {
                
       
        try {
            String fileName = "C:\\Users\\SatishM\\AppData\\Local\\Temp\\morena_images\\test.png";
                 
           log.info("FileName : " + fileName);
             final byte[] imageByte = Base64.decodeBase64(imageString);
           FileOutputStream out = new FileOutputStream(fileName);
            out.write(imageByte);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
         return null;
              }
             
              });
       
        
    

    }
    public static void main(String args1[])
    {
        new UcmsScanner().getScanners("Hello");
       
    }
}