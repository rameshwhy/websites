/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.BiometricBean;
import com.secuera.middleware.cardreader.beans.CardInfoBean;
import com.secuera.middleware.cardreader.beans.CSRBean;
import com.secuera.middleware.cardreader.beans.CertificateBean;
import com.secuera.middleware.cardreader.beans.ChuidBean;
import com.secuera.middleware.cardreader.beans.ContainerInfoBean;
import com.secuera.middleware.cardreader.beans.FacialBean;
import com.secuera.middleware.cardreader.beans.FacialImageBean;
import com.secuera.middleware.cardreader.beans.InitializationBean;
import com.secuera.middleware.cardreader.beans.PersonalizationBean;
import com.secuera.middleware.cardreader.beans.PrintedInfoBean;
import com.secuera.middleware.cardreader.beans.SecurityBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.util.List;
import javax.smartcardio.CardChannel;
import javax.swing.ImageIcon;

/**
 *
 * @author admin
 */
public interface MiddleWareServerCardreader {
    //basic Card Access Functions
    public  List<String> getTerminalsList() throws MiddleWareException;
    
    //Local PIN Management Functions
    public boolean verifyLocalPin(CardChannel channel, String localPin) throws MiddleWareException;
    public boolean changeLocalPin(CardChannel channel, String oldPin, String newPin) throws MiddleWareException;
    public boolean resetLocalPin(CardChannel channel, String unblockPin, String newPin) throws MiddleWareException;

    //Global PIN Management Functions
    public boolean verifyGlobalPin(CardChannel channel, String globalPin) throws MiddleWareException;
    public boolean changeGlobalPin(CardChannel channel, String oldPin, String newPin) throws MiddleWareException;
    public boolean resetGlobalPin(CardChannel channel, InitializationBean initvalue, String newPin) throws MiddleWareException;

    //CHUID Functions
    public ChuidBean readChuid(CardChannel channel) throws MiddleWareException;
    public boolean writeChuid(CardChannel channel,ChuidBean chuid, String piv_admin_key) throws MiddleWareException;

    public byte[] readFASCN(CardChannel channel) throws MiddleWareException;
    
    
    //printed information functions
    public PrintedInfoBean readPrintedInfo(CardChannel channel,String localPin) throws MiddleWareException;
    public boolean writePrintedInfo(CardChannel channel, PrintedInfoBean printInfo, String piv_admin_key) throws MiddleWareException;

    //card information functions
    public CardInfoBean readCardInfo(CardChannel channel,String localPin) throws MiddleWareException;
    public boolean writeCardInfo(CardChannel channel, CardInfoBean cardInfo,String piv_admin_key) throws MiddleWareException;
        
    //CIN/IIN/CUID/BAP
    public String readCIN(CardChannel channel) throws MiddleWareException;
    public String readIIN(CardChannel channel) throws MiddleWareException;
    public String readCUID(CardChannel channel) throws MiddleWareException;
    public String readBAP(CardChannel channel) throws MiddleWareException;
    
    //Lock Card functions
    public boolean cardLock(CardChannel channel,InitializationBean initvalue) throws MiddleWareException;
    public boolean cardUnlock(CardChannel channel,InitializationBean initvalue) throws MiddleWareException;
    
    
    //Misc supporting Functions
    public boolean changePUK(CardChannel channel,String oldPUK, String newPUK) throws MiddleWareException;
    public boolean resetPUK(CardChannel channel, InitializationBean initvalue, String newPUK) throws MiddleWareException;
    public Integer readPinLength(CardChannel channel,char pinType) throws MiddleWareException;

    
    //Facial Image Functions	
    public FacialBean readFacialImage(CardChannel channel,String localPin) throws MiddleWareException;
    public boolean writeFacialImage(CardChannel channel, String piv_admin_key,FacialImageBean facialImage) throws MiddleWareException;
    

    
    //finger print functions
    public BiometricBean readFingerPrintSecu(CardChannel channel,String localPin, String globalPin) throws MiddleWareException;
    public boolean verifyFingerPrint(CardChannel channel,byte[] fingerPrint) throws MiddleWareException;
    public boolean enrollFingerPrint(CardChannel channel,byte[] fingerPrint, String fingerID,String piv_admin_key) throws MiddleWareException;
    
    
    public List<String> getFPTerminalsID3( int ImgQuality,int ImgCount) throws MiddleWareException;
    public BiometricBean readFingerPrintID3( int ImgQuality,int ImgCount,String deviceName,Integer fgType) throws MiddleWareException;
    public void disposeFPCapture(Integer imgQuality, Integer imgCount) throws MiddleWareException;
    
    
    
    
    public boolean cardPersonalization(CardChannel channel,PersonalizationBean peso, String piv_admin_key) throws MiddleWareException;
    public boolean cardInitialization(CardChannel channel,InitializationBean initvalue) throws MiddleWareException;

    

    

    
    public CertificateBean readCertificate(CardChannel channel,char CertificateType) throws MiddleWareException;
    public byte[] generateCertificate(CardChannel channel,String piv_admin_key,CSRBean certBean) throws MiddleWareException;
    public boolean loadCertificate(CardChannel channel,String piv_admin_key,byte[] certificate,char certType) throws MiddleWareException;

    
    public byte[] generateContentCertificate(CardChannel channel,CSRBean certBean) throws MiddleWareException;
    
    
    
    public ContainerInfoBean readContainerInfo(CardChannel channel, String piv_admin_key) throws MiddleWareException;
    
    
    public SecurityBean readSecContainer(CardChannel channel) throws MiddleWareException;
    
    /*
    public String loadCertificate(CardChannel channel,char certificateType,
            String algorithmType, String piv_admin_key, String commonName, String organisationalunit,
            String organisation, String city, String state,
            String country,String certificateTemplate, String caType)  throws MiddleWareException;
    
    */
    

}
