package com.id3.fingerprint;

public class FingerError
{
  public static final int Success = 0;
  public static final int LicenseNotFound = -1;
  public static final int InvalidLicense = -2;
  public static final int LicenseExpired = -3;
  public static final int LicensingError = -4;
  public static final int MaxFingers = -5;
  public static final int MaxUsers = -6;
  public static final int InvalidHandle = -10;
  public static final int InvalidParameter = -11;
  public static final int InvalidFormat = -12;
  public static final int InsufficientBuffer = -13;
  public static final int LibraryNotFound = -14;
  public static final int FormatLimitation = -20;
  public static final int EmptyRecord = -21;
  public static final int NoDevice = -30;
  public static final int CardStatusError = -1000;
  public static final int CardUnkwownError = -1001;
  public static final int CardRemoved = -1002;
  public static final int CardNotTransacted = -1003;
  public static final int CardSharingViolation = -1004;
  public static final int DataIntegrityCheckFailed = -3000;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerError
 * JD-Core Version:    0.6.2
 */