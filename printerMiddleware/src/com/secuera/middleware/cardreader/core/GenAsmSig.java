package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.CertificateBean;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.security.util.DerEncoder;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.util.ObjectIdentifier;
import sun.security.x509.X500Name;

public class GenAsmSig {

    // Author: Harpreet Singh
    /*1.2.840.113549.1.7.1 - data 
     1.2.840.113549.1.7.2 - signedData 
     1.2.840.113549.1.7.3 - envelopedData 
     1.2.840.113549.1.7.4 - signedAndEnvelopedData 
     1.2.840.113549.1.7.5 - digestedData 
     1.2.840.113549.1.7.6 - encryptedData
	 
	    
     --Digest algorithm OIDS
     1.3.14.3.2.26 ---SHA 1
     2.16.840.1.101.3.4.2.1 --SHA256
     2.16.840.1.101.3.4.2.3 --SHA512
    
     --Encryption OIDS
     1.2.840.113549.1.1.13 --SHA512 with RSA Encryption 
     1.2.840.113549.1.1.11 --SHA256 with RSA Encryption
     1.2.840.113549.1.1.1  -- RSAwithSHA1
    	
     1.2.840.113549.1.1.5 -- sha1
	
	
     /* Declare OIDS here*/
    final int CMS_SIGNED_DATA[] = {1, 2, 840, 113549, 1, 7, 2};
    final int PIV_CHUIDSecurityObject_data[] = {2, 16, 840, 1, 101, 3, 6, 1};
    final int PIV_BioMetricObject_data[]={2,16,840,1,101,3,6,2};
    final int PIV_Fascn_data[]= {2,16,840,1,101,3,6,6};
    
    final int Digest_SHA512_data[] = {2, 16, 840, 1, 101, 3, 4, 2, 3};
    final int emailAddress_data[] = {1, 2, 840, 113549, 1, 9, 1};
    final int pivSigner_dn_data[] = {2, 16, 840, 1, 101, 3, 6, 5};
    final int RSAContent_type_data[] = {1, 2, 840, 113549, 1, 9, 3};
    final int RSAMessageDigest_data[] = {1, 2, 840, 113549, 1, 9, 4};
    final int SHA512WithRSA_Encryption_data[] = {1, 2, 840, 113549, 1, 1, 13};
    final int Content_SigningTime[] = {1, 2, 840, 113549, 1, 9, 5};
    
    
    
    private byte bert_tag[];
    private byte[] input_data;
    private PrivateKey privatekey;
    private byte[] piv_FASCN;
    private char container_type;
    
    /*Variables to collect Issuer Information*/
    private String issuerCommonName; 
    private String issuerOrganizationUnit;
    private String issuerOrganizationName;
    private String issuerCountry;
    private BigInteger serialNumber;
   
    /*Variables to Collect Subject Information  */
    private String subjectCommonName;
    private String subjectOrganizationUnit;
    private String subjectOrganizationName;
    private String subjectCity;
    private String subjectState;
    private String subjectCountry;
    private String subjectEmail;
    private Date  signingDate;
    
    
    CommonUtil util = new CommonUtil();

    public GenAsmSig() {
        super();
    }

     public String genAsmSig(String str_hex_input, PrivateKey privateKey, byte[] cert,String str_fascn, 
             Date dt_signingtime,char data_type) throws MiddleWareException, IOException {
        
       /*Data_Type='C'---Chuid
         Data_Type='F'---Facial
         Data_Type='S'---Security Container
        */
        String asmSignature=null;
        container_type=data_type;
        signingDate=dt_signingtime;
        
        if (data_type=='C')
        {
            asmSignature=genCHUIDSig(str_hex_input, privateKey,cert);
        }else if (data_type=='F')
        {
            asmSignature=genFacSig(str_hex_input, privateKey,cert,str_fascn);
        }else
        {
            asmSignature=genSecSig(str_hex_input, privateKey,cert);
        }
        
        
        return asmSignature;
    }
    
     public String genSecSig(String str_hex_sec, PrivateKey privateKey,byte[] cert ) throws MiddleWareException, IOException {

        byte[] asmSignature = null;
        byte[] tot_signer;
        byte[] ver;
        byte[] digestAlgo;
        
        byte[] signer_info;
        // Store input data in class variable.
        input_data = util.hex1ToByteArray(str_hex_sec);
        privatekey = privateKey;
        
        
        // Get Version number
        ver = getVersion('F');
        digestAlgo = getDigestAlgo();
        
        signer_info=getsignerInfo(cert);

        // put the above combined info into sequence.
        DerOutputStream out_1;
        out_1 = new DerOutputStream();
        
        DerValue attr_ver = new DerValue(ver);
        DerValue attr_digestAlgo = new DerValue(digestAlgo);
        DerValue attr_signer_info = new DerValue(signer_info);

        DerValue[] x = {attr_ver,attr_digestAlgo,attr_signer_info};
        out_1.putSequence(x);
        
        
        byte[] any_tag = {(byte) 0xA0};
        byte[] num_bytes = calcBertValue(out_1.toByteArray().length, false);

        // Now Calculate the total bytes for the structure.
        byte[][] signer_arrays = new byte[3][];
        signer_arrays[0] = any_tag;
        signer_arrays[1] = num_bytes;
        signer_arrays[2] = out_1.toByteArray();

        tot_signer = util.combine_data(signer_arrays);
        
        /*Now format the header and calculate the total number of bytes in ASM field.*/
        DerOutputStream out_2;
        out_2 = new DerOutputStream();
        // Out put the OID
        ObjectIdentifier CMS_SIGNED_DATA_OID = ObjectIdentifier.newInternal(CMS_SIGNED_DATA);

        try {
            out_2.putOID(CMS_SIGNED_DATA_OID);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Put everything in Outer Sequence.
        DerOutputStream out_3;
        out_3 = new DerOutputStream();
        DerValue attr_cms_oid = new DerValue(out_2.toByteArray());
        DerValue attr_struct = new DerValue(tot_signer);

        DerValue[] y = {attr_cms_oid,attr_struct};
        out_3.putSequence(y);
        
        // Add one more outer sequence. Don't know why though.
        
        DerOutputStream out_4;
        out_4 = new DerOutputStream();
        DerValue attr_cms = new DerValue(out_3.toByteArray());
        
        DerValue[] z = {attr_cms};
        out_4.putSequence(z);
              
        asmSignature=out_4.toByteArray();
        
        System.out.println("Final Security Container Signature Object : " + util.arrayToHex(asmSignature));

        
        out_1.close();
        out_2.close();
        out_3.close();
        out_4.close();
        
        return util.arrayToHex(asmSignature);


    }

    
    public String genFacSig(String str_hex_fac, PrivateKey privateKey,byte[] cert,String str_fascn ) throws MiddleWareException, IOException {

        byte[] asmSignature = null;
        byte[] tot_signer;
        byte[] ver;
        byte[] pivBio;
        byte[] digestAlgo;
        
        byte[] signer_info;
        // Store input data in class variable.
        input_data = util.hex1ToByteArray(str_hex_fac);
        privatekey = privateKey;
        piv_FASCN=util.hex1ToByteArray(str_fascn);
        
        // Get Version number
        ver = getVersion('F');
        digestAlgo = getDigestAlgo();
        pivBio = getpivBio();
        
        signer_info=getsignerInfo(cert);

        // put the above combined info into sequence.
        DerOutputStream out_1;
        out_1 = new DerOutputStream();
        
        DerValue attr_ver = new DerValue(ver);
        DerValue attr_digestAlgo = new DerValue(digestAlgo);
        DerValue attr_pivCHUID = new DerValue(pivBio);
        DerValue attr_signer_info = new DerValue(signer_info);

        DerValue[] x = {attr_ver,attr_digestAlgo,attr_pivCHUID,
        				attr_signer_info};
        out_1.putSequence(x);
        
        
        byte[] any_tag = {(byte) 0xA0};
        byte[] num_bytes = calcBertValue(out_1.toByteArray().length, false);

        // Now Calculate the total bytes for the structure.
        byte[][] signer_arrays = new byte[3][];
        signer_arrays[0] = any_tag;
        signer_arrays[1] = num_bytes;
        signer_arrays[2] = out_1.toByteArray();

        tot_signer = util.combine_data(signer_arrays);
        
        /*Now format the header and calculate the total number of bytes in ASM field.*/
        DerOutputStream out_2;
        out_2 = new DerOutputStream();
        // Out put the OID
        ObjectIdentifier CMS_SIGNED_DATA_OID = ObjectIdentifier.newInternal(CMS_SIGNED_DATA);

        try {
            out_2.putOID(CMS_SIGNED_DATA_OID);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Put everything in Outer Sequence.
        DerOutputStream out_3;
        out_3 = new DerOutputStream();
        DerValue attr_cms_oid = new DerValue(out_2.toByteArray());
        DerValue attr_struct = new DerValue(tot_signer);

        DerValue[] y = {attr_cms_oid,attr_struct};
        out_3.putSequence(y);
        
        // Add one more outer sequence. Don't know why though.
        
        DerOutputStream out_4;
        out_4 = new DerOutputStream();
        DerValue attr_cms = new DerValue(out_3.toByteArray());
        
        DerValue[] z = {attr_cms};
        out_4.putSequence(z);
              
        asmSignature=out_4.toByteArray();
        
        System.out.println("Final Digital Signature output : " + util.arrayToHex(asmSignature));

        
        out_1.close();
        out_2.close();
        out_3.close();
        out_4.close();
        
        return util.arrayToHex(asmSignature);


    }
    
    
    
    
    
    
    public String genCHUIDSig(String str_hex_chuid, PrivateKey privateKey, byte[] cert) throws MiddleWareException, IOException {

        byte[] asmSignature = null;
        byte[] tot_signer;
        byte[] ver;
        byte[] pivCHUID;
        byte[] digestAlgo;
        byte[] formatCert;
        byte[] signer_info;
        // Store CHUID data in class variable.
        input_data = util.hex1ToByteArray(str_hex_chuid);
        privatekey = privateKey;

        // Get Version number
        ver = getVersion('C');
        digestAlgo = getDigestAlgo();
        pivCHUID = getpivCHUIDSecurityObj();
        formatCert = genCertDer(cert);

        signer_info=getsignerInfo(cert);

        // put the above combined info into sequence.
        DerOutputStream out_1;
        out_1 = new DerOutputStream();
        
        DerValue attr_ver = new DerValue(ver);
        DerValue attr_digestAlgo = new DerValue(digestAlgo);
        DerValue attr_pivCHUID = new DerValue(pivCHUID);
        DerValue attr_formatCert = new DerValue(formatCert);
        DerValue attr_signer_info = new DerValue(signer_info);

        DerValue[] x = {attr_ver,attr_digestAlgo,attr_pivCHUID,attr_formatCert,
        				attr_signer_info};
        out_1.putSequence(x);
        
        
        byte[] any_tag = {(byte) 0xA0};
        byte[] num_bytes = calcBertValue(out_1.toByteArray().length, false);

        // Now Calculate the total bytes for the structure.
        byte[][] signer_arrays = new byte[3][];
        signer_arrays[0] = any_tag;
        signer_arrays[1] = num_bytes;
        signer_arrays[2] = out_1.toByteArray();

        tot_signer = util.combine_data(signer_arrays);
        
        /*Now format the header and calculate the total number of bytes in ASM field.*/
        DerOutputStream out_2;
        out_2 = new DerOutputStream();
        // Out put the OID
        ObjectIdentifier CMS_SIGNED_DATA_OID = ObjectIdentifier.newInternal(CMS_SIGNED_DATA);

        try {
            out_2.putOID(CMS_SIGNED_DATA_OID);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Put everything in Outer Sequence.
        DerOutputStream out_3;
        out_3 = new DerOutputStream();
        DerValue attr_cms_oid = new DerValue(out_2.toByteArray());
        DerValue attr_struct = new DerValue(tot_signer);

        DerValue[] y = {attr_cms_oid,attr_struct};
        out_3.putSequence(y);
        
        // Add one more outer sequence. Don't know why though.
        
        DerOutputStream out_4;
        out_4 = new DerOutputStream();
        DerValue attr_cms = new DerValue(out_3.toByteArray());
        
        DerValue[] z = {attr_cms};
        out_4.putSequence(z);
              
        asmSignature=out_4.toByteArray();
        
        System.out.println("Final Digital Signature output : " + util.arrayToHex(asmSignature));

        
        out_1.close();
        out_2.close();
        out_3.close();
        out_4.close();
        
        return util.arrayToHex(asmSignature);


    }

    public byte[] getsignerInfo(byte[] cert) throws IOException, MiddleWareException {
        byte[] signerInformation = null;

        DerOutputStream out_1, out_2;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();

        byte[] cmsVersion;
        byte[] issuerInfo;
        byte[] digestAlgo;
        byte[] signerInfo;
        byte[] signatureOID;
        byte[] signature = null;
        
        byte[] any_tag = {(byte) 0xA0};

        parseCert(cert);

        cmsVersion = getCMSVersion();
        issuerInfo = getIssuerInfo();
        digestAlgo=getDigestAlgo();
        signerInfo = signerInfo();
    
        signatureOID=getSignatureOID();
        
        try {
            signature = getSignature();
        } catch (MiddleWareException ex) {
            Logger.getLogger(GenAsmSig.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(GenAsmSig.class.getName()).log(Level.SEVERE, null, ex);
        }


        DerValue attr_ver = new DerValue(cmsVersion);
        DerValue attr_issuer = new DerValue(issuerInfo);
        DerValue attr_algo = new DerValue(digestAlgo);
        DerValue attr_signer = new DerValue(signerInfo);
        DerValue attr_sigOID = new DerValue(signatureOID);
        DerValue attr_sig = new DerValue(signature);

        DerValue[] x = {attr_ver, attr_issuer,attr_algo,attr_signer,attr_sigOID, attr_sig};
        out_1.putSequence(x);

        // Encode the values in Set
        DerEncoder[] y = {out_1};
        out_2.putOrderedSet(DerValue.tag_Set, y);

        signerInformation = out_2.toByteArray();

        System.out.println("Digital Signature output : " + util.arrayToHex(signerInformation));

        out_1.close();
        out_2.close();


        return signerInformation;


    }

    private byte[] getVersion(char data_type) {
        /* Here 02-- represents integer.
         * 01-- Number of following bytes.
         * 03--represents version number.
         * */
        
        byte[] ver ;
        byte[] ver03={0x02, 0x01, 0x03};
        byte[] ver01={0x02, 0x01, 0x01};

        if (data_type=='C')
        {
            ver = ver03;
        }else 
        {
             ver = ver01;

        }
        
        return ver;

    }

    private byte[] getCMSVersion() {
        /* Here 02-- represents integer.
         * 01-- Number of following bytes.
         * 01--represents version number.
         * */
        byte[] ver = {0x02, 0x01, 0x01};

        return ver;

    }

    private byte[] getpivCHUIDSecurityObj() throws MiddleWareException, IOException {
        byte[] pivCHUID = null;
        DerOutputStream out_1, out_2;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier PIV_CHUIDSecurityObject_OID = ObjectIdentifier.newInternal(PIV_CHUIDSecurityObject_data);
        out_1.putOID(PIV_CHUIDSecurityObject_OID);

        DerValue attr_oid = new DerValue(out_1.toByteArray());

        DerValue[] x = {attr_oid};
        out_2.putSequence(x);

        pivCHUID = out_2.toByteArray();

        out_1.close();
        out_2.close();


        return pivCHUID;

    }

    
    private byte[] getpivBio() throws MiddleWareException, IOException {
        byte[] pivBIO = null;
        DerOutputStream out_1, out_2;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier PIV_BIO_OID = ObjectIdentifier.newInternal(PIV_BioMetricObject_data);
        out_1.putOID(PIV_BIO_OID);

        DerValue attr_oid = new DerValue(out_1.toByteArray());

        DerValue[] x = {attr_oid};
        out_2.putSequence(x);

        pivBIO = out_2.toByteArray();

        out_1.close();
        out_2.close();


        return pivBIO;

    }
    
    private byte[] getDigestAlgo() throws MiddleWareException, IOException {
        byte[] digestAlgo = null;
        DerOutputStream out_1, out_2, out_3;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier Digest_SHA512_data_OID = ObjectIdentifier.newInternal(Digest_SHA512_data);
        out_1.putOID(Digest_SHA512_data_OID);

        DerValue attr_oid = new DerValue(out_1.toByteArray());

        DerValue[] x = {attr_oid};
        out_2.putSequence(x);



        // Encode the values in Set
        DerEncoder[] y = {out_2};
        out_3.putOrderedSet(DerValue.tag_Set, y);

        digestAlgo = out_3.toByteArray();

        out_1.close();
        out_2.close();
        out_3.close();


        return digestAlgo;

    }

    private byte[] genCertDer(byte[] cert) {

        byte[] any_tag = {(byte) 0xA0};
        byte[] num_bytes = calcBertValue(cert.length, false);

        // Now combine all the information.
        byte[][] cert_arrays = new byte[3][];
        cert_arrays[0] = any_tag;
        cert_arrays[1] = num_bytes;
        cert_arrays[2] = cert;

        cert = util.combine_data(cert_arrays);

        System.out.println("Certificate output : " + util.arrayToHex(cert));

        return cert;

    }

    private byte[] getSignature() throws IOException, MiddleWareException, SignatureException {

    	CommonUtil util= new CommonUtil();
        byte[] signature = null;
        byte[] digSignature;

        DerOutputStream out_1;
        out_1 = new DerOutputStream();
       

        // Get the Digital signature on CHUID Data.
        GenDigitalSignature gensig = new GenDigitalSignature();
        signature = gensig.GenSignature(input_data, privatekey);
        out_1.putOctetString(signature);

        digSignature = out_1.toByteArray();

        out_1.close();
        

        System.out.println("getSignature() : " + util.arrayToHex(digSignature));

        return digSignature;


    }

    private  byte[] getSignatureOID() throws IOException {

		CommonUtil util= new CommonUtil();
        byte[] signatureOID = null;
        

        DerOutputStream out_1, out_2;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        

        // Out put the OID
        ObjectIdentifier SHA512WithRSA_Encryption_data_OID = ObjectIdentifier.newInternal(SHA512WithRSA_Encryption_data);
        out_1.putOID(SHA512WithRSA_Encryption_data_OID);

        
        
        DerValue attr_oid = new DerValue(out_1.toByteArray());
        DerValue[] x = {attr_oid};
        out_2.putSequence(x);
        
        signatureOID = out_2.toByteArray();

        out_1.close();
        out_2.close();
        
        //470ef6617f2b74d9c0
        //30 0b 06 09 2a 86 48 86 f7 0d 01 01 0d 
        //04 82 01 00 
        //470ef6617f2b74d9c09dbe4309b0d89268c584847e8464667b9140db545e2b963e7edfd0db23389c6b17ca599cbb67146dda83d0bf24e7e9323228da1e6c0260d9fe1c25df0c6c2acb73b867e4b85b761205b09f6e58d97b8e9190ee3bfbe0ea0613ef487786f9270a187732dc949a45dffbd3c535988ac3251ab600a9744933f2a2cc648ac4aacf991a08308ec6d23ea249e9bc856c65eb139f5fa405e2052549c325a604472ed24f0acef885054a82d1e9d5fefb9b3fafee3c583d9fcec276dff161a5046a7f16263fba96e990792c1dcf79745c84b4c2dc63c8faa9d63d50809610685f700327e9c494a448e5ee91a586130ff9a7d6808741fb0673439ac0
        
        System.out.println("getSignatureOID() : " + util.arrayToHex(signatureOID));

        return signatureOID;


    } 
    
    
    
    
    public byte[] signerInfo() throws IOException {

        byte[] signerInfo = null;
        byte[] subjectInfo;
        byte[] rsaContent;
        byte[] signingTime;
        byte[] messageDigest = null;
        byte[] any_tag = {(byte) 0xA0};
        byte[] pivFASCN=null;
        int len;

        /* Get RSA Content Type and Hash message  */
        rsaContent = getRSAContentType();
        // Get Content Signing Time.
        signingTime=getSigningTime();
        
        try {
            messageDigest = getRSAMessageDigest();
        } catch (IOException ex) {
            Logger.getLogger(GenAsmSig.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        /*Collect Subject Information  */
        subjectInfo = getSubjectInfo();

        if (container_type=='C')
        {
            len = rsaContent.length+signingTime.length + messageDigest.length + subjectInfo.length;
        }else
        {
            // Get PivFASCN and add its length.
            pivFASCN=getPIVFASCN();
            len = rsaContent.length + messageDigest.length + subjectInfo.length + pivFASCN.length ;
        }
        
        
        byte[] num_bytes = calcBertValue(len, false);

        if (container_type=='C')
        {
            // Now combine all the information.
            byte[][] subject_arrays = new byte[6][];
            subject_arrays[0] =any_tag;
            subject_arrays[1] = num_bytes;
            subject_arrays[2] = rsaContent;
            subject_arrays[3] = signingTime;
            subject_arrays[4] = messageDigest;
            subject_arrays[5] = subjectInfo;
        
            signerInfo = util.combine_data(subject_arrays);
        }else
        {
            // Now combine all the information.
            byte[][] subject_arrays_piv = new byte[7][];
            subject_arrays_piv[0] =any_tag;
            subject_arrays_piv[1] = num_bytes;
            subject_arrays_piv[2] = rsaContent;
            subject_arrays_piv[3] = signingTime;
            subject_arrays_piv[4] = messageDigest;
            subject_arrays_piv[5] = subjectInfo;
            subject_arrays_piv[6] = pivFASCN;
            
            signerInfo = util.combine_data(subject_arrays_piv);
        }
        
        return signerInfo;


    }

    private byte[] getRSAMessageDigest() throws IOException {

        byte[] rsaHash = null;
        byte[] hash;
        DerOutputStream out_1, out_2, out_3, out_hash;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_hash = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier RSAMessageDigest_data_OID = ObjectIdentifier.newInternal(RSAMessageDigest_data);
        out_1.putOID(RSAMessageDigest_data_OID);

        //Get Value of the Hash of original message.
        hash=getMessageHash();
        out_2.putOctetString(hash);

        //Encode the values in Set
        DerEncoder[] y = {out_2};
        out_3.putOrderedSet(DerValue.tag_Set, y);


        DerValue attr_oid = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_3.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_hash.putSequence(x);

        rsaHash = out_hash.toByteArray();

        out_1.close();
        out_2.close();
        out_3.close();
        out_hash.close();

        return rsaHash;

    }

    
    private byte[] getPIVFASCN() throws IOException {

        byte[] pivFASCN = null;
        DerOutputStream out_1, out_2, out_3, out_hash;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_hash = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier PIV_Fascn_data_OID = ObjectIdentifier.newInternal(PIV_Fascn_data);
        out_1.putOID(PIV_Fascn_data_OID);

        //Get Value FASCAN.
        out_2.putOctetString(piv_FASCN);

        //Encode the values in Set
        DerEncoder[] y = {out_2};
        out_3.putOrderedSet(DerValue.tag_Set, y);


        DerValue attr_oid = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_3.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_hash.putSequence(x);

        pivFASCN = out_hash.toByteArray();

        out_1.close();
        out_2.close();
        out_3.close();
        out_hash.close();

        return pivFASCN;

    }
    
    
    private byte[] getRSAContentType() throws IOException {

        byte[] rsaContent = null;
        DerOutputStream out_1, out_2, out_3, out_content;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_content = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier RSAContent_type_data_OID = ObjectIdentifier.newInternal(RSAContent_type_data);
        out_1.putOID(RSAContent_type_data_OID);

        // Get PIV CHUID Security Object as value.
        ObjectIdentifier PIV_CHUIDSecurityObject_OID = ObjectIdentifier.newInternal(PIV_CHUIDSecurityObject_data);
        out_2.putOID(PIV_CHUIDSecurityObject_OID);

        //Encode the values in Set
        DerEncoder[] y = {out_2};
        out_3.putOrderedSet(DerValue.tag_Set, y);


        DerValue attr_oid = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_3.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_content.putSequence(x);

        rsaContent = out_content.toByteArray();

        out_1.close();
        out_2.close();
        out_3.close();
        out_content.close();

        return rsaContent;

    }

    private byte[] getIssuerInfo() throws IOException {
        //  X500Name(String commonName, String organizationUnit,
        //  String organizationName, String country)
    	        
        DerOutputStream issuerInfo, out_final;
        byte[] issuerInformation;
        byte[] certSerial;

        X500Name x500Name = new X500Name(issuerCommonName, issuerOrganizationUnit, issuerOrganizationName,
        								issuerCountry);

        issuerInfo = new DerOutputStream();
        out_final = new DerOutputStream();
        issuerInfo.putInteger(BigInteger.ZERO);

        x500Name.encode(issuerInfo); // X.500 name

        // Remove first 3 bytes.
        issuerInformation = util.extract_data(issuerInfo.toByteArray(), 3,issuerInfo.toByteArray().length - 3);

        // Get Certificate Serial number
        certSerial = getCertSerial(serialNumber);

        DerValue attr_info = new DerValue(issuerInformation);
        DerValue attr_value = new DerValue(certSerial);

        DerValue[] x = {attr_info, attr_value};
        out_final.putSequence(x);


        //System.out.println("X500  NAME " + util.arrayToHex(subjectInformation));

        issuerInfo.close();
        return out_final.toByteArray();

    }

    private byte[] getSubjectInfo() throws IOException
    {	
    	DerOutputStream subjectInfo, out_1, out_2, pivSigner_oid, final_seq;
        
        byte[] subjectInformation;
        byte[] subEmail;

        subjectInfo = new DerOutputStream();
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        pivSigner_oid = new DerOutputStream();
        final_seq = new DerOutputStream();

        X500Name x500Name = new X500Name(subjectCommonName, subjectOrganizationUnit, subjectOrganizationName,
        		 					subjectCity,subjectState, subjectCountry);


        subjectInfo.putInteger(BigInteger.ZERO);

        x500Name.encode(subjectInfo); // X.500 name

        // Remove first 5 bytes. Not required.
        subjectInformation = util.extract_data(subjectInfo.toByteArray(), 5,subjectInfo.toByteArray().length - 5);

        //Now get the Subject Email info.
        subEmail = getSubjectEmail(subjectEmail);

         // Calculate total length of bytes for subjectinfo and email and manually force it into sequence.
        int len =subEmail.length + subjectInformation.length;
        byte[] num_bytes= calcBertValue(len,false);
        byte[] seq_tag= {0x30};
        
        byte[][] subject_arrays = new byte[4][];
        subject_arrays[0] = seq_tag;
        subject_arrays[1] = num_bytes;
        subject_arrays[2] = subEmail;
        subject_arrays[3] = subjectInformation;

        byte[] subject_bytes = util.combine_data(subject_arrays);
        out_1.write(subject_bytes);

       // Encode the values in Set
        DerEncoder[] y = {out_1};
        out_2.putOrderedSet(DerValue.tag_Set, y);

        // Now wrap the above information under PIV Signer_dn OID.
        //pivSigner_dn_data

        //Out put the OID
        ObjectIdentifier pivSigner_dn_data_OID = ObjectIdentifier.newInternal(pivSigner_dn_data);
        pivSigner_oid.putOID(pivSigner_dn_data_OID);


        DerValue attr_oid_piv = new DerValue(pivSigner_oid.toByteArray());
        DerValue attr_value = new DerValue(out_2.toByteArray());

        DerValue[] x_piv = {attr_oid_piv, attr_value};
        final_seq.putSequence(x_piv);

        //System.out.println("Subject Info " + util.arrayToHex(final_seq.toByteArray()));

        subjectInfo.close();
        out_1.close();
        out_2.close();
        pivSigner_oid.close();
        final_seq.close();

        return final_seq.toByteArray();

    }

    private byte[] getSubjectEmail(String subjectEmail) throws IOException {

        byte[] emailInfo = null;
        DerOutputStream out_1, out_2, out_3, out_email;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_email = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier emailAddress_data_OID = ObjectIdentifier.newInternal(emailAddress_data);
        out_1.putOID(emailAddress_data_OID);

        out_email.putIA5String(subjectEmail);

        DerValue attr_oid = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_email.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_2.putSequence(x);

        // Encode the values in Set
        DerEncoder[] y = {out_2};
        out_3.putOrderedSet(DerValue.tag_Set, y);

        emailInfo = out_3.toByteArray();

        out_1.close();
        out_2.close();
        out_3.close();
        out_email.close();

        return emailInfo;

    }

    private byte[] getMessageHash() throws IOException {

        byte[] hash ;

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        digest.update(input_data);
        hash = digest.digest();
        
        
        return hash;

    }

    private byte[] getCertSerial(BigInteger serialnbr) throws IOException {
    	
    	byte[] certSerial = null;
    	DerOutputStream out_1,out_2;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
       
        out_1.putInteger(serialnbr);	
               	      		
 	//Encode the values in Set
        DerEncoder[] y = {out_1};
        out_2.putOrderedSet(DerValue.tag_Set, y);


        certSerial = out_2.toByteArray();

        out_1.close();
        out_2.close();
    	
        return certSerial;
    }

    
    
    private void parseCert (byte[] cert) throws IOException 
    {
        CertificateBean certBean = new CertificateBean();
    
        ReadCertificate rc = new ReadCertificate();
        certBean = rc.parseCert(cert);

        /*Collect Issuer Information*/
        issuerCommonName=certBean.getIssuerCommonName(); 
        issuerOrganizationUnit=certBean.getIssuerOrganizationUnit();
        issuerOrganizationName=certBean.getIssuerOrganizationName();
        issuerCountry=certBean.getIssuerCountry();
        serialNumber=certBean.getBGserialNumber();
               
        /*Collect Subject Information  */
        subjectCommonName=certBean.getSubjectCommonName();
        subjectOrganizationUnit=certBean.getSubjectOrganizationUnit();
        subjectOrganizationName=certBean.getSubjectOrganizationName();
        subjectCity=certBean.getSubjectCity();
        subjectState=certBean.getSubjectState();
        subjectCountry=certBean.getSubjectCountry();
        subjectEmail=certBean.getSubjectEmail();
     }
    
    
    private byte[] getSigningTime() throws IOException {

        byte[] signingTime = null;
        DerOutputStream out_1, out_2, out_3, out_time;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_3 = new DerOutputStream();
        out_time = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier Content_SigningTime_OID = ObjectIdentifier.newInternal(Content_SigningTime);
        out_1.putOID(Content_SigningTime_OID);

        //Get Value FASCAN.
        //YYMMDDhhmmssZ
        out_2.putUTCTime(signingDate);

        //Encode the values in Set
        DerEncoder[] y = {out_2};
        out_3.putOrderedSet(DerValue.tag_Set, y);


        DerValue attr_oid = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_3.toByteArray());

        DerValue[] x = {attr_oid, attr_value};
        out_time.putSequence(x);

        signingTime = out_time.toByteArray();

        out_1.close();
        out_2.close();
        out_3.close();
        out_time.close();

        return signingTime;

    }

    
    
    
    private byte[] calcBertValue(Integer len, boolean binclude_bert) {
        // Determine value of Bert Tag

        byte[] num_bytes;
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for size of data bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size of data bytes
        byte[] len_tag_small = {(byte) 0x81};
        byte[] len_tag_big = {(byte) 0x82};

        if (len > 255) {
            if (binclude_bert) {
                bert_tag = bert_tag_size_big;
            } else {
                bert_tag = len_tag_big;
            }

        } else {
            if (binclude_bert) {
                bert_tag = bert_tag_size_small;
            } else {
                bert_tag = len_tag_small;
            }

        }

        // Now calculate actual data size being stored.
        // Following condition makes sure that converted hex string is always
        // even.

        String tot_hex_size;

        if (len > 255 && len < 4096) {

            tot_hex_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_size = Integer.toHexString(len);

        }

       num_bytes = util.combine_data(bert_tag,util.hex1ToByteArray(tot_hex_size));
        
        return num_bytes ;

    }
}
