/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.beans.PKIBean;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

/**
 *
 * @author Harpreet Singh
 */
public class GenKeyPair {

    PKIBean pki = new PKIBean();

    public GenKeyPair() {
        super();
    }

    public PKIBean keyPair() {

        PublicKey publicKey;
        PrivateKey privateKey;
        KeyPairGenerator keyGen = null;

        try {
            keyGen = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        keyGen.initialize(2048, new SecureRandom());
        KeyPair keypair = keyGen.generateKeyPair();

        publicKey = keypair.getPublic();
        privateKey = keypair.getPrivate();

        pki.setPublicKey(keypair.getPublic());
        pki.setPrivateKey(keypair.getPrivate());

        return pki;

    }
}
