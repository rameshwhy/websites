/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.common;

import com.secuera.middleware.cardreader.entities.MiddleWareException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Harpreet Singh
 */
public class DesEncrypter {

    Cipher ecipher;
    private String transformation = "DESede/ECB/NoPadding";

    /**
     * Default Constructor
     */
    public DesEncrypter() {
        super();
      
    }

    
    public DesEncrypter(byte[] key_p) throws MiddleWareException {
        try {

            SecretKeySpec myKey = new SecretKeySpec(key_p, "DESede");
            
            ecipher = Cipher.getInstance(transformation);
            ecipher.init(Cipher.ENCRYPT_MODE, myKey);
   
        } catch (javax.crypto.NoSuchPaddingException e) {
           throw new MiddleWareException(e.getMessage());
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (java.security.InvalidKeyException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    public byte[] encrypt(byte[] encr) throws MiddleWareException {

        try {

            // Encrypt
            byte[] enc = ecipher.doFinal(encr);

            return enc;
        } catch (javax.crypto.BadPaddingException e) {
            throw new MiddleWareException(e.getMessage());
        } catch (IllegalBlockSizeException e) {
            throw new MiddleWareException(e.getMessage());
        }        
    }
   
}
