/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.biometrics.applet;

import com.id3.fingerprint.FingerCapture;
import com.id3.fingerprint.FingerCaptureDevice;
import com.id3.fingerprint.FingerCaptureListener;
import com.id3.fingerprint.FingerEnroll;
import com.id3.fingerprint.FingerEnrollStatus;
import com.id3.fingerprint.FingerException;
import com.id3.fingerprint.FingerImage;
import com.id3.fingerprint.FingerImageRecord;
import com.id3.fingerprint.FingerImageRecordFormat;
import com.id3.fingerprint.FingerImpression;
import com.id3.fingerprint.FingerLicense;
import com.id3.fingerprint.FingerTemplate;
import com.id3.fingerprint.FingerTemplateRecord;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author krishnapriya
 */
public class FingerprintAuthentication extends javax.swing.JApplet implements FingerCaptureListener {

    /**
     * *****************************************************************************************************************************
     * Static Variables
     * *****************************************************************************************************************************
     */
    private static BiometricsJAppletHelper appletHelper = BiometricsJAppletHelper.getInstance();
    private static Logger logger = null;
    java.util.List<String> terminals = null;
    BiometricError biometricError = null;
    BiometricResponseBean biometricResponse = null;
    java.util.List<String> deviceList = null;
    java.util.List<FingerCaptureDevice> deviceHandleList = new ArrayList<>();
    protected static FingerCaptureDevice selectedDeviceHandle = null;
    private String defaultValue = "------------Select-------------";
    byte[] sub_imp_type = {0x00};
    int sub_bio; //Finger Type.
    int i_capture = 0;
    int image_count = 3;
    int image_quality = 50;
    int minImageCount=0;
    boolean template_failed = false;
    //MiddleWareServerCardreader mws = null;
    private BufferedImage bufferedImage = null;
    private String deviceName = null;
    private FingerCaptureDevice[] devices;
    private FingerCapture capture;
    private FingerEnroll fgEnroll;
    public int imageCount;
    private int fgType;
       
    HashMap<Integer, String> fingerImageMap = new HashMap<Integer, String>();
    String methodName = null;
    String[] fingerArray = new String[2];
    
    private String fingerImage = ""; 	
    private String fingerTemplateImage =  "";	
    
    public FingerprintAuthentication() {
        
        trace("====================== INSIDE CONSTRUCTOR =============================");
        
        if(capture != null){
            try {
                capture.dispose();
               
            } catch (Exception ex) {
                Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(fgEnroll != null){
            fgEnroll.dispose();
        }
        appletHelper.initialiseTheme();
        deviceList = new ArrayList<>();
        initComponents();
        initComponentsCustom();
        
        
        

    }

    public void trace(String paramString) {
        System.out.println("INFO: " + paramString);
    }

    
    @Override
  public void start() {
         //deviceList.add(defaultValue);

         //populateDevicesList();
         
  }
   public final void initScreen() {

        System.out.println("Inside initScreen >>>>>>>>  ");

        initializeFingers();
       //jLabel_status.setText("Please select finger to start enrollment process.");
    }

    public final void initComponentsCustom() {

        trace("Inside initComponentsCustom >>>>>>>>  ");
       // jLabel_status.setText("Please select a device first to start enrollment process.");

    }

    private void initializeFingers() {

        trace("Inside initializeFingers >>>>>>>>  ");

        for (int i = 0; i <= 10; i++) {
            sub_bio = i;
        }


        String[] scannedFingers = null;
        if (scannedFingers != null) {
            for (int i = 0; i < scannedFingers.length; i++) {
             //   setFingerStatus("Y", sub_bio);

            }
        }

        sub_bio = 0;
    }

//    public void getDeviceHandle() {
//        JComboBox comboBox = this.jComboBox_userSelection;
//        String value = comboBox.getSelectedItem().toString();
//        for (FingerCaptureDevice device : deviceHandleList) {
//            try {
//                System.out.println("Selected Finger Device Terminal Name >>>>> 2222222 =====================>   " + device.getName());
//
//                if (value.equalsIgnoreCase(device.getName())) {
//                    FingerprintAuthentication.selectedDeviceHandle = device;
//                    break;
//                }
//            } catch (FingerException ex) {
//                Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }

    public static void main(String args[]) {

        try {
            FingerprintAuthentication fp = new FingerprintAuthentication();


          //  fp.jComboBox_userSelection.setSelectedItem("Secugen Hamster FDU04");

        } catch (Exception ex) {
            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//    private void setFingerprintArray(String templateHexBytes, String imageString)
//    {
//        trace("sub_bio::::::::::::::::::::::::"+sub_bio);   
//        fingerImageMap.put(sub_bio, imageString);
//                
//                if(sub_bio <= 5){
//                     fingerArray[0][sub_bio] = imageString;
//                     fingerArray[0][sub_bio + 5] = templateHexBytes;
//                } else {
//                    fingerArray[1][sub_bio] = imageString;
//                    fingerArray[1][sub_bio - 5] = templateHexBytes;
//                }
//    }
    
    @Override
    public void deviceAdded(FingerCapture paramFingerCapture, FingerCaptureDevice device) {

        if (device != null) {
            try {
                System.out.print(device.getName() + " : Device Added =========================================.\n");
                System.out.print(" : Device Added =========================================  " + deviceList.contains(device.getName()));
                if (!deviceList.contains(device.getName())) {

                    deviceList.add(device.getName());
                    jComboBox_deviceList.setModel(new javax.swing.DefaultComboBoxModel(deviceList.toArray()));
                    System.out.println("Newly Added device name ...  " + device.getName());
                }
            } catch (FingerException ex) {
                Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void deviceRemoved(FingerCapture paramFingerCapture, String deviceName) {
        System.out.print(deviceName + " : Device removed.\n");
        if (deviceName != null) {

            if (deviceList.contains(deviceName)) {

                deviceList.remove(deviceName);
                jComboBox_deviceList.setModel(new javax.swing.DefaultComboBoxModel(deviceList.toArray()));

                System.out.println("Newly removed device name ...  " + deviceName);
            }

        }

    }
    private static String[] statusMessage = {
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.CaptureStopped,
        "Please place your finger on the device........",
        "Please remove your finger from the device........",
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.RollFinger,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.FingerPlaced,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.FingerRemoved,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.FingerDownBorder,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.FingerTopBorder,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.FingerRightBorder,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.FingerLeftBorder,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.PressFingerHarder,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.SwipeTooFast,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.SwipeTooSlow,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.SwipeTooSkewed,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.InapSpeed,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.ImageSmall,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.PoorQuality,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.FingerLifted,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.RollingFast,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.DirtySensor,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.FingerShifted,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.RollingTooShort,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.FewerFinger,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.TooManyFinger,
        com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants.WrongHand};

    @Override
    public void statusChanged(FingerCapture sender, FingerCaptureDevice device, int status) {
        System.out.print("\n- " + statusMessage[status] + " - \n");


        System.out.print("\n- " + status + " - \n");


        if (status == 4) {
            //this.jProgressBar.setValue(100);
            //this.jLabel_status.setText("Fingure Print Captured successfully.");
            //this.jLabel_status.setVisible(true);
        } else {
            // this.jProgressBar.setValue(10);
            this.jLabel_status.setText(statusMessage[status]);
            this.jLabel_status.setVisible(true);

        }

        if (status == 1) {
        }
        String errorCode = null;

        switch (status) {
            case 0:
                errorCode = "";
                break;
            case 1:
                errorCode = "";

                break;
            case 2:
                errorCode = "-1";

                break;
            case 3:
                errorCode = "-1";
                break;
            case 4:
                errorCode = "";

                break;
            case 5:
                errorCode = "-1";
                break;
            case 6:
                errorCode = "-1";
                break;
            case 7:
                errorCode = "-1";
                break;
            case 8:
                errorCode = "-1";
                break;
            case 9:
                errorCode = "-1";
                break;
            case 10:
                errorCode = "-1";
                break;
            case 11:
                errorCode = "-1";
                break;
            case 12:
                errorCode = "-1";
                break;
            case 13:
                errorCode = "-1";
                break;
            case 14:
                errorCode = "-1";
                break;
            case 15:
                errorCode = "-1";
                break;
            case 16:
                errorCode = "-1";
                break;
            case 17:
                errorCode = "-1";
                break;
            case 18:
                errorCode = "-1";
                break;
            case 19:
                errorCode = "-1";
                break;
            case 20:
                errorCode = "-1";
                break;
            case 21:
                errorCode = "-1";
                break;
            case 22:
                errorCode = "-1";
                break;
            case 23:
                errorCode = "-1";
                break;
            case 24:
                errorCode = "-1";
                break;
        }


    }

    @Override
    public void imagePreview(FingerCapture paramFingerCapture, FingerCaptureDevice paramFingerCaptureDevice, FingerImage image) {
        try {
            System.out.println("   - %d finger(s) detected.\n" + image.getSegmentCount());

            image.setImpressionType(FingerImpression.LiveScanPlain);


            this.jLabel_fgImage.setIcon(new javax.swing.ImageIcon(image.createImageAwt().getScaledInstance(98, 128, Image.SCALE_DEFAULT)));
            this.jLabel_fgImage.revalidate();
            this.jLabel_fgImage.repaint();
            this.jLabel_fgImage.setVisible(true);


        } catch (FingerException ex) {
            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void imageCaptured(FingerCapture paramFingerCapture, FingerCaptureDevice device, FingerImage image) {
        try {
            
            if (device != null) {
                System.out.print(device.getName() + " : ");
            }

            if (image.getSegmentCount() > 0) {
                System.out.printf("   - %d finger(s) detected.\n", image.getSegmentCount());
            }

        } catch (FingerException e) {
        }


    }

    @Override
    public void imageProcessed(FingerCapture paramFingerCapture, FingerCaptureDevice paramFingerCaptureDevice, FingerImage image) {
        try {
            // display image information

            displayImageInformation(image);
        } catch (FingerException ex) {
            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        }


        try {
            addImage(image);

        } catch (Exception ex) {
            // Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    private void displayImageInformation(FingerImage image) throws FingerException {
        trace("   - capture device vendor : " + image.getCaptureDeviceVendor() + "\n");
        trace("   - image width : " + image.getWidth() + " pixels\n");
        trace("   - image height : " + image.getHeight() + " pixels\n");
        trace("   - impression type : " + image.getImpressionType() + "\n");
        trace("   - Horizontal res. : " + image.getHorizontalResolution() + " dpi\n");
        trace("   - Vertical res. : " + image.getVerticalResolution() + " dpi\n");
        trace("   - NFIQ : " + image.getQuality() + "\n");


        String iconFile;

        if (image != null) {

            counter = 0;
            while (counter <= 100) {
                jProgressBar.setValue(counter);
                counter++;
                try {
                    Thread.sleep(10L);
                } catch (Exception ex) {
                }
            }


            this.jLabel_status.setText("Finger Print Captured successfully.");
            this.jLabel_status.setVisible(true);
                 bufferedImage = (BufferedImage) image.createImageAwt();

                FingerTemplate template = image.createTemplate();
                 
                trace("=========  Template  ================ [ " + template + " ]");
                
                
                byte[] templateBytes = template.toIso19794CompactCard(template.getMinutiaCount());
                
                com.secuera.middleware.core.common.CommonUtil util = new com.secuera.middleware.core.common.CommonUtil();
                
                FingerImageRecord fpr = new FingerImageRecord();
                
                FingerImage fpi = image.clone();
                fpr.add(fpi);
                
                byte[] ansiFormat = fpr.toBuffer(FingerImageRecordFormat.Incits381_2009);
                 
                String ansitemplateHexBytes = util.arrayToHex(ansiFormat);
                  
                trace("=========  ANSI Template bytes ================ [ " + ansitemplateHexBytes + " ]");
                
                String templateHexBytes = util.arrayToHex(templateBytes);
                trace("=========  Template bytes ================ [ " + templateHexBytes + " ]");
                //bufferedImage = createCompactTemplate();
               // this.imageQuality.setText("" + image.getQuality());

                String imageString = imageToString(bufferedImage);
//                fingerImage = imageString;
//                fingerTemplateImage = templateHexBytes;
                fingerArray[0] = imageString;
                fingerArray[1] = templateHexBytes;
                /*fingerImageMap.put(sub_bio, imageString);
                
                if(sub_bio <= 5){
                     fingerArray[0][sub_bio] = imageString;
                     fingerArray[0][sub_bio + 5] = templateHexBytes;
                } else {
                    fingerArray[1][sub_bio] = imageString;
                    fingerArray[1][sub_bio - 5] = templateHexBytes;
                }*/
               
                
              //  setImageSections(bufferedImage,image.getQuality(),templateHexBytes,imageString );
                this.jLabel_fgImage.setIcon(new javax.swing.ImageIcon(bufferedImage.getScaledInstance(117, 155, Image.SCALE_DEFAULT)));
                this.jLabel_fgImage.revalidate();
                this.jLabel_fgImage.repaint();
                this.jLabel_fgImage.setVisible(true);

                iconFile = "/images/accept.png";
               // setFingerStatus("Y", sub_bio);
               // setImageCaptureStatus(i_capture, iconFile);
               // this.jLabel_scan1.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource(iconFile)));
               // this.jLabel_scan1.setVisible(true);
              //  setButtons(true);
            

        }
        //  setButtons(true);
    }

    
    public String imageToString(BufferedImage bImage)   {

        String imageString = null;

        //image to bytes
 
        try {

            System.out.println("================= ENCODED STRING VALUE START ============================ \n" + bImage + "\n");
              
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bImage, "jpg", baos );
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
             
            //bytes to string

            BASE64Encoder enc = new BASE64Encoder();
             
            imageString =  enc.encode(imageInByte);
            
            //System.out.println("================ IMAGE STRING NEW ===============\n" + imageString + "\n");
           
            System.out.println("================= ENCODED STRING VALUE ENDS ============================ \n");
            

        } catch (Exception ex) {

            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);

        }

        return imageString;

    }
    
    
    public void addImage(FingerImage image) throws Exception {

        int fgImgStatus = 0;
        String errorCode = "";
        String errorMsg = "";
        if (fgEnroll == null) {
            try {

                fgEnroll = new FingerEnroll();
            } catch (FingerException ex) {
                throw new Exception(ex.getMessage());
                //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //add image
        try {
            fgImgStatus = fgEnroll.addImage(image);
        } catch (FingerException ex) {

            throw new Exception(ex.getMessage());
            //Logger.getLogger(Id3BioCapture.class.getName()).log(Level.SEVERE, null, ex);
        }


        //check image status
        switch (fgImgStatus) {
            case FingerEnrollStatus.InProgress:
                errorCode = "";
                errorMsg = "In Progress";
                break;
            case FingerEnrollStatus.WrongFinger:
                errorCode = "-1";
                errorMsg = "Wrong Finger Placed";
                break;
            case FingerEnrollStatus.UnknownFinger:
                errorCode = "-1";
                errorMsg = "Unknown Finger Placed";
                break;
            case FingerEnrollStatus.NonMatch:
                errorCode = "-1";
                errorMsg = "The presented finger does not match with the previous presentations.";
                break;
            case FingerEnrollStatus.Completed:
                errorCode = "";
                errorMsg = "Compelete.";
                break;
            case FingerEnrollStatus.Failed:
                errorCode = "-1";
                errorMsg = "Finger Catpure Failed.";
                break;

        }

        //return if there is an error
        if (errorCode.equalsIgnoreCase("-1")) {
        } else {

            imageCount++;
        }



    }

    public BufferedImage createCompactTemplate() {

        trace("========= BEFORE CREATING THE IMAGE TEMPLATE ====================");
        BufferedImage fgImage = null;
        FingerTemplate[] fgTemplates = null;
        FingerTemplate fgTemplate = null;
        FingerTemplateRecord fgTemplateRecord = null;
        FingerImage image = null;


        try {
            image = fgEnroll.getBestImage(sub_bio);
            trace("========= BEST IMAGE QUALITY IS ====================>   " + image.getQuality());
        
        } catch (FingerException ex) {
            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            fgTemplateRecord = fgEnroll.createTemplateRecord(1);
            
            trace("========= TEMPLATE COUNTS ====================>  " + fgTemplateRecord.getFingerCount());
        
        } catch (FingerException ex) {
            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        }


        fgTemplates = fgTemplateRecord.getTemplates();
        fgTemplate = fgTemplates[0];
        int minuCount = 0;

        try {
            minuCount = fgTemplate.getMinutiaCount();
            
            trace("========= MINUTIA COUNT ====================>  " + minuCount);
        
        } catch (FingerException ex) {
            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {

            byte[] templateImage =  fgTemplate.toIso19794CompactCard(minuCount);
            
            InputStream in = new ByteArrayInputStream(templateImage);
	    BufferedImage bImageFromConvert = ImageIO.read(in);
            fgImage = bImageFromConvert;
            
        } catch (FingerException ex) {
            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
        }
  
        imageCount = 0;

        fgEnroll.dispose();

        trace("========= AFTER CREATING THE IMAGE TEMPLATE ====================");
        
        return fgImage;

    }

    private void checkLicence(Integer imageCount) {
        try {
            FingerLicense.checkLicense();
            fgEnroll = new FingerEnroll();
            fgEnroll.setMinimumImageCount(imageCount);
            fgEnroll.setImageQualityThreshold(90);
            trace("License is valid\n");
            trace(" - path : " + FingerLicense.getLicensePath() + "\n");
            trace(" - owner : " + FingerLicense.getLicenseOwner() + "\n");
            trace(" - count : " + FingerLicense.getMaxFingerCount() + "\n");
            trace(" - imageCount :"+image_count);
            trace(" - fgEnroll.imageCount :"+fgEnroll.getMinimumImageCount());
        } catch (FingerException ex) {

            trace("\nAn error has occured !");

        }
    }

    private void populateDevicesList() {
        try {
            capture = new FingerCapture(this, false);
            fgEnroll = new FingerEnroll();
            fgEnroll.setMinimumImageCount(image_count);
           /* lpinkyCount = image_count; 	
            lringCount = image_count;		
            lmiddleCount = image_count;
            lindexCorthumbCountunt = image_count;
            lthumbCount = image_count;
            rpinkyCount = image_count;
            rringCount = image_count;
            rmiddleCount = image_count;	
            rindexCount = image_count;	
             = image_count;*/
            Thread.sleep(60L);
            devices = capture.getDevices();
            trace("=========== List Of Devices ====================  " + devices.length);
            trace(" ============= fgEnroll.imageCount populateDevicesList ==================:"+fgEnroll.getMinimumImageCount());
            if (devices != null && devices.length > 0) {


                for (FingerCaptureDevice device : devices) {
                    trace("Device Name is >>>>>>>>>>>  " + device);
                    if (!deviceList.contains(device.getName())) {
                        deviceList.add(device.getName());
                    }
                }
                jComboBox_deviceList.setModel(new javax.swing.DefaultComboBoxModel(deviceList.toArray()));
                this.jLabel_errorMessage.setVisible(false);
                this.jLabel_status.setVisible(true);
                initScreen();

            } else {
                trace("InSide Else >>>>>>>>>>>  " + devices);
                this.jLabel_errorMessage.setText("ERROR: Please attach a finger capturing terminal first.");
                this.jLabel_status.setVisible(false);
                this.jLabel_errorMessage.setVisible(true);

            }


        } catch (InterruptedException | FingerException ex) {
            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
            if (ex instanceof FingerException) {
                this.jLabel_status.setText(ex.getMessage());
                this.jLabel_status.setForeground(new java.awt.Color(163, 24, 0));
            }
        }

    }

    public void startCapture() {
        JComboBox comboBox = this.jComboBox_deviceList;
        String value = comboBox.getSelectedItem().toString();
        FingerCaptureDevice captureDevice = null;


        try {
            for (FingerCaptureDevice fcd : devices) {

                if (value.equalsIgnoreCase(fcd.getName())) {
                    captureDevice = fcd;
                    break;
                }

            }
            trace("Finger Number ===================  " + sub_bio);

            captureDevice.startCapture(sub_bio);
            //captureDevice.stopCapture();
            //fgEnroll.dispose();
        } catch (FingerException ex) {
            Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
            this.jLabel_status.setText(ex.getMessage());

        }


    }
    protected int counter = 0;

    private void resetProgressBar() {
        this.jProgressBar.setValue(0);
        this.jLabel_fgImage.setIcon(null);
      //  this.jLabel_scan1.setVisible(false);
      //  this.jLabel_scan1.setIcon(null);
      //  this.imageQuality.setText(new Integer(0).toString());
    }

    public HashMap<Integer, String> getFingerImages(){
         
        return fingerImageMap;
    }
    
     public String[] getFingerImagesArray(){
         
        System.out.println("fingerArray:::::::::::"+fingerArray.length);
         if((fingerArray.length<1))
         {
             return null;
         }
         else
            return fingerArray;
    }
    
    public String validateFingerprints(){
        
        return null;
    }    
     private boolean isEmpty(String str) {
        return str == null || "".equalsIgnoreCase(str) || str.length() == 0;
    }

    @Override
    public void destroy() 
    {
        
        
        System.out.println("applet:destroyStart");
  
       AccessController.doPrivileged(new PrivilegedAction() 
      {

          @Override
           public Void run() {
               try {
                    removeAll();
                    System.gc();
                    
                    RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
 
                    //
                    // Get name representing the running Java virtual machine.
                    // It returns something like 6460@AURORA. Where the value
                    // before the @ symbol is the PID.
                    //
                    String jvmName = bean.getName();
                    System.out.println("Name = " + jvmName);

                    //
                    // Extract the PID by splitting the string returned by the
                    // bean.getName() method.
                    //
                    long pid = Long.valueOf(jvmName.split("@")[0]);
                    System.out.println("PID  = " + pid);
                    
                    String command = "taskkill /F /PID " + pid;
                    
                    
                    
                    System.out.println("applet:destroyRemoved");
                    Runtime runtime = Runtime.getRuntime ();
                    //runtime.exec ("taskkill /f /im java.exe").waitFor ();
                    runtime.exec (command).waitFor ();
                    
                    
                } catch ( InterruptedException | IOException ex) {
                    Logger.getLogger(FingerprintAuthentication.class.getName()).log(Level.SEVERE, null, ex);
                }
               return null;
           }
       });
    }

    /**
     * Initializes the applet FingerprintAuthentication
     */
    @Override
    public void init() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FingerprintAuthentication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FingerprintAuthentication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FingerprintAuthentication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FingerprintAuthentication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the applet */
        try {
            java.awt.EventQueue.invokeAndWait(new Runnable() {
                public void run() {
                    initComponents();
                     checkLicence(image_count);
        
          
         deviceList.add(defaultValue);

         populateDevicesList();
         
    //     setButtons(false);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method is called from within the init() method to initialize the
     * form. WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Jbutton_leftThumb = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jButton_selectDevice = new javax.swing.JButton();
        jComboBox_deviceList = new javax.swing.JComboBox();
        jLabel_status = new javax.swing.JLabel();
        jLabel_errorMessage = new javax.swing.JLabel();
        Jbutton_leftThumb1 = new javax.swing.JButton();
        jLabel_fgImage = new javax.swing.JLabel();
        jProgressBar = new javax.swing.JProgressBar();

        Jbutton_leftThumb.setToolTipText("Left Hand Thumb");
        Jbutton_leftThumb.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Jbutton_leftThumb.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        Jbutton_leftThumb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_leftThumbActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setOpaque(false);

        jButton_selectDevice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/busy.png"))); // NOI18N
        jButton_selectDevice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_selectDeviceActionPerformed(evt);
            }
        });

        jComboBox_deviceList.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jComboBox_deviceList.setModel(new javax.swing.DefaultComboBoxModel(deviceList.toArray()));
        jComboBox_deviceList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_deviceListActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jComboBox_deviceList, 0, 205, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_selectDevice, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jComboBox_deviceList, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_selectDevice, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29))
        );

        jLabel_status.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel_status.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_status.setText("Please select a finger and click on \"Capture\" button.");
        jLabel_status.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_errorMessage.setBackground(new java.awt.Color(0, 0, 0));
        jLabel_errorMessage.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel_errorMessage.setForeground(new java.awt.Color(163, 24, 0));
        jLabel_errorMessage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_errorMessage.setText("Error Message");
        jLabel_errorMessage.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        Jbutton_leftThumb1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Jbutton_leftThumb1.setText("Capture Fingerprint");
        Jbutton_leftThumb1.setToolTipText("Capture Fingerprint");
        Jbutton_leftThumb1.setFocusTraversalPolicyProvider(true);
        Jbutton_leftThumb1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jbutton_leftThumb1ActionPerformed(evt);
            }
        });

        jLabel_fgImage.setBackground(new java.awt.Color(255, 255, 255));
        jLabel_fgImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_fgImage.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Finger Image View", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 3, 14), java.awt.Color.darkGray)); // NOI18N
        jLabel_fgImage.setOpaque(true);

        jProgressBar.setBackground(new java.awt.Color(149, 204, 212));
        jProgressBar.setForeground(new java.awt.Color(18, 20, 20));
        jProgressBar.setMinimum(50);
        jProgressBar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jProgressBar.setStringPainted(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel_status, javax.swing.GroupLayout.PREFERRED_SIZE, 740, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_errorMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 740, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(Jbutton_leftThumb1, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(73, 73, 73)))
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_fgImage, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel_status, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_errorMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(61, 61, 61)
                        .addComponent(Jbutton_leftThumb1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel_fgImage, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Jbutton_leftThumb1.getAccessibleContext().setAccessibleName("Capture Fingerprint");
    }// </editor-fold>//GEN-END:initComponents

    private void Jbutton_leftThumbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_leftThumbActionPerformed
        i_capture = 0;
        sub_bio = 6;

        //    jLabel_leftThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //    //jLabel_status.setText("Place Your Left Hand Thumb on Scanner & Click Capture");
        //    //setButtons(false);
        //     resetProgressBar();
        //    setButtons(false);
        //    populateFPCountImages(image_count,lthumbCount);
        //    startCapture();

             try{
                Thread.sleep(1000);
                resetProgressBar();
           //     setButtons(false);
             //   populateFPCountImages(image_count,lthumbCount);

                startCapture();
                methodName = "Jbutton_leftThumbActionPerformed";
            } catch(Exception e){

            }
    }//GEN-LAST:event_Jbutton_leftThumbActionPerformed

    private void Jbutton_leftThumb1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jbutton_leftThumb1ActionPerformed
        i_capture = 0;

        //    jLabel_leftThumb.setIcon(new javax.swing.ImageIcon(BiometricsJApplet.class.getResource("/images/button_process.png")));
        //    //jLabel_status.setText("Place Your Left Hand Thumb on Scanner & Click Capture");
        //    //setButtons(false);
        //     resetProgressBar();
        //    setButtons(false);
        //    populateFPCountImages(image_count,lthumbCount);
        //    startCapture();

          try{
                Thread.sleep(1000);
                resetProgressBar();
           //     setButtons(false);
           //     populateFPCountImages(image_count,lthumbCount);

                startCapture();
                methodName = "Jbutton_leftThumbActionPerformed";
            } catch(Exception e){

            }

        
    }//GEN-LAST:event_Jbutton_leftThumb1ActionPerformed

    private void jComboBox_deviceListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_deviceListActionPerformed

        JComboBox comboBox = jComboBox_deviceList;
        String value = comboBox.getSelectedItem().toString();

        if (!defaultValue.equalsIgnoreCase(value)) {
         //   setButtons(true);
            deviceName = value;
            jLabel_status.setText("Please select finger to start enrollment process.");
        } else {
       //     setButtons(false);
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(this, "All the captured data will be lost. \n Do you really want to re-start the process.", "Re-Start Process", dialogButton);
            if (dialogResult == 0) {
         //       resetScreenComponents();
                jLabel_status.setText("Please select a device first to start enrollment process.");
            } else {
                comboBox.setSelectedItem(deviceName);
                trace("No Option");
            }

        }
    }//GEN-LAST:event_jComboBox_deviceListActionPerformed

    private void jButton_selectDeviceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_selectDeviceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton_selectDeviceActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Jbutton_leftThumb;
    private javax.swing.JButton Jbutton_leftThumb1;
    private javax.swing.JButton jButton_selectDevice;
    private javax.swing.JComboBox jComboBox_deviceList;
    private javax.swing.JLabel jLabel_errorMessage;
    private javax.swing.JLabel jLabel_fgImage;
    private javax.swing.JLabel jLabel_status;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JProgressBar jProgressBar;
    // End of variables declaration//GEN-END:variables
}
