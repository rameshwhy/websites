/*     */ package com.id3.fingerprint;
/*     */ 
import java.io.File;
/*     */ import java.util.ArrayList;
/*     */ 
/*     */ public class FingerMatch
/*     */ {
/*  17 */   private boolean _disposed = false;
/*  18 */   long _handle = 0L;
/*  19 */   ArrayList<FingerTemplateRecord> _templateRecords = new ArrayList();
/*     */ 
/*     */   static
/*     */   {
/* 383 */     String tmpStr = "id3Finger.dll";
                File TMP_DIR = new File(System.getProperty("java.io.tmpdir"));
                File localFile = new File(TMP_DIR, tmpStr);


                 System.load(localFile.getAbsolutePath());
/*     */   }
/*     */ 
/*     */   public FingerMatch()
/*     */     throws FingerException
/*     */   {
/*  28 */     long[] handle_ptr = new long[1];
/*  29 */     int result = nInitialize(handle_ptr);
/*     */ 
/*  31 */     if (result != 0) {
/*  32 */       throw new FingerException(result);
/*     */     }
/*  34 */     this._handle = handle_ptr[0];
/*     */   }
/*     */ 
/*     */   public FingerMatch(int maxRotation)
/*     */     throws FingerException
/*     */   {
/*  45 */     long[] handle_ptr = new long[1];
/*  46 */     int result = nInitialize(handle_ptr);
/*     */ 
/*  48 */     if (result == 0) {
/*  49 */       result = nSetMaximumRotation(this._handle, maxRotation);
/*     */     }
/*  51 */     if (result != 0) {
/*  52 */       throw new FingerException(result);
/*     */     }
/*  54 */     this._handle = handle_ptr[0];
/*     */   }
/*     */ 
/*     */   public void dispose()
/*     */   {
/*  62 */     nDispose(this._handle);
/*  63 */     this._handle = 0L;
/*  64 */     this._disposed = true;
/*  65 */     this._templateRecords.clear();
/*     */   }
/*     */ 
/*     */   protected void finalize()
/*     */     throws Throwable
/*     */   {
/*     */     try
/*     */     {
/*  75 */       dispose();
/*     */     }
/*     */     catch (Exception localException)
/*     */     {
/*     */     }
/*     */     finally
/*     */     {
/*  82 */       super.finalize();
/*     */     }
/*     */   }
/*     */ 
/*     */   public int getMaximumRotation()
/*     */     throws FingerException
/*     */   {
/*  94 */     int[] param = new int[1];
/*  95 */     int result = nGetMaximumRotation(this._handle, param);
/*     */ 
/*  97 */     if (result != 0) {
/*  98 */       throw new FingerException(result);
/*     */     }
/* 100 */     return param[0];
/*     */   }
/*     */ 
/*     */   public void setMaximumRotation(int value)
/*     */     throws FingerException
/*     */   {
/* 111 */     int result = nSetMaximumRotation(this._handle, value);
/*     */ 
/* 113 */     if (result != 0)
/* 114 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public int getThreshold()
/*     */     throws FingerException
/*     */   {
/* 124 */     int[] level = new int[1];
/* 125 */     int result = nGetThreshold(this._handle, level);
/*     */ 
/* 127 */     if (result != 0) {
/* 128 */       throw new FingerException(result);
/*     */     }
/* 130 */     return level[0];
/*     */   }
/*     */ 
/*     */   public void setThreshold(int value)
/*     */     throws FingerException
/*     */   {
/* 140 */     int result = nSetThreshold(this._handle, value);
/*     */ 
/* 142 */     if (result != 0)
/* 143 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public int getUsedCores()
/*     */     throws FingerException
/*     */   {
/* 153 */     int[] level = new int[1];
/* 154 */     int result = nGetUsedCores(this._handle, level);
/*     */ 
/* 156 */     if (result != 0) {
/* 157 */       throw new FingerException(result);
/*     */     }
/* 159 */     return level[0];
/*     */   }
/*     */ 
/*     */   public void setUsedCores(int value)
/*     */     throws FingerException
/*     */   {
/* 170 */     int result = nSetUsedCores(this._handle, value);
/*     */ 
/* 172 */     if (result != 0)
/* 173 */       throw new FingerException(result);
/*     */   }
/*     */ 
/*     */   public int getFingerCount()
/*     */   {
/* 181 */     return nGetFingerCount(this._handle);
/*     */   }
/*     */ 
/*     */   public int compare(FingerTemplate reference, FingerTemplate probe)
/*     */     throws FingerException
/*     */   {
/* 193 */     int[] score = new int[1];
/* 194 */     int result = nCompareTemplates(this._handle, reference.getHandle(), probe.getHandle(), score);
/*     */ 
/* 196 */     if (result != 0) {
/* 197 */       throw new FingerException(result);
/*     */     }
/* 199 */     return score[0];
/*     */   }
/*     */ 
/*     */   public FingerMatchResult compare(FingerTemplateRecord reference, FingerTemplateRecord probe)
/*     */     throws FingerException
/*     */   {
/* 211 */     long[] hMatchResult = new long[1];
/* 212 */     int result = nCompareRecords(this._handle, reference.getHandle(), probe.getHandle(), hMatchResult);
/*     */ 
/* 214 */     if (result != 0) {
/* 215 */       throw new FingerException(result);
/*     */     }
/* 217 */     return new FingerMatchResult(hMatchResult[0]);
/*     */   }
/*     */ 
/*     */   public void addReference(FingerTemplate template)
/*     */     throws FingerException
/*     */   {
/* 231 */     FingerTemplateRecord templateRecord = new FingerTemplateRecord(template);
/*     */ 
/* 233 */     int result = nAddReference(this._handle, templateRecord.getHandle());
/*     */ 
/* 235 */     if (result != 0) {
/* 236 */       throw new FingerException(result);
/*     */     }
/*     */ 
/* 239 */     this._templateRecords.add(templateRecord);
/*     */   }
/*     */ 
/*     */   public void addReference(FingerTemplateRecord templateRecord)
/*     */     throws FingerException
/*     */   {
/* 253 */     int result = nAddReference(this._handle, templateRecord.getHandle());
/*     */ 
/* 255 */     if (result != 0) {
/* 256 */       throw new FingerException(result);
/*     */     }
/*     */ 
/* 259 */     this._templateRecords.add(templateRecord);
/*     */   }
/*     */ 
/*     */   public void removeReference(int index)
/*     */     throws FingerException
/*     */   {
/* 269 */     int result = nRemoveReferenceAt(this._handle, index);
/*     */ 
/* 271 */     if (result != 0) {
/* 272 */       throw new FingerException(result);
/*     */     }
/*     */ 
/* 275 */     this._templateRecords.remove(index);
/*     */   }
/*     */ 
/*     */   public void removeAllReferences()
/*     */     throws FingerException
/*     */   {
/* 284 */     int result = nRemoveAllReferences(this._handle);
/*     */ 
/* 286 */     if (result != 0) {
/* 287 */       throw new FingerException(result);
/*     */     }
/*     */ 
/* 290 */     this._templateRecords.clear();
/*     */   }
/*     */ 
/*     */   public FingerMatchCandidateList search(FingerTemplateRecord probe, int maxCandidates)
/*     */     throws FingerException
/*     */   {
/* 303 */     if (maxCandidates < 0) {
/* 304 */       throw new FingerException(-11);
/*     */     }
/* 306 */     long[] handle = new long[1];
/* 307 */     int result = nSearch(this._handle, probe.getHandle(), handle, maxCandidates);
/*     */ 
/* 309 */     if (result != 0) {
/* 310 */       throw new FingerException(result);
/*     */     }
/* 312 */     return new FingerMatchCandidateList(handle[0]);
/*     */   }
/*     */ 
/*     */   public FingerMatchCandidateList search(FingerTemplateRecord probe, int startIndex, int maxCandidates)
/*     */     throws FingerException
/*     */   {
/* 328 */     return search(probe, startIndex, this._templateRecords.size() - startIndex, maxCandidates);
/*     */   }
/*     */ 
/*     */   public FingerMatchCandidateList search(FingerTemplateRecord probe, int startIndex, int count, int maxCandidates)
/*     */     throws FingerException
/*     */   {
/* 346 */     if (maxCandidates < 0) {
/* 347 */       throw new FingerException(-11);
/*     */     }
/* 349 */     long[] handle = new long[1];
/* 350 */     int result = nSearchIndex(this._handle, probe.getHandle(), startIndex, count, handle, maxCandidates);
/*     */ 
/* 352 */     if (result != 0) {
/* 353 */       throw new FingerException(result);
/*     */     }
/* 355 */     return new FingerMatchCandidateList(handle[0]);
/*     */   }
/*     */ 
/*     */   public FingerMatchCandidateList search(FingerTemplateRecord probe, int[] indexArray, int maxCandidates)
/*     */     throws FingerException
/*     */   {
/* 371 */     if (maxCandidates < 0) {
/* 372 */       throw new FingerException(-11);
/*     */     }
/* 374 */     long[] handle = new long[1];
/* 375 */     int result = nSearchList(this._handle, probe.getHandle(), indexArray, handle, maxCandidates);
/*     */ 
/* 377 */     if (result != 0) {
/* 378 */       throw new FingerException(result);
/*     */     }
/* 380 */     return new FingerMatchCandidateList(handle[0]);
/*     */   }
/*     */ 
/*     */   private static final native int nInitialize(long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nDispose(long paramLong);
/*     */ 
/*     */   private static final native int nGetMaximumRotation(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nSetMaximumRotation(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nGetThreshold(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nSetThreshold(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nGetUsedCores(long paramLong, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nSetUsedCores(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nGetFingerCount(long paramLong);
/*     */ 
/*     */   private static final native int nCompareTemplates(long paramLong1, long paramLong2, long paramLong3, int[] paramArrayOfInt);
/*     */ 
/*     */   private static final native int nCompareRecords(long paramLong1, long paramLong2, long paramLong3, long[] paramArrayOfLong);
/*     */ 
/*     */   private static final native int nSearch(long paramLong1, long paramLong2, long[] paramArrayOfLong, int paramInt);
/*     */ 
/*     */   private static final native int nSearchIndex(long paramLong1, long paramLong2, int paramInt1, int paramInt2, long[] paramArrayOfLong, int paramInt3);
/*     */ 
/*     */   private static final native int nSearchList(long paramLong1, long paramLong2, int[] paramArrayOfInt, long[] paramArrayOfLong, int paramInt);
/*     */ 
/*     */   private static final native int nAddReference(long paramLong1, long paramLong2);
/*     */ 
/*     */   private static final native int nRemoveReferenceAt(long paramLong, int paramInt);
/*     */ 
/*     */   private static final native int nRemoveAllReferences(long paramLong);
/*     */ }

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerMatch
 * JD-Core Version:    0.6.2
 */