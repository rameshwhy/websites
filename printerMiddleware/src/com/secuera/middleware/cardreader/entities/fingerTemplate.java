package com.secuera.middleware.cardreader.entities;

import java.awt.image.BufferedImage;

public class fingerTemplate {
	byte[] fingerPrint;
	BufferedImage fingerImage;
	
	
	public byte[] getFingerPrint() {
		return fingerPrint;
	}
	public void setFingerPrint(byte[] fingerPrint) {
		this.fingerPrint = fingerPrint;
	}
	public BufferedImage getFingerImage() {
		return fingerImage;
	}
	public void setFingerImage(BufferedImage fingerImage) {
		this.fingerImage = fingerImage;
	}
	
}
