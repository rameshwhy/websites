/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

import java.io.Serializable;

/**
 *
 * @author Satish K
 */
public class CredentialInformationBean implements Serializable {
    private ChuidBean chuid;
    private String CIN;
    private String IIN;
    private String CUID;
    private String BAP;
    private String ATR;
    private String cardModel;
    
    public String getBAP() {
        return BAP;
    }

    public void setBAP(String BAP) {
        this.BAP = BAP;
    }

    public String getCIN() {
        return CIN;
    }

    public void setCIN(String CIN) {
        this.CIN = CIN;
    }

    public String getCUID() {
        return CUID;
    }

    public void setCUID(String CUID) {
        this.CUID = CUID;
    }

    public String getIIN() {
        return IIN;
    }

    public void setIIN(String IIN) {
        this.IIN = IIN;
    }

    public ChuidBean getChuid() {
        return chuid;
    }

    public void setChuid(ChuidBean chuid) {
        this.chuid = chuid;
    }

    public String getATR() {
        return ATR;
    }

    public void setATR(String ATR) {
        this.ATR = ATR;
    }

    public String getCardModel() {
        return cardModel;
    }

    public void setCardModel(String cardModel) {
        this.cardModel = cardModel;
    }

   
    
    
}
