/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.beans.piv.FacialBean;
import com.secuera.middleware.beans.piv.FingerPrintBean;
import com.secuera.middleware.beans.piv.PinPolicyBean;
import java.io.IOException;
import javax.smartcardio.*;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.util.Date;

/**
 *
 * @author Harpreet Singh
 */
public class PivUtil implements PivUtilInterface {
    byte[] facialImageDataSecu;
    String End_of_record = "EOR";
    //byte[] key_p = { 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23,0x24, 0x25, 0x26, 0x27 };
    String[] next_record = {"01", "02", "03", "04", "05", "06", "07", "08",
        "09", "0A"};
    String[] insurance_meta = {"01", "02", "03", "04", "05", "06", "07", "08",
        "09", "0A"};
    Integer[] insurance_meta_size = {80, 18, 6, 50, 50, 50, 50, 50, 50, 50};
    private byte[] bert_tag = new byte[2];
    private byte[] num_bytes;
    SendCommand sendCard = new SendCommand();
    CommonUtil util = new CommonUtil();
    GeneralAuth auth = new GeneralAuth();
    ExceptionMessages expMsg = new ExceptionMessages();
    
    
    public boolean verifyLocalPin(CardChannel channel, String data, Integer pinLength) throws MiddleWareException {

        String errorMsg;
        byte[] pinData;
        boolean rtnvalue = false;

        StringBuffer sb = new StringBuffer();
        byte[] command_data = {0x00, 0x20, 0x00, (byte) 0x80};

        // convert local PIN to Hex
        String hexstr = util.stringToHex(data);
        pinData = util.hexStringToByteArray(hexstr, pinLength);

        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 16) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);

        } else {
            hex_len_pin = Integer.toHexString(len_pin);
        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare verify PIN command
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, pinData);
        CommandAPDU VERIFY_PIN_APDU = new CommandAPDU(command_data);

        // send verify PIN command
        try {
            sendCard.sendCommand(channel, VERIFY_PIN_APDU, sb);

            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean changeLocalPin(CardChannel channel, String oldPin, String newPin, Integer pinLength) throws MiddleWareException {
        byte[] oldpinData;
        byte[] newpinData;
        byte[] pinData;
        String errorMsg;
        boolean rtnvalue = false;
        byte[] command_data = {0x00, 0x24, 0x00, (byte) 0x80};
        StringBuffer sb = new StringBuffer();

        //convert old PIN to Hex
        String hexstr = util.stringToHex(oldPin);
        oldpinData = util.hexStringToByteArray(hexstr, pinLength);

        hexstr = "";
        //convert New PIN to Hex
        hexstr = util.stringToHex(newPin);
        newpinData = util.hexStringToByteArray(hexstr, pinLength);

        //Calculate length of Data to be sent.
        pinData = util.combine_data(oldpinData, newpinData);
        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);

        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare verify PIN Change commands
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, oldpinData);
        command_data = util.combine_data(command_data, newpinData);
        CommandAPDU CHANGE_PIN_APDU = new CommandAPDU(command_data);

        System.out.println("Command " + util.arrayToHex(command_data));

        // send PIN Change command
        try {
            sendCard.sendCommand(channel, CHANGE_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean resetLockPin(CardChannel channel, String unblockPin, String newPin, Integer pinLength) throws MiddleWareException {
        boolean rtnvalue = false;
        String errorMsg;
        byte[] unBlockData;
        byte[] newpinData;
        byte[] pinData;
        byte[] command_data = {0x00, 0x2C, 0x00, (byte) 0x80};
        StringBuffer sb = new StringBuffer();

        // convert Unblock PIN to Hex
        String hexstr = util.stringToHex(unblockPin);
        unBlockData = util.hexStringToByteArray(hexstr, pinLength);

        hexstr = "";
        // convert New PIN to Hex
        hexstr = util.stringToHex(newPin);
        newpinData = util.hexStringToByteArray(hexstr, pinLength);

        //Calculate length of Data to be sent.
        pinData = util.combine_data(unBlockData, newpinData);
        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);
        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare Unblock PIN commands
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, unBlockData);
        command_data = util.combine_data(command_data, newpinData);
        CommandAPDU CHANGE_PIN_APDU = new CommandAPDU(command_data);

        // send PIN Change command
        try {
            sendCard.sendCommand(channel, CHANGE_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean verifyGlobalPin(CardChannel channel, String data, Integer pinLength) throws MiddleWareException {
        byte[] pinData;
        String errorMsg;
        boolean rtnvalue = false;
        byte[] command_data = {0x00, 0x20, 0x00, 0x00};
        StringBuffer sb = new StringBuffer();

        // convert Global PIN to Hex
        String hexstr = util.stringToHex(data);
        pinData = util.hexStringToByteArray(hexstr, pinLength);

        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);
        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare verify PIN command
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, pinData);
        CommandAPDU VERIFY_PIN_APDU = new CommandAPDU(command_data);


        // send verify PIN command
        try {

            sendCard.sendCommand(channel, VERIFY_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;
        } catch (CardException e) {

            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean changeGlobalPin(CardChannel channel, String oldPin, String newPin, Integer pinLength) throws MiddleWareException {
        byte[] oldpinData;
        byte[] newpinData;
        byte[] pinData;
        String errorMsg;
        boolean rtnvalue = false;

        byte[] command_data = {0x00, 0x24, 0x00, 0x00};
        StringBuffer sb = new StringBuffer();

        // convert old PIN to Hex
        String hexstr = util.stringToHex(oldPin);
        oldpinData = util.hexStringToByteArray(hexstr, pinLength);

        hexstr = "";
        // convert New PIN to Hex
        hexstr = util.stringToHex(newPin);
        newpinData = util.hexStringToByteArray(hexstr, pinLength);

        //Calculate length of Data to be sent.
        pinData = util.combine_data(oldpinData, newpinData);
        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);

        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare verify PIN Change commands
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, oldpinData);
        command_data = util.combine_data(command_data, newpinData);
        CommandAPDU CHANGE_PIN_APDU = new CommandAPDU(command_data);

        // send PIN Change command
        try {
            sendCard.sendCommand(channel, CHANGE_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public boolean changePUK(CardChannel channel, String oldPUK, String newPUK, Integer pukLength) throws MiddleWareException {
        byte[] oldpinData;
        byte[] newpinData;
        byte[] pinData;
        String errorMsg;
        boolean rtnvalue;
        byte[] command_data = {0x00, 0x24, 0x00, (byte) 0x81};
        StringBuffer sb = new StringBuffer();

        // convert old PUK PIN to Hex
        String hexstr = util.stringToHex(oldPUK);
        oldpinData = util.hexStringToByteArray(hexstr, pukLength);

        hexstr = "";
        // convert New PUK PIN to Hex
        hexstr = util.stringToHex(newPUK);
        newpinData = util.hexStringToByteArray(hexstr, pukLength);

        //Calculate length of Data to be sent.
        pinData = util.combine_data(oldpinData, newpinData);
        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);

        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare Change PUK commands
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, oldpinData);
        command_data = util.combine_data(command_data, newpinData);

        System.out.println(util.arrayToHex(command_data));

        CommandAPDU CHANGE_PIN_APDU = new CommandAPDU(command_data);


        // send Change PUK command
        try {

            sendCard.sendCommand(channel, CHANGE_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    /*
    
    public static String convertFascn(String binValue) {
    String retValue="";
    
    if (binValue.matches("00001")) {
    retValue = "0";
    } else if (binValue.matches("10000")) {
    retValue = "1";
    } else if (binValue.matches("01000")) {
    retValue = "2";
    } else if (binValue.matches("11001")){
    retValue = "3";
    } else if (binValue.matches("00100")) {
    retValue = "4";
    } else if (binValue.matches("10101")) {
    retValue = "5";			
    } else if (binValue.matches("01101")) {
    retValue = "6";			
    } else if (binValue.matches("11100")) {
    retValue = "7";			
    } else if (binValue.matches("00010")) {
    retValue = "8";			
    } else if (binValue.matches("10011")) {
    retValue = "9";	
    }
    return retValue;
    
    }
    
    public static String getFASCN(String infascn) {
    // d4e739da739ced39ce739da1685a1082108ce73984119257fc
    //String hex = "d4e739da739ced39ce739da1685a1082108ce73984119257fc";
    String bin = "";
    String binFragment = "";
    int iHex;
    infascn = infascn.trim();
    infascn = infascn.replaceFirst("0x", "");
    
    for (int i = 0; i < infascn.length(); i++) {
    iHex = Integer.parseInt("" + infascn.charAt(i), 16);
    binFragment = Integer.toBinaryString(iHex);
    
    while (binFragment.length() < 4) {
    binFragment = "0" + binFragment;
    }
    bin += binFragment;
    }
    String miscfascn = "";
    
    String fascn = "";
    while (bin.length()> 0) {
    miscfascn = bin.substring(0,5);
    bin=bin.substring(5);
    
    fascn = fascn+convertFascn(miscfascn);
    
    }
    return fascn.substring(0,fascn.length()-1);
    }
     */
    /*
    public ChuidBean readChuid(CardChannel channel) throws MiddleWareException {
    byte[] res = null;
    String returnValue;
    String errorMsg;
    ChuidBean chuid;
    boolean result;
    
    StringBuffer sb = new StringBuffer();
    byte[] apdu_chuid = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05, 0x5C,
    0x03, (byte) 0x5F, (byte) 0xC1, 0x02}; // BERT-TLV CHUID tag
    
    try {
    CommandAPDU CHUID_APDU = new CommandAPDU(apdu_chuid);
    
    res = sendCard.sendCommand(channel, CHUID_APDU, sb);
    
    //raise error if unable to Read CHUID
    if (!sb.toString().equals("9000")) {
    errorMsg = expMsg.GetErrorMsg(sb.toString(), "CHUID");
    throw new MiddleWareException(errorMsg);
    }
    if (res == null || res.length == 0) {
    chuid = new ChuidBean();
    
    } else {
    //convert byte array to string & remove parity bits
    returnValue = util.arrayToHex(res);
    System.out.println(returnValue);
    
    if (returnValue.substring(2, 4).equals("82")) {
    returnValue = returnValue.substring(16);
    } else if (returnValue.substring(2, 4).equals("81")) {
    returnValue = returnValue.substring(12);
    } else {
    returnValue = returnValue.substring(10);
    }
    //53 82  09 ba ee02 09b6               
    //bert tag = 53   length type = 82   lenght = 09 ba  chuid tag= ee02   length after chuid tag 09b6
    
    //parse CHUID
    chuid = parseChuid(returnValue);
    
    //Verify Signature
    ParseAsmObject parseASm = new ParseAsmObject();
    result = parseASm.parseChuidObj(returnValue);
    
    chuid.setSignatureVerified(result);
    }
    return chuid;
    
    } catch (CardException e) {
    throw new MiddleWareException(e.getMessage());
    }
    }
    
     */
    @Override
    public byte[] getFASCN(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String returnValue;
        String errorMsg;
        byte[] FASCN = null;


        StringBuffer sb = new StringBuffer();
        byte[] apdu_chuid = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05, 0x5C,
            0x03, (byte) 0x5F, (byte) 0xC1, 0x02}; // BERT-TLV CHUID tag



        try {
            CommandAPDU CHUID_APDU = new CommandAPDU(apdu_chuid);

            res = sendCard.sendCommand(channel, CHUID_APDU, sb);

            //raise error if unable to Read FASCN
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "FASCN");
                throw new MiddleWareException(errorMsg);
            }
            if (res == null || res.length == 0) {
                FASCN = null;

            } else {
                //convert byte array to string & remove parity bits
                returnValue = util.arrayToHex(res);

                FASCN = util.hex1StringToByteArray(returnValue.substring(returnValue.indexOf("3019") + 4, returnValue.indexOf("3019") + 54));


                //FASCN = util.hex1StringToByteArray(returnValue.substring(4, 54));


            }
            return FASCN;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    public FacialBean readFacialinfo(CardChannel channel) throws MiddleWareException {
        // Send : 00 CB 3F FF 00 00 05
        // 5C 03 5F C1 08
        // Recv :
        // 53 82 19 94 BC 82 19 8E 03 0D 00 00 16 CA 02 6C 00 1B 05 01 14 07 04
        // 13 0B 16 21 5A 14 07
        // 04 13 0B 16 21 5A 14 0B 04 13 0B 16 21 5A 00 00 02 20 FE 4E 49 53 54
        // 20 43 72 65 61 74 6F
        // 72 00 00 00 00 00 00 D4 E7 39 DA 73 9C ED 39 CE 73 9D A1 68 5A 08 C9
        // 2A DE 0A 61 84 E7 39
        // C3 E2 00 00 00 00 // 96 bytes till this point
        // 46 41 43 00 30 31 30 00 00 00 16 CA 00 01 00 00 16
        // BC 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 01 01 00 01 00
        // 01 02 00 00
        //byte[] jpegimg_start = { (byte) 0xFF, (byte) 0xD8 };
        //byte[] jpegimg_end = { (byte) 0xFF, (byte) 0xD9 };
        // 78 bytes CBEFF Header.

        FacialBean facialBean = new FacialBean();
        String str_facialInfo;

        try {
            str_facialInfo = getFacialinfo(channel);

        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());

        }
        if (str_facialInfo == null || str_facialInfo.length() == 0) {

            facialBean.setSignatureVerified(false);
            return facialBean;
        } else {

            ManageCHUID chuid = new ManageCHUID();
            String strChuid = chuid.getChuid(channel);

            verifyFacialSignature verifyFacial = new verifyFacialSignature();

            
                facialBean = verifyFacial.verifySig(str_facialInfo, strChuid);
            

            return facialBean;
        }
    }

    @Override
    public boolean writeFacialinfo(CardChannel channel, byte[] adminKey, byte[] diversifiedAdminKey, byte[] facialData,
            String str_hex_creation_dt, String str_hex_exp_dt, String valid_from_date, String str_creator, String str_fascn, Date dt_signingtime, byte[] cert,
            PrivateKey privatekey, String algoType) throws MiddleWareException {


        boolean rtnvalue = true;
        StringBuffer sb = new StringBuffer();
        byte[] facial_cont_data = null;
        //byte[] facialData = util.hex1ToByteArray(facial_data);
        byte[] creator = util.hex1ToByteArray(util.stringToHex(str_creator));
        byte[] error_tag = {(byte) 0xFE, 0x00};
        //byte[] image_tag = {(byte) 0xBC};

        GenFacCbeffHeader cbeff = new GenFacCbeffHeader();
        //facial_cont_data = facialData;
        try {
            //facial_cont_data = util.hex1ToByteArray(       "ffd8ffe000104a46494600010101004b004b0000ffdb0043000b08080a08070b0a090a0d0c0b0d111c12110f0f1122191a141c29242b2a282427272d3240372d303d302727384c393d43454849482b364f554e465440474845ffdb0043010c0d0d110f1121121221452e272e4545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545ffc000110800f000bb03012200021101031101ffc4001b00000105010100000000000000000000000500010304060207ffc4003d10000103020402070507030403010000000100020304110512213141511322617191a1b106325281c114232442d1e1f03362f1153472b26392a2c2ffc4001801000301010000000000000000000000000001020304ffc4001f1101010003010101010101010000000000010002112131410312325161ffda000c03010002110311003f00f54492491124924911249249112492514b551c43ac754452d9242e4c66269b0d7b940fc689f72f7ed012722662c6afe0917340b9216567c6ea03f290403c40d170fc5e50d191ce73b804bfa27fc36b03da74beaba59b8713a97b1ae790d279957e0c4ed612016e63541910e2915497114ac99b761042ed55324924c889264e9222649249112492491174924922249d24911324e70636e5348f6c6c2e71b5907aa9e4ab7964672b38952ba986e92ab14bb8c700cce43a5e95e499a5ca3881c13b9ec81a6381a5cffcc4e97f9f041ababe3a7febc833ec2269b8f2dd66b6a15f6c94b9acc8cc87e23eefe8bb35300397aa0f636e3e4b3df6aaba970018f89a76019a91fcee53c74354f6e95320fedb0f3d0f9a93756a2550e84ead703ceda2a66b5ad91b1c2d32389b00d1fcb2af2d25645197090b9bc6ed363e8b8378628641d4cede1bee6fe26eaa351d8e3ab958008d96e37365d189ec1f78d0dff89d90ea4ac918d0183377969bf9abbfea6db86cf1965f8db4497514d0d4c913f347207008dd1e26c9ecc79b49eab3ce0d7fde424381e5a14c1e6e0b8e570d9db78a0c9a5c46d8a484e19897483a29b47b74ef4541bad877649a752493a64e524c9d2444c9274911749274911327e092a389d50a7a7b03673b6497501ba9d7d599e6104474e2875454861e86276def155ea2ac42d2c1abdc333adb8e43bcaa41af95fd1b0f59def11e8b35ddb05dc93c9202c889b0dde39f6254f83090f4b95ce7712d1af8d95e884346c1663649ad6bb85c37b82a35b362d21cd1621d0b47011803bae6ea793efcac4b475913725346dc9f097004f6ebb9426a24ae81d967a6980f88589f00a39318afa66e59e56cbda7fc5bc958a7c7455b7a29acd900d0db43d96fe7621d351ba87fa8bcf57a4241e07878abaf6896884846a0d8282b723e4ce1a386dc15c7300a48a32f0d0ed4922fe49136a0cc52ae9c5a16b641c9cf20fe8add3fb410c8447885249013b3ac0b7bee34b77ab4eacc2a89ad64913eaa4e4d6dff00609c625864b1d9d874d0b0fe7681a7814e99119074f46f6b99b90d3b778e0ac473c754ceb0cb25f29bf3557ec6d630d46132891a352cd88e770ab365e91ae9e9ac1edf7e3fe7f025a8f6221e6190349caf69ea9e5d9dcb4d86d68aa8b2b8fdeb770b24c9d95b0d8df30e7ba9b0dac7d3d400e24399ff00d04f174cb2396dae928a2904b131e36234528370b7b09264e92226492491176924971444c4db5592c67101f697b8eac875b733b01f32b47884fd0533dc0eabcf713ab26611335909cc4769d07f3b5466fcb4c0fb49d39004af377125ce2389dac3bb5f24ab7148f07a1e965feb49b378f77f3eaa20d1f696420f5210331e646a4f7a0d5b2453d53ebaaec5a348a33ee8038959169529b11c4b1090b8cce899c18cfd764d1cd5b0bbfab5039ea0dfc2e13bab5f5566b61bb7b4903e43457e928cbc0bb1a3fe200428540b57b4f381d771e7716bf78d3c959a5a591ae0eb108a41401b6202beca6e6a177580435b13c104dff453cadced68b9000b0ec57cc22cb9fb383b846e7a82cc5b1480f46e793bd8e8a17d561f3380aaa72c76d9e395d71f428bd4d28703a2055b44f65dcc710470be8531d49c46966a6adc3f2d6e0f54ea88dba98c9eb5872e07d55aa4c522c620388510e8eb61febc234ce389b21986e2021a811c8f746e71b1cdee93cafc0a7c5a9a4c2ea198fe1e3469b54c4d1a3813a923d7c559db3448b19db4d2b268ee209b8fc27978a2123cc81b3464661cb9f228410c9db247110ea7a98fed14faedf137c7d54b86543a485f0b8f5ad61ff21b1f9a97d8f4b6b8056895ae809d86665f97108e0584c32b1d04f99ba6521c0766c42dc45209181edd4385c2db07658e66a9124925744932749117492499c6c094440bda0a8c90e5bfbc560a27f4b884f3bfac184907b858799f25a8f69ea089001f918e79eeb2c9c47a0c29ce23ad210dbf9ac727b6f81cad46fc9492ccede42403f2dfcbcd666a2a0cd505b7d01b7f847f159451e1b0b5c6d9632e777ff02ca610d754d41776a9f9bacebab4141460dafbad0d2d335a068a9d1c395a344561000d547adaeb453b18d0177a5b651e6e5b2705396aec01c92ca2eb9bea917222691808d50eaba70f6957dc5579756d926658ec5290c777345c8dbf445b01af654c4fa5a8d5a5b9483c74fd13e2106661d166a29dd876241fb02754c7e4b3366e37035d49054d1b8f5e826124679c6ed0dbb352a727a0c44399a325b39beaa5ab6324aba7aa6db254c66179e1623427bb654e37b9f87c2f20f490bcc6fef07f4baa7b644699208e73caf71dc42d960751d251065eee8f43f4587be7a76c83768b1ee5a4f66ea3af949b748dd7bc2783a69ccd96a924b700a65bd84e924991176b894d9879aed455072c44a22c17b4ee2f9aa2dbb5a1a3c50792cd869a3e0dbb8f6db4ba258bcbd24f29f8e4007cae50a71cf3c4de4d007ceffa85cefb746272a3ed64a451c83621ac6f98baa9ecd53d9a090a4f6a1d9e297b4b47a157bd9b80ba99a4043fe6bc4eeed0431e82cae4715f75c44ccb6bab01d65215add745a2631d923359374e08dd54bb705b6d92ca4ae4c973a2e83c2539b2951be3d159bb48bdc289e421208555457074591c6a9f52eb6c56e658f38dd00c628ef138d949c6af4a2c1e7fb76032c57fbda7208f96a3d574db74988460e8fcb50d1de013ea50ef662530e2d3d39da68ef6ed06de851069c9885313b3a37447b6c4803c95b629daf50bc4946e0ef7b4f5b22d82ce629d9ae8d7791d103a2bc7d343c4023cee15ec3e53d3804fbc2ff3ff00291c61365e931baed1c976aa61b289a918eec5682e81d9733c649d249395d2ad5eecb4af3c82b2a8e2aecb432149f267b79e6252de565c6ae90dbc155200ad70f800fdbd14d5b633c23b49fe78a8813f6e941d9c5a3c160dd240f1c3999203f18ffa5d18c2257414313610dbdb7282e3674939978ffa90a6c2b116c744d2e3a808f9563ec76a2aeb1972d703d96555bed14d13f2ccd4365f6860072dee790dcaa671ba599c73b1c073ca983ff26a1f6d5c58cc751ee9569b517e2b2d4b24121cd03c144e2a83cd4b5119e9adb6c9fed1da87b2524267ca5a96ead578d5869d4a638942c1773ec834f2b9d700d90ba8735b7334c00ef414b699d8d404d9bd62a09ea4d4c66d1e8b3d4d57451bb499a4ef724688a478846e88f46e0426923bf6114eefb2fb4d4af02d99c5a4778289d71304ed7dac62a83ff607ff00d20559536c5a9a4076947a84731b70cd38e376bbc47ec9b4a76bda47895c681d61e5fcf052c2ec954cb9fcc41f1ff2aaba50f6d1cd6bb640038f6ff0aee7263aa27fb83bc6dfba9657a07b3d3668248f8b0e9dc51a0b2bececf6aab5f4905be6b5238ae8c1d97366699d2492554dd21f8c9fc03f9222866366d40f0965e4cf6f39ab75eb18d1c1bf551c64babf29d6e49f309e6b9c409e02c3c8a6a4ebd63e41f9580789fd973dd303c61b79076961fa21b4d4d2be32c17b762318ab2c413c5c00ee56f0aa40f8ae06a98e8af5f6a186e16d8f387801ce1a38ef74266a291953082d6e58cf5efa11dfcd6e0d2641ab7e6abba9da5e3aa0db6240242d0cf96797e7b7904651082804beec998960d8db868aed3e792363ac412893698388ea827b82b11d112f1750bbb4c4fe4d4d4b4ce7b41b2e2b2131dd1fa4a60d8b655710a61a8b294a86c8d5cae8e3242a35b879e8617bc662f24b8fcae005a19a8c116732e0281f035b1f445a5cc76e09bdbb956285390e468b21414a2a32de121e5f6735c3813b5bb94d5d472504f6a77b83395f45a68a9a3a639e365c9e2e25c4772af5540677175b42adc8f96587e49ed949242fa889c776bda7cd6a316717cc3ff242d23bc5c7d504aea2e81f7b71456be4d681d7f7a123c35fa2cf2776808f6b7452897038afaf466c7b00576a88786bf725be3a7f954308cbd155407604bbe5727f45605fecec6936201683e8a58d5a3c12a0896175f6362b7ac399a0f35e65844bd561075e3d8bd1a8a4e969237762d7f37e587ea77759493a65ad95da138f3b2d03ee8aa0bed13bf02ebeca5f19e3ede741e5f5921e4e3f44d44ec8c99c36000efd47ea5346e02598f7a6a3d69a6fee36eeb11fa85817555317176477d81b7cee4fa1088e0ae1d0b7b50dc54e7a499c3810ef41f4567059418997295ae272d386b1cd51ba943b6d1289edb0d54c1c392a23546ca66305f8aec37ada2e6492c2e57544e1349a70453afb12a7b88ec554aed5e8a32201aaad5c1d52534e523d839687685569295cd3766dc95973c364b710a40e07552368549905cf59a015d4b1b430ab2ed7554ea09b1b24cc3765f1d681a850d5bf350e1b21d9a4b4fcd4d8e691925529dd9fd9e848f7849faa259f22585485b531bb83da5bf31fe110ca046f6f163891dc8461b27df0b1d0bee0f2b8b8f307c519a901a64cba6602dd9a1fa84345261afc9291c095e8981cd9e9434ee179bd2919da76bea3f9e2b73ecf4a735b9aac1d365fa1b2d2249037092e8b9eeb8205ed27fb028e13a2cffb4eeb50bedc0592cbcab1f4bcf037ad2bb826a477e149e21ae27bee3f9f25c03962941dedf55c53b8f4120e25baf81fd173dd57159d6a499bc721f21fb287039af1b14d39cd1b8f02c250bc126cb76f1694be578bdd5b685fa05603d0ea79439a15a0eb8b20a995439ce6903656a09a2a48a3b1d6c2fdeb98a2045dc10bc522998e062371c884e5c5d5a78b146387bcb99f108cb08ccb250cf235a2e2c529aaa6360c009ed3b236cff923352d6b99d233debeab963cd85d57c38cb3477976ee56dcc0d294874ea673f454e67e85589069a2a53900149ab766f1f92d110a9875f001fdaf07d13fb433700a38ee303b7026fe855870b3c9dbaace18ed231cb2f911fbad1541bc0c27e117f0596c31e418bb5f6f32b5530062603b10a5f654701eb01f0e8b61804a448de7a7a2c5447ef9a389dfbff965aac0651d283c93c5ed199cb72d3a2e94519d029174dcb39dd677daa75a8bbddafc9688f1ee595f6b9e4534439e62a72f2ac3db06f3d49081be81474fa44fb8dd83ea0a46ff00667df60eb8b250eb476dc8b8778dd605d4d0b9c4c597930ffd903c31f96a9e3b5182ebc727631c7cca014afc95d2763b54c364f7a4b694afea857e2945c927642e8de1cc0a59ba40d222dca8b463314f98e87452c918906ab2065c520b86b9879122c57267c65c3359d2341d435cac199f9efb1ea8a7224b00a486974eb040e1c6a781b95c48e4241a851498fd587ddb9de7fb5ba7d12d54e1ff00b6b98044dd344ce9010b330fb4b291f8881e1bcf2a9a9f1a6cf259ad781c0916ba48d0e29197c80028755496692a57499f51b1542ba4b46425164f1a973cf6e175626062c21a0ee7f6542b1dd2d686f0ba258969450b4f1b1f32b44e163bdab7347d414fdaefa95ac9fdd603b5880b294fefc0d3b83f5fd96a2a1e3aade24e9e2143ed5560e21e5e74cdfe169b05765934ec3e2b34e01cf16f7403ea8ee0f2112341e2118fb4e5e5e834cecd137b94ea950bef0b6fcb557974972bedd3cd9ae2b21ed8bbab10e01856b9fee1ed58df6ade1d2b41fcad37539f95e1ed8773ff000a728dc9f9ae61786d23b8973b31efd45bc97158ecb48d0dd8937ee2764afd1d26db5be7a7eeb1f974511d0480f0195659d218ebde78179f55a594e564b73bb837cd65eb05e57bb8dd5e053fa3ad25a9c32aae1a2e8ec6ecd6586c26bb2c818f363c0f35b3a399b23010a7234da639191ca7986669696e9c3b1566b9f11390e878226c6e70a39280bf56e8548b686490e74d73d61e5bae854756cd6ebdca67e1d207712ba6d1c8382adb5399556d3f4aeccff0538a78db6b340b29c53968bb947268a5774392d1be4118b041f13abcb1bb5d55caa943412b2b8ad597b9cd69fd93c4db4e4e8dd5e9cf4d5a09dae8ce2c2cf81874cb6f0007d50ec1a20f998e70d09f156b1193a6c4edbe507e572adf75658ff9dffda5a219b1081a75b589f55a098fdfbbb1a2dffb14030f23fd559ccfe88e4ce06b2468d801fbacdae90b6c5bdb7f54470e796b81e20aa8d6e6919756e99a6398b48b6a824dbdc3df9a9daee08a817082e10ecd4847c26c8cb1dd50ba31f2e5cbdba9346ac2fb4ae1d34aee4dfa2dcca6cd583f6905dd37223f64b3f2afcfdb1557feca33fdc7d53bcde91807324f7d815cd5dfec71b7b7d4a57fc28ec27c163f2e8abd4ff4cdb8bcdbc1676a0662fb0d568aa34000e0093e07f5409e01ce7b755785399b21a090f041b11b2d1e0b8ce5708e736238f359e91b6794e2f65a6409638e4e2dea54b501c1a78144593b42f34c331c9a96d1c84b98389e0b4d4d8c45334657f8e85628975639096a0c8d7262f60082b6bb4f7c78ae5d5c3e353baf5119a40421b553b5a0dcaad518ab18d373af6207575b254136243501b89b12c40b898e3df9acfcb77bc83cf5ed57a56f9aad1b334dd816b8f2c33db19c222ca0c8e00358db0ef3fc0a02ebcf2c8753757a36f43411378b85dc865cba37bb8b9d60a4ebbabc02b98412ec4d84f1bdfc0a319f357545bf2bade682e126d89c23983e611381f7c426fee254b323d4c06765f9e8afcf1e498b87020843a99dd58ddf0946f12680c8dc0583da4fa20394af63d813f3091bcc0211a6bbaa1667d9e96f3307362d192b6c5e5cf99da69bdc2160fda23984daeee0d03f9dcb7739b30f62f3ec6df99ad07dece5c7c52cfcabf3f6c95659cc03b40f305720fe0838f1252add2e7914ed20d23073711e7fb2cbe5d1579ceafb6e3abe242085ba483b6e8d54690b9c3de2f085b9b62e1c48558f924dc32a1ba87735c30e8acc8dcd1768559a355a9e5ce9a691a354469b821ed3aabf4e54e56bf9fb1389c405238970d5431bb452e658b7450bdaa07b75569fb2acfbdc10990d4e51b93f24d4b0e77345bde3771ec0bb945f43b057b0f88749988d18013e04abde8b24db3d64b95aefec6f9edea7c9538db960b9fcb6f12bbaa717c8d8f77bdd777601aa8deebe563760ebbbb4a41a2374f875998bc409d36f257a27e5af769b93ea50d89d971061e215b6bcfdb9c7913eaa59969695df703b08456aa73253c641f75a3d103a575e2207157ba5fc2917d7652309d8f7b3b381510dcf020ad58bb85f4d579be1352e64e0871d02d2b7149dad037b2d31cf45967f9edb55507a8eee5e758bbb3bde7e1bdfbcaf42ac70642f71e0179bd73f374f7f89566d3f91672a9a4d3923727d13036a48c0db35fc4a92a9a045aeda9f351bcfe0d8de2469df7591e5bd04e3352b4f1e90f90086c9fd67761b7922cf00d2b8fc2e71fa7aa10e3721dcd510d5b2f5cb79aa6e195e42bd2755f71c0faaad3b40909e6b51b1c8fb703757a9caa415984d8a4d587189c6a70aa46e53662b26e92edfb281c576e71b281c74410d14a45fb0fa2250fdc5239f6b97906ddfadbd10dcbd2023b6df2576ae4b42c60d058b95593517bece92627ac459b7e64ebf5f05cb7b39807e4a22ecf9796ea78eda1e05c9b494834ac0a5cff8c75b89519d2a93904541239a96a2d2521b36c1772bacd201515193a3be454b3b3436e6a27f6ef0d75def71e1b2302a5f6e287e0d4e1ceb1d4ad28c3f4d820214bfffd9");
            //facialData = util.hex1StringToByteArray("ffd8ffe000104a46494600010101004b004b0000ffdb0043000b08080a08070b0a090a0d0c0b0d111c12110f0f1122191a141c29242b2a282427272d3240372d303d302727384c393d43454849482b364f554e465440474845ffdb0043010c0d0d110f1121121221452e272e4545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545ffc000110800f000bb03012200021101031101ffc4001b00000105010100000000000000000000000500010304060207ffc4003d10000103020402070507030403010000000100020304110512213141511322617191a1b106325281c114232442d1e1f03362f1153472b26392a2c2ffc4001801000301010000000000000000000000000001020304ffc4001f1101010003010101010101010000000000010002112131410312325161ffda000c03010002110311003f00f54492491124924911249249112492514b551c43ac754452d9242e4c66269b0d7b940fc689f72f7ed012722662c6afe0917340b9216567c6ea03f290403c40d170fc5e50d191ce73b804bfa27fc36b03da74beaba59b8713a97b1ae790d279957e0c4ed612016e63541910e2915497114ac99b761042ed55324924c889264e9222649249112492491174924922249d24911324e70636e5348f6c6c2e71b5907aa9e4ab7964672b38952ba986e92ab14bb8c700cce43a5e95e499a5ca3881c13b9ec81a6381a5cffcc4e97f9f041ababe3a7febc833ec2269b8f2dd66b6a15f6c94b9acc8cc87e23eefe8bb35300397aa0f636e3e4b3df6aaba970018f89a76019a91fcee53c74354f6e95320fedb0f3d0f9a93756a2550e84ead703ceda2a66b5ad91b1c2d32389b00d1fcb2af2d25645197090b9bc6ed363e8b8378628641d4cede1bee6fe26eaa351d8e3ab958008d96e37365d189ec1f78d0dff89d90ea4ac918d0183377969bf9abbfea6db86cf1965f8db4497514d0d4c913f347207008dd1e26c9ecc79b49eab3ce0d7fde424381e5a14c1e6e0b8e570d9db78a0c9a5c46d8a484e19897483a29b47b74ef4541bad877649a752493a64e524c9d2444c9274911749274911327e092a389d50a7a7b03673b6497501ba9d7d599e6104474e2875454861e86276def155ea2ac42d2c1abdc333adb8e43bcaa41af95fd1b0f59def11e8b35ddb05dc93c9202c889b0dde39f6254f83090f4b95ce7712d1af8d95e884346c1663649ad6bb85c37b82a35b362d21cd1621d0b47011803bae6ea793efcac4b475913725346dc9f097004f6ebb9426a24ae81d967a6980f88589f00a39318afa66e59e56cbda7fc5bc958a7c7455b7a29acd900d0db43d96fe7621d351ba87fa8bcf57a4241e07878abaf6896884846a0d8282b723e4ce1a386dc15c7300a48a32f0d0ed4922fe49136a0cc52ae9c5a16b641c9cf20fe8add3fb410c8447885249013b3ac0b7bee34b77ab4eacc2a89ad64913eaa4e4d6dff00609c625864b1d9d874d0b0fe7681a7814e99119074f46f6b99b90d3b778e0ac473c754ceb0cb25f29bf3557ec6d630d46132891a352cd88e770ab365e91ae9e9ac1edf7e3fe7f025a8f6221e6190349caf69ea9e5d9dcb4d86d68aa8b2b8fdeb770b24c9d95b0d8df30e7ba9b0dac7d3d400e24399ff00d04f174cb2396dae928a2904b131e36234528370b7b09264e92226492491176924971444c4db5592c67101f697b8eac875b733b01f32b47884fd0533dc0eabcf713ab26611335909cc4769d07f3b5466fcb4c0fb49d39004af377125ce2389dac3bb5f24ab7148f07a1e965feb49b378f77f3eaa20d1f696420f5210331e646a4f7a0d5b2453d53ebaaec5a348a33ee8038959169529b11c4b1090b8cce899c18cfd764d1cd5b0bbfab5039ea0dfc2e13bab5f5566b61bb7b4903e43457e928cbc0bb1a3fe200428540b57b4f381d771e7716bf78d3c959a5a591ae0eb108a41401b6202beca6e6a177580435b13c104dff453cadced68b9000b0ec57cc22cb9fb383b846e7a82cc5b1480f46e793bd8e8a17d561f3380aaa72c76d9e395d71f428bd4d28703a2055b44f65dcc710470be8531d49c46966a6adc3f2d6e0f54ea88dba98c9eb5872e07d55aa4c522c620388510e8eb61febc234ce389b21986e2021a811c8f746e71b1cdee93cafc0a7c5a9a4c2ea198fe1e3469b54c4d1a3813a923d7c559db3448b19db4d2b268ee209b8fc27978a2123cc81b3464661cb9f228410c9db247110ea7a98fed14faedf137c7d54b86543a485f0b8f5ad61ff21b1f9a97d8f4b6b8056895ae809d86665f97108e0584c32b1d04f99ba6521c0766c42dc45209181edd4385c2db07658e66a9124925744932749117492499c6c094440bda0a8c90e5bfbc560a27f4b884f3bfac184907b858799f25a8f69ea089001f918e79eeb2c9c47a0c29ce23ad210dbf9ac727b6f81cad46fc9492ccede42403f2dfcbcd666a2a0cd505b7d01b7f847f159451e1b0b5c6d9632e777ff02ca610d754d41776a9f9bacebab4141460dafbad0d2d335a068a9d1c395a344561000d547adaeb453b18d0177a5b651e6e5b2705396aec01c92ca2eb9bea917222691808d50eaba70f6957dc5579756d926658ec5290c777345c8dbf445b01af654c4fa5a8d5a5b9483c74fd13e2106661d166a29dd876241fb02754c7e4b3366e37035d49054d1b8f5e826124679c6ed0dbb352a727a0c44399a325b39beaa5ab6324aba7aa6db254c66179e1623427bb654e37b9f87c2f20f490bcc6fef07f4baa7b644699208e73caf71dc42d960751d251065eee8f43f4587be7a76c83768b1ee5a4f66ea3af949b748dd7bc2783a69ccd96a924b700a65bd84e924991176b894d9879aed455072c44a22c17b4ee2f9aa2dbb5a1a3c50792cd869a3e0dbb8f6db4ba258bcbd24f29f8e4007cae50a71cf3c4de4d007ceffa85cefb746272a3ed64a451c83621ac6f98baa9ecd53d9a090a4f6a1d9e297b4b47a157bd9b80ba99a4043fe6bc4eeed0431e82cae4715f75c44ccb6bab01d65215add745a2631d923359374e08dd54bb705b6d92ca4ae4c973a2e83c2539b2951be3d159bb48bdc289e421208555457074591c6a9f52eb6c56e658f38dd00c628ef138d949c6af4a2c1e7fb76032c57fbda7208f96a3d574db74988460e8fcb50d1de013ea50ef662530e2d3d39da68ef6ed06de851069c9885313b3a37447b6c4803c95b629daf50bc4946e0ef7b4f5b22d82ce629d9ae8d7791d103a2bc7d343c4023cee15ec3e53d3804fbc2ff3ff00291c61365e931baed1c976aa61b289a918eec5682e81d9733c649d249395d2ad5eecb4af3c82b2a8e2aecb432149f267b79e6252de565c6ae90dbc155200ad70f800fdbd14d5b633c23b49fe78a8813f6e941d9c5a3c160dd240f1c3999203f18ffa5d18c2257414313610dbdb7282e3674939978ffa90a6c2b116c744d2e3a808f9563ec76a2aeb1972d703d96555bed14d13f2ccd4365f6860072dee790dcaa671ba599c73b1c073ca983ff26a1f6d5c58cc751ee9569b517e2b2d4b24121cd03c144e2a83cd4b5119e9adb6c9fed1da87b2524267ca5a96ead578d5869d4a638942c1773ec834f2b9d700d90ba8735b7334c00ef414b699d8d404d9bd62a09ea4d4c66d1e8b3d4d57451bb499a4ef724688a478846e88f46e0426923bf6114eefb2fb4d4af02d99c5a4778289d71304ed7dac62a83ff607ff00d20559536c5a9a4076947a84731b70cd38e376bbc47ec9b4a76bda47895c681d61e5fcf052c2ec954cb9fcc41f1ff2aaba50f6d1cd6bb640038f6ff0aee7263aa27fb83bc6dfba9657a07b3d3668248f8b0e9dc51a0b2bececf6aab5f4905be6b5238ae8c1d97366699d2492554dd21f8c9fc03f9222866366d40f0965e4cf6f39ab75eb18d1c1bf551c64babf29d6e49f309e6b9c409e02c3c8a6a4ebd63e41f9580789fd973dd303c61b79076961fa21b4d4d2be32c17b762318ab2c413c5c00ee56f0aa40f8ae06a98e8af5f6a186e16d8f387801ce1a38ef74266a291953082d6e58cf5efa11dfcd6e0d2641ab7e6abba9da5e3aa0db6240242d0cf96797e7b7904651082804beec998960d8db868aed3e792363ac412893698388ea827b82b11d112f1750bbb4c4fe4d4d4b4ce7b41b2e2b2131dd1fa4a60d8b655710a61a8b294a86c8d5cae8e3242a35b879e8617bc662f24b8fcae005a19a8c116732e0281f035b1f445a5cc76e09bdbb956285390e468b21414a2a32de121e5f6735c3813b5bb94d5d472504f6a77b83395f45a68a9a3a639e365c9e2e25c4772af5540677175b42adc8f96587e49ed949242fa889c776bda7cd6a316717cc3ff242d23bc5c7d504aea2e81f7b71456be4d681d7f7a123c35fa2cf2776808f6b7452897038afaf466c7b00576a88786bf725be3a7f954308cbd155407604bbe5727f45605fecec6936201683e8a58d5a3c12a0896175f6362b7ac399a0f35e65844bd561075e3d8bd1a8a4e969237762d7f37e587ea77759493a65ad95da138f3b2d03ee8aa0bed13bf02ebeca5f19e3ede741e5f5921e4e3f44d44ec8c99c36000efd47ea5346e02598f7a6a3d69a6fee36eeb11fa85817555317176477d81b7cee4fa1088e0ae1d0b7b50dc54e7a499c3810ef41f4567059418997295ae272d386b1cd51ba943b6d1289edb0d54c1c392a23546ca66305f8aec37ada2e6492c2e57544e1349a70453afb12a7b88ec554aed5e8a32201aaad5c1d52534e523d839687685569295cd3766dc95973c364b710a40e07552368549905cf59a015d4b1b430ab2ed7554ea09b1b24cc3765f1d681a850d5bf350e1b21d9a4b4fcd4d8e691925529dd9fd9e848f7849faa259f22585485b531bb83da5bf31fe110ca046f6f163891dc8461b27df0b1d0bee0f2b8b8f307c519a901a64cba6602dd9a1fa84345261afc9291c095e8981cd9e9434ee179bd2919da76bea3f9e2b73ecf4a735b9aac1d365fa1b2d2249037092e8b9eeb8205ed27fb028e13a2cffb4eeb50bedc0592cbcab1f4bcf037ad2bb826a477e149e21ae27bee3f9f25c03962941dedf55c53b8f4120e25baf81fd173dd57159d6a499bc721f21fb287039af1b14d39cd1b8f02c250bc126cb76f1694be578bdd5b685fa05603d0ea79439a15a0eb8b20a995439ce6903656a09a2a48a3b1d6c2fdeb98a2045dc10bc522998e062371c884e5c5d5a78b146387bcb99f108cb08ccb250cf235a2e2c529aaa6360c009ed3b236cff923352d6b99d233debeab963cd85d57c38cb3477976ee56dcc0d294874ea673f454e67e85589069a2a53900149ab766f1f92d110a9875f001fdaf07d13fb433700a38ee303b7026fe855870b3c9dbaace18ed231cb2f911fbad1541bc0c27e117f0596c31e418bb5f6f32b5530062603b10a5f654701eb01f0e8b61804a448de7a7a2c5447ef9a389dfbff965aac0651d283c93c5ed199cb72d3a2e94519d029174dcb39dd677daa75a8bbddafc9688f1ee595f6b9e4534439e62a72f2ac3db06f3d49081be81474fa44fb8dd83ea0a46ff00667df60eb8b250eb476dc8b8778dd605d4d0b9c4c597930ffd903c31f96a9e3b5182ebc727631c7cca014afc95d2763b54c364f7a4b694afea857e2945c927642e8de1cc0a59ba40d222dca8b463314f98e87452c918906ab2065c520b86b9879122c57267c65c3359d2341d435cac199f9efb1ea8a7224b00a486974eb040e1c6a781b95c48e4241a851498fd587ddb9de7fb5ba7d12d54e1ff00b6b98044dd344ce9010b330fb4b291f8881e1bcf2a9a9f1a6cf259ad781c0916ba48d0e29197c80028755496692a57499f51b1542ba4b46425164f1a973cf6e175626062c21a0ee7f6542b1dd2d686f0ba258969450b4f1b1f32b44e163bdab7347d414fdaefa95ac9fdd603b5880b294fefc0d3b83f5fd96a2a1e3aade24e9e2143ed5560e21e5e74cdfe169b05765934ec3e2b34e01cf16f7403ea8ee0f2112341e2118fb4e5e5e834cecd137b94ea950bef0b6fcb557974972bedd3cd9ae2b21ed8bbab10e01856b9fee1ed58df6ade1d2b41fcad37539f95e1ed8773ff000a728dc9f9ae61786d23b8973b31efd45bc97158ecb48d0dd8937ee2764afd1d26db5be7a7eeb1f974511d0480f0195659d218ebde78179f55a594e564b73bb837cd65eb05e57bb8dd5e053fa3ad25a9c32aae1a2e8ec6ecd6586c26bb2c818f363c0f35b3a399b23010a7234da639191ca7986669696e9c3b1566b9f11390e878226c6e70a39280bf56e8548b686490e74d73d61e5bae854756cd6ebdca67e1d207712ba6d1c8382adb5399556d3f4aeccff0538a78db6b340b29c53968bb947268a5774392d1be4118b041f13abcb1bb5d55caa943412b2b8ad597b9cd69fd93c4db4e4e8dd5e9cf4d5a09dae8ce2c2cf81874cb6f0007d50ec1a20f998e70d09f156b1193a6c4edbe507e572adf75658ff9dffda5a219b1081a75b589f55a098fdfbbb1a2dffb14030f23fd559ccfe88e4ce06b2468d801fbacdae90b6c5bdb7f54470e796b81e20aa8d6e6919756e99a6398b48b6a824dbdc3df9a9daee08a817082e10ecd4847c26c8cb1dd50ba31f2e5cbdba9346ac2fb4ae1d34aee4dfa2dcca6cd583f6905dd37223f64b3f2afcfdb1557feca33fdc7d53bcde91807324f7d815cd5dfec71b7b7d4a57fc28ec27c163f2e8abd4ff4cdb8bcdbc1676a0662fb0d568aa34000e0093e07f5409e01ce7b755785399b21a090f041b11b2d1e0b8ce5708e736238f359e91b6794e2f65a6409638e4e2dea54b501c1a78144593b42f34c331c9a96d1c84b98389e0b4d4d8c45334657f8e85628975639096a0c8d7262f60082b6bb4f7c78ae5d5c3e353baf5119a40421b553b5a0dcaad518ab18d373af6207575b254136243501b89b12c40b898e3df9acfcb77bc83cf5ed57a56f9aad1b334dd816b8f2c33db19c222ca0c8e00358db0ef3fc0a02ebcf2c8753757a36f43411378b85dc865cba37bb8b9d60a4ebbabc02b98412ec4d84f1bdfc0a319f357545bf2bade682e126d89c23983e611381f7c426fee254b323d4c06765f9e8afcf1e498b87020843a99dd58ddf0946f12680c8dc0583da4fa20394af63d813f3091bcc0211a6bbaa1667d9e96f3307362d192b6c5e5cf99da69bdc2160fda23984daeee0d03f9dcb7739b30f62f3ec6df99ad07dece5c7c52cfcabf3f6c95659cc03b40f305720fe0838f1252add2e7914ed20d23073711e7fb2cbe5d1579ceafb6e3abe242085ba483b6e8d54690b9c3de2f085b9b62e1c48558f924dc32a1ba87735c30e8acc8dcd1768559a355a9e5ce9a691a354469b821ed3aabf4e54e56bf9fb1389c405238970d5431bb452e658b7450bdaa07b75569fb2acfbdc10990d4e51b93f24d4b0e77345bde3771ec0bb945f43b057b0f88749988d18013e04abde8b24db3d64b95aefec6f9edea7c9538db960b9fcb6f12bbaa717c8d8f77bdd777601aa8deebe563760ebbbb4a41a2374f875998bc409d36f257a27e5af769b93ea50d89d971061e215b6bcfdb9c7913eaa59969695df703b08456aa73253c641f75a3d103a575e2207157ba5fc2917d7652309d8f7b3b381510dcf020ad58bb85f4d579be1352e64e0871d02d2b7149dad037b2d31cf45967f9edb55507a8eee5e758bbb3bde7e1bdfbcaf42ac70642f71e0179bd73f374f7f89566d3f91672a9a4d3923727d13036a48c0db35fc4a92a9a045aeda9f351bcfe0d8de2469df7591e5bd04e3352b4f1e90f90086c9fd67761b7922cf00d2b8fc2e71fa7aa10e3721dcd510d5b2f5cb79aa6e195e42bd2755f71c0faaad3b40909e6b51b1c8fb703757a9caa415984d8a4d587189c6a70aa46e53662b26e92edfb281c576e71b281c74410d14a45fb0fa2250fdc5239f6b97906ddfadbd10dcbd2023b6df2576ae4b42c60d058b95593517bece92627ac459b7e64ebf5f05cb7b39807e4a22ecf9796ea78eda1e05c9b494834ac0a5cff8c75b89519d2a93904541239a96a2d2521b36c1772bacd201515193a3be454b3b3436e6a27f6ef0d75def71e1b2302a5f6e287e0d4e1ceb1d4ad28c3f4d820214bfffd9");
         
           facial_cont_data = cbeff.getFacialHeader(facialData, str_hex_creation_dt, str_hex_exp_dt, valid_from_date, creator, str_fascn, dt_signingtime, privatekey, cert, algoType);
        } catch (IOException ex) {
           throw new MiddleWareException(ex.getMessage());
        } catch (NoSuchAlgorithmException ex) {
           throw new MiddleWareException(ex.getMessage());
        } catch (InvalidKeyException ex) {
            throw new MiddleWareException(ex.getMessage());
        } catch (NoSuchProviderException ex) {
           throw new MiddleWareException(ex.getMessage());
        }
     

        if (facial_cont_data.length > UcmsMiddlewareConstants.facialImageLength) {
            throw new MiddleWareException("Size of Picture is to big.");
        }


        // Integer len = facial_cont_data.length;

        //System.out.println("Length of facial data " + len);

        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, adminKey);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, diversifiedAdminKey);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        System.out.println("after auth");


        //////////////////////////////////////////////////////	
        byte[] cont_id = {0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x08}; //For Facial Container
        byte[] apdu_facial = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};
        ///////////////////////////////////////////////////////


        //Calculate size of the facial record to be stored, with Image Tag

        byte[] apdu_facial_data = util.combine_data(facial_cont_data, error_tag);
        //populate this variable for security container    
        facialImageDataSecu = apdu_facial_data;
        
        //Now Calculate size of the facial record to be stored
        Integer len_total_bert = apdu_facial_data.length;

        calcBertValue(len_total_bert, true);

        byte[] bert_tag_total = bert_tag;
        byte[] byte_fac_len = num_bytes;

        apdu_facial_data = util.combine_data(byte_fac_len, apdu_facial_data);
        apdu_facial_data = util.combine_data(bert_tag_total, apdu_facial_data);

        
        
        apdu_facial_data = util.combine_data(cont_id, apdu_facial_data);

        Integer len_total_apdu = apdu_facial_data.length;

        calcBertValue(len_total_apdu, false);

        byte[] total_size = num_bytes;
        byte[] zero_size = {0x00};

        if (total_size.length > 1) {
            total_size = util.combine_data(zero_size, total_size);
        }


        apdu_facial = util.combine_data(apdu_facial, total_size);
        apdu_facial = util.combine_data(apdu_facial, apdu_facial_data);


        System.out.println(util.arrayToHex(apdu_facial));

        CommandAPDU READ_APDU = new CommandAPDU(apdu_facial);

        try {

            sendCard.sendCommand(channel, READ_APDU, sb);

            //Raise Error if APDU command is not successful
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();
                errMsg.append("Unable to write Facial Image");
                errMsg.append(" (").append(sb.toString()).append(")");
                throw new MiddleWareException(errMsg.toString());
            }
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        return rtnvalue;
    }

    public String getFacialinfo(CardChannel channel) throws MiddleWareException {
        // Send : 00 CB 3F FF 00 00 05
        // 5C 03 5F C1 08
        // Recv :
        // 53 82 19 94 BC 82 19 8E 03 0D 00 00 16 CA 02 6C 00 1B 05 01 14 07 04
        // 13 0B 16 21 5A 14 07
        // 04 13 0B 16 21 5A 14 0B 04 13 0B 16 21 5A 00 00 02 20 FE 4E 49 53 54
        // 20 43 72 65 61 74 6F
        // 72 00 00 00 00 00 00 D4 E7 39 DA 73 9C ED 39 CE 73 9D A1 68 5A 08 C9
        // 2A DE 0A 61 84 E7 39
        // C3 E2 00 00 00 00 // 96 bytes till this point
        // 46 41 43 00 30 31 30 00 00 00 16 CA 00 01 00 00 16
        // BC 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 01 01 00 01 00
        // 01 02 00 00
        //byte[] jpegimg_start = { (byte) 0xFF, (byte) 0xD8 };
        //byte[] jpegimg_end = { (byte) 0xFF, (byte) 0xD9 };
        // 78 bytes CBEFF Header.

        byte[] res = null;
        String returnValue;
        byte[] apdu_facial = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00,
            0x00, 0x05, 0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x08};

        StringBuffer sb = new StringBuffer();

        try {
            // Create APDU Object
            CommandAPDU READ_APDU = new CommandAPDU(apdu_facial);

            res = sendCard.sendCommand(channel, READ_APDU, sb);

            //Raise Error if APDU command is not successful
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();
                errMsg.append("Unable to read Facial Image");
                errMsg.append(" (").append(sb.toString()).append(")");
                throw new MiddleWareException(errMsg.toString());
            }

            if (res == null || res.length == 0) {
                returnValue = "";

            } else {
                //convert byte array to string & remove parity bits
                returnValue = util.arrayToHex(res);

                //remove leading characters
                if (returnValue.substring(2, 4).equals("82")) {
                    returnValue = returnValue.substring(8);
                } else if (returnValue.substring(2, 4).equals("81")) {
                    returnValue = returnValue.substring(6);
                } else {
                    returnValue = returnValue.substring(4);
                }

            }
            return returnValue;
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    @Override
    public boolean verifyFingerPrint(CardChannel channel, byte[] fingerInfo, byte[] sub_imp_type) throws MiddleWareException {
        boolean rtnvalue = false;
        StringBuffer sb = new StringBuffer();
        byte[] command_data;
        byte[] zero_size = {0x00};

        byte[] apdu_cls = {0x00, 0x21, 0x00, 0x00};
        byte[] var_len_1;
        byte[] apdu_tag = {0x7F, 0x2E};
        byte[] var_len_2;
        byte[] minutiae_tag = {(byte) 0x81};
        byte[] apdu_ber_tag = {(byte) 0x81};
        byte[] var_len_3;
        String tot_hex_data_size;
        int add_len_1;

        // byte[] apdu_biotype = {(byte)0x95, 0x03, 0x08} ;
        // byte[] sub_bio_type ={0x09};

        if ((fingerInfo.length) > 255 && (fingerInfo.length) < 4096) {

            tot_hex_data_size = "0" + Integer.toHexString((fingerInfo.length));
            add_len_1 = 3;
        } else {
            tot_hex_data_size = Integer.toHexString((fingerInfo.length));
            add_len_1 = 2;
        }

        var_len_3 = util.hex1ToByteArray(tot_hex_data_size);

        // Var_len_2 is greater than 127, then ber-tlv tag 81 must be added.

        if (fingerInfo.length + add_len_1 > 127) {

            var_len_3 = util.combine_data(apdu_ber_tag, var_len_3);
            add_len_1 += 1;
        }

        // Following condition makes sure that converted hex string is always
        // even.
        String hex_var_len_2;
        int add_len_2; // This is for tag "7F2E"...
        if ((fingerInfo.length + add_len_1) > 255
                && (fingerInfo.length + add_len_1) < 4096) {

            hex_var_len_2 = "0"
                    + Integer.toHexString((fingerInfo.length + add_len_1));
            add_len_2 = 4;
        } else {
            hex_var_len_2 = Integer.toHexString((fingerInfo.length + add_len_1));
            add_len_2 = 3;
        }

        var_len_2 = util.hex1ToByteArray(hex_var_len_2);

        // Var_len_2 is greater than 127, then ber-tlv tag 81 must be added.
        int add_len_3 = 0;
        if (fingerInfo.length + add_len_1 > 127) {

            var_len_2 = util.combine_data(apdu_ber_tag, var_len_2);
            add_len_3 = 1;
        }

        // Now calculate the LC Value of commands

        String hex_var_len_1;

        if ((fingerInfo.length + add_len_1 + add_len_2 + add_len_3) > 255
                && (fingerInfo.length + add_len_1 + add_len_2 + add_len_3) < 4096) {

            hex_var_len_1 = "0"
                    + Integer.toHexString((fingerInfo.length + add_len_1
                    + add_len_2 + add_len_3));

        } else {
            hex_var_len_1 = Integer.toHexString((fingerInfo.length + add_len_1
                    + add_len_2 + add_len_3));

        }

        var_len_1 = util.hex1ToByteArray(hex_var_len_1);

        if (var_len_1.length > 1) {
            var_len_1 = util.combine_data(zero_size, var_len_1);
        }

        command_data = util.combine_data(apdu_cls, var_len_1);
        command_data = util.combine_data(command_data, apdu_tag);
        command_data = util.combine_data(command_data, var_len_2);
        command_data = util.combine_data(command_data, minutiae_tag);
        command_data = util.combine_data(command_data, var_len_3);
        command_data = util.combine_data(command_data, fingerInfo);
        // command_data = util.combine_data(command_data, apdu_biotype);
        // command_data = util.combine_data(command_data, sub_bio_type);
        // command_data = util.combine_data(command_data, sub_imp_type);

        // System.out.println("Final Command " + util.arrayToHex(command_data
        // ));

        CommandAPDU VERIFY_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, VERIFY_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (sb.toString().equals("9000")) {
            // sat123 Credential.setfinger_print(fingerInfo);
            rtnvalue = true;
        }
        return rtnvalue;

    }

    @Override
    public boolean writeCustomInfo(String strhex_data, char container_type,
            CardChannel channel, byte[] adminKey, byte[] diversifiedAdminKey) throws MiddleWareException {
        // TO write Customised data in the card Container.
        boolean rtnvalue = false;
        byte[] cont_id;
        byte[] newData;
        byte[] command_data;
        StringBuffer sb = new StringBuffer();
        System.out.println("received Hex  " + strhex_data);
        System.out.println("received Hex string length " + strhex_data.length());

        int str_len = 520; // Minimum data String has to be of this length.
        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {
            auth.generalAuth(channel, adminKey);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, diversifiedAdminKey);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        Integer hex_len = strhex_data.length(); // For Bert Tag.

        if (hex_len < str_len) {
            strhex_data = util.formatHexString(str_len, strhex_data);
        }

        newData = util.hex1ToByteArray(strhex_data);

        int len = newData.length;
        System.out.println("Length of byte newData " + len);

        byte[] person_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0D}; // Personal
        // info
        // container
        // identifier.
        byte[] med_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0E}; // Medical
        // container
        // identifier.
        byte[] ins_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0F}; // Insurance
        // container
        // identifier.
        byte[] fam_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x10}; // Family
        // History
        // container
        // identifier.

        if (container_type == 'P') {
            cont_id = person_id;
        } else if (container_type == 'M') {
            cont_id = med_id;
        } else if (container_type == 'I') {
            cont_id = ins_id;
        } else {
            cont_id = fam_id;
        }

        // UPDATE Container APDU Command
        String tot_hex_size;
        String tot_hex_data_size;
        byte[] zero_size = {0x00};
        Integer add_len;

        byte[] apdu_command = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};

        // Now Determine value of Bert Tag
        byte[] bert_tag = new byte[2];
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for
        // size of data
        // bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size
        // of data bytes

        if (len > 255) {

            bert_tag = bert_tag_size_big;
            add_len = 9; // For larger data we need one extra byte to store
            // length.
        } else {
            bert_tag = bert_tag_size_small;
            add_len = 7;
        }

        // Following condition makes sure that converted hex string is always
        // even.
        if ((len + add_len) > 255 && (len + add_len) < 4096) {
            tot_hex_size = "0" + Integer.toHexString(len + add_len);

        } else {
            tot_hex_size = Integer.toHexString(len + add_len);

        }

        byte[] total_size = util.hex1ToByteArray(tot_hex_size);

        // Now calculate actual data size being stored
        // Following condition makes sure that converted hex string is always
        // even.
        if (len > 255 && len < 4096) {

            tot_hex_data_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_data_size = Integer.toHexString(len);

        }

        byte[] num_of_bytes = util.hex1ToByteArray(tot_hex_data_size);

        // System.out.println("total_hex_size: " + tot_hex_size);
        // System.out.println("total_Byte_size: " +
        // util.arrayToHex(total_size));

        if (total_size.length > 1) {
            total_size = util.combine_data(zero_size, total_size);
        }

        command_data = util.combine_data(apdu_command, total_size);
        command_data = util.combine_data(command_data, cont_id);
        command_data = util.combine_data(command_data, bert_tag);
        command_data = util.combine_data(command_data, num_of_bytes);
        command_data = util.combine_data(command_data, newData);

        System.out.println("Final Command " + util.arrayToHex(command_data));

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (sb.toString().equals("9000")) {
            rtnvalue = true;
        }
        return rtnvalue;

    }

    @Override
    public void writeCustomInfo(CardChannel channel, char record_type,
            String[] str_data_array, byte[] adminKey, byte[] diversifiedAdminKey) throws MiddleWareException {

        // TO write Insurance Container.
        // 5FC10D
        // 5FC10E
        int j = 0;
        int n = 0;
        int k = 0;

        byte[] cont_id;
        String str_data;
        String hex_data = null;
        String total_hex_data = null;
        byte[] newData;
        byte[] command_data;
        StringBuffer sb = new StringBuffer();
        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, adminKey);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, diversifiedAdminKey);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        for (int i = 0; i < str_data_array.length; i += 1) {

            str_data = str_data_array[i];

            if (str_data == End_of_record) {
                n = j; // Store the counter j in n before incrementing.
                j++; // Increment to get to next record identifier.
                k = 0; // Initialize the meta data array pointer, so that next
                // record's
                // field identifier's can be appended.
                continue;
            }

            hex_data = util.stringToHex(str_data);
            hex_data = util.formatHexString(insurance_meta_size[k], hex_data);

            if (j == 0 && i == 0) {
                hex_data = next_record[j] + insurance_meta[k] + hex_data;
            } else if (j != n) {
                hex_data = next_record[j] + insurance_meta[k] + hex_data;
                n = j; // again assign j to n, so that next record field only
                // gets
                // appended, when next record is detected.
            } else {
                hex_data = insurance_meta[k] + hex_data;
            }

            if (i == 0) {
                total_hex_data = hex_data;
            } else {
                total_hex_data = total_hex_data + hex_data;
            }

            k++; // Increment meta data counter to get the meta data for next
            // field.

        }

        // total_hex_data:
        // 01010148617270726565742053696e67682020202020202020202020202020202020202020202020202020
        // 02303831353139373220
        // 03333920
        // 020152616a696e6465722053696e67682020202020202020202020202020202020202020202020202020
        // 02303931353139363120
        // 03343520

        System.out.println("total_hex_data: " + total_hex_data);

        newData = util.hexToByteArray(total_hex_data);

        Integer len = newData.length;

        byte[] person_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0D}; // Personal
        // info
        // container
        // identifier.
        byte[] med_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0E}; // Medical
        // container
        // identifier.
        byte[] ins_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0F}; // Insurance
        // container
        // identifier.

        if (record_type == 'P') {
            cont_id = person_id;
        } else if (record_type == 'M') {
            cont_id = med_id;
        } else {
            cont_id = ins_id;
        }

        // UPDATE Container APDU Command
        byte[] apdu_command = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF, 0x00};
        byte[] total_size = util.hexToByteArray(Integer.toHexString(len + 8));
        byte[] bert_tag = {0x53, (byte) 0x81}; // identifier for size of data
        // bytes
        byte[] num_of_bytes = util.hexToByteArray(Integer.toHexString(len));

        command_data = util.combine_data(apdu_command, total_size);
        command_data = util.combine_data(command_data, cont_id);
        command_data = util.combine_data(command_data, bert_tag);
        command_data = util.combine_data(command_data, num_of_bytes);
        command_data = util.combine_data(command_data, newData);

        // System.out.println("Final Command " + util.arrayToHex(command_data
        // ));

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {

            throw new MiddleWareException(e.getMessage());
        }

    }

    @Override
    public byte[] readCustomInfo(CardChannel channel, char record_type) throws MiddleWareException {

        // TO read Insurance Container.
        // 5FC10D
        // 5FC10E
        StringBuffer sb = new StringBuffer();
        byte[] res = null;
        byte[] cont_id;
        byte[] command_data;

        byte[] person_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0D}; // Personal
        // info
        // container
        // identifier.
        byte[] med_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0E}; // Medical
        // container
        // identifier.
        byte[] ins_id = {0x5C, 0x03, 0x5F, (byte) 0xC1, 0x0F}; // Insurance
        // container
        // identifier.

        if (record_type == 'P') {
            cont_id = person_id;
        } else if (record_type == 'M') {
            cont_id = med_id;
        } else {
            cont_id = ins_id;
        }

        // Read Container APDU Command

        // byte[] apdu_read = { 0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05 }
        // 0x5C, 0x03, (byte) 0x5F, (byte) 0xC1, 0x0D }; // BERT-TLV

        byte[] apdu_command = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x05};

        command_data = util.combine_data(apdu_command, cont_id);

        CommandAPDU READ_APDU = new CommandAPDU(command_data);

        try {

            res = sendCard.sendCommand(channel, READ_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        System.out.println("Final read " + util.arrayToHex(res));
        return res;

    }

    @Override
    public boolean updateFingerPrint(CardChannel channel, byte[] adminKey, byte[] diversifiedAdminKey, byte[] fingerInfo,
            String fgType, byte[] sub_imp_type) throws MiddleWareException {

        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, adminKey);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, diversifiedAdminKey);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }
        // byte[] sub_bio_type = getFingerType(fgType);
        byte[] sub_bio_type = getFingerID(fgType);

        boolean rtnvalue = false;
        StringBuffer sb = new StringBuffer();
        byte[] command_data;
        byte[] zero_size = {0x00};

        byte[] apdu_cls = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};
        byte[] var_len_1;
        byte[] apdu_tag = {0x7F, 0x2E};
        byte[] var_len_2;
        byte[] minutiae_tag = {(byte) 0x81};

        byte[] apdu_ber_tag = {(byte) 0x81};
        byte[] var_len_3;

        byte[] apdu_biotype = {(byte) 0x95, 0x03, 0x08};
        // byte[] sub_imp_type = {0x00} ;

        // Now calculate actual data size being stored
        // Following condition makes sure that converted hex string is always
        // even.

        String tot_hex_data_size;
        int add_len_1 = 0;
        if ((fingerInfo.length) > 255 && (fingerInfo.length) < 4096) {
            tot_hex_data_size = "0" + Integer.toHexString((fingerInfo.length));
            add_len_1 = 2;
        } else {
            tot_hex_data_size = Integer.toHexString((fingerInfo.length));
            add_len_1 = 1;
        }

        var_len_3 = util.hex1ToByteArray(tot_hex_data_size);

        // Var_len_2 is greater than 127, then ber-tlv tag 81 must be added.

        if (fingerInfo.length > 127) {
            var_len_3 = util.combine_data(apdu_ber_tag, var_len_3);
            add_len_1 += 1;
        }

        // Following condition makes sure that converted hex string is always
        // even.
        String hex_var_len_2;
        int add_len_2; // This is for tag "7F2E"...
        if ((fingerInfo.length + 5 + add_len_1) > 255
                && (fingerInfo.length + 5 + add_len_1) < 4096) {

            hex_var_len_2 = "0"
                    + Integer.toHexString((fingerInfo.length + 5 + add_len_1));
            add_len_2 = 4;
        } else {
            hex_var_len_2 = Integer.toHexString((fingerInfo.length + 5 + add_len_1));
            add_len_2 = 3;
        }

        var_len_2 = util.hex1ToByteArray(hex_var_len_2);

        // Var_len_2 is greater than 127, then ber-tlv tag 81 must be added.
        int add_len_3 = 0;
        if (fingerInfo.length + 5 + add_len_1 > 127) {

            var_len_2 = util.combine_data(apdu_ber_tag, var_len_2);
            add_len_3 = 1;
        }

        // Now calculate the LC Value of commands

        String hex_var_len_1;

        if ((fingerInfo.length + 5 + add_len_1 + add_len_2 + add_len_3) > 255
                && (fingerInfo.length + 5 + add_len_1 + add_len_2 + add_len_3) < 4096) {

            hex_var_len_1 = "0"
                    + Integer.toHexString((fingerInfo.length + 5 + add_len_1
                    + add_len_2 + add_len_3));

        } else {
            hex_var_len_1 = Integer.toHexString((fingerInfo.length + 5
                    + add_len_1 + add_len_2 + add_len_3));

        }

        var_len_1 = util.hex1ToByteArray(hex_var_len_1);

        if (var_len_1.length > 1) {
            var_len_1 = util.combine_data(zero_size, var_len_1);
        }

        command_data = util.combine_data(apdu_cls, var_len_1);
        command_data = util.combine_data(command_data, apdu_tag);
        command_data = util.combine_data(command_data, var_len_2);
        command_data = util.combine_data(command_data, minutiae_tag);
        command_data = util.combine_data(command_data, var_len_3);
        command_data = util.combine_data(command_data, fingerInfo);
        command_data = util.combine_data(command_data, apdu_biotype);
        command_data = util.combine_data(command_data, sub_bio_type);
        command_data = util.combine_data(command_data, sub_imp_type);

        System.out.println("Final COmmand " + util.arrayToHex(command_data));

        // 00 db 3f ff
        // 73
        // 7f 2e 70 81 69
        // 3e115f70227858257a2c29654a337b683658503e5b3c43664d485d22506b41546e44547046547318576b6159b06a5a70785e704860504662965a63ac3165722967b03d6ab9236daf4f6e655c746b1c78af4a78a2177aaf657bac2b7cb471886d4f8da8298e7558956a
        // 950308
        // 0000

        // 00 db 3f ff
        // 87 7f2e 81 83
        // 81 81 7b
        // 4e1c795d20572428654229784232582a3b80343d5e523d55364a9b2e4e716150714e51702b53721e55af5a57b0385b54365d96555faf32657e4965ae2a69b5196c72416d681670b12770b47672704c7aab427d89627d6f6d7eb12a83775283ac268776408b60718b8f2d8f9534917a5891ad3c9a80479d6236a17d9503080a00

        CommandAPDU WRITE_APDU = new CommandAPDU(command_data);

        try {

            sendCard.sendCommand(channel, WRITE_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        if (sb.toString().equals("9000")) {
            rtnvalue = true;
        }

        return rtnvalue;

    }

    public int getFingerType(String str_fgType) {
        int sub_bio;
        /*
         * Finger Number Finger Type Button on each finger flag for each finger
         * Code HEX
         * --------------------------------------------------------------
         * ------------------------------ 1 RT Jbutton_rightThumb
         * jLabel_rightThumb ‘05’ {0x05} 2 RI Jbutton_rightIndex
         * jLabel_rightIndex ‘09’ {0x09} 3 RM Jbutton_rightMiddle
         * jLabel_rightMiddle ‘0D’ {0x0D} 4 RR Jbutton_rightRing
         * jLabel_rightRing ‘11’ {0x11} 5 RP Jbutton_rightPinky
         * jLabel_rightPinky ‘15’ {0x15}
         * 
         * 6 LT Jbutton_leftThumb jLabel_leftThumb ‘06’ {0x06} 7 LI
         * Jbutton_leftIndex jLabel_leftIndex ‘0A’ {(byte) 0x0A} 8 LM
         * Jbutton_leftMiddle jLabel_leftMiddle ‘0E’ {0x0E} 9 LR
         * Jbutton_leftRing jLabel_leftRing ‘12’ {0x12} 10 LP Jbutton_leftPinky
         * jLabel_leftPinky ‘16’ {0x16}
         */

        if ("05".equalsIgnoreCase(str_fgType)) {
            sub_bio = 1;
        } else if ("09".equalsIgnoreCase(str_fgType)) {
            sub_bio = 2;
        } else if ("0D".equalsIgnoreCase(str_fgType)) {
            sub_bio = 3;
        } else if ("11".equalsIgnoreCase(str_fgType)) {
            sub_bio = 4;
        } else if ("15".equalsIgnoreCase(str_fgType)) {
            sub_bio = 5;
        } else if ("06".equalsIgnoreCase(str_fgType)) {
            sub_bio = 6;
        } else if ("0A".equalsIgnoreCase(str_fgType)) {
            sub_bio = 7;
        } else if ("0E".equalsIgnoreCase(str_fgType)) {
            sub_bio = 8;
        } else if ("12".equalsIgnoreCase(str_fgType)) {
            sub_bio = 9;
        } else if ("16".equalsIgnoreCase(str_fgType)) {
            sub_bio = 10;
        } else {
            sub_bio = 0;
        }

        return sub_bio;

    }

    public byte[] getFingerType(int fgType) {
        byte[] sub_bio = new byte[1];

        byte[] RT = {0x05};
        byte[] RI = {0x09};
        byte[] RM = {0x0D};
        byte[] RR = {0x11};
        byte[] RL = {0x15};
        byte[] LT = {0x06};
        byte[] LI = {(byte) 0x0A};
        byte[] LM = {0x0E};
        byte[] LR = {0x12};
        byte[] LL = {0x16};

        // ‘05’ Right thumb
        // ‘09’ Right index
        // ‘0D’ Right middle
        // ‘11’ Right ring
        // ‘15’ Right little
        // ‘06’ Left thumb
        // ‘0A’ Left index
        // ‘0E’ Left middle
        // ‘12’ Left ring
        // ‘16’ Left little

        switch (fgType) {
            case 1:
                sub_bio = RT;
                break;
            case 2:
                sub_bio = RI;
                break;
            case 3:
                sub_bio = RM;
                break;
            case 4:
                sub_bio = RR;
                break;
            case 5:
                sub_bio = RL;
                break;
            case 6:
                sub_bio = LT;
                break;
            case 7:
                sub_bio = LI;
                break;
            case 8:
                sub_bio = LM;
                break;
            case 9:
                sub_bio = LR;
                break;
            case 10:
                sub_bio = LL;
                break;

            default:
                break;
        }

        return sub_bio;

    }

    public byte[] getFingerID(String fgType) {
        byte[] sub_bio = new byte[1];

        byte[] RT = {0x05}; // Right thumb
        byte[] RI = {0x09}; // Right index
        byte[] RM = {0x0D}; // Right middle
        byte[] RR = {0x11}; // Right ring
        byte[] RL = {0x15}; // Right little
        byte[] LT = {0x06}; // Left thumb
        byte[] LI = {(byte) 0x0A}; // Left index
        byte[] LM = {0x0E}; // Left middle
        byte[] LR = {0x12}; // Left ring
        byte[] LL = {0x16}; // Left little

        if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.rightThumb)) {
            sub_bio = RT;
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.rightIndex)) {
            sub_bio = RI;
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.rightMiddle)) {
            sub_bio = RM;
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.rightRing)) {
            sub_bio = RR;
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.rightLittle)) {
            sub_bio = RL;
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.leftThumb)) {
            sub_bio = LT;
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.leftIndex)) {
            sub_bio = LI;
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.leftMiddle)) {
            sub_bio = LM;
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.leftRing)) {
            sub_bio = LR;
        } else if (fgType.equalsIgnoreCase(UcmsMiddlewareConstants.leftLittle)) {
            sub_bio = LL;
        }
        return sub_bio;

    }

    public void deleteInstance(CardChannel channel) throws MiddleWareException {

        // First of all Perform general Authentication.
        // scard.generalAuth(channel,key_p); //get the Admin Authentication

        // NOw Send Delete Instance command
        // 5C 02 3F F5 53 00
        StringBuffer sb = new StringBuffer();

        byte[] apdu_delete = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF, 0x06,
            0x5C, 0x02, 0x3F, (byte) 0xF5, 0x53, 0x00};

        CommandAPDU READ_APDU = new CommandAPDU(apdu_delete);

        try {

            sendCard.sendCommand(channel, READ_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    public FingerPrintBean getBioInfo(CardChannel channel) throws MiddleWareException {

        byte[] res = null;
        StringBuffer sb = new StringBuffer();
        FingerPrintBean fingerPrintBean = new FingerPrintBean();
        String errorMsg;
        String str_finger;
        byte[] apdu_cls = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00,
            0x04, 0x5C, 0x02, 0x7F, 0x61};

        CommandAPDU VERIFY_APDU = new CommandAPDU(apdu_cls);

        try {

            res = sendCard.sendCommand(channel, VERIFY_APDU, sb);

            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "FingerImageReadError");
                throw new MiddleWareException(errorMsg);
            }
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }



        try {
            String ss = util.arrayToHex(res);
            String end_tag = "87";
            int start_loc = 0;
            int end_pos;

            end_pos = util.findtagLocation(start_loc, ss, end_tag);

            String tag = "82";
            int tag_pos = 0;
            int j = 0;
            for (int i = 0; i < end_pos;) {
                tag_pos = util.findtagLocation(i, ss, tag);

                if (tag_pos == -1 || tag_pos > end_pos) {
                    break;
                }

                str_finger = ss.substring(tag_pos + 4, tag_pos + 6);
                if(str_finger.equalsIgnoreCase(UcmsMiddlewareConstants.rightThumb)){
                    fingerPrintBean.setRightThumb(true);
                }else if(str_finger.equalsIgnoreCase(UcmsMiddlewareConstants.rightMiddle)){
                    fingerPrintBean.setRightMiddle(true);
                }else if(str_finger.equalsIgnoreCase(UcmsMiddlewareConstants.rightRing)){
                    fingerPrintBean.setRightRing(true);
                }else if(str_finger.equalsIgnoreCase(UcmsMiddlewareConstants.rightLittle)){
                    fingerPrintBean.setRightLittle(true);
                }else if(str_finger.equalsIgnoreCase(UcmsMiddlewareConstants.rightIndex)){
                    fingerPrintBean.setRightIndex(true);
                }else if(str_finger.equalsIgnoreCase(UcmsMiddlewareConstants.leftThumb)){
                    fingerPrintBean.setLeftThumb(true);
                }else if(str_finger.equalsIgnoreCase(UcmsMiddlewareConstants.leftIndex)){
                    fingerPrintBean.setLeftIndex(true);
                }else if(str_finger.equalsIgnoreCase(UcmsMiddlewareConstants.leftMiddle)){
                    fingerPrintBean.setLeftMiddle(true);
                }else if(str_finger.equalsIgnoreCase(UcmsMiddlewareConstants.leftRing)){
                    fingerPrintBean.setLeftRing(true);
                }else if(str_finger.equalsIgnoreCase(UcmsMiddlewareConstants.leftLittle)){
                    fingerPrintBean.setLeftLittle(true);
                }
                /*
                switch (str_finger.toUpperCase()) {
                    case UcmsMiddlewareConstants.rightThumb:
                        fingerPrintBean.setRightThumb(true);
                        break;
                    case UcmsMiddlewareConstants.rightMiddle:
                        fingerPrintBean.setRightMiddle(true);
                        break;
                    case UcmsMiddlewareConstants.rightRing:
                        fingerPrintBean.setRightRing(true);
                        break;
                    case UcmsMiddlewareConstants.rightLittle:
                        fingerPrintBean.setRightLittle(true);
                        break;
                    case UcmsMiddlewareConstants.rightIndex:
                        fingerPrintBean.setRightIndex(true);
                        break;
                    case UcmsMiddlewareConstants.leftThumb:
                        fingerPrintBean.setLeftThumb(true);
                        break;
                    case UcmsMiddlewareConstants.leftIndex:
                        fingerPrintBean.setLeftIndex(true);
                        break;
                    case UcmsMiddlewareConstants.leftMiddle:
                        fingerPrintBean.setLeftMiddle(true);
                        break;
                    case UcmsMiddlewareConstants.leftRing:
                        fingerPrintBean.setLeftRing(true);
                        break;
                    case UcmsMiddlewareConstants.leftLittle:
                        fingerPrintBean.setLeftLittle(true);
                        break;
                }*/
                i = tag_pos + 6;
                j++;
            }
        } catch (Exception e) {
            throw new MiddleWareException(e.getMessage());
        }

        return fingerPrintBean;
    }

    public void select(CardChannel channel, byte[] data) throws CardException {

        StringBuffer sb = new StringBuffer();

        byte cla = 0x00, ins = (byte) (0xA4 & 0xFF), p1 = 0x04, p2 = 0x00, de = 0x00;

        CommandAPDU SELECT_APDU = new CommandAPDU(cla, ins, p1, p2, data, de);

        System.out.println("SELECT_AID: " + channel);

        try {
            sendCard.sendCommand(channel, SELECT_APDU, sb);
        } catch (CardException e) {
            throw e;
        }

    }

    private void calcBertValue(Integer len, boolean binclude_bert) {
        // Determine value of Bert Tag
        // if (binclude_bert)
        byte[] bert_tag_size_small = {0x53, (byte) 0x81}; // identifier for
        // size of data
        // bytes
        byte[] bert_tag_size_big = {0x53, (byte) 0x82}; // identifier for size
        // of data bytes
        byte[] len_tag_small = {(byte) 0x81};
        byte[] len_tag_big = {(byte) 0x82};

        if (len > 255) {
            if (binclude_bert) {
                bert_tag = bert_tag_size_big;
            } else {
                bert_tag = len_tag_big;
            }

        } else {
            if (binclude_bert) {
                bert_tag = bert_tag_size_small;
            } else {
                bert_tag = len_tag_small;
            }

        }

        // Now calculate actual data size being stored.
        // Following condition makes sure that converted hex string is always
        // even.

        String tot_hex_size;

        if (len > 255 && len < 4096) {

            tot_hex_size = "0" + Integer.toHexString(len);

        } else {
            tot_hex_size = Integer.toHexString(len);

        }

        num_bytes = util.hex1ToByteArray(tot_hex_size);

    }

    /* Close Logical Channel */
    void closeManager(CardChannel channel) throws CardException {

        StringBuffer sb = new StringBuffer();
        byte[] log_ch1 = {0x00, 0x70, (byte) 0x80, 0x01, 0x00};

        CommandAPDU CLOSE_APDU = new CommandAPDU(log_ch1);

        try {
            sendCard.sendCommand(channel, CLOSE_APDU, sb);
        } catch (CardException e) {
            throw e;
        }

    }

    public String readCIN(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String errorMsg;
        StringBuffer sb = new StringBuffer();

        // First of all open Logical Channel '01' with card manager.

        try {
            selectManager(channel, true);
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getMessage());
        }


        byte[] apdu_CIN = {(byte) 0x80, (byte) 0xCA, 0x00, 0x45};
        CommandAPDU CIN_APDU = new CommandAPDU(apdu_CIN);

        try {

            res = sendCard.sendCommand(channel, CIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // Leave the first 2 bytes or 4 characters of the result.
        return util.arrayToHex(res).substring(4);
    }

    public String readIIN(CardChannel channel) throws MiddleWareException {

        // 01 A4 0400 00 ; this opens a logical channel “01” with the card
        // manager.
        // · 80 CA 00 42 00 ; Get Data to read the “Issuer Identification
        // Number” over the newly open logical
        // channel.
        // · 80 CA 0045 0A ; Get Data to read the “Card Image Number” over the
        // newly open logical
        // channel.
        // · 00 70 8001 00 ; Close the logical channel “01”.
        // First of all open Logical Channel '01' with card manager.

        byte[] res = null;

        String errorMsg;
        StringBuffer sb = new StringBuffer();

        try {
            selectManager(channel, true);
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getMessage());
        }

        byte[] apdu_IIN = {(byte) 0x80, (byte) 0xCA, 0x00, 0x42};

        CommandAPDU IIN_APDU = new CommandAPDU(apdu_IIN);

        try {
            res = sendCard.sendCommand(channel, IIN_APDU, sb);

            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // Leave the first 2 bytes or 4 characters of the result.
        return util.arrayToHex(res).substring(4);

    }

    public String readBAP(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String errorMsg;
        StringBuffer sb = new StringBuffer();

        // 80 CA 00 EE 
        // EE 05 14 34 08 72 82  

        // First of all open Logical Channel '01' with card manager.

        try {
            selectManager(channel, true);
        } catch (MiddleWareException m) {
            throw new MiddleWareException(m.getMessage());
        }


        byte[] apdu_BAP = {(byte) 0x80, (byte) 0xCA, 0x00, (byte) 0xEE};
        CommandAPDU CUID_BAP = new CommandAPDU(apdu_BAP);

        try {

            res = sendCard.sendCommand(channel, CUID_BAP, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // Leave the first 3 bytes.
        String str_result = util.arrayToHex(util.extract_data(res, 2, 4));
        return str_result;

    }

    public String readCUID(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String errorMsg;
        StringBuffer sb = new StringBuffer();

        // First of all open Logical Channel '01' with card manager.
       
        try {
            selectManager(channel, true);
        } catch (MiddleWareException m) {
            //DO NOT throw exception because if if card is unlocked then call select manager 
            //if card is locked select manager call is not required.
        }
         
        //80 CA 9F 7F 
        // 9F 7F 2A 48 20 50 2B 82 31 80 30 00 63 13 26 00 00 00 11 00 00
        //14 32 13 26 14 33 13 26 14 34 13 26 00 00 00 00 14 35 13 26 00 00 00 00 

        byte[] apdu_CUID = {(byte) 0x80, (byte) 0xCA, (byte) 0x9F, 0x7F};
        CommandAPDU CUID_APDU = new CommandAPDU(apdu_CUID);

        try {

            res = sendCard.sendCommand(channel, CUID_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // Leave the first 2 bytes or 4 characters of the result.
        String str_res = util.arrayToHex(res);
        System.out.println(str_res);

        String str_res_1 = util.arrayToHex(util.extract_data(res, 3, 4));
        String str_res_2 = util.arrayToHex(util.extract_data(res, 19, 2));
        String str_res_3 = util.arrayToHex(util.extract_data(res, 15, 4));

        String str_result = str_res_1 + str_res_2 + str_res_3;

        System.out.println(str_result);
        return str_result;


    }

    /* select Card Manager method */
    private void selectManager(CardChannel channel, Boolean logicalChannel) throws MiddleWareException {
        // Card manager selection on logical channel 0
        // 00 A4 04 00 00 (6CXX, 6283, 9000) ; Status 6283 is returned when the
        // CM is locked

        String errorMsg;
        StringBuffer sb = new StringBuffer();
        byte[] log_ch0 = {0x00, (byte) 0xA4, 0x04, 0x00, 0x00};
        byte[] log_ch1 = {0x01, (byte) 0xA4, 0x04, 0x00, 0x00};

        byte[] apdu_manager;

        if (logicalChannel) {
            apdu_manager = log_ch1;

        } else {
            apdu_manager = log_ch0;
        }

        CommandAPDU INIT_APDU = new CommandAPDU(apdu_manager);

        try {
            sendCard.sendCommand(channel, INIT_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    /*
    private String readPinLength(CardChannel channel, String pinType) throws MiddleWareException {
    byte[] res = null;
    byte[] apdu_pin=null;
    StringBuffer sb = new StringBuffer();
    String errorMsg;
    
    //set APDU command for local PIN / Global PIN / PUK 
    byte[] apdu_LP = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04,
    0x5C, 0x02, 0x7F, 0x72};
    byte[] apdu_GP = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04,
    0x5C, 0x02, 0x7F, 0x71};
    
    byte[] apdu_PUK = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04,
    0x5C, 0x02, 0x7F, 0x73};
    
    //Send : 00 CB 3F FF 00 00 04 5C 02 7F 72 
    //Recv : 7F 72 0B 5F 01 01 08 9F 17 01 02 93 01 02 
    //Send : 00 CB 3F FF 00 00 04 5C 02 7F 71 
    //Recv : 7F 71 0B 5F 01 01 08 9F 17 01 02 93 01 02 
    // Leave the first 6 bytes or 12 characters of the result.
    //7f 72 0b 5f 01 01 08 9f 17 01 02 93 01 02         
    
    
    
    if (equals(pinType, UcmsMiddlewareConstants.LOCAL_PIN) ) {
    apdu_pin = apdu_LP;
    } else if (equals(pinType, UcmsMiddlewareConstants.GLOBAL_PIN)) {
    apdu_pin = apdu_GP;
    } else if (equals(pinType, UcmsMiddlewareConstants.PUK_PIN)) {
    apdu_pin = apdu_PUK;
    }
    
    CommandAPDU LP_APDU = new CommandAPDU(apdu_pin);
    
    try {
    
    res = sendCard.sendCommand(channel, LP_APDU, sb);
    if (!sb.toString().equals("9000")) {
    errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
    throw new MiddleWareException(errorMsg);
    }
    String hextStringResult =  util.arrayToHex(res);
    if (hextStringResult.length() <14){
    throw new MiddleWareException("Unsatisfied hex result from card");
    }else{
    return util.arrayToHex(res).substring(12, 14);
    }
    
    } catch (CardException e) {
    throw new MiddleWareException(e.getMessage());
    }
    }
    
    
     */
    private boolean equals(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equalsIgnoreCase(str2);
    }

    private int readPinLength(CardChannel channel, String pinType) throws MiddleWareException {
        byte[] res = null;
        byte[] apdu_pin = null;
        StringBuffer sb = new StringBuffer();
        String errorMsg;

        //set APDU command for local PIN / Global PIN / PUK 
        byte[] apdu_LP = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04,
            0x5C, 0x02, 0x7F, 0x72};
        byte[] apdu_GP = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04,
            0x5C, 0x02, 0x7F, 0x71};

        byte[] apdu_PUK = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04,
            0x5C, 0x02, 0x7F, 0x73};

        //Send : 00 CB 3F FF 00 00 04 5C 02 7F 72 
        //Recv : 7F 72 0B 5F 01 01 08 9F 17 01 02 93 01 02 
        //Send : 00 CB 3F FF 00 00 04 5C 02 7F 71 
        //Recv : 7F 71 0B 5F 01 01 08 9F 17 01 02 93 01 02 
        // Leave the first 6 bytes or 12 characters of the result.
        //7f 72 0b 5f 01 01 08 9f 17 01 02 93 01 02         


        if (pinType.equalsIgnoreCase(UcmsMiddlewareConstants.LOCAL_PIN)) {

            apdu_pin = apdu_LP;
        } else if (pinType.equalsIgnoreCase(UcmsMiddlewareConstants.GLOBAL_PIN)) {
            apdu_pin = apdu_GP;
        } else if (pinType.equalsIgnoreCase(UcmsMiddlewareConstants.PUK_PIN)) {
            apdu_pin = apdu_PUK;
        }

        CommandAPDU LP_APDU = new CommandAPDU(apdu_pin);

        try {

            res = sendCard.sendCommand(channel, LP_APDU, sb);
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }
            String hextStringResult = util.arrayToHex(res);
            if (hextStringResult.length() < 14) {
                throw new MiddleWareException("Unsatisfied hex result from card");
            } else {

                return Integer.valueOf(util.arrayToHex(res).substring(12, 14), 16).intValue();
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    @Override
    public PinPolicyBean readPinPolicy(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String hextStringResult;
        StringBuffer sb = new StringBuffer();
        String errorMsg;
        String appletVersion;
        PinPolicyBean ppBean = new PinPolicyBean();

        ppBean.setMaxLocalPinLength(readPinLength(channel, UcmsMiddlewareConstants.LOCAL_PIN));
        ppBean.setMaxGlobalPinLength(readPinLength(channel, UcmsMiddlewareConstants.GLOBAL_PIN));
        ppBean.setMaxLocalPUKLength(readPinLength(channel, UcmsMiddlewareConstants.PUK_PIN));

        appletVersion = getAppletVersion(channel);

        //if (appletVersion.equalsIgnoreCase(UcmsMiddlewareConstants.appletVersion01)) {
        //   ppBean.setMinLocalPinLength(3);
        //   ppBean.setMinGlobalPinLength(3);
        //} else {


        //set APDU command for reading pin policy. 
        byte[] apdu_Pin = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04, 0x5C, 0x02, 0x3F, (byte) 0xF3};

        // Send : 00 CB 3F FF 00 00 04 5C 02 3F F3 
        //Recv : 53 15 
        //16 36 FF >> These 3 bytes represents Local pin policy.
        //16 06 FF  >> These 3 bytes represents Global pin policy.
        //00 15 00 15 00 15 00 15 00 15 00 15 00 15 00 
        //duration:125 ms SW : 9000
        /*
        * Get Data  ( CHV management )00 cb 3f ff 00 00 04 5c 02 3f f3 00 00 
        Local PIN         Numeric in ASCII with FF padding  pin length must be at least 6 characters
        Local PUK         Any byte value except FF for padding  pin length must be at least 6 characters
        Local SO PIN      not initialized (FF)
        Global PIN        Numeric in ASCII with FF padding  pin length must be at least 6 characters
        Global PUK        Any byte value  pin length must be at least 6 characters
        Global SO PIN     not initialized (FF)
        */
        //The high nibble encodes the type of values allowed, and the low nibble has the minimum character count.
        //FF means not initialized.


        CommandAPDU CMD_APDUPin = new CommandAPDU(apdu_Pin);

        try {

            res = sendCard.sendCommand(channel, CMD_APDUPin, sb);
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }

            hextStringResult = util.arrayToHex(res);

            if (hextStringResult.length() < 14) {
                ppBean.setMinLocalPinLength(3);
                ppBean.setMinGlobalPinLength(3);
            } else {
                hextStringResult = util.arrayToHex(res).substring(4, 16);
                ppBean.setMinLocalPinLength(Integer.valueOf(hextStringResult.substring(1, 2), 16).intValue());
                ppBean.setMinGlobalPinLength(Integer.valueOf(hextStringResult.substring(7, 8), 16).intValue());
            }

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        return ppBean;
    }

    public String getAppletVersion(CardChannel channel) throws MiddleWareException {
        byte[] res = null;
        String appletVersion = null;
        StringBuffer sb = new StringBuffer();
        String errorMsg;

        //set APDU command for Applet version
        byte[] apdu_applet_version = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00, 0x00, 0x04,
            0x5C, 0x02, 0x3F, (byte) 0xF0};

        CommandAPDU LP_APDU = new CommandAPDU(apdu_applet_version);

        try {

            res = sendCard.sendCommand(channel, LP_APDU, sb);
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "");
                throw new MiddleWareException(errorMsg);
            }



            appletVersion = util.arrayToHex(util.extract_data(res, 2, 2));
            return appletVersion;
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }
    
    /* select Card Manager method */
    public byte[] getCardData(CardChannel channel) throws MiddleWareException {
        
        
        // Get Card Recognition Data to find out SCP protocol to use
        // I(1;1) CA 0066 00 (9000)
        //   73 32 
        // 06 07 2a864886fc6b01
        //600c060a2a864886fc6b02020101
        //63 09 06 07 2a 86 48 86 fc 6b 03
        //64 0e 06 0c 2b 06 01 04 01 81 ef 6f 02 04 03 15


        // 73 2f 
        //  06 07 2a 86 48 86 fc 6b 01 
        //  60 0c 06 0a 2a 86 48 86 fc 6b 02 02 01 01 
        //  63 09 06 07 2a 86 48 86 fc 6b 03 
        //  64 0b 06 09 2a 86 48 86 fc 6b 04 01 05
        // 2A864886FC6B040105


        StringBuffer sb = new StringBuffer();
        String scp_tag = "64";
        String str_scp_data;
        String str_res;
        int tag_loc;
        byte[] res = null;
        byte[] scp_data = null;
        byte[] apdu_manager = {0x00, (byte) 0xCA, 0x00, 0x66, 0x00};

        
        try{
            selectSecurityDomain(channel);
        } catch(MiddleWareException e){
            throw new MiddleWareException("Error selecting Secure Channel");
        }
        
        
        CommandAPDU INIT_APDU = new CommandAPDU(apdu_manager);

        try {
            res = sendCard.sendCommand(channel, INIT_APDU, sb);
            if (!sb.toString().equals("9000")) {
                
                throw new MiddleWareException("Error Reading Card information");
            }
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }
        System.out.println("Card Data: " + util.arrayToHex(res));

        str_res = util.arrayToHex(res);
        // Get Secure Channel Protocol Data.
        tag_loc = util.findtagLocation(0, str_res, scp_tag);
        String scp_length = str_res.substring(tag_loc + 6, tag_loc + 8);
        int i = Integer.valueOf(scp_length, 16).intValue();

        str_scp_data = str_res.substring(tag_loc + 8, tag_loc + 8 + i * 2);

        scp_data = util.hex1ToByteArray(str_scp_data);

        return scp_data;
    }
    
    /* select Security Domain method */
    private void selectSecurityDomain(CardChannel channel) throws MiddleWareException {

        // **----------------------------------------------
        // ** Check presence and status of PIV-SD
        // **----------------------------------------------
        // ; Select PIV Application Security Domain using Oberthur registered
        // AID
        // 00 A4 04 00 10 A0 00 00 00 77 01 00 00 06 10 00 FD 00 00 00 27 (9000,
        // 6A82)
        // 00 A4 04 00 
        //Recv : 6F 3C 84 07
        //A0 00 00 01 51 00 00
        //A5 31 9F 6E 2A 48 20 50 2B 82 31 80 30 00 63 31 16 00 00 00 0C 00 00 14 32 31 16 14 33 31 16 14 34 31 16 00 00 00 00 14 35 31 16 00 00 00 00 9F 65 01 FF 


        StringBuffer sb = new StringBuffer();
        byte[] apdu_security_piv = {0x00, (byte) 0xA4, 0x04, 0x00, 0x10,
            (byte) 0xA0, 0x00, 0x00, 0x00, 0x77, 0x01, 0x00, 0x00, 0x06,
            0x10, 0x00, (byte) 0xFD, 0x00, 0x00, 0x00, 0x27};

        CommandAPDU SEC_APDU_PIV = new CommandAPDU(apdu_security_piv);

        try {
            sendCard.sendCommand(channel, SEC_APDU_PIV, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        // ; Select PIV Application Issuer Security Domain
        // 00 A4 04 00 07 A0 00 00 01 51 00 00 (9000, 6A82)

        byte[] apdu_security = {0x00, (byte) 0xA4, 0x04, 0x00, 0x07,
            (byte) 0xA0, 0x00, 0x00, 0x01, 0x51, 0x00, 0x00};

        CommandAPDU SEC_APDU = new CommandAPDU(apdu_security);

        try {
            sendCard.sendCommand(channel, SEC_APDU, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }

    @Override
    public byte[] getfacialImageDataSecu() {
        return facialImageDataSecu;
    }
}