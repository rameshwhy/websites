/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.nxp;

import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstantsNXP;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import com.secuera.middleware.cardreader.exceptions.ExceptionMessages;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.core.common.SendCommand;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class ManagePins {
 
    CommonUtil util = new CommonUtil();
    SendCommand sendCard = new SendCommand();
    ExceptionMessages expMsg = new ExceptionMessages();
    
    byte[] soPin_data = {0x00, (byte)0x20, 0x00, (byte) 0x84};
    byte[] userPin_data = {0x00, (byte)0x20, 0x00, (byte) 0x83};
    byte[] changeUserPin_data = {0x00, 0x24, 0x00, (byte) 0x83};
    byte[] changeSoPin_data = {0x00, 0x24, 0x00, (byte) 0x84};    
    
    
     public ManagePins() {
        super();
    }
    
    
     
    public boolean verifyPin(CardChannel channel, String data, String pinType,Integer pinLength) throws MiddleWareException {

        String errorMsg;
        byte[] pinData;
        boolean rtnvalue = false;

        StringBuffer sb = new StringBuffer();
        byte[] command_data;
        

        if (pinType.equalsIgnoreCase(UcmsMiddlewareConstantsNXP.USER_PIN))
         {
             command_data=userPin_data;
         }else if(pinType.equalsIgnoreCase(UcmsMiddlewareConstants.SO_PIN))
         {
               command_data=soPin_data;
         }else
         {
            
              throw new MiddleWareException("Please enter valid PIN Type");
         }
    
        // convert local PIN to Hex
        String hexstr = util.stringToHex(data);
        pinData = util.hexStringToByteArray(hexstr, pinLength);

        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 16) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);

        } else {
            hex_len_pin = Integer.toHexString(len_pin);
        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare verify PIN command
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, pinData);
        CommandAPDU VERIFY_PIN_APDU = new CommandAPDU(command_data);
                  
        // send verify PIN command
        try {
            sendCard.sendCommand(channel, VERIFY_PIN_APDU, sb);

            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }
    
    
    public boolean changePin(CardChannel channel, String pinType,String oldPin, String newPin, Integer pinLength) throws MiddleWareException {
        byte[] oldpinData;
        byte[] newpinData;
        byte[] pinData;
        String errorMsg;
        boolean rtnvalue = false;
        byte[] command_data;
     
        StringBuffer sb = new StringBuffer();

        if (pinType.equalsIgnoreCase(UcmsMiddlewareConstantsNXP.USER_PIN))
         {
             command_data=changeUserPin_data;
         }else if(pinType.equalsIgnoreCase(UcmsMiddlewareConstants.SO_PIN))
         {
              command_data=changeSoPin_data;
         }else
         {
            
              throw new MiddleWareException("Please enter valid PIN Type");
         }
        
        
        //convert old PIN to Hex
        String hexstr = util.stringToHex(oldPin);
        oldpinData = util.hexStringToByteArray(hexstr, pinLength);

        hexstr = "";
        //convert New PIN to Hex
        hexstr = util.stringToHex(newPin);
        newpinData = util.hexStringToByteArray(hexstr, pinLength);

        //Calculate length of Data to be sent.
        pinData = util.combine_data(oldpinData, newpinData);
        int len_pin = pinData.length;

        String hex_len_pin;

        if (len_pin < 15) {
            hex_len_pin = "0" + Integer.toHexString(len_pin);

        } else {
            hex_len_pin = Integer.toHexString(len_pin);

        }

        byte[] byte_len_pin = util.hex1ToByteArray(hex_len_pin);

        // prepare verify PIN Change commands
        command_data = util.combine_data(command_data, byte_len_pin);
        command_data = util.combine_data(command_data, oldpinData);
        command_data = util.combine_data(command_data, newpinData);
        CommandAPDU CHANGE_PIN_APDU = new CommandAPDU(command_data);

        System.out.println("Command " + util.arrayToHex(command_data));

        // send PIN Change command
        try {
            sendCard.sendCommand(channel, CHANGE_PIN_APDU, sb);
            // check if success return true
            if (!sb.toString().equals("9000")) {
                errorMsg = expMsg.GetErrorMsg(sb.toString(), "PIN");
                throw new MiddleWareException(errorMsg);
            } else {
                rtnvalue = true;
            }

            return rtnvalue;

        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }
    
    
    
    
}
