package com.secuera.middleware.core.piv;

import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

public class VerifyDigitalSignature {

    public VerifyDigitalSignature() {
        super();
    }

    public boolean VerifySignature(byte[] input_data, byte[] signature, PublicKey pubkey, String SHASignatureAlgo) throws MiddleWareException {
        boolean lb_verify = false;
        String algo = "";

        Signature sig = null;


        try {

            if (SHASignatureAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.SHA512withRSA)) {
                algo = "SHA512withRSA";
                sig = Signature.getInstance(algo);
            } else if (SHASignatureAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.SHA256withRSA)) {
                algo = "SHA256withRSA";
                sig = Signature.getInstance(algo);
            } else if (SHASignatureAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.SHA1withRSA)) {
                algo = "SHA1withRSA";
                sig = Signature.getInstance(algo);
            } else if (SHASignatureAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.sha1WithECDSA)) {
                algo = "SHA1withECDSA";
                sig = Signature.getInstance(algo, "SunEC");
            } else if (SHASignatureAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.sha224WithECDSA)) {
                algo = "SHA224withECDSA";
                sig = Signature.getInstance(algo, "SunEC");
            } else if (SHASignatureAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.sha256WithECDSA)) {
                algo = "SHA256withECDSA";
                sig = Signature.getInstance(algo, "SunEC");
            } else if (SHASignatureAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.sha384WithECDSA)) {
                algo = "SHA384withECDSA";
                sig = Signature.getInstance(algo, "SunEC");
            } else if (SHASignatureAlgo.equalsIgnoreCase(UcmsMiddlewareConstants.sha512WithECDSA)) {
                algo = "SHA512withECDSA";
                sig = Signature.getInstance(algo, "SunEC");
            }

            sig.initVerify(pubkey);
            sig.update(input_data);

            lb_verify = sig.verify(signature);
        } catch (NoSuchAlgorithmException ex) {
            lb_verify = false;
        } catch (NoSuchProviderException ex) {
            lb_verify = false;
        } catch (InvalidKeyException ex) {
            lb_verify = false;
        } catch (SignatureException ex) {
            lb_verify = false;
        }
        return lb_verify;

    }
}
