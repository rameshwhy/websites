package com.id3.fingerprint;

public class FingerTemplateFormat
{
  public static final int Unknown = 0;
  public static final int Incits378 = 1;
  public static final int Iso19794 = 2;
  public static final int Iso19794CompactCard = 3;
  public static final int Incits378_2009 = 4;
  public static final int Iso19794_2011 = 5;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerTemplateFormat
 * JD-Core Version:    0.6.2
 */