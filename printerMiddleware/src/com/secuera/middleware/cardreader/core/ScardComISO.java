/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

/**
 *
 * @author Harpreet Singh
 */
public class ScardComISO implements Comparable<ScardComISO>{
	short XCC,YCC;
	byte Angle,MinType;
	
        ScardComISO (short xcc, short ycc , byte angle, byte mintype)
	{
		XCC=xcc;
		YCC=ycc;
		Angle=angle;
		MinType=mintype;
	}
	
	int ISOCC (byte[] ISOCC, int offset_pos)
	{
		ISOCC[offset_pos] = (byte)XCC;
		ISOCC[offset_pos+1] = (byte)YCC;
		ISOCC[offset_pos+2] = (byte) ( MinType << 6 |  (Angle >>2 &0x3F));
		return 3;
	}
        
	
	
    @Override
	public int compareTo(ScardComISO n)
	{
		if ( YCC == n.YCC && XCC == n.XCC ) return 0;
		if ( YCC == n.YCC )
			return  XCC < n.XCC ? -1 : 1;
		return YCC < n.YCC ? -1 :1 ;
	}
	
	static ScardComISO getConvertedTemp (byte[] ISO, int offset_pos,long resX , long resY )
	{
		long X = (ISO[offset_pos]&0x003F) <<8 | (ISO[offset_pos+1]&0x00FF);
		long Y = (ISO[offset_pos+2]&0x003F) <<8 | (ISO[offset_pos+3]&0x00FF);
		byte Angle = (byte)(ISO[offset_pos+4]&0x00FF);
		byte MinType= (byte)( (ISO[offset_pos] >> 6) &0x03);

		short x = (short) (( (X*100)+resX/2)/resX);
		short y = (short) (( (Y*100)+resY/2)/resY);
		
		return new ScardComISO (x,y,Angle,MinType);
		
		
	}
}

