 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.core.common.SendCommand;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.Date;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import sun.security.util.DerEncoder;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.util.ObjectIdentifier;

/**
 *
 *
 * @author Harpreet Singh
 */
public class LoadSecurityCont {

    SendCommand sendCard = new SendCommand();
    CommonUtil util = new CommonUtil();
    PivUtilInterface piv = new PivUtil();
    GenAsmSig asmSig = new GenAsmSig();
    GeneralAuth auth = new GeneralAuth();
    /* Declare OIDS here*/
    final int Signed_data[] = {1, 2, 840, 113549, 1, 7, 2};  //- signedData OID
    final int LDS_Security_Object[] = {1, 3, 27, 1, 1, 1}; //LDS Security object OID
   
    private byte[] uniNullSeperator = {0x05, 0x00};
    private byte[] anyTag = {(byte) 0xa0};
    private byte[] seqTag = {0x30};
    private int[] Digest_data;
    private boolean lbSkip_PrintedCont = true;
    private boolean lbSkip_ChuidCont = true;
    private boolean lbSkip_FingerCont = true;
    private boolean lbSkip_FacialCont = true;

    public boolean loadSecCont(CardChannel channel, byte[] Printed_info, byte[] Chuid_info, byte[] Finger_info,
            byte[] Facial_info, PrivateKey privateKey, byte[] cert, Date dt_signingtime,
            byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY, String algoType) throws MiddleWareException {

        DerOutputStream out_1;
        out_1 = new DerOutputStream();
        byte[] asmSignature;
        byte[] LDS_Object;
        byte[] SecurityData;
        boolean  retValue=false;
        /*
        if (algoType.equalsIgnoreCase("SHA256")) {
        Digest_data = Digest_SHA256_data;
        } else if (algoType.equalsIgnoreCase("SHA512")) {
        Digest_data = Digest_SHA512_data;
        } else if (algoType.equalsIgnoreCase("SHA1")) {
        Digest_data = Digest_SHA256_data;
        }
         */

        if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC224)) {
            Digest_data = UcmsMiddlewareConstants.Digest_SHA224_data;
        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC256)) {
            Digest_data = UcmsMiddlewareConstants.Digest_SHA256_data;
        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.ECC384)) {
            Digest_data = UcmsMiddlewareConstants.Digest_SHA384_data;
        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.RSA1024)) {
            Digest_data = UcmsMiddlewareConstants.Digest_SHA256_data;
        } else if (algoType.equalsIgnoreCase(UcmsMiddlewareConstants.RSA2048)) {
            Digest_data = UcmsMiddlewareConstants.Digest_SHA512_data;
        }





        try {
            LDS_Object = LDSObject(Printed_info, Chuid_info, Finger_info, Facial_info);

            String str_hex_input = util.arrayToHex(LDS_Object);

            // Now get the Signed Object.
            asmSignature = util.hex1StringToByteArray(asmSig.genAsmSig(str_hex_input, privateKey, cert, "", dt_signingtime, 'S', algoType));

            //Wrap LDS object in octet string.
            out_1.putOctetString(LDS_Object);
            //Again wrap in outer sequence and then replace the sequence tag with any tag.
            LDS_Object = wrapInSequence(out_1.toByteArray());
            LDS_Object = util.combine_data(anyTag, util.strip_data(LDS_Object, 1));

            //Now add header OID's to the LDS Object.
            LDS_Object = addLDSHeader(LDS_Object);

            SecurityData = util.combine_data(LDS_Object, asmSignature);

            //Now add Signed data OID and put the whole data in Sequence.
            byte[] signedData_oid = getSignedDataOID();
            SecurityData = util.combine_data(signedData_oid, SecurityData);


            Integer len = SecurityData.length;
            byte[] tot_len = util.calcBertValue(len, false, false);

            byte[][] sec_data = new byte[3][];
            sec_data[0] = seqTag;
            sec_data[1] = tot_len;
            sec_data[2] = SecurityData;

            SecurityData = util.combine_data(sec_data);

            out_1.close();

            //Now load the security data on the card.
            retValue = loadCont(channel, PRE_PERSO_PIV_ADMIN_KEY, PIV_ADMIN_KEY, SecurityData);
        } catch (IOException ex) {
            throw new MiddleWareException(ex.getMessage());
        }
        return retValue;
    }

    private byte[] LDSObject(byte[] Printed_info, byte[] Chuid_info, byte[] Finger_info,
            byte[] Facial_info) throws IOException, MiddleWareException {


        byte[] lds_ver = {0x02, 0x01, 0x00};   // This represents version nbr of LDS Security object.
        byte[] digest_algo;
        byte[] group_hash;

        byte[] LDS_Object;
        //Get Digest Algo.
        digest_algo = getDigestAlgo(false);
        //Get dataGroupHashValues
        group_hash = getGroupHash(Printed_info, Chuid_info, Finger_info, Facial_info);


        byte[][] grp_arrays = new byte[4][];
        grp_arrays[0] = lds_ver;
        grp_arrays[1] = digest_algo;
        grp_arrays[2] = uniNullSeperator;
        grp_arrays[3] = group_hash;

        byte[] grp_bytes = util.combine_data(grp_arrays);


        Integer len = grp_bytes.length;
        byte[] tot_len = util.calcBertValue(len, false, false);

        byte[][] grp_arrays_seq = new byte[3][];
        grp_arrays_seq[0] = seqTag;
        grp_arrays_seq[1] = tot_len;
        grp_arrays_seq[2] = grp_bytes;

        LDS_Object = util.combine_data(grp_arrays_seq);

        return LDS_Object;
    }

    private byte[] getGroupHash(byte[] Printed_info, byte[] Chuid_info, byte[] Finger_info, byte[] Facial_info) throws MiddleWareException,IOException {

        byte[] printed_hash = null;
        byte[] chuid_hash = null;
        byte[] finger_hash = null;
        byte[] facial_hash = null;
        byte[] der_printed = null;
        byte[] der_chuid;
        byte[] der_finger;
        byte[] der_facial;
        byte[] grp_bytes = null;
        byte[] grp_hash;



        if (!(Printed_info == null) && !(Printed_info.length == 0)) {
            printed_hash = getMessageHash(Printed_info);
            der_printed = getDerMessageDigest(printed_hash, 1);
            grp_bytes = der_printed;
            lbSkip_PrintedCont = false;
        }

        if (!(Chuid_info == null) && !(Chuid_info.length == 0)) {
            chuid_hash = getMessageHash(Chuid_info);
            der_chuid = getDerMessageDigest(chuid_hash, 2);
            if (!(grp_bytes == null) && !(grp_bytes.length == 0)) {
                grp_bytes = util.combine_data(grp_bytes, der_chuid);
            } else {
                grp_bytes = der_chuid;
            }

            lbSkip_ChuidCont = false;

        }

        if (!(Finger_info == null) && !(Finger_info.length == 0)) {
            finger_hash = getMessageHash(Finger_info);
            der_finger = getDerMessageDigest(finger_hash, 3);
            if (!(grp_bytes == null) && !(grp_bytes.length == 0)) {
                grp_bytes = util.combine_data(grp_bytes, der_finger);
            } else {
                grp_bytes = der_finger;
            }
            lbSkip_FingerCont = false;

        }

        if (!(Facial_info == null) && !(Facial_info.length == 0)) {
            facial_hash = getMessageHash(Facial_info);
            der_facial = getDerMessageDigest(facial_hash, 4);
            if (!(grp_bytes == null) && !(grp_bytes.length == 0)) {
                grp_bytes = util.combine_data(grp_bytes, der_facial);
            } else {
                grp_bytes = der_facial;
            }
            lbSkip_FacialCont = false;
        }


        Integer len = grp_bytes.length;
        byte[] tot_len = util.calcBertValue(len, false, false);


        byte[][] grp_arrays = new byte[3][];
        grp_arrays[0] = seqTag;
        grp_arrays[1] = tot_len;
        grp_arrays[2] = grp_bytes;

        grp_hash = util.combine_data(grp_arrays);

        return grp_hash;

    }

    private byte[] computeGrpHeader() {

        // Here are listed all the container ID's which may be mapped/loaded to
        // security container.
        // Card Capability Container 0xDB00
        // CHUID 0x3000
        // FingerPrint 0x6010
        // Facial Image 0x6030
        // Printed Info 0x3001
        // Discovery Object 0x6050
        // Iris Image 0x1015
        // Now Define Datagroups to container mapping.

        byte[] map_tag = {(byte) 0xBA}; // Tag indicating start of mapping DG1 to containers.
        byte[] printed_cont = {0x01, 0x30, 0x01}; //Printed Info container mapped to DG1.
        byte[] chuid_cont = {0x02, 0x30, 0x00}; //CHUID container mapped to DG2.
        byte[] finger_cont = {0x03, 0x60, 0x10}; //Finger Print container mapped to DG2.
        byte[] facial_cont = {0x04, 0x60, 0x30}; //Facial container mapped to DG4.
        byte[] mapBytes=null;


        if (!lbSkip_PrintedCont) {
            mapBytes = printed_cont;

        }

        if (!lbSkip_ChuidCont) {
            if (!(mapBytes == null) && !(mapBytes.length == 0)) {
                mapBytes = util.combine_data(mapBytes, chuid_cont);
            } else {
                mapBytes = chuid_cont;
            }

        }

        if (!lbSkip_FingerCont) {
            if (!(mapBytes == null) && !(mapBytes.length == 0)) {
                mapBytes = util.combine_data(mapBytes, finger_cont);
            } else {
                mapBytes = finger_cont;
            }

        }
        if (!lbSkip_FacialCont) {
            if (!(mapBytes == null) && !(mapBytes.length == 0)) {
                mapBytes = util.combine_data(mapBytes, facial_cont);
            } else {
                mapBytes = facial_cont;
            }

        }


        // Total length of DG mapping.
        Integer len = mapBytes.length;
        byte[] map_len = util.calcBertValue(len, false, false);


        byte[][] grp_arrays = new byte[3][];
        grp_arrays[0] = map_tag;
        grp_arrays[1] = map_len;
        grp_arrays[2] = mapBytes;


        byte[] grpHeader = util.combine_data(grp_arrays);

        return grpHeader;
    }


    

    public boolean loadCont(CardChannel channel, byte[] PRE_PERSO_PIV_ADMIN_KEY, byte[] PIV_ADMIN_KEY,
            byte[] secData) throws MiddleWareException {

        // TO write Security Container
        // Security Object (PIV) 0x9000
        // Data Element (TLV) Tag Type Max. Bytes
        // Mapping of DG to container ID 0xBA Variable 100
        // LDS Security Object (MRTDDocument SO) 0xBB Variable 900
        // Error Detection Code 0xFE LRC 0

        // 00 DB 3F FF 00
        // 03 5D 5C 03 5F C1 06
        // 53 82 03 54 BA 15 01 DB 00
        // 02 30 00 03 60 10 05 60 30 06 30 01 07 60 50 09
        // 10 15 BB 82 03 37 30 82 03 33 06 09 2A 86 48 86
        // F7 0D 01 07 02 A0 82 03 24 30 82 03 20 02 01 03
        // 31 0F 30 0D 06 09 60 86 48 01 65 03 04 02 01 05
        // 00 30 82 01 3A 06 05 2B 1B 01 01 01 A0 82 01 2F
        // 04 82 01 2B 30 82 01 27 02 01 00 30 0D 06 09 60



        boolean rtnvalue = true;
        StringBuffer sb = new StringBuffer();
        byte[] apdu_security = {0x00, (byte) 0xDB, 0x3F, (byte) 0xFF};
        byte[] error_tag = {(byte) 0xFE, 0x00};

        Integer len = secData.length;
        byte[] byte_LDS_len = util.calcBertValue(len, false, false);

        //Get Admin Authentication with pre_perso_key if fail use diversified key.
        try {

            auth.generalAuth(channel, PRE_PERSO_PIV_ADMIN_KEY);
        } catch (MiddleWareException e) {
            try {
                auth.generalAuth(channel, PIV_ADMIN_KEY);
            } catch (MiddleWareException m) {
                throw new MiddleWareException(m.getMessage());
            }
        }



        //////////////////////////////////////////////////////
        byte[] cont_id = {0x5c, 0x03, 0x5F, (byte) 0xC1, 0x06}; //For Security Container

        byte[] grpHeader = computeGrpHeader();

        // Now define Security object Tag.
        byte[] security_tag = {(byte) 0xBB};

        ///////////////////////////////////////////////////////
        byte[][] grp_arrays = new byte[5][];
        grp_arrays[0] = grpHeader;
        grp_arrays[1] = security_tag;
        grp_arrays[2] = byte_LDS_len;
        grp_arrays[3] = secData;
        grp_arrays[4] = error_tag;

        byte[] grp_bytes = util.combine_data(grp_arrays);

        //Calculate size of the total Security data record to be stored.
        Integer len_sec = grp_bytes.length;
        byte[] byte_sec_len = util.calcBertValue(len_sec, true, false);

        byte[] apdu_sec_data = util.combine_data(byte_sec_len, grp_bytes);
        apdu_sec_data = util.combine_data(cont_id, apdu_sec_data);

        Integer len_total_apdu = apdu_sec_data.length;
        byte[] total_size = util.calcBertValue(len_total_apdu, false, true);

        byte[] zero_size = {0x00};

        if (total_size.length > 1) {
            total_size = util.combine_data(zero_size, total_size);
        }

        apdu_sec_data = util.combine_data(total_size, apdu_sec_data);
        apdu_sec_data = util.combine_data(apdu_security, apdu_sec_data);


        System.out.println("Security_data_final:" + util.arrayToHex(apdu_sec_data));

        CommandAPDU READ_APDU = new CommandAPDU(apdu_sec_data);

        try {

            sendCard.sendCommand(channel, READ_APDU, sb);

            //Raise Error if APDU command is not successful
            if (!sb.toString().equals("9000")) {
                StringBuilder errMsg = new StringBuilder();
                errMsg.append("Unable to write Security Object");
                errMsg.append(" (").append(sb.toString()).append(")");
                throw new MiddleWareException(errMsg.toString());
            }
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

        return rtnvalue;
    }

    private byte[] getMessageHash(byte[] input_data) throws MiddleWareException,IOException {

        byte[] hash;
        String str_algo;
        /*
        if (Digest_data == Digest_SHA256_data) {
            str_algo = "SHA-256";
        } else {
            str_algo = "SHA-512";
        }
         */
        if (Digest_data == UcmsMiddlewareConstants.Digest_SHA256_data) {
            str_algo = "SHA-256";
        } else if (Digest_data == UcmsMiddlewareConstants.Digest_SHA384_data){
            str_algo = "SHA-384";
        }else if(Digest_data == UcmsMiddlewareConstants.Digest_SHA224_data){
            str_algo = "SHA-224";
        }else {
            str_algo = "SHA-512";
        }
        
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance(str_algo);
        } catch (NoSuchAlgorithmException ex) {
            // TODO Auto-generated catch block
            throw new MiddleWareException(ex.getMessage());
        }
        digest.update(input_data);
        hash = digest.digest();


        return hash;

    }

    private byte[] getSignedDataOID() throws MiddleWareException, IOException {

        byte[] signedData = null;

        DerOutputStream out_1;
        out_1 = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier Signed_data_OID = ObjectIdentifier.newInternal(Signed_data);
        out_1.putOID(Signed_data_OID);

        signedData = out_1.toByteArray();
        out_1.close();

        return signedData;

    }

    private byte[] getDigestAlgo(boolean setTag) throws MiddleWareException, IOException {

        byte[] digestAlgo = null;

        DerOutputStream out_1, out_2;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();

        // Out put the OID
        ObjectIdentifier Digest_data_OID = ObjectIdentifier.newInternal(Digest_data);
        out_1.putOID(Digest_data_OID);

        DerValue attr_oid = new DerValue(out_1.toByteArray());

        DerValue[] x = {attr_oid};
        out_2.putSequence(x);

        digestAlgo = out_2.toByteArray();

        if (setTag) {
            // Encode the values in Set
            DerOutputStream out_3;
            out_3 = new DerOutputStream();

            DerEncoder[] y = {out_2};
            out_3.putOrderedSet(DerValue.tag_Set, y);
            digestAlgo = out_3.toByteArray();

            out_3.close();
        }


        out_1.close();
        out_2.close();

        return digestAlgo;

    }

    private byte[] addLDSHeader(byte[] input_data) throws MiddleWareException, IOException {

        DerOutputStream out_1;
        out_1 = new DerOutputStream();
        byte[] headerFormat = null;
        byte[] lds_ver = {0x02, 0x01, 0x03};   // This represents version nbr of Signed object.


        //Out put the OID
        ObjectIdentifier LDS_Security_Object_OID = ObjectIdentifier.newInternal(LDS_Security_Object);
        out_1.putOID(LDS_Security_Object_OID);

        byte[] sec_data = util.combine_data(out_1.toByteArray(), input_data);


        Integer len = sec_data.length;
        byte[] tot_len = util.calcBertValue(len, false, false);

        byte[][] sec_arrays = new byte[3][];
        sec_arrays[0] = seqTag;
        sec_arrays[1] = tot_len;
        sec_arrays[2] = sec_data;


        sec_data = util.combine_data(sec_arrays);

        /**
         * ***********
         */
        // Now get the set of digestAlgorithms
        byte[] set_of_algo = getDigestAlgo(true);

        /**
         * *********
         */
        len = sec_data.length + uniNullSeperator.length + set_of_algo.length + lds_ver.length;
        tot_len = util.calcBertValue(len, false, false);

        byte[][] hdr_arrays = new byte[6][];
        hdr_arrays[0] = seqTag;
        hdr_arrays[1] = tot_len;
        hdr_arrays[2] = lds_ver;
        hdr_arrays[3] = set_of_algo;
        hdr_arrays[4] = uniNullSeperator;
        hdr_arrays[5] = sec_data;


        byte[] hdr_bytes = util.combine_data(hdr_arrays);

        //Again wrap in outer sequence and then replace the sequence tag with any tag.
        headerFormat = wrapInSequence(hdr_bytes);
        headerFormat = util.combine_data(anyTag, util.strip_data(headerFormat, 1));


        return headerFormat;

    }

    private byte[] wrapInSequence(byte[] input_data) throws MiddleWareException, IOException {

        byte[] output_data = null;

        DerOutputStream out_1;
        out_1 = new DerOutputStream();

        // Out put the OID
        DerValue attr_oid = new DerValue(input_data);

        DerValue[] x = {attr_oid};
        out_1.putSequence(x);

        output_data = out_1.toByteArray();

        out_1.close();


        return output_data;

    }

    private byte[] getDerMessageDigest(byte[] hash, int group_nbr) throws IOException {

        byte[] derHash = null;

        DerOutputStream out_1, out_2, out_hash;
        out_1 = new DerOutputStream();
        out_2 = new DerOutputStream();
        out_hash = new DerOutputStream();

        //Get Value of the Hash of original message.
        out_1.putInteger(group_nbr);
        out_2.putOctetString(hash);

        DerValue attr_grp = new DerValue(out_1.toByteArray());
        DerValue attr_value = new DerValue(out_2.toByteArray());

        DerValue[] x = {attr_grp, attr_value};
        out_hash.putSequence(x);

        derHash = out_hash.toByteArray();

        out_1.close();
        out_2.close();
        out_hash.close();

        return derHash;

    }
}
