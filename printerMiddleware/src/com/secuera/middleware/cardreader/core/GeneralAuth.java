/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;
import com.secuera.middleware.cardreader.entities.MiddleWareException;

/**
 *
 * @author admin
 */
public class GeneralAuth {

    private CommonUtil util = new CommonUtil();
    private SendCommand sendCard = new SendCommand();

    public void generalAuth(CardChannel channel, byte[] key_p) throws MiddleWareException {


        StringBuffer sb = new StringBuffer();
        byte[] res = null;
        byte[] command_data;
        byte[] command_AES = {0x00, (byte) 0x87, 0x08, (byte) 0x9B, 0x04,
            0x7C, 0x02, (byte) 0x81, 0x00, 0x0C};

        byte[] command_DES = {0x00, (byte) 0x87, 0x03, (byte) 0x9B, 0x04,
            0x7C, 0x02, (byte) 0x81, 0x00, 0x0C};


        // If length is 16 , then it's AES key
        if (key_p.length == 16) {
            command_data = command_AES;
        } else {
            command_data = command_DES;
        }




        //required before authorisation
        checkPIVInstance(channel);

        //prepare APDU command for authorisation
        CommandAPDU GENERAL_AUTH_APDU = new CommandAPDU(command_data);

        // System.out.println("1st command" + util.arrayToHex(command_data));

        try {

            res = sendCard.sendCommand(channel, GENERAL_AUTH_APDU, sb);


            if (!sb.toString().equals("9000")) {
                throw new MiddleWareException("Error Authenticating the Card. Error Code = " + sb);
            }

        } catch (CardException e1) {
            throw new MiddleWareException("Error Authenticating the Card.");
        }


        byte[] ret_encrypt;
        byte[] command_ret;
        byte[] command_ret_DES = {0x00, (byte) 0x87, 0x03, (byte) 0x9B, 0x0C,
            0x7C, 0x0A, (byte) 0x82, 0x08};
        byte[] command_ret_AES = {0x00, (byte) 0x87, 0x08, (byte) 0x9B, 0x14,
            0x7C, 0x12, (byte) 0x82, 0x10};

        // Now send the 2nd command to complete general authentication.

        if (key_p.length == 16) {
            ret_encrypt = aesencrypt(key_p, util.strip_data(res, 4));
            command_ret = command_ret_AES;

        } else {
            ret_encrypt = desencrypt(key_p, util.strip_data(res, 4));

            command_ret = command_ret_DES;

        }



        byte[] command_ret_auth = util.combine_data(command_ret, ret_encrypt);

        CommandAPDU GENERAL_AUTH_APDU_RET = new CommandAPDU(command_ret_auth);

        try {

            sendCard.sendCommand(channel, GENERAL_AUTH_APDU_RET, sb);
            if (!sb.toString().equals("9000")) {
                throw new MiddleWareException("Error Authenticating the Card. Error Code = " + sb);
            }

        } catch (CardException e1) {
            throw new MiddleWareException("Error Authenticating the Card.");

            // TODO Auto-generated catch block
            // e1.printStackTrace();
        }
       
    }

    public byte[] desencrypt(byte[] key, byte[] from_card) throws MiddleWareException {
        try {
            // Create encrypter/decrypter class
            DesEncrypter encrypter = new DesEncrypter(key);

            // Encrypt
            byte[] res = encrypter.encrypt(from_card);

            return res;
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    public byte[] aesencrypt(byte[] key, byte[] from_card) throws MiddleWareException {
        try {
            // Create encrypter/decrypter class
            AesEncrypter encrypter = new AesEncrypter(key);

            // Encrypt
            byte[] res = encrypter.encrypt(from_card);

            return res;
        } catch (MiddleWareException e) {
            throw new MiddleWareException(e.getMessage());
        }
    }

    private void checkPIVInstance(CardChannel channel) throws MiddleWareException {

        // ; Check PIV instance FCI
        // 00 A4 04 00 09 A0 00 00 03 08 00 00 10 00 00 [ \

        StringBuffer sb = new StringBuffer();
        byte[] apdu_security_piv = {0x00, (byte) 0xA4, 0x04, 0x00, 0x09,
            (byte) 0xA0, 0x00, 0x00, 0x03, 0x08, 0x00, 0x00, 0x10, 0x00,
            0x00};

        CommandAPDU SEC_APDU_PIV = new CommandAPDU(apdu_security_piv);

        try {
            sendCard.sendCommand(channel, SEC_APDU_PIV, sb);
        } catch (CardException e) {
            throw new MiddleWareException(e.getMessage());
        }

    }
}
