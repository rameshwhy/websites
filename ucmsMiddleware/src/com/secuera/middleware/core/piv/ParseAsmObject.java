package com.secuera.middleware.core.piv;

import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import sun.security.x509.X509CertImpl;

public class ParseAsmObject {

    // Author: Harpreet Singh
    CommonUtil util = new CommonUtil();
    VerifyDigitalSignature verify = new VerifyDigitalSignature();
    // This is OID for PIV-CHUIDSecurity Object.
    String PIVChuidSecurity = "6086480165030601";
    String SHASignatureAlgo;
    String RSAContentType = "2a864886f70d010903";

    public ParseAsmObject() {
        super();
    }

    public boolean parseChuidObj(String str_hex_chuid) throws MiddleWareException {

        PublicKey pubkey;
        byte[] signed_data;
        byte[] signature_data;
        boolean lb_verify = false;

        // get Public Key.
        pubkey = getPublicKey(str_hex_chuid);
        System.out.println(pubkey.toString());
        if (pubkey == null) {
            lb_verify = false;
            return lb_verify;
        }

        signed_data = getSignedData(str_hex_chuid);

        signature_data = getSignature(str_hex_chuid);

        if (signature_data == null) {
            lb_verify = false;
            return lb_verify;
        }
        lb_verify = verify.VerifySignature(signed_data, signature_data, pubkey, SHASignatureAlgo);
        return lb_verify;

    }

    private byte[] getSignedData(String str_hex_chuid) {
        int i_startRSA, i_startcontent;
        String any_tag = "a0";
        String str_content_len, str_content;
        int contentLen, i_start;

        i_startRSA = str_hex_chuid.toLowerCase().indexOf(RSAContentType.toLowerCase());
        i_startcontent = str_hex_chuid.toLowerCase().lastIndexOf(any_tag, i_startRSA);

        str_hex_chuid = str_hex_chuid.substring(i_startcontent);

        if (str_hex_chuid.substring(2, 4).equals("82")) {
            str_content_len = str_hex_chuid.substring(4, 8);
            i_start = 8;
        } else if (str_hex_chuid.substring(2, 4).equals("81")) {
            str_content_len = str_hex_chuid.substring(4, 6);
            i_start = 6;
        } else {
            str_content_len = str_hex_chuid.substring(4);
            i_start = 4;
        }
        contentLen = Integer.valueOf(str_content_len, 16).intValue();
        str_content = str_hex_chuid.substring(2, i_start + contentLen * 2);
        str_content = "31" + str_content;


        return util.hex1ToByteArray(str_content);

    }

    public PublicKey getPublicKey(String str_hex_chuid)  throws MiddleWareException {
        int i_startcert, certLen;
        String any_tag = "A0";
        String str_cert;
        byte[] cert_encoded;

        //Find End of "PIVChuidSecurity" OID and add 1 to get to start of Certificate
        i_startcert = util.findEndLocation(str_hex_chuid, PIVChuidSecurity);

        //a0 82 03 71
        if ((str_hex_chuid.substring(i_startcert, i_startcert + 2)).equalsIgnoreCase(any_tag)) {

            // Get Certificate data
            String cert_length = str_hex_chuid.substring(i_startcert + 4, i_startcert + 8);
            certLen = Integer.valueOf(cert_length, 16).intValue();
            str_cert = str_hex_chuid.substring(i_startcert + 8, i_startcert + 8 + certLen * 2);

            cert_encoded = util.hex1ToByteArray(str_cert);
            // parse PublicKey out of certificate data.
            X509CertImpl cert = null;
            try {
                cert = new X509CertImpl(cert_encoded);
            } catch (CertificateException ex) {
                throw new MiddleWareException(ex.getMessage());
            }

            return cert.getPublicKey();

        } else {
            return null;
        }


    }

    private byte[] getSignature(String str_hex_chuid) {

        int i_startsig = 0, sigLen;
        // String octet_tag = "04";
        String str_sig;
        byte[] sig_encoded;
        String strNullSeprator = "05";
        boolean loopVar = true;
        int i = 0;
        String sig_length;

        while (loopVar == true) {
            //Find End of "SHA512WithRSA" OID and add 1 to get to start of Certificate
            //i_startsig = util.findEndLocation(str_hex_chuid, SHAWithRSASIG[i]);
            i_startsig = util.findFromEnd(str_hex_chuid, UcmsMiddlewareConstants.SHAWithSIG[i]);

            if (i_startsig > 0 || i == UcmsMiddlewareConstants.SHAWithSIG.length - 1) {
                SHASignatureAlgo = UcmsMiddlewareConstants.SHAWithSIG[i];
                loopVar = false;
            }
            i++;
        }



        if (i_startsig == 0) {

            sig_encoded = null;
        } else {


            if (str_hex_chuid.substring(i_startsig, i_startsig + 2).equalsIgnoreCase(strNullSeprator)) {
                sig_length = str_hex_chuid.substring(i_startsig + 8, i_startsig + 12);
                i_startsig = i_startsig + 4;
            } else {
                sig_length = str_hex_chuid.substring(i_startsig + 4, i_startsig + 8);
            }



            sigLen = Integer.valueOf(sig_length, 16).intValue();

            str_sig = str_hex_chuid.substring(i_startsig + 8, i_startsig + 8 + sigLen * 2);
            sig_encoded = util.hex1ToByteArray(str_sig);

        }

        return sig_encoded;

    }
}
