/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;


import java.util.List;
/**
 *
 * @author admin
 */
public class FldDataforCard {
	List flddata;
	List fldsizes;

	public FldDataforCard(List flddata, List fldsizes) {
		this.flddata = flddata;
		this.fldsizes = fldsizes;
	}

	public FldDataforCard() {

	}

	public List getFlddata() {
		return flddata;
	}

	public void setFlddata(List flddata) {
		this.flddata = flddata;
	}

	public List getFldsizes() {
		return fldsizes;
	}

	public void setFldsizes(List fldsizes) {
		this.fldsizes = fldsizes;
	}

}
