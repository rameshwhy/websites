// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.ica;

import eu.gnome.morena.DeviceBase;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class ICADevice extends DeviceBase
{

    protected void fireTransferDone(String s)
    {
        if(reporter != null)
            reporter.cancel();
        super.fireTransferDone((new File(s)).getAbsolutePath());
    }

    protected void fireTransferFailed(int i, String s)
    {
        if(reporter != null)
            reporter.cancel();
        super.fireTransferFailed(i, s);
    }

    ICADevice(String s, long l)
    {
        super(s, l);
        timer = null;
        reporter = null;
    }

    protected Timer timer;
    protected TimerTask reporter;
}
