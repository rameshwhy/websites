// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.ica;

import eu.gnome.morena.*;
import java.util.*;

// Referenced classes of package eu.gnome.morena.ica:
//            ICAScanner, ICACamera, ICAConstants

public class ICAManager extends Manager
    implements ICAConstants
{

    private native void _initialize();

    private native void _close();

    private void add(String s, long l, int i)
    {
        Object obj = Configuration.isScanner(s, (i & 2) != 0) ? ((Object) (new ICAScanner(s, l))) : ((Object) (new ICACamera(s, l)));
        devices.add(obj);
        fireDeviceConnected(((Device) (obj)));
        System.out.println((new StringBuilder()).append("Adding device 0x").append(Long.toHexString(l)).append(" ").append(s).toString());
    }

    private void remove(String s, long l)
    {
        Iterator iterator = devices.iterator();
        do
        {
            if(!iterator.hasNext())
                break;
            Device device = (Device)iterator.next();
            if(!s.equals(device.toString()))
                continue;
            devices.remove(device);
            fireDeviceDisconnected(device);
            System.out.println((new StringBuilder()).append("Removing device 0x").append(Long.toHexString(l)).append(" ").append(s).toString());
            break;
        } while(true);
    }

    private ICAManager()
    {
        isInitialized = false;
        isAvailable = false;
        devices = new Vector();
    }

    public List listDevices()
    {
        return devices;
    }

    public boolean available()
    {
        if (!this.isInitialized)
    	      try
    	      {
    	        _initialize();
    	        this.isAvailable = true;
    	        synchronized (this)
    	        {
    	          wait(1000L);
    	        }
    	      }
    	      catch (Throwable localThrowable)
    	      {
    	      }
    	      finally
    	      {
    	        this.isInitialized = true;
    	      }
    	    return this.isAvailable;
    }

    public void close()
    {
        _close();
    }

    public String toString()
    {
        return "ICA";
    }

    public static final ICAManager INSTANCE = new ICAManager();
    private boolean isInitialized;
    private boolean isAvailable;
    private List devices;

}
