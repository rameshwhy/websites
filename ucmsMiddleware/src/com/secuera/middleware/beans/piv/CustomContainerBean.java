/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.beans.piv;

/**
 *
 * @author Satish K
 */
public class CustomContainerBean {
    private byte[] containerID;
    private int containerSize;
    private String readAccessRequired;
    
    public byte[] getContainerID() {
        return containerID;
    }

    public void setContainerID(byte[] containerID) {
        this.containerID = containerID;
    }

    public int getContainerSize() {
        return containerSize;
    }

    public void setContainerSize(int containerSize) {
        this.containerSize = containerSize;
    }

    public String getReadAccessRequired() {
        return readAccessRequired;
    }

    public void setReadAccessRequired(String readAccessRequired) {
        this.readAccessRequired = readAccessRequired;
    }

  

    
    
    
}
