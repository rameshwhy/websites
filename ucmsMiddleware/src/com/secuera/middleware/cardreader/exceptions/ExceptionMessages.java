package com.secuera.middleware.cardreader.exceptions;

//import com.secuera.entities.MiddleWareException;
public class ExceptionMessages {

    public static String INVALID_LOCAL_PIN_EXCEPTION = "Invalid PIN.";
    public static String INVALID_GLOBAL_PIN_EXCEPTION = "Invalid Global PIN.";
    public static String INVALID_BIO_EXCEPTION = "Invalid Finger Print.";    
    public static String PIN_LENGTH_EXCEPTION = "Invalid PIN Length.";    
    public static String CARD_COMMUNICATION_EXCEPTION = "Unable to Communicate with Card.";
    public static String NO_CARD_TERMINAL_EXCEPTION = "No Card Reader terminal detected.";
    public static String NO_CARD_EXCEPTION = "Missing Card.";
    //exceptions in card initialization
    public static String INIT_LOCAL_PIN = "Unable to Upload Local PIN.";
    public static String INIT_GLOBAL_PIN = "Unable to Upload Global PIN.";
    public static String INIT_UNLOCK_PIN = "Unable to Upload Unlock PIN.";
    public static String INIT_KEY_9B03 = "Unable to upload Key 9B03";
    public static String INIT_KEY_9E08 = "Unable to upload Key 9E08";
    public static String INIT_KEY_9B08 = "Unable to upload Key 9B08";
    public static String INIT_GPKEYS = "Unable to Inject Custom GPKeys";
    public static String INIT_SECURE_CH101 = "Unable to open Secure Channel 101";
    public static String INIT_SECURE_CH103 = "Unable to open Secure Channel 103";
    //Lock Card
    public static String CARD_LOCK_EXCEPTION = "Unable to Lock The Card";
    public static String DISABLECONTACTLESS_EXCEPTION = "Unable to disable contact less";
    public static String INVALID_SIGNATURE_EXCEPTION = "Invalid Signature";
    public static String INVALID_CERTIFICATE_LENGTH = "Certificate Length is invalid";
    public static String CARD_UNLOCK_EXCEPTION = "Unable to UnLock The Card";
    public static String USE_PHASE_COMMAND ="Unable to en Pesonalization.";

    public static String FingerImageReadError = "Error Reading Finger Print information.";
    
     // Exceptions in Assymetric key Injection
    public static String ECCKEY256 = "Unable to Inject ECC256 Key ";
    public static String ECCKEY384 = "Unable to Inject ECC384 Key ";
    public static String RSAKEY2048 = "Unable to Inject RSA2048 Key ";
    public static String RSAKEY1024 = "Unable to Inject RSA1024 Key ";
    
    public String GetErrorMsg(String errorCode, String modType) {
        StringBuilder retValue = new StringBuilder();
        retValue.append("");


        if (modType.equalsIgnoreCase("PIN")) {

            System.out.println(errorCode.substring(0, 3));
            if (errorCode.substring(0, 3).equalsIgnoreCase("63C")) {
                retValue.append("Invalid PIN.").append(errorCode);
            }

            if (errorCode.equalsIgnoreCase("6983")) {
                retValue.append("PIN is Blocked. Please run Unblock Local PIN.").append(errorCode);
            }
        }



        if (errorCode.equalsIgnoreCase("6e00")) {

            retValue.append("Card is Locked, please run Unlock the card function.").append(errorCode);

        }

        if (errorCode.equalsIgnoreCase("6a88")) {

            retValue.append("Reference data not not found.").append(errorCode);

        }

        if (errorCode.equalsIgnoreCase("6a80")) {

            retValue.append("Reference data not not found.").append(errorCode);

        }
        if (errorCode.equalsIgnoreCase("6d00")) {

            retValue.append("instruction code not supported or invalid.").append(errorCode);

        }

        if (errorCode.substring(0, 2).equalsIgnoreCase("6C")) {
            retValue.append("Wrong length Le,SW2 indicates the correct Length.").append(errorCode);
        }





        if (retValue.toString().equalsIgnoreCase("")) {
            retValue.append("Unknown Error.").append(errorCode);
        }

        return retValue.toString();
    }
}
