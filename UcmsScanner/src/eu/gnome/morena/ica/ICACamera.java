// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) deadcode 

package eu.gnome.morena.ica;

import eu.gnome.morena.*;

// Referenced classes of package eu.gnome.morena.ica:
//            ICADevice

public class ICACamera extends ICADevice
    implements Camera, Runnable
{

    private native void _initialize(long l);

    private native void _transfer(long l)
        throws MorenaException;

    ICACamera(String s, long l)
    {
        super(s, l);
        _initialize(l);
    }

    public void run()
    {
        try
        {
            _transfer(handle);
        }
        catch(MorenaException morenaexception)
        {
            transferDoneListener.transferFailed(morenaexception.getErrorCode(), morenaexception.getMessage());
        }
        catch(Throwable throwable)
        {
            transferDoneListener.transferFailed(-1, throwable.toString());
        }
    }

    public void startTransfer(TransferDoneListener transferdonelistener)
        throws Exception
    {
        super.startTransfer(transferdonelistener);
        (new Thread(this)).start();
    }
}
