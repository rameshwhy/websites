/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.core.piv;

import com.secuera.middleware.core.common.CommonUtil;
import com.secuera.middleware.beans.piv.FacialBean;
import com.secuera.middleware.cardreader.constants.UcmsMiddlewareConstants;
import com.secuera.middleware.cardreader.entities.MiddleWareException;
import java.security.PublicKey;

/**
 *
 * @author Harpreet Singh
 */
public class verifyFacialSignature {

    CommonUtil util = new CommonUtil();
    VerifyDigitalSignature verify = new VerifyDigitalSignature();
    ParseAsmObject pso = new ParseAsmObject();
    FacialBean fb = new FacialBean();

    String SHASignatureAlgo;
    String RSAContentType="2a864886f70d010903";
    // This is OID for PIV-CHUIDSecurity Object.
    boolean lb_signature = true;

    public verifyFacialSignature() {
        super();
    }

    public FacialBean verifySig(String str_hex_fac, String str_hex_chuid) throws MiddleWareException {
        PublicKey pubkey;
        byte[] fac_data;
        byte[] signature;
        byte[] signed_data = null;
        boolean lb_verify = true;

        signature = getSignature(str_hex_fac);

        if (signature == null) {
            lb_signature = false;
            lb_verify = false;
            fb.setSignatureVerified(lb_verify);
        }


        fac_data = getFacData(str_hex_fac);
        fb.setFacial_data(fac_data);
        System.out.println("imgae 2 " + util.arrayToHex(fac_data));

        // get Public Key.
        pubkey = pso.getPublicKey(str_hex_chuid);

        if (pubkey == null) {
            lb_verify = false;
            fb.setSignatureVerified(lb_verify);
        }

        //get the actual data that was signed
        if (lb_verify) {
            signed_data = getSignedData(str_hex_fac);
        }

        //util.arrayToHex(  signature);

        if (lb_verify) {
            lb_verify = verify.VerifySignature(signed_data, signature, pubkey, SHASignatureAlgo);

        }

        fb.setSignatureVerified(lb_verify);

        return fb;

    }

    private byte[] getFacData(String str_hex_fac) {

        String fac_data = null;
        //,str_temp_fac, fac_identifier = "46414300";
        String image_start_tag = "ffd8";
        String image_end_tag = "ffd9";
        int start_loc, end_loc;


        // Now look for image_start_tag. Subtract 3 to get the pointer to start of image tag
        start_loc = util.findEndLocation(str_hex_fac, image_start_tag) - 4;
        //end_loc = util.findEndLocation(str_hex_fac, image_end_tag);

        end_loc = str_hex_fac.lastIndexOf(image_end_tag)+4;
        fac_data = str_hex_fac.substring(start_loc, end_loc);

        return util.hex1ToByteArray(fac_data);

    }

    private byte[] getSignedData(String str_hex_fac) {
        int i_startRSA,i_startcontent;
        String any_tag = "a0";
        String str_content_len,str_content;
        int contentLen,i_start;

        i_startRSA = str_hex_fac.toLowerCase().indexOf(RSAContentType.toLowerCase());
        i_startcontent=str_hex_fac.toLowerCase().lastIndexOf(any_tag, i_startRSA);

        str_hex_fac = str_hex_fac.substring(i_startcontent);

         if (str_hex_fac.substring(2,  4).equals("82")) {
                    str_content_len = str_hex_fac.substring(4,8);
                    i_start=8;
                } else if (str_hex_fac.substring(2, 4).equals("81")) {
                    str_content_len = str_hex_fac.substring(4,6);
                    i_start=6;
                } else {
                    str_content_len = str_hex_fac.substring(4);
                    i_start=4;
                }
        contentLen = Integer.valueOf(str_content_len, 16).intValue();
        str_content = str_hex_fac.substring(2, i_start   + contentLen * 2);
        str_content = "31" + str_content;

        return util.hex1ToByteArray(str_content);

    }


    private byte[] getSignature(String str_hex_fac) {

        int i_startsig = 0, sigLen;
        // String octet_tag = "04";
        String str_sig;
        byte[] sig_encoded;
        String strNullSeprator = "05";
        boolean loopVar = true;
        int i = 0;
        String sig_length;

        while (loopVar == true) {
            //Find End of "SHA512WithRSA" OID and add 1 to get to start of Certificate
            //i_startsig = util.findEndLocation(str_hex_chuid, SHAWithRSASIG[i]);
            i_startsig = util.findFromEnd(str_hex_fac,UcmsMiddlewareConstants.SHAWithSIG[i]);

            if (i_startsig > 0 || i == UcmsMiddlewareConstants.SHAWithSIG.length - 1) {
                SHASignatureAlgo = UcmsMiddlewareConstants.SHAWithSIG[i];
                loopVar = false;
            }
            i++;
        }



        if (i_startsig == 0) {

            sig_encoded = null;
        } else {


            if (str_hex_fac.substring(i_startsig, i_startsig+2).equalsIgnoreCase(strNullSeprator)) {
                sig_length = str_hex_fac.substring(i_startsig + 8, i_startsig + 12);
                i_startsig = i_startsig+4;
            }else{
                sig_length = str_hex_fac.substring(i_startsig + 4, i_startsig + 8);
            }

            sigLen = Integer.valueOf(sig_length, 16).intValue();

            str_sig = str_hex_fac.substring(i_startsig + 8, i_startsig + 8 + sigLen * 2);
            sig_encoded = util.hex1ToByteArray(str_sig);

        }


        return sig_encoded;
    }

}
