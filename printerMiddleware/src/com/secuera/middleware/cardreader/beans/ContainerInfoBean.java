/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.beans;

import java.util.List;

/**
 *
 * @author Satish K
 */
public class ContainerInfoBean {
    List PIVObjectsBean;

    public List getPIVObjectsBean() {
        return PIVObjectsBean;
    }

    public void setPIVObjectsBean(List PIVObjectsBean) {
        this.PIVObjectsBean = PIVObjectsBean;
    }

    public String toString() {
        return "ContainerInfoBean{" + "PIVObjectsBean=" + PIVObjectsBean + '}';
    }

    
}
