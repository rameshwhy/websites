package com.id3.fingerprint;

public class FingerCaptureStatus
{
  public static final int Stopped = 0;
  public static final int PlaceFinger = 1;
  public static final int RemoveFinger = 2;
  public static final int RollFinger = 3;
  public static final int FingerPlaced = 4;
  public static final int FingerRemoved = 5;
  public static final int SwipeTooFast = 6;
  public static final int SwipeTooSlow = 7;
  public static final int SwipeTooSkewed = 8;
  public static final int SwipeBadSpeed = 9;
  public static final int ImageTooSmall = 10;
  public static final int ImagePoorQuality = 11;
  public static final int MoveFingerUp = 12;
  public static final int MoveFingerDown = 13;
  public static final int MoveFingerLeft = 14;
  public static final int MoveFingerRight = 15;
  public static final int PressFingerHarder = 16;
  public static final int RollPartLift = 17;
  public static final int RollTooFast = 18;
  public static final int RollDirty = 19;
  public static final int RollShifted = 20;
  public static final int RollTooShort = 21;
  public static final int FewerFingers = 22;
  public static final int TooManyFingers = 23;
  public static final int WrongHand = 24;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerCaptureStatus
 * JD-Core Version:    0.6.2
 */