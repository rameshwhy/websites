package com.id3.fingerprint;

public class FingerImageFormat
{
  public static final int Unknown = 0;
  public static final int BMP = 1;
  public static final int PNG = 2;
  public static final int TIFF = 3;
  public static final int JPEG = 4;
  public static final int JPEG2000 = 5;
  public static final int WSQ = 6;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerImageFormat
 * JD-Core Version:    0.6.2
 */