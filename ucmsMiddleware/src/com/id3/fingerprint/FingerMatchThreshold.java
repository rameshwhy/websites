package com.id3.fingerprint;

public class FingerMatchThreshold
{
  public static final int FMR100 = 2000;
  public static final int FMR1000 = 2700;
  public static final int FMR10000 = 3400;
  public static final int FMR100000 = 4100;
  public static final int FMR1000000 = 4800;
  public static final int FMR10000000 = 5500;
  public static final int LowSecurity = 2000;
  public static final int MediumSecurity = 2700;
  public static final int HighSecurity = 3400;
  public static final int VeryHighSecurity = 4100;
}

/* Location:           C:\SecuEra Demo\cardPersonalization\lib\com.id3.fingerprint.jar
 * Qualified Name:     com.id3.fingerprint.FingerMatchThreshold
 * JD-Core Version:    0.6.2
 */