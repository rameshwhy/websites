/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secuera.middleware.cardreader.core;

import com.secuera.middleware.cardreader.entities.MiddleWareException;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *
 * @author Harpreet Singh
 */
public class DiscoverKeys {

    private CommonUtil util = new CommonUtil();
    private SendCommand sendCard = new SendCommand();

    public byte[] getAdminKeyAlgo(CardChannel channel) throws MiddleWareException
    {

        //  Send : 00 CB 3F FF 00 00 04 5C 02 3F F7 
        // Recv : 53 82 01 38 

          ////9A 07 00 00 00 00 00 81 02 06 12 16 
          //9B 08 35 00 00 00 00 81 FF 06 10 16 
           //9C 14 00 00 00 00 00 81 05 06 15 16    
      
        byte[] key_algo = null;
        StringBuffer sb = new StringBuffer();
        byte[] res = null;
        byte[] command_data = {0x00, (byte) 0xCB, 0x3F, (byte) 0xFF, 0x00,0x00,
            0x04, 0x5C, (byte) 0x02, 0x3F, (byte) 0xF7};

        //prepare APDU command for authorisation
        CommandAPDU DISCOVER_KEY_APDU = new CommandAPDU(command_data);

        // System.out.println("1st command" + util.arrayToHex(command_data));

        try {

            res = sendCard.sendCommand(channel, DISCOVER_KEY_APDU, sb);
            if (!sb.toString().equals("9000")) {
                throw new MiddleWareException("Error reading Key Discovery container. Error Code = " + sb);
            }

        } catch (CardException e1) {
            throw new MiddleWareException("Error Authenticating the Card.");
        }

        
        
        return key_algo;
        
    }

    
}
